

public class Zaokruhli
{
  /**
   * metóda zaokrúhli pretypovaním číslo na tri desatinné miesta
   */
  public static double zaokruhluj(double paCislo)
  {
      paCislo=paCislo*1000;
      int z=(int)paCislo;
      double v=(double)z/1000;
      return v;
  }
}

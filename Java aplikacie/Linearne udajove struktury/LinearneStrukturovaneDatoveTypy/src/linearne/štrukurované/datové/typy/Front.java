/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import linearne.štrukurované.datové.typy.GUI.GUI;
import linearne.štrukurované.datové.typy.Objekty.Automobil;
import linearne.štrukurované.datové.typy.Objekty.IObjekt;

/**
 * Trieda Front predstavuje abstraktný datový typ FRONT, ktorý je implementovaný
 * binárnym súborom(prvky sú uložené v binárnom súbore). Je potomkom abstraktnej
 * triedy ILinearDataTypy.
 *
 * @author Dominik
 */
public class Front extends ILinearDataTypy {

    private int aMohutnost;
    private File aPrvy;
    private File aPosledny;
    private int aCisloSuboruPrvehoPrvku;
    private File aPodadresar;
    private GUI aGui;

    /**
     * Konštruktor príjme ako parameter Gui,ktorý zabezpečuje jeho zobrazenie a
     * inicializuje atrbúty triedy front na počiatočnú hodnotu- aMohutnost =0,
     * aPrvy = null; aPosledny = null;aGui = g; aPodadresar = novy podadresar
     * "prvky frontu". Do adresára prvky frontu budú ukladané prvky, ktoré front
     * aktuálne používa * !!!Trieda Gui musí obsahovať metódu
     * setAktualnePouzivanyTab(ILinearDataTypy paTyp), ktorá v prípade, že toto
     * Gui používajú aj iné ADT, nastaví aktuálne používaný ADT .
     *
     * @param g - trieda, ktorá sa postará o zobrazenie frontu užívateľovi.
     */
    //AK nepoužívae GUI, tak stači s tejto tiredy vymazať metódu zobrazSa(gg...), ostatné metódy 
    //by mali fungovať
    public Front(GUI g) {
        aPrvy = null;
        aGui = g;
        aMohutnost = 0;
        aPodadresar = new File("Prvky Frontu");
        if (!aPodadresar.exists()) {
            aPodadresar.mkdir();//vytvori novy priecinok v pricinku programu kde budu ulozene prvky frontu

        }
        //String menoAktAdresara = System.getProperty("user.dir");
        //System.out.println("Meno aktualneho adresara je: " + menoAktAdresara);//vypise prieconok kde cestu k priecinku kde budu ukladane subory
    }

    /**
     * Metóda zruší ADS front a jeho prvky;
     */
    @Override
    public void zrus() {
        aPrvy = null;
        aPosledny = null;
        aMohutnost = 0;
    }

    /**
     * Metóda vráti aktuálny počet prvkov vo fronte.
     *
     * @return boolean = true-front je prázdny; false-front nie je prázdny.
     */
    @Override
    public boolean jePrazdny() {
        return aMohutnost == 0;
    }

    /**
     * Metóda pridá prvok na koniec frontu(za posledne vložený prvok).
     *
     * @param paObjekt= objekt, ktorý bude pridaný do frontu
     */
    public void pridajNaKoniec(IObjekt paObjekt) throws IOException {
        try {

            File subor = new File(aPodadresar, "p" + aMohutnost + ".txt");
            subor.createNewFile();

            FileOutputStream fileOut = new FileOutputStream(subor);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(paObjekt);
            fileOut.close();
            out.close();

            if (aPrvy == null) {
                aPrvy = subor;
                aCisloSuboruPrvehoPrvku = aMohutnost;
            }
            aPosledny = subor;
            aMohutnost++;
        } catch (IOException ex) {
            throw ex;
        }

    }

    /**
     * Metóda odoberie prvok(objekt), ktorý bol do frontu vložený ako prvý
     *
     * @return IObjekt - odobraný prvok(objekt)
     */
    public IObjekt odoberZoZaciatku() throws IOException, ClassNotFoundException, FileNotFoundException {
        IObjekt lpNavrat = null;
        ObjectInputStream in;
        try (FileInputStream fileIn = new FileInputStream(aPrvy)) {
            in = new ObjectInputStream(fileIn);
            lpNavrat = (IObjekt) in.readObject();
        }
        in.close();
        aMohutnost--;
        aPrvy.delete();
        aPrvy = new File(aPodadresar, "p" + (++aCisloSuboruPrvehoPrvku) + ".txt");
        if (aMohutnost == 0) {
            aPrvy = null;
            aPosledny = null;
        }
        return lpNavrat;

    }

    /**
     * Metóda vráti prvok, ktorý bol do frontu vložený ako prvý
     *
     * @return IObjekt - prvok, ktorý sa nachádza na začiatku frontu a má sa
     * zobraziť
     * @throws FileNotFoundException - chyba ak sa súbor, ktorý obsahuje
     * informácie o prvku nepodarilo zobraziť
     * @throws IOException
     * @throws ClassNotFoundException - chyba ak sa zo súboru uloženého v
     * objekte nedá vytvoriť žiadny objekt, ktorý by sa zobrazil
     * @throws NullPointerException - chyba ak sa žiadny prvok vo fronte zatiaľ
     * nenachádza
     */
    @Override
    public IObjekt citajZaciatok() throws FileNotFoundException, IOException, ClassNotFoundException {
        IObjekt lpNavrat = null;
        try {
            FileInputStream fileIn = new FileInputStream(aPrvy);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            lpNavrat = (IObjekt) in.readObject();
            fileIn.close();
            in.close();
            return lpNavrat;
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        } catch (ClassNotFoundException ex) {
            throw ex;
        } catch (NullPointerException ex) {
            throw ex;
        }

    }

    /**
     * Metóda vraáti prvok, ktorý bol do frontu vložený ako posledný
     *
     * @return IObjekt-prvok, ktorý sa nachádza na konci frontu,vložený ako
     * posledný
     * @throws FileNotFoundException - chyba ak sa súbor, ktorý obsahuje
     * informácie o prvku nepodarilo zobraziť
     * @throws IOException
     * @throws ClassNotFoundException - chyba ak sa zo súboru uloženého v
     * objekte nedá vytvoriť žiadny objekt, ktorý by sa zobrazil
     * @throws NullPointerException - chyba ak sa žiadny prvok vo fronte zatiaľ
     * nenachádza
     */
    public IObjekt citajKoniec() throws FileNotFoundException, IOException, ClassNotFoundException {
        IObjekt lpNavrat = null;
        try {
            FileInputStream fileIn = new FileInputStream(aPosledny);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            lpNavrat = (IObjekt) in.readObject();
            fileIn.close();
            in.close();
            return lpNavrat;
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        } catch (ClassNotFoundException ex) {
            throw ex;
        } catch (NullPointerException ex) {
            throw ex;
        }

    }

    /**
     * Metóda zobrazí/vzkreslí front a jeho prvky na plátno.
     *
     * @param gg - plátno kde bude front zobrazený
     * @param poziciaX-pozicia x na plátne kde bude front vykreslený, v programe
     * je násobená *10,čim dostaneme rozlíšenie plátna(šírka)
     * @param poziciaY -pozicia y na plátne kde bude front vykreslený, v
     * programe je násobená *10, čim dostaneme rozlíšenie plátna(výška)
     */
    @Override
    public void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY) {
        File aZobrazeny = null;
        //gg.drawString(this.toString(), poziciaX, poziciaY);
        int width = poziciaX * 10;
        int height = poziciaY * 20;
        aGui.setAktualnePouzivanyTab(this);
        gg.drawString(new Date().toString(), 20, height - 20);
        if (aPrvy != null) {
            for (int i = 0; i < aMohutnost; i++) {
                try {
                    gg.drawString("Prvy Prvok: ", width / 2 - 40 * width / 200 - 60, 5 * height / 100 + (5 * height / 100) - 5 * height / 200);

                    gg.drawRect(width / 2 - 40 * width / 200, 5 * height / 100 + i * (5 * height / 100), 40 * width / 100, 5 * height / 100);
                    IObjekt lpNavrat = null;
                    aZobrazeny = new File(aPodadresar, "p" + (aCisloSuboruPrvehoPrvku + i) + ".txt");
                    FileInputStream fileIn = new FileInputStream(aZobrazeny);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    lpNavrat = (IObjekt) in.readObject();
                    gg.setColor(Color.DARK_GRAY);
                    gg.drawString(lpNavrat.toString(), width / 2 - 30 * width / 200 + 2, 5 * height / 100 + (i + 1) * (5 * height / 100) - 5 * height / 200);
                    gg.setColor(Color.BLACK);
                    fileIn.close();
                    in.close();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Front sa nezobrazil spravne", "ERROR=Zobrazenie front", JOptionPane.ERROR_MESSAGE);
                } catch (ClassNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Front sa nezobrazil spravne", "ERROR=Zobrazenie front", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }

    /**
     * Metóda vracia hodnotu atribútu aMohutnosť, ktorý v sebe udržiava hodnotu
     * predstavujúcu aktuálny počet prvkov vložených vo fronte.
     *
     * @return aMohutnost
     */
    public int mohutnost() {
        return aMohutnost;
    }

    /**
     * Metóda načíta prvky, ktoré sme si predtým uložili do súboru z tohoto
     * súboru do zoznamu. Staré prvky sa stratia.
     *
     * @throws FileNotFoundException-chyba ak neboli zatiaľ uložené žiadne prvky
     * alebo ak sa niektorý zo súborov nepodarilo načitať, lebo ho používateľ
     * upravil v textovom editore.
     * @throws IOException-chyba, ktorá nastala v inom prípade
     */
    public void nacitajPrvkyZoSuboru() throws FileNotFoundException, IOException, NullPointerException {
        File podadresarUlozPrvkyFrontu = new File("Front ulozisko");
        if (!podadresarUlozPrvkyFrontu.exists() || podadresarUlozPrvkyFrontu.list().length == 0) {

            throw new FileNotFoundException("Zatial neboli ulozene ziadne prvky");
        }
        zrus();
        String[] nazvyUlozSuborov = podadresarUlozPrvkyFrontu.list();
        if (nazvyUlozSuborov.length == 0) {
            throw new FileNotFoundException();
        }

        for (int i = 0; i < nazvyUlozSuborov.length; i++) {
            pridajNaKoniec(vytvorObjektZoSuboru(podadresarUlozPrvkyFrontu, najdiSuborSporadovymCislom(nazvyUlozSuborov, 1000 + i)));
        }

    }

    /**
     * Metóda uloží prvky, ktoré sa aktuálne nachádzaju vo fronte do súboru.
     * Prvky, ktoré boli uložené predtým sa stratia(vymažú).
     *
     * @throws FileNotFoundException- chyba, ak sa nepodarilo vytvoriť súbor pre
     * zápis, kvôli tomu, že taký súbor už existuje.
     */
    @Override
    public void zapisPrvkyDoSuboru() throws FileNotFoundException {

        File podadresarUlozPrvkyFrontu = new File("Front ulozisko");
        if (!podadresarUlozPrvkyFrontu.exists()) {
            podadresarUlozPrvkyFrontu.mkdir();
        } else if (podadresarUlozPrvkyFrontu.list().length > 0) {
            File prvky[] = podadresarUlozPrvkyFrontu.listFiles();
            for (int i = 0; i < prvky.length; i++) {
                prvky[i].delete();
            }
        }

        File aZobrazeny = null;
        for (int i = 0; i < aMohutnost; i++) {
            FileInputStream fileIn = null;
            try {
                IObjekt lpNavrat = null;
                aZobrazeny = new File(aPodadresar, "p" + (aCisloSuboruPrvehoPrvku + i) + ".txt");
                fileIn = new FileInputStream(aZobrazeny);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                lpNavrat = (IObjekt) in.readObject();
                fileIn.close();
                in.close();

                lpNavrat.zapisDoSuboru(podadresarUlozPrvkyFrontu, i);
            } catch (FileNotFoundException ex) {
                FileNotFoundException fe = new FileNotFoundException("Front=nepodarilo sa ulozit cely.");
                throw fe;
            } catch (IOException ex) {
                Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fileIn.close();
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Metóda zobrazí v textovom poli všetky prvky frontu, ktoré s v ňom
     * aktuálne nachádzajú.
     *
     * @param poziciaX-pozícia x(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param poziciaY-pozícia y(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param height-výška iného okna, ktoré vyvoláva túto metódu
     * @param width-šírka iného okna, ktoré vyvoláva túto metódu
     */
    @Override
    public void uplnaPrehliadka(int poziciaX, int poziciaY, int height, int width) {

        JDialog lpPrehliadka = new JDialog();
        lpPrehliadka.setTitle("Front-uplna prehliadka");
        lpPrehliadka.setSize(500, 500);
        lpPrehliadka.setLayout(new BorderLayout());

        JTextArea lpTextovePole;
        JScrollPane lpPosuvatko;
        JPanel center = new JPanel();

        center.setLayout(new GridLayout(1, 1));
        lpPrehliadka.add(center, BorderLayout.CENTER);
        lpTextovePole = new JTextArea("Vsetky objekty a ich poradie od prveho ulozeneho: \n");

        File podadresarUlozPrvkyFrontu = new File("Front ulozisko");
        File aZobrazeny = null;

        for (int i = 0; i < aMohutnost; i++) {
            FileInputStream fileIn = null;
            try {
                IObjekt lpNavrat = null;
                aZobrazeny = new File(aPodadresar, "p" + (aCisloSuboruPrvehoPrvku + i) + ".txt");
                fileIn = new FileInputStream(aZobrazeny);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                lpNavrat = (IObjekt) in.readObject();
                fileIn.close();
                in.close();

                lpTextovePole.append((i + 1) + ". " + lpNavrat.toString() + "\n");

            } catch (IOException ex) {
                Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fileIn.close();
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        lpTextovePole.setEditable(false);
        lpPosuvatko = new JScrollPane(lpTextovePole, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        center.add(lpPosuvatko);

        lpPrehliadka.setLocation(poziciaX + width / 2 - lpPrehliadka.getWidth() / 2, poziciaY + height / 2 - lpPrehliadka.getHeight() / 2);
        lpPrehliadka.setModal(false);
        lpPrehliadka.setVisible(true); //zobraz okno 
    }

}

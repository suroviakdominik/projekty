/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.spusteniehry;

import Grafika.Platno;
import java.util.HashMap;
import java.util.Scanner;
import šach.hra.InicializaciaHry.IInicializator;
import šach.hra.InicializaciaHry.InicializatorNovejHry;
import šach.hra.InicializaciaHry.InicializatorUlozenejHry;

/**
 * Táto trieda zabezpečuje komunikáciu s používateľom hneď po spustení programu.
 * Poskytne používateľovi rôzne možnosti hry, vyzve ho na zadanie voľby a danú
 * voľbu spracuje.
 *
 * @author Dominik
 */
public class Menu {

    private Scanner aScan;
    private HashMap<Integer, IInicializator> aInicializaciaHry;

    /**
     * Pri vytváraní objektu tejto triedy sa v konštruktore vytvoria objekty
     * typu Scanner,HashMap, InicializatorNovejHry, InicializatorUlozenejHry.
     * Scanner má za úlohu načítavať jednotlivé voľby.HashMap zhromažďuje
     * spomenuté inicializátory.
     */
    public Menu() {
        aScan = new Scanner(System.in);
        aInicializaciaHry = new HashMap<>();
        aInicializaciaHry.put(1, new InicializatorNovejHry(this));
        aInicializaciaHry.put(2, new InicializatorUlozenejHry(this));
    }

    /**
     * má za úlohu do terminálu vypísať všetky voľby, ktoré dokáže hlavné menu
     * spracovať
     */
    private void vypisHlavneMenu() {
        System.out.println("------Vitajte v hre Šach------");
        System.out.println("-------- 1- Nová hra ---------");
        System.out.println("---  2- Načítať Uloženú Hru --");
        System.out.println("---      3- Ukonci Hru -------");
    }

    /**
     * pri volaní tejto metódy sa ako prvé zavolá metóda vypisHlavneMenu().
     * Potom výpis požiada používateľa o zadanie voľby. Na spracovanie voľby sa
     * použije metóda voľbaHlavneMenu(), ktorá vyhodnotí či je zadaná voľba
     * správna alebo neprávna.
     */
    public void spustiHlavneMenu() {
        vypisHlavneMenu();
        boolean lpVykonavat = false;
        IInicializator lpHra = null;
        int lpVolba;
        do {

            System.out.println("Zadajte voľbu: ");
            System.out.print("> ");

            if (aScan.hasNextInt()) {
                lpVolba = aScan.nextInt();
                lpVykonavat = volbaHlavneMenu(lpVolba);
            }
            if (lpVykonavat == false) {
                System.out.println("Vašu voľbu neviem spracovať. Prosím zadajte číslo prislúchajúce danej voľbe. ");
            }

        } while ((!lpVykonavat));

    }

    /**
     * ako parameter dostane táto metóda voľbu zadanú v predchádzajúcej metóde.
     * Nasledovne sa táto voľba vyhodnotí. Ak používateľ zadal voľbu 1, t. j.
     * nová hra, zavolá sa inicializátor, ktorý sa nachádza v HashMap na pod
     * kľúčom 1 a pošle sa mu správa inicializuj(). Ak hráč zadal voľbu 2, t.j.
     * načítanie uloženej hry, zavolá sa metóda menuUlozenejHry. Pri voľbe 3 sa
     * hra ukončí. Pri nekorektnej voľbe sa žiadosť o zadanie voľby opakuje.
     *
     * @param paVolba - volba poskytnutá hráčom
     * @return vráti true-korektná voľba, false - nekorektná voľba
     */
    private boolean volbaHlavneMenu(int paVolba) {
        switch (paVolba) {
            case 1:
                aInicializaciaHry.get(1).inicializuj();
                return true;

            case 2:
                System.out.println("\f");
                menuUlozenejHry();
                return true;

            case 3:
                System.out.print("Hra sa ukončila.");
                if (Platno.jeVytvorenePlatno() == true) {
                    System.out.println(" Vypnite platno s grafickymi objektami.");
                }

                return true;

            default:
                return false;
        }

    }

    /**
     * vypíše možnosti, ktoré môže používateľ použiť ak chce načítať predtým
     * uloženú hru.
     */
    private void vypisMenuUlozenejHry() {
        System.out.println("------------Menu uloženej hry----------");
        System.out.println("-------- 1- Ulozena Pozicia 1 ---------");
        System.out.println("-------  2- Ulozena Pozicia 2 ----------");
        System.out.println("------------3- Hlavné Menu -------------");
    }

    /**
     * ak chce hráč načítať uloženú hru, zavolá sa táto metóda, ktorá v prvom
     * kroku pomocou metódy vypisMenuUlozenejHry() poskytne používateľovi voľby,
     * ktoré môže použiť. Potom sa použije scanner ktorý voľbu načíta. Táto
     * voľba sa pošle metóde volbaMenuUlozenejHry(int paVolba).
     */
    private void menuUlozenejHry() {
        vypisMenuUlozenejHry();
        boolean lpVykonavat;
        do {
            System.out.println("Zadajte voľbu: ");
            System.out.print("> ");
            if (aScan.hasNextInt()) {
                int lpVolba = aScan.nextInt();
                lpVykonavat = volbaMenuUlozenejHry(lpVolba);
            } else {
                System.out.println("Vašu voľbu neviem spracovať.");
                lpVykonavat = false;
            }

        } while (!lpVykonavat);
    }

    /**
     * vyhodnotí voľbu načítanú v predchádzajúcej metóde.Ak je voľba korektná
     * vráti true, inak vráti false. Pri voľbe 1 aj 2 sa zavolá inicializátor,
     * ktorý sa nachádza v HashMap a k nemu prislúchajúci kľúč je 2 a pošle sa
     * mu správa inicializuj(). Voľby 1 aj 2 sa líšia iba tým z akej uloženej
     * pozície sa daná hra načíta. Ak nie voľba správna metóda vráti false a
     * zopakuje sa žiadosť o zadanie voľby.
     *
     * @param paVolba- volba zadaná hráčom
     * @return true- korektná voľba, false nekorektná voľba
     */
    private boolean volbaMenuUlozenejHry(int paVolba) {
        switch (paVolba) {
            case 1:
                InicializatorUlozenejHry inicUlozHry = (InicializatorUlozenejHry) aInicializaciaHry.get(2);
                inicUlozHry.vlozSuborSUlozenouPoziciou("ulozenaPozicia1.txt");

                return inicUlozHry.inicializuj();

            case 2:
                InicializatorUlozenejHry inicUlozHry2 = (InicializatorUlozenejHry) aInicializaciaHry.get(2);
                inicUlozHry2.vlozSuborSUlozenouPoziciou("ulozenaPozicia2.txt");

                return inicUlozHry2.inicializuj();

            case 3:
                System.out.print("\f");
                spustiHlavneMenu();
                return true;

            default:
                return false;
        }
    }

}

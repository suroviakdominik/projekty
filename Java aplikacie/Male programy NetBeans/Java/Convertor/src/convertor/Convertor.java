package convertor;


/**
 *
 * @author Hawk
 */
public class Convertor {

    public int HexaToDecimal(String hexa) {
        int deci= Integer.parseInt(hexa,16);
        return deci;
    }

    public String DecimalToHexa(int deci) {
        String hexa = Integer.toHexString(deci);
        return hexa;
    }
    
}

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function upravZviera() {
        $nazov = htmlspecialchars($_POST["nazovKat"]);
        $popis = htmlspecialchars($_POST["popisKat"]);
        $id = htmlspecialchars($_POST["idKat"]);
       
        $sql = sprintf("UPDATE kategoria SET meno='%s',popis='%s' WHERE id=%d",
                $this->conn->real_escape_string($nazov), 
                $this->conn->real_escape_string($popis),
                $this->conn->real_escape_string($id));

        if ($this->conn->query($sql) === TRUE) {
            
            echo "Kategoria bola upravena.";
            header("Location: index.html");
        } else {
            echo "Error updating record: " . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}
if (isset($_SESSION["login"]) && $_SESSION["login"] == 0) {
$db = new DBPraca();
$db->upravZviera();
$db->ukonciPristup();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import šach.sachovnica.figurky.Figurka;
import Grafika.IGrafickeObjekty;
import Grafika.*;

/**
 * Trieda Dama je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Dáma extends Figurka {

    private IGrafickeObjekty[] aVykreslenieDamy;

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba dámy
     */
    public Dáma(String paFarba) {
        super("Dáma", paFarba);
        aVykreslenieDamy = new IGrafickeObjekty[3];
    }

    /**
     * je metóda, ktorá vykreslí dámu na plátno.
     */
    @Override
    public void nakresli() {
        aVykreslenieDamy[0] = new Obdlznik(30, 10, this.getX(), this.getY() + 15, this.getFarba());
        aVykreslenieDamy[1] = new Elipsa(20, 30, this.getX() + 4, this.getY() - 10, this.getFarba());
        aVykreslenieDamy[2] = new Kruh(10, this.getX() + 8, this.getY() - 18, this.getFarba());
        for (IGrafickeObjekty prechadzac : aVykreslenieDamy) {
            prechadzac.zobraz();
        }
    }

    /**
     * -je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie dámy.
     *
     * @return grafické objekty potrebné na vykreslenie dámy
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aVykreslenieDamy;
    }

    /**
     * metóda vracajúca popis dámy
     *
     * @return popis dámy vo forme stringu
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Dáma/Strelec" + "\n";
        popis += "Pohyb: po diagonálach" + "\n";
        popis += "Vyhadzovanie: po diagonálach" + "\n";
        popis += "Ine: pri pohybe a vyhadzovaní nesmie prejsť cez/ponad inú figúrku" + "\n";
        return popis;
    }
}

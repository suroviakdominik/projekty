<?php

session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function prihlas() {
        $mail = htmlspecialchars($_POST["email"]);
        $heslo = htmlspecialchars($_POST["password"]);
        $page = htmlspecialchars($_POST["page"]);

        $sql = sprintf("SELECT id,heslo FROM pouzivatelia WHERE email='%s'",
                $this->conn->real_escape_string($mail));
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            $row = mysqli_fetch_assoc($result);
            echo "id: " . $row["id"] . "<br>";
            if (password_verify($heslo, $row["heslo"])) {
                //echo "dobre heslo";
                $_SESSION["login"] = $row["id"];

                $uID = $row["id"];
                $sql = sprintf("UPDATE pouzivatelia SET poslednePrihlasenie=now() WHERE id=%d",
                        $this->conn->real_escape_string($uID));

                if ($this->conn->query($sql) === TRUE) {
                    echo "Record updated successfully";
                } else {
                    echo "Error updating record: " . $this->conn->error;
                }
                
                if($row["id"]==0) {
                    header("Location: ../Administrator/index.html");
                } else {
                    header("Location: $page");
                }
            } else {
                //echo "zle heslo";
                header("Location: $page");
            }
        } else {
            $_SESSION["registracia"] = true;
            header("Location: ../Registracia/Registracia.php");
        }
        $email = htmlspecialchars($_POST["email"]);
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

if (!empty($_POST["email"]) && !empty($_POST["password"])) {

    $databaza = new DBPraca();
    $databaza->prihlas();
    $databaza->ukonciPristup();
}
?>


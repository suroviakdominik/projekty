/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package datumy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Hawk
 */
public class DateMaker {

    private Date datum;

    public boolean isValidDate(String inDate) {
        if (inDate == null)
          return false;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (inDate.trim().length() != dateFormat.toPattern().length())
          return false;

        dateFormat.setLenient(false);

        try {
          this.datum = dateFormat.parse(inDate.trim());
        }  
        catch (ParseException pe) {
          return false;
        }

        return true;
    }

    public long calculateDifference() {
        Date today = new Date();

        long diff = today.getTime() - datum.getTime();

        return diff / (1000 * 60 * 60 * 24);
    }


}

import java.awt.*;

/**
 * �tvorec, s ktor�m mo�no pohybova� a nakresl� sa na pl�tno.
 * 
 * @author  Michael Kolling and David J. Barnes
 * @version 1.0  (15 July 2000)
 */

public class Stvorec
{
    private int aStrana;
    private int aXLavyHorny;
    private int aYLavyHorny;
    private String aFarba;
    private boolean aJeViditelny;

    /**
     * Vytvor nov� �tvorec preddefinovanej farby na preddefinovanej poz�cii.
     */
    public Stvorec()
    {
        aStrana = 30;
        aXLavyHorny = 60;
        aYLavyHorny = 50;
        aFarba = "red";
        aJeViditelny = false;
    }

    /**
     * (�tvorec) Zobraz sa.
     */
    public void zobraz()
    {
        aJeViditelny = true;
        nakresli();
    }
    
    /**
     * (�tvorec) Skry sa.
     */
    public void skry()
    {
        zmaz();
        aJeViditelny = false;
    }
    
    /**
     * (�tvorec) Posu� sa vpravo o pevn� d�ku.
     */
    public void posunVpravo()
    {
        posunVodorovne(20);
    }

    /**
     * (�tvorec) Posu� sa v�avo o pevn� d�ku.
     */
    public void posunVlavo()
    {
        posunVodorovne(-20);
    }

    /**
     * (�tvorec) Posu� sa hore o pevn� d�ku.
     */
    public void posunHore()
    {
        posunZvisle(-20);
    }

    /**
     * (�tvorec) Posu� sa dole o pevn� d�ku.
     */
    public void posunDole()
    {
        posunZvisle(20);
    }

    /**
     * (�tvorec) Posu� sa vodorovne o d�ku dan� parametrom.
     */
    public void posunVodorovne(int paVzdialenost)
    {
        zmaz();
        aXLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (�tvorec) Posu� sa zvisle o d�ku dan� parametrom.
     */
    public void posunZvisle(int paVzdialenost)
    {
        zmaz();
        aYLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (�tvorec) Posu� sa pomaly vodorovne o d�ku dan� parametrom.
     */
    public void pomalyPosunVodorovne(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aXLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (�tvorec) Posu� sa pomaly vodorovne o d�ku dan� parametrom.
     */
    public void pomalyPosunZvisle(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aYLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (�tvorec) Zme� d�ku strany na hodnotu dan� parametrom.
     * D�ka strany mus� by� nez�porn� cel� ��slo. 
     */
    public void zmenStranu(int paDlzka)
    {
        zmaz();
        aStrana = paDlzka;
        nakresli();
    }

    /**
     * (�tvorec) Zme� farbu na hodnotu dan� parametrom.
     * Nazov farby mus� by� po anglicky.
     * Mo�n� farby s� tieto:
     * �erven� - "red"
     * �lt�    - "yellow"
     * modr�   - "blue"
     * zelen�  - "green"
     * fialov� - "magenta"
     * �ierna  - "black"
     * biela - "white"
     * hned� - "brown"
     */
    public void zmenFarbu(String paFarba)
    {
        aFarba = paFarba;
        nakresli();
    }

    /*
     * Draw the square with current specifications on screen.
     */
    private void nakresli()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.draw(this, aFarba,
                        new Rectangle(aXLavyHorny, aYLavyHorny, aStrana, aStrana));
            canvas.wait(10);
        }
    }

    /*
     * Erase the square on screen.
     */
    private void zmaz()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

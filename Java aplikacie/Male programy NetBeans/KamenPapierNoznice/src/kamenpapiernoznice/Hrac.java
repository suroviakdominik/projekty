/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kamenpapiernoznice;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class Hrac {
    private String aMeno;
    private Double aKredit;
    private Double aZostavajuciKredit;
    private Random aGenerator;
    private Double aStavka;

    public Hrac(String aMeno, Double paKredit) {
        this.aMeno = aMeno;
        this.aKredit = paKredit;
        aGenerator=new Random();
        aZostavajuciKredit=paKredit;
    }

    public Double getaStavka() {
        return aStavka;
    }

    public void setaStavka(Double aStavka) {
        this.aStavka = aStavka;
    }
    
    

    public String getaMeno() {
        return aMeno;
    }

    public void setaMeno(String aMeno) {
        this.aMeno = aMeno;
    }

    public Double getaKredit() {
        return aKredit;
    }

    public void setaKredit(Double aKredit) {
        this.aKredit = aKredit;
    }

    public Double getaZostavajuciKredit() {
        return aZostavajuciKredit;
    }

    public void setaZostavajuciKredit(Double aZostavajuciKredit) {
        this.aZostavajuciKredit = aZostavajuciKredit;
    }
    
    public Stavka vygenerujTah()
    {
        int vyg=aGenerator.nextInt(3);
        return Stavka.values()[vyg];
    }
}

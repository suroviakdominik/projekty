

public enum Stav
{
    VOLNE(','),
    STENA('#'),
    PREHLADANE('_'),
    VCESTE('.'),
    START('S'),
    CIEL('C');
    private char aZnak;
    private Stav(char paZnak)
    {
        aZnak=paZnak;
    }
    
    public String toString()
    {
        String vys="";
        vys+=aZnak;
        return vys;
    } 
    
    public static Stav CharToStav(char paZnak)
    {
       switch(paZnak)
      {
          case ',':return VOLNE;
          case '#':return STENA;
          case '_':return PREHLADANE;
          case '.':return VCESTE;
          case 's':
          case 'S':return START;
          case  'c':
          case  'C':return CIEL;
          default:return null; 
       }
    }
    
    public boolean MozemSpustit()
    {
        return (this==VOLNE)||(this==VCESTE)||(this==START)||(this==CIEL);
    }
}

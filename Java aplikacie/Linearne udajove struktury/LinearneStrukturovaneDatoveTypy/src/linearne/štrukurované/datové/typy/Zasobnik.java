/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import linearne.štrukurované.datové.typy.GUI.GUI;
import linearne.štrukurované.datové.typy.Objekty.IObjekt;

/**
 * Trieda zásobník predstavue šablónu, podľa, ktorej môžu byť vytvárané objekty
 * typu ADT Zásobník. ADT Zásobník je implementovaný pomocou lineárne
 * zreťazeného zoznamu. Je potomkom abstraktnej triedy ILinearDataTypy.
 *
 * @author Dominik
 */
public class Zasobnik extends ILinearDataTypy {

    private class Uzol {

        private Uzol aPredchadzajuciUzol;
        private IObjekt hodnota;

        public Uzol(IObjekt paPrvok) {
            this.hodnota = paPrvok;
        }

        public void setPredchadzajuciUzol(Uzol aPredchadzajuciUzol) {
            this.aPredchadzajuciUzol = aPredchadzajuciUzol;
        }

        public Uzol getPredchadzajuciUzol() {
            return aPredchadzajuciUzol;
        }

        public IObjekt getHodnota() {
            return hodnota;
        }

    }
    private Uzol aVrcholZasobnika;
    private int aVelkostZasobnika;
    private GUI aGui;

    /**
     * Konštruktor príjme ako parameter Gui,ktorý zabezpečuje zobrazenie
     * zásobníka a inicializuje atrbúty triedy zásobník na počiatočnú hodnotu-
     * aVelkostZasobnika =0, aGui = g; aVrcholZasobnika = null. !!!Trieda Gui
     * musí obsahovať metódu setAktualnePouzivanyTab(ILinearDataTypy paTyp),
     * ktorá v prípade, že toto Gui používajú aj iné ADT, nastaví aktuálne
     * používaný ADT .
     *
     * @param g - trieda, ktorá sa postará o zobrazenie zásobníka užívateľovi.
     */
    //AK nepoužívae GUI, tak stači s tejto tiredy vymazať metódu zobrazSa(gg...), ostatné metódy 
    //by mali fungovať
    public Zasobnik(GUI g) {
        aVrcholZasobnika = null;
        aVelkostZasobnika = 0;
        aGui = g;
    }

    /**
     * Metóda zruší zásobník. Vymaže z neho všetky prvky.
     */
    @Override
    public void zrus() {
        aVelkostZasobnika = 0;
        aVrcholZasobnika = null;
    }

    /**
     * Metóda vloží prvok(objekt typu IObjekt) na vrchol zásobníka, t.j za
     * prvok, ktorý bol vložený ako posledný.
     *
     * @param paPrvok-objekt typu IObjekt, ktorý bude vložený na vrchol
     * zásobníka
     */
    public void vloz(IObjekt paPrvok) {
        aVelkostZasobnika++;
        Uzol novyUzol = new Uzol(paPrvok);
        novyUzol.setPredchadzajuciUzol(aVrcholZasobnika);
        aVrcholZasobnika = novyUzol;

    }

    /**
     * Metóda vyberie prvok(objekt typu IObjekt) z vrcholu zásobníka(ten ktorý
     * bol vložebý ako posledný)a vráti ho ako návratovú hodnotu.
     *
     * @return IObjek-(objekt typu IObjekt) vybraný z vrcholu zásobníka.
     */
    public IObjekt vyber() {

        try {
            aVelkostZasobnika--;
            IObjekt prvokNaVrchole = aVrcholZasobnika.getHodnota();
            aVrcholZasobnika = aVrcholZasobnika.getPredchadzajuciUzol();
            return prvokNaVrchole;
        } catch (NullPointerException ex) {
            throw ex;
        }

    }

    /**
     * Metóda vráti aktuálny počet prvkov vo zásobníku.
     *
     * @return boolean = true-zásobník je prázdny; false-zásobník nie je
     * prázdny.
     */
    @Override
    public boolean jePrazdny() {
        return aVrcholZasobnika == null;
    }

    /**
     * Metóda vráti prvok(objekt typu Iobjekt), ktorý bol do zásobníka vložený
     * ako posledný, je na vrchole zásobníka
     *
     * @return IObjekt-prvok(objekt typu Iobjekt), ktorý bol do zasobníka
     * vloźený ako posledný (nachádza sa na vrchole zásobníka.
     */
    @Override
    public IObjekt citajZaciatok()//vrati hodnotu na vrchole zasobnika 
    {
        return aVrcholZasobnika.getHodnota();
    }

    /**
     * Metóda vráti hodnotu atribútu aVelkostZasobnika, ktorý uchováva hodnotu o
     * aktuálnom počte prvokv(objekt typu Iobjekt) v zásobníku
     *
     * @return int -aktuálny počet prvkov v zásobníku
     */
    @Override
    public int mohutnost() {
        return aVelkostZasobnika;
    }

    /**
     * Metóda zobrazí/vzkreslí zásobník a jeho prvky na plátno.
     *
     * @param gg - plátno kde bude zásobník zobrazený
     * @param poziciaX-pozicia x na plátne kde bude zásobník vykreslený, v
     * programe je násobená *10,čim dostaneme rozlíšenie plátna(šírka)
     * @param poziciaY -pozicia y na plátne kde bude zásobník vykreslený, v
     * programe je násobená *10, čim dostaneme rozlíšenie plátna(výška)
     */
    @Override
    public void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY) {
        //gg.drawString(this.toString(), poziciaX, poziciaY);
        int width = poziciaX * 10;
        int height = poziciaY * 20;
        aGui.setAktualnePouzivanyTab(this);
        gg.drawString(new Date().toString(), 20, height - 20);
        Uzol lpZobrazeny = aVrcholZasobnika;
        if (aVrcholZasobnika != null) {
            for (int i = 0; i < aVelkostZasobnika; i++) {
                gg.drawString("Vrchol zasobnika: ", width / 2 - 40 * width / 200 - 100, 5 * height / 100 + (5 * height / 100) - 5 * height / 200);
                gg.drawRect(width / 2 - 40 * width / 200, 5 * height / 100 + i * (5 * height / 100), 40 * width / 100, 5 * height / 100);
                gg.drawString(lpZobrazeny.getHodnota().toString(), width / 2 - 30 * width / 200 + 2, 5 * height / 100 + (i + 1) * (5 * height / 100) - 5 * height / 200);
                lpZobrazeny = lpZobrazeny.getPredchadzajuciUzol();
            }
        }
    }

    /**
     * Metóda načíta prvky, ktoré sme si predtým uložili do súboru z tohoto
     * súboru do zásobníka. Staré prvky sa stratia.
     *
     * @throws java.io.FileNotFoundException-chyba súbor sa nepodarilo
     * načitať,bol buď pozmenený v textovom editore užívateľom alebo žiadny ešte
     * nebol vytvorený
     */
    @Override
    public void nacitajPrvkyZoSuboru() throws FileNotFoundException, IOException {
        File podadresarUlozPrvkyZasobnika = new File("Zasobnik ulozisko");
        if (!podadresarUlozPrvkyZasobnika.exists()) {
            throw new FileNotFoundException("Zatial neboli ulozene ziadne prvky");
        }
        zrus();
        String[] nazvyUlozSuborov = podadresarUlozPrvkyZasobnika.list();
        if (nazvyUlozSuborov.length == 0) {
            throw new FileNotFoundException();
        }

        for (int i = 0; i < nazvyUlozSuborov.length; i++) {
            vloz(vytvorObjektZoSuboru(podadresarUlozPrvkyZasobnika, najdiSuborSporadovymCislom(nazvyUlozSuborov, 1000 + nazvyUlozSuborov.length - i - 1)));

        }
    }

    /**
     * Metóda uloží prvky, ktoré sa aktuálne nachádzaju v zásobníku do súboru.
     * Prvky, ktoré boli uložené predtým sa stratia(vymažú).
     *
     * @throws FileNotFoundException- chyba, ak sa nepodarilo vytvoriť súbor pre
     * zápis, kvôli tomu, že taký súbor už existuje.
     */
    @Override
    public void zapisPrvkyDoSuboru() throws FileNotFoundException {
        File podadresarUlozPrvkyZasobnika = new File("Zasobnik ulozisko");
        if (!podadresarUlozPrvkyZasobnika.exists()) {
            podadresarUlozPrvkyZasobnika.mkdir();
        } else if (podadresarUlozPrvkyZasobnika.list().length > 0) {
            File prvky[] = podadresarUlozPrvkyZasobnika.listFiles();
            for (int i = 0; i < prvky.length; i++) {
                prvky[i].delete();
            }
        }
        Uzol prechadzac = aVrcholZasobnika;
        for (int i = 0; i < aVelkostZasobnika; i++) {
            try {
                prechadzac.getHodnota().zapisDoSuboru(podadresarUlozPrvkyZasobnika, i);
            } catch (IOException ex) {
                FileNotFoundException fe = new FileNotFoundException("Zasobnik=nepodarilo sa ulozit cely.");
                throw fe;
            }
            prechadzac = prechadzac.getPredchadzajuciUzol();
        }
    }

    /**
     * Metóda zobrazí v textovom poli všetky prvky zásobníka, ktoré s v ňom
     * aktuálne nachádzajú.
     *
     * @param poziciaX-pozícia x(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param poziciaY-pozícia y(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param height-výška iného okna, ktoré vyvoláva túto metódu
     * @param width-šírka iného okna, ktoré vyvoláva túto metódu
     */
    @Override
    public void uplnaPrehliadka(int poziciaX, int poziciaY, int height, int width) {
        JDialog lpPrehliadka = new JDialog();
        lpPrehliadka.setTitle("Zasobnik-uplna prehliadka");
        lpPrehliadka.setSize(500, 500);
        lpPrehliadka.setLayout(new BorderLayout());

        JTextArea lpTextovePole;
        JScrollPane lpPosuvatko;
        JPanel center = new JPanel();

        center.setLayout(new GridLayout(1, 1));
        lpPrehliadka.add(center, BorderLayout.CENTER);
        lpTextovePole = new JTextArea("Vsetky objekty a ich poradie od vrcholu zasobnika: \n");

        Uzol prechadzac = aVrcholZasobnika;
        for (int i = 0; i < aVelkostZasobnika; i++) {
            lpTextovePole.append((i + 1) + ". " + prechadzac.getHodnota().toString() + "\n");
            prechadzac = prechadzac.getPredchadzajuciUzol();
        }

        lpTextovePole.setEditable(false);
        lpPosuvatko = new JScrollPane(lpTextovePole, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        center.add(lpPosuvatko);

        //lpPrehliadka.pack(); //prizpusob velikost okna
        lpPrehliadka.setLocation(poziciaX + width / 2 - lpPrehliadka.getWidth() / 2, poziciaY + height / 2 - lpPrehliadka.getHeight() / 2);
        lpPrehliadka.setModal(false);
        lpPrehliadka.setVisible(true); //zobraz okno 
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.data;

/**
 *
 * @author Dominik
 */
public abstract class ImplementatorZaznam {
    protected Class aImplementatorForClass;
    protected static ImplementatorZaznam aInstance;

    protected ImplementatorZaznam(Class aImplementatorForClass) {
        this.aImplementatorForClass = aImplementatorForClass;
    }
    
    public abstract Zaznam toZaznam(byte paBinDataZaznam[]);

    public abstract byte[] toByteArray(Zaznam paZaznam);
    
    public abstract int get_Velkost_Zaznamu_V_Bajtoch();//velkoet zaznamu pre ktory som implementatorom v bajtoch - t.j atributy a ich velkost v pamati
    
    public abstract Zaznam vytvorNeplatnyZaznam();
}

package com.suroviak.dominik.zooAplication.FragmentsInfoAboutZoo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;

/**
 * Created by Dominik on 12.3.2015.
 */
public class Ticket_Prices_Fragmet extends Fragment {

    private String aNameOfRootDownloadDir;
    private Info_About_Dirs_File aInfoFileInfo_About_ZOO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_ticket_prices, container, false);
        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.INFO.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.INFO.getaNameOfInfoFile());
        try {
            aInfoFileInfo_About_ZOO = (Info_About_Dirs_File) sw.readObjetctFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        FileWorker fw = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER +
                aInfoFileInfo_About_ZOO.getaItemsInfo().get("I2").getRelativePathOfFilesOfurrentItemInDevice() + "popis.txt");
        String nacitaneInfo = "";
        try {
            nacitaneInfo = fw.readDataFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String parsInfo[] = nacitaneInfo.split(";;;");
        nacitajCeny(rootView, parsInfo[0]);
        TextView tw = (TextView) rootView.findViewById(R.id.text_cennik_popis);
        tw.setText("\n\n" + parsInfo[1] + "\n\n\n");

        return rootView;
    }

    private void nacitajCeny(View rootView, String s) {
        String ceny[] = s.split("\\|\\|");
        TextView tw = (TextView) rootView.findViewById(R.id.text_cena_leto_deti);
        tw.setText(ceny[0] + " €");

        tw = (TextView) rootView.findViewById(R.id.text_cena_zima_deti);
        tw.setText(ceny[1] + " €");

        tw = (TextView) rootView.findViewById(R.id.text_cena_leto_studenti);
        tw.setText(ceny[2] + " €");

        tw = (TextView) rootView.findViewById(R.id.text_cena_zima_studenti);
        tw.setText(ceny[3] + " €");

        tw = (TextView) rootView.findViewById(R.id.text_cena_leto_dospeli);
        tw.setText(ceny[4] + " €");

        tw = (TextView) rootView.findViewById(R.id.text_cena_zima_dospeli);
        tw.setText(ceny[5] + " €");


    }


}

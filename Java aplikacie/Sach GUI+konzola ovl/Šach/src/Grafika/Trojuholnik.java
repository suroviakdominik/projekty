/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafika;

import java.awt.*;

/**
 * Rovnoramenny trojuholnik, s ktorym mozno pohybovat a nakresli sa na platno.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 1.0 (15 July 2000)
 */
public class Trojuholnik implements IGrafickeObjekty {

    private int aVyska;
    private int aZakladna;
    private int aXLavyHorny; // lavy horny bod oblasti
    private int aYLavyHorny; // lavy horny bod oblasti
    private String aFarba;
    private boolean aJeViditelny;

    /**
     * Vytvor novy rovnoramenny trojuholnik preddefinovanej farby na
     * preddefinovanej pozicii.
     */
    public Trojuholnik(int paVyska, int paZakladna, int paXLavyHornyRoh, int paYPravyHornyRoh, String paFarba) {
        aVyska = paVyska;
        aZakladna = paZakladna;
        aXLavyHorny = paXLavyHornyRoh;
        aYLavyHorny = paYPravyHornyRoh;
        aFarba = paFarba;
        aJeViditelny = false;
    }

    /**
     * (Trujuholnik) Zobraz sa.
     */
    @Override
    public void zobraz() {
        aJeViditelny = true;
        this.nakresli();
    }

    /**
     * (Trujuholnik) Skry sa.
     */
    @Override
    public void skry() {
        this.zmaz();
        aJeViditelny = false;
    }

    /**
     * (Trujuholnik) Posun sa vpravo o pevnu dlzku.
     */
    @Override
    public void posunVpravo() {
        this.posunVodorovne(20);
    }

    /**
     * (Trujuholnik) Posun sa vlavo o pevnu dlzku.
     */
    @Override
    public void posunVlavo() {
        this.posunVodorovne(-20);
    }

    /**
     * (Trujuholnik) Posun sa hore o pevnu dlzku.
     */
    @Override
    public void posunHore() {
        this.posunZvisle(-20);
    }

    /**
     * (Trujuholnik) Posun sa dole o pevnu dlzku.
     */
    @Override
    public void posunDole() {
        this.posunZvisle(20);
    }

    /**
     * (Trujuholnik) Posun sa vodorovne o dlzku danu parametrom.
     */
    @Override
    public void posunVodorovne(int paVzdialenost) {
        this.zmaz();
        aXLavyHorny += paVzdialenost;
        this.nakresli();
    }

    /**
     * (Trujuholnik) Posun sa zvisle o dlzku danu parametrom.
     */
    @Override
    public void posunZvisle(int paVzdialenost) {
        this.zmaz();
        aYLavyHorny += paVzdialenost;
        this.nakresli();
    }

    /**
     * (Trujuholnik) Posun sa pomaly vodorovne o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunVodorovne(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aXLavyHorny += delta;
            this.nakresli();
        }
    }

    /**
     * (Trujuholnik) Posun sa pomaly zvisle o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunZvisle(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aYLavyHorny += delta;
            this.nakresli();
        }
    }

    /**
     * (Trujuholnik) Zmen rozmery vysky a zakladne na hodnoty dane parametrami.
     * Obe hodnoty musia byt nezaporne cele cisla.
     */
    public void zmenRozmery(int paVyska, int paZakladna) {
        this.zmaz();
        aVyska = paVyska;
        aZakladna = paZakladna;
        this.nakresli();
    }

    /**
     * (Trujuholnik) Zmen farbu na hodnotu danu parametrom. Nazov farby musi byt
     * po anglicky. Mozne farby su tieto: cervena - "red" zlta - "yellow" modra
     * - "blue" zelena - "green" fialova - "magenta" cierna - "black" biela -
     * "white" hneda - "brown"
     */
    @Override
    public void zmenFarbu(String paFarba) {
        aFarba = paFarba;
        this.nakresli();
    }

    /*
     * Draw the triangle with current specifications on screen.
     */
    private void nakresli() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            int[] xpoints = {aXLavyHorny, aXLavyHorny + (aZakladna / 2), aXLavyHorny - (aZakladna / 2)};
            int[] ypoints = {aYLavyHorny, aYLavyHorny + aVyska, aYLavyHorny + aVyska};
            canvas.draw(this, aFarba, new Polygon(xpoints, ypoints, 3));

        }
    }

    /*
     * Erase the triangle on screen.
     */
    private void zmaz() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

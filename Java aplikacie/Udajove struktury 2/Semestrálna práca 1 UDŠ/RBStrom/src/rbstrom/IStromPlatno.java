/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom;

import rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public interface IStromPlatno {
    public Vrchol getKoren();
    public int getaPocetPrvkov();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Hra.Hrac;
import wof00.Miestnost.MiestnostLabak;

/**
 *
 * @author suroviak3
 */
public class PredmetNavleky implements IPredmet, Serializable {

    @Override
    public String getNazov() {
        return "navleky";
    }

    @Override
    public String getPopis() {
        return "navleky do labaku";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        if (paHrac.isaMamNaVleky()) {
            if (paHrac.dajPoziciu() instanceof MiestnostLabak) {
                return "V labaku musite pouzivat navleky. Nemozete si ich vyzut";
            }
            paHrac.vyzutNavleky();
            return "Vyzuli ste si navleky";
        } else {
            if (paHrac.maPredmet("navleky")) {
                paHrac.obutSiNavleky();
                return "Obuli ste si navleky";
            }

            return "V inventari nemate navleky.";
        }

    }

}

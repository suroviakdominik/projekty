/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Ine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import šach.hra.Hrac;
import šach.sachovnica.figurky.Dáma;
import šach.sachovnica.figurky.Figurka;
import šach.sachovnica.figurky.Kon;
import šach.sachovnica.figurky.Kral;
import šach.sachovnica.figurky.Kralovna;
import šach.sachovnica.figurky.Pesiak;
import šach.sachovnica.figurky.Veza;

/**
 *
 * @author Dominik
 */
public class PracaSoSuborom {

    private String aNacitaneMenoHracaA;
    private String aNacitaneMenoHracaB;
    private String aNacitanaFarbaFigurokHracaA;
    private String aNacitanaFarbaFigurokHracaB;
    private String aNacitaneJeNaTahuHracA;
    private int aNacitaneHodiny;
    private int aNacitaneMinuty;
    private int aNacitaneSekundy;
    private Figurka[][] aSuborovaMapaFigurok;
    private Scanner aScan;
    private PrintWriter aZapisovac;
    private File aSubor;

    /**
     * Konštruktor príjme ako parameter názov súboru a mapu figúrok/kontajner,
     * kde sú jednotlivé figúrky a ich rozmiestnenia. Parameter názov súboru
     * použije na vytvorenie objektu typu File, ktorý priradí k atribútu aSubor.
     * Paramete mapa figúrok priradí atribútu aSuborovaMapaFigurok.
     *
     * @param paSubor - súbor s ktorým sa pracuje
     * @param paMapaFigurok - rozmiestnenie figúrok v poli [8][8]
     * @throws FileNotFoundException - ak súbor neexistuje odošle správu o
     * zlyhaní klientovi, ktorý ju vyhodnotí
     */
    public PracaSoSuborom(String paSubor, Figurka[][] paMapaFigurok) throws FileNotFoundException {

        aSubor = new File(paSubor);
        aSuborovaMapaFigurok = paMapaFigurok;

    }

    /**
     * – ak bol súbor načítaný vráti meno prvého hráča, inak vráti null.
     *
     * @return aNacitaneMenoHrac
     */
    public String dajMenoHracaA() {
        return aNacitaneMenoHracaA;
    }

    /**
     * – ak bol súbor načítaný vráti meno druhého hráča, inak vráti null.
     *
     * @return aNacitaneMenoHracaB
     */
    public String dajMenoHracaB() {
        return aNacitaneMenoHracaB;
    }

    /**
     * ak bol súbor načítaný vráti farbu figúrok prvého hráča, inak vráti null.
     *
     * @return aNacitanaFarbaFigurokHracaA
     */
    public String dajFarbuFigurokHracaA() {
        return aNacitanaFarbaFigurokHracaA;
    }

    /**
     * ak bol súbor načítaný vráti farbu figúrok druhého hráča, inak vráti null.
     *
     * @return aNacitanaFarbaFigurokHracaB
     */
    public String dajFarbuFigurokHracaB() {
        return aNacitanaFarbaFigurokHracaB;
    }

    /**
     * ak bol súbor načítaný, tak zistí a vráti či je na ťahu hráč A.
     *
     * @return true ak je na ťahu hráč A, inak false
     */
    public boolean dajJeNaTahuHracA() {
        if (aNacitaneJeNaTahuHracA.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * otvorí súbor a načíta jeho obsah, jednotlivé hodnoty priradí do
     * atribútov, ktoré môže použiť iný objekt, napríklad na vytvorenie nového
     * hráča sa dá použiť načítané meno, farba figúrok...
     *
     * @throws Exception -odošle správu o výnimke klientovi, ktorý ju spracuje.
     */
    public void nacitajTextovyDokument() throws Exception {
        aScan = new Scanner(aSubor);
        aNacitaneMenoHracaA = aScan.next();

        aNacitaneMenoHracaB = aScan.next();

        aScan.nextLine();

        aNacitanaFarbaFigurokHracaA = aScan.next();

        aNacitanaFarbaFigurokHracaB = aScan.next();

        aScan.nextLine();
        aNacitaneJeNaTahuHracA = aScan.next();
        aScan.nextLine();
        aNacitaneHodiny = Integer.parseInt(aScan.next());
        aNacitaneMinuty = Integer.parseInt(aScan.next());
        aNacitaneSekundy = Integer.parseInt(aScan.next());

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                ;
                int figurka = aScan.nextInt();
                if (figurka != 0) {
                    if (figurka == 1) {
                        aSuborovaMapaFigurok[i][j] = new Pesiak("red");
                    } else if (figurka == 2) {
                        aSuborovaMapaFigurok[i][j] = new Pesiak("green");
                    } else if (figurka == 3) {
                        aSuborovaMapaFigurok[i][j] = new Kon("red");
                    } else if (figurka == 4) {
                        aSuborovaMapaFigurok[i][j] = new Kon("green");
                    } else if (figurka == 5) {
                        aSuborovaMapaFigurok[i][j] = new Veza("red");
                    } else if (figurka == 6) {
                        aSuborovaMapaFigurok[i][j] = new Veza("green");
                    } else if (figurka == 7) {
                        aSuborovaMapaFigurok[i][j] = new Dáma("red");
                    } else if (figurka == 8) {
                        aSuborovaMapaFigurok[i][j] = new Dáma("green");
                    } else if (figurka == 9) {
                        aSuborovaMapaFigurok[i][j] = new Kral("red");
                    } else if (figurka == 10) {
                        aSuborovaMapaFigurok[i][j] = new Kral("green");
                    } else if (figurka == 11) {
                        aSuborovaMapaFigurok[i][j] = new Kralovna("red");
                    } else if (figurka == 12) {
                        aSuborovaMapaFigurok[i][j] = new Kralovna("green");
                    }

                }

            }

        }
        aScan.close();

    }

    /**
     * otvorí súbor a zapíše do neho súčasný stav hry. Potrebné údaje získa od
     * jednotlivých hráčov, ktoré vstupujú ako parametre.
     *
     * @param paHracA- hráčA/protihráč hráčaB
     * @param paHracB- hráčB/protihráč hráčaA
     * @throws FileNotFoundException- informuje klienta o výnimke
     */
    public void ulozDoSuboru(Hrac paHracA, Hrac paHracB) throws FileNotFoundException {
        aZapisovac = new PrintWriter(aSubor);
        aZapisovac.print(paHracA.getMeno());
        aZapisovac.print(" ");
        aZapisovac.print(paHracB.getMeno());
        aZapisovac.println();
        aZapisovac.print(paHracA.getFarbaFigurok());
        aZapisovac.print(" ");
        aZapisovac.print(paHracB.getFarbaFigurok());
        aZapisovac.println();
        aZapisovac.print(paHracA.isaJeNaTahu());
        aZapisovac.println();
        aZapisovac.print(Casovac.getHodiny());
        aZapisovac.print(" ");
        aZapisovac.print(Casovac.getMinuty());
        aZapisovac.print(" ");
        aZapisovac.print(Casovac.getSekundy());
        aZapisovac.println();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (aSuborovaMapaFigurok[i][j] == null) {
                    aZapisovac.print(0);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Pešiak")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(1);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Pešiak")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(2);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kôň")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(3);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kôň")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(4);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Veza")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(5);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Veza")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(6);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Dáma")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(7);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Dáma")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(8);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kral")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(9);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kral")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(10);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kralovna")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("red"))) {
                    aZapisovac.print(11);
                    aZapisovac.print(" ");
                } else if ((aSuborovaMapaFigurok[i][j].getMeno().equals("Kralovna")) && (aSuborovaMapaFigurok[i][j].getFarba().equals("green"))) {
                    aZapisovac.print(12);
                    aZapisovac.print(" ");
                }

            }
            aZapisovac.println();
        }
        aZapisovac.close();

    }

    /**
     * ak bola načítaná hra tak vráti počet minút, ktoré boli v uloženej hre
     * odohrané.
     *
     * @return aNacitanaMinuta
     */
    public int getaMinuty() {
        return aNacitaneMinuty;
    }

    /**
     * ak bola načítaná hra tak vráti počet sekúnd, ktoré boli v uloženej hre
     * odohrané.
     *
     * @return aNacitaneSekundy
     */
    public int getaSekundy() {
        return aNacitaneSekundy;
    }

    /**
     * ak bola načítaná hra tak vráti počet hodín, ktoré boli v uloženej hre
     * odohrané.
     *
     * @return aNacitaneHodiny
     */
    public int getaHodiny() {
        return aNacitaneHodiny;
    }

}

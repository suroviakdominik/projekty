
import java.text.DecimalFormat;
public class Adresa
{
   private String aUlica;
   private String aMesto;
   private int aPSC;
   private int aCislo;
   
   public Adresa(String paUlica,
                 String paMesto,
                 int paPSC,
                 int paCislo)
   {
       aUlica=paUlica;
       aMesto=paMesto;
       aPSC=paPSC;
       aCislo=paCislo;
   }

   public String getUlica()
   {
       return aUlica;
   }  
   
    public String getMesto()
   {
       return aMesto;
   }  

    public int getPSC()
   {
       return aPSC;
   } 
   
    public int getCislo()
   {
       return aCislo;
   }  
   
   public String toString()
   {
      DecimalFormat df=new DecimalFormat("00,000");
      String vysledok;
      vysledok=" "+aUlica;
      vysledok+=" "+aCislo;
      vysledok+=" "+aMesto.toUpperCase();
      vysledok+=" "+df.format(aPSC);
      return vysledok;
    }
}   
      
      
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nacitajavypissubor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author Hawk
 */
public class Subor {

    private String path = "";
    private File file = null;

    public Subor(String path) {
        this.path = path;
        this.file = new File(this.path);
    }

    public void ReadFile(int paLine, String FilePath) {
        String fileContent = "";
        int nrLine=paLine;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               nrLine++;
               fileContent+=nrLine + " " + str+"\n";
            }
            in.close();
            } catch (IOException e) {}

        try {
        File output = new File(FilePath);
        Writer writer = new BufferedWriter(new FileWriter(output));
        writer.write(fileContent);
        writer.close();
        } catch(IOException ioe) {}

    }

    public String ReadFile(int paLine) {
        String fileContent = "";
        int nrLine=paLine;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               nrLine++;
               fileContent+=nrLine + " " + str+"\n";
            }
            in.close();
            } catch (IOException e) {}

        return fileContent;
    }

        public String ReadFile() {
        String fileContent = "";
        int nrLine=0;

        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               nrLine++;
               fileContent+=nrLine + " " + str+"\n";
            }
            in.close();
            } catch (IOException e) {}

        return fileContent;
    }


    /*public int CountLines() {
        int numLines = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               numLines++;
            }
            in.close();
        } catch (IOException e) {}
        return numLines;
    }

    public int CountChars() {
        int numChars = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               numChars+=str.length();
            }
            in.close();
        } catch (IOException e) {}
        return numChars;
    }

    public void FindWords() {

        String text = this.ReadFile();
        boolean bool = true;
        String slovo = "";
        int i=0;

        while(bool) {

            if((text.charAt(i)!=' ') && (text.charAt(i)!='\n')) {
                slovo+=text.charAt(i);
                i++;
            } else {
                if(!(slovo.contentEquals("")||slovo.contentEquals("\n"))) {
                    // co robime so samotnym slovom
                    System.out.println(">>"+slovo+"<<");
                    //-----------------------------
                }
                i++;
                slovo="";
            }
            if(i==(text.length()-1)) {
                //ak by sme mali v bufferi dake slovo
                System.out.println(">>"+slovo+"<<");
                //-----------------------------------
                bool=false;
            }
        }

    }*/
}

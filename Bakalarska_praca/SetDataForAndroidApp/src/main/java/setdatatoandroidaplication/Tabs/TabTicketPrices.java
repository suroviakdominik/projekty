/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JTextFieldWithHint;
import MyGUIJComponent.JTextAreaWithHint;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import setdatatoandroidaplication.DataTransferer;

/**
 *  V rámci tejo triedy mozno editovat cennik a jeho popis
 * @author Dominik
 */
public class TabTicketPrices extends Tab {

    private JPanel aTabPanelHlavny;
    private GridBagConstraints aGConstraints;
    private JTextAreaWithHint aDodatocnyPopis;
   
    
    /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabTicketPrices(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNazovTabu, paCommandProcessingForTab);
        aDataTransferer = DataTransferer.getInstance();
        JPanel lpPanel = getMiddleMainTabPanel();
        lpPanel.setLayout(new BorderLayout());
        aTabPanelHlavny = new JPanel(new GridBagLayout());

        lpPanel.add(aTabPanelHlavny, BorderLayout.CENTER);
        addButtonForSaveChanges(lpPanel);
        initializeMainTabPanelComponents();
        updateData();
        for(Component prech:aTabPanelHlavny.getComponents())
        {
          prech.setFont(FONT);
        }
    }

    /**
     * inicializuje vsetky vizualne komponenty pre tento tab
     */
    private void initializeMainTabPanelComponents() {
        aGConstraints = new GridBagConstraints();
        aGConstraints.fill = GridBagConstraints.BOTH;
        aGConstraints.gridy = 0;
        aGConstraints.weightx = 0.5;
        aGConstraints.weighty = 0.05;
        addComponentsForChildTicketPrices();
        aGConstraints.gridy = 1;
        addComponentsFoStudentTicketPrices();
        aGConstraints.gridy = 2;
        addComponentsForAdultTicketPrices();
        aGConstraints.gridy = 3;
        aGConstraints.gridwidth = 3;
        aGConstraints.weighty = 0.85;
        addComponentsForTicketPricesDescription();

    }

    /**
     *vytvori komponenty, kde je mozne editaova cennik pre deti
     */
    private void addComponentsForChildTicketPrices() {
        JLabel l = new JLabel("Deti: ");
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        aTabPanelHlavny.add(l, aGConstraints);

        JTextFieldWithHint tf = new JTextFieldWithHint("cena lístka leto");
        tf.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tf, aGConstraints);

        JTextFieldWithHint tfz = new JTextFieldWithHint("cena lístka zima");
        tfz.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tfz, aGConstraints);

    }

    /**
     *vytvori komponenty, kde je mozne editaova cennik pre studentov
     */
    private void addComponentsFoStudentTicketPrices() {
        JLabel l = new JLabel("Študenti: ");
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        aTabPanelHlavny.add(l, aGConstraints);

        JTextFieldWithHint tf = new JTextFieldWithHint("cena lístka leto");
        tf.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tf, aGConstraints);

        JTextFieldWithHint tfz = new JTextFieldWithHint("cena lístka zima");
        tfz.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tfz, aGConstraints);

    }

    /**
     *vytvori komponenty, kde je mozne editaova cennik pre dospelych
     */
    private void addComponentsForAdultTicketPrices() {
        JLabel l = new JLabel("Dospelí: ");
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        aTabPanelHlavny.add(l, aGConstraints);

        JTextFieldWithHint tf = new JTextFieldWithHint("cena lístka leto");
        tf.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tf, aGConstraints);

        JTextFieldWithHint tfz = new JTextFieldWithHint("cena lístka zima");
        tfz.setCheckIfIsTextDouble(true);
        aTabPanelHlavny.add(tfz, aGConstraints);
    }

    /**
     * vytvori komponenty v ramci ktorych je mozne pridat a editovat popis cennika
     */
    private void addComponentsForTicketPricesDescription() {
        aDodatocnyPopis = new JTextAreaWithHint("Dodatočné informácie: ");
        //aTextArea.setColumns(30);
        //Dimension d = new Dimension(300, 300);
        //aTextArea.setPreferredSize(d);
        aDodatocnyPopis.setLineWrap(true);
        aDodatocnyPopis.setFont(FONT);

        JScrollPane lpScrollPaneOfTextArea = new JScrollPane(aDodatocnyPopis);

        lpScrollPaneOfTextArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        Border border = BorderFactory.createLineBorder(Color.BLACK);
        aDodatocnyPopis.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        aTabPanelHlavny.add(lpScrollPaneOfTextArea, aGConstraints);
    }

    /**
     * prida tlacidlo pomocou ktoreho sa daju ulozit zmeny
     * @param paNaKtoryPanel - panel na ktory bude tlacidlo pridane
     */
    private void addButtonForSaveChanges(JPanel paNaKtoryPanel) {
        JButton b = new JButton("Ulož vykonané zmeny");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                action_save_changes();
            }
        });
        paNaKtoryPanel.add(b, BorderLayout.SOUTH);
    }

    
    /**
     * skontroluje ci je cena cisleny udaj
     * @param paTicketPrice
     * @return true - ak je cena cislo
     */
    private boolean action_is_ticket_price_of_number_Format(String paTicketPrice) {
        Integer ret = null;
        try {
            ret = Integer.parseInt(paTicketPrice);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    /**
     * ulozi zmeny vykonane v cenniku
     */
    private void action_save_changes() {
        aDataTransferer.removeAllData();
        String ticket_prices = "";
        String dodatocneInfo = "";
        for (Component prech : aTabPanelHlavny.getComponents()) {
            if (prech instanceof JTextFieldWithHint) {
                JTextFieldWithHint tfwh = (JTextFieldWithHint) prech;
                if (tfwh.getText().equals("") || tfwh.getText().equals(tfwh.getTextHint())) {
                    JOptionPane.showMessageDialog(this,
                            "Info about zoo file can not be updated, becouse  some fields are empty.",
                            "Info about zoo file update error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                ticket_prices += tfwh.getText() + "||";
            }

        }
        String dodPopis="";
        if(!aDodatocnyPopis.getText().equals(aDodatocnyPopis.getDefaultText()))
            dodPopis=aDodatocnyPopis.getText();
        aDataTransferer.addString("TICKET_PRICES", ticket_prices + ";;;"+dodPopis);
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.INFO, PrikazyENUM.UPRAV)) {
            JOptionPane.showMessageDialog(this, "Ticket prices was updated.");
        } else {
            JOptionPane.showMessageDialog(this,
                    "AppERROR: Info file: ticket prices cannot be updated.",
                    "ERROR: Application Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * nacita aktualne data cennika, ktor boli ulozene do vybraneho komponenta
     */
    private void updateData() {
        aDataTransferer.removeAllData();
        aDataTransferer.addString("TICKET_PRICES", "");
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.INFO, PrikazyENUM.DAJDATA)) {
            String ticket_prices[] = aDataTransferer.getString("RET_TICKET_PRICES").split(";;;");
            int pocitadlo = 0;
            int pocitadlo2 = 0;
            String a[] = ticket_prices[0].split("\\|\\|");
            for (String prech : ticket_prices[0].split("\\|\\|")) {
                JTextFieldWithHint tfwh = (JTextFieldWithHint) aTabPanelHlavny.getComponent(pocitadlo + 1);
                tfwh.setText(prech);
                tfwh.setIsTextNeeded(true);
                pocitadlo++;
                pocitadlo2++;
                if (pocitadlo2 == 2) {
                    pocitadlo++;
                    pocitadlo2 = 0;
                }
            }
            if (!ticket_prices[1].equals("")&&!ticket_prices[1].equals("\n")) {
                aDodatocnyPopis.setText(ticket_prices[1]);
                aDodatocnyPopis.setJeUznejakyTextnacitany(true);
                aDodatocnyPopis.setSelectionStart(0);
                aDodatocnyPopis.setSelectionEnd(0);
            }
            else
            {
                aDodatocnyPopis.setJeUznejakyTextnacitany(false);
            }
        }

    }

    @Override
    public void updateAllTabData() {

    }

}

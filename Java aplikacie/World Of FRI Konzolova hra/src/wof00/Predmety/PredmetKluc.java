/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;
import sun.awt.AWTSecurityManager;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class PredmetKluc implements IPredmet, Serializable {

    private String aNazov;
    private String aPopis;
    private Random aGeneratorX;
    private Random aGeneratorY;

    public PredmetKluc(String paNazov, String paPopis) {
        aNazov = paNazov;
        aPopis = paPopis;
        aGeneratorX = new Random(648895554);
        aGeneratorY = new Random(852123544);
    }

    @Override
    public String getNazov() {
        return "kluc";
    }

    @Override
    public String getPopis() {
        return "kluc od IC";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        int odpoved = Integer.MIN_VALUE;

        Scanner scan = new Scanner(System.in);
        int x = aGeneratorX.nextInt(100);
        int y = aGeneratorY.nextInt(100);

        int sucet = x + y;
        System.out.println("Sucet cisel " + x + " a " + y + " je: ");
        System.out.print(">");
        if (scan.hasNextInt()) {
            odpoved = scan.nextInt();
        }

        if (odpoved == sucet) {
            return "Spravne";
        } else {
            return "Nespravne";
        }
    }

}

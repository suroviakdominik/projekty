
/**
 * Trieda vykonáva elementárne stĺpcové operácie na matici
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class ElementarneStlpcoveOperacie
{
  private double [] pomocnePole;
  private PoctySMaticami aPocSMat;
   /**
     * Konštruktor načíta predtým vytvórenú maticu
     */
  public ElementarneStlpcoveOperacie(PoctySMaticami paPocSMat)
  {
     aPocSMat=paPocSMat;
  }
                                                      
    
     /**
      * metóda vymení stĺpce nachádzajúce sa na indexoch i a j
      * @param i stĺpec
      * @param j stĺpec, ktorý sa má vymeniť s i-tym stĺpcom
      */
     public void vymenStlpce(int i,int j)//ity.stlpec za jty.stlpec
     {
         pomocnePole=new double[aPocSMat.dajPocetStlpcov()];
             for (int n=0;n<aPocSMat.dajPocetRiadkov();n++)
             {
                pomocnePole[n]=aPocSMat.zistiHodnotuCopyPola(n,i);
                aPocSMat.vlozHodnotuDoCopyPola(n,i,aPocSMat.zistiHodnotuCopyPola(n,j));
                aPocSMat.vlozHodnotuDoCopyPola(n,j,pomocnePole[n]);
             }
             System.out.println("Vymenil som "+(i+1)+". stlpec za "+(j+1)+". stlpec");
     }
     
    /**
     * metóda vydelí dva stĺpce j/i podľa prvku v stĺpci nachádzajúceho sa na s-tom mieste, výsledok delenie použije pri násobení i-teho stĺpca a pripočítaní k j-temu stĺpcu
     * @param i nasobený stĺpec
     * @param j stĺpec ku ktorému sa pripočíta nácobok i-teho stĺpca
     * @param s riadok ktorý sa bude deliť
     */
     public void pripocitajNasoboItehoStlpcaKJetemuStlpcu(int i,int j,int s)//1.stlpec,2.stlpec
    {
           double vysDel=(aPocSMat.zistiHodnotuCopyPola(s,j)/aPocSMat.zistiHodnotuCopyPola(s,i))*(-1);
           for (int l=0;l<aPocSMat.dajPocetRiadkov();l++)
           {
                        double nasobItyRiadok=aPocSMat.zistiHodnotuCopyPola(l,i)*vysDel;
                        aPocSMat.vlozHodnotuDoCopyPola(l,j,(aPocSMat.zistiHodnotuCopyPola(l,j)+nasobItyRiadok));
           }
           if (vysDel!=0)
           {
              System.out.println("K "+(j+1)+". stlpcu som pripocital "+Zaokruhli.zaokruhluj(vysDel)+"-nasobok "+(i+1)+". stlpca");
           }
    } 
    
    /**
     * metóda pripočíta i-ty stĺpec k j-temu stĺpcu
     * @param i stĺpec
     * @param j stĺpec, ktorý vznikol pripočítaním i-teho stĺpca k predchádzajúcemu j-temu stĺpcu
     */
    public void pripocitajItyStlpecKJetemuStlpcu(int i,int j)
    {
        for (int p=0;p<aPocSMat.dajPocetRiadkov();p++)
        {
           aPocSMat.vlozHodnotuDoCopyPola(p,j,(aPocSMat.zistiHodnotuCopyPola(p,i)+aPocSMat.zistiHodnotuCopyPola(p,j))); 
        }
        System.out.println(+(i+1)+". stlpec som pripocital k "+(j+1)+". stlpcu");
    }   
    
    /**
     * metóda vynásobí paItyStlpec paCinitelom
     * @param paItyStlpec stlpec
     * @param paCinitel činiteľ
     */
    public void vynasobStlpcecKonstantou(int paItyStlpec, int paCinitel)
    {
        for (int j=0;j<aPocSMat.dajPocetStlpcov();j++)
        {
          aPocSMat.vlozHodnotuDoCopyPola(j,paItyStlpec,(paCinitel*aPocSMat.zistiHodnotuCopyPola(j,paItyStlpec))); 
        }
    }
}

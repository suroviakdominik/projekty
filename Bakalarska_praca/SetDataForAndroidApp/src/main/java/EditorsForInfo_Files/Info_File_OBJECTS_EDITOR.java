/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.ScaleImage;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import setdatatoandroidaplication.DataTransferer;

/**
 *   * Editor, ktory edituje OBJEKTY
 * @author Dominik
 */
class Info_File_OBJECTS_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTranferer;

     /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce objekty
     *                      a popis ich dat
     */
    public Info_File_OBJECTS_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aInfoAboutDirsFile = paFilesInfo;
        aDataTranferer = DataTransferer.getInstance();
    }

    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return true- ak sa data podarilo nastavit
     */
    @Override
    public boolean getDataOfInfoFile() {
        Info_About_Item selectedObject = aDataTranferer.getItemInfo("GET_IMAGE_OBJECT");
        if (selectedObject != null) {

            BufferedImage im = getPictureOfSelectedObjectIfExists(selectedObject);
            if (im != null) {
                aDataTranferer.removeAllData();
                aDataTranferer.addImage("RET_IMAGE_OBJECT", im);
                return true;
            } else {
                return false;
            }

        }

        if (aDataTranferer.getItemInfo("GET_GEO_POSITION") != null) {
            Info_About_Item object = aDataTranferer.getItemInfo("GET_GEO_POSITION");
            return getGEOCoordinates(object);
        }

        selectedObject = aDataTranferer.getItemInfo("GET_TEXT_OBJECT");
        if (selectedObject != null) {
            return getDescriptionOfSelectedObject(selectedObject);
        }

        String paSelectedCategory = aDataTranferer.getString("GET_OBJECTS_OF_CHOOSEN_CATEGORY");
        if (paSelectedCategory != null) {
            return getObjectsOfSelectedCategory(paSelectedCategory);
        }
        aDataTranferer.removeAllData();
        return false;
    }

    /**
     * Vytvorenie noveho objektu. Data dostaneme zo zalozky pomocou objektu DataTransferer
     * @return true - objekt sa podarilo vytvorit
     */
    @Override
    public boolean addNewItem() {
        String nazovNovZvier = aDataTranferer.getString("NEW_OBJECT");
        String doKtorejCat = aDataTranferer.getString("CHOOSEN_CATEGORY");
        aDataTranferer.removeAllData();
        if (nazovNovZvier.equals("")) {
            return false;
        }
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).getOriginalName().equals(nazovNovZvier)) {
                return false;
            }
        }
        String relativePath;
        relativePath = aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + InfoSuboryENUM.FILE_SEPARATOR
                + (aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + aInfoAboutDirsFile.getNumberOfNextItem() 
                + InfoSuboryENUM.FILE_SEPARATOR);

        Info_About_Item newInfo = new Info_About_Item(nazovNovZvier, doKtorejCat, relativePath);
        aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + aInfoAboutDirsFile.getNumberOfNextItem(),
                newInfo);
        DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                + InfoSuboryENUM.OBJECTS.getaRelativePathOfDirecotryWhereIAM()
                + InfoSuboryENUM.OBJECTS.getSignOfDir()
                + InfoSuboryENUM.FILE_SEPARATOR 
                + (InfoSuboryENUM.OBJECTS.getSignOfDir() 
                        + aInfoAboutDirsFile.getNumberOfNextItem()));
        dw.vytvorAdresarAkNeexistuje();
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + relativePath
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        fw.createFileIfNonExists();
        newInfo.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.OBJECTS.getServerDIRDomainOfThisFile()
                + relativePath
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        aInfoAboutDirsFile.raiseNumberOfNextNewItem();
        aDataTranferer.addItemInfo("CREATED_OBJECT", newInfo);
        return saveInfoFile();
    }

    /**
     * Zmaze objekt, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer
     * @return true -objekt sa podarilo zmazat
     */
    @Override
    public boolean deleteItem() {
        Info_About_Item lpObjectToDelete = aDataTranferer.getItemInfo("SELECTED_OBJECT_TO_DELETE");
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).equals(lpObjectToDelete)) {
                File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                        + aAktualneUdajeInfoSuboru.get(prech).getRelativePathOfFilesOfurrentItemInDevice());
                try {
                    aAktualneUdajeInfoSuboru.remove(prech);
                    delete(f);
                } catch (IOException ex) {
                    Logger.getLogger(Info_File_OBJECTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                    return false;
                }

                saveInfoFile();
                return true;
            }
        }
        return false;
    }

     /**
     * Upravi data objektu, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -objekt sa podarilo upravit
     */
    @Override
    public boolean editItem() {
        Info_About_Item lpEditedObject = aDataTranferer.getItemInfo("EDITED_OBJECT");
        String path = aDataTranferer.getString("EDIT_PICTURE_PATH");
        String text = aDataTranferer.getString("EDITED_TEXT");

        if (lpEditedObject == null) {
            if (aDataTranferer.getItemInfo("DELETE_PICTURE_OBJECT") != null) {
                lpEditedObject = aDataTranferer.getItemInfo("DELETE_PICTURE_OBJECT");
                lpEditedObject.setDateOfEditOrCreateFile(null);
                saveInfoFile();
                return deletePicture(lpEditedObject);

            }
            return false;
        }
        String relativePath = lpEditedObject.getRelativePathOfFilesOfurrentItemInDevice();
        lpEditedObject.setDateOfEditOrCreateFile(null);
        if (path != null) {
            return editPicture(path, lpEditedObject);
        } else if (text != null) {
            return editDescriptionOfSelectedObject(text, lpEditedObject);
        } else {
            return editGeoCoordinate(lpEditedObject);
        }
    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.OBJECTS.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.OBJECTS.getaRelativePathOfDirecotryWhereIAM());
        sw.saveObjectToFile();
        System.out.println("MyLogFiles: Info_About_Dirs_File in editor: "
                        +this.getClass().getSimpleName()
                        +" has been saved.");
        return true;
    }

    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
    @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }

    private void deleteExistsPictureThatWillBeReplaced(Info_About_Item paDeletedObject, String paDirectoryWithPicture, String paEndOfPictureFile) {
        File f = new File(paDirectoryWithPicture);
        if (f.listFiles().length == 0) {
            return;
        }
        for (File prech : f.listFiles()) {
            if (!prech.isDirectory() && prech.getName().split("\\.")[0].equals(InfoSuboryENUM.NAME_OF_PICTURE_FILE)) {
                paDeletedObject.getDomainsOfFilesInCurrentItem().remove(InfoSuboryENUM.OBJECTS.getServerDIRDomainOfThisFile()
                        + paDeletedObject.getRelativePathOfFilesOfurrentItemInDevice() 
                        + InfoSuboryENUM.NAME_OF_PICTURE_FILE 
                        + "." + paEndOfPictureFile);
                prech.delete();
                return;
            }
        }
    }

    private String getEndOfFile(Path paPath) {
        String fileName = paPath.getFileName().toString();
        try {
            return fileName.split("\\.")[1].toLowerCase();
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }

    }

    private BufferedImage getPictureOfSelectedObjectIfExists(Info_About_Item paSelectedObject) {
        String relativeTargetPath = paSelectedObject.getRelativePathOfFilesOfurrentItemInDevice();
        File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativeTargetPath);
        if (f.listFiles().length == 0) {
            f = null;
        } else {
            for (File prech : f.listFiles()) {
                if (prech.getName().split("\\.").length > 1 && ((prech.getName().split("\\.")[1].toLowerCase()).equals("jpg")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("png")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("gif"))) {
                    f = new File(prech.getAbsolutePath());
                    break;
                } else {
                    f = null;
                }
            }
        }
        BufferedImage myPicture = null;
        if (f != null) {

            try {
                myPicture = ImageIO.read(f);
            } catch (IOException ex) {
                Logger.getLogger(Info_File_OBJECTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return null;
            }
        }
        return myPicture;
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    private boolean getGEOCoordinates(Info_About_Item paWhoObject) {
        aDataTranferer.addString("RET_LATITUDE", paWhoObject.getZemSirkaText());
        aDataTranferer.addString("RET_LONGITUDE", paWhoObject.getZemDlzkaText());
        System.out.println("\nMyLogFiles: Geo coordinate of object: "
                + paWhoObject.getOriginalName() + "\n has been set to data transfer "
                + "object in editor:" + this.getClass().getSimpleName() + "\n");
        return true;

    }

    private boolean getDescriptionOfSelectedObject(Info_About_Item paSelectedObject) {
        String ret = paSelectedObject.getRelativePathOfFilesOfurrentItemInDevice();
        aDataTranferer.removeAllData();
        if (ret != null) {
            FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER + ret + "popis.txt");
            fw.createFileIfNonExists();
            try {
                String textData = fw.readDataFromFile();
                aDataTranferer.addString("RET_TEXT_OBJECT", textData);
            } catch (IOException ex) {
                Logger.getLogger(Info_File_OBJECTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return false;
            }
            System.out.println("MyLogFiles: Description of object: "
                    + paSelectedObject.getOriginalName() + "\n has been set to data transfer object "
                    + "in editor:" + this.getClass().getSimpleName() + "\n");
            return true;
        } else {
            return false;
        }
    }

    private boolean getObjectsOfSelectedCategory(String paSelectedCategoryKey) {
        if (paSelectedCategoryKey != null) {
            aDataTranferer.removeAllData();
            if (aAktualneUdajeInfoSuboru.size() == 0) {
                return false;
            }
            for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
                if (aAktualneUdajeInfoSuboru.get(prech).getParentDirectory().equals(paSelectedCategoryKey)) {
                    aDataTranferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
                }
            }

            System.out.println("\nMyLogFiles: Objects of selected category \n"
                    + "has been set to data transfer object in editor:" + this.getClass().getSimpleName());
            return true;
        }
        aDataTranferer.removeAllData();
        return false;
    }

    private boolean editPicture(String paPathOfNewPicture, Info_About_Item paEditedObject) {
        String relativePath = paEditedObject.getRelativePathOfFilesOfurrentItemInDevice();
        try {

            Path source = FileSystems.getDefault().getPath(paPathOfNewPicture);
            deleteExistsPictureThatWillBeReplaced(paEditedObject, InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativePath, getEndOfFile(source));
            Path target = FileSystems.getDefault().getPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativePath
                    + InfoSuboryENUM.NAME_OF_PICTURE_FILE + "." + getEndOfFile(source));
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            ScaleImage.scale(target.toString(), getEndOfFile(source));

            String domainOfFile = (InfoSuboryENUM.OBJECTS.getServerDIRDomainOfThisFile()
                    + relativePath + InfoSuboryENUM.NAME_OF_PICTURE_FILE + "." + getEndOfFile(source));
            if(!paEditedObject.getDomainsOfFilesInCurrentItem().contains(domainOfFile))
            {
                paEditedObject.getDomainsOfFilesInCurrentItem().add(domainOfFile);
            }

        } catch (IOException ex) {
            Logger.getLogger(Info_File_OBJECTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return false;
        }

        System.out.println("\nMyLogFiles: Picture has been replaced by new picture '\n"
                + "for object: " + paEditedObject.getOriginalName() + " in editor: " + this.getClass().getSimpleName());
        saveInfoFile();
        return true;
    }

    private boolean editDescriptionOfSelectedObject(String paNewText, Info_About_Item paEditedObject) {
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + paEditedObject.getRelativePathOfFilesOfurrentItemInDevice()
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
        try {
            fw.writeDataToFile(paNewText);

        } catch (IOException ex) {
            Logger.getLogger(Info_File_OBJECTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        System.out.println("\nMyLogFiles: Description of object: "
                + paEditedObject.getOriginalName() + "\n has been updated"
                + " in editor: " + this.getClass().getSimpleName());
        saveInfoFile();
        return true;
    }

    private boolean editGeoCoordinate(Info_About_Item paEditedObject) {
        Double lon = aDataTranferer.getDouble("SET_LONGITUDE");
        if (lon != null) {
            paEditedObject.setaZemepisnaDlzka(lon);
            paEditedObject.setSuradniceZemDlzkaText(aDataTranferer.getString("SET_LONGITUDE"));
        }
        Double lat = aDataTranferer.getDouble("SET_LATITUDE");
        if (lat != null) {
            paEditedObject.setaZemepisnaSirka(lat);
            paEditedObject.setSuradnicaZemSirkaText(aDataTranferer.getString("SET_LATITUDE"));
        }
        saveInfoFile();
        System.out.println("\nMyLogFiles: Geo coordinate of object: "
                + paEditedObject.getOriginalName() + "\n has been updated"
                + " in editor: " + this.getClass().getSimpleName());
        return true;
    }

    private boolean deletePicture(Info_About_Item paEditedObject) {
        for (String prech : paEditedObject.getDomainsOfFilesInCurrentItem()) {
            if (prech.contains(InfoSuboryENUM.NAME_OF_PICTURE_FILE)) {
                File f1 = new File(prech);
                File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + paEditedObject.getRelativePathOfFilesOfurrentItemInDevice() + f1.getName());
                f.delete();
                return true;
            }
        }
        return false;
    }
}

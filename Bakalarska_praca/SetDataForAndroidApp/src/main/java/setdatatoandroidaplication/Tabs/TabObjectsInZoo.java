/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JTextFieldWithHint;
import MyGUIJComponent.JTextAreaWithHint;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;
import GeoLocationComponents.JLatLonPanel;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import com.suroviak.dominik.zooAplication.Navigation.BodNaCeste;
import com.suroviak.dominik.zooAplication.Navigation.Cesta;
import com.suroviak.dominik.zooAplication.Navigation.Coordinate;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import setdatatoandroidaplication.DataTransferer;

/**
 *  Slúži na vytváranie a editáciu objektov nachádzajúcich sa v rámci 
 * zoologickej záhrady, medzi ktoré patria: 
 * reštaurácie, bufety, spoločenské zariadenia, toalety...
 * @author Dominik
 */
public class TabObjectsInZoo extends Tab {

    private JComboBox<Info_About_Item> aCategoryList = null;
    private JComboBox<Info_About_Item> aObjectList;
    private JTextAreaWithHint aTextArea = null;
    private JTextField aPathOfChoosenPicture = null;
    private JTextFieldWithHint aNameOfNewCategory = null;
    private JTextFieldWithHint aNameOfNewObject = null;
    private JPanel aZobrazovacObrazkov = null;
    private Info_About_Item aLastSelectedObject = null;
    private JLatLonPanel aSuradniceZemSirka = null;
    private JLatLonPanel aSuradniceZemDlzka = null;
    private JButton aButtonDeletePicture;

    public final int PREFFERED_HIGHT = 30, PREFFERED_WIDTH = 160, MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 20, MARGIN_RIGHT = 20;

     /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabObjectsInZoo(JTabbedPane paTabPane, KeyListener paKeyListener, String paNameOfNewTab, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNameOfNewTab, paCommandProcessingForTab);
        aCommandProcessingForTab = paCommandProcessingForTab;
        initializeMainTabPanelComponents();

    }

    private void initializeMainTabPanelComponents() {
        JPanel strPanel = getMiddleMainTabPanel();
        strPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);

        c.gridy = 0;
        c.weightx = 0.33;
        c.weighty = 0.05;
        addComponentsForObjectCategoryCreate(strPanel, c, aGlobalKeyListener);
        c.gridy = 1;
        c.weightx = 0.33;

        addComponentsForAnimalCreate(strPanel, c, aGlobalKeyListener);

        c.gridy = 2;

        addComponentsForAnimalGEOPositionOnMap(strPanel, c, aGlobalKeyListener);
        c.gridy = 3;
        c.fill = GridBagConstraints.BOTH;
        addComponentsForObjectPictureAdd(strPanel, c, aGlobalKeyListener);
        c.weighty = 0.80;
        c.gridy = 4;
        c.gridx = 0;
        c.gridwidth = 2;
        c.weightx = 0.5;
        addComponentsForObjectDescriptionEdit(strPanel, c, aGlobalKeyListener);
    }

    /**
     * Metóda vytvorí vizuálne komponenty pre vytváranie kategórií objektov
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForObjectCategoryCreate(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {

        try {
            Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
            JLabel label = new JLabel("Vyberte si jednu z exitujúcich kategórií: ");
            label.addKeyListener(paKeyListener);
            label.setFont(FONT);
            label.setPreferredSize(d);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            label.setBorder(BorderFactory.createCompoundBorder(border,
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            paMainTabPanel.add(label, c);

            aCategoryList = new JComboBox<Info_About_Item>();
            aCategoryList.setPreferredSize(d);
            updateCategories();
            aCategoryList.setFont(FONT);
            aCategoryList.setEditable(false);
            aCategoryList.addKeyListener(paKeyListener);
            paMainTabPanel.add(aCategoryList, c);
            aCategoryList.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    if (aTextArea.boliZmenyUlozene()) {
                        updateObjects();
                    } else {
                        doYouWantToSaveDescriptionChange((Info_About_Item) aObjectList.getSelectedItem());
                        updateObjects();
                    }
                }

            });

            aCategoryList.getEditor().getEditorComponent().addKeyListener(paKeyListener);

            aNameOfNewCategory = new JTextFieldWithHint("Zadajte názov novej kategórie a stlačte ENTER: ");
            //textF.setPreferredSize(new Dimension(PREFFERED_WIDTH + 100, PREFFERED_HIGHT));
            aNameOfNewCategory.setFont(FONT);
            aNameOfNewCategory.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, PREFFERED_HIGHT));

            paMainTabPanel.add(aNameOfNewCategory, c);
            aNameOfNewCategory.addKeyListener(paKeyListener);
            aNameOfNewCategory.addKeyListener(new KeyAdapter() {

                @Override
                public void keyPressed(KeyEvent ke) {
                    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                        aSuradniceZemDlzka.restore();
                        aSuradniceZemSirka.restore();
                        aNameOfNewCategory.setIsTextNeeded(true);
                        if (aTextArea.boliZmenyUlozene()) {
                            actionAddNewCategory();
                        } else {
                            doYouWantToSaveDescriptionChange((Info_About_Item) aObjectList.getSelectedItem());
                            actionAddNewCategory();
                        }
                    }
                }

            });

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "TabObjektyVZOOChyba\n metóda:pridajComboBoxVyberKategorie(...)\n" + e.getMessage(), "ERROR=Vypis Kategorie", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda vytvorí vizuálne komponenty pre vytváranie a výber objektov
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalCreate(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        try {
            Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
            JLabel label = new JLabel("Vyberte si jeden z existujúcich objektov :");
            label.setFont(FONT);
            label.setPreferredSize(d);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            label.setBorder(BorderFactory.createCompoundBorder(border,
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            paMainTabPanel.add(label, c);

            aObjectList = new JComboBox<Info_About_Item>();

            updateObjects();
            if (aObjectList.getSelectedItem() != null) {
                aLastSelectedObject = (Info_About_Item) aObjectList.getSelectedItem();
            }
            aObjectList.setEditable(false);
            aObjectList.setPreferredSize(d);
            aObjectList.addKeyListener(paKeyListener);
            aObjectList.setFont(FONT);
            paMainTabPanel.add(aObjectList, c);
            aObjectList.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (aTextArea.boliZmenyUlozene()) {
                        updateGEOCoordinates();
                        updatePicture();
                        updateTextArea();

                    } else {
                        doYouWantToSaveDescriptionChange(aLastSelectedObject);
                        updateGEOCoordinates();
                        updatePicture();
                        updateTextArea();
                    }
                    aPathOfChoosenPicture.setText("");
                    aLastSelectedObject = (Info_About_Item) aObjectList.getSelectedItem();
                }

            });

            aNameOfNewObject = new JTextFieldWithHint("Zadajte názov nového objektu a stlačte ENTER: ");
            aNameOfNewObject.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, PREFFERED_HIGHT));
            aNameOfNewObject.setFont(FONT);
            paMainTabPanel.add(aNameOfNewObject, c);

            aObjectList.getEditor().getEditorComponent().addKeyListener(paKeyListener);
            label.addKeyListener(paKeyListener);
            aNameOfNewObject.addKeyListener(paKeyListener);
            aNameOfNewObject.addKeyListener(new KeyAdapter() {

                @Override
                public void keyPressed(KeyEvent ke) {
                    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                        aNameOfNewObject.setIsTextNeeded(true);
                        if (aTextArea.boliZmenyUlozene()) {
                            actionAddNewObject();
                        } else {
                            doYouWantToSaveDescriptionChange(aLastSelectedObject);
                            actionAddNewObject();
                        }
                    }
                }

            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "TabObjektyVZOOChyba\n metóda:pridajKomponentyNaEditaciuATvorbuObjektov(...)\n" + e.getMessage(), "ERROR=Tvorba GUI komponentov pre objekty", JOptionPane.ERROR_MESSAGE);
        }

    }

   /**
     * Metóda vytvorí vizuálne komponenty pre zadanie súraníc  objektu na mape
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalGEOPositionOnMap(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
        JLabel l = new JLabel("Zemepisná šírka, zemepisná dĺžka: ");
        l.setFont(FONT);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        paMainTabPanel.add(l, c);
        l.setPreferredSize(d);
        aSuradniceZemSirka = new JLatLonPanel(InfoSuboryENUM.MIN_LAT, InfoSuboryENUM.MAX_LAT);
        aSuradniceZemSirka.setPreferredSize(d);
        aSuradniceZemSirka.setFont(FONT);
        paMainTabPanel.add(aSuradniceZemSirka, c);

        aSuradniceZemDlzka = new JLatLonPanel(InfoSuboryENUM.MIN_LON, InfoSuboryENUM.MAX_LON);
        aSuradniceZemDlzka.setPreferredSize(d);
        aSuradniceZemDlzka.setFont(FONT);
        paMainTabPanel.add(aSuradniceZemDlzka, c);

        aSuradniceZemDlzka.addFocusListener(new GEODataFocusSave(aSuradniceZemDlzka));
        aSuradniceZemSirka.addFocusListener(new GEODataFocusSave(aSuradniceZemSirka));

        if (aObjectList.getSelectedItem() == null) {
            aSuradniceZemDlzka.setEnabled(false);
            aSuradniceZemSirka.setEnabled(false);
        }
        updateGEOCoordinates();
    }

    /**
     * Metóda vytvorí vizuálne komponenty pre vloženie obrazku  objektu
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForObjectPictureAdd(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
        JButton bt1 = new JButton("Vyber obrázok");
        bt1.addKeyListener(paKeyListener);
        bt1.setFont(FONT);
        bt1.setPreferredSize(d);
        paMainTabPanel.add(bt1, c);

        aPathOfChoosenPicture = new JTextField();
        aPathOfChoosenPicture.addKeyListener(paKeyListener);
        aPathOfChoosenPicture.setEditable(false);
        aPathOfChoosenPicture.setPreferredSize(d);
        aPathOfChoosenPicture.setFont(FONT);
        paMainTabPanel.add(aPathOfChoosenPicture, c);

        bt1.setSize(100, 100);
        bt1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                actionChangePictureOfSelectedObject();
            }
        });

        c.gridheight = 2;
        aZobrazovacObrazkov = new JPanel(new BorderLayout());
        JScrollPane pane = new JScrollPane(aZobrazovacObrazkov);
        pane.setPreferredSize(null);
        pane.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, WIDTH));
        pane.setBackground(Color.LIGHT_GRAY);
        paMainTabPanel.add(pane, c);
        updatePicture();
        c.gridheight = 1;
    }

     /**
     * Metóda vytvorí vizuálne komponenty pre vloženie popisu objektu
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForObjectDescriptionEdit(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        aTextArea = new JTextAreaWithHint();
        //aTextArea.setColumns(30);
        //Dimension d = new Dimension(300, 300);
        //aTextArea.setPreferredSize(d);
        aTextArea.setLineWrap(true);
        aTextArea.addKeyListener(paKeyListener);
        aTextArea.setFont(FONT);

        JScrollPane lpScrollPaneOfTextArea = new JScrollPane(aTextArea);
        lpScrollPaneOfTextArea.addKeyListener(paKeyListener);
        lpScrollPaneOfTextArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        lpScrollPaneOfTextArea.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        aTextArea.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        paMainTabPanel.add(lpScrollPaneOfTextArea, c);
        updateTextArea();
        c.gridy = 5;
        c.weighty = 0.01;
        
        JButton btSaveText = new JButton("Ulož zmeny vykonané v texte");
        btSaveText.addKeyListener(paKeyListener);
        btSaveText.setFont(FONT);

        paMainTabPanel.add(btSaveText, c);
        btSaveText.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionChangeDescriptionOfSelectedObject((Info_About_Item) aObjectList.getSelectedItem());
            }
        });
        
        //AK BY SOM CHCEL OBRAZKY MAZAT- ODKOENTOVAT
        /*c.gridwidth=1;
        c.gridx=2;
        aButtonDeletePicture=new JButton("Odstráň obrázok");
        aButtonDeletePicture.setFont(FONT);
        aButtonDeletePicture.addKeyListener(paKeyListener);
        aButtonDeletePicture.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                aDataTransferer.addItemInfo("DELETE_PICTURE_OBJECT", (Info_About_Item)aObjectList.getSelectedItem());
                aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.UPRAV);
                updatePicture();
                aDataTransferer.removeAllData();
            }
        });
        strPanel.add(aButtonDeletePicture,c);*/
    }

    /**
     * zobrazi vsetky aktualne vytvorene kategorie objektov
     * naplni ich do combo box kategorii
     */
    private void updateCategories() {
        if (aCategoryList != null) {
            aCategoryList.removeAllItems();
            aDataTransferer.removeAllData();
            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aCategoryList.addItem(prech);

            }
            aDataTransferer.removeAllData();
        }
        //this.getParent().repaint();
    }

    /**
     * zobrazi vsetky aktualne vytvorene zvierata zvolenej kategorie
     * naplni ich do combo box zvierat
     */
    private void updateObjects() {

        if (aCategoryList != null
                && aCategoryList.getSelectedItem() != null && aObjectList != null) {
            if (aObjectList.getSelectedItem() != null) {
                if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                    aSuradniceZemDlzka.setEnabled(true);
                    aSuradniceZemSirka.setEnabled(true);
                }
            } else {
                if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                    aSuradniceZemDlzka.setEnabled(false);
                    aSuradniceZemSirka.setEnabled(false);
                }
            }
            String keyOfSelectedCategory = getKeyOfSelectedCategory();
            DataTransferer.getInstance().addString("GET_OBJECTS_OF_CHOOSEN_CATEGORY", keyOfSelectedCategory);
            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.DAJDATA);

            aObjectList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aObjectList.addItem(prech);
            }
            updatePicture();
            updateTextArea();
            updateGEOCoordinates();
        }

        DataTransferer.getInstance().removeAllData();

        //this.getParent().repaint();
    }

   /**
     * zobrazi suradnice aktualne vytvorene objektu zvolenej kategorie ak su k dispozicii
     * naplni ich do panelu suradnic
     */
    private void updateGEOCoordinates() {
        if (aObjectList != null && aObjectList.getSelectedItem() != null) {
            if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                aSuradniceZemDlzka.setEnabled(true);
                aSuradniceZemSirka.setEnabled(true);
                aDataTransferer.removeAllData();
                aDataTransferer.addItemInfo("GET_GEO_POSITION", (Info_About_Item) aObjectList.getSelectedItem());

                aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.DAJDATA);
                if (aDataTransferer.getString("RET_LATITUDE") != null && aDataTransferer.getString("RET_LONGITUDE") != null) {
                    aSuradniceZemDlzka.setData(aDataTransferer.getString("RET_LONGITUDE"));
                    aSuradniceZemSirka.setData(aDataTransferer.getString("RET_LATITUDE"));
                } else {
                    aSuradniceZemDlzka.restore();
                    aSuradniceZemSirka.restore();
                }
            }

        } else {
            if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                aSuradniceZemDlzka.restore();
                aSuradniceZemSirka.restore();
                aSuradniceZemDlzka.setEnabled(false);
                aSuradniceZemSirka.setEnabled(false);
            }
        }
    }

    /**
     * zobrazi popis vybraneho objektu zvolenej kategorie
     * naplni ho do textoveho pola zobrazujuceho popis
     */
    private void updateTextArea() {
        if (aTextArea != null && aObjectList.getSelectedItem() != null) {
            aTextArea.setDefaultText("Vložte popis vybraného objektu");
            aTextArea.setEditable(true);
            aTextArea.setEnabled(true);
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_TEXT_OBJECT", (Info_About_Item) aObjectList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.DAJDATA)) {
                if (aDataTransferer.getString("RET_TEXT_OBJECT").length() != 0) {
                    aTextArea.setText(aDataTransferer.getString("RET_TEXT_OBJECT"));
                    aTextArea.setJeUznejakyTextnacitany(true);
                    aTextArea.setSelectionStart(0);
                    aTextArea.setSelectionEnd(0);
                } else {
                    aTextArea.setJeUznejakyTextnacitany(false);
                }
                aDataTransferer.removeAllData();
            } else {
                JOptionPane.showMessageDialog(this, "ERROR: Text_data was not load. ", "ERROW: Load text data", JOptionPane.ERROR_MESSAGE);
            }

        } else if (aTextArea != null) {
            aTextArea.setDefaultText("Vyberte objekt pre úpravu");
            aTextArea.setEditable(false);
            aTextArea.setEnabled(false);

        }
        this.getParent().repaint();
    }

    /**
     * 
     * zobrazi obrazok vytvoreneho objektu zvolenej kategorie ak je kdispozicii
     * zobrazi ho na panele zobrazujucom obrazky
     */
    
    private void updatePicture() {
        if (aZobrazovacObrazkov != null && aObjectList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_IMAGE_OBJECT", (Info_About_Item) aObjectList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.DAJDATA)) {
                aZobrazovacObrazkov.removeAll();
                JLabel picLabel = new JLabel(new ImageIcon(aDataTransferer.getImage("RET_IMAGE_OBJECT")));
                aZobrazovacObrazkov.add(picLabel, BorderLayout.CENTER);
            } else {
                aZobrazovacObrazkov.removeAll();
            }
        } else if (aZobrazovacObrazkov != null) {
            aZobrazovacObrazkov.removeAll();
        }
        this.getParent().repaint();
    }

    private String getKeyOfSelectedCategory() {
        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);
        String paKeyOfCategory = null;
        for (String prech : aDataTransferer.getAllInfoAboutItemKeys()) {
            if (((Info_About_Item) aCategoryList.getSelectedItem()).equals(aDataTransferer.getItemInfo(prech))) {
                paKeyOfCategory = prech;
                break;
            }
        }
        if (paKeyOfCategory == null) {
            System.err.println("ERROR MyLogFiles: Get key of selected category return null in class: " + this.getClass().getSimpleName());
        }
        return paKeyOfCategory;
    }

    /**
     * vytvori novu kategoriu objektov
     */
    private void actionAddNewCategory() {

        //nastav data do transfera dat
        aDataTransferer.addString("NEW_CATEGORY", this.aNameOfNewCategory.getText());
        aDataTransferer.addString("OBJECT_CATEGORY", "");
        //vykonaj
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.PRIDAJ)) {
            Info_About_Item createdCategory=aDataTransferer.getItemInfo("CREATED_OBJECT_CATEGORY") ;
            updateCategories();
            if (createdCategory!= null) {
                aCategoryList.setSelectedItem(createdCategory);
            }
            aNameOfNewCategory.setIsTextNeeded(false);
            JOptionPane.showMessageDialog(this, "Category: " + this.aNameOfNewCategory.getText() + " was created.");

        } else {
            JOptionPane.showMessageDialog(this,
                    "Cattegory can not be create, becouse of the text field is empty or category exist already.",
                    "Create category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * vytvori novy objekt vo vybranej kategorii
     */
    private void actionAddNewObject() {
        //nastav data do transfera dat

        String paKeyOfCategory = getKeyOfSelectedCategory();
        //vykonaj
        aDataTransferer.addString("CHOOSEN_CATEGORY", paKeyOfCategory);
        aDataTransferer.addString("NEW_OBJECT", this.aNameOfNewObject.getText());

        if (paKeyOfCategory != null && aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.PRIDAJ)) {
            Info_About_Item createdObject=aDataTransferer.getItemInfo("CREATED_OBJECT");
            updateObjects();
            if (createdObject!= null) {
                aObjectList.setSelectedItem(createdObject);
            }
            aLastSelectedObject = (Info_About_Item) aObjectList.getSelectedItem();
            aNameOfNewObject.setIsTextNeeded(false);
            JOptionPane.showMessageDialog(this, "Object: " + this.aNameOfNewObject.getText() + " was created.");

        } else {
            JOptionPane.showMessageDialog(this,
                    "Object can not be create, becouse of the text field: new Object is empty\n or Object already exists or category is not chosen.",
                    "Object category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * zmeni obrazok vybraneho objektu
     */
    private void actionChangePictureOfSelectedObject() {
        if (aObjectList.getSelectedItem() != null) {
            final JFileChooser chooser = new JFileChooser();

            chooser.setCurrentDirectory(new File(aLastVisitedDir));

            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.showOpenDialog(null);
            if (chooser.getSelectedFile() != null) {
                String path = chooser.getSelectedFile().getAbsolutePath();
                chooser.setCurrentDirectory(new File(path));
                String filename = chooser.getSelectedFile().getName();
                String separatedName[] = filename.split("\\.");
                if (separatedName.length == 2 && (separatedName[1].toLowerCase().equals("jpg"))
                        || (separatedName[1].toLowerCase().equals("png")) || (separatedName[1].toLowerCase().equals("gif"))) {
                    File f = new File(path);
                    long a = f.length() / 1000;
                    if (a < 600) {
                        try {
                            aLastVisitedDir = path;
                            aPathOfChoosenPicture.setText(path);
                            BufferedImage myPicture = ImageIO.read(new File(path));
                            aZobrazovacObrazkov.removeAll();
                            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
                            aZobrazovacObrazkov.add(picLabel, BorderLayout.CENTER);

                            aDataTransferer.addItemInfo("EDITED_OBJECT", (Info_About_Item) aObjectList.getSelectedItem());
                            aDataTransferer.addString("EDIT_PICTURE_PATH", path);
                            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.UPRAV)) {
                                this.getParent().repaint();
                            } else {
                                JOptionPane.showMessageDialog(this, "ERROR(chyba aplikácie): Obrázok sa nepodarilo nahrať.\n"
                                        + " Trieda: TabObjektyVZOO", "ERROR- Chyba aplikácie. ", JOptionPane.ERROR_MESSAGE);
                            }

                        } catch (IOException ex) {
                            Logger.getLogger(TabObjectsInZoo.class.getName()).log(Level.SEVERE, null, ex);
                            ex.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Obrázky môžu mať maximálnu veľkosť 600 kB.\n"
                                + " Vami vybraný obrázok má veľkosť: " + a
                                + " kB", "ERROR- Obrázok zaberá príliš mnoho miesta. ", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(this, "Obrázky môžu byť iba formátu: jpg, png, gif", "ERROR- Zly formát obrázka", JOptionPane.ERROR_MESSAGE);
                }

            }
        } else {
            JOptionPane.showMessageDialog(this, "Nie je zvolený objekt, ktorého obrázok je portrebné zmeniť. Najskôr vyberte objekt.");
        }
        DataTransferer.getInstance().removeAllData();
    }

    /**
     * zmen popis vybraneho zvierata
     * @param paKomu- aktualne vybrane zviera 
     */
    private void actionChangeDescriptionOfSelectedObject(Info_About_Item paKomu) {
        if (aObjectList.getSelectedItem() != null) {
            aDataTransferer.addItemInfo("EDITED_OBJECT", paKomu);
            aDataTransferer.addString("EDITED_TEXT", aTextArea.getText());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.UPRAV)) {
                JOptionPane.showMessageDialog(this, "Text was successfully save.");
            } else {
                JOptionPane.showMessageDialog(this, "ERROR: Text was not save. ", "ERROW: Save text", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Object must be choosed, before you try to save text.",
                    "No object choosen",
                    JOptionPane.WARNING_MESSAGE);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aTextArea.setJeUznejakyTextnacitany(true);
        aDataTransferer.removeAllData();
    }

    /**
     * opyta sa nas ci chceme ulozit zmeny teextu predtym ako budu stratene
     * @param paKomu - objekt, ktoremu sa maju ulozit zmeny
     */
    private void doYouWantToSaveDescriptionChange(Info_About_Item paKomu) {
        if (JOptionPane.showConfirmDialog(this,
                "Zmeny v popise objektu neboli uložené a po prepnutí budú stratené. \nChcete tieto dáta pred vykonaním akcie uložiť?", "Uloženie popisu",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            actionChangeDescriptionOfSelectedObject(paKomu);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aNameOfNewObject.setIsTextNeeded(false);
        aNameOfNewObject.setIsTextNeeded(false);
    }

    /**
     * aktualizuj vsetky data tohto tabu
     */
    @Override
    public void updateAllTabData() {
        Info_About_Item lpSelectedObject = null;
        Info_About_Item lpSelectedCategory = null;
        if (aCategoryList != null && aObjectList != null) {
            lpSelectedObject = (Info_About_Item) aObjectList.getSelectedItem();
            lpSelectedCategory = (Info_About_Item) aCategoryList.getSelectedItem();
        }
        updateCategories();
        updateObjects();
        if (lpSelectedObject != null && lpSelectedCategory != null) {
            aCategoryList.setSelectedItem(lpSelectedCategory);
            aObjectList.setSelectedItem(lpSelectedObject);
            aLastSelectedObject = (Info_About_Item) aObjectList.getSelectedItem();
        }
    }

    /**
     * Trieda, ktora pri zadavanisuradnic zistuje, ci boli vsetky udaje spravne
     * Ak su udaje spravne, ulozi ich a priradi vybranemu zvieratu
     */
    private class GEODataFocusSave extends FocusAdapter{

        private JLatLonPanel aLatLonPanelThatGetEvent;

        private GEODataFocusSave(JLatLonPanel paJLatLonPanel) {
            aLatLonPanelThatGetEvent = paJLatLonPanel;
        }

        @Override
        public void focusLost(FocusEvent fe) {
            if (aSuradniceZemSirka.areSetValidValues() && aSuradniceZemDlzka.areSetValidValues()) {
                if (aSuradniceZemDlzka.areValuesInAreatOfZOO() || aSuradniceZemSirka.areValuesInAreatOfZOO()) {
                    Cesta paOfflinCesta = TabOfflineMapa.initCestaObject();
                    if (paOfflinCesta != null) {
                        String lat[] = aSuradniceZemSirka.getTextRepresentation().split(":");
                        String lon[] = aSuradniceZemDlzka.getTextRepresentation().split(":");
                        Coordinate latCoord = new Coordinate(lat[0], lat[1], lat[2]);
                        Coordinate lonCoord = new Coordinate(lon[0], lon[1], lon[2]);
                        BodNaCeste b = TabOfflineMapa.pridajNovyObjekt(paOfflinCesta, latCoord, lonCoord);
                        if (b != null) {
                            aDataTransferer.removeAllData();
                            aDataTransferer.addItemInfo("EDITED_OBJECT", (Info_About_Item) aObjectList.getSelectedItem());

                            aDataTransferer.addDouble("SET_LONGITUDE", aSuradniceZemDlzka.getPartOfGeoPoint());
                            aDataTransferer.addString("SET_LONGITUDE", aSuradniceZemDlzka.getTextRepresentation());

                            aDataTransferer.addDouble("SET_LATITUDE", aSuradniceZemSirka.getPartOfGeoPoint());
                            aDataTransferer.addString("SET_LATITUDE", aSuradniceZemSirka.getTextRepresentation());

                            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.UPRAV);
                            aDataTransferer.removeAllData();
                            TabOfflineMapa.saveRoadToFile(paOfflinCesta);
                            System.out.println("DEBUG: Objekt bol umietnený na mapu na súradnice: <" + latCoord.getDECRepresentationOfCoordinate() + ", "
                                    + lonCoord.getDECRepresentationOfCoordinate());

                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(TabObjectsInZoo.this, "Objekt sa nepodarilo umiestnit na mapu:\n"
                            + "Zadaná súradnica neodpovedá bodu nachádzajúcemu sa v areáli zoo.\n"
                            + "Zemepisná Šírka rozmedzie: < "
                            +InfoSuboryENUM.MIN_LAT_DEGREE+": "
                            +InfoSuboryENUM.MIN_LAT_MINUTES+":"
                            + InfoSuboryENUM.MIN_LAT_SECONDS+" ; "
                            +InfoSuboryENUM.MAX_LAT_DEGREE+": "+InfoSuboryENUM.MAX_LAT_MINUTES+":"
                            + InfoSuboryENUM.MAX_LAT_SECONDS+" >\n "
                            + "Zemepisná dĺžka rozmedzie: < "
                            +InfoSuboryENUM.MIN_LON_DEGREE
                            +": "+InfoSuboryENUM.MIN_LON_MINUTES+":"
                            + InfoSuboryENUM.MIN_LON_SECONDS+" ; "
                            +InfoSuboryENUM.MAX_LON_DEGREE+": "+InfoSuboryENUM.MAX_LAT_MINUTES+":"
                            + InfoSuboryENUM.MAX_LON_SECONDS+" >",
                            "ERROR: neumiestnený objekt.",
                            JOptionPane.INFORMATION_MESSAGE);
                }

            }
        }

    }

}

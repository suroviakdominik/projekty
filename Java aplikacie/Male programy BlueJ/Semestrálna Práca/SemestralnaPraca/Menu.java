import java.util.ArrayList;
import java.util.Scanner;
/**
 *Trieda zjednocuje jednotlivé triedy a vytvára program v jednom celku
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class Menu
{
    private Scanner scan;
    private Relacie aRelacie;
    private Matica aMatice;

  public Menu()
  {
     scan=new Scanner(System.in);
  }

    /**
     * metóda vypíše do terminálu hlavné menu
     */
    private void vypisHlavneMenu()
    {
        System.out.println("\f");
        System.out.println("________________________________________");
        System.out.println("             HLAVNÉ MENU");
        System.out.println("_________________________________________");
        System.out.println( "VLASTNSTI RELÁCIE A OPERÁCIE S MATICAMI");
        System.out.println("              RELÁCIE-1");
        System.out.println("        OPERÁCIE S MATICAMI-2");
        System.out.println("           UKONČIŤ PROGRAM-0");
        System.out.println("________________________________________");
    }

    /**
     * metóda sa rozhoduje čo vykoná na základe voľby používateľa
     * @param v voľba používateľa
     */
    private boolean volbaHlavneMenu(int v)
    {
        vypisHlavneMenu();
        boolean temp;
        String znak;
        do
        {
            switch(v)
            {
                case 0: System.out.println("Program sa ukončil.");
                return true;
                case 1: spustiMenuRelacie();
                return true;
                case 2: spustiMenuMatice(); 
                return true;
                default:
                return false;
            }
        }
        while (v!=0);

    }

    
    /**
     * metóda vypíše menu Relácií
     */
    private void vypisMenuRelacie()
    {
        System.out.println("\f");
        System.out.println("_________________________________________");
        System.out.println("          VLASTNOSTI  RELÁCIE ");
        System.out.println("     _____________________________");
        System.out.println("      A>B 1, A>=B 2, A<B 3, A<=B 4");
        System.out.println("        A+B%2=0 5, A+B%2!=0 6");
        System.out.println("        (A+B)>C 7, (A+B)>=C 8");
        System.out.println("        (A+B)<C 9, (A+B)<=C 10");
        System.out.println("        |A+B|>C 11; |A+B|>=C 12");
        System.out.println("        |A+B|<C 13, |A+B|<=C 14");
        System.out.println("            A/B 15, B/A 16");
        System.out.println("             |A|=|B| 17");
        System.out.println("     ______________________________");
        System.out.println("        Späť do hlavného menu 0");
        System.out.println("_________________________________________");
    }

    /**
     * meoda na základe voľby používateľa a relácie vytvorenej používateľom vykoná danú voľbu
     * @param znak voľba
     * @param relacia vytvórenárelácia
     */
    private boolean volbaRelacie(int znak,Relacie relacie)
    {

        switch(znak)
        {
            case 0:
            spustiHlavneMenu();
            return false;

            case 1:
            relacie.relaciaAJeVacsieAkoB();
            return true;

            case 2:
            relacie.relaciaAJeVacsieNajviacRovneB();
            return true;

            case 3:
            relacie.relaciaAJeMensieAkoB();
            return true;

            case 4:
            relacie.relaciaAJeMensieNajviacRovneB();
            return true;

            case 5:
            relacie.relaciaAPlusBJeParneCislo();
            return true;

            case 6:
            relacie.relaciaAPlusBJeNeparneCislo();
            return true;

            case 7:
            System.out.println("Zadajte číslo C");
            int c=scan.nextInt();
            relacie.relaciaAPlusBJeVacsieAkoCislo(c);
            return true;

            case 8:
            System.out.println("Zadajte číslo C");
            int n=scan.nextInt();
            relacie.relaciaAPlusBJeVacsieNajviacRovneCislo(n);
            return true;

            case 9:
            System.out.println("Zadajte číslo C");
            int m=scan.nextInt();
            relacie.relaciaAPlusBJeMensieAkoCislo(m);
            return true;

            case 10:
            System.out.println("Zadajte číslo C");
            int o=scan.nextInt();
            relacie.relaciaAPlusBJeMensieNajviacRovneCislo(o);
            return true;

            case 11:
            System.out.println("Zadajte číslo C");
            int p=scan.nextInt();
            relacie.relaciaAbsolutnaHodnotaAPlusBJeVacsieAkoCislo(p);
            return true;

            case 12:
            System.out.println("Zadajte číslo C");
            int q=scan.nextInt();
            relacie.relaciaAbsolutnaHodnotaAPlusBJeVacsieNajviacRovneCislo(q);
            return true;

            case 13:
            System.out.println("Zadajte číslo C");
            int r=scan.nextInt();
            relacie.relaciaAbsolutnaHodnotaAPlusBJeMensieAkoCislo(r);
            return true;

            case 14:
            System.out.println("Zadajte číslo C");
            int s=scan.nextInt();
            relacie.relaciaAbsolutnaHodnotaAPlusBJeMensieNajviacRovneCislo(s);
            return true;

            case 15:
            relacie.relaciaADeliB();
            return true;

            case 16:
            relacie.relaciaBDeliA();
            return true;

            case 17:
            relacie.relaciaAbsolutnaHodnotaAJeRovnaAbsolutnejHodnoteB();
            return true;

            default:System.out.println("Vašu voľbu neviem spracovať. Prosím zopakujte voľbu.");
            return true;
        }

    }

    /**
     * metóda zistí vlastnosti relácie
     * @param relacia vytvórená relácia 
     */
    private void zistiVlastnostiRelacii(Relacie paRelacie)
    {
        VlastnostiRelacie vlastRel=new VlastnostiRelacie(paRelacie);
        boolean[] pravdHod={vlastRel.reflexivnost(),vlastRel.symetrickost(),vlastRel.antisymetrickost(),vlastRel.asymetrickost(),vlastRel.tranzitivnost()};

        for (int i=0;i<pravdHod.length;i++)
        {
            if(pravdHod[i]==true)
            {
                if (i==0)
                {
                    System.out.println("Relácia je reflexívna");
                }

                if(i==1)
                {
                    System.out.println("Relácia je symetrická");
                }

                if(i==2)
                {
                    System.out.println("Relácia je antisymetrická");
                }

                if(i==3)
                {
                    System.out.println("Relácia je asymetrická");
                }

                if(i==4)
                {
                    System.out.println("Relácia je tranzitívna");
                }
            }
        }
        if ((pravdHod[0]==true)&&(pravdHod[1]==true) &&(pravdHod[4]==true))
        {
            System.out.println("Vami zadaná relácia je reláciou ekvivalencie");
        }    

        else if ((pravdHod[0]==true)&&(pravdHod[2]==true) &&(pravdHod[4]==true))
        {
            System.out.println("Vami zadaná relácia je reláciou čiastočného usporiadania");
        }

    }

    /**
     * metóda vypíše menu matíc do terminálu
     */
    private void vypisMenuMatice()
    {
        System.out.println("\f");
        System.out.println("___________________________________________");
        System.out.println("            OPERÁCIE S MATICAMI");
        System.out.println("    ___________________________________");
        System.out.println("              Sčítanie matíc 1");
        System.out.println("              Násobenie matíc 2");
        System.out.println("            Determinant Matice 3");
        System.out.println("     Prevod matice na trojuholníkový tvar 4");
        System.out.println("     ___________________________________");
        System.out.println("          Späť do hlavného menu 0");
        System.out.println("___________________________________________");
    } 

    /**
     * metóda vykoná vo%lbu zadanú používateľom na danej matici
     * @param paVolba voľba používateľa
     * @paMatica vytvorená matica
     */
    private boolean volbaMatice(int paVolba,Matica paMatica)
    {
        PoctySMaticami pocSMat=new PoctySMaticami(paMatica);
        switch(paVolba)
        {
            case 0:
            spustiHlavneMenu();
            return false;

            case 1:
            System.out.println("Druhá matica má automaticky prednastavenú veľkosť na veľkosť prvej matice !!!");
            pocSMat.scitajMatice();
            return true;

            case 2:
            System.out.println("Druhý činiteľ má automaticky rovnaký počet riadkov ako prvý činiteľ stĺpcov !!! Zadajte počet stĺpcov druhého činiteľa.");
            int pocStlp=scan.nextInt();
            pocSMat.nasobenieMatic(pocStlp);
            return true;

            case 3:
            pocSMat.vypocitajDeterminantMatice();
            return true;

            case 4:
            pocSMat.upravMaticuNaTrojuholnikovuMaticu();
            return true;

            default:System.out.println("Vašu voľbu neviem spracovať");
            return true;
        }
    }

    /**
     * spustenie hlavného menu programu
     */
    public void spustiHlavneMenu()
    {
        boolean temp;
        do
        {
            vypisHlavneMenu();
            System.out.println("Prosím vás zadajte voľbu.");
            int v=scan.nextInt();
            temp=volbaHlavneMenu(v);
            if(!temp)
            {
                System.out.println("Vami zadaná voľba nie je korektná. Zopakujte voľbu");
            }
        }
        while(!temp);
    }    

    /**
     * spustenie menu relácií
     */
    private void spustiMenuRelacie()
    {
        boolean temp=false;
        vypisMenuRelacie();
        do
        {
            System.out.println("Prosím zadajte voľbu: ");
            int v=scan.nextInt();
            if ((v!=0)&&(v<=17))
            {
                System.out.print("Zadajte veľkosť dvojrozmerného štvorcoeho poľa pravdivostných hodnôt relácie: ");
                int pocA=scan.nextInt();
                aRelacie=new Relacie(pocA,pocA);
                temp=volbaRelacie(v,aRelacie);
                System.out.println();
                System.out.println("Poľe hodnôt relácie je: ");
                aRelacie.vypisPolaRelacii();
                System.out.println();
                zistiVlastnostiRelacii(aRelacie);
            }
            else if((v==0)||(v>17))
            {
                temp=volbaRelacie(v,aRelacie);
            }
        }
        while(temp);
    }

    /**
     * spustenie menu matíc
     */
    private void spustiMenuMatice()
    {
        boolean temp=false; 
        vypisMenuMatice(); 
        do
        {
            System.out.println("Prosím zadajte voľbu: ");
            int v=scan.nextInt();
            boolean pravdHodnota=false;
            if ((v!=0)&&(v<=4))
            {
                if (aMatice!=null)
                {
                    System.out.println("Ak chcete použiť k výpočtu predchádzajúcu maticu stlačte 1. Ak chcete vytvoriť novú maticu stlačte 2.");
                    int volba=scan.nextInt();
                    switch (volba)
                    {
                        case 1:
                        pravdHodnota=true;
                        break;

                        case 2:
                        pravdHodnota=false;
                        break;
                    }
                }

                if((pravdHodnota==false)||(aMatice==null))
                {
                    System.out.print("Zadajte počet riadkov matice: ");
                    int pocRiadkov=scan.nextInt();
                    System.out.print("Zadajte počet stĺpcov matice: ");
                    int pocStlpcov=scan.nextInt();
                    aMatice=new Matica(pocRiadkov,pocStlpcov);
                    System.out.println("Ak chcete generovať maticu stlačte 1. Ak chcete načítať maticu stlačte 2.");
                    int volba=scan.nextInt();
                    boolean nacitanieMatice;            
                    do
                    {
                        switch(volba)
                        {
                            case 1:
                            System.out.print("Od: ");
                            int od=scan.nextInt();
                            System.out.print("Do: ");
                            int po=scan.nextInt();
                            aMatice.generujNahodnuMaticu(od,po);
                            System.out.println("Vygerovaná matica: ");
                            aMatice. vypisMaticu();
                            nacitanieMatice=true;
                            break;

                            case 2:
                            aMatice.nacitajMaticu();
                            nacitanieMatice=true;
                            break;

                            default:System.out.println("Zopakujte voľbu");
                            nacitanieMatice=false;
                        }
                    }
                    while(!nacitanieMatice);
                }
                temp=volbaMatice(v,aMatice);

            }
            else if((v==0)||(v>4))
            {
                temp=volbaMatice(v,aMatice);
            }
        }
        while(temp);

    
    
    
    
    }
}


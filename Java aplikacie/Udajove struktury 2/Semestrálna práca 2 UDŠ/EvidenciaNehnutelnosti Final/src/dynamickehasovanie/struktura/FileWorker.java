/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.struktura;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class FileWorker {

    private File aFileToPocessing;
    private BufferedReader aFileReader;
    private Reader aReader;

    public FileWorker(File paInputFile) {
        aFileToPocessing = paInputFile;
    }

    /**
     * spusti citanie zo suboru zadaneho v konstruktore
     *
     * @throws UnsupportedEncodingException
     * @throws FileNotFoundException
     */
    public void startRead() throws UnsupportedEncodingException, FileNotFoundException {
        aReader = new InputStreamReader(new FileInputStream(aFileToPocessing), "WINDOWS-1250");
        aFileReader = new BufferedReader(aReader);
    }

    /**
     * ak sme spustili citanie, nacita dalsi riadok
     *
     * @return null- ak uz nema precitat aky riadok nacitany riadok- inak
     * @throws IOException
     */
    public String readNextLine() throws IOException {
        return aFileReader.readLine();
    }

    /**
     * ukonci citanie zo suboru
     */
    public void stopRead() {
        if (aFileReader != null) {
            try {
                aFileReader.close();
                aReader.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * zapise data do suboru
     *
     * @param paFile- subor, kde data zapisat
     * @param paData- data na zapis
     * @param paAppend - ci zapidsovat od zaciatku alebo doplnit za aktualny
     * koniec suboru data
     */
    public static void writeDataToFile(File paFile, String paData, boolean paAppend) {
        Writer w = null;
        try {
            w = new OutputStreamWriter(new FileOutputStream(paFile, paAppend), "WINDOWS-1250");
            BufferedWriter bW = new BufferedWriter(w);
            BufferedReader bR = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(paData.getBytes("WINDOWS-1250")), "WINDOWS-1250"));
            String paLine;
            while ((paLine = bR.readLine()) != null) {
                bW.write(paLine);
                bW.newLine();
            }
            bR.close();
            bW.flush();
            bW.close();
            w.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

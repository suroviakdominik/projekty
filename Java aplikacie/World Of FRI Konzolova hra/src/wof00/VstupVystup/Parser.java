package wof00.VstupVystup;

import wof00.Hra.Hrac;
import java.util.Scanner;

/**
 * Trieda Parser cita vstup zadany hracom do terminaloveho okna a pokusi sa
 * interpretovat ho ako prikaz hry. Kazda sprava dajPrikaz sposobi, ze parser
 * precita jeden riadok z terminaloveho okna a vyberie z neho prve dve slova.
 * Tie dve slova pouzije ako parametre v sprave new triede Prikaz.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 * @author lokalizacia: Lubomir Sadlon, Jan Janech
 * @version 2012.02.21
 */
public class Parser {

    private NazvyPrikazov aPrikazy;  // odkaz na pripustne nazvy prikazov
    private Scanner aCitac;         // zdroj vstupov od hraca
    private String aPrikaz;

    /**
     * Vytvori citac na citanie vstupov z terminaloveho okna.
     */
    public Parser() {
        aPrikazy = new NazvyPrikazov();
        aCitac = new Scanner(System.in);
    }

    /**
     * @return prikaz zadany hracom
     */
    /*public IVykonavac dajVykonavac(String paNazov)
     {
     if(paNazov.equals("pomoc"))
     {
     return new VykonavacPomoc();
     }
     else
     {
     return null;
     }
            
     }*/
    public Prikaz dajPrikaz(Hrac paHrac) {
        String vstupnyRiadok;   // riadok textu napisany hracom
        String prikaz = null;
        String parameter = null;

        if (!paHrac.masZivot()) {
            System.out.println("Váš život je rovný nule. Môžete použiť iba príkazy: profil, pomoc, ukonci");

        }

        System.out.print("> ");     // vyzva pre hraca na zadanie prikazu

        vstupnyRiadok = aCitac.nextLine();

        // najde prve dve slova v riadku 
        Scanner tokenizer = new Scanner(vstupnyRiadok);
        if (tokenizer.hasNext()) {
            prikaz = tokenizer.next();//prve slovo 
            aPrikaz = prikaz;

            if ((aPrikazy.jePrikaz(prikaz)) && (!paHrac.masZivot())) {

                if ((prikaz.equals("profil")) || (prikaz.equals("ukonci")) || (prikaz.equals("pomoc"))) {
                    return new Prikaz(aPrikazy.dajVykonavac(prikaz), prikaz, parameter);
                }
                System.out.println("Váš život je rovný 0. Zadaný príkaz nemôžete použiť.");

            }

            if (tokenizer.hasNext()) {
                parameter = tokenizer.next();      // druhe slovo
                // vsimnite si, ze zbytok textu sa ignoruje
            }
        }

        if ((aPrikazy.jePrikaz(prikaz)) && (paHrac.masZivot())) {
            // vytvori platny prikaz
            return new Prikaz(aPrikazy.dajVykonavac(prikaz), prikaz, parameter);
        } else {
            // vytvori neplatny - "neznamy" prikaz
            return new Prikaz(null, prikaz, parameter);
        }
    }

    public String getaPrikaz() {
        return aPrikaz;
    }
}

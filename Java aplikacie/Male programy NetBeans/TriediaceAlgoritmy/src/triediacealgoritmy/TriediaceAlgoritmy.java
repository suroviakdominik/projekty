/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triediacealgoritmy;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Dominik
 */
public class TriediaceAlgoritmy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Double[] a =new Double[10000];
        
        Random gen=new Random();
        for (int i = 0; i < a.length; i++) {
           a[i]=gen.nextDouble()*1000;
        }
        
        System.out.println(Arrays.asList(a)+"\n");
        
        long startTime=System.nanoTime();
        System.out.println(Sort.javaSort(Arrays.<Double>asList(a))+"\n");
        System.out.println(System.nanoTime()-startTime);
        startTime=System.nanoTime();
        System.out.println(Sort.bubbleSort(Arrays.<Double>asList(a)));
        System.out.println(System.nanoTime()-startTime);
    }
    
}

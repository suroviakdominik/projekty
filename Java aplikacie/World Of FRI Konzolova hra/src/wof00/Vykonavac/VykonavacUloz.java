/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacUloz implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        try {
            zapis(paHrac);
        } catch (IOException ex) {
            Logger.getLogger(VykonavacUloz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void zapis(Hrac paHrac) throws IOException {
        File file = new File("stav.txt");
        PrintWriter zapisovac = new PrintWriter(file);

        zapisovac.println(paHrac.getMeno());
        zapisovac.println(paHrac.getaPenazenka());
        zapisovac.println(paHrac.dajZivot());
        zapisovac.println(paHrac.dajPoziciu().getaNazov());

        zapisovac.close();
    }
}

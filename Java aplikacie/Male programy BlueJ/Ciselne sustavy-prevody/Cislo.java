

public class Cislo
{
     public static int decToBin(int c)
     {
     int z;
     int decad=1;
     int v=0;
     while (c>0)
     {
        z=c%2;
        c=c/2;
        v+=z*decad;
        decad*=10;
     }   
     return v;
    }

    
    public  static int binToDec(int c)
    {
     int binom=1;
     int v=0;
     int z;
     while (c>0)
     {
     z=c%10;
     c/=10;
     v+=z*binom;
     binom*=2;
     }   
     return v;
    }
}    
     
    
Jaguár americký alebo jaguár (Panthera onca) je najväčšia americká mačkovitá šelma. Je to samotársky lovec, živí sa kapybarami, najväčšími žijúcimi hlodavcami, prasatami pekari, ale tiež rybami, malými aligátormi, korytnačkami, domácim zvieratstvom a menšími druhmi kopytníkov.

Po tigrovi a levovi je jaguár tretia najväčšia mačkovitá šelma na svete. Dorastá do dĺžky 100, vo výnimočnom prípade aj 180 centimetrov, k tomu treba prirátať 40 – 70 centimetrov dlhý chvost. V kohútiku býva vysoký až 1 meter. Je oveľa pevnejší a mohutnejší než leopard, iba jeho chvost je o poznanie kratší ako u jeho africko-ázijského príbuzného. Jeho hmotnosť sa pohybuje medzi 70 (u samíc) a 110 kg (samec).

Obvyklá farba je jasno zlatožltá, niekedy prechádza do červena s čiernymi prstencovými škvrnami (rozetami), niekedy obkľučujúcimi malé škvrnky. Tieto škvrny sú oveľa väčšie než má leopard. Ako aj pri leopardovi môžeme dosť často pozorovať u neho melanickú formu. Prejavuje sa celou čiernou kožou, ale pri dobrom osvetlení sú na koži badateľné tmavšie škvrny.

V dažďovom lese žijúce jaguáre sú menšie a najčastejšie s tmavším farebným odtieňom, ako ich príbuzní na otvorených savanách.

Pohlavnú dospelosť dosahuje v troch rokoch, samica po zhruba stodennej brezosti rodí jedno až štyri mláďatá, z ktorých však do dospelosti dorastú maximálne dve.

Hlavnou oblasťou jeho výskytu je amazonský dažďový les. Ale okrem toho sa jaguár vyskytuje v temer celej Južnej a Strednej Amerike, od Mexika až po Argentínu. Začiatkom 20. Storočia sa vyskytoval ešte aj na juhozápade USA, ale vplyvom rozširovania ľudského osídľovania krajiny sa stal zriedkavým a okolo roku 1950 bol celkom vyhubený.

Na život uprednostňuje tropický dažďový les, predsa však občas žije tiež v buši, alebo savane zarastenej stromami, húštinami, alebo trstinou.

Jaguár je samotárske suchozemské zviera. Jeho revír je v závislosti na výskyt potravy veľký asi 25 až 150 km². K inému pohlaviu sa priblíži iba v období párenia.

Kvôli svojej ťažkej telesnej konštrukcii sa nemôže dobre šplhať, ale je dobrým plavcom. Skúmaním pomocou rádiometrie sa zistilo, že jaguáre sú aktívne aj cez deň. Strávia však 40 – 50% dňa nečinnosťou.

Jaguár korisť vysliedi, pomaly sa k nej priblíži, alebo striehne a čaká na mieste.Potom po krátkom behu zabije korisť úderom laby a strhne ju na zem. Jaguáre sú jediné mačkovité šelmy, ktoré zabíjajú prehryznutím hlavy a nie krku.

Ich korisť sa skladá z jeleňov, svíň pekari, tapírov a kapybár. Zvieratá žijúce na stromoch, ako opice a leňochody sa málokedy stávajú ich korisťou. Kde je možné loviť vo vode, uspokojí sa jaguár aj rybou, ale i menším kajmanom. Pre zmenšovanie svojho životného priestoru a postupu človeka a jeho hospodárenia napáda tiež často dobytok. Ak je v tiesni, zaútočí i na človeka. Vedci zistili, že jaguár žerie vlastne všetko, čo môže chytiť. V žalúdkoch mŕtvych jaguárov našli ostatky 85 rôznych druhov zvierat.

Čas párenia jaguára pretrváva celý rok. V severných oblastiach rozšírenia je obmedzená v dobe od konca novembra do konca januára.

Po období brezivosti, ktoré trvá asi sto dní privedie samica najčastejšie v apríli alebo júni na svet až štyri mláďatá. Rodia sa slepé so zreteľne strakatou srsťou. Mladé vychováva predovšetkým samica, ale pri výchove bol pozorovaný aj samec. Po šiestich týždňoch je potomstvo približne také veľké ako domáca mačka a začína nasledovať svojich rodičov na potulkách. Mláďatá opustia matku približne vo veku jedného alebo dvoch rokov.

Vo veku približne troch rokov jaguár pohlavne dospieva. Doba života je v priemere 10 – 12 rokov v divočine a 20 – 22 rokov v zajatí.

Milí priatelia ZOO Bratislava!

Nazbierali sa u Vás doma, či v práci prázdne a čisté PET fľaše aj s uzávermi? Prineste ich do ZOO Bratislava a veľmi potešíte naše ľudoopy. Vyhovujúce sú všetky fľaše s objemom min. 1 liter.
Chovatelia nalejú do PET fliaš vodu, čaj, alebo šťavu z čerstvého ovocia a zeleniny a položia ich do spální, alebo výbehov šimpanzov učenlivých a orangutanov sumatrianskych.
Šimpanzy a orangutany si fľaše samé otvoria, z fľaše napijú a zničia ju, (samec šimpanzov Jimmy používa takéto fľaše aj ako nástroj na hlasné bubnovanie) preto je spotreba plastových fliaš v ZOO Bratislava veľmi vysoká.

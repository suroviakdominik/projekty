/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;
import wof00.VstupVystup.NazvyPrikazov;
import wof00.VstupVystup.Prikaz;

/**
 *
 * @author suroviak3
 */
public class VykonavacPomoc implements IVykonavac {

    public VykonavacPomoc() {

    }

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        System.out.println("Zabludil si. Si sam. Tulas sa po fakulte.");
        System.out.println();
        System.out.println("Mozes pouzit tieto prikazy:");
        NazvyPrikazov.vypisExistPrikazov();
        paHrac.dajPoziciu().vypisInfoMiestnosti();

        return false;
    }

}

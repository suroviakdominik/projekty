/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.controller;

import java.util.Iterator;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import katastersoft.MyComponents.Zalohovac;
import katastersoft.data.KatastralneUzemie;
import katastersoft.data.ListVlastnictva;
import katastersoft.data.Nehnutelnost;
import katastersoft.data.Osoba;
import katastersoft.rbstrom.BVSAlgoritmy;
import katastersoft.rbstrom.RBStrom;
import katastersoft.rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public class Manazer {

    private RBStrom<Integer, KatastralneUzemie> aKatastrePodlaCKU;
    private RBStrom<String, KatastralneUzemie> aKatastrePodlaNazvu;
    private RBStrom<String, Osoba> aOsoby;

    private GeneratorDat aGenerator;

    public Manazer() {
        aKatastrePodlaCKU = new RBStrom<>();
        aKatastrePodlaNazvu = new RBStrom<>();
        aOsoby = new RBStrom<>();
        aGenerator = new GeneratorDat(aKatastrePodlaCKU, aKatastrePodlaNazvu, aOsoby);

    }

    /**
     * prida nove KU
     *
     * @param cisloKU
     * @param nazovKU
     * @return
     */
    public boolean pridajKU(Integer cisloKU, String nazovKU) {
        if (!aKatastrePodlaCKU.obsahujeKluc(cisloKU) && !aKatastrePodlaNazvu.obsahujeKluc(nazovKU)) {
            KatastralneUzemie kU = new KatastralneUzemie(cisloKU, nazovKU);
            aKatastrePodlaCKU.pridaj(kU.getaCisloKU(), kU);
            aKatastrePodlaNazvu.pridaj(kU.getaNazovKU(), kU);
            return true;
        }
        return false;
    }

    /**
     * pridaj LV do zadaneho KU
     *
     * @param paIdentKU
     * @param cisloLV
     * @param paJeZadNazovKU
     * @return
     */
    public boolean pridajLV(String paIdentKU, Integer cisloLV, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        ListVlastnictva novy = new ListVlastnictva(lpKU, cisloLV);
        if (lpKU.pridajNovyLV(novy)) {
            return true;
        }
        return false;
    }

    /**
     * prida nehnutelnost do zadaneho KU na zadany LV
     *
     * @param cisloKU
     * @param cisloLV
     * @param suppisneCislo
     * @param adresa
     * @return
     */
    public boolean pridajNehnutelnost(Integer cisloKU, Integer cisloLV, Integer suppisneCislo, String adresa) {
        if (!aKatastrePodlaCKU.obsahujeKluc(cisloKU)) {
            return false;
        }
        if (!aKatastrePodlaCKU.dajHodnotu(cisloKU).existujeListVlastnictva(cisloLV)) {
            return false;
        }
        Nehnutelnost nova = new Nehnutelnost(suppisneCislo, adresa);
        aKatastrePodlaCKU.dajHodnotu(cisloKU).pridajNehnutelnostNaLV(nova, cisloLV);
        return true;
    }

    /**
     * prida novu osobu
     *
     * @param novaOsoba
     * @return
     */
    public boolean pridajOsobu(Osoba novaOsoba) {
        if (aOsoby.obsahujeKluc(novaOsoba.getaRodneCislo())) {
            return false;
        }
        aOsoby.pridaj(novaOsoba.getaRodneCislo(), novaOsoba);
        return true;
    }

    /**
     * prida noveho majitela, resp. vytvori majetkovy podiel zadanemu majitelovi
     * na zadanom LV
     *
     * @param paIdentKU
     * @param paCLV
     * @param paRC
     * @param paKUZadanyNazvom
     * @return
     */
    public boolean pridajMajitela(String paIdentKU, Integer paCLV, String paRC, boolean paKUZadanyNazvom) {
        KatastralneUzemie lpKU = null;
        if (paKUZadanyNazvom) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        Osoba prid = aOsoby.dajHodnotu(paRC);
        if (prid == null || lpKU == null) {
            return false;
        }
        return lpKU.getListVlastnictva(paCLV).pridajMajetkovyPodiel(prid);
    }

    //---------------------------------VYPISY ---------------------------------------
    /**
     * Vypis vsetkych KU
     *
     * @param paPodlaNazvu
     * @param paTextArea
     */
    public void vypisKU(boolean paPodlaNazvu, JTextArea paTextArea) {
        int pocitadlo = 0;
        paTextArea.setText("Pocet KU: " + aKatastrePodlaCKU.getaPocetPrvkov() + "\n\n");
        List<Vrchol> data = null;
        if (paPodlaNazvu) {
            data = BVSAlgoritmy.getInorder(aKatastrePodlaNazvu.getKoren());
        } else {
            data = BVSAlgoritmy.getInorder(aKatastrePodlaCKU.getKoren());
        }

        for (Vrchol prech : data) {

            SwingUtilities.invokeLater(() -> {
                paTextArea.append(prech.getData().toString() + "\n");
            });

        }
    }

    /**
     * Vypis vsetkych osob
     *
     * @param paTextArea
     */
    public void vypisOsob(JTextArea paTextArea) {
        int pocitadlo = 0;

        paTextArea.setText("Pocet existujucich osob: " + aOsoby.getaPocetPrvkov() + "\n\n");
        List<Vrchol> psOsoby = BVSAlgoritmy.getInorder(aOsoby.getKoren());
        for (Vrchol prech : psOsoby) {
            pocitadlo++;
            Osoba os = (Osoba) prech.getData();

            SwingUtilities.invokeLater(() -> {
                paTextArea.append(os.vypisIbaKontaktneUdaje() + "\n");
            });

        }

    }

    /**
     * Vypis vsetkych nehnutelnosti v zadanom KU
     *
     * @param text
     * @param jTextArea1
     * @param paKatZadNazov
     * @return
     */
    public boolean vypisNehnutelostiVKU(String text, JTextArea jTextArea1, boolean paKatZadNazov) {
        jTextArea1.setText("");
        KatastralneUzemie paKU = null;
        if (paKatZadNazov) {
            if (!aKatastrePodlaNazvu.obsahujeKluc(text)) {
                return false;
            }
            paKU = aKatastrePodlaNazvu.dajHodnotu(text);
        } else {
            if (!aKatastrePodlaCKU.obsahujeKluc(Integer.parseInt(text))) {
                return false;
            }
            paKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(text));
        }
        for (Vrchol prech : paKU.getNehnutelnosti()) {
            String data = prech.getData().toString();
            SwingUtilities.invokeLater(() -> {
                jTextArea1.append(prech.getData().toString());
            });
        }
        return true;
    }

    /**
     * Vypis vsetkych nehnutelnosti na zadanom LV v zadanom KU
     *
     * @param cisloKU
     * @param cisloLV
     * @return
     */
    public String vypisNehnutelnostiLV(Integer cisloKU, Integer cisloLV) {
        String ret = "";
        if (aKatastrePodlaCKU.obsahujeKluc(cisloKU)) {
            ret += aKatastrePodlaCKU.dajHodnotu(cisloKU).vypisZListuVlastnictva(cisloLV);
        }
        return ret;
    }

    /**
     * vypis LV zadaneho KU
     *
     * @param paKU
     * @return
     */
    public String vypisLVKU(String paIdentKU, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        String ret = "";
        for (Vrchol prech : lpKU.getLV()) {
            ret = ret.concat(prech.getData().toString() + "\n");
        }
        return ret;
    }

    /**
     * Vypis informacii o zadanej nehnutelnosti v zadanom KU
     *
     * @param paIdentKU
     * @param cisloNehnutelnosti
     * @param paKUPodlaNazvu
     * @return
     */
    public String vypisKonkretnejNehnutelnostiVKU(String paIdentKU, int cisloNehnutelnosti, boolean paKUPodlaNazvu) {
        String ret = "";
        KatastralneUzemie lpKU = null;
        if (paKUPodlaNazvu) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        ret += lpKU.vypisNehnutelnostZKU(cisloNehnutelnosti);
        ret+="\nList vlastnictva na ktorom "
                + "je nehnutelnost zapisana: \n"+lpKU.getNehnutelnost(cisloNehnutelnosti).getaZapNaLV().toString();
        return ret;
    }

    /**
     * Vypis osob, ktore obyvaju zadanu nehnutelnost v zadanom KU
     *
     * @param paKU
     * @param paSCNehn
     * @param paKUPodlaNazvu- zadaval sa nazov KU a nie cislo - true
     * @return
     */
    public String vypisOsobyNehnutelnosti(String paKU, Integer paSCNehn, boolean paKUPodlaNazvu) {
        if (!existujeKU(paKU, paKUPodlaNazvu)) {
            return "Pri vypise sa zistilo ze KU neexituje";
        }
        if (!existujeNehnutelnost(paKU, paSCNehn, paKUPodlaNazvu)) {
            return "Pri vypise sa zistilo ze nehnutelnost neexituje";
        }
        if (paKUPodlaNazvu) {
            return aKatastrePodlaNazvu.dajHodnotu(paKU).getNehnutelnost(paSCNehn).vypisObyvatelov();
        }
        return aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paKU)).getNehnutelnost(paSCNehn).vypisObyvatelov();
    }

    /**
     * Vypis informacii o zadanom LV v zadanom KU
     *
     * @param paKU
     * @param paCisloLV
     * @param paKUPodlaNazvu
     * @return
     */
    public String vypisLV(String paKU, int paCisloLV, boolean paKUPodlaNazvu) {
        if (!existujeKU(paKU, paKUPodlaNazvu)) {
            return "Pri vypise sa zistilo ze KU neexituje";
        }
        if (!existujeLV(paKU, paCisloLV, paKUPodlaNazvu)) {
            return "Pri vypise sa zistilo ze LV neexituje";
        }
        if (paKUPodlaNazvu) {
            return aKatastrePodlaNazvu.dajHodnotu(paKU).getListVlastnictva(paCisloLV).toString();
        }
        return aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paKU)).getListVlastnictva(paCisloLV).toString();
    }

    /**
     * vypise vsetky LV, na ktorych sa osoba nachadza ak je paIdentKU null-
     * potom vypise vsetky inak vypise tie LV, ktore sa nachadzaju v ktastri zo
     * zadanym paIdent KU
     *
     * @param paRCOS-osoba, ktorej LV sa maju vypisat
     * @param paIdentKU-KU=null=nezalezi na KU, KU!=null-iba tie LV, ktore su v
     * danom KU
     * @param selected-true- bol zadany nazov KU a nie cislo false- bolo zadane
     * cislo a nie nazov KU
     * @return
     */
    public String vypisLVOSobyVKU(String paRCOS, String paIdentKU, boolean selected) {
        String ret = "";
        KatastralneUzemie lpFiltrKU = null;
        if (paIdentKU != null) {
            if (selected) {
                lpFiltrKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
            } else {
                lpFiltrKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
            }
        }
        for (ListVlastnictva prch : aOsoby.dajHodnotu(paRCOS).getLVOsoby()) {
            if (lpFiltrKU != null) {
                if (!prch.getaKU().equals(lpFiltrKU)) {
                    continue;
                }
            }
            ret += prch.toString();
        }
        return ret;
    }

    /**
     * Hlada osobu ak ju najde tak ju vrati, inak vrati null
     *
     * @param paRC
     * @return
     */
    public Osoba hladajOsobu(String paRC) {
        return aOsoby.dajHodnotu(paRC);
    }

    //--------------------------KONTROLA EXISTENCIE---------------------
    /**
     *
     * @param paIdentKU
     * @param vyhlPodlaNazvu
     * @return true - existuje zadane KU
     */
    public boolean existujeKU(String paIdentKU, boolean vyhlPodlaNazvu) {
        if (vyhlPodlaNazvu) {
            return aKatastrePodlaNazvu.obsahujeKluc(paIdentKU);
        }
        return aKatastrePodlaCKU.obsahujeKluc(Integer.parseInt(paIdentKU));
    }

    /**
     *
     * @param paIdentKU
     * @param paCisloLV
     * @param paKUPodlaNazvu
     * @return true - exituje LV zo zadanym cislo v zadanom KU
     */
    public boolean existujeLV(String paIdentKU, Integer paCisloLV, boolean paKUPodlaNazvu) {
        KatastralneUzemie lpKU = null;
        if (paKUPodlaNazvu) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        return lpKU.existujeListVlastnictva(paCisloLV);
    }

    /**
     *
     * @param paIdentKU
     * @param paSupCislo
     * @param paKUPodlaNazvu
     * @return true - existuje nehnutelnost zo zadanym sup. cislom v zad. KU
     */
    public boolean existujeNehnutelnost(String paIdentKU, Integer paSupCislo, boolean paKUPodlaNazvu) {
        KatastralneUzemie lpKU = null;
        if (paKUPodlaNazvu) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        return lpKU.existujeNehnutelnost(paSupCislo);
    }

    /**
     *
     * @param paRodCislo
     * @return true -existuje osoba zo zadanym rod. cislom
     */
    public boolean existujeOsoba(String paRodCislo) {
        return aOsoby.obsahujeKluc(paRodCislo);
    }

    /**
     *
     * @param cisKU
     * @param supisneCislo
     * @param paCKUPodlaNazvu
     * @return Nehnutelnost - nehnutelnost, ktora ma zadane sup. cislo v zadanom
     * KU
     */
    public Nehnutelnost getNehnutelnost(String cisKU, Integer supisneCislo, boolean paCKUPodlaNazvu) {
        KatastralneUzemie lpKU = null;
        if (paCKUPodlaNazvu) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(cisKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(cisKU));
        }
        return lpKU.getNehnutelnost(supisneCislo);
    }

    /**
     * zmeni osobe trvale bydlosko z aktualneho na bydlisko zadane sup. cislom v
     * zad. KU
     *
     * @param paRCOS
     * @param paKU
     * @param paSupCislo
     * @param paJeZadNazovKU
     */
    public void zmenOsobeTrvaleBydlisko(String paRCOS, String paKU, Integer paSupCislo, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU = null;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paKU));
        }
        Osoba os = aOsoby.dajHodnotu(paRCOS);
        if (lpKU.getNehnutelnost(paSupCislo) != null) {
            if (os.getaTrvaleBydlisko() != null) {
                os.getaTrvaleBydlisko().odoberObyvatela(paRCOS);
            }
            os.setaTrvaleBydlisko(lpKU.getNehnutelnost(paSupCislo));
        } else {
            throw new IllegalStateException("Nova nehutelost v zadanom KU neexistuje");
        }
    }

    /**
     * zmena majitela nehnutelností na zadanom LV. Zadany stary majitel sa nahradi novym
     * @param lpRCStary
     * @param lpRCNovy
     * @param paKU
     * @param paSC
     * @param paJeZadNazovKU
     * @return 
     */
    public boolean zmenMajitelaNehnutelnosti(String lpRCStary, String lpRCNovy, String paKU, int paSC, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU = null;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paKU));
        }
        if(aOsoby.dajHodnotu(lpRCNovy)==null){
            return false;
        }
        if (lpKU.getNehnutelnost(paSC).getaZapNaLV().odoberMajetkovyPodiel(lpRCStary)) {
            lpKU.getNehnutelnost(paSC).getaZapNaLV().pridajMajetkovyPodiel(aOsoby.dajHodnotu(lpRCNovy));
            return true;
        }
        return false;
    }

    /**
     * odstrani majetkovy podiel majitelovi zo zadanym RC na zadanom LV v zadanom KU
     * @param paIdentKU
     * @param psCLV
     * @param rodneCislo
     * @param paJeZadNazovKU
     * @return 
     */
    public boolean odstranMajetkovyPodiel(String paIdentKU, int psCLV, String rodneCislo, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU = null;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        return lpKU.getListVlastnictva(psCLV).odoberMajetkovyPodiel(rodneCislo);
    }

    /**
     * Odstrani osobu zo zadanym RC zo vsetkych LV, kde mala majetkovy podiel,
     * nehnutelnosti, kde bola ubytovana a odstrani ju z pomedzi vsetkych exist. osob
     * Uz nebude existovat
     * @param rodneCislo 
     */
    public void odstranOsobu(String rodneCislo) {
        aOsoby.dajHodnotu(rodneCislo).predOdstranenimOsoby();
        aOsoby.odstran(rodneCislo);
    }

    /**
     * Odstranenie zadaneho LV. Vsetky nehnutelnosti sa presunu na druhy zadany LV v zadanom KU
     * Presun mozno uskutocnit iba na LV, ktore sa nachadza v tom istom KU ako mazany LV
     * @param paIdentKU
     * @param cisloOdstLV
     * @param cisloLVPresun
     * @param paJeZadNazovKU 
     */
    public void odstranLV(String paIdentKU, int cisloOdstLV, Integer cisloLVPresun, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        ListVlastnictva odstrLV = lpKU.getListVlastnictva(cisloOdstLV);
        ListVlastnictva presuvamDoLV = lpKU.getListVlastnictva(cisloLVPresun);
        List<Nehnutelnost> presuvaneNehnutelnosti = odstrLV.getNehnutelnosti();
        for (Nehnutelnost next : presuvaneNehnutelnosti) {
            lpKU.odstranExistujucNehnutelnost(next.getaSupisneCislo());
            presuvamDoLV.pridajNehnutelnost(next);
        }
        lpKU.odoberLV(cisloOdstLV);
    }

    /**
     * Odstranenie nehnutelnosti. Obyvatelia, ktory tu boli ubytovany
     * sa stanu "bezdomovcami". 
     * @param paIdentKU
     * @param paCicloLV
     * @param paSupCislo
     * @param paJeZadNazovKU 
     */
    public void odstranNehnutelnost(String paIdentKU, int paCicloLV, int paSupCislo, boolean paJeZadNazovKU) {
        KatastralneUzemie lpKU;
        if (paJeZadNazovKU) {
            lpKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKU);
        } else {
            lpKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKU));
        }
        lpKU.getNehnutelnost(paSupCislo).zrusUbytovanychAkRusisNehnutelnost();
        lpKU.getListVlastnictva(paCicloLV).odstranNehnutelnostZLV(paSupCislo);
    }

    /**
     * Odstranenie KU. Vsetky LV mazaneho KU
     * sa presunu do noveho zadaneho KU
     * @param paIdentOdstrKU
     * @param paIdentKUPresuvamLV
     * @param paJeZadNazovKU 
     */
    public void odstranKU(String paIdentOdstrKU, String paIdentKUPresuvamLV, boolean paJeZadNazovKU) {
        KatastralneUzemie lpOdstrKU;
        KatastralneUzemie lpPresuvamDoKU;
        if (paJeZadNazovKU) {
            lpOdstrKU = aKatastrePodlaNazvu.dajHodnotu(paIdentOdstrKU);
        } else {
            lpOdstrKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentOdstrKU));
        }
        if (paJeZadNazovKU) {
            lpPresuvamDoKU = aKatastrePodlaNazvu.dajHodnotu(paIdentKUPresuvamLV);
        } else {
            lpPresuvamDoKU = aKatastrePodlaCKU.dajHodnotu(Integer.parseInt(paIdentKUPresuvamLV));
        }
        lpPresuvamDoKU.pridajLVRusenehoKU(lpOdstrKU);
        aKatastrePodlaCKU.odstran(lpOdstrKU.getaCisloKU());
        aKatastrePodlaNazvu.odstran(lpOdstrKU.getaNazovKU());
        if (aKatastrePodlaCKU.obsahujeKluc(lpOdstrKU.getaCisloKU()) || aKatastrePodlaNazvu.obsahujeKluc(lpOdstrKU.getaNazovKU())) {
            throw new IllegalStateException("Nepodrilo sa odstranit KU z RB stromov-podla nazvu alebo podla CKU");
        }
    }

    /**
     * Generovanie dat, ktore naplnia aplikaciu
     * @param po
     * @param ku
     * @param lv
     * @param nehn
     * @param maj 
     */
    public void generujData(int po, int ku, int lv, int nehn, int maj) {
        aKatastrePodlaCKU = new RBStrom<>();
        aKatastrePodlaNazvu = new RBStrom<>();
        aOsoby = new RBStrom<>();
        aGenerator = new GeneratorDat(aKatastrePodlaCKU, aKatastrePodlaNazvu, aOsoby);
        aGenerator.generujData(po, ku, lv, nehn, maj);
        aGenerator = null;
    }

    /**
     * VYTVOENIE ZALOHY DAT NACH. SA V APLIKACII
     * ulozenie dat z aplikacie do suboru
     */
    public void ulozDataDoSuboru() {
        Zalohovac z = new Zalohovac(aKatastrePodlaCKU, aKatastrePodlaNazvu, aOsoby);
        z.vytvorZalohu();
    }

    /**
     * OBNOVENIE ZALOHY, ktoru sme v minulosti vytvorili
     */
    public void obnovDataZoSuboru() {
        aKatastrePodlaCKU = new RBStrom<>();
        aKatastrePodlaNazvu = new RBStrom<>();
        aOsoby = new RBStrom<>();
        Zalohovac z = new Zalohovac(aKatastrePodlaCKU, aKatastrePodlaNazvu, aOsoby);
        z.obnovZalohu();
    }
}

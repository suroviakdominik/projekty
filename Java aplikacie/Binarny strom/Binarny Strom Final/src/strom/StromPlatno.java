package strom;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author suroviak3
 */
import java.awt.*;
import javax.swing.*;
import strom.Strom;

/**
 * Trieda Platno predstavuje šablónu, podľa ktorej budú vytvárané objekty, ktoré
 * sa budú používať v GUI ako komponenty na vykreslenie jedntlivých ADŠ
 *
 * @author Dominik
 */
public class StromPlatno extends JComponent {

    private Strom aStrom;        //  atribut problemu

    /**
     * Konštrukto má za úlohu priradiť atribútu aStrom ADŠ, ktorá bude n atoto
     * plátno vykresľovaná.
     *
     * @param paStrom :Strom- ADŠ používajúci toto plátno
     */
    public StromPlatno(Strom paStrom) {
        this.aStrom = paStrom;
    }

    /**
     * Metóda vykresľuje danú ADŠ na plátno do hlavného okna GUI
     *
     * @param g
     */
    public void paintComponent(Graphics g) // MUSI sa volat presne takto
    {
        Graphics2D gg = (Graphics2D) (g);

        //  vykreslenie nasho problemu
        aStrom.zobrazSa(gg, getWidth() * 10 / 100, getHeight() * 5 / 100, getHeight(), getWidth());
        aStrom.znazorni(gg, 80, getWidth() / 2);
        gg.setColor(Color.DARK_GRAY);
        gg.drawString("Dominik Suroviak", getWidth() - 140, getHeight() - 20);
        gg.setColor(Color.BLUE);
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 16));
        gg.drawString("PREHLIADKA", 10, getHeight() - 135);
        gg.drawRect(8, getHeight() - 150, 105, 30);
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));

        aStrom.prehliadka(gg, getHeight() - 100, 1);
        aStrom.prehliadka(gg, getHeight() - 75, 2);
        aStrom.prehliadka(gg, getHeight() - 50, 3);
        aStrom.prehliadka(gg, getHeight() - 25, 4);
    }
}

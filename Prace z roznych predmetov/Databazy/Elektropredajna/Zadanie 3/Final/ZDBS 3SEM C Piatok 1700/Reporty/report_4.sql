set pagesize 400;
set linesize 500;

ttitle left datum center 'Vypis tovarov z celkovymi trzbami';
column datum new_value datum noprint

SELECT nazov_kat,typ.nazov_typu,nazov,cena,sum(VYfakturovanyTovar.mnozstvo), sum(cena),mnozZaKat,cenZaKat,celkoveTrzbyPredajne 
from faktura 
join vyfakturovanyTovar using (cislo_fa,osobne_cislo)
join tovar using(katalogoveCislo)
join PREDAJNA on(Tovar.id_predajne=Predajna.id_predajne)
join kategoria using (id_kategorie)
join Typ using(id_Typu)
join (select id_kategorie,sum(VYfakturovanyTovar.mnozstvo) as mnozZaKat,sum(cena) as cenZaKat from faktura 
              join vyfakturovanyTovar using (cislo_fa,osobne_cislo)
              join tovar using(katalogoveCislo)
              join kategoria using (id_kategorie)
              group by id_kategorie) using(id_kategorie)
join (select id_predajne as id_pred_CT,sum(faktura.CENA_ZA_FAKTURU)as celkoveTrzbyPredajne 
      from predajna join zamestnanec 
      using(id_predajne) 
      join faktura 
      using(osobne_cislo)
      group by id_predajne)
on(id_pred_CT = id_predajne)
where faktura.DATUM_VYSTAVENIA>TO_DATE('&&Od_Datumu','DD.MM.RR')
and faktura.DATUM_VYSTAVENIA<TO_DATE('&&Do_Datumu','DD.MM.RR')
group by(nazov_kat,typ.nazov_typu,nazov,cena,mnozZaKat,cenZaKat,celkoveTrzbyPredajne)
order by nazov_kat;

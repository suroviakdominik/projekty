/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pohyblivaradciarka;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class ParserDat {
    private List<String> aData;
    private ArrayList<Double> aDoubleData;
    private ArrayList<String> aStringData;
    private Double aMax=null;
    private Double aMin=null;

    public ParserDat(List<String> aData) {
        this.aData = aData;
        aDoubleData=new ArrayList<>();
        aStringData=new ArrayList<>();
    }
    
    public void spracuj()
    {
        for(String prech:aData)
        {
                if(prech.matches("\\d+.?\\d*"))
                {
                    Double d=new Double(prech);
                    aDoubleData.add(d);
                }
                else
                {
                     aStringData.add(prech);
                }
                
            
        }
    }
    
    public String getDoubleValues()
    {
        String ret="";
        for(Double prech:aDoubleData)
        {
            ret+=prech+"\n";
        }
        return ret;
    }
    
    public String getStringValues()
    {
        String ret="";
        for(String prech:aStringData)
        {
            ret+=prech+"\n";
        }
        return ret;
    }
    
    public Double vypocitajPriemer()
    {
        if(aDoubleData.size()>=2)
        {
            double max=Double.MIN_VALUE;
            double min=Double.MAX_VALUE;
            double sum=0;
            for(Double prech:aDoubleData)
            {
                sum+=prech;
                if(prech<min)
                {
                    min=prech;
                }
                if(prech>max)
                {
                    max=prech;
                }
            }
            aMax=max;
            aMin=min;
            return sum/aDoubleData.size();
        }
        return null;
    }

    public Double getaMax() {
        if(aMin==null)
            vypocitajPriemer();
        return aMax;
    }

    public Double getaMin() {
        if(aMax==null)
            vypocitajPriemer();
        return aMin;
    }
    
   
    
}

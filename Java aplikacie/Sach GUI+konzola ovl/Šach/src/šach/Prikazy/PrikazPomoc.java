/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import šach.hra.Hrac;

/**
 * Aby bol použitý objekt tejto triedy, musí používateľ zadať príkaz pomoc.
 * Vypisuje základné informácie o hre.
 *
 * @author Dominik
 */
public class PrikazPomoc implements IPrikaz {

    /**
     * vypíše základné informácie o hre: ako sa hra ovláda, aké príkazy možno
     * použiť. Vždy vracia false, pretože hráč nevykonal ešte ťah.
     *
     * @param paHracKtoryZadavaPrikazy
     * @param lpParameter
     * @return vždy false
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {
        System.out.println("Hra šach je určená pre dvoch hráčov.");
        System.out.println("Ovládanie: hráč si vyberie pomocou príkazu vyber +'suradnice(y,x)', figurku ktorú chce použiť- napríklad: vyber 21 ");
        System.out.println("Nasledovne hráč môže použiť príkaz chod +'súradnice(y,x)' alebo príkaz info. Ak hráč zadal príkaz chod, skontroluje sa jeho ťah");
        System.out.println("a ak je platný figúrka sa premiestni. Ak bol použitý príkaz info, vypíšu sa základne informácie o predtým vybranej figurke.");
        System.out.println("Počas hry si môže hráč uložiť hru pomocou príkazu uloz. Uloženú hru môže neskôr znova načítať. Ak chcete ukončiť hru zadajte príkaz ukonči alebo ");
        return false;
    }

}

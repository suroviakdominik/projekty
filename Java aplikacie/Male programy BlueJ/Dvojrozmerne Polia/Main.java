import java.util.Scanner;

public class Main
{
  public static void main(String [] args)
  {
      Scanner scan= new Scanner(System.in);
      System.out.println("Zadajte pocet riadkov pola");
      int pocRiadkov=scan.nextInt();
      System.out.println("Zadajte pocet stlpcov pola");
      int pocStlpcov=scan.nextInt();
      
      DvojrozmernePole pole = new DvojrozmernePole(pocRiadkov,pocStlpcov);
      pole.generateMatrices();
      System.out.println("Originalna matica je:");
      pole.vypis();
      DvojrozmernePole pole2=new DvojrozmernePole(pole);
      
      System.out.println();
      
      System.out.println("Zadajte riadok, ktory chcete vymenit Cislo riadku musi byt mensie alebo rovne "+pocRiadkov);
      int vymenRiadok=scan.nextInt();
      System.out.println("Zadajte stlpec, ktory chcete vymenit. Cislo stlpca musi byt mensie alebo rovne "+pocStlpcov);
      int vymenStlpec=scan.nextInt();
      
      if((vymenRiadok<=pocRiadkov) && (vymenStlpec<=pocStlpcov))
      {
        System.out.println("Zadajte nove cislo. Originalne cislo je: " +pole.aDataS[vymenRiadok-1] [vymenStlpec-1]);
        int noveCislo=scan.nextInt();
        pole.vymenPrvok(vymenRiadok-1,vymenStlpec-1,noveCislo);
        System.out.println("Zmenena matica je:");
        pole.vypis();
      }
      
      else
      {
          System.out.println("Prvok nemozem zmenit, pretoze sa v poli nenachadza !");
      }
      
      System.out.println();
      
      System.out.println("Originalna matica je:");
      pole2.vypis();
      
      System.out.println();
      
      if (pocRiadkov==pocStlpcov)
      {
         System.out.println("Horna Trojuholnikova matica");
         pole.hornaTrojuholnikovaMatica(pole2);
         System.out.println("Dolna Trojuholnikova matica");
         pole.dolnaTrojuholnikovaMatica(pole2);
      }
  }
}
package com.suroviak.dominik.zooAplication.ActivityClass;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.suroviak.dominik.zooAplication.R;


public class SplashScreenAnimationActvity extends Activity {
    private static String KEY_FIRST_RUN = "";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_animation_actvity);


        sharedPreferences = getPreferences(MODE_PRIVATE);

        if (!sharedPreferences.contains("KEY_FIRST_RUN")) {
            KEY_FIRST_RUN = "NO_FIRST_RUN";
            Log.d("First", "First run!");
            Intent i = new Intent(this, FirstRunActivity.class);
            startActivity(i);
        } else {
            Log.d("Second...", "Second run...!");
            Intent i = new Intent(getBaseContext(), ListViewUpdateActivity.class);
            startActivity(i);
        }

        editor = sharedPreferences.edit();
        editor.putString("KEY_FIRST_RUN", KEY_FIRST_RUN);
        editor.commit();
        this.finish();
        //Remove activity

    }


}

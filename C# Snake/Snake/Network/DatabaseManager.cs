﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Snake.Network
{
    class DatabaseManager
    {
        private const String ConnectString =
            "Data Source=sharp.kst.fri.uniza.sk;Initial Catalog=potst_SuroviakDominik556262Db;User ID=potst_SuroviakDominik556262;Password=O8vzhQ7;Connection Timeout=5";

        /// <summary>
        /// skontroluje ci sa mozno pripojit k databaze
        /// </summary>
        /// <returns></returns>
        public static bool checkForDatabaseConnectivity()
        {
            using (SqlConnection con = new SqlConnection(ConnectString))
            {
                try
                {
                    con.Open();
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// prida nove skore do tabulky
        /// </summary>
        /// <param name="paPlayerName"></param>
        /// <param name="paScore"></param>
        public static void addNewSingleplayerScore(string paPlayerName, int paScore)
        {
            using (var ctx = new potst_SuroviakDominik556262DbEntities())
            {
                ctx.SinglePlayerScore.Add(new SinglePlayerScore() { PlayerName = paPlayerName, Score = paScore, Date = DateTime.Now });
                ctx.SaveChanges();
            }
            /*using (SqlConnection con = new SqlConnection(ConnectString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                    "INSERT INTO SinglePlayerScore VALUES(@PlayerName, @Score, CURRENT_TIMESTAMP)", con))
                    {
                        command.Parameters.Add(new SqlParameter("PlayerName", paPlayerName));
                        command.Parameters.Add(new SqlParameter("Score", paScore));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine(@"Count not insert.");
                }
            }*/
        }

        /*
        /// <summary>
        /// prida nove skore do tabulky
        /// </summary>
        /// <param name="paPlayerName"></param>
        /// <param name="paScore"></param>
        public static void addNewSingleplayerScore(string paPlayerName, int paScore)
        {
            using (SqlConnection con = new SqlConnection(ConnectString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                    "INSERT INTO SinglePlayerScore VALUES(@PlayerName, @Score, CURRENT_TIMESTAMP)", con))
                    {
                        command.Parameters.Add(new SqlParameter("PlayerName", paPlayerName));
                        command.Parameters.Add(new SqlParameter("Score", paScore));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine(@"Count not insert.");
                }
            }
        }*/

        /// <summary>
        /// vypise pat najlepsich hracov celkovo
        /// </summary>
        /// <returns></returns>
        public static List<SinglePlayerScore> DisplayBestFiveSinglePlayerScore()
        {
            using (var ctx = new potst_SuroviakDominik556262DbEntities())
            {
                var list = ctx.SinglePlayerScore
                    .OrderByDescending(t => t.Score)
                    .Take(5);
                return list.ToList();
            }
        }

        /// <summary>
        /// vypise 5 najlepsich skore hraca, ktoreho meno dostalo v parametri
        /// </summary>
        /// <param name="paPlayerName"></param>
        /// <returns></returns>
        public static List<SinglePlayerScore> DisplayBestMyBestScore(string paPlayerName)
        {
            using (var ctx = new potst_SuroviakDominik556262DbEntities())
            {
                var list = ctx.SinglePlayerScore
                    .Where(t => t.PlayerName.StartsWith(paPlayerName))
                    .OrderByDescending(t => t.Score)
                    .Take(5);
                return list.ToList();
            }
        }

        /*

        /// <summary>
        /// jedna instancia tejto triedy predstavuje jeden riadok z tabulky score v databaze
        /// </summary>
        public class SinglePlayerScore
        {
            public string PlayerName { get; set; }
            public int PlayerScore { get; set; }
            public DateTime Date { get; set; }
            public override string ToString()
            {
                return PlayerName + "   " + PlayerScore + "   " + Date.Day + "." + Date.Month + "." + Date.Year;
            }
        }*/
    }
}

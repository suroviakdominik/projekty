/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import WorkWithFile.SerializationWorker;



import java.util.HashMap;
import setdatatoandroidaplication.DataTransferer;

/**
 *   * Editor, ktory edituje KATEGORIE ZVIERAT ALABO OBJEKTOV
 * @author Dominik
 */
public class Info_File_CATEGORIES_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTransferer;
     /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce kategorie objektov a zvierat
     *                      a popis ich dat
     */
    public Info_File_CATEGORIES_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aInfoAboutDirsFile = paFilesInfo;
        aDataTransferer=DataTransferer.getInstance();
    }
    
    
    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return
     */
    @Override
    public boolean getDataOfInfoFile() {
        Info_About_Item paCategoriesOfAnimals=aDataTransferer.getItemInfo("GET_ANIMAL_CATEGORIES");
        if(aAktualneUdajeInfoSuboru.size()==0)
        {
            return false;
        }
        
        aDataTransferer.removeAllData();
        System.out.println();
        for (String prech:aAktualneUdajeInfoSuboru.keySet()) {
            if(paCategoriesOfAnimals!=null && prech.contains(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()+InfoSuboryENUM.ANIMALS.getSignOfDir())){
                aDataTransferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
                System.out.println("MyLogFiles: Animal category: "
                        +aAktualneUdajeInfoSuboru.get(prech).getOriginalName()
                        +" was set to data transferer object.");
            }
            else if(paCategoriesOfAnimals==null&&prech.contains(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()+InfoSuboryENUM.OBJECTS.getSignOfDir()))
            {
                 aDataTransferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
                 System.out.println("MyLogFiles: Object category: "
                        +aAktualneUdajeInfoSuboru.get(prech).getOriginalName()
                        +" was set to data transferer object.");
            }
        }
        return true;
    }

     /**
     * Vytvorenie novej kategorie. Data dostaneme zo zalozky pomocou objektu DataTransferer
     * @return true - kategoriu sa podarilo vytvorit
     */
    @Override
    public boolean addNewItem() {
        String nazovNovkat=aDataTransferer.getString("NEW_CATEGORY");
        String kategoriaPreZvierata=aDataTransferer.getString("ANIMALS_CATEGORY");
        aDataTransferer.removeAllData();
        if(nazovNovkat.equals(""))
        {
            return false;
        }
        for(String prech:aAktualneUdajeInfoSuboru.keySet())
        {
            if(aAktualneUdajeInfoSuboru.get(prech).getOriginalName().equals(nazovNovkat))
            {
                return false;
            }
        }
        if(kategoriaPreZvierata!=null){
            Info_About_Item newCat=new Info_About_Item(nazovNovkat, null, null);
            aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    +InfoSuboryENUM.ANIMALS.getSignOfDir()
                    +aInfoAboutDirsFile.getNumberOfNextItem()
                    ,newCat);
                aDataTransferer.addItemInfo("CREATED_ANIMAL_CATEGORY", newCat);
                System.out.println("\nMyLogFiles: Animal category: " + nazovNovkat
                        + "\n has been created in editor"+this.getClass().getSimpleName());
        }
        else
        {
            Info_About_Item newCat=new Info_About_Item(nazovNovkat, null, null);
            aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    +InfoSuboryENUM.OBJECTS.getSignOfDir()
                    +aInfoAboutDirsFile.getNumberOfNextItem() 
                    ,newCat);
            aDataTransferer.addItemInfo("CREATED_OBJECT_CATEGORY", newCat);
            System.out.println("\nMyLogFiles: Object category: " + nazovNovkat
                        + "\n has been created in editor"+this.getClass().getSimpleName());
        }
        aInfoAboutDirsFile.raiseNumberOfNextNewItem();
        return saveInfoFile();
    }

      /**
     * Zmaze kategoriu, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer
     * @return true -kategoriu sa podarilo zmazat
     */
    @Override
    public boolean deleteItem() {
        Info_About_Item lpCatToDel=aDataTransferer.getItemInfo("SELECTED_CATEGORY_TO_DELETE");
        for(String prech:aAktualneUdajeInfoSuboru.keySet())
        {
            if(aAktualneUdajeInfoSuboru.get(prech).equals(lpCatToDel))
            {
                aAktualneUdajeInfoSuboru.remove(prech);
                saveInfoFile();
                System.out.println("\nMyLogFiles: Category: " + lpCatToDel.getOriginalName()
                        + "\n has been deleted in editor"+this.getClass().getSimpleName());
                return true;
            }
        }
        return false;
    }

    /**
     * Upravi data kategorie zvierat alebo objektov, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -kategoriu sa podarilo upravit(zmenit data)
     */
    @Override
    public boolean editItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.CATEGORIES.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER+
                InfoSuboryENUM.CATEGORIES.getaRelativePathOfDirecotryWhereIAM());
        
        System.out.println("MyLogFiles: Info_About_Dirs_File in editor: "
                        +this.getClass().getSimpleName()
                        +" has been saved.");
        sw.saveObjectToFile();
        return true;
    }

    
    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
     @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }
    
    
    
}

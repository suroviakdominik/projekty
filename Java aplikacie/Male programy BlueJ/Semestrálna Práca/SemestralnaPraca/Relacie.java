
 /**
  *Táto trieda vytvóri a vykoná reláciu medzi množinami A a B. Keď si vyberiete metódu začínajucu slovom  "relácia", trieda naplní pole pravdivostnými hodnotami.
  * 
  * @author (Dominik Suroviak) 
  * @version (a version number or a date)
  */
public class Relacie
{
   private PoliaMnozinAHodnotRelacie aPolRel;
   private int aPocetRiadkov;
   private int aPocetStlpcov;
    
   /**
     * Konštruktor pošle správu triede PoliaMnozinAHodnotRelacie aby vytvorila nový objekt: pole množiny A a pole množiny B a pole pravdivostných hodnôt relácie. 
     * Následne vás postupne vyzve aby ste zadali prvky množín.
     */
     public Relacie(int paPocetRiadkov,int paPocetStlpcov)
     {
       aPocetRiadkov=paPocetRiadkov;
       aPocetStlpcov=paPocetStlpcov;
       aPolRel=new PoliaMnozinAHodnotRelacie(aPocetRiadkov,aPocetStlpcov);
       aPolRel.nacitajMnozinuA();
       aPolRel.nacitajMnozinuB();
    }
     
        /**
         * vráti počet prvkov množiny A
         */      
        public int dajPocetPrvkovMnozinyA()
        {
           return aPocetRiadkov;
        }
    
        /**
         * vráti počet prvkov množiny B
         */
        public int dajPocetPrvkovMnozinyB()
        {
           return aPocetStlpcov;
        }
    
        /**
         * zavolá metódu zisti HodnotuDoNRozmernehoPolaRelacie(i,j) v triede PoliaMnozinHodnotARelacie
         * @ param i riadok
         * @ param j stĺpec
         */
        public int zistiHodnotuDoNRozmernehoPolaRelacie(int i,int j)
        {
          return aPolRel.zistiHodnotuDoNRozmernehoPolaRelacie(i,j);
        }
    
        /**
         * vráti prvok množiny A nachádzajúci sa na indexe i pomocou metódy triedy PoliaMnozinHodnotARelacie
         * @param i index poľa
         */
        public int dajPrvokPolaA(int i)
        {
            return aPolRel.dajPrvokPolaA(i);
        } 
   
       /**
        * vráti prvok množiny B nachádzajúci sa na indexe i pomocou metódy triedy PoliaMnozinHodnotARelacie
        * @param i index poľa
        */
       public int dajPrvokPolaB(int i)
       {
          return aPolRel.dajPrvokPolaB(i);
       } 
     
      /**
       * zavolá metódu vypisPoleHodnotRelacie() triedy PoliaMnozinHodnotARelacie pomocou ktorej vypíše pole pravdivostných hodnôt
       */
      public void vypisPolaRelacii()
      {
          aPolRel.vypisPoleHodnotRelacie();
      }    
    
      
      public  void relaciaAJeVacsieAkoB()
       {
        
          for ( int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if (aPolRel.dajPrvokPolaA(i)>aPolRel.dajPrvokPolaB(j))
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      }
       
       public  void relaciaAJeVacsieNajviacRovneB()
       {
          
           for ( int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if (aPolRel.dajPrvokPolaA(i)>=aPolRel.dajPrvokPolaB(j))
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
       }
       
       public  void relaciaAJeMensieAkoB()
       {
          
          for ( int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if (aPolRel.dajPrvokPolaA(i)<aPolRel.dajPrvokPolaB(j))
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          }
       }
       
       public  void relaciaAJeMensieNajviacRovneB()
       {
       
          for ( int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if (aPolRel.dajPrvokPolaA(i)<=aPolRel.dajPrvokPolaB(j))
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
       }
       
       public void relaciaAPlusBJeParneCislo()
       {
        
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
              for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
              {
                  if((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))%2==0)
                  {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);  
                  }   
                  
                  else
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                  }
              }
          } 
       }
       
       public void relaciaAPlusBJeNeparneCislo()
       {
        
          
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
              for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
              {
                  if((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))%2!=0)
                  {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);  
                  }   
                  
                  else
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                  }
              }
          } 
       }
       
       public void relaciaAPlusBJeVacsieAkoCislo(int paCislo)
       {
         
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if ((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))>paCislo)
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
      public void relaciaAPlusBJeVacsieNajviacRovneCislo(int paCislo)
       {
          
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if ((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))>=paCislo)
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
      public void relaciaAPlusBJeMensieAkoCislo(int paCislo)
       {
         
         for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if ((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))<paCislo)
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
      public void relaciaAPlusBJeMensieNajviacRovneCislo(int paCislo)
       {
          
          
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                if ((aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j))<=paCislo)
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
      public void relaciaAbsolutnaHodnotaAPlusBJeVacsieNajviacRovneCislo(int paCislo)
       {
          
          
           for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                int sucetPrvkovMnozinAPlusB=aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j);
                if (sucetPrvkovMnozinAPlusB<0)
                    {
                        sucetPrvkovMnozinAPlusB=sucetPrvkovMnozinAPlusB*(-1);
                    }
                    
                if (sucetPrvkovMnozinAPlusB>=paCislo)
                {
                    aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          }
      }
      
      public void relaciaAbsolutnaHodnotaAPlusBJeVacsieAkoCislo(int paCislo)
       {
          
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                int sucetPrvkovMnozinAPlusB=aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j);
                if (sucetPrvkovMnozinAPlusB<0)
                    {
                        sucetPrvkovMnozinAPlusB=sucetPrvkovMnozinAPlusB*(-1);
                    }
                    
                if (sucetPrvkovMnozinAPlusB>paCislo)
                {
                    aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
       public void relaciaAbsolutnaHodnotaAPlusBJeMensieNajviacRovneCislo(int paCislo)
       {
         
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                int sucetPrvkovMnozinAPlusB=aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j);
                if (sucetPrvkovMnozinAPlusB<0)
                    {
                        sucetPrvkovMnozinAPlusB=sucetPrvkovMnozinAPlusB*(-1);
                    }
                    
                if (sucetPrvkovMnozinAPlusB<=paCislo)
                {
                    aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      }
      
      public void relaciaAbsolutnaHodnotaAPlusBJeMensieAkoCislo(int paCislo)
       {
         
          for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            {
                int sucetPrvkovMnozinAPlusB=aPolRel.dajPrvokPolaA(i)+aPolRel.dajPrvokPolaB(j);
                if (sucetPrvkovMnozinAPlusB<0)
                    {
                        sucetPrvkovMnozinAPlusB=sucetPrvkovMnozinAPlusB*(-1);
                    }
                    
                if (sucetPrvkovMnozinAPlusB<paCislo)
                {
                    aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                }

                else 
                {
                     aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0);
                }
            }
          } 
      } 
      
      public void relaciaADeliB()
      {
         
         for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            { 
                if ((aPolRel.dajPrvokPolaB(j)%aPolRel.dajPrvokPolaA(i))==0)
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                  }
                  
                else
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0); 
                  }
            }
          }
      }   
      
      public void relaciaBDeliA()
      {
         
         for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            { 
                if ((aPolRel.dajPrvokPolaA(i)%aPolRel.dajPrvokPolaB(j))==0)
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                  }
                  
                else
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0); 
                  }
            }
          }
      }
      
      public void relaciaAbsolutnaHodnotaAJeRovnaAbsolutnejHodnoteB()
      {
         
         for (int i=0;i<dajPocetPrvkovMnozinyA();i++)
          {
            for (int j=0;j<dajPocetPrvkovMnozinyB();j++)
            { 
                int prvkoMnozinyA=aPolRel.dajPrvokPolaA(i);
                if (prvkoMnozinyA<0)
                {
                    prvkoMnozinyA=prvkoMnozinyA*(-1);
                }
                int prvkoMnozinyB=aPolRel.dajPrvokPolaB(j);
                if (prvkoMnozinyB<0)
                {
                    prvkoMnozinyB=prvkoMnozinyB*(-1);
                }
                
                if (prvkoMnozinyA==prvkoMnozinyB)
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,1);
                  }
                  
                else
                  {
                      aPolRel.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,0); 
                  }
            }
          }
      }
}
      
      
    
       
       
       
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
     
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyosjavp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {
    public static String readDataFromFile(File f, String paOddelovacRiadkova) throws Exception
    {
        Reader r=null;
        String ret="";
        try {
            r = new InputStreamReader(new FileInputStream(f),"Windows-1250");
            BufferedReader bReader =new BufferedReader(r);
            String loadedLine=null;
            while((loadedLine=bReader.readLine())!=null)
            {
                ret+=loadedLine+paOddelovacRiadkova;
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                r.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ret;
    }
}

set pagesize 60;
set linesize 123;



ttitle left datum center 'Vypis trzieb za kazdy mesiac pre predajnu';
column datum new_value datum noprint


SELECT max(faktura.cena_za_fakturu), predajna.nazov_predajne,substr(faktura.datum_vystavenia,4,2) as mesiac
from tovar,vyfakturovanytovar,kategoria, faktura,Predajna,Zamestnanec
where tovar.KATALOGOVECISLo=vyfakturovanytovar.KATALOGOVECISLO
and tovar.id_kategorie=kategoria.ID_KATEGORIE
and faktura.cislo_fa = vyfakturovanytovar.CISLO_FA
and Predajna.id_predajne = Zamestnanec.id_predajne
AND   Zamestnanec.osobne_cislo = Faktura.osobne_cislo
and kategoria.ID_KATEGORIE='&ZadanaKategoria'
and TO_NUMBER(TO_CHAR(faktura.datum_vystavenia,'YYYY')) = TO_Number('&ZadanyRok')
group by predajna.nazov_predajne,faktura.cena_za_fakturu,faktura.datum_vystavenia

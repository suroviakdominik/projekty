/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meteheuristikasimulatedannealing;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class Solver {

    private List<Integer> aUsedCities;
    private List<Integer> aNotUsedCities;
    private DataLoader aDataLoader;

    public Solver(DataLoader paData) {
        aUsedCities = new LinkedList<>();
        aNotUsedCities = new LinkedList<>();
        aDataLoader = paData;
        for (int i = 0; i < paData.getCountOfAllCities(); i++) {
            aNotUsedCities.add(i);
        }
    }

    //--------------------------INICIALIZACIA POCIATOCNEHO RIESENIA--------------------------------------
    //algoritmus zvacsovania o najvyhodnejsi uzol
    public String initStartSolutionWithDualHeuristic() {
        //inicializacia pripustneho riesenia podla zadania
        System.out.println(aDataLoader.vypisMatVzd());
        addToUsedCities(0, 0);
        int cityI2 = aDataLoader.getMinDistanceCityIndexFromCityIndex(0, aUsedCities);
        addToUsedCities(cityI2, 1);
        int cityI3 = aDataLoader.getMinDistanceCityIndexFromCityIndex(cityI2, aUsedCities);
        addToUsedCities(cityI3, 2);

        System.out.println(vypisPouzite());
        System.out.println(vypisNePouzite());
        while (aNotUsedCities.size() > 0) {
            findCityThatWillBePutToUsedCitiesAndPutIT();
            System.out.println(vypisPouzite());
            System.out.println(vypisNePouzite());
        }
        return vypisPouzite();
    }

    private void addToUsedCities(int paCityIndex, int paWhere) {
        aNotUsedCities.remove(aNotUsedCities.indexOf(paCityIndex));
        aUsedCities.add(paWhere, paCityIndex);
    }

    private void findCityThatWillBePutToUsedCitiesAndPutIT() {
        int indexKde = -1;
        int indexNasledujuciPoKde = -1;
        int najlepsiNajdenyIndexNaKtoryVlozit = -1;
        int najlepsiIndexPrvkuKtoryVlozit = -1;
        double minimZhorsenieVzd = Double.MAX_VALUE;
        System.out.println("***************************Vloz nove mesto do aktualneho retazca***********************************************");
        for (int i = 0; i < aNotUsedCities.size(); i++) {
            System.out.println("-------------SKUSAM VLOZIT: " + aDataLoader.getNameOfCity(aNotUsedCities.get(i)));
            for (int j = 0; j < aUsedCities.size(); j++) {
                indexKde = j;
                indexNasledujuciPoKde = (j + 1 == aUsedCities.size()) ? 0 : j + 1;
                double zhorsenie = aDataLoader.getDistance(aUsedCities.get(indexKde), aNotUsedCities.get(i))
                        + aDataLoader.getDistance(aNotUsedCities.get(i), aUsedCities.get(indexNasledujuciPoKde))
                        - aDataLoader.getDistance(aUsedCities.get(indexKde), aUsedCities.get(indexNasledujuciPoKde));
                if (zhorsenie < minimZhorsenieVzd) {
                    minimZhorsenieVzd = zhorsenie;
                    najlepsiNajdenyIndexNaKtoryVlozit = indexNasledujuciPoKde;
                    najlepsiIndexPrvkuKtoryVlozit = i;
                }
                System.out.println("Medzi: " + aDataLoader.getNameOfCity(aUsedCities.get(indexKde))
                        + "\t" + aDataLoader.getNameOfCity(aUsedCities.get(indexNasledujuciPoKde))
                        + "\tZhorsenie: " + zhorsenie);
            }
            System.out.println("---------------------------------------------------");
        }
        System.out.println("Vlozil som: " + aDataLoader.getNameOfCity(aNotUsedCities.get(najlepsiIndexPrvkuKtoryVlozit)));
        if (najlepsiNajdenyIndexNaKtoryVlozit != 0) {
            System.out.println("Medzi: " + aDataLoader.getNameOfCity(aUsedCities.get(najlepsiNajdenyIndexNaKtoryVlozit - 1))
                    + "\t" + aDataLoader.getNameOfCity(aUsedCities.get(najlepsiNajdenyIndexNaKtoryVlozit)));
        } else {
            System.out.println("Medzi: " + aDataLoader.getNameOfCity(aUsedCities.get(aUsedCities.size() - 1))
                    + "\t" + aDataLoader.getNameOfCity(aUsedCities.get(najlepsiNajdenyIndexNaKtoryVlozit)));
        }
        addToUsedCities(aNotUsedCities.get(najlepsiIndexPrvkuKtoryVlozit), najlepsiNajdenyIndexNaKtoryVlozit);

        System.out.println("************************************************************************************************");
    }

    public String vypisPouzite() {
        String ret = "";
        ret += "Pouzite: \n";
        for (int i = 0; i < aUsedCities.size(); i++) {
            ret += aDataLoader.getNameOfCity(aUsedCities.get(i)) + "\t";
        }
        if (aUsedCities.size() > 0) {
            ret += aDataLoader.getNameOfCity(aUsedCities.get(0));
        }

        return ret;
    }

    public String vypisNePouzite() {
        String ret = "";
        ret += "Nepouzite: \n";
        for (int i = 0; i < aNotUsedCities.size(); i++) {
            ret += aDataLoader.getNameOfCity(aNotUsedCities.get(i)) + "\t";
        }
        if (aNotUsedCities.size() > 0) {
            ret += aDataLoader.getNameOfCity(aNotUsedCities.get(0));
        }
        return ret;
    }

    public String spustSimulatedAnnealing() {
        String ret = "\n\n----------------------------------METAHEURISTIKA SIMULOVANE CHLADENIE----------------------------\n";
        SimulatedAnnealing sA = new SimulatedAnnealing(aDataLoader, aUsedCities);
        sA.setTemperature(100);
        sA.setaTempChangeCoef(0.005);
        ret += "\n" + sA.start();
        ret += vypisPouzite();
        return ret;
    }
}

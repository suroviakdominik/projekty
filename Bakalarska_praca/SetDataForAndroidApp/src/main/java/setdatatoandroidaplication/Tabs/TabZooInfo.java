/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JOpeningHoursPanel;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import static java.awt.image.ImageObserver.WIDTH;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import setdatatoandroidaplication.DataTransferer;

/**
 * V ramci tohto tabu je mozne menit otvaracia hodiny a zakladne informacie
 * o zoologickej zahrade
 * @author Dominik
 */
public class TabZooInfo extends Tab {

   
    private JPanel aTabPanel;
    private GridBagConstraints aGridConstrains;
    private static final Font FONT = new Font("myfont", WIDTH, 16);
    private final int PREFFERED_HIGHT = 30, PREFFERED_WIDTH = 150, MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 150, MARGIN_RIGHT = 150;
    private JTextField aNazovZOO;
    private JTextField aPocetZvierat;
    private JTextField aPocetPavilonov;
    private JTextField aEmail;
    private JTextField aTelCislo;
    private JOpeningHoursPanel aOpeningHoursPanel;
    private DataTransferer aDataTransferer=DataTransferer.getInstance();
    private JTextField aWebPage;

     /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabZooInfo(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paVykonavacPrikazov) {
        super(paTabPane, paKeyListener, paNazovTabu,paVykonavacPrikazov);
        aCommandProcessingForTab = paVykonavacPrikazov;
        aTabPanel = super.getMiddleMainTabPanel();
        aTabPanel.setLayout(new GridBagLayout());
        aGridConstrains = new GridBagConstraints();

        initializeMainTabPanelComponents();
        for (Component prech : getMiddleMainTabPanel().getComponents()) {
            prech.addKeyListener(paKeyListener);
        }
    }

    /**
     * inicializuje vsetky vizualne komponenty pre tento tab
     */
    private void initializeMainTabPanelComponents() {
        aGridConstrains.fill = GridBagConstraints.BOTH;

        aGridConstrains.weighty = 0.05;
        addComponentsForAddNameOfZoo();
        aGridConstrains.gridy = 1;
        addComponentsForAddCountOfAnimals();
        aGridConstrains.gridy = 2;
        addComponentsFoAddCountOfAnimalCategoriesArea();
        aGridConstrains.gridy = 3;

        addComponentsForZooContactsData();
        aGridConstrains.weighty = 0.05;
        aGridConstrains.gridy = 5;
        addComponentForEditNameOfWebPage();
        
        aGridConstrains.weighty = 0.71;
        aGridConstrains.gridy = 6;
        addComponenteForOpeningHoursData();
        aGridConstrains.weighty = 0.04;
        aGridConstrains.gridy = 7;
        aGridConstrains.gridwidth = 2;
        addComponentsForSaveDateChanges();
        addFocusListenersForNumberTypeChecke();
        updateData();
    }

    @Override
    public void updateAllTabData() {

    }

    /**
     * prida komponent na zadanie nazvu ZOO
     */
    private void addComponentsForAddNameOfZoo() {

        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        aGridConstrains.weightx = 0.3;
        JLabel l = new JLabel("Zadajte názov ZOO: ");
        l.setFont(FONT);
        l.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aGridConstrains.anchor = GridBagConstraints.LINE_END;
        aTabPanel.add(l, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aGridConstrains.weightx = 0.7;
        aNazovZOO = new JTextField();
        aNazovZOO.setFont(FONT);
        aNazovZOO.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aNazovZOO.setMinimumSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aNazovZOO, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }

    /**
     * prida komponent na zadanie poctu zvierat
     */
    private void addComponentsForAddCountOfAnimals() {
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        aGridConstrains.weightx = 0.3;
        JLabel l = new JLabel("Zadajte počet zvierat: ");
        l.setFont(FONT);
        l.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(l, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aGridConstrains.weightx = 0.7;
        aPocetZvierat = new JTextField();
        aPocetZvierat.setFont(FONT);
        aPocetZvierat.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aPocetZvierat, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }

    /**
     *  prida komponenty na zadanie poctu pavilonov
     */
    private void addComponentsFoAddCountOfAnimalCategoriesArea() {
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        aGridConstrains.weightx = 0.3;
        JLabel l = new JLabel("Zadajte počet pavilonov: ");
        l.setFont(FONT);
        l.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(l, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aGridConstrains.weightx = 0.7;
        aPocetPavilonov = new JTextField();
        aPocetPavilonov.setFont(FONT);
        aPocetPavilonov.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aPocetPavilonov, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }

    /**
     * prida komponenty na zadanie kontaktnych udajov
     */
    private void addComponentsForZooContactsData() {
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        JLabel lTelC = new JLabel("Telefónne číslo: ");
        lTelC.setFont(FONT);
        lTelC.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(lTelC, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aTelCislo = new JTextField();
        aTelCislo.setFont(FONT);
        aTelCislo.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aTelCislo, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);

        aGridConstrains.gridy = 4;

        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        JLabel lEmail = new JLabel("Email: ");
        lEmail.setFont(FONT);
        lEmail.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(lEmail, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aEmail = new JTextField();
        aEmail.setFont(FONT);
        aEmail.setName("mail");
        aEmail.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aEmail, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }

    /**
     * prida komponenty na zadnie adresy webstranky
     */
    private void addComponentForEditNameOfWebPage() {
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        JLabel lWebPage = new JLabel("Webová stránka: ");
        lWebPage.setFont(FONT);
        lWebPage.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(lWebPage, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aWebPage= new JTextField();
        aWebPage.setFont(FONT);
        aWebPage.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        aTabPanel.add(aWebPage, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }
    
    /**
     * prida komponenty na zadanie otvaracich hodin
     */
    private void addComponenteForOpeningHoursData() {
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, 0);
        JLabel lab = new JLabel();
        lab.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        lab.setText("Zadajte otváracie hodiny: ");
        lab.setFont(FONT);
        aTabPanel.add(lab, aGridConstrains);

        aGridConstrains.insets = new Insets(MARGIN_TOP, 0, MARGIN_BOTTOM, MARGIN_RIGHT);

        aGridConstrains.gridy = 6;
        aGridConstrains.weightx = 0;
        aOpeningHoursPanel = new JOpeningHoursPanel(FONT);

        aTabPanel.add(aOpeningHoursPanel, aGridConstrains);
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
    }

    /**
     * prida komponente na uloženie zmien
     */
    private void addComponentsForSaveDateChanges() {
        JButton bt1 = new JButton("Ulož informácie o zoo");
        bt1.setFont(FONT);
        bt1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionSaveDataChanges();
            }
        });
        aTabPanel.add(bt1, aGridConstrains);
    }

    /**
     * prida foculistener na kontrolu ciselnych vstupov
     */
    private void addFocusListenersForNumberTypeChecke() {
        aPocetZvierat.addFocusListener(new FocusTestDataType(aPocetZvierat));
        aPocetPavilonov.addFocusListener(new FocusTestDataType(aPocetPavilonov));
        //aTelCislo.addFocusListener(new FocusTestDataType(aTelCislo));
        aEmail.addFocusListener(new FocusTestDataType(aEmail));
    }

    
    /**
     * ulozi vykonane zmeny v ramci tabu
     */
    private void actionSaveDataChanges() {
        if(!aOpeningHoursPanel.checkTimesData())
        {
           JOptionPane.showMessageDialog(this,
                    "Info about zoo file can not be updated, becouse of bad open hours format.",
                    "Info about zoo file update error",
                    JOptionPane.ERROR_MESSAGE);
           return;
        }
        for(Component prech:getMiddleMainTabPanel().getComponents())
        {
            if(prech instanceof JTextField)
            {
                JTextField f=(JTextField)prech;
                if(f.getText().equals(""))
                {
                  JOptionPane.showMessageDialog(this,
                    "Info about zoo file can not be updated, becouse some fields are empty.",
                    "Info about zoo file update error",
                    JOptionPane.ERROR_MESSAGE);
                     return; 
                }
            }
        }
        aDataTransferer.removeAllData();
        String mainInfo=aNazovZOO.getText()+"\n"+aPocetZvierat.getText()+"\n"
                +aPocetPavilonov.getText()+"\n"+aTelCislo.getText()+"\n"+aEmail.getText()+"\n"+aWebPage.getText();
        aDataTransferer.addString("MAIN_INFO", mainInfo);
        aDataTransferer.addString("OPENING_HOURS",aOpeningHoursPanel.getData());
        if(aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.INFO, PrikazyENUM.UPRAV))
        {
             JOptionPane.showMessageDialog(this, "Info about zoo was updated.");
             
        }
        else
        {
            JOptionPane.showMessageDialog(this,
                    "AppERROR: Info file cannot be updated.",
                    "ERROR: Application Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * nacita vsetky aktualne udaje, ktore boli naposledy ulozene
     */
     private void updateData()
        {
            if(aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.INFO, PrikazyENUM.DAJDATA))
            {
                String baseInfo[]=aDataTransferer.getString("MAIN_INFO").split("\n");
                aNazovZOO.setText(baseInfo[0]);
                aPocetZvierat.setText(baseInfo[1]);
                aPocetPavilonov.setText(baseInfo[2]);
                aTelCislo.setText(baseInfo[3]);
                aEmail.setText(baseInfo[4]);
                aWebPage.setText(baseInfo[5]);
                String openedHoursRows[][]=new String[7][2];
                String otvHodJedDna[] = aDataTransferer.getString("OPENING_HOURS").split(";;;");
                for(int i=0;i<7;i++)
                {
                    for (int j = 0; j < 2; j++) {
                        openedHoursRows[i][j]=otvHodJedDna[i].split("\\|")[j];
                    }
                }
                aOpeningHoursPanel.setData(openedHoursRows);
            }
           
        }

    
    /**
     * Kontrolujr polia, kde data musia by cislami
     */
    private class FocusTestDataType implements FocusListener {

        private final JTextField aTextField;
        public final Pattern VALID_EMAIL_ADDRESS_REGEX
                = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        private FocusTestDataType(JTextField paTextField) {
            aTextField = paTextField;
        }

        @Override
        public void focusGained(FocusEvent e) {

        }

        @Override
        public void focusLost(FocusEvent e) {
            if (aTextField.getName() == null) {
                try {
                    if (!aTextField.getText().equals("")) {
                        Integer.parseInt(aTextField.getText());
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(aTextField, "V tomto poli môžu byť zadané iba čísla.", "Zadaný údaj nie je číslo.", JOptionPane.INFORMATION_MESSAGE);
                    aTextField.setText("");
                }
            } else if (!aTextField.getText().equals("")&&!validateMail(aTextField.getText())) {
                JOptionPane.showMessageDialog(aTextField, "Zlý formát mailu.", "Zlý formát mailu.", JOptionPane.INFORMATION_MESSAGE);
                aTextField.setText("");
            }

        }

        private boolean validateMail(String emailStr) {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
            return matcher.find();
        }

       
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication;

import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class DataTransferer {

    private HashMap<String, String> aStringData;
    private HashMap<String, Integer> aIntData;
    private HashMap<String, Double> aDoubleData;
    private HashMap<String, BufferedImage> aImageData;
    private static DataTransferer aInstance = null;
    private HashMap<String,Info_About_Item> aInfoAboutItemData;

    private DataTransferer() {
        aStringData = new HashMap<>();
        aIntData = new HashMap<>();
        aDoubleData = new HashMap<>();
        aImageData = new HashMap<>();
        aInfoAboutItemData=new HashMap<>();
    }

    public static DataTransferer getInstance() {
        if (aInstance == null) {
            aInstance = new DataTransferer();
        }
        return aInstance;
    }

    public Integer getInt(String paKey) {
        return aIntData.get(paKey);
    }

    public void addInt(String paKey, int paValue) {
        aIntData.put(paKey, paValue);
    }
    
    public List<String> getAllStrings()
    {
        List<String> ret=new ArrayList<>();
        for(String prech:aStringData.keySet())
        {
            ret.add(aStringData.get(prech));
        }
        return ret;
    }
    
    public List<String> getAllStringsKeys()
    {
        ArrayList<String> ret=new ArrayList<>();
        for(String prech:aStringData.keySet())
        {
            ret.add(prech);
        }
        return ret;
    }
    
    public List<Info_About_Item> getAllInfoAboutItem()
    {
       ArrayList<Info_About_Item> ret=new ArrayList<>();
        for(String prech:aInfoAboutItemData.keySet())
        {
            ret.add(aInfoAboutItemData.get(prech));
        }
        return ret;
    }
    
    public List<String> getAllInfoAboutItemKeys()
    {
        ArrayList<String> ret=new ArrayList<>();
        for(String prech:aInfoAboutItemData.keySet())
        {
            ret.add(prech);
        }
        return ret;
    }

    public String getString(String paKey) {
        return aStringData.get(paKey);
    }

    public void addString(String paKey, String paValue) {
        aStringData.put(paKey, paValue);
    }

    public Double getDouble(String paKey) {
        return aDoubleData.get(paKey);
    }

    public void addDouble(String paKey, double paValue) {
        aDoubleData.put(paKey, paValue);
    }

    public BufferedImage getImage(String paKey) {
        return aImageData.get(paKey);
    }

    public void addImage(String paKey, BufferedImage paValue) {
        aImageData.put(paKey, paValue);
    }
    
    public void addItemInfo(String paKey,Info_About_Item paItem)
    {
        aInfoAboutItemData.put(paKey, paItem);
    }
    
    public Info_About_Item getItemInfo(String paKey)
    {
        return aInfoAboutItemData.get(paKey);
    }
    
    public void removeAllData()
    {
        aImageData.clear();
        aStringData.clear();
        aIntData.clear();
        aDoubleData.clear();
        aInfoAboutItemData.clear();
    }

}

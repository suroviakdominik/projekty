import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * Trida Gui je podtridou tridy JPanel a
 * obsahuje objekty nutne k vytvoreni
 * grafickeho uzivatelskeho prostredi - GUI
 * 
 * @author Jirka Krhanek
 * @version 2006-05-10
 */
public class Gui extends JPanel
{
    // instanci promenne - jednotlive komponenty GUI
    // tlacitka
    private JButton kc1, kc2, kc5, kc10, kc20;
    public JButton vytisknout, storno, objednat;
    // popisek
    private JLabel minceCelkem;
    // vychozi nastaveni hodnoty mince
    private int mince = 0;
    
    // seznam prepinacu
    private ButtonGroup tarif;
    // jednotlive prepinace
    private JRadioButton min10, min20, min30, min40, min50;
    // popisek
    private JLabel tarifCelkem;
    // vychozi nastaveni hodnoty jizdenky
    private int cena = 0;
    private int celkovaCena = 0;
    
    // textove pole
    public JTextArea area;
    // soupatka pro textove pole
    private JScrollPane scroll; 
    
    // popisky
    private JLabel radka1, radka2, radka3, empty;
    
    // vychozi text pro textove pole
    String navod = "  1) objednat �asov� p�smo\n\n  2) vhodit jednotliv� mince\n\n  3) vytisknout l�stek\n\n"; 
    String objednano = "  Objednano za: ";   

    
/**
 * Metoda init() inicializuje Gui,
 * specifikuje spravce rozvrzeni
 * a vytvari jednotlive komponenty
 */
    public void init()
    {
        // nastaveni spravce rozvrzeni
        setLayout(new BorderLayout());
        // nastaveni barvy pozadi
        //setBackground(Color.cyan);
        
        // vytvoreni panelu pro titulek programu
        JPanel north = new JPanel();
        // nastaveni spravce rozvrzeni panelu north
        north.setLayout(new FlowLayout());
        // umisteni panelu north do panelu vyssi urovne
        add(north, BorderLayout.NORTH);
        // inicializace popisku nadpis
        JLabel nadpis = new JLabel(" > > >  Automat na j�zdenky  < < < ");
        // pridani komponenty do panelu
        north.add(nadpis);
        
        // dalsi vytvoreni panelu pro tlacitka minci
        JPanel west = new JPanel();
        // nastaveni spravce rozvrzeni panelu west
        west.setLayout(new GridLayout(6,1));
        // umisteni panelu west do panelu vyssi urovne
        add(west, BorderLayout.WEST);
              
        // inicializace deklarovanych talcitek a popisku
        // jejich umisteni do panelu a vytvoreni objektu pro naslouchani udalosti
        kc1 = new JButton("  1 koruna  ");
        kc1.addActionListener(new ObsluhaTlacitka(this));
        west.add(kc1);
        
        kc2 = new JButton("2 koruny");
        kc2.addActionListener(new ObsluhaTlacitka(this));
        west.add(kc2);
        
        kc5 = new JButton("5 korun");
        kc5.addActionListener(new ObsluhaTlacitka(this));
        west.add(kc5);
        
        kc10 = new JButton("10 korun");
        kc10.addActionListener(new ObsluhaTlacitka(this));
        west.add(kc10);
        
        kc20 = new JButton("20 korun");
        kc20.addActionListener(new ObsluhaTlacitka(this));
        west.add(kc20);
                
        minceCelkem = new JLabel("Vhozeno: " + mince + " K�");
        west.add(minceCelkem);
        minceCelkem.setHorizontalAlignment(minceCelkem.CENTER);
        
        
        // vytvoreni dalsiho panelu pro vyber casoveho pasma, tedy ceny jizdenky
        JPanel east = new JPanel();
        east.setLayout(new GridLayout(6,1));
        add(east, BorderLayout.EAST);
        
        min10 = new JRadioButton("10 minut  ", false);
        min10.addActionListener(new ObsluhaSeznamu(this));
        east.add(min10);
        
        min20 = new JRadioButton("20 minut", false);
        min20.addActionListener(new ObsluhaSeznamu(this));
        east.add(min20);
        
        min30 = new JRadioButton("30 minut", false);
        min30.addActionListener(new ObsluhaSeznamu(this));
        east.add(min30);
        
        min40 = new JRadioButton("40 minut", false);
        min40.addActionListener(new ObsluhaSeznamu(this));
        east.add(min40);
        
        min50 = new JRadioButton("50 minut", false);
        min50.addActionListener(new ObsluhaSeznamu(this));
        east.add(min50);
        
        // vytvoreni skupiny prepinacu a umisteni prepinacu do tohoto seznamu
        tarif = new ButtonGroup();
        tarif.add(min10);
        tarif.add(min20);
        tarif.add(min30);
        tarif.add(min40);
        tarif.add(min50);
        
        tarifCelkem = new JLabel("Cena: " + cena + " K�");
        east.add(tarifCelkem);
        tarifCelkem.setHorizontalAlignment(tarifCelkem.CENTER);
        
        
        // dalsi panel pro OK, Storno a Dalsi tlacitka
        JPanel south = new JPanel();
        south.setLayout(new FlowLayout());
        add(south, BorderLayout.SOUTH);
        
        // nacteni ikony pro tlacitko OK
        ImageIcon obrazekOK = new ImageIcon("tl-ok.jpg");
        vytisknout = new JButton("Vytisknout", obrazekOK);
        vytisknout.setEnabled(false);
        vytisknout.addActionListener(new ObsluhaTlacitka(this));
        south.add(vytisknout);
        
        storno = new JButton("Storno");
        storno.addActionListener(new ObsluhaTlacitka(this));
        south.add(storno);
        
        objednat = new JButton("Objednat");
        objednat.addActionListener(new ObsluhaTlacitka(this));
        south.add(objednat);
        
        
        // posledni panel pro umisteni textove oblasti
        JPanel center = new JPanel();
        center.setLayout(new GridLayout(1,1));
        add(center, BorderLayout.CENTER);
        
        // nova textova oblast
        area = new JTextArea(navod); 
        // oblast neni editovatelna
        area.setEditable(false);
        // umozneni scrolovani oblasti, vertikalne vzdy a horizontalne pokud je potreba
        scroll = new JScrollPane(area,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        center.add(scroll); 
    }

    
/**
 * Metoda secte mince
 * 
 * @param hodnota hodnota zvolene mince
 *
 */
    public void sectiMince(int hodnota)
    {
        // pricte hodnotu k minci a vysledek se zapise do mince
        mince += hodnota;
    }
    
/**
 * Metoda nastavi text popisku
 */
    public void nastavMinceCelkem()
    {
        // nastaveni textu pro popisek minceCelkem
        minceCelkem.setText("Vhozeno: " + mince + " K�");
    }
    
/**
 * Metoda vraci hodnotu mince
 *
 * @return vraci hodnotu mince
 */
    public int vratMince()
    {
        return mince;
    }
    
/**
 * Metoda nastavuje hodnotu mince
 *
 * @param hodnota hodnota mince
 */   
    public void nastavMince(int hodnota)
    {
        mince = hodnota;
    }

    
/**
 * Metoda vypocita cenu jizdenky a to podle zvoleneho prepinace ze seznamu tlacitek
 */
    public void vypoctiCenuJizdenky()
    {
        // podminky ktere urci, jaky prepinac byl vybran a podle toho stanovi cenu jizdenky
        if(min10.isSelected())
        {
            nastavCenuJizdenky(8);
        }
        else if(min20.isSelected())
        {
            nastavCenuJizdenky(12);
        }
        else if(min30.isSelected())
        {
            nastavCenuJizdenky(16);
        }
        else if(min40.isSelected())
        {
            nastavCenuJizdenky(20);
        }
        else if(min50.isSelected())
        {
            nastavCenuJizdenky(24);
        }
        // cena se pak uvede v textu popisku tarifCelkem
        tarifCelkem.setText("Cena: " + cena + " K�");
    }

    
/**
 * Metoda vraci cenu jizdenky
 * 
 * @return vraci cenu jizdenky
 */
    public int vratCenuJizdenky()
    {
        return cena;
    }

/**
 * Metoda nastavuje cenu jizdenky
 * 
 * @param hodnota hodnota predavana cene
 */    
    public void nastavCenuJizdenky(int hodnota)
    {
        cena = hodnota;
    }

/**
 * Metoda vraci celkovou cenu jizdenek
 * 
 * @return celkova cena
 */  
    public int vratCelkovouCenuJizdenek()
    {
        return celkovaCena;
    }

/**
 * Metoda nastavuje celkovou cenu jizdenek
 * 
 * @param hodnota hodnota predavana celkove cene
 */  
    public void nastavCelkovouCenuJizdenek(int hodnota)
    {
        celkovaCena = hodnota;
    }

/**
 * Metoda pricita k celkove cene cenu jizdenky
 */  
    public void pricistCenu()
    {
        celkovaCena += vratCenuJizdenky();
    }

    
/**
 * Metoda slouzi ke stornovani objednavky
 */
    public void stornovat()
    {
        // nastavni hodnotu mince na 0
        mince = 0;
        // prepsani popisku
        minceCelkem.setText("Vhozeno: " + mince + " K�");
        // vybere prepinac, ktery odpovida 10 minutam
        min10.setSelected(true);
        // nastavi hodnotu ceny jizdenky na zakladnich 8 pro 10 minutovou jizdenku
        cena = 8;
        // prepsani popisku
        tarifCelkem.setText("Cena: " + cena + " K�");
        
        // nastaveni ceny jizdenky
        nastavCenuJizdenky(8);
        // nastaveni celkove ceny jizdenek
        nastavCelkovouCenuJizdenek(0);
        
        // vycisteni seznamu mince a jizdenky
        Automat.mince.clear();
        Automat.jizdenky.clear();
        
        // nastaveni textu pro textovou oblast
        area.setText(navod);
        // tlacitko vytisknout nastavime na false - neda se na nej mackat
        vytisknout.setEnabled(false);
        // tlacitko storno nastavime na true - da se na nej mackat
        storno.setEnabled(true);
        // zmena popisku tlacitka objednat na Objednat
        objednat.setLabel("Objednat");
        // tlacitko objednat nastavime na true - da se na nej mackat
        objednat.setEnabled(true);
    }

    
/**
 * Metoda zobrazuje informace v textove oblasti
 * 
 * @param text text ktery se prida k retezci navod
 */    
    public void zobrazitVypis(String text)
    {
        // zretezeni textu s navodem vypiseme v textove oblasti
        area.setText(navod + text);
    }

}



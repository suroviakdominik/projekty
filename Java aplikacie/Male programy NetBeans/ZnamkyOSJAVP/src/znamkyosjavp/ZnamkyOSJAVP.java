/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyosjavp;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class ZnamkyOSJAVP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String osZnamky=FileWorker.readDataFromFile(new File("."+File.separator+"os.txt"),"|");
            String javaZnamky=FileWorker.readDataFromFile(new File("."+File.separator+"java.txt"),"|");
            HashSet<String> znamkyJava=new HashSet<>(Arrays.asList(javaZnamky.split("\\|")));
            HashSet<String> znamkyOS=new HashSet<>(Arrays.asList(osZnamky.split("\\|")));
            znamkyJava.retainAll(znamkyOS);
            System.out.println(znamkyJava);
        } catch (Exception ex) {
            Logger.getLogger(ZnamkyOSJAVP.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
       
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidencianehnutelnosti.manazer_prikazov;

import dynamickehasovanie.data.GPSPozicia;
import dynamickehasovanie.data.Nehnutelnost;
import dynamickehasovanie.data.Zaznam;
import dynamickehasovanie.struktura.DynamickeHesovanie;
import dynamickehasovanie.triestrom.TrieStrom;
import evidencianehnutelnosti.DialogParametreVytvaranejStruktury;
import evidencianehnutelnosti.GenerujDataDialog;
import evidencianehnutelnosti.MainMenu;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Dominik
 */
public class Manazer {

    private static final File PRIECINOK_ZO_SUBORMI_DH = new File("NehnutelnostiData");
    private DynamickeHesovanie<Nehnutelnost> aDynHasovanie;
    private JFrame aMainWindow;

    public Manazer(JFrame paMainWindow) {
        aMainWindow = paMainWindow;
        if (DynamickeHesovanie.existujeUlozenyStav(PRIECINOK_ZO_SUBORMI_DH)) {
            try {
                aDynHasovanie = new DynamickeHesovanie<>(Nehnutelnost.getImplmentatorTohtoZaznamu(), PRIECINOK_ZO_SUBORMI_DH);
            } catch (IOException ex) {
                Logger.getLogger(Manazer.class.getName()).log(Level.SEVERE, null, ex);
                vytvorNovuStrukturu();
            }
        } else {
            vytvorNovuStrukturu();
        }
    }

    public void vytvorNovuStrukturu() {
        DialogParametreVytvaranejStruktury dial = new DialogParametreVytvaranejStruktury(aMainWindow, true);
        dial.setLocationRelativeTo(aMainWindow);
        if (aDynHasovanie != null) {
            aDynHasovanie.uvolniSubory();
        }
        do {
            dial.setVisible(true);
        } while (dial.getReturnStatus() == DialogParametreVytvaranejStruktury.RET_CANCEL);
        aDynHasovanie = new DynamickeHesovanie<>(dial.getMaximalnyPocetZaznamovVBloku(),
                Nehnutelnost.getImplmentatorTohtoZaznamu(),
                dial.getMaximalnaHlbkaTrieStromu(),
                PRIECINOK_ZO_SUBORMI_DH);
    }

    public TrieStrom getTrieStrom() {
        return aDynHasovanie.getaTrieStrom();
    }

    public boolean pridajZaznam(String paNazovKU, int paSupisneCislo, String paPopis, List<GPSPozicia> paGPSPozicie) {
        GPSPozicia[] gpSPoz = new GPSPozicia[paGPSPozicie.size()];
        paGPSPozicie.<GPSPozicia>toArray(gpSPoz);
        return aDynHasovanie.vloz(new Nehnutelnost(paSupisneCislo, paNazovKU.toCharArray(), paPopis.toCharArray(), gpSPoz), false) == DynamickeHesovanie.STAV_VKLADANIA.VLOZENY;
    }

    public Zaznam vyhladajNehnutelnost(String nazovKU, int supCislo) {
        Nehnutelnost hlNehn = new Nehnutelnost(supCislo, nazovKU.toCharArray());
        return aDynHasovanie.hladaj(hlNehn);
    }

    public void vypisCelehoObsahuDatabazy(JTextArea paTextArea) {
        paTextArea.setText("");
        aDynHasovanie.vypisStrukturyPomocouVlakna(paTextArea);
    }

    public Zaznam vyradNehnutelnostZEvidencie(String paNazovKU, int paSupCislo) {
        return aDynHasovanie.odstran(new Nehnutelnost(paSupCislo, paNazovKU.toCharArray()));
    }

    public void vztvorZalohuAktualneVlozenychDat() {
        aDynHasovanie.vytvorZalohu();
    }

    public void obnovDataZoZalohy() {
        aDynHasovanie.obnovPredtymVytvorenuZalohu();
    }

    public void generujData(MainMenu paMainFrame) {
        GenerujDataDialog dial = new GenerujDataDialog(paMainFrame, true, aDynHasovanie);
        dial.setLocationRelativeTo(paMainFrame);
        dial.setVisible(true);
    }

    public void ulozDHPriUkonceniAplikacie() {
        aDynHasovanie.ulozDataAktualnyStavStruktury();
    }

    public boolean modifikujZaznam(String paNazovKU, int paSupisneCislo, String paPopis, List<GPSPozicia> paGpsPozicie) {
        GPSPozicia[] gpSPoz = new GPSPozicia[paGpsPozicie.size()];
        paGpsPozicie.<GPSPozicia>toArray(gpSPoz);
        return aDynHasovanie.vloz(new Nehnutelnost(paSupisneCislo, paNazovKU.toCharArray(), paPopis.toCharArray(), gpSPoz), true) == DynamickeHesovanie.STAV_VKLADANIA.MODIFIKOVANY;
    }

    public String vypisZaklInformacie() {
        String ret = "DYNAMICKÉ HASHOVANIE\n";
        ret = ret.concat("Priečinok kde sú uložené dáta: "+PRIECINOK_ZO_SUBORMI_DH.getAbsolutePath()+"\n");
        ret = ret.concat("Počet záznamov v jednom bloku: "+aDynHasovanie.getVelkostBlokuVBajtoch()/aDynHasovanie.getVelkostJednehoZaznamuVBajtoch()+"\n");
        ret = ret.concat("Veľkosť bloku v bajtoch: "+aDynHasovanie.getVelkostBlokuVBajtoch()+" B\n");
        ret = ret.concat("Veľkosť záznamu v bajtoch: "+aDynHasovanie.getVelkostJednehoZaznamuVBajtoch()+" B\n");
        ret = ret.concat("Maximálna hĺbka trie stromu: "+aDynHasovanie.getMaxHlbkaTrieStromu()+"\n");
        return ret;
    }

    public String vypisCelehoObsahuDatabazy() {
        return aDynHasovanie.sekvencnyVypisSuboru();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montecarlo.strelci;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class MonteCarloStrelci {

    private static Random strelaA = new Random();
    public static Random strelaB = new Random();
    public static Random strelaC = new Random();
    public static Random strelaD = new Random();
    public static int pocetPokusov = 100000;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int pocetPokusov = 1000000;
        Random geneatorVolbaCielov = new Random();
        Random generatorStrelby = new Random();
        int pocetVsetciMrtvy = 0;
        int pocetPreziloA = 0;
        int pocetPreziloB = 0;
        int pocetPreziloC = 0;
        int pocetPreziloD = 0;
        double strelyHracov[] = new double[4];
        int pocetZivych;
        int volbyCielov[] = new int[4];//0=ciel A, 1= cielB,2= ciel C
        boolean zivotyHracov[] = new boolean[4];//0= zivot hraca A, 1 = zivot hraca B...
        
        //EXPERIMENTY
        for (int i = 0; i < pocetPokusov; i++) {
            pocetZivych = 4;
            //na zaciatku su vsetci zivy
            for (int j = 0; j < 4; j++) {
                zivotyHracov[j] = true;
            }

            //strielaju sa pokym vsetci nezomru alebo nezostane iba jeden
            while (pocetZivych > 1) {
            //VYBER PO KOM BUDU ZIVY STRIELAT
                //po kom bude strielat hrac A ak je zivy
                if (zivotyHracov[0] = true) {
                    boolean vybralSiZivehoHraca = false;
                    do {
                     if(zivotyHracov[volbyCielov[0]=1+geneatorVolbaCielov.nextInt(3)]==true)
                     {
                         vybralSiZivehoHraca=true;
                     }
                    } while (!vybralSiZivehoHraca);
                }
                
                //po kom bude strielat hrac B ak je zivy
                if (zivotyHracov[1] = true) {
                    boolean vybralSiZivehoHraca = false;
                    
                    do {
                        volbyCielov[1]=geneatorVolbaCielov.nextInt(4);
                     if(volbyCielov[1]!=1 && zivotyHracov[volbyCielov[1]]==true)
                     {
                         vybralSiZivehoHraca=true;
                     }
                    } while (!vybralSiZivehoHraca);
                }
                
                //po kom bude strielat hrac C ak je zivy
                if (zivotyHracov[2] = true) {
                    boolean vybralSiZivehoHraca = false;
                    
                    do {
                        volbyCielov[2]=geneatorVolbaCielov.nextInt(4);
                     if(volbyCielov[2]!=2 && zivotyHracov[volbyCielov[2]]==true)
                     {
                         vybralSiZivehoHraca=true;
                     }
                    } while (!vybralSiZivehoHraca);
                }
                
                //po kom bude strielat hrac D ak je zivy
                if (zivotyHracov[3] = true) {
                    boolean vybralSiZivehoHraca = false;
                    
                    do {
                        volbyCielov[3]=geneatorVolbaCielov.nextInt(4);
                     if(volbyCielov[3]!=3 && zivotyHracov[volbyCielov[3]]==true)
                     {
                         vybralSiZivehoHraca=true;
                     }
                    } while (!vybralSiZivehoHraca);
                }
            
              //STRELBA!!!!!!!
                for (int j = 0; j < 4; j++) {
                    strelyHracov[j]= generatorStrelby.nextDouble();
                }
                
                //Vyhodnotenie Strelby
                if(strelyHracov[0]<=0.8)
                {
                    zivotyHracov[volbyCielov[0]]=false;
                }
                if(strelyHracov[1]<=0.7)
                {
                    zivotyHracov[volbyCielov[1]]=false;
                }
                if(strelyHracov[2]<=0.5)
                {
                    zivotyHracov[volbyCielov[2]]=false;
                }
                if(strelyHracov[3]<=0.4)
                {
                    zivotyHracov[volbyCielov[3]]=false;
                }
                
                //zistenie kolko je zivych
                for (int j = 0; j < 4; j++) {
                    if(zivotyHracov[j]==false)
                    {
                        pocetZivych--;
                    }
                }
                
            }
            if(pocetZivych==0)
            {
                pocetVsetciMrtvy++;
            }
            else if(zivotyHracov[0]==true)
            {
                pocetPreziloA++;
            }
            else if(zivotyHracov[1]==true)
            {
                pocetPreziloB++;
            }
            else if(zivotyHracov[2]==true)
            {
                pocetPreziloC++;
            }
            else if(zivotyHracov[3]==true)
            {
                pocetPreziloD++;
            }

        }
        System.out.println("Pravdepodobnost, ze su vsetci mrtvy: "+((double)pocetVsetciMrtvy/(double)pocetPokusov));
        System.out.println("Pravdepodobnost, ze zije A: "+((double)pocetPreziloA/(double)pocetPokusov));
        System.out.println("Pravdepodobnost, ze zije B: "+((double)pocetPreziloB/(double)pocetPokusov));
        System.out.println("Pravdepodobnost, ze zije C: "+((double)pocetPreziloC/(double)pocetPokusov));
         System.out.println("Pravdepodobnost, ze zije D: "+((double)pocetPreziloD/(double)pocetPokusov));
    }

}

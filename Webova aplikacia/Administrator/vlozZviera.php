<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
        $sql = "SELECT id FROM zviera";
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row["id"] > $this->biggestId) {
                    $this->biggestId = $row["id"];
                }
            }
        }
    }

    function vlozitZviera() {


        $this->biggestId+=1;
        $nazov = htmlspecialchars($_POST["menoZv"]);
        $popis = htmlspecialchars($_POST["popisZv"]);
        $katZv = htmlspecialchars($_POST["katZv"]);
        $obr = htmlspecialchars($_POST["obrZv"]);

        $sql1 = "SELECT id FROM kategoria WHERE meno='$katZv'";
        $result1 = mysqli_query($this->conn, $sql1);

        if (mysqli_num_rows($result1) > 0) {
            $row = mysqli_fetch_assoc($result1);
            $idKat=$row["id"];
            if ($obr == "") {
                $sql = sprintf("INSERT INTO zviera(id,idKategoria,nazov,popis,nazovObrazok)
                        VALUES ($this->biggestId,%d,'%s','%s',NULL)",
                        $this->conn->real_escape_string($idKat),
                        $this->conn->real_escape_string($nazov),
                        $this->conn->real_escape_string($popis));
            } else {
                $sql = sprintf("INSERT INTO zviera(id,idKategoria,nazov,popis,nazovObrazok)
                        VALUES ($this->biggestId,%d,'%s','%s','%s')",
                        $this->conn->real_escape_string($idKat),
                        $this->conn->real_escape_string($nazov),
                        $this->conn->real_escape_string($popis),
                        $this->conn->real_escape_string($obr));
            }

            if ($this->conn->query($sql) === TRUE) {
                echo "Nove zviera vytvorene";
                
                
            } else {
                echo "Error: " . $sql . "<br>" . $this->conn->error;
            }
        } else {
            echo "Zadana kategoria neexistuje.";
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}
if(isset($_SESSION["login"])&&$_SESSION["login"]==0)
{
$db = new DBPraca();
$db->findBiggestId();
$db->vlozitZviera();
$db->ukonciPristup();
}
else
{
    echo "Vkladat nove zviera moze admin po prihlaseni.";
}
?>
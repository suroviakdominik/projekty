Vretenica severná (obyčajná) je asi najznámejší a zároveň jediný jedovatý had na Slovenku. Je opradený množstvom povier a bájok, ktoré však nie sú pravdivé a žiaľ prispievajú k tomu, že sa stáva častým terčom útokov ľudí, ktorí sa s ním v prírode, na záhradách či poliach stretávajú. Je to však veľká chyba, lebo tento druh, podobne ako všetky hady, významne znižuje počty hlodavcov.

Taxonómia:
Taxonómia druhu prešla celkovou revíziou (Joger et al. 2007), ale zatiaľ nie je úplne ustálená. V minulosti sa v rámci areálu rozšírenia rozlišovalo 5 poddruhov: V. berus berus (Linnaeus, 1758), V. berus bosniensis (Boettger, 1889), V. berus pseudaspis Schreiber, 1912, a V. berus seoanei Lataste 1879 v Európskej časti a V. berus sachalinensis Darevsky, 1917 v Ázijskej časti areálu. Nilson (Nilson et al. 2005) v Európskej časti rozlišuje už len 2 poddruhy: V. berus berus (Linnaeus, 1758) a V. berus bosniensis (Boettger, 1889). Podľa Jogera (Joger in McDiarmid et al. 1999) V. berus bosniensis (Boettger, 1889) taxonomicky reprezentuje samostatný druh V. bosniensis (pozri nižšie).

Engelmann (Engelmann et al. 1993) opísal Vipera berus pseudaspis Schreiber 1912 ako odlišný poddruh, čiastočne synonymický k opisu Stejnegera (Stejneger 1907). Poddruh V. berus sachalinensis Darevsky, 1915 bol povýšený na samostatný druh Vipera sachalinensis Darevsky, 1917. Taxonómiu V. berus možno vyriešil Joger (Joger et al. 2007), ktorý sa na základe genetických analýz domnieva, že Vipera bosniensis, V. nikolskii, V. barani a V. sachalinensis patria do jediného komplexu v rámci odlišných populácií Vipera berus.
Na území Slovenska sa vyskytuje poddruh Vipera berus berus (Linnaeus, 1758).
vipera-berus-14

Charakteristika:
Telo je kratšie a pomerne zavalité. Celková dĺžka zriedka presahuje 80cm, pričom samce sú menšie (zvyčajne do 60cm). Chvost je v pomere k telu krátky (u samcov dlhší), zakončený malou ostrou šupinou. Análny štítok je celistvý (Baruš et al. 1992; Lác 1968, Nilson et al., 2005).


Hlava je široká, mierne dorzoventrálne (smerom od chrbta k bruchu) stlačená, pri pohľade zhora oválna, alebo slabo trojuholníkovitá. Od krku je výrazne oddelená a jej tvar je ovplyvnený stupňom naplnenia jedových žliaz. Oko je pomerne malé, s vertikálnou zreničkou, zhora kryté párom rozšírených supraokulárnych („nadočných“) štítkov (jeden nad každým okom). Dúhovka je červená, alebo červenkastá (Baruš et al. 1992, Lác 1968).


Šupiny sú na tele usporiadané v 19-23 radoch. V strede sú výrazne kýlnaté, okrem ventrálií (brušné šupiny) a spodného radu ventrolaterálnych šupín (šupiny v najspodnejšom bočnom rade), ktoré sú hladké. Nozdra sa nachádza v strede nasálnej („nosovej“)šupiny. Pileus tvorí 5 veľkých štítkov a väčšinou 10 menších štítkov. Celkovo je folidóza (tvar a usporiadanie šupín) hlavy a hlavne pileusu (tvar a usporiadanie šupín iba na hornej časti hlavy, ktoré zároveň slúžia ako rozlišovací znak medzi druhmi) individuálne veľmi variabilná (Baruš et al. 1992, Lác 1968, Nilson et al. 2005).


Sfarbenie jedincov je individuálne a pomerne variabilné. Charakteristický je sexuálny dichromatizmus (obr. 1 a 2), ktorý je navyše podmienený aj sezóne (samce sú v období rozmnožovanie sfarbené intenzívnejšie ako v ostatnej časti roku). Samce majú základnú farbu v odtieňoch sivej, sivozelnej a samice v hnedastej, hrdzavej, alebo žltohnedej farbe. U oboch pohlaví sa po celej dĺžke chrbta až po koniec chvosta tiahne tmavý, alebo čierny kľukatý pás. Táto kresba môže byť u niektorých jedincov prerušovaná, prípadne môže celkom chýbať. Na bokoch tela sú zvyčajne pozdĺžne usporiadané tmavšie škvrny rôznej veľkosti (sensu Baruš et al. 1992, Lác 1968, Nilson et al. 2005).
vipera-berus-5 	vipera-berus-13
samec 	samica


Na dorzálnej („vrchnej“) strane hlavy je často tmavá kresba v tvare písmena X. Po bokoch hlavy sa od nozdry cez oko až na krk, tiahne tmavý pás (ochranná funkcia jedového aparátu pred UV žiarením). Supralabiálne štítky (štítky, kryjúce horný „pysk“) majú väčšinou bielu farbu, ktorá prechádza až na podhrdlie, kde sa postupne mieša s oranžovými až hnedastými škvrnami. Brušná strana tela je od krku sfarbená v odtieňoch tmavo hnedo až čierne, niekedy s jemnými oranžovými škvrnami. Koniec chvosta je často žltkastý (sensu Baruš et al. 1992, Lác 1968, Nilson et al. 2005).


Častý je výskyt melanismu (tmavé sfarbenie pokožky pôsobením tmavého pigmentu – melanínu) – morpha prester (Linnaeus, 1761), ktorý je charakteristický pre obe pohlavia. Jedince sú sfarbené jednoliato čierne (obr. 3), pričom tmavá kľukatá kresba nie je vidno. Podhrdlie a koniec chvosta môže byť žlté až oranžové.

vipera-berus-9
Vipera berus, morfa prester (Linnaeus, 1761)

Okrem čiernych jedincov sa zriedkavo vyskytujú jasno oranžové – morpha chersea (Linnaeus, 1758). Táto farebná aberácia je častejšia u samíc. Takisto ako u morfy prester ani tu nie je kľukatá kresba viditeľná. (sensu Baruš et al. 1992, Lác 1968, Nilson et al. 2005).


Veľmi zriedkavý je výskyt albinotických jedincov (Trutnau 1975, 1979), kedy je na belavom podklade jasne vidno oranžová kľukatá kresba. Oči sú výrazne červené.


Sfarbenie mláďat je podobné ako u dospelcov, s výnimkou morfy prester a chersea, kde sú mláďatá po pôrode sfarbené normálne, a čierne alebo oranžové sfarbenie získavajú až postupne v neskoršom veku (sensu Baruš et al. 1992, Lác 1968).


Biológia:
Vretenica severná zväčša opúšťa zimovisko skoro na jar (samce zvyčajne skorej ako samice, často je vonku ešte sneh), približne v polovici marca. Vo vyšších nadmorských výškach neskôr. Zvieratá vyhľadávajú mikroklimaticky vhodné miesta, často blízko zimoviska, kde sa slnia (pri slnení je vretenica často stočená do „terča“, pričom sa ešte splošťuje, aby maximálne zväčšila plochu tela, čím efektívnejšie využíva teplo zo slnečného žiarenia). Zo začiatku sa samce môžu slniť spoločne, avšak už po niekoľkých dňoch si začínajú brániť svoje okrsky. Okrsky sa môžu čiastočne prekrývať a ich plocha je od pár m2 po niekoľko 10m2 (Nilson et al. 2005, Hromádka & Voženílek 1976).


V období párenia dochádza medzi samcami k rituálnym súbojom, kedy sa vzájomne preplietajú, vztyčujú a snažia sa protivníkovi pritlačiť hlavu k zemi. Samice a juvenilné jedince nie sú vzájomne agresívne vôbec. Obdobie rozmnožovania trvá približne od konca apríla a po jeho skončení sa vretenice rozliezajú po okolí. Veľkosť „lovných“ okrskov je absolútne individuálna a závisí ako na konkrétnej lokalite tak na danom jedincovi (sensu Baruš et al. 1992, Lác 1968).


Oplodnenie je vnútorné a kopulácia trvá aj niekoľko hodín (v priemere 1-4). Vretenica severná je ovoviviparný druh, to znamená, že mláďatá sa vyvíjajú v tele matky a zárodočný obal pretrhávajú buď pri prechode matkinou kloakou počas pôrodu, alebo tesne po vyjdení z kloaky. Samice rodí okolo 20 mláďat, ktoré sa takmer ihneď zvliekajú. Jedový aparát mláďat je plne funkčný ihneď po pôrode (Lác 1968).


Zloženie koristi sa mení s vekom a fyzickým stavom jedinca. Juvenilné zvieratá lovia malé skokany (Rana temporaria, R. arvalis), mláďatá jašterice živorodej (Zootoca vivipara), alebo slepúcha lámavého (Anguis fragilis). Dospelé jedince sa živia takmer výlučne teplokrvnými cicavcami (hlodavce), alebo príležitostne vtákmi. Výnimočne bol zaznamenaný kanibalizmus (Baruš et al. 1992, Nilson et al. 2005).


Reakcia pri vyrušení závisí od ročného obdobia, pohlavia, stavu jedinca a pod. Vo všeobecnosti platí, že sú menej ostražité v období rozmnožovania a samice v priebehu gravidity. Zvieratá sa vtedy snažia pomaly ujsť. Ak sa im zabráni v úteku, stočia sa do klbka a hlasito syčia, pričom esovito stáčajú hlavu a robia krátke výpady. Tieto útoky bývajú väčšinou so zavretou tlamou, avšak pri uchopení, alebo prišliapnutí uštkne okamžite (Baruš et al. 1992).

 

vipera-berus-6
plávajúca Vipera berus (Linnaeus, 1761)

Vretenica pomerne dobre pláva a nevyhýba sa blízkosti vody – podmáčané rašeliništia a vresoviská sú obľúbeným biotopom. Šplhá sa zriedkavo, avšak dokáže vyliezť na kríky aj do výšky 2m (Baruš et al. 1992).

 

Koncom leta sa vretenice opäť sťahujú do blízkosti zimovísk, ktoré môžu byť v nepriaznivejších podmienkach hromadné. Hibernácia (zimovanie) je v kľudovom stave pri teplote vzduchu v zimovisku cca 8-9°C (Baruš et al. 1992).


Jedový aparát:
Chrup je solenoglyfný (jedové zuby sú umiestnené na hornej čeľusti vpredu a majú dokonalý vnútorný kanálik, sú pomerne dlhé a zahnuté,) a zreteľne delený na uchopovacie, dozadu zahnuté, kratšie zuby a 3-5mm dlhé jedové zuby. Tie nasadajú na výrazne skrátenú os maxillare (kosť hornočeľustná). V kľudovej polohe sú sklopené smerom dozadu a uložené v slizničnej riase. Vztýčenie jedových zubov sa nedeje automaticky s otvorením úst – je nezávislé. Jedové zuby môže používať aj pri uchopení a posúvaní koristi smerom do ústnej dutiny (sensu Baruš et al. 1992).


Jedová žľaza sa nachádza za okom. Jed sa tvorí v cylindrických bunkách väziva jedovej žľazy odkiaľ sa postupne sústreďuje do pružných kanálikov jedovej žľazy, čím dochádza k jej zväčšeniu. V prednej časti jedového aparátu je slizová žľaza, ktorej sekrét upcháva vývod jedovej žľazy, aby sa mohla naplniť (sensu Baruš et al. 1992). Sekrét slizovej žľazy zároveň zrieďuje vlastný jed, čím sa uľahčuje jeho výdaj (Frommmhold 1969). Vývod jedového aparátu má vyústenie tesne nad v slizničnej riase tesne nad bázou jedového zubu. Jed je zo žľazy vytlačený stiahnutím okolitých svalov (sensu Baruš et al. 1992).


Jedový zub je funkčný približne 6 týždňov, potom vypadáva a na jeho miesto sa posúva náhradný zub. Výmena zubov neprebieha symetricky na oboch stranách, takže zviera má k dispozícii vždy aspoň jeden zub (sensu Baruš et al. 1992).

 

vipera-berus_0
jedový zub Vipera berus (Linnaeus, 1761)


Pri útoku je tlama široko otvorená a jedové zuby smerujú dopredu. Tela koristi sa dotýka najprv spodná čeľusť, čím spevňuje pozíciu hlavy a vytvára oporu pre efektívnejšie prekonanie odporu kože zubami. Šabľovitý tvar jedového zubu po preniknutí do koristi vytvára kapsu pre zväčšenie plochy na vstrebanie jedu. Účinku spätného prúdenia jedu bráni slizničná riasa vo vývode jedovej žľazy (sensu Baruš et al. 1992).


Samotný útok býva väčšinou jeden, avšak pri veľkom podráždení môže uhryznúť opakovane. Ak bol útok vyprovokovaný rušiteľom a nie potravou, vretenica rušiteľa nikdy neprenasleduje, ale naopak sa snaží ujsť (Baruš et al. 1992). Navyše, nie každé uštknutie končí zároveň intoxikáciou a to ani v prípade, že sú viditeľné vpichy po zuboch. Tvorba jedu je energeticky veľmi náročná, preto zviera pri obrannom uštknutí nemusí vôbec vpustiť do rany jed. Valenta (2008) uvádza, že z viac ako 90 prípadov uštknutia v ČR, zaznamenaných v rokoch 1999-2005 bolo viac ako polovica bez príznakov intoxikácie.


Jed sa tvorí neustále, avšak intenzita tvorby je závislá na stupni naplnenia jedového aparátu. V okolitom tkanive sa nachádza veľké množstvo melanocytov, ktoré majú funkciu ochrany jedu pred UV žiarením (Baruš et al. 1992). Jed je zmesou viacerých zložiek prevažne enzymatickej bielkovinovej povahy (peptidy, polypeptidy, proteínové toxíny a pod.). Vzhľadom je to priehľadná viskózna tekutina, žltkastej až zelenavej farby bez zápachu. Chuť je mierne horká a jed na vzduchu kryštalizuje (Valenta 2008).


Účinky jedu sú najmä hemolytické (rozklad červených krviniek rozrušením bunkovej membrány a uvolnením krvného farbiva) a hemoragické (krvácanie) (u V. bosniensis je výrazný podiel neurotoxických komponentov). Toxicita jedu je vysoká, avšak jeho množstvo je malé. Pri uštknutí sa do rany dostane približne 25-30mg natívneho toxínu, čo zodpovedá 5-10mg suchého jedu (Kornalík 1955, 1967; Felix 1968, 1978; Frommhold 1969; Orlov et al. 1982; Sosnovskij 1983), pričom letálna dávka pre priemerného zdravého dospelého človeka je okolo 15-20mg jedu v sušine (Valenta 2008).

Biotop:
Z ekologického hľadiska ide o eurytopný druh (druh vyskytujúci sa na mnohých typoch stanovíšť), ktorý je prispôsobený extrémnym podmienkam s náhlymi výkyvmi. V rámci celkového areálu rozšírenia obýva širokú škálu biotopov od otvorených plôch, cez pieskové duny a slaniská. Na Slovensku vyhľadáva slnečné, ale vlhkejšie miesta lesnatej krajiny (lúky, pasienky, polomy, porasty čučoriedok a brusníc, vresoviská, rašeliništia, okolie lesných potokov, opustené lesné kameňolomy, zarastené kamenité stráne a okraje lesov, ako aj horské lúky, hole, okraje kosodreviny, krovinaté okraje polí a medze). Hyposometricky (výškové stupne) vystupuje až do alpínskeho pásma, vo Vysokých Tatrách až do 2000m n.m. (Dyk 1958; Ponec 1965, 1978; Lác 1968; Lukniš et al. 1972; Hrabě et al. 1973).

 

vipera-berus-4
Vipera berus, (Linnaeus, 1761)


Rozšírenie na Slovensku:
Podľa Baruša (Baruš et al. 1992) je z hypsometrického hľadiska súvislé rozšírenie na vhodných lokalitách od 400-600m n.m. Prakticky obýva všetky vyššie položené oblasti (Malá a Veľká Fatra, Vysoké a Nízke Tatry, Muránska planina, Slovenské Rudohorie, Vihorlat). V oblasti Malých Karpát sa nevyskytuje a sporadický výskyt v Bielych Karpatoch nebol preukázaný (Baruš et al. 1992). Ojedinelý výskyt bol zaznamenaný v Slovenskom Krase (Štollman & Uhrin 1992).


Celkové rozšírenie:
Obýva takmer celú Európu a Áziu v páse ohraničenom na západe Anglickom a Škótskom, okrem Írska, kde sa nevyskytuje. Z juhu je výskyt ohraničený stredným Francúzskom, severným Talianskom a jadranským pobrežím. Hranica ďalej pokračuje až do severného Albánska, Macedónie a severného Grécka. Na južnej strane zasahuje do Srbska, Bulharska Rumunska (V. berus bosniensis) a Moldavska, ďalej na východne cez Ukrajinu, Rusko, severné Mongolsko, severnú Čínu, severnú Kóreu na východoruské pobrežie a Sachalin (V. sachalinensis). Severným okrajom jej rozšírenia je Nórsko, Švédsko a Fínsko, okrem najsevernejších častí. Od severozápadu klesá jej výskyt juhovýchodne po 60. rovnobežku (Valenta 2008).


Ochrana:
Vretenica severná je ohrozená najmä negatívnym dopadom antropických vplyvov (vplyvy spôsobené ľudskou činnosťou) – priamo (zabíjanie, odchyt), alebo nepriamo (chemizácia, aplikácia postrekov proti hmyzu, hlodavcom, melioračné zásahy, zástavba, rozdrobenie a genetická izolácia populácií).

 

vipera-berus-8
zabitá Vipera berus (Linnaeus, 1761)


Vretenica severná (tak ako aj ostatné druhy našich plazov) patrí podľa vyhlášky MŽP SR č. 24/2003 Z.z., ktorou sa vykonáva zákon 543/2002 Z.z. o ochrane prírody a krajiny k chráneným druhom živočíchov, vyskytujúcich sa na území Slovenskej republiky.


Na Slovensku sa okrem vretenice severnej prirodzene nevyskytuje žiadny iný druh jedovatých hadov. Ľudia žiaľ extrémne preceňujú toxicitu vretenice severnej a myslia si, že všetky hady sú nebezpečné. Zrejme preto ich stále zabíjajú.


Na základe kresby, alebo sfarbenia si s vretenicou často mýlia aj iné druhy našich chránených hadov – napr.: užovku hladkú (Coronella austriaca Laurenti, 1768), alebo užovku fŕkanú (Natrix tessellata (Laurenti, 1768)), pre ktoré potom končí stretnutie s človekom, napriek ich zákonnej ochrane tragicky.

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake.DrawableObjects
{
    /// <summary>
    /// enum polozky menu v game over
    /// </summary>
    public enum GameOverOptions
    {
        StartMenu,
        SaveScore,
        Exit
    }

    /// <summary>
    /// ak pouzivatel zvoli najeku polozku z menu, vyvola sa udalost, ktora dostane ako parameter objekt tejto triedy
    /// </summary>
    public class GamOvMenuOptEventArgs : EventArgs
    {
        private GameOverOptions aSelOption;
        public GamOvMenuOptEventArgs(GameOverOptions paSelectedOption)
        {
            aSelOption = paSelectedOption;
        }

        public GameOverOptions SelOption
        {
            get { return aSelOption; }
        }
    }

    
    /// <summary>
    /// Trieda ktora je zodpovedna za vykreslenie GameScreen obrazovky a volbu moznosti na nej sa nachadzajucich 
    /// </summary>
    class GameOverScreen:IDrawableObject
    {
        private const int VerticalGap =10;///vertikalna medzera menu poloziek
        private const int HorizontalGap = 10;//////horizontalna medzera menu poloziek

        private Texture2D aGameOverBackgText;///obrazok vykreslovany na pozadi game over screen
        private GraphicsDevice aGraphicsDevice;///obrazovka, ktora sluzi ako platno
        private Texture2D aOptionFrame;///ramik pre polozku menu
        private SpriteFont aFont;///pismo pre polozku menu
        private int aCountOfOptions;///pocet enum moznosti menu
        private List<Vector2> aPositionsOfOptions;///zoznam suradnic, kde vykreslovat polozky menu
        private List<string> aOptionNames;///mena jednotlivych poloziek menu
        public GameOverOptions SelectedOption { get; set; }///aktualne zvolena moznost v start menu - este nepotvrdena
        private float aTimeFromLastUpdate;///cas od posledneho updatu zvolenej polozky menu

        public event EventHandler<GamOvMenuOptEventArgs> OptionSelectedEvent;/// udalost ktora sa vyvola ked pouzivatel vyberie jednu z menu

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public GameOverScreen(GraphicsDevice graphicsDevice)
        {
            aGraphicsDevice = graphicsDevice;
            aPositionsOfOptions = new List<Vector2>();
            aOptionNames = new List<string>();
            aOptionNames.Add("Start menu");
            aOptionNames.Add("Save Score");
            aOptionNames.Add("Exit");
            SelectedOption = GameOverOptions.StartMenu;
            aCountOfOptions = Enum.GetValues(typeof(GameOverOptions)).Length;
        }

        /// <summary>
        /// sirka ramika menu polozky
        /// </summary>
        public float OptFrameWidth
        {
            get { return aOptionFrame.Width; }
        }

        /// <summary>
        /// vyska ramika menu polozky
        /// </summary>
        public float OptFrameHeight
        {
            get { return aOptionFrame.Height; }
        }

        /// <summary>
        /// nacita polozky, ktore sa budu vykreslovat, prehravat ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aGameOverBackgText = content.Load<Texture2D>("game-over");
            aOptionFrame = content.Load<Texture2D>("start");
            aFont = content.Load<SpriteFont>("SMFont");
            int pom = aCountOfOptions;
            Vector2 firstOptPos = new Vector2(aGraphicsDevice.Viewport.Width - OptFrameWidth - HorizontalGap,
                                             aGraphicsDevice.Viewport.Height - pom * VerticalGap - pom * OptFrameHeight);

            for (int i = 0; i < aCountOfOptions; i++)
            {
                aPositionsOfOptions.Add(new Vector2(firstOptPos.X, firstOptPos.Y + i * VerticalGap + i * OptFrameHeight));
            }
        }

        /// <summary>
        /// Update vybranej moznosti- ak pouzivatel stlacil sipku nadol alebo nahor
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
            {
                OnOptionSelectedEvent(new GamOvMenuOptEventArgs(SelectedOption));
            }
            aTimeFromLastUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (aTimeFromLastUpdate > 0.1)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                {
                    if (SelectedOption != 0)
                    {
                        SelectedOption--;
                    }
                    else
                    {
                        SelectedOption = (GameOverOptions)Enum.GetValues(typeof(GameOverOptions)).GetValue(aCountOfOptions - 1);
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Down) ||
                         GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                {
                    if (SelectedOption != (GameOverOptions)Enum.GetValues(typeof(GameOverOptions)).GetValue(aCountOfOptions - 1))
                    {
                        SelectedOption++;
                    }
                    else
                    {
                        SelectedOption = 0;
                    }
                }
                aTimeFromLastUpdate = 0;
            }
        }

        /// <summary>
        /// Vykreslenie poloziek menu
        /// </summary>
        /// <param name="drawer"></param>
        public void Draw(SpriteBatch drawer)
        {
            for (int i = 0; i < aPositionsOfOptions.Count; i++)
            {
                Color optCol = Color.White;
                if ((GameOverOptions) Enum.GetValues(typeof (GameOverOptions)).GetValue(i) == SelectedOption)
                {
                    optCol = Color.Red;
                }
                Vector2 stringSize = aFont.MeasureString(aOptionNames[i]);
                drawer.Draw(aOptionFrame, aPositionsOfOptions[i], null, optCol, 0f, Vector2.Zero, 1f, SpriteEffects.None,
                    1f);
                Vector2 stringPos = new Vector2(aPositionsOfOptions[i].X + OptFrameWidth/2 - stringSize.X/2,
                    aPositionsOfOptions[i].Y + OptFrameHeight/2 - stringSize.Y/2);
                drawer.DrawString(aFont, aOptionNames[i], stringPos, Color.Black,0f,Vector2.Zero, Vector2.One, SpriteEffects.None,1f);
            }
            Rectangle destRect = new Rectangle(0,0,aGraphicsDevice.Viewport.Width,aGraphicsDevice.Viewport.Height);
            drawer.Draw(aGameOverBackgText, destinationRectangle:destRect, color:Color.White,layerDepth: 0f);
        }

        protected virtual void OnOptionSelectedEvent(GamOvMenuOptEventArgs e)
        {
            var handler = OptionSelectedEvent;
            if (handler != null) handler(this, e);
        }
    }
}

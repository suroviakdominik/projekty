/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.data;

import dynamickehasovanie.struktura.TypeInfo;
import static dynamickehasovanie.data.ImplementatorZaznam.aInstance;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Dominik
 */
public class Osoba extends Zaznam {

    //private static final int DLZKA_RODNE_CISLO = 11;
    private static final int DLZKA_MENO = 20;
    private static final int DLZKA_PRIEZVISKO = 20;
    private static final int VELKOST_PRVOK_ROD_CISLO = TypeInfo.sizeof(Integer.class);
    private static final int VELKOST_PRVOK_MENO = TypeInfo.sizeof(Character.class);
    private static final int VELKOST_PRVOK_PRIEZVISKO = TypeInfo.sizeof(Character.class);
    private static final int VELKOST_PRVOK_CISLO_DOMU = TypeInfo.sizeof(Integer.class);

    private int aRodneCislo;
    private char[] aMeno = new char[DLZKA_MENO];
    private char[] aPriezvisko = new char[DLZKA_PRIEZVISKO];
    private int aCisloDomu;

    public Osoba(int paRodneCislo, char[] paMeno, char[] paPriezvisko, int paCisloDomu) {
        super();
        aRodneCislo = paRodneCislo;
        for (int i = 0; i < aMeno.length && i < paMeno.length; i++) {
            aMeno[i] = paMeno[i];
        }

        for (int i = 0; i < aPriezvisko.length && i < paPriezvisko.length; i++) {
            aPriezvisko[i] = paPriezvisko[i];
        }

        this.aCisloDomu = paCisloDomu;
    }

    private Osoba() {
        super();
        aRodneCislo = -1;
    }

    public Osoba(int aRodneCislo) {
        this.aRodneCislo = aRodneCislo;
        this.aMeno[0] = 'A';
    }

    public static ImplementatorZaznam getImplmentatorTohtoZaznamu() {
        return ImplementatorOsoba.getInstance();
    }

    @Override
    public int hashCode() {
        String ret = "";
        return  aRodneCislo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Osoba other = (Osoba) obj;
        return aRodneCislo == other.aRodneCislo;
    }
    
    @Override
    public String toString() {
        return  "JE PLATNY: "+super.jePlatny()+" "
                + "Osoba{" + "RodneCislo= " + String.valueOf(aRodneCislo).trim() + 
                ", Meno=" + String.valueOf(aMeno).trim() + ", Priezvisko=" 
                + String.valueOf(aPriezvisko).trim() + ", CisloDomu=" +
                aCisloDomu + '}';
    }

    /**
     *
     * @author Dominik
     */
    private static class ImplementatorOsoba extends ImplementatorZaznam {

        private ImplementatorOsoba(Class aImplementatorForClass) {
            super(aImplementatorForClass);
        }

        @Override
        public Zaznam toZaznam(byte[] paBinDataZaznam) {
            ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(paBinDataZaznam);
            DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
            Osoba ret = new Osoba();
            try {
                char platnost = hlpInStream.readChar();
                if (platnost == Platnost.PLATNY.get_Znacka_V_Subore()) {
                    ret.aJePlatny = Platnost.PLATNY;
                }else{
                    ret.aJePlatny = Platnost.NEPLATNY;
                }

                ret.aRodneCislo = hlpInStream.readInt();

                for (int i = 0; i < DLZKA_MENO; i++) {
                    ret.aMeno[i] = hlpInStream.readChar();
                }

                for (int i = 0; i < DLZKA_PRIEZVISKO; i++) {
                    ret.aPriezvisko[i] = hlpInStream.readChar();
                }

                ret.aCisloDomu = hlpInStream.readInt();

            } catch (IOException e) {
                throw new IllegalStateException("Error during conversion from byte array.");
            }
            return ret;
        }

        @Override
        public byte[] toByteArray(Zaznam paZaznam) {
            ByteArrayOutputStream hlpByteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
            if (paZaznam instanceof Osoba) {
                Osoba os = (Osoba) paZaznam;
                try {
                    hlpOutStream.writeChar(os.aJePlatny.get_Znacka_V_Subore());

                    hlpOutStream.writeInt(os.aRodneCislo);

                    for (int i = 0; i < DLZKA_MENO; i++) {
                        hlpOutStream.writeChar(os.aMeno[i]);
                    }

                    for (int i = 0; i < DLZKA_PRIEZVISKO; i++) {
                        hlpOutStream.writeChar(os.aPriezvisko[i]);
                    }

                    hlpOutStream.writeInt(os.aCisloDomu);

                    return hlpByteArrayOutputStream.toByteArray();

                } catch (IOException e) {
                    throw new IllegalStateException("Error during conversion to byte array.");
                }
            }
            throw new IllegalArgumentException("Implementator nie je implementatorom "
                    + "pre genericky typ: " + paZaznam.getClass().getSimpleName()
                    + " zadany pri vytvarani dynamickeho hashu"
                    + "Implementator je implementatorom pre objekty typu: "
                    + super.aImplementatorForClass.getSimpleName());
        }

        public static ImplementatorZaznam getInstance() {
            if (aInstance != null) {
                return aInstance;
            } else {
                return new ImplementatorOsoba(Osoba.class);
            }
        }

        @Override
        public int get_Velkost_Zaznamu_V_Bajtoch() {
            return Zaznam.getVelkostObjektuVBajtoch() + VELKOST_PRVOK_ROD_CISLO
                    + DLZKA_MENO * VELKOST_PRVOK_MENO + DLZKA_PRIEZVISKO * VELKOST_PRVOK_PRIEZVISKO
                    + VELKOST_PRVOK_CISLO_DOMU;
        }

        @Override
        public Zaznam vytvorNeplatnyZaznam() {
            Osoba os = new Osoba();
            os.setPlatny(Platnost.NEPLATNY);
            return os;
        }
    }
}

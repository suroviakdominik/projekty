/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $("#odKom").click(function () {
        var prihlaseny;
        var xmlhttp;

        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {

                if (xmlhttp.responseText != "none")
                {
                    alert("Na to aby ste mohi pisat komentare, musite byt prihlaseny.");
                    return;
                }
                else
                {
                    var s = document.getElementById("sub");
                    s.submit();
                }
            }

        }
        xmlhttp.open("POST", "../Prihlasenie/checklogin.php", true);
        xmlhttp.send();

    });
});

$(document).ready(function () {

    vypisKomenty();

});
function vypisKomenty()
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;
            document.getElementById("komentare").innerHTML = navrat;
        }
    }
    xmlhttp.open("POST", "vypisKoment.php", true);
    xmlhttp.send();
    ;

}


function  zmazKoment(komId)
{

    var elP = document.getElementsByTagName("p");
    var komDiv = elP[0].parentNode;

    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        vypisKomenty();
    }
    xmlhttp.open("POST", "zmazKoment.php?idKomentara=" + komId + "", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("idKoment=" + komId);
}

function upravKoment(element, komId)
{

    var x = element;
    var nodeDiv = x.parentNode;
    var nodeP = nodeDiv.childNodes;
    nodeDiv.innerHTML = "<form method=\"POST\">" +
            "<textarea class=\"koment\">" + nodeP[1].innerHTML + "</textarea>" +
            "<input type=\"button\" value=\"Uloz\" onclick=\"ulozUpravu(this," + komId + ")\">" +
            "</form>";


}

function ulozUpravu(el, komId)
{
    var x = el;
    var nodeForm = x.parentNode;
    var nodeTextA = nodeForm.childNodes[0];
    var texInArea = nodeTextA.value;

    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            vypisKomenty();
        }
        //document.getElementById("komentare").innerHTML=navrat;

    }
    xmlhttp.open("POST", "upravKoment.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("text=" + texInArea + "&komId=" + komId);
}

package com.suroviak.dominik.zooAplication;

import android.os.Environment;

/**
 * Created by Dominik on 3.3.2015.
 */
public enum Info_Dirs_File_ENUM {
    NEWS("NEWS", "info_about_file_news", "N", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//N-news
    ANIMALS("ANIMALS", "info_about_file_animals", "A", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//A-animals
    CATEGORIES("CATEGORIES", "info_about_file_categories", "C", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//C-categories
    EVENTS("EVENTS", "info_about_file_events", "E", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//E-events
    INFO("INFO", "info_about_file_info", "I", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//I-info about zoo
    OBJECTS("OBJECTS", "info_about_file_objects", "O", "Files/", "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/");//O-objects

    private final String aKeyName;
    private final String aNameOfInfoFile;
    private final String aSignOfDir;
    private final String aRelativePathOfDirecotryWhereIAM;
    private final String aServerDIRDomainOfThisFile;
    public static final String ROOT_DEVICE_FOLDER = Environment.getExternalStorageDirectory() + "/ZooBratislava/";//for work
    public static final String TEMPORARY_DEVICE_FOLDER = Environment.getExternalStorageDirectory() + "/ZooBratislava/TEMP/";
    public static final String ENDOFINFOFILE = "i";
    public static final String ADRESSMAINFILEONSERVER = "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/Files/main";
    public static final String RELATIVE_PATH_OF_OFFLINE_MAP_FILE = "Files/";
    public static final String NAME_OF_SAVE_FILE_OFFLINE_ROAD = "cesta.and";
    public static final String ADRESS_OFFLINE_MAP_FILE_ON_SERVER = "http://st.fri.uniza.sk/~suroviak3/ZooBratislava/Files/" + NAME_OF_SAVE_FILE_OFFLINE_ROAD;

    private Info_Dirs_File_ENUM(String paKeyName, String paNameOfFile, String paSignOfDir, String paPathOfDirecotryWhereIAM, String paServerDomainOfThisFile) {
        aKeyName = paKeyName;
        aNameOfInfoFile = paNameOfFile;
        aSignOfDir = paSignOfDir;
        aRelativePathOfDirecotryWhereIAM = paPathOfDirecotryWhereIAM;
        aServerDIRDomainOfThisFile = paServerDomainOfThisFile;


    }


    public String getaNameOfInfoFile() {
        return aNameOfInfoFile;
    }

    public String getKeyName() {
        return aKeyName;
    }

    public String getSignOfDir() {
        return aSignOfDir;
    }

    public String getaRelativePathOfDirecotryWhereIAM() {
        return aRelativePathOfDirecotryWhereIAM;
    }

    public String getServerDIRDomainOfThisFile() {
        return aServerDIRDomainOfThisFile;
    }

    public static String getNameOfInfoFileObject(Info_About_Dirs_File paInfoFileObject)
    {
        String ret=null;
        for(Info_Dirs_File_ENUM value:Info_Dirs_File_ENUM.values())
        {
            if(paInfoFileObject.getServerDomainOfThisFile().contains(value.getaNameOfInfoFile()))
            {
                ret=value.getaNameOfInfoFile();
                break;
            }
        }
        return ret;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hadajcislo;

import java.util.Random;

/**
 *
 * @author Hawk
 */
public class NumberGenerator {

    private int limit;
    private int randomNum;

    public NumberGenerator(int paLimit) {
        this.limit = paLimit;
        this.randomNum = this.GenerateNumber(0, 100);
        System.out.println("The number is: "+this.randomNum);
    }

    public int GenerateNumber(int min, int max) {
        return min + (new Random()).nextInt(max-min);
    }

    public String getGuesses() {
        return "Počet zostávajúcich pokusov: " + this.limit;
    }

    public String Guess(String number) {
        int num=0;
        try{
            num = Integer.parseInt(number);
        } catch(NumberFormatException ioe) {}

        int dif = Math.abs(this.randomNum - num);
        if (dif==0) return "Uhadol si!";

        limit--;

        if ((dif<=20)&&(dif>15)) return "Zima";
        if ((dif<=15)&&(dif>10)) return "Teplo";
        if ((dif<=10)&&(dif>5)) return "Horuco";
        if ((dif<=5)&&(dif>0)) return "Velmi horuco";

        if(limit==0) {
            return "Koniec hry!";
        }

        return "Ruska Zima";
    }

}

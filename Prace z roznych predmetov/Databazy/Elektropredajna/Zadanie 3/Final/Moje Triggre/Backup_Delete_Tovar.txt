create or replace TRIGGER BACKUPDELETETOVAR 
AFTER DELETE ON TOVAR 
REFERENCING OLD AS STARY NEW AS  NOVY
FOR EACH ROW
BEGIN
  Insert into BACKUPTOVAR
  values (:stary.katalogoveCislo,:stary.id_predajne,:stary.id_kategorie,
  :stary.id_typu,:stary.nazov,:stary.minimum_na_sklade,:stary.cena,:stary.mnozstvo,user);
END;
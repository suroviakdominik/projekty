#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

//FUNKCIA MAIN
//******************************************************
/// Aplikacia, ktora sluzi ako operacia
/// signal nad danym semaforom to znamena ze inkrementuje 
/// jeho hodnotu o jednotku. Funkcia prijima len jeden
/// argument ktory je povinny a predstavuje identifikator 
/// semafora nad ktorym sa ma operacia signal vykonat.
/// \param argv predstavuje pole argumentov v tomto pripade pozostava iba z identifikatora semafora.
/// \return 4 v pripade uspesneho ukoncenia ,-1 v pripade chyby.
int main(int argc, char *argv[])
{
int idSemafora = atoi(argv[1]);
struct sembuf signal[1] = {0,1,0};//sem_num=0,sem_op=1,sem_flg=0, pole obsahujuce prvky typu: struktura sembuf. {0,1,0} naplni strukturu nachadzajucu sa na indexe 0,
if(semop(idSemafora,&signal[0],1)<0)
{
printf("Error: Chyba pri post(signal) semafora. Operaciu post(signal) sa nepodarilo vykonat");
return -1;
}
//DEBUG printf("Nova hodnota semafora po vykonani post:  %d  \n",semctl(idSemafora,0,GETVAL));
return 4;
}
﻿using System;
using System.Net;
using System.Threading;
using Lidgren.Network;

namespace SnakeServer
{
    public class ServerNetworking : IDisposable
    {
        private const string ServerId = "snake";///nazov appp, ktora moze pouzivat server
        private const int MaxNumberOfConnections = 2;///max poce klientov
        public event EventHandler<MessProcEventArgs> aNewMessageProcessed;///udalost, ktora sluzi k vypisu dat do logu
        private NetServer aServer;
        private int aPortNumber;///cislo portu
        private Thread aListeningThread;///vlakno, ktore kontroluje data ziskane od klientova a preposiela ich dalsim

        ///<summary>
        /// konstruktor
        /// </summary>
        public ServerNetworking(int paPortNumber)
        {
            aPortNumber = paPortNumber;
            NetPeerConfiguration serverConfig = new NetPeerConfiguration(ServerId);
            serverConfig.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            serverConfig.Port = aPortNumber;
            serverConfig.MaximumConnections = MaxNumberOfConnections;
            aServer = new NetServer(serverConfig);
        }

        /// <summary>
        /// cislo portu na ktorom bezi server
        /// </summary>
        public int PortNumber
        {
            get { return aPortNumber; }
        }

        /// <summary>
        /// ak nastane udalost, tak ten kto vypisuje musi dostat to co ma vypisat. Dostane to ako instanciu tejto triedy
        /// </summary>
        public class MessProcEventArgs : EventArgs
        {
            private String aMessage;
            public MessProcEventArgs(String paSelectedOption)
            {
                aMessage = paSelectedOption;
            }

            public String SelOption
            {
                get { return aMessage; }
            }
        }

        protected virtual void OnMessageProcessedEvent(MessProcEventArgs e)
        {
            if (aNewMessageProcessed != null)
            {
                aNewMessageProcessed(this, e);
            }
        }

        /// <summary>
        /// stav servera - bezi, nespusteny ...
        /// </summary>
        public NetPeerStatus ServerStatus
        {
            get { return aServer.Status; }
        }

        /// <summary>
        /// spusti server
        /// </summary>
        public void startServer()
        {
            aServer.Start();
            OnMessageProcessedEvent(new MessProcEventArgs("Server start ... Port number: " + aPortNumber + Environment.NewLine));
        }

        /// <summary>
        /// kontroluje data od klientov kazdych paMilisecondInterval ms
        /// </summary>
        /// <param name="paMilisecondInterval"></param>
        public void startListening(int paMilisecondInterval)
        {
            aListeningThread = new Thread(() =>
            {
                OnMessageProcessedEvent(new MessProcEventArgs("Listening has been started ..." + Environment.NewLine));
                try
                {
                    do
                    {

                        checkForNewMessagesAndProcessIt();
                        Thread.Sleep(paMilisecondInterval);
                    } while (true);
                }
                catch (Exception ex)
                {
                    OnMessageProcessedEvent(new MessProcEventArgs("Listening has been stopped" + Environment.NewLine));
                    OnMessageProcessedEvent(new MessProcEventArgs(ex.StackTrace));
                    throw;
                }
            });
            aListeningThread.Start();
        }

        /// <summary>
        /// skontroluje nove data od klientov a rozposle ich dalsim
        /// </summary>
        private void checkForNewMessagesAndProcessIt()
        {
            //use local message variable
            NetIncomingMessage msgIn;
            //standard receive loop - loops through all received messages, until none is left
            while ((msgIn = aServer.ReadMessage()) != null)
            {
                //create message type handling with a switch
                switch (msgIn.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        //This type handles all data that have been send by you.
                        OnMessageProcessedEvent(
                            new MessProcEventArgs("Data incoming from client: " + msgIn.SenderConnection +
                                                  Environment.NewLine));
                        processIncomingData(msgIn);
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        String mess = msgIn.ReadString();
                        OnMessageProcessedEvent(new MessProcEventArgs("Client: " + mess + " send approve message: " + msgIn.SenderConnection + Environment.NewLine));
                        msgIn.SenderConnection.Approve();
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        mess = msgIn.ReadString();
                        TypDat? changeType = null;
                        switch (msgIn.SenderConnection.Status)
                        {

                            case NetConnectionStatus.Connected:
                                OnMessageProcessedEvent(new MessProcEventArgs("Client connected: " + mess + msgIn.SenderConnection + Environment.NewLine));
                                changeType = TypDat.PripojenieHraca;
                                sendToNewClientAllCurrentClients(msgIn.SenderConnection);
                                break;
                            case NetConnectionStatus.Disconnected:
                                OnMessageProcessedEvent(new MessProcEventArgs("Client DISconnected: " + msgIn.SenderConnection + Environment.NewLine));
                                changeType = TypDat.OdpojenieHraca;
                                break;
                            //user connected to server
                        }
                        if (changeType != null)
                        {
                            sendClientsServerStatusChangeMessages(changeType, msgIn.SenderEndPoint);
                        }
                        break;
                }
                //Recycle the message to create less garbage
                aServer.Recycle(msgIn);
            }
        }

        /// <summary>
        /// ukoncenie prace servera
        /// </summary>
        public void stopServer()
        {
            aListeningThread.Abort();
            aServer.Shutdown("Server has been shut down.");
            OnMessageProcessedEvent(new MessProcEventArgs("Server has been shut down." + Environment.NewLine));
        }

        public void Dispose()
        {
            stopServer();
        }

        /// <summary>
        /// spracuje data prichadzajuce od klientov a rozposle ich ostatnym klientom
        /// </summary>
        /// <param name="paMessage"></param>
        private void processIncomingData(NetIncomingMessage paMessage)
        {
            TypDat dataTyp = (TypDat)paMessage.ReadByte();
            switch (dataTyp)
            {
                case TypDat.ZmenaPozicie:
                    Direction dir = (Direction)paMessage.ReadByte();
                    bool moveSnakeAboutPartWidth = paMessage.ReadBoolean();
                    float speed = paMessage.ReadFloat();
                    string speedStr = "\tSPEED: " + speed;
                    string vykPohyb = "\tONE MOVEMENT FINISH: " + moveSnakeAboutPartWidth;
                    sendNewDirectionToAllClients(dir, moveSnakeAboutPartWidth, speed, paMessage.SenderEndPoint);
                    switch (dir)
                    {
                        case Direction.Down:
                            OnMessageProcessedEvent(new MessProcEventArgs("Down" + speedStr + vykPohyb + Environment.NewLine));
                            break;
                        case Direction.Right:
                            OnMessageProcessedEvent(new MessProcEventArgs("Right" + speedStr + vykPohyb + Environment.NewLine));
                            break;
                        case Direction.Up:
                            OnMessageProcessedEvent(new MessProcEventArgs("Up" + speedStr + vykPohyb + Environment.NewLine));
                            break;

                        case Direction.Left:
                            OnMessageProcessedEvent(new MessProcEventArgs("Left" + speedStr + vykPohyb + Environment.NewLine));
                            break;

                        case Direction.Non:
                            OnMessageProcessedEvent(new MessProcEventArgs("Non" + speedStr + vykPohyb + Environment.NewLine));
                            break;
                    }
                    break;
                case TypDat.VygenerovaneNoveJedlo:
                    float xPos = paMessage.ReadFloat();
                    float yPos = paMessage.ReadFloat();
                    OnMessageProcessedEvent(new MessProcEventArgs("NEW FOOD CREATED AT POSITION: " + xPos + " , " + yPos + Environment.NewLine));
                    sendNewFoodPositionToAllClients(xPos, yPos, paMessage.SenderEndPoint);
                    break;
                case TypDat.PrehraHraca:
                    sendPlayerLoseToAllOtherClients(paMessage.SenderEndPoint);
                    OnMessageProcessedEvent(new MessProcEventArgs("Player "+paMessage.SenderEndPoint+" lose the game"));
                    break;
            }
        }

        /// <summary>
        /// spravu o prehre hraca posle ostatnym klientom
        /// </summary>
        /// <param name="paSenderIpEndPoint"></param>
        private void sendPlayerLoseToAllOtherClients(IPEndPoint paSenderIpEndPoint)
        {
            NetOutgoingMessage messToClient;
            foreach (NetConnection conn in aServer.Connections)
            {
                if (!conn.RemoteEndPoint.Equals(paSenderIpEndPoint))
                {
                    messToClient = aServer.CreateMessage();
                    messToClient.Write(paSenderIpEndPoint);
                    messToClient.Write((byte)TypDat.PrehraHraca);
                    aServer.SendMessage(messToClient, conn, NetDeliveryMethod.ReliableOrdered);
                }
            }
        }

        /// <summary>
        /// spravu o pozicii noveho jedla posle ostatnym klientom
        /// </summary>
        /// <param name="xPos"></param>
        /// <param name="yPos"></param>
        /// <param name="paSenderIpEndPoint"></param>
        private void sendNewFoodPositionToAllClients(float xPos, float yPos, IPEndPoint paSenderIpEndPoint)
        {
            NetOutgoingMessage messToClient;
            foreach (NetConnection conn in aServer.Connections)
            {
                if (!conn.RemoteEndPoint.Equals(paSenderIpEndPoint))
                {
                    messToClient = aServer.CreateMessage();
                    messToClient.Write(paSenderIpEndPoint);
                    messToClient.Write((byte)TypDat.VygenerovaneNoveJedlo);
                    messToClient.Write(xPos);
                    messToClient.Write(yPos);
                    aServer.SendMessage(messToClient, conn, NetDeliveryMethod.ReliableOrdered);
                }
            }
        }

        /// <summary>
        /// spracu o pohybe hada posle ostatnym klientomn
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="paMoveSnakePartWidth"></param>
        /// <param name="paSpeed"></param>
        /// <param name="paSenderIpEndPoint"></param>
        private void sendNewDirectionToAllClients(Direction dir, bool paMoveSnakePartWidth, float paSpeed, IPEndPoint paSenderIpEndPoint)
        {
            foreach (NetConnection conn in aServer.Connections)
            {
                if (!conn.RemoteEndPoint.Equals(paSenderIpEndPoint))
                {
                    var messToClient = aServer.CreateMessage();
                    messToClient.Write(paSenderIpEndPoint);
                    messToClient.Write((byte)TypDat.ZmenaPozicie);
                    messToClient.Write((byte)dir);
                    messToClient.Write(paMoveSnakePartWidth);
                    messToClient.Write(paSpeed);
                    aServer.SendMessage(messToClient, conn, NetDeliveryMethod.ReliableOrdered);
                }
            }
        }

        /// <summary>
        /// posle spracu o pripojeni odpojeni noveho hraca ostatnym klientom
        /// </summary>
        /// <param name="changeType"></param>
        /// <param name="paSenderIpEndPoint"></param>
        private void sendClientsServerStatusChangeMessages(TypDat? changeType, IPEndPoint paSenderIpEndPoint)
        {
            foreach (NetConnection conn in aServer.Connections)
            {
                if (!conn.RemoteEndPoint.Equals(paSenderIpEndPoint))
                {
                    var messToClient = aServer.CreateMessage();
                    messToClient.Write(paSenderIpEndPoint);
                    messToClient.Write((byte)changeType);
                    aServer.SendMessage(messToClient, conn, NetDeliveryMethod.ReliableOrdered);
                }
            }
        }

        /// <summary>
        /// novo pripojenemu klientovi posle aktualnych klientov
        /// </summary>
        /// <param name="paNewClientConnection"></param>
        private void sendToNewClientAllCurrentClients(NetConnection paNewClientConnection)
        {
            foreach (var iter in aServer.Connections)
            {
                if (!paNewClientConnection.RemoteEndPoint.Equals(iter.RemoteEndPoint))
                {
                    var messToClient = aServer.CreateMessage();
                    messToClient.Write(iter.RemoteEndPoint);
                    messToClient.Write((byte)TypDat.PripojenieHraca);
                    aServer.SendMessage(messToClient, paNewClientConnection, NetDeliveryMethod.ReliableOrdered);
                }
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public class StromPlatno extends JPanel implements MouseMotionListener {

    private final int aPriemerKruzniceZobrazujucehoVrchol = 25;
    private List<VrcholNaVykreslenie> aVrcholyNaVykreslenie;
    private RBStrom aStrom;
    private int aPredpokladanyPocetUrovniStromu;

    public StromPlatno(RBStrom paStrom) {
        aStrom = paStrom;
        aVrcholyNaVykreslenie = new ArrayList<>();
        if (paStrom.getKoren() != null) {
            aVrcholyNaVykreslenie.add(new VrcholNaVykreslenie(getSize().width / 2 - aPriemerKruzniceZobrazujucehoVrchol, 50, paStrom.getKoren(), 1));
        }
        setAutoscrolls(true);
        addMouseMotionListener(this);
        setOpaque(true);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        int pocetVykreslenychVrcholov = 0;
        if(aStrom.jePocetCiernychDoListovRovnaky()){
            g2.setColor(Color.GREEN);
            g2.drawString("Strom je Vyvazeny-pocet ciernych do listov je rovnaky: ",50,10);
        }else{
            g2.setColor(Color.red);
            g2.drawString("ERROR: Strom nie je Vyvazeny-pocet ciernych do listov nie je rovnaky: ",50,10);
        }
        if(aStrom.skontrolujDvaCerveneZaSebou()){
            g2.setColor(Color.GREEN);
            g2.drawString("Ziadne dva cervene nejdu za sebou: ",50,20);
        }else{
            g2.setColor(Color.red);
            g2.drawString("ERROR: Niekde idu dva cervene za sebou: ",50,20);
        }
        aPredpokladanyPocetUrovniStromu = (int) (Math.log(aStrom.getaPocetPrvkov()) / Math.log(2)) + 1;
        int posunutieOdPredchadzajuceho = (int) Math.pow(2, aPredpokladanyPocetUrovniStromu) / 2;
        while (aVrcholyNaVykreslenie.size() > 0) {

            if (aVrcholyNaVykreslenie.get(0).aVrchol.jeCerveny()) {
                g2.setColor(Color.red);
            } else {
                g2.setColor(Color.BLACK);
            }
            g2.drawOval(aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX,
                    aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY,
                    aPriemerKruzniceZobrazujucehoVrchol,
                    aPriemerKruzniceZobrazujucehoVrchol);
            pocetVykreslenychVrcholov++;
            g2.drawString(aVrcholyNaVykreslenie.get(0).aVrchol.getaKluc().toString(),
                    aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX + aPriemerKruzniceZobrazujucehoVrchol / 4,
                    aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol / 2 + aPriemerKruzniceZobrazujucehoVrchol / 4);
            if (aVrcholyNaVykreslenie.get(0).aVrchol.getaLavySyn() != null) {
                Vrchol pridajLavySynNaVykreslenie = aVrcholyNaVykreslenie.get(0).aVrchol.getaLavySyn();
                VrcholNaVykreslenie lavySynNaVykreslenie = new VrcholNaVykreslenie(
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX - aPriemerKruzniceZobrazujucehoVrchol * posunutieOdPredchadzajuceho,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol * 2,
                        pridajLavySynNaVykreslenie,
                        aVrcholyNaVykreslenie.get(0).aUrovenVrchola + 1);
                aVrcholyNaVykreslenie.add(lavySynNaVykreslenie);
                g2.setColor(Color.BLACK);
                g2.drawLine(aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX + aPriemerKruzniceZobrazujucehoVrchol / 2,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX - aPriemerKruzniceZobrazujucehoVrchol * posunutieOdPredchadzajuceho + aPriemerKruzniceZobrazujucehoVrchol,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol * 2 + aPriemerKruzniceZobrazujucehoVrchol / 2);
            }

            if (aVrcholyNaVykreslenie.get(0).aVrchol.getaPravySyn() != null) {
                Vrchol pridajPravySynNaVykreslenie = aVrcholyNaVykreslenie.get(0).aVrchol.getaPravySyn();
                VrcholNaVykreslenie pravySynNaVykreslenie = new VrcholNaVykreslenie(
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX + aPriemerKruzniceZobrazujucehoVrchol * posunutieOdPredchadzajuceho,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol * 2,
                        pridajPravySynNaVykreslenie,
                        aVrcholyNaVykreslenie.get(0).aUrovenVrchola + 1);
                aVrcholyNaVykreslenie.add(pravySynNaVykreslenie);
                g2.setColor(Color.BLACK);
                g2.drawLine(aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX + aPriemerKruzniceZobrazujucehoVrchol / 2,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaX + aPriemerKruzniceZobrazujucehoVrchol * posunutieOdPredchadzajuceho,
                        aVrcholyNaVykreslenie.get(0).aSuradnicaVykresleniaY + aPriemerKruzniceZobrazujucehoVrchol * 2 + aPriemerKruzniceZobrazujucehoVrchol / 2);
            }
            int aktUroven = aVrcholyNaVykreslenie.get(0).aUrovenVrchola;
            aVrcholyNaVykreslenie.remove(0);
            if (aVrcholyNaVykreslenie.size() > 1) {
                int novaUroven = aVrcholyNaVykreslenie.get(0).aUrovenVrchola;
                if (aktUroven != novaUroven) {
                    posunutieOdPredchadzajuceho = (int) Math.pow(2, aPredpokladanyPocetUrovniStromu - novaUroven + 1) / 2;
                }
            }
        }
        if (pocetVykreslenychVrcholov != aStrom.getaPocetPrvkov()) {
            JOptionPane.showMessageDialog(this, "Strom ma: "+aStrom.getaPocetPrvkov()+"\nPodarilo sa mi vykresli iba: "+pocetVykreslenychVrcholov, 
                    "Chyba stromu-nepodarilo sa vykreslit vsetky prvky", JOptionPane.ERROR_MESSAGE);
        }
        g2.drawString("Pocet vykreslenych vrcholov: " + pocetVykreslenychVrcholov+" z "+aStrom.getaPocetPrvkov()+" v strome",
                getSize().width / 2 - aPriemerKruzniceZobrazujucehoVrchol*4, 20);
        if (aStrom.getKoren() != null) {
            aVrcholyNaVykreslenie.clear();
            aVrcholyNaVykreslenie.add(new VrcholNaVykreslenie(getSize().width / 2 - aPriemerKruzniceZobrazujucehoVrchol, 50, aStrom.getKoren(), 1));
        }

    }

    void aktualizujData() {
        aVrcholyNaVykreslenie.clear();
        if (aStrom.getKoren() != null) {
            aVrcholyNaVykreslenie.add(new VrcholNaVykreslenie(getSize().width / 2 - aPriemerKruzniceZobrazujucehoVrchol, 50, aStrom.getKoren(), 1));
        }
        repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
        scrollRectToVisible(r);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    private class VrcholNaVykreslenie {

        private int aSuradnicaVykresleniaX;
        private int aSuradnicaVykresleniaY;
        private Vrchol aVrchol;
        private int aUrovenVrchola;

        public VrcholNaVykreslenie(int aSuradnicaVykresleniaX, int aSuradnicaVykresleniaY, Vrchol aVrchol, int paUrovenVrchola) {
            this.aSuradnicaVykresleniaX = aSuradnicaVykresleniaX;
            this.aSuradnicaVykresleniaY = aSuradnicaVykresleniaY;
            this.aVrchol = aVrchol;
            aUrovenVrchola = paUrovenVrchola;
        }

    }
}

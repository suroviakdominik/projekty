/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Miestnost;

import java.io.Serializable;
import java.util.HashMap;
import wof00.Hra.Hrac;
import wof00.Osoby.IOsoba;
import wof00.Predmety.IPredmet;

/**
 *
 * @author suroviak3
 */
public class MiestnostLabak implements IMiestnost, Serializable {

    private String aNazov;
    private String aPopisMiestnosti;
    private HashMap<String, IMiestnost> aVychody;
    private HashMap<String, IPredmet> aPredmety;
    private HashMap<String, IOsoba> aOsoby;
    private int aDlzkaZoSucasnejDoTejtoMiestnosti;
    private IMiestnost aPredchodca;
    private boolean aJePristupna = true;

    public MiestnostLabak(String paNazov, String paPopisMiestnosti) {
        aNazov = paNazov;
        aPopisMiestnosti = paPopisMiestnosti;
        aVychody = new HashMap<String, IMiestnost>();
        aPredmety = new HashMap<String, IPredmet>();
        aOsoby = new HashMap<String, IOsoba>();
    }

    @Override
    public void setaDlzkaZoSucasnejDoTejtoMiestnosti(int aDlzkaZoSucasnejDoTejtoMiestnosti) {
        this.aDlzkaZoSucasnejDoTejtoMiestnosti = aDlzkaZoSucasnejDoTejtoMiestnosti;
    }

    @Override
    public void setaPredchodca(IMiestnost aPredchodca) {
        this.aPredchodca = aPredchodca;
    }

    @Override
    public int dlzkaZatNajdNajkrCesZoSucVrch() {
        return aDlzkaZoSucasnejDoTejtoMiestnosti;
    }

    @Override
    public IMiestnost dajPredchodcuVrchola() {
        return aPredchodca;
    }

    @Override
    public IMiestnost dajVychod(String paSmer) {
        {
            return aVychody.get(paSmer);
        }

    }

    @Override
    public void vlozPredmet(IPredmet paPredmet) {
        aPredmety.put(paPredmet.getNazov(), paPredmet);
    }

    @Override
    public void nastavVychody(IMiestnost paMiestnost) {
        aVychody.put(paMiestnost.getaNazov(), paMiestnost);
    }

    @Override
    public String dajPopis() {
        return aPopisMiestnosti;
    }

    @Override
    public String dajPopisVychodov() {
        String vychod = "Vychod: ";
        for (String prechadzac : aVychody.keySet()) {
            vychod = vychod + " " + prechadzac;
        }
        return vychod;
    }

    @Override
    public String getaNazov() {
        return aNazov;
    }

    @Override
    public String dajPredmety() {
        String predmety = "Predmety miestnosti: ";
        for (String prechadzac : aPredmety.keySet()) {
            predmety = predmety + " " + prechadzac;
        }
        return predmety;
    }

    @Override
    public void vypisInfoMiestnosti() {
        System.out.println("Teraz si v miestnosti " + this.dajPopis());
        System.out.println(this.dajPopisVychodov());
        if (this.maPredmety()) {
            System.out.println(dajPredmety());
        }
        System.out.println(vypisOsoby());
    }

    @Override
    public IPredmet zoberPredmet(String paNazov) {
        return aPredmety.remove(paNazov);
    }

    @Override
    public boolean maPredmety() {
        boolean maPredmety = !aPredmety.isEmpty();
        return maPredmety;
    }

    @Override
    public IPredmet zistiPredmet(String paNazov) {
        return aPredmety.get(paNazov);
    }

    @Override
    public void zmazVychod(String paMiestnost) {
        aVychody.remove(paMiestnost);
    }

    @Override
    public void vlozOsobu(IOsoba osoba) {
        aOsoby.put(osoba.getaMeno(), osoba);
    }

    @Override
    public void vymazOsobu(String paMenoOsoby) {
        aOsoby.remove(paMenoOsoby);
    }

    @Override
    public String vypisOsoby() {
        String retazec = "Osoby: ";
        for (String prechadzac : aOsoby.keySet()) {
            retazec += prechadzac + "  ,  ";
        }
        return retazec;
    }

    @Override
    public IOsoba dajOsobu(String paMeno) {
        return aOsoby.get(paMeno);
    }

    @Override
    public void vymazVychod(IMiestnost paMiestnost) {
        aVychody.remove(paMiestnost.getaNazov());
    }

    @Override
    public boolean jePristupna(Hrac paHrac) {
        return aJePristupna;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vizitky;

import java.io.File;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Dominik
 */
public class VytvaracVizitiek {
    private HashMap<String,String> aPostoveSmerovacieCisla;

    public VytvaracVizitiek() {
        aPostoveSmerovacieCisla=new HashMap<>();
    }
    
    
    public void run() {
        nacitajSmerovacieCisla();
        String meno = nacitajMeno();
        String priezvisko=nacitajPriezvisko();
        String mesto=nacitajMesto();
        String adresa=nacitajUlicaACD();
        String psc=aPostoveSmerovacieCisla.get(mesto.trim());
        System.out.println("------------------Vizitka----------------------");
        System.out.println("Meno: "+meno);
        System.out.println("Priezvisko: "+priezvisko);
        System.out.println("Adresa: "+adresa+", "+psc+", "+mesto);
        System.out.println("-----------------------------------------------");
        String s="------------------Vizitka----------------------\n"
                + "Meno: "+meno+"\n"
                + "Priezvisko: "+priezvisko+"\n"
                + "Adresa: "+adresa+", "+psc+", "+mesto+"\n"
                + "-----------------------------------------------";
        FileWorker.writeDataToFile(new File("./vizitky.txt"),s, true);
                
    }
    

    

    private String nacitajMeno(){
        String meno = null;
        while (meno == null) {
            meno = ConsoleWorker.nacitajDataZKonzoly("vase meno", "^[A-Z]\\D+");
        }
        return meno;
    }

    private String nacitajPriezvisko() {
        String priezvisko = null;
        while (priezvisko == null) {
            priezvisko = ConsoleWorker.nacitajDataZKonzoly("vase priezvisko", "^[A-Z]\\D+");
        }
        return priezvisko;
    }
    
    private String nacitajMesto() {
        String mesto = null;
        while (mesto == null) {
            mesto = ConsoleWorker.nacitajDataZKonzoly("mesto vasho bydliska", "^[A-Z]\\D+");
        }
        return mesto;
    }

    private String nacitajUlicaACD() {
        String adresa = null;
        while (adresa == null) {
            adresa = ConsoleWorker.nacitajDataZKonzoly("ulicu a cislo domu v tvare ulica cd", "^[A-Z]\\D+\\s+\\s*\\d+");
        }
        adresa=adresa.replaceAll("\\s+"," ");
        return adresa;
    }

    private void nacitajSmerovacieCisla() {
        String psc=FileWorker.readDataFromFile(new File("./psc.txt"),"\n");
        Pattern p = Pattern.compile("\\D+\\d*.*\\s*:\\s*\\d{5,5}\\s*");
        Matcher m =p.matcher(psc);
        while(m.find())
        {
           String[] pom= m.group().split(":");
           aPostoveSmerovacieCisla.put(pom[0].trim(),pom[1].trim());
        }
        
    }

}

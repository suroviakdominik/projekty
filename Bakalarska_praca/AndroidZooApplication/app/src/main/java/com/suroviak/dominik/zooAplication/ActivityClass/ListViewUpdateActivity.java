package com.suroviak.dominik.zooAplication.ActivityClass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.suroviak.dominik.zooAplication.Connectivity;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.UpdateClasses.DownloadDataIfItNecessary;
import com.suroviak.dominik.zooAplication.UpdateClasses.IProgressBarUpdate;

import java.io.IOException;


public class ListViewUpdateActivity extends Activity implements View.OnClickListener, IProgressBarUpdate {


    private ProgressBar mProgress;
    private TextView aTextView;
    private int mProgressStatus = 0;
    private DownloadDataIfItNecessary aDownloadData;
    private Handler mHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_update);//neni potrebne nastavovat ked exted ListView
        mProgress = (ProgressBar) findViewById(R.id.progresbarupdate);
        aTextView = (TextView) findViewById(R.id.info_download_progress);


        Button btSkip = (Button) findViewById(R.id.button);
        Button btDownload = (Button) findViewById(R.id.button2);
        btSkip.setOnClickListener(this);
        btDownload.setOnClickListener(this);

        if (Connectivity.isConnectedWifi(this)) {
            new DownloadThread().start();

            btDownload.setVisibility(View.INVISIBLE);
        } else if (!Connectivity.isConnected(this)) {
            Intent i = new Intent(getBaseContext(), MainMenuTabActivity.class);
            startActivity(i);
            ListViewUpdateActivity.this.finish();
        }


    }

    @Override
    public void setProgressStatus(int paProgressStatus) {
        mProgressStatus = paProgressStatus;

    }

    @Override
    public void setText(final String paText) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                aTextView.setText(paText);
            }
        });
    }

    /*ked bola tato aktivita urobena ako list view aby sa dalo zisti odskrnute polozky v list view
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /*Toast.makeText(getApplicationContext(), "Pushed: " + itemsToUpdate[position], Toast.LENGTH_SHORT).show();
        //vypíše všetky tie, ktoré sú zaškrtnuté
        String selected = "";
        int cntChoice = listview.getCount();
        SparseBooleanArray sparseBooleanArray = listview.getCheckedItemPositions();
        String pushedItems="";
        for (int i = 0; i < cntChoice; i++) {
            if (sparseBooleanArray.get(i)) {
                pushedItems+=listview.getItemAtPosition(i).toString()+"; ";
            }
        }
        Toast.makeText(getApplicationContext(), "Checked Items: " + pushedItems, Toast.LENGTH_LONG).show();*/


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                Toast.makeText(this, "Skip clicked", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getBaseContext(), MainMenuTabActivity.class);

                startActivity(i);
                break;

            case R.id.button2:
                new DownloadThread().start();
                break;
        }
    }


    private class DownloadThread extends Thread {
        public void run() {
            super.run();
            aDownloadData = new DownloadDataIfItNecessary(ListViewUpdateActivity.this);
            aDownloadData.createInfo_Anout_Dirs_Object();
            try {
                aDownloadData.computeSizeOfFilesToDownload();
                new ProgressBarThread().start();
                aDownloadData.update();


            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent i = new Intent(getBaseContext(), MainMenuTabActivity.class);
            startActivity(i);
            ListViewUpdateActivity.this.finish();
        }
    }


    private class ProgressBarThread extends Thread {

        public void run() {
            super.run();
            while (mProgressStatus < 100) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Update the progress bar
                mHandler.post(new Runnable() {
                    public void run() {
                        mProgress.setProgress(mProgressStatus);
                        aTextView.setText("Download: " + mProgressStatus + "%");
                    }
                });
            }
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vizitky;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {
    public static String readDataFromFile(File f,String paOddelovacRiadkov)
    {
        Reader r=null;
        try {
            String ret="";
            r = new InputStreamReader(new FileInputStream(f),"Windows-1250");
            BufferedReader bR=new BufferedReader(r);
            String loadedLine=null;
            while((loadedLine=bR.readLine())!=null)
            {
                ret+=loadedLine+paOddelovacRiadkov;
            }
            return ret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                if(r!=null)
                r.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return null;
    }
    
    public static void writeDataToFile(File f,String paData,boolean paAppend)
    {
        Writer w=null;
        Reader r=null;
        try {
            w = new OutputStreamWriter(new FileOutputStream(f,paAppend),"Windows-1250");
            BufferedWriter bW=new BufferedWriter(w);
            r = new InputStreamReader(new ByteArrayInputStream(paData.getBytes()),"Windows-1250");
            BufferedReader bR=new BufferedReader(r);
            String loadedLine=null;
            while((loadedLine=bR.readLine())!=null)
            {
                bW.write(loadedLine);
                bW.newLine();
            }   bW.flush();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                if(w!=null)
                w.close();
                if(r!=null)
                {
                  r.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }
}

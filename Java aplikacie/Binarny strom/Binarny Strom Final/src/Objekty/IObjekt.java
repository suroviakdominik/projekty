/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objekty;

import java.io.File;
import java.io.IOException;

/**
 * Interface reprezetujúci rozhranie pre objekty, ktoré sa môžu nachádzať v ADŠ
 *
 * @author Dominik
 */
public interface IObjekt {

    public boolean napln();//boolean preto ked sa ma opyta chces nazozaj pridat

    public boolean zobraz();//boolean preto ked sa nas opyta ci chceme dany objekt  z evidenice odobrat

    public void nacitajZoSuboru(File paPodadresar, String panazov) throws IOException;

    public void zapisDoSuboru(File paPodadresar, int paCisloSuboru) throws IOException;

    public String toString();

    public String getPopis();

    public boolean equals(Object obj);

}

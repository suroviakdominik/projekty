/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import šach.hra.Hrac;

/**
 * Objekt tejto triedy sa zavolá pri príkaze info. Objekt vráti základné
 * informácie o figúrke, ktorú má hráč aktuálne vybranú.
 *
 * @author Dominik
 */
public class PrikazInfoOFigurke implements IPrikaz {

    /**
     * ak bol zadaný príkaz info, metóda vypíše informácie o figúrke, ktorú si
     * hráč predtým vybral. Zakaždým vracia false. Po vykonaní tohto príkazu sa
     * hráč, ktorý zadáva príkazy nemení.
     *
     * @param paHracKtoryZadavaPrikazy -hráč, ktorý je na ťahu
     * @param lpParameter
     * @return
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {
        if (paHracKtoryZadavaPrikazy.getaVybranaFigurka() != null) {
            System.out.println(paHracKtoryZadavaPrikazy.getaVybranaFigurka().getPopis());
        } else {
            System.out.println("Nemáte vybranú žiadnu figurku. Vyberte si figurku zo šachovnice pomocou príkazu: vyber +súradnice figurky na šachovnici");
        }
        return false;
    }

}


import java.util.ArrayList;
public class Veze
{
  private String aNazovVeze;
  private ArrayList<Integer> aArrayList;
  public Veze(String paNazovVeze)
  {
     aArrayList=new ArrayList<Integer>();
     aNazovVeze=paNazovVeze;
  }
   
    public int velkostArrayListu()
    {
        return aArrayList.size();
    }   
    
    public int zistiPrvokArrayListu()
     {
         if (aArrayList.isEmpty()==false)
         { 
           return aArrayList.get(aArrayList.size()-1);
         }
         else
         {
             return 0;
         }    
     }  
     
     public void vymazPrvokArrayListu()
     {
       aArrayList.remove(aArrayList.size()-1); 
     }
  
     public void premiestnenieKotucaVezu(Integer paKotuc)
     {
         if((aArrayList.isEmpty()==true)||((aArrayList.get(aArrayList.size()-1)>paKotuc)&&(paKotuc>0)))
         {
           aArrayList.add(paKotuc);
         }
         else
         {
             System.out.println("Neplatny tah !");
         }
     }
  
     public String vypisPrvkovArrayListu()
     {
        String vysledok="";
        if(aArrayList.isEmpty()==false)
        {
           for (Integer prechadzac:aArrayList)
           {
              vysledok+=" "+prechadzac;
           }
           return vysledok;
        }
        else
        {
           return "Na vezi nie je ziadny kotuc";
        }
     }
     
     public void nacitajArrayList(int paPocetKotucov)
     {
       int kotuc=paPocetKotucov;
         for(int i=0;i<paPocetKotucov;i++)
       {
           aArrayList.add(kotuc--);
       }
     } 
           
     
    
}


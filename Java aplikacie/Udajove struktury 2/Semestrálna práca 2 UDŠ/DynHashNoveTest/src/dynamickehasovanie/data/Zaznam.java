/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.data;

import dynamickehasovanie.struktura.TypeInfo;

/**
 *
 * @author Dominik
 */
public abstract class Zaznam {
    protected Platnost aJePlatny;
    
    protected Zaznam() {
        aJePlatny = Platnost.PLATNY;
    }

    protected static int getVelkostObjektuVBajtoch() {
        return Platnost.get_byte_velkost_znacky_v_subore();
    }

    public boolean jePlatny() {
        return aJePlatny == Platnost.PLATNY;
    }

    public void setPlatny(Platnost paPlatnost) {
        aJePlatny = paPlatnost;
    }
    
    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);
    
    @Override
    public abstract String toString();
    
    public static enum Platnost {

        PLATNY('P', "Platny zaznam"),//prvy parameter nemenit
        NEPLATNY('N', "Neplatny Zaznam");////

        private final char a_Znacka_V_Subore;
        private String a_Popis;

        private Platnost(char p_Znacka_V_Subore, String pa_Popis) {
            a_Znacka_V_Subore = p_Znacka_V_Subore;
            a_Popis = pa_Popis;
        }

        public static int get_byte_velkost_znacky_v_subore() {
            return TypeInfo.sizeof(char.class);
        }

        public char get_Znacka_V_Subore() {
            return a_Znacka_V_Subore;
        }

        public String getA_Popis() {
            return a_Popis;
        }
    }
}

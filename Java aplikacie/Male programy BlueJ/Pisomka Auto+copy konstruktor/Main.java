

public class Main
{
 public static void main()
 {
     Auto auto1=new Auto();
     auto1.onString();
     Auto auto2=new Auto("Mercedes","Biela","Nafta",2.9);
     auto2.onString();
     auto2.zmenFarbu("Cierna");
     Auto auto3=new Auto(auto2);
     auto3.onString();
 }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;



/**
 * Instancia tejto triedz popisuje subory danej polozky. Je agregovana
 * objektovm typu Info_About_DIRS_FIle.
 * !!!Kazda z poloziek musi byt agregovana niektorym z objektov Info_About_DIRS_FIle
 * @author Dominik
 */
public class Info_About_Item implements Serializable{
   private ArrayList<String> aDomainsOfFilesInCurrentItem=null;
   private String aPathOfFilesOfurrentItemInDevice=null;
   private Date aDateOfEditOrCreateFile=null;
   private String aOriginalName=null;
   private String aParentDirectory=null;
   private double aSuradnicaZemepisnaDlzka=-1;
   private double aSuradnicaZemepisnaSirka=-1;
   private String aSuradnicaZemSirkaText=null;
   private String aSuradniceZemDlzkaText=null;
   private static final long serialVersionUID = 1L;

   /**
    * Vytvori novu polozku. Urci jej meno, v ramci ktorej inej kategorie sa nachadya
    * a relat=ivnu cestu s=uborov pre polozku v konkr=etnom zariadeni (PC, Mobil)
    * @param paOriginalName - nazov vztvaranej polozky (napr. pri zvieratach Medved Hnedy)
    * @param paParentDirectory - nazzov priecinka (napr. kategoria zvierat), ktory je popisovany
    *                               inym objektom typu Info_About_Dirs_File ako sucasna polozka Info_About_Item
    * @param paPathOfFilesOfurrentItemInDevice - cesta priecinka, kde su subory pre tuto polozku (obrazky, texty...)
    */
    public Info_About_Item(String paOriginalName,String paParentDirectory,String paPathOfFilesOfurrentItemInDevice) {
        aOriginalName=paOriginalName;
        aPathOfFilesOfurrentItemInDevice=paPathOfFilesOfurrentItemInDevice;
        aParentDirectory=paParentDirectory;
        aDomainsOfFilesInCurrentItem=new ArrayList<>();
    }

    /**
     * Nastavi polozke zemepisnu dlzku
     * @param paSuradnicaZemepisnaDlzka - zemepisna dlzka, ktora bude nastavena
     */
    public void setaZemepisnaDlzka(double paSuradnicaZemepisnaDlzka) {
        this.aSuradnicaZemepisnaDlzka = paSuradnicaZemepisnaDlzka;
    }

    /**
     * Nastvi polozke zemepisnu sirku
     * @param paSuradnicaZemepisnaSirka - zemepisna sirka, ktora bude nastavena
     */
    public void setaZemepisnaSirka(double paSuradnicaZemepisnaSirka) {
        this.aSuradnicaZemepisnaSirka = paSuradnicaZemepisnaSirka;
    }
    
    
    /**
     * nastavi datum poslednej zmeny danej polozky(napr. sa zmeni popis)
     * @param paDateOfEditOrCreateFile - datim poslednej zmeny
     */
    public void setDateOfEditOrCreateFile(Date paDateOfEditOrCreateFile) {
        this.aDateOfEditOrCreateFile = paDateOfEditOrCreateFile;
    }

    /**
     * originalny nazov polozky (mnapr. Medved hnedy pri ziveratach)
     * @param paOriginalName 
     */
    public void setOriginalName(String paOriginalName) {
        this.aOriginalName = paOriginalName;
    }

    /**
     * nazzov priecinka (napr. kategoria zvierat), ktory je popisovany
    *   inym objektom typu Info_About_Dirs_File ako sucasna polozka Info_About_Item
     * @param paParentDirectory 
     */
    public void setParentDirectory(String paParentDirectory) {
        this.aParentDirectory = paParentDirectory;
    }

    /**
     * 
     * @return deomeny suborov na serveri (obrazky, text...) tejto poozky
     */
    public ArrayList<String> getDomainsOfFilesInCurrentItem() {
        return aDomainsOfFilesInCurrentItem;
    }

    /**
     * 
     * @return relativana cesta priecinka zo subormi tejto polozky
     *          v yariadeni (PC, Mobil)
     */
    public String getRelativePathOfFilesOfurrentItemInDevice() {
        return aPathOfFilesOfurrentItemInDevice;
    }

    /**
     * 
     * @return  datum poslednej zmeny polozky
     */
    public Date getDateOfEditOrCreateFile() {
        return aDateOfEditOrCreateFile;
    }

    /**
     * 
     * @return nazov polozky
     */
    public String getOriginalName() {
        return aOriginalName;
    }

    /**
     * 
     * @return nadares8r tejto polozky. !!! nemusia byt tieto polozky umiestnene
     *          v tomto nadaresari ale vedia jeho nazov
     */
    public String getParentDirectory() {
        return aParentDirectory;
    }

    /**
     * 
     * @return zemepisna dlzka polozky
     */
    public double getZemepisnaDlzka() {
        return aSuradnicaZemepisnaDlzka;
    }

    /**
     * 
     * @return zemepisna sirka polozkz
     */
    public double getZemepisnaSirka() {
        return aSuradnicaZemepisnaSirka;
    }

    /**
     * 
     * @param aSuradnicaZemSirkaText text obsahujuci zmepisnu sirku(to je: nie v dekadickej forme) 
     *                                  !!!!! v tvare DD:MM:SS.SSS MUSI BYT ODDELENE DVOJBODKAMI
     */
    public void setSuradnicaZemSirkaText(String aSuradnicaZemSirkaText) {
        this.aSuradnicaZemSirkaText = aSuradnicaZemSirkaText;
    }

  /**
     * 
     * @param aSuradnicaZemSirkaText text obsahujuci zmepisnu dlzku(to je: nie v dekadickej forme) 
     *                                  !!!!! v tvare DD:MM:SS.SSS MUSI BYT ODDELENE DVOJBODKAMI
     */
    public void setSuradniceZemDlzkaText(String aSuradniceZemDlzkaText) {
        this.aSuradniceZemDlzkaText = aSuradniceZemDlzkaText;
    }

    /**
     * 
     * @return zemepisna sirka v klasickej forme DD:MM:SS
     */
    public String getZemSirkaText() {
        return aSuradnicaZemSirkaText;
    }

    /**
     * 
     * @return zemepisna dlzka v klasickej forme DD:MM:SS
     */
    public String getZemDlzkaText() {
        return aSuradniceZemDlzkaText;
    }

    @Override
    public String toString() {
        return aOriginalName;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.aDomainsOfFilesInCurrentItem);
        hash = 47 * hash + Objects.hashCode(this.aPathOfFilesOfurrentItemInDevice);
        hash = 47 * hash + Objects.hashCode(this.aDateOfEditOrCreateFile);
        hash = 47 * hash + Objects.hashCode(this.aOriginalName);
        hash = 47 * hash + Objects.hashCode(this.aParentDirectory);
        hash = 47 * hash + (int) (Double.doubleToLongBits(this.aSuradnicaZemepisnaDlzka) ^ (Double.doubleToLongBits(this.aSuradnicaZemepisnaDlzka) >>> 32));
        hash = 47 * hash + (int) (Double.doubleToLongBits(this.aSuradnicaZemepisnaSirka) ^ (Double.doubleToLongBits(this.aSuradnicaZemepisnaSirka) >>> 32));
        hash = 47 * hash + Objects.hashCode(this.aSuradnicaZemSirkaText);
        hash = 47 * hash + Objects.hashCode(this.aSuradniceZemDlzkaText);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Info_About_Item other = (Info_About_Item) obj;
        if (!Objects.equals(this.aDomainsOfFilesInCurrentItem, other.aDomainsOfFilesInCurrentItem)) {
            return false;
        }
        if (!Objects.equals(this.aPathOfFilesOfurrentItemInDevice, other.aPathOfFilesOfurrentItemInDevice)) {
            return false;
        }
        if (!Objects.equals(this.aDateOfEditOrCreateFile, other.aDateOfEditOrCreateFile)) {
            return false;
        }
        if (!Objects.equals(this.aOriginalName, other.aOriginalName)) {
            return false;
        }
        if (!Objects.equals(this.aParentDirectory, other.aParentDirectory)) {
            return false;
        }
        if (Double.doubleToLongBits(this.aSuradnicaZemepisnaDlzka) != Double.doubleToLongBits(other.aSuradnicaZemepisnaDlzka)) {
            return false;
        }
        if (Double.doubleToLongBits(this.aSuradnicaZemepisnaSirka) != Double.doubleToLongBits(other.aSuradnicaZemepisnaSirka)) {
            return false;
        }
        if (!Objects.equals(this.aSuradnicaZemSirkaText, other.aSuradnicaZemSirkaText)) {
            return false;
        }
        if (!Objects.equals(this.aSuradniceZemDlzkaText, other.aSuradniceZemDlzkaText)) {
            return false;
        }
        return true;
    }
    
    
    
    
}

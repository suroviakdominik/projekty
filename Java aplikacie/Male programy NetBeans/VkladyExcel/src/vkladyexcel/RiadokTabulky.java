/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vkladyexcel;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author Dominik
 */
public class RiadokTabulky implements Serializable {

    private String aMeno;
    private String aPriezvisko;
    private Double aVyskaVkladu;

    public RiadokTabulky(String aMeno, String aPriezvisko, Double aVyskaVkladu) {
        this.aMeno = aMeno;
        this.aPriezvisko = aPriezvisko;
        this.aVyskaVkladu = aVyskaVkladu;
    }

    public String getaMeno() {
        return aMeno;
    }

    public String getaPriezvisko() {
        return aPriezvisko;
    }

    public Double getaVyskaVkladu() {
        return aVyskaVkladu;
    }

    public static Comparator<RiadokTabulky> podlaMena = new Comparator<RiadokTabulky>() {

        @Override
        public int compare(RiadokTabulky o1, RiadokTabulky o2) {
            if (o1.aMeno.compareTo(o2.aMeno) == 0) {
                return o1.aPriezvisko.compareTo(o2.aPriezvisko);
            }
            return o1.aMeno.compareTo(o2.aMeno);
        }
    };

    @Override
    public String toString() {
        return "RiadokTabulky{" + "aMeno=" + aMeno + ", aPriezvisko=" + aPriezvisko + ", aVyskaVkladu=" + aVyskaVkladu + '}';
    }
    
    

    public enum NazvyStlpcov {

        MENO("Meno") {

                    @Override
                    public Object dajHodnotu(RiadokTabulky paRiadok) {
                        return paRiadok.aMeno;
                    }

                },
        PRIEZVISKO("Priezvisko") {

                    @Override
                    public Object dajHodnotu(RiadokTabulky paRiadok) {
                        return paRiadok.aPriezvisko;
                    }

                },
        VKLAD("Vyska vkladu") {

                    @Override
                    public Object dajHodnotu(RiadokTabulky paRiadok) {
                        return paRiadok.aVyskaVkladu;
                    }

                };
        private String aNazovStlpca;

        private NazvyStlpcov(String aNazovStlpca) {
            this.aNazovStlpca = aNazovStlpca;
        }

        public String getaNazovStlpca() {
            return aNazovStlpca;
        }

        public abstract Object dajHodnotu(RiadokTabulky paRiadok);
    }
}

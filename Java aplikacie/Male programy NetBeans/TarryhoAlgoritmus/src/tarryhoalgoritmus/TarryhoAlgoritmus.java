/*
 * Tarryho algoritmus je taky algoritmus, ktory preskuma cely graf a vrati sa tam odkial zacal.
 * Algoritmus pouziva tieto pravidla:
 * T1: Kazdou hranou mozeme v jednom smere prechadzat iba raz
 * T2: Hranou prveho prichodu(t.j. hranou ktorou sme sa dostali do takeho vrchola kde sme este neboli)sa mozeme vratit(pouzit ju) za predpokladu T1
 *     (t.j. ze nepojdeme tym istym smerom ako pri prvom prechode) iba ak nie je ina moznost
 * 
 * Algoritmus berie do sledu prvu pouzitelnu hranu z mnoziny hran.
 * 
 */
package tarryhoalgoritmus;

import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Dominik
 */
public class TarryhoAlgoritmus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
      System.out.println("Zadajte vrchol, z ktoreho chcete zacat prieskum: ");
      Scanner scan=new Scanner(System.in);
      int vrchol=scan.nextInt();
      
      Vypocet vypocet=new Vypocet();
      vypocet.nacitajTextovyDokument();
      if(vypocet.getaPocetVrcholov()>=vrchol)
      {
        vypocet.vykonajAlgoritmus(vrchol);
      }
      else
      {
          System.out.println("Taky vrchol neexistuje");
      }
    }
}

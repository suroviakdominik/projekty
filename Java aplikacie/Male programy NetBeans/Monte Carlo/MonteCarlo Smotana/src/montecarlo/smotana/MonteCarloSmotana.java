/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montecarlo.smotana;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class MonteCarloSmotana {
    public static Random generator;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        generator = new Random();
        int cenaNakupu=10;
        double cenaPredajaPatDni;
        double cenaPredajaSiestyDen;
        
        double dopytPrvychPatDni;
        double zostatokZDopytuTyzdnaNaSiestyDen;//to co sme nepredali cez tyzden z toho co chceme predavat.
        int pocetPokusov = 500;
        int aTria=300;
        int cTria=400;
        double naklady;
        double prijmy;
        double zisk;
        double sumZisk=0;
        for (int i = 0; i < pocetPokusov; i++)
        {
               dopytPrvychPatDni = generujZTria(aTria, cTria);
               System.out.print("Dopyt 5 dni: "+dopytPrvychPatDni+"\t");
               zostatokZDopytuTyzdnaNaSiestyDen = dopytPrvychPatDni<380?380-dopytPrvychPatDni:0;
               System.out.print("Zostatok z tyzdna: "+zostatokZDopytuTyzdnaNaSiestyDen+"\t");
               cenaPredajaPatDni = 11+3*(generator.nextDouble());
               System.out.print("Cena 5 dni: "+cenaPredajaPatDni+"\t");
               cenaPredajaSiestyDen = cenaPredajaPatDni/2;
               System.out.print("Cena 6. den: "+cenaPredajaSiestyDen+"\t");
               naklady = 380*cenaNakupu;
               System.out.print("Naklady: "+naklady+"\t");
               prijmy = (dopytPrvychPatDni<380?dopytPrvychPatDni:380)*cenaPredajaPatDni+zostatokZDopytuTyzdnaNaSiestyDen*cenaPredajaSiestyDen;
               System.out.print("Prijmy: "+prijmy+"\t");
               zisk = prijmy-naklady;
               System.out.println("Zisk: "+zisk);
               sumZisk+=zisk;
        }
        double ocakavanyZisk380 = sumZisk/pocetPokusov;
        System.out.println(ocakavanyZisk380);
        sumZisk=0;
        for (int i = 0; i < pocetPokusov; i++)
        {
               dopytPrvychPatDni = generujZTria(aTria, cTria);
               System.out.print("Dopyt 5 dni: "+dopytPrvychPatDni+"\t");
               zostatokZDopytuTyzdnaNaSiestyDen = dopytPrvychPatDni<380?380-dopytPrvychPatDni:0;
               System.out.print("Zostatok z tyzdna: "+zostatokZDopytuTyzdnaNaSiestyDen+"\t");
               cenaPredajaPatDni = 11+3*(generator.nextDouble());
               System.out.print("Cena 5 dni: "+cenaPredajaPatDni+"\t");
               cenaPredajaSiestyDen = cenaPredajaPatDni/2;
               System.out.print("Cena 6. den: "+cenaPredajaSiestyDen+"\t");
               naklady = 340*cenaNakupu;
               System.out.print("Naklady: "+naklady+"\t");
               prijmy = (dopytPrvychPatDni<380?dopytPrvychPatDni:380)*cenaPredajaPatDni+zostatokZDopytuTyzdnaNaSiestyDen*cenaPredajaSiestyDen;
               System.out.print("Prijmy: "+prijmy+"\t");
               zisk = prijmy-naklady;
               System.out.println("Zisk: "+zisk);
               sumZisk+=zisk;
        }
        double ocakavanyZisk340 = sumZisk/pocetPokusov;
        System.out.println(ocakavanyZisk340);
        System.out.println(ocakavanyZisk380+", "+ocakavanyZisk340);
     }
    
    public static long generujZTria(double paA,double paC)
    {
       
        return Math.round((generator.nextDouble()+generator.nextDouble())*((paC-paA)/2)+paA);
    }
    
}

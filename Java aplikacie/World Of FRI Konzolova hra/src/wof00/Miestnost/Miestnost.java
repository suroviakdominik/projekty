package wof00.Miestnost;

import java.io.IOError;
import java.io.Serializable;
import wof00.Predmety.IPredmet;
import java.util.HashMap;
import wof00.Hra.Hrac;
import wof00.Osoby.IOsoba;

/**
 * Trieda Miestnost realizuje jednu miestnost/priestor v celom priestore hry.
 * Kazda "miestnost" je z inymi miestnostami spojena vychodmi. Vychody z
 * miestnosti su oznacovane svetovymi stranami sever, vychod, juh a zapad. Pre
 * kazdy vychod si miestnost pamata odkaz na susednu miestnost alebo null, ak
 * tym smerom vychod nema.
 *
 * @author Michael Kolling, David J. Barnes
 * @version 2006.03.30
 * @author lokalizacia: Lubomir Sadlon, Jan Janech
 * @version 2012.02.21
 */
public class Miestnost implements IMiestnost, Serializable {

    public String aPopisMiestnosti;

    private HashMap<String, IMiestnost> aVychody;
    private HashMap<String, IPredmet> aPredmety;
    private HashMap<String, IOsoba> aOsoby;
    private String aNazov;
    private int aDlzkaZoSucasnejDoTejtoMiestnosti;
    private IMiestnost aPredchodca;
    private boolean aJePristupna = true;

    /**
     * Vytvori miestnost popis ktorej je v parametrom. Po vytvoreni miestnost
     * nema ziadne vychody. Popis miesnost strucne charakterizuje.
     *
     * @param paPopis text popisu miestnosti.
     */
    public Miestnost(String paNazov, String paPopis) {
        this.aPopisMiestnosti = paPopis;
        aVychody = new HashMap<String, IMiestnost>();
        aPredmety = new HashMap<String, IPredmet>();
        aOsoby = new HashMap<String, IOsoba>();
        aNazov = paNazov;

    }

    @Override
    public void setaDlzkaZoSucasnejDoTejtoMiestnosti(int aDlzkaZoSucasnejDoTejtoMiestnosti) {
        this.aDlzkaZoSucasnejDoTejtoMiestnosti = aDlzkaZoSucasnejDoTejtoMiestnosti;
    }

    @Override
    public void setaPredchodca(IMiestnost aPredchodca) {
        this.aPredchodca = aPredchodca;
    }

    @Override
    public void vlozOsobu(IOsoba osoba) {
        aOsoby.put(osoba.getaMeno(), osoba);
    }

    @Override
    public void vymazOsobu(String paMenoOsoby) {
        aOsoby.remove(paMenoOsoby);
    }

    @Override
    public void vlozPredmet(IPredmet paPredmet) {
        aPredmety.put(paPredmet.getNazov(), paPredmet);
    }

    public boolean existujeVychod(String paSmer) {
        if (aVychody.containsKey(paSmer)) {
            return true;
        }
        return false;

    }

    /**
     * Nastavi vychody z miestnosti. Kazdy vychod je urceny bud odkazom na
     * miestnost alebo hodnotou null, ak vychod tym smerom neexistuje.
     *
     * @param paSever miestnost smerom na sever.
     * @param paVychod miestnost smerom na vychod.
     * @param paJuh miestnost smerom na juh.
     * @param paZapad miestnost smerom na zapad.
     */
    @Override
    public void nastavVychody(IMiestnost paMiestnost) {
        aVychody.put(paMiestnost.getaNazov(), paMiestnost);

        /*if (paSever != null) {
         aSevernyVychod = paSever;
         }
         if (paVychod != null) {
         aVychodnyVychod = paVychod;
         }
         if (paJuh != null) {
         aJuznyVychod = paJuh;
         }
         if (paZapad != null) {
         //</editor-fold>
         aZapadnyVychod = paZapad;
         }
         if (paHore != null) {
         //</editor-fold>
         aHornyVychod = paHore;
         }
         if (paDole != null) {
         //</editor-fold>
         aDolnyVychod = paDole;
         }*/
    }

    @Override
    public String getaNazov() {
        return aNazov;
    }

    /**
     * @return textovy popis miestnosti.
     */
    @Override
    public String dajPopis() {
        return aPopisMiestnosti;
    }

    @Override
    public String dajPopisVychodov() {
        String vychod = "Vychod: ";
        for (String prechadzac : aVychody.keySet()) {
            vychod = vychod + " " + prechadzac;
        }
        return vychod;
    }

    @Override
    public String dajPredmety() {
        String predmety = "Predmety miestnosti: ";
        for (String prechadzac : aPredmety.keySet()) {
            predmety = predmety + " " + prechadzac;
        }
        return predmety;
    }

    @Override
    public IMiestnost dajVychod(String paSmer) {
        return aVychody.get(paSmer);

        /*if (paSmer.equals("sever")) {
         return aSevernyVychod;
         }
         if (paSmer.equals("vychod")) {
         return aVychodnyVychod;
         }
         if (paSmer.equals("juh")) {
         return aJuznyVychod;
         }
         if (paSmer.equals("zapad")) {
         return aZapadnyVychod;
         }
         if (paSmer.equals("hore")) {
         return aHornyVychod;
         }
         if (paSmer.equals("dole")) {
         return aDolnyVychod;
         }
         return null;*/
    }

    @Override
    public void vypisInfoMiestnosti() {

        System.out.println("Teraz si v miestnosti " + this.dajPopis());
        System.out.println(this.dajPopisVychodov());
        if (this.maPredmety()) {
            System.out.println(dajPredmety());
        }
        System.out.println(vypisOsoby());

        /*System.out.println("Teraz si v miestnosti " + aAktualnaMiestnost.dajPopis());
         System.out.print("Vychody: ");
         if(aAktualnaMiestnost.dajVychod("sever") != null) {
         System.out.print("sever ");
         }
         if(aAktualnaMiestnost.dajVychod("vychod") != null) {
         System.out.print("vychod ");
         }
         if(aAktualnaMiestnost.dajVychod("juh") != null) {
         System.out.print("juh ");
         }
         if(aAktualnaMiestnost.dajVychod("zapad") != null) {
         System.out.print("zapad ");
         }
         if(aAktualnaMiestnost.dajVychod("hore") != null) {
         System.out.print("hore ");
         }
         if(aAktualnaMiestnost.dajVychod("dole") != null) {
         System.out.print("dole ");
         }
         System.out.println();*/
    }

    @Override
    public IPredmet zoberPredmet(String paNazov) {
        return aPredmety.remove(paNazov);
    }

    @Override
    public IPredmet zistiPredmet(String paNazov) {
        return aPredmety.get(paNazov);
    }

    @Override
    public boolean maPredmety() {
        boolean maPredmety = !aPredmety.isEmpty();
        return maPredmety;
    }

    @Override
    public void zmazVychod(String paMiestnost) {
        aVychody.remove(paMiestnost);
    }

    @Override
    public String vypisOsoby() {
        String retazec = "Osoby: ";
        for (String prechadzac : aOsoby.keySet()) {
            retazec += prechadzac + "  ,  ";
        }
        return retazec;
    }

    @Override
    public IOsoba dajOsobu(String paMeno) {
        return aOsoby.get(paMeno);
    }

    @Override
    public int dlzkaZatNajdNajkrCesZoSucVrch() //dlazka zatial najdnej najkratsej cesty zo sucasneho vrcholu
    {
        return aDlzkaZoSucasnejDoTejtoMiestnosti;
    }

    @Override
    public IMiestnost dajPredchodcuVrchola() {
        return aPredchodca;
    }

    @Override
    public void vymazVychod(IMiestnost paMiestnost) {
        aVychody.remove(paMiestnost.getaNazov());
    }

    @Override
    public boolean jePristupna(Hrac paHrac) {
        return aJePristupna;
    }

}

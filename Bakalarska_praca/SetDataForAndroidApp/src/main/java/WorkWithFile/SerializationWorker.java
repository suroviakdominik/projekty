/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WorkWithFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Trieda sluziaca na pracu so serializaciou
 * @author Dominik
 */
public class SerializationWorker<T>{
    private T aObjectThatIsSerializable;
    private String aEndOfFiles;
    private String aNameOfFile;
    private String aPath=null;

    /**
     * 
     * @param paEndOfFiles - koncovka serializovaneho suboru na distku (napr. bin))
     */
    public SerializationWorker(String paEndOfFiles) {
        aEndOfFiles=paEndOfFiles;
    }

    /**
     * 
     * @param paNameOfFile - nazov serializovaneho suboru 
     */
    public void setNameOfFile(String paNameOfFile) {
        this.aNameOfFile = paNameOfFile;
    }
    
    
    /**
     * 
     * @param aObjectThatIsSerializable objekt, ktory bude serializovany
     */
    public void setObjectThatIsSerializable(T aObjectThatIsSerializable) {
        this.aObjectThatIsSerializable = aObjectThatIsSerializable;
    }
    
    /**
     * 
     * @param paPath - cesta serializovaneho suboru na disku bez jeho mena
     *                  cize iba priecinok, kde sa serializovany subor nachadza
     */
    public void setPath(String paPath)
    {
        aPath=paPath;
    }
    
    /**
     * 
     * @return true - objekt uspesne serializovany
     *         false- objekt sa nepodarilo serializovat, doslo k chybe
     */
    public boolean saveObjectToFile()
    {
        FileOutputStream stream = null;
        ObjectOutputStream writer=null;
        try {
            File f = new File(aPath+aNameOfFile+"."+aEndOfFiles);
            stream = new FileOutputStream(f);
            writer=new ObjectOutputStream(stream);
            writer.writeObject(aObjectThatIsSerializable);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(writer!=null)
                {
                writer.flush();
                writer.close();
                }
                if(stream!=null)
                stream.close();
                return true;
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return false;
    }
    
    /**
     * 
     * @return deserializovany objekt vytvoreny zo suboru nachadyajucej sa na 
     *          cesta zadanej v predchadyajucich metodach
     */
    public T readObjetctFromFile()
    {
        T ret=null;
        
        FileInputStream stream =null;
        ObjectInputStream reader=null;
        try {
            File f = new File(aPath+aNameOfFile+"."+aEndOfFiles);
            stream = new FileInputStream(f);
            reader = new ObjectInputStream(stream);
            ret =(T)reader.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            try
            {
                if(reader!=null)
                reader.close();
                
                if(stream!=null)
                stream.close();
            }
            catch(IOException ex){
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return ret;
    }
}




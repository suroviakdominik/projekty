/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package skuska_1;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Hawk
 */
public class Pretekari {
    
    ArrayList<Pretekar> pr = new ArrayList<Pretekar>();

    public void add(Pretekar per) {
        pr.add(per);
    }

    public void sort() {
        Collections.sort(pr);
    }

    public String vypis() {
        this.sort();
        String out = "";

        for(int i=0;i<pr.size();i++) {
            out+= pr.get(i).getMeno() + " - " + pr.get(i).getMaxVykon()+"\n";
        }

        return out;
    }

}

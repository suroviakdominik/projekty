package com.suroviak.dominik.zooAplication.Navigation;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dominik on 29.3.2015.
 */
public class Road {
    private List<RoadPoint> aBodyNaCeste;
    private List<Vrchol> aVrcholy;
    private List<Hrana> aHrany;

    public Road() {
        aBodyNaCeste = new ArrayList<>();
        aVrcholy = new ArrayList<>();
        aHrany = new ArrayList<>();
    }

    public int getPocetVrcholovNaCeste() {
        return aVrcholy.size();
    }

    public List<Hrana> getHrany() {
        return aHrany;
    }

    public List<Vrchol> getVrcholy() {
        return aVrcholy;
    }


    public RoadPoint getBodNaCeste(RoadPoint.Suradnica paSuradnica) {
        for (RoadPoint prech : aBodyNaCeste) {
            if (prech.getSuradnica().equals(paSuradnica)) {
                return prech;
            }
        }
        System.out.println("ERROR: Cesta: getBodNaCeste(Coord,Coord)- vracia null hodnotu");
        return null;
    }

    public RoadPoint getBodNaCeste(int paIndexBodu) {
        return aBodyNaCeste.get(paIndexBodu);
    }

    public void pridajNovyBodNaCestu(RoadPoint paNovyBod) {
        if (!aBodyNaCeste.contains(paNovyBod)) {
            aBodyNaCeste.add(paNovyBod);
            aVrcholy.add(new Vrchol(paNovyBod, false));
        } else {
            Log.e("MyLog/DEBUG:", "Nepridal som bod na cestu lebo uz existuje.");
        }
    }

    public void pridajSusedaZadanemuBodu(RoadPoint paKomuPridavam, RoadPoint paPridavanySused, double paVzdialenost) {
        Vrchol v1 = aVrcholy.get(aVrcholy.indexOf(new Vrchol(paKomuPridavam, false)));
        Vrchol v2 = aVrcholy.get(aVrcholy.indexOf(new Vrchol(paPridavanySused, false)));
        if (v1 != null && v2 != null) {
            if (!aHrany.contains(new Hrana(v1, v2, paVzdialenost)) && !aHrany.contains(new Hrana(v2, v1, paVzdialenost))) {
                aHrany.add(new Hrana(v1, v2, paVzdialenost));
            }
        } else {
            Log.e("MyLog/DEBUG", "Nepodarilo sa pridat suseda: " + paPridavanySused.toString() + "vrcholu: " + paKomuPridavam.toString() + "pretoze niektory z bodov neexistuje");
        }
    }

    public void odstranHranu(Hrana paHranaNaOdstranenie) {
        if (aHrany.contains(paHranaNaOdstranenie)) {
            aHrany.remove(aHrany.indexOf(paHranaNaOdstranenie));
            Log.e("MyLogs/DEBUG", "Hrana: " + paHranaNaOdstranenie.toString() + " bola odstranena.");
        }
    }


    /**
     * Created by Dominik on 2.4.2015.
     */
    public static class Hrana {
        private Vrchol aVrchol1;
        private Vrchol aVrchol2;
        private double aDistance;


        public Hrana(Vrchol paVrchol1, Vrchol paVrchol2, double paVzdialenostVrcholov) {
            this.aVrchol1 = paVrchol1;
            aVrchol2 = paVrchol2;
            aDistance = paVzdialenostVrcholov;

        }

        public double getaDistance() {
            return aDistance;
        }

        public Vrchol getaVrchol1() {
            return aVrchol1;
        }

        public Vrchol getaVrchol2() {
            return aVrchol2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Hrana)) return false;

            Hrana hrana = (Hrana) o;

            if (!aVrchol1.equals(hrana.aVrchol1)) return false;
            if (!aVrchol2.equals(hrana.aVrchol2)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = aVrchol1.hashCode();
            result = 31 * result + aVrchol2.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Hrana{" +
                    "aVrchol1=" + aVrchol1 +
                    ", aVrchol2=" + aVrchol2 +
                    ", aDistance=" + aDistance +
                    '}';
        }
    }


    public static class Vrchol {
        private RoadPoint aBodPrisluchajuciVrcholu;
        private boolean aDefinitivnyVrchol;
        private double aDoterazNajdenaNajkratsiaCesta;
        private Vrchol aVrcholZKtorehoSomPrisiel;

        public Vrchol(RoadPoint paBodPrislVrcholu, boolean paIsDefinitivnyVrchol) {
            aBodPrisluchajuciVrcholu = paBodPrislVrcholu;
            aDefinitivnyVrchol = paIsDefinitivnyVrchol;
            aDoterazNajdenaNajkratsiaCesta = Double.MAX_VALUE;
        }

        public boolean isaDefinitivnyVrchol() {
            return aDefinitivnyVrchol;
        }

        public void setaDefinitivnyVrchol(boolean aDefinitivnyVrchol) {
            this.aDefinitivnyVrchol = aDefinitivnyVrchol;
        }

        public double getaDoterazNajdenaNajkratsiaCesta() {
            return aDoterazNajdenaNajkratsiaCesta;
        }

        public void setaDoterazNajdenaNajkratsiaCesta(double aDoterazNajdenaNajkratsiaCesta) {
            this.aDoterazNajdenaNajkratsiaCesta = aDoterazNajdenaNajkratsiaCesta;
        }

        public Vrchol getaVrcholZKtorehoSomPrisiel() {
            return aVrcholZKtorehoSomPrisiel;
        }

        public void setaVrcholZKtorehoSomPrisiel(Vrchol aVrcholZKtorehoSomPrisiel) {
            this.aVrcholZKtorehoSomPrisiel = aVrcholZKtorehoSomPrisiel;
        }

        public RoadPoint getaBodPrisluchajuciVrcholu() {
            return aBodPrisluchajuciVrcholu;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Vrchol)) return false;

            Vrchol vrchol = (Vrchol) o;

            if (!aBodPrisluchajuciVrcholu.equals(vrchol.aBodPrisluchajuciVrcholu)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return aBodPrisluchajuciVrcholu.hashCode();
        }

        @Override
        public String toString() {
            return "Vrchol{" +
                    "aBodPrisluchajuciVrcholu=" + aBodPrisluchajuciVrcholu +
                    ", aDefinitivnyVrchol=" + aDefinitivnyVrchol +
                    ", aVrcholZKtorehoSomPrisiel=" + aVrcholZKtorehoSomPrisiel +
                    '}';
        }
    }

}

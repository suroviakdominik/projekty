.MODEL small
.STACK 100h
.DATA
PocetRiadkovMatice DW  4 
PocetStlpcovMatice DW 4

Riadok1 DW 0,2,3,4
Riadok2 DW 5,6,-7,8
Riadok3 DW 9,10,11,12
Riadok4 DW 13,14,-112,16

;.......potrebne k vypisu realnehoo cials.....
MaxCislic EQU 100
CisloBCD DB MaxCislic DUP (0)
IOBuffer DB 5 dup(0)
;..............................................

VysledokPriemerCiselDiagonaly DD ?
SucetDiagonaly DW 'n'


errorSucDiagonalyPrekrocilRozsahTypuWord DB 'Sucet clenov hlavnej diagonaly sa nezmesti do definovaneho typu! $' 
errorSucClenovPrekrocilRozsahTypuWord DB 'Sucet clenov matice sa nezmesti do definovaneho typu! $'
errorPriemerDiagonaly DB 'Priemer diagonaly nemozno zistit, lebo sucet diagonaly nie je vypocitany. Volat Proc SucDiagonaly! $'

VypisPriemerDiagonaly DB 'Priemer diagonaly: $'
VypisSucDiagonaly DB 'Sucet hlavnej diagonaly je: $'
VypisSucClenov DB 'Sucet clenov matice je: $'
VypisMinimimMatice DB 'Minimum: $'
VypisRiadokMinima DB 'Riadok minima: $'
VypisStlpecMinima DB 'Stlpec minima: $'
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
DesiatPriNas DW 10
Postupnost DW 15 dup(?)
dlzkaPostupnost DW (?)
ZapisanyZnakDoSuboru DB (?)
Ano DW 'A'
Nie DW 'N'
MenoSuboru DB 'TextSub.txt',0 
PopisovacSuboru DW (?)
;--------------------------------------------------------------------------PROCEDURA NACITA CISLO S TERMINALU, SKONVERTUJE Z ASCII NA CISLO A ULOZI DO REGISTRA AX----------------------------------                                                                                                               
.CODE
.386
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 
;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo
                                                                                                                         
Write ENDP

;-------------------------------------------------------------------------------------PROCEDURA ZAPIS ZNAKU Z REGISTRA AX DO SUBORU---------------------------------------------------------------------------
WriteFile PROC 
      push dx
      push cx
      push ax
      mov cx,0
          
     
    
     cmp ax,0
     jl VypisMinu
     
     
     KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        loop VypisCisl
        
       mov di,0
       pop ax
       pop cx
       pop dx
       ret

     VypisMinu:
        push ax
        mov al,'-'
        mov bx,PopisovacSuboru
        mov cx,1                                                                           
        mov ZapisanyZnakDoSuboru,al                                                                                           
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        
        pop ax
        neg ax
        jmp KonvertujNaDekCisl

WriteFile ENDP
;-----------------------------------------------------PROCEDURA VYPISE MATICU DEFINOVANU PO RIADKOCH V DATOVOM SEGMENTE NA TERMINAL----------------
VypisMatice PROC
      push ax
      push bx
      push cx
      push dx
      push di
     
      mov cx,PocetRiadkovMatice                                                            
      mov di,0
      mov bx,2
      CyklusPrechodRiadkami:
             push cx
             mov cx,PocetStlpcovMatice
             
          CyklusPrechodStlpcami:
             mov ax,[Riadok1+di]
             call Write
             mov dl,09h;vypise tabulator vodorovne
             mov ah,2
             int 21h
             add di,bx
           loop CyklusPrechodStlpcami                                                                                                           
        pop cx
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h
        
      loop CyklusPrechodRiadkami
      
      pop di
      pop dx
      pop cx
      pop bx 
      pop ax
      ret
VypisMatice ENDP             
;----------------------------------------------------PROCEDURA VYPOCITA SUCET HLAVNEJ DIAGONALY MATICE-------------------------------------------------          
SucetHlavnejDiagonaly PROC
      push ax
      push bx
      push cx
      push dx
      push di
      push si
      
      mov cx,PocetRiadkovMatice
      mov di,0
      mov si,2
      mov dx,cx
      shl dx,1
      add dx,si
      
      mov ax,0
      
      Cyklus:
         add ax,[Riadok1+di]
         jo Chyba;ak OF=1,to je bol prekroceny rozsah nami deklarovaneho typu vyhodi chybu
         add di,dx
      loop Cyklus
      push ax
      mov dx,offset VypisSucDiagonaly
      mov ah,9
      int 21h
      pop ax
      mov SucetDiagonaly,ax
      call Write
      
      mov DL,0AH;uloz do registra posun dole
      mov AH,2;do registra AH uloz cislo sluzby
      int 21h
      
      pop si
      pop di
      pop dx
      pop cx
      pop bx
      pop ax
      ret
    Chyba:
      mov dx,offset errorSucDiagonalyPrekrocilRozsahTypuWord
      mov ah,9
      int 21h
      mov DL,0AH;uloz do registra posun dole
      mov AH,2;do registra AH uloz cislo sluzby
      int 21h

      pop si
      pop di
      pop dx
      pop cx
      pop bx
      pop ax
      ret
SucetHlavnejDiagonaly ENDP
;----------------------------------------------------PROCEDURA VYPOCITA SUCET CLENOV MATICE--------------------------------------------
SucetClenovMatice PROC  
      push ax      
      push cx
      push di
    
      mov cx,PocetRiadkovMatice
      mov ax,0
      mov di,0
     
      CyklusRiadky:
          push cx
          mov cx,PocetStlpcovMatice 
           CyklusStlpce:
                 add ax,[Riadok1+di]
                 jo ChybaSucClenov;;ak OF=1,to je ak sucet prekrocil rozsah nami deklarovaneho typu vyhodi chybu
                 inc di
                 inc di
                 
           loop CyklusStlpce
           
       pop cx
       loop CyklusRiadky
           
 
      push ax 
      mov dx,offset VypisSucClenov
      mov ah,9
      int 21h
      
      pop ax 
      call Write
      Kon: mov DL,0AH;uloz do registra posun dole
           mov AH,2;do registra AH uloz cislo sluzby
           int 21h
           pop di
           pop cx
           pop ax
           ret
      
      ChybaSucClenov:
      pop cx;
      mov dx,offset errorSucClenovPrekrocilRozsahTypuWord
      mov ah,9
      int 21h
      jmp Kon 
      
SucetClenovMatice ENDP
;-----------------------------------------------------------------------PROCEDURA VYPISE SURADNICE MINIMALNEHO CLENA MATICE A HODNOTU MINIMA----------------------------------------------------------------------------
MimimumMatice PROC
     push ax
     push bx
     push cx 
     push dx
     push di
     push si
     push bp
     
    
      mov cx,PocetRiadkovMatice
      mov ax,Riadok1
      mov di,2
      mov si,0
      mov bx,0
        CyklusRiad:
          mov bp,0
          push cx
          mov cx,PocetStlpcovMatice 
          mov dx,0
           CyklusStlp:
                 cmp [Riadok1+di],ax
                 jl ZmenMinPrv
                 inc di
                 inc di
                 inc dx
                 inc dx 
                 jmp NextPow
                 ZmenMinPrv:
                     mov ax,[Riadok1+di]
                     shr dx,1
                     mov bx,dx
                     add bx,2
                     shl dx,1
                     inc dx
                     inc dx                            
                     inc di
                     inc di
                     mov bp,1
                 NextPow:
                 loop CyklusStlp
       pop cx 
       cmp bp,1
       jne NextRow
       mov si,cx
       NextRow:    
         loop CyklusRiad
       
       push ax
       mov dx,offset VypisMinimimMatice
       mov ah,9
       int 21h
       pop ax
       call Write;vypise minimum
       mov dl,09h; vypise horizontalny tabulator
       mov ah,2
       int 21h
       
       mov dx,offset VypisRiadokMinima
       mov ah,9
       int 21h
       mov cx,PocetRiadkovMatice
       sub cx,si
       add cx,1
       mov ax,cx
       call Write;vypise riadok v ktorom sa nachadza minimum
       mov dl,09h; vypise horizontalny tabulator
       mov ah,2
       int 21h
       
       mov dx,offset  VypisStlpecMinima
       mov ah,9
       int 21h
       mov ax,bx;vypise stlpecVKtoromSanachadzaminimum
       call Write
       
       mov DL,0AH;uloz do registra posun dole
       mov AH,2;do registra AH uloz cislo sluzby
       int 21h
       pop bp
       pop si
       pop di
       pop dx
       pop cx
       pop bx
       pop ax
       ret
       
MimimumMatice ENDP
;---------------------------------------------------------PROCEDURA NA VYPIS REALNEHO CISLA DO TERMINALU-----------------------------------------
Int16 PROC
; vypis celeho cisla typu word
; cislo je v ax
; modifikuje ax, bx, cx, dx, di
	mov bx, offset IOBuffer
	mov di,0
	mov cx,10
Cyklu:	mov dx,0
	div cx
	mov [bx+di], dl
	inc di
	cmp ax,0
	jnz Cyklu
Vypis:	mov dl,[bx+di-1]
	or dl, 30h
	mov ah,2
	int 21h
	dec di
	cmp di,0
	jnz Vypis
	ret
Int16 ENDP

Mantisa	PROC
; prevedie des. cast Cisla do BCD kodu v premennej CisloBCD, kde najvyssia cislica je pred des. ciarkou
; v Cislo zostane cela cast
; vstupny parameter: si je pocet miest mantisy
; vystupny parameter: si je pocet BCD cislic
; modifikuje ax, bx, cx, dx, di
	mov dl,2
	mov bx,1; kolko cislic BCD uz mam
DalsiBitMantisy:
	mov di, offset CisloBCD

	; skopiruj dalsiu cislicu mantisy do najvyssej BCD cislice
	shr Cislo,1
	adc byte ptr [di],0

	; vydel BCD cislo dvoma
	mov cx,bx
	mov ah,0
Delenie:	mov al,[di]
	aad; des. uprava ax
	div dl
	mov [di],al
	inc di
	loop Delenie
	mov [di],ah; odloz posledny zvysok
	inc bx
	dec si
	jnz DalsiBitMantisy
	dec bx
	mov si, bx
	ret
Mantisa	ENDP

Real32 PROC pascal Cislo:dword
	mov cx,word ptr [Cislo+2]
	; vypis minus, ak ide o zaporne cislo
	test cx,8000h
	jz Kladne
	mov dl,'-'
	mov ah, 2
	int 21h
Kladne:	; vyber exponent
	and cx,0111111110000000b
	shr cx,7
	sub cx,127; cx je exponent
	; vynuluj znamienko a exponent
	mov ebx,Cislo
	and ebx,007FFFFFh
	; pridaj najvyssi bit mantisy
	or ebx,00800000h;
	mov Cislo, ebx
	mov si,23; pocet bitov mantisy
	; posun des. ciarku o exponent
	sub si,cx
	call Mantisa; prevedie des. cast Cislo do BCD kodu, v Cislo zostane cela cast, v si pocet BCD cislic
	mov ax,word ptr [Cislo]
	call int16; vypise celu cast
	mov dl,'.'
	mov ah,2
	int 21h
; vypis mantisy
	mov di, offset CisloBCD
	inc di
	dec si
	mov cx,si
VypisM:	mov dl,[di]
	or dl,30h
	mov ah,2
	int 21h
	inc di
	loop VypisM
	ret 
Real32 ENDP


;---------------------------------------------------------------HLAVNY PROGRAM--------------------------------------------------------
Zaciatok:
       mov ax,@DATA
       mov ds,ax
       
       call VypisMatice
       call  SucetHlavnejDiagonaly
       call SucetClenovMatice
       call MimimumMatice 
       
       cmp SucetDiagonaly,'n'
       jne PriemerDiagonaly
       jmp VypChyba
       PriemerDiagonaly:
           fild PocetRiadkovMatice
           fild SucetDiagonaly
           fdiv st(0),st(1)
           fstp VysledokPriemerCiselDiagonaly
           push VysledokPriemerCiselDiagonaly
           mov dx, offset VypisPriemerDiagonaly
           mov ah,9
           int 21h
           call Real32
           jmp Koniec
       VypChyba:
           mov dx, offset errorPriemerDiagonaly
           mov ah,9
           int 21h
Koniec:
       mov ah,4Ch
       int 21h

END Zaciatok
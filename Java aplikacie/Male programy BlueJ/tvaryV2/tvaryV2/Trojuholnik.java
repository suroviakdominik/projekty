import java.awt.*;

/**
 * Rovnoramenn� trojuholn�k, s ktor�m mo�no pohybova� a nakresl� sa na pl�tno.
 * 
 * @author  Michael Kolling and David J. Barnes
 * @version 1.0  (15 July 2000)
 */

public class Trojuholnik
{
    private int aVyska;
    private int aZakladna;
    private int aXLavyHorny; // lavy horny bod oblasti
    private int aYLavyHorny; // lavy horny bod oblasti
    private String aFarba;
    private boolean aJeViditelny;

    /**
     * Vytvor nov� rovnoramenn� trojuholn�k preddefinovanej farby na preddefinovanej poz�cii.
     */
    public Trojuholnik()
    {
        aVyska = 30;
        aZakladna = 40;
        aXLavyHorny = 50;
        aYLavyHorny = 15;
        aFarba = "green";
        aJeViditelny = false;
    }

    /**
     * (Trujuholn�k) Zobraz sa.
     */
    public void zobraz()
    {
        aJeViditelny = true;
        nakresli();
    }
    
    /**
     * (Trujuholn�k) Skry sa.
     */
    public void skry()
    {
        zmaz();
        aJeViditelny = false;
    }
    
    /**
     * (Trujuholn�k) Posu� sa vpravo o pevn� d�ku.
     */
    public void posunVpravo()
    {
        posunVodorovne(20);
    }

    /**
     * (Trujuholn�k) Posu� sa v�avo o pevn� d�ku.
     */
    public void posunVlavo()
    {
        posunVodorovne(-20);
    }

    /**
     * (Trujuholn�k) Posu� sa hore o pevn� d�ku.
     */
    public void posunHore()
    {
        posunZvisle(-20);
    }

    /**
     * (Trujuholn�k) Posu� sa dole o pevn� d�ku.
     */
    public void posunDole()
    {
        posunZvisle(20);
    }

    /**
     * (Trujuholn�k) Posu� sa vodorovne o d�ku dan� parametrom.
     */
    public void posunVodorovne(int paVzdialenost)
    {
        zmaz();
        aXLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Trujuholn�k) Posu� sa zvisle o d�ku dan� parametrom.
     */
    public void posunZvisle(int paVzdialenost)
    {
        zmaz();
        aYLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Trujuholn�k) Posu� sa pomaly vodorovne o d�ku dan� parametrom.
     */
    public void pomalyPosunVodorovne(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aXLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Trujuholn�k) Posu� sa pomaly zvisle o d�ku dan� parametrom.
     */
    public void pomalyPosunZvisle(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aYLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Trujuholn�k) Zme� rozmery v��ky a z�kladne na hodnoty dan� parametrami.
     * Obe hodnoty musia by� nez�porn� cel� ��sla. 
     */
    public void zmenRozmery(int paVyska, int paZakladna)
    {
        zmaz();
        aVyska = paVyska;
        aZakladna = paZakladna;
        nakresli();
    }


    /**
     * (Trujuholn�k) Zme� farbu na hodnotu dan� parametrom.
     * Nazov farby mus� by� po anglicky.
     * Mo�n� farby s� tieto:
     * �erven� - "red"
     * �lt�    - "yellow"
     * modr�   - "blue"
     * zelen�  - "green"
     * fialov� - "magenta"
     * �ierna  - "black"
     * biela - "white"
     * hned� - "brown"
     */
    public void zmenFarbu(String paFarba)
    {
        aFarba = paFarba;
        nakresli();
    }

    /*
     * Draw the triangle with current specifications on screen.
     */
    private void nakresli()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            int[] xpoints = { aXLavyHorny, aXLavyHorny + (aZakladna/2), aXLavyHorny - (aZakladna/2) };
            int[] ypoints = { aYLavyHorny, aYLavyHorny + aVyska, aYLavyHorny + aVyska };
            canvas.draw(this, aFarba, new Polygon(xpoints, ypoints, 3));
            canvas.wait(10);
        }
    }

    /*
     * Erase the triangle on screen.
     */
    private void zmaz()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

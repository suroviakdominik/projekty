

public class Auto
{
    private String aZnacka;
    private String aFarba;
    private String aTypPaliva;
    private double aObjemMotora;
    
    public Auto()
    {
        aZnacka="BMW";
        aFarba="Cierna";
        aTypPaliva="benzin";
        aObjemMotora=2.5;
    }
    
    public Auto(Auto paAuto)
    {
        aZnacka=paAuto.aZnacka;
        aFarba=paAuto.aFarba;
        aTypPaliva=paAuto.aTypPaliva;
        aObjemMotora=paAuto.aObjemMotora;
    }
    
    public Auto(String paZnacka,
                String paFarba,
                String paTypPaliva,
                double paObjemMotora)
    {
        aZnacka=paZnacka;
        aFarba=paFarba;
        aTypPaliva=paTypPaliva;
        aObjemMotora=paObjemMotora;
    }

    public void onString()
    {
        String vysledok;
        vysledok="Znacka: "+aZnacka;
        vysledok+=" Farba: "+aFarba;
        vysledok+=" Palivo: "+aTypPaliva;
        vysledok+=" Objem Motora: " +aObjemMotora;
        System.out.println(vysledok);
    }

    public void zmenFarbu(String paFarba)
    {
        aFarba=paFarba;
    }
}    

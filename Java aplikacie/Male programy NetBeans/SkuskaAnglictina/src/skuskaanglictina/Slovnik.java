/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skuskaanglictina;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Dominik
 */
public class Slovnik implements Serializable{
    private HashMap<String,String> aSlovaVSlovniku;

    public Slovnik() {
        aSlovaVSlovniku=new HashMap<>();
    }
    
    public void pridajSlovoDoSlovnika(String paENG,String paSK)
    {
        aSlovaVSlovniku.put(paENG, paSK);
    }
    
    public String getPreklad(String paEng)
    {
        return aSlovaVSlovniku.get(paEng);
    }
    
    public String getNahodneAnglickeSlovo()
    {
        Random r=new Random();
        String[] keys=new String[aSlovaVSlovniku.keySet().size()];
        aSlovaVSlovniku.keySet().<String>toArray(keys);
        return keys[r.nextInt(keys.length-1)];
        
    }
}

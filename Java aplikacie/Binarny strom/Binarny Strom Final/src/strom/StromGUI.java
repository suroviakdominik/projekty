package strom;

import Objekty.*;
import java.awt.*;
import java.awt.event.*;//musime importovat preto lebo podbalicky sa neimportuju
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Trieda predstavujúca okna aplikácie.
 *
 * @author suroviak3
 */
public class StromGUI extends JFrame {

    private Strom strom;
    private int aPoziciaOknaX;
    private int aPoziciaOknaY;
    private IObjekt aVkladanyObjekt;

    /**
     * Konštruktor vytvóri okno programu. Inicializuje jeho prvky ako
     * tlačidlá,Menu...
     */
    public StromGUI() {
        this.strom = new Strom();

        //this.setLocation( 100, 100 );//umiestnenie okna
        // this.setSize( 800, 500 );//a velkost okna,ale na tvrdo,nemozno menit rozmery
        //nahradime inym kodom
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension d = t.getScreenSize();//zisti maximalne rozlisenie obrazovky
        //  System.out.println("DPI = " + t.getScreenResolution() + "  *  Screen : " + d.width + " x " + d.height );    

        final int OKNO_CAST = 80;                         // percento z celej plochy obrazovky-urci velkost okna podla maximalneho rozlisenia nasej obrazovky
        this.setLocation(d.width * (100 - OKNO_CAST) / 100 / 2, d.height * (100 - OKNO_CAST) / 100 / 2); //umiestnenie okna//height,width -rozlisenie obrazovky na danom pc
        this.setSize(d.width * OKNO_CAST / 100, d.height * OKNO_CAST / 100);//velkost okna
        aPoziciaOknaX = d.width * (100 - OKNO_CAST) / 100 / 2;
        aPoziciaOknaY = d.height * (100 - OKNO_CAST) / 100 / 2;
        //  umozni ihned ukoncenie cez ikonu okna     
        //  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//nespyta sa nas ci chceme ukoncit  
        // ukoncenie cez udalost WindowEvent,  ak bude prihlasena        
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        //  prihlasenie  WindowEvent                                                                                    
        this.addWindowListener(new WindowAdapter() {//Windows Adapter je anonymna trieda                                 
            @Override
            public void windowClosing(WindowEvent e) {
                akcia_koniec();
            }   //Windows Event je anonymna trieda              
        });

        this.setLayout(new BorderLayout(2, 2));      // volba pre rozmiestnenie tlacitkovej listy a platna//parametre predstavuju zonu kde to bude v border layout
        this.add(BorderLayout.CENTER, new StromPlatno(strom));//platno sa automaticky prekresluje pri zmene rozmerov
        //OVLADANIE KLAVESNICOU
        Klavesnica klav = new Klavesnica();
        this.addKeyListener(klav);

        ///////////////////////////////////////////////////////////////OVLADANIE  TLACIDLAMI////////////////////////////////////////////////////////////////////
        //vytvoríme pomocnú lištu,  ktorú   posadíme   na    SEVER     hlavného formulára
        //a na lištu budeme sádzať jednotlivé tlačidlá,  ku ktorým prihlásime potrebne udalosti
        //najprv lista
        //  lista,  butoniky v strede
        JPanel pnLista = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //  lista bude hore
        pnLista.setBackground(Color.LIGHT_GRAY);
        this.add(BorderLayout.NORTH, pnLista);

        //----------------------------Tlacidlo Zrus Strom------------------------------//
        JButton btPrKnih = new JButton("Zrus strom"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrKnih.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zrusenie();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrKnih.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btPrKnih);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Zrus Strom Koniec---------------------//

        //----------------------------Tlacidlo Citaj Aktualny------------------------------//
        JButton btCitajAktualny = new JButton("Citaj aktualny"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajAktualny.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_citajAktualny();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajAktualny.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btCitajAktualny);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Citaj Aktualny Koniec---------------------//

        //----------------------------Tlacidlo Je Koren------------------------------//
        JButton btJeKoren = new JButton("Je aktualny Korenom"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btJeKoren.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_jeKoren();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btJeKoren.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btJeKoren);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Je Koren Koniec---------------------//

        //----------------------------Tlacidlo Je Koren------------------------------//
        JButton btJeList = new JButton("Je Aktualny Listom"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btJeList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_jeList();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btJeList.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btJeList);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Je Koren Koniec---------------------//

        //----------------------------Tlacidlo Pridanie Korena------------------------------//
        JButton btPridajKoren = new JButton("Pridaj Koren"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPridajKoren.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieKorena();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPridajKoren.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btPridajKoren);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Pridanie Korena Koniec---------------------//

        //----------------------------Tlacidlo Pridanie Laveho Listu------------------------------//
        JButton btPridajLavList = new JButton("Pridaj Lavy List"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPridajLavList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieLavehoListu();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPridajLavList.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btPridajLavList);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Pridanie Laveho Listu Koniec---------------------//

        //----------------------------Tlacidlo Pridanie Praveho Listu------------------------------//
        JButton btPridajPravyList = new JButton("Pridaj Pravy List"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPridajPravyList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pridaniePravehoListu();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPridajPravyList.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btPridajPravyList);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Pridanie Praveho Listu Koniec---------------------//

        //----------------------------Tlacidlo Odobranie Laveho Listu------------------------------//
        JButton btOdobLavyList = new JButton("Odober Lavy List"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdobLavyList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_odobranieLavehoListu();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdobLavyList.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btOdobLavyList);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Odobranie Laveho Listu Koniec---------------------//

        //----------------------------Tlacidlo Odobranie Praveho Listu------------------------------//
        JButton btOdobPravyList = new JButton("Odober Pravy List"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdobPravyList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_odobraniePravehoListu();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdobPravyList.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnLista.add(btOdobPravyList);              	//  pridanie na listu                        
        //--------------------------------Tlacidlo Odobranie Praveho Listu Koniec---------------------//

        ///////////////////////////////////////////////////////////////////////HORNE MENU//////////////////////////////////////////////////////////////////////////
        //Ovládanie  pomocou  menu
        //musíme vytvoriť   “neviditeľnú lištu pre menu”   a   priradiť ju pre nášmu formulár
        //  lista pre horne menu
        JMenuBar mb = new JMenuBar();
        this.setJMenuBar(mb);

        //a potom  popridávať jednotlivé  hlavne položky menu
        //JMenu su vodorovne polozky horneho menu
        //JItem su zvisle polozky jednotlivych vodorovnych poloziek horneho menu
        //  polozky menu na hornej liste
        JMenu mnSubor = new JMenu("Súbor");
        mb.add(mnSubor);

        JMenuItem mniSubor_Koniec = new JMenuItem("Koniec programu");      // polozka v menu
        mniSubor_Koniec.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_koniec();
            }     // 
        });
        mnSubor.add(mniSubor_Koniec);

        //dalsia polozka menu na hornej liste
        JMenu mStrom = new JMenu("Strom");
        mb.add(mStrom);

        JMenuItem mniZrusenie = new JMenuItem("Zrusenie");
        mniZrusenie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zrusenie();
            }
        });
        mStrom.add(mniZrusenie);

        mStrom.addSeparator(); //prida oddelovac zvislych poloziek          

        //dalsia polozka menu na hornej liste    
        JMenu mnPomoc = new JMenu("Pomoc");
        mb.add(mnPomoc);

        JMenuItem mniPomoc_Pomoc = new JMenuItem("Pomoc");
        mniPomoc_Pomoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pomoc();
            }
        });

        JMenuItem mniSubor_citZozSuboru = new JMenuItem("Nacitaj zo suboru");      // polozka v menu
        mniSubor_citZozSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_nacitajZoSuboru();

            }     // 
        });
        mnSubor.add(mniSubor_citZozSuboru);
        mnSubor.addSeparator();

        JMenuItem mniSubor_zapisDoSuboru = new JMenuItem("Zapis do suboru");      // polozka v menu
        mniSubor_zapisDoSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_zapisDoSuboru();

            }     // 

        });
        mnSubor.add(mniSubor_zapisDoSuboru);
        mnSubor.addSeparator();

        mnPomoc.add(mniPomoc_Pomoc);

        mnPomoc.addSeparator();							//  separator

        JMenuItem mniPomoc_Info = new JMenuItem("O programe");
        mniPomoc_Info.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_info();
            }
        });
        mnPomoc.add(mniPomoc_Info);

        this.setResizable(true);//aby sme mohli zmenit velkost okna potiahnutim mysou
        this.setTitle("Údajová štruktŕa STROM");//nadpis hlavneho okna nasho problemu
        this.setVisible(true);
        this.toFront();//okno zviditelnime a dame ho navrch okien   

    }

    //--------------------------------------------Pridavanie objektu do ADT-------------------------------
    /**
     * Metóda zobrazí výzvu na vybranie objektu typu IObjekt ak vyoknávame
     * nejakú akciu, ktorá má objekt ako vstupný parameter
     */
    private void oknoButtonVyberObjektu() {
        final JDialog ramik = new JDialog(); //okno
        ramik.setLayout(new FlowLayout(FlowLayout.CENTER));

        final JButton btPridajKnihu = new JButton("Pridaj Knihu"); //tlacitku s nazvem

        btPridajKnihu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieKnihy();
                ramik.dispose();
            }

        });

        ramik.add(btPridajKnihu);

        final JButton btPridajAutomobil = new JButton("Pridaj Automobil"); //tlacitku s nazvem
        btPridajAutomobil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieAutomobilu();
                ramik.dispose();
            }
        });

        ramik.add(btPridajAutomobil);

        final JButton btPridajOsobu = new JButton("Pridaj Osobu"); //tlacitku s nazvem
        btPridajOsobu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieOsoby();
                ramik.dispose();
            }
        });

        ramik.add(btPridajOsobu);

        ramik.pack(); //prizpusob velikost okna
        ramik.setLocation(aPoziciaOknaX + this.getWidth() / 2 - ramik.getWidth() / 2, aPoziciaOknaY + this.getHeight() / 2 - ramik.getHeight() / 2); //levy horni roh bude na souradnici [100, 100]
        ramik.setModal(true);
        ramik.setVisible(true); //zobraz okno   

    }

    /**
     * Metóda vytvóri nový objekt typu Osoba a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieOsoby() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie osoby' ");
        pridanie(new Osoba());//priadenie novej osoby do evidencie
        this.repaint();
    }

    /**
     * Metóda vytvóri nový objekt typu Kniha a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieKnihy() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie knihy' "); 
        pridanie(new Kniha());
        this.repaint();
    }

    /**
     * Metóda vytvóri nový objekt typu Automobil a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieAutomobilu() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie automobilu' ");
        pridanie(new Automobil());
        this.repaint();
    }

    /**
     * Metóda nastaví hodnotu atribútu aVkladanýObjekt na hodnotu prijatú ako
     * parameter
     *
     * @param paObjekt:IObjekt - objekt ktorý sa priradí atribútu
     */
    private void pridanie(IObjekt paObjekt) {
        if (paObjekt.napln() == true) {
            aVkladanyObjekt = paObjekt;
        } else {
            aVkladanyObjekt = null;
        }

    }

    //////////////////////////////////////////////////////AKCIE, KTORE DOKAZE PROGRAM VYKONAT/////////////////////////////////////////////////
    /**
     * Metóda vypíše základné info o programe.
     */
    private void akcia_info() // info()vola sa pri klknuti na pomoc
    {
        JOptionPane.showMessageDialog(this,
                "                                                                   "
                + "                                                                   \n\n"
                + "GUI : Jozef Kopecký \n"
                + "Program: Dominik Suroviak\n\n",
                "Údajová štruktúra STROM  -  O programe", JOptionPane.INFORMATION_MESSAGE);//nadpis okna akcie info 
        this.repaint();
    }

    /**
     * metóda zobrazí základné ovládacie prvky programu
     */
    private void akcia_pomoc() // pomoc()
    {
        JOptionPane.showMessageDialog(this,
                "                                                                   "
                + "                                                                   \n\n"
                + "Z           Zrusenie ........ \n"
                + "H           Hladanie .... \n"
                + "P           Pridanie pravého listu.... \n"
                + "L           Pridanie ľavého listu .... \n"
                + "K           Odobranie ľavého listu .... \n"
                + "O           Odobranie pravého listu .... \n"
                + "S           Zápis do súboru .... \n"
                + "D           Načítanie .... \n"
                + "R           Zobrazenie aktuálneho .... \n"
                + "šipka hore  Nastav aktuálny na predka aktuálneho .... \n"
                + "šipka vpravo  Nastav aktuálny na pravého syna aktuálneho .... \n"
                + "šipka vľavo   Nastav aktuálny na ľavého syna aktuálneho .... \n"
                + ". . . . \\n\nn"
                + "F1    pomoc\n\n"
                + "Esc   koniec\n\n\n"
                + "\n\n\n\n\n",
                "Údajová štruktúra STROM  -  Pomoc", JOptionPane.INFORMATION_MESSAGE); //Nadpis okna nasho problemu 

        this.repaint();
    }

    private void akcia_citajAktualny() {
        if (!strom.jePrazdny()) {
            try {
                strom.citajAktualny();
            } catch (IOException ex) {
                Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "ADŠ je prázdna. Nie je čo zobraziť.", "ERROR=Zobrazenie aktuálneho", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * metóda na ukončenie aplikácie a zavretir hlavného okna programu.
     */
    private void akcia_koniec() // koniec()
    {
        if (JOptionPane.showConfirmDialog(this,
                "Chceš skutočne ukončit program", "Ukončenie",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            System.exit(0);
        }
        this.repaint();
    }

    //////////////////////////////////////////////////////AKCIE, KTORE DOKAZE PROGRAM VYKONAT/////////////////////////////////////////////////
    /**
     * Metóda predstavuej akciu, ktorá sa vyokná ak v hlavnom menu klikneme na
     * položku súbor a načítaj zo súboru. Na základe atribútu aAktualnyTab
     * rozhodne, ktorý ADT má načítať zo súboru
     */
    private void akcia_nacitajZoSuboru() {
        try {
            if (JOptionPane.showConfirmDialog(this,
                    "Načitanie prvkov spôsobí vymazanie aktuálnych prvkov. Chcete naozaj načítať prvky zo súboru", "Načitanie zo súboru",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                    == JOptionPane.YES_OPTION) {
                //  this.dispose(); 
                strom.nacitajPrvkyZoSuboru();
            }
            this.repaint();

        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Súbor nebol nájdený. Bol vymazany uzivatelom priamo z adresara kde sa subory ukladaju. ", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        } catch (NoSuchElementException ex) {
            JOptionPane.showMessageDialog(null, "Súbory boli pozmenené. Niektoré súbory boli pozmenené. Nepodarilo sa načítať všetky prvky. ", "ERROR=pozmeneny subor", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metóda predstavuej akciu, ktorá sa vyokná ak v hlavnom menu klikneme na
     * položku súbor a zapíš zo súboru. Na základe atribútu aAktualnyTab
     * rozhodne, ktorý ADT má zapísať zo súboru
     */
    private void akcia_zapisDoSuboru() {
        try {
            if (!strom.jePrazdny()) {
                strom.zapisPrvkyDoSuboru();
                JOptionPane.showMessageDialog(this, "Všetky prvky boli úspešne uložené do súboru.");
            } else {
                JOptionPane.showMessageDialog(null, "ADŠ je prázdna. Nie je čo uložiť.", "ERROR=uloženie ADŠ", JOptionPane.ERROR_MESSAGE);
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR=Zápis do súboru", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * metóda, ktorá zrušíaktuálne vytvorený strom
     */
    private void akcia_zrusenie() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'zrusenie' ");
        if (JOptionPane.showConfirmDialog(this,
                "Chceš skutočne zrušiť strom", "Zrušenie stromu",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            strom.zrus();
        }

        this.repaint();
    }

    /**
     * metóda informuje uživateľa o tom, či je aktuálny prvok Korenom
     */
    private void akcia_jeKoren() {
        try {
            if (strom.jeKorenom()) {
                JOptionPane.showMessageDialog(this, "Aktuálne vyznačený prvok JE koreňom.");
            } else {
                JOptionPane.showMessageDialog(this, "Aktuálne vyznačený prvok NIE JE koreňom.");
            }
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.repaint();
    }

    /**
     * metóda informuje uživateľa o tom, či je aktuálny prvok listom
     */
    private void akcia_jeList() {
        try {
            if (strom.jeListom()) {
                JOptionPane.showMessageDialog(this, "Aktuálne vyznačený prvok JE listom.");
            } else {
                JOptionPane.showMessageDialog(this, "Aktuálne vyznačený prvok NIE JE listom.");
            }
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.repaint();
    }

    /**
     * metóda, ktorá umožní pridať použvateľovi do stromu pravý list
     */
    private void akcia_pridaniePravehoListu() {
        if (!strom.maAktualnyPravyList()) {
            oknoButtonVyberObjektu();
            try {
                if (aVkladanyObjekt != null && strom.hladaj(aVkladanyObjekt) == -1) {
                    if (strom.pridajPravyList(aVkladanyObjekt)) {
                        this.repaint();
                    } else {
                        JOptionPane.showMessageDialog(null, "Sucasny prvok uz ma praveho syna. Nemozno pridat pravy list.", "ERROR=Pridanie praveho listu", JOptionPane.ERROR_MESSAGE);
                    }
                    this.repaint();
                } else if (strom.hladaj(aVkladanyObjekt) != -1) {
                    JOptionPane.showMessageDialog(null, "Objekt, ktory chcete pridat sa uz nachadza v strome, preto ho nemozno pridat", "ERROR=Pridanie praveho listu", JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo pridat. Neda sa ulozit do binarneho suboru.", "ERROR=Pridanie praveho listu", JOptionPane.ERROR_MESSAGE);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo pridat. Neda sa ulozit do binarneho suboru.", "ERROR=Pridanie praveho listu", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Sucasny prvok uz ma praveho syna. Nemozno pridat pravy list.", "ERROR=Pridanie praveho listu", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * metóda, ktorá umožní pridať použvateľovi do stromu ľavý list
     */
    private void akcia_pridanieLavehoListu() {
        if (!strom.maAktualnyLavyList()) {
            try {
                oknoButtonVyberObjektu();
                if (aVkladanyObjekt != null && strom.hladaj(aVkladanyObjekt) == -1) {
                    if (strom.pridajLavyList(aVkladanyObjekt)) {
                        this.repaint();

                    } else {
                        JOptionPane.showMessageDialog(null, "Sucasny prvok uz ma laveho syna. Nemozno pridat lavy list.", "ERROR=Pridanie laveho listu", JOptionPane.ERROR_MESSAGE);
                    }
                    this.repaint();
                } else if (strom.hladaj(aVkladanyObjekt) != -1) {
                    JOptionPane.showMessageDialog(null, "Objekt, ktory chcete pridat sa uz nachadza v strome, preto ho nemozno pridat", "ERROR=Pridanie laveho listu", JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo pridat. Neda sa ulozit do binarneho suboru.", "ERROR=Pridanie laveho listu", JOptionPane.ERROR_MESSAGE);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo pridat. Neda sa ulozit do binarneho suboru.", "ERROR=Pridanie laveho listu", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Sucasny prvok uz ma laveho syna. Nemozno pridat lavy list.", "ERROR=Pridanie laveho listu", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * metóda, ktorá umožní odobrať použvateľovi zo stromu pravý list
     */
    private void akcia_odobraniePravehoListu() {
        try {
            if (strom.odoberPravyList() != null) {
                this.repaint();
            } else {
                JOptionPane.showMessageDialog(null, "Sucasny prvok nema pravy list.", "ERROR=Odobranie praveho listu", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * metóda, ktorá umožní odobrať použvateľovi zo stromu ľavý list
     */
    private void akcia_odobranieLavehoListu() {
        try {
            if (strom.odoberLavyList() != null) {
                this.repaint();
            } else {
                JOptionPane.showMessageDialog(null, "Sucasny prvok nema lavy list.", "ERROR=Odobranie laveho listu", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * metóda, ktorá umoźní používateľovi pridať Koreň.
     */
    private void akcia_pridanieKorena() {
        if (strom.jePrazdny()) {
            oknoButtonVyberObjektu();
            if (aVkladanyObjekt != null && strom.jePrazdny()) {
                try {
                    strom.pridajKoren(aVkladanyObjekt);
                } catch (IOException ex) {
                    Logger.getLogger(StromGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.repaint();
            }
        } else if (!strom.jePrazdny()) {
            JOptionPane.showMessageDialog(null, "Strom uz koren obsahuje.", "ERROR=Pridaj koren", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void akcia_aktualNaPredok() {
        if (strom.set_AktualnyOtecNaPredka()) {
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Aktualny nema ziadneho predka", "ERROR=Nastav Aktualny Na Predka", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void akcia_aktualNaPravySyn() {
        if (strom.set_AktualnyOtecNaPravy()) {
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Aktualny nema praveho syna", "ERROR=Nastav Aktualny Na Praveho Syna", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void akcia_aktualNaLavySyn() {
        if (strom.set_AktualnyOtecNaLavy()) {
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Aktualny nema laveho syna", "ERROR=Nastav Aktualny Na Laveho Syna", JOptionPane.ERROR_MESSAGE);
        }
    }

    private class Klavesnica extends KeyAdapter // vnorena trieda Klavesnica
    {

        public void keyPressed(KeyEvent e) // ked stlacim klavesu, tak vznikne nejaka instancia  triedy KeyEvent a vlozi sa do tato instancia bude parametrom tejto funkcie
        {
            //  System.out.println( "Stisol som " + e.getKeyCode() );   // kontrolny vypis o stisku klavesy

            switch (e.getKeyCode()) {                             // vyber cinnosti podla klavesy

                case KeyEvent.VK_Z:
                    akcia_zrusenie();
                    break;

                case KeyEvent.VK_ESCAPE:
                    akcia_koniec();
                    break;

                case KeyEvent.VK_F1:
                    akcia_pomoc();
                    break;

                case KeyEvent.VK_UP:
                    akcia_aktualNaPredok();
                    break;

                case KeyEvent.VK_RIGHT:
                    akcia_aktualNaPravySyn();
                    break;

                case KeyEvent.VK_LEFT:
                    akcia_aktualNaLavySyn();
                    break;
                case KeyEvent.VK_S:
                    akcia_zapisDoSuboru();
                    break;

                case KeyEvent.VK_D:
                    akcia_nacitajZoSuboru();
                    break;

                case KeyEvent.VK_P:
                    akcia_pridaniePravehoListu();
                    break;

                case KeyEvent.VK_L:
                    akcia_pridanieLavehoListu();
                    break;

                case KeyEvent.VK_O:
                    akcia_odobraniePravehoListu();
                    break;

                case KeyEvent.VK_K:
                    akcia_odobranieLavehoListu();
                    break;

                case KeyEvent.VK_R:
                    akcia_citajAktualny();
                    break;

                // tu budeme pridavat
            } // switch
        }  // keyPressed
    }  // Klavesnica

    public static void main(String[] args) {

        JOptionPane.showMessageDialog(new StromGUI(),
                "\nVitajte ...\n\n( Pomoc zobrazíte stlačením klávesy F1 ) \n\n", "Štart programu", JOptionPane.INFORMATION_MESSAGE);

    }
}

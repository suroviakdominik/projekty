﻿using System.Windows.Forms;

namespace Snake
{
    class DataDialog
    {
        private const int LeftPadding = 50;
        private const int DialogWidth = 300;
        private const int DialogHeight = 200;
        private const string ButtonText = "OK";
        public static DialogResult Result { get; set; }///vrati ci bol dialog ukonceny potvrdenim alebo zrusenim

        /// <summary>
        /// vyskoci dialog zo ziadostou o textovy vstup 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static string showDialog(string text, string caption)
        {
            Result = DialogResult.No;
            Form prompt = new Form();
            prompt.StartPosition = FormStartPosition.CenterParent;
            prompt.Width = DialogWidth;
            prompt.Height = DialogHeight;
            prompt.Text = caption;
            Label textLabel = new Label() { Left = LeftPadding, Top = 20, Text = text };
            TextBox inputBox = new TextBox() { Left = LeftPadding, Top = 50, Width = DialogWidth - 2 * LeftPadding };
            Button confirmation = new Button() { Text = ButtonText, Left = LeftPadding, Width = 100, Top = 80 };
            confirmation.Click += (sender, e) =>
            {
                Result = DialogResult.OK;
                prompt.Close();
            };
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(inputBox);
            prompt.ShowDialog();
            return inputBox.Text;
        }
    }
}

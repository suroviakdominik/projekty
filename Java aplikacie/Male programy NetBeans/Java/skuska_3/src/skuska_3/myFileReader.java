/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package skuska_3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 *
 * @author Hawk
 */
public class myFileReader {

    private String path = "";
    private File file = null;

    public myFileReader(String path) {
        this.path = path;
        this.file = new File(this.path);
    }

    public void WriteFile(String content) {
        try {
            Writer writer = new BufferedWriter(new FileWriter(file,true));
            System.out.println(content);
            content+="––––––––––––––––––––––––––––––––----------\n";
            writer.write(content);
            System.out.println(content);
            writer.close();
        } catch(IOException ioe) {
            System.out.println("Writing in to the file failed...PROBLEM?");
        }
        System.out.println("Your file has been written");
    }

    public ArrayList<String> CountLines() {
        ArrayList<String> riadky = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               riadky.add(str);
            }
            in.close();
        } catch (IOException e) {}
        return riadky;
    }
}


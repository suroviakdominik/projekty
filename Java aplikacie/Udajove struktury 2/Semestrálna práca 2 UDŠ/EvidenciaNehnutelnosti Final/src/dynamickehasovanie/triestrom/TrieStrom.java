/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.triestrom;

import dynamickehasovanie.data.Zaznam;
import dynamickehasovanie.struktura.Blok;
import dynamickehasovanie.struktura.FileWorker;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class TrieStrom {

    private int MAXIMALNA_HLBKA_TRIE_STROMU = Integer.BYTES * 8 - 1;//pretoze hasovacia funkcia zaznamu vracia int a indexujem od 0 -> -1

    private Vrchol aKoren;

    public TrieStrom(int paMaxHlbka) {
        aKoren = null;
        MAXIMALNA_HLBKA_TRIE_STROMU = paMaxHlbka < MAXIMALNA_HLBKA_TRIE_STROMU ? paMaxHlbka : MAXIMALNA_HLBKA_TRIE_STROMU;
    }

    private TrieStrom() {

    }

    public Vrchol getKoren() {
        return aKoren;
    }

    public int getMAXIMALNA_HLBKA_TRIE_STROMU() {
        return MAXIMALNA_HLBKA_TRIE_STROMU;
    }

    public void setKoren(Vrchol paVrchol) {
        aKoren = paVrchol;
    }

    public ExternyVrchol traverzuj(int paPodlaBitovehoRetazca) {
        Vrchol skum = aKoren;
        int pom;
        int pom2 = 1;//aby som vedel zistit najmensi bit po posune v cisle podla ktoreho traverzujem
        for (int i = 0; i < MAXIMALNA_HLBKA_TRIE_STROMU; i++) {
            pom = paPodlaBitovehoRetazca >>> i;
            pom = pom & pom2;
            if (pom > 0 && skum.aPravySyn != null) {
                skum = skum.aPravySyn;
            } else if (pom <= 0 && skum.aLavySyn != null) {
                skum = skum.aLavySyn;
            } else {
                return (ExternyVrchol) skum;
            }
        }
        return (ExternyVrchol) skum;
    }

    public int getPocetPrvkov() {
        int ret = 0;
        if (aKoren == null) {
            return 0;
        }
        List<Vrchol> front = new LinkedList<>();
        front.add(aKoren);
        int spracHlbka = 0;
        while (front.size() > 0) {
            Vrchol spracVrchol = front.get(0);
            ret++;
            if (spracVrchol.getaLavySyn() != null) {
                front.add(spracVrchol.getaLavySyn());
            }
            if (spracVrchol.getaPravySyn() != null) {
                front.add(spracVrchol.getaPravySyn());
            }
            front.remove(0);
        }
        return ret;
    }

    public static TrieStrom vytvorTrieStromZoSuboru(File paSuborZTrieStromom) {
        TrieStrom ret = new TrieStrom();
        FileWorker fw = new FileWorker(paSuborZTrieStromom);
        try {
            LinkedList<Vrchol> spracVrcholy = new LinkedList<>();
            fw.startRead();
            String vrcholyVRiadku[] = null;
            ret.MAXIMALNA_HLBKA_TRIE_STROMU = Integer.parseInt(fw.readNextLine());
            String loadedLine;
            long cisloBloku;
            long pocetPlatnychZaznamov;
            loadedLine = fw.readNextLine();
            if (loadedLine == null) {
                return ret;
            } else if (loadedLine.trim().matches("1(\\|\\|\\d+){2}")) {
                loadedLine = loadedLine.trim();
                cisloBloku = Long.parseLong(loadedLine.split("\\|\\|")[1]);
                pocetPlatnychZaznamov = Long.parseLong(loadedLine.split("\\|\\|")[2]);
                ret.aKoren = new ExternyVrchol(0, null, cisloBloku, pocetPlatnychZaznamov);
                spracVrcholy.add(ret.aKoren);
            }else{
                ret.aKoren = new Vrchol(0, null);
                spracVrcholy.add(ret.aKoren);
            }
            boolean somNaKonciRiadku = true;
            int posunVRamciRiadku = 0;
            Vrchol spracVrchol;
            Vrchol lavySynSprac;
            Vrchol pravySynSprac;
            while (spracVrcholy.size() > 0) {
                spracVrchol = spracVrcholy.getFirst();
                if (somNaKonciRiadku) {
                    try {
                        if ((loadedLine = fw.readNextLine()) != null) {
                            vrcholyVRiadku = loadedLine.split("\\s");
                        } else {
                            return ret;
                        }
                        posunVRamciRiadku = 0;
                        somNaKonciRiadku = false;
                    } catch (IOException ex) {
                        Logger.getLogger(TrieStrom.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                //nacitaj lavy syn
                if (vrcholyVRiadku[posunVRamciRiadku].matches("1(\\|\\|\\d+){2}") || vrcholyVRiadku[posunVRamciRiadku].matches("1\\|\\|-\\d+\\|\\|\\d+")) {
                    cisloBloku = Long.parseLong(vrcholyVRiadku[posunVRamciRiadku].split("\\|\\|")[1]);
                    pocetPlatnychZaznamov = Long.parseLong(vrcholyVRiadku[posunVRamciRiadku].split("\\|\\|")[2]);
                    lavySynSprac = new ExternyVrchol(spracVrchol.getaHlbkaVrchola() + 1, spracVrchol, cisloBloku, pocetPlatnychZaznamov);
                    lavySynSprac.setaOtec(spracVrchol);
                    spracVrchol.setaLavySyn(lavySynSprac);
                } else {
                    lavySynSprac = new Vrchol(spracVrchol.getaHlbkaVrchola() + 1, spracVrchol);
                    lavySynSprac.setaOtec(spracVrchol);
                    spracVrchol.setaLavySyn(lavySynSprac);
                    spracVrcholy.add(lavySynSprac);
                }
                posunVRamciRiadku++;
                //naitaj pravy syn
                if (vrcholyVRiadku[posunVRamciRiadku].matches("1(\\|\\|\\d+){2}") || vrcholyVRiadku[posunVRamciRiadku].matches("1\\|\\|-\\d+\\|\\|\\d+")) {
                    cisloBloku = Long.parseLong(vrcholyVRiadku[posunVRamciRiadku].split("\\|\\|")[1]);
                    pocetPlatnychZaznamov = Long.parseLong(vrcholyVRiadku[posunVRamciRiadku].split("\\|\\|")[2]);
                    pravySynSprac = new ExternyVrchol(spracVrchol.getaHlbkaVrchola() + 1, spracVrchol, cisloBloku, pocetPlatnychZaznamov);
                    pravySynSprac.setaOtec(spracVrchol);
                    spracVrchol.setaPravySyn(pravySynSprac);
                } else {
                    pravySynSprac = new Vrchol(spracVrchol.getaHlbkaVrchola() + 1, spracVrchol);
                    pravySynSprac.setaOtec(spracVrchol);
                    spracVrchol.setaPravySyn(pravySynSprac);
                    spracVrcholy.add(pravySynSprac);
                }
                posunVRamciRiadku++;

                if (posunVRamciRiadku == vrcholyVRiadku.length) {
                    somNaKonciRiadku = true;
                }
                spracVrcholy.removeFirst();
            }
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TrieStrom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TrieStrom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TrieStrom.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            fw.stopRead();
        }
        return ret;
    }

    public void zapisTrieStromuDoSuboru(File paFile) {
        StringBuilder prvkyVHlbke = new StringBuilder(15000);
        paFile.delete();
        int poce = 0;
        FileWorker.writeDataToFile(paFile,String.valueOf(MAXIMALNA_HLBKA_TRIE_STROMU),false);
        if(aKoren == null){
            return;
        }
        List<Vrchol> front = new LinkedList<>();
        front.add(aKoren);
        int spracHlbka = 0;
        int pocetPrejdenychVHlbke = 0;
        while (front.size() > 0) {
            Vrchol spracVrchol = front.get(0);
            if (pocetPrejdenychVHlbke == Math.pow(2, spracHlbka)) {
                FileWorker.writeDataToFile(paFile, prvkyVHlbke.toString() + "\n", true);
                prvkyVHlbke = new StringBuilder(15000);
                spracHlbka++;
                pocetPrejdenychVHlbke = 0;
            }
            pocetPrejdenychVHlbke++;
            if (spracVrchol != null) {
                prvkyVHlbke.append(spracVrchol.toString().concat("\t"));
                poce++;
            }
            if (spracVrchol != null && spracVrchol.getaLavySyn() != null) {
                front.add(spracVrchol.getaLavySyn());
            }
            if (spracVrchol != null && spracVrchol.getaPravySyn() != null) {
                front.add(spracVrchol.getaPravySyn());
            }
            front.remove(0);
        }
        FileWorker.writeDataToFile(paFile, prvkyVHlbke.toString() + "\n", true);
        System.out.println("Poc: " + poce);
    }

    public String vypisTrieStromuLevelOrder() {
        StringBuilder ret = new StringBuilder(30000);
        StringBuilder prvkyVHlbke = new StringBuilder(15000);
        int poce = 0;
        ret.append(Integer.toString(MAXIMALNA_HLBKA_TRIE_STROMU)).append("\n");
        if (aKoren == null) {
            return ret.toString();
        }
        List<Vrchol> front = new LinkedList<>();
        front.add(aKoren);
        int spracHlbka = 0;
        int pocetPrejdenychVHlbke = 0;
        boolean vsetciVAktualnejHlbkeSuNull = true;
        while (front.size() > 0) {
            Vrchol spracVrchol = front.get(0);
            if (pocetPrejdenychVHlbke == Math.pow(2, spracHlbka)) {
                if (vsetciVAktualnejHlbkeSuNull) {
                    return ret.toString();
                } else {
                    ret.append(prvkyVHlbke.toString() + "\n");
                    prvkyVHlbke = new StringBuilder(15000);
                }
                spracHlbka++;
                pocetPrejdenychVHlbke = 0;
                vsetciVAktualnejHlbkeSuNull = true;
            }
            pocetPrejdenychVHlbke++;
            if (spracVrchol != null) {
                prvkyVHlbke.append(spracVrchol.toString() + "\t");
                vsetciVAktualnejHlbkeSuNull = false;
                poce++;
            } else {
                prvkyVHlbke.append("null\t");
            }
            if (spracVrchol != null && spracVrchol.getaLavySyn() != null) {
                front.add(spracVrchol.getaLavySyn());
            } else if (spracHlbka < MAXIMALNA_HLBKA_TRIE_STROMU) {
                front.add(null);
            }
            if (spracVrchol != null && spracVrchol.getaPravySyn() != null) {
                front.add(spracVrchol.getaPravySyn());
            } else if (spracHlbka < MAXIMALNA_HLBKA_TRIE_STROMU) {
                front.add(null);
            }
            front.remove(0);
        }
        System.out.println("Poc: " + poce);
        ret.append(prvkyVHlbke.toString());
        return ret.toString();
    }

    /**
     * Metoda transformuje pri preplneni bloku externy vrchol obsahujuci
     * preplneny blok na interny. Upravi strukturu trie stromu. Nasledne vrati
     * novovzniknute externe vrcholy. Ak doslo znova k preplneniu - > nastavi
     * externemu vrcholu cislo bloku transformovaneho interneho vrchola(cis blok
     * je ALOKOVANY) Ak nedoslo k preplneniu - > vrati vzniknute vrcholy, ktore
     * su vsak NEALOKOVANE VSETKO SA ROBI ZATIAL LEN V OPERACNEJ PAMATI
     *
     * @param paExternyVrchol
     * @param paPreplBlok - blok, v ktotom doslo k preplneniu
     * @return
     */
    public ExterneVrcholyVznikTrasfExtNaInt vytvor_Z_Ext_Vrchola_Int_a_Pridaj_Mu_Externe(ExternyVrchol paExternyVrchol, Blok paPreplBlok) {
        if (paExternyVrchol.getaHlbkaVrchola() == MAXIMALNA_HLBKA_TRIE_STROMU) {
            throw new IllegalStateException("Tak to teda nie. Externy vrchol uz je na poslednej urovni. Nemozno z neho spravit interny");
        }
        Vrchol internyVrchol = new Vrchol(paExternyVrchol.aHlbkaVrchola, paExternyVrchol.aOtec);
        Blok blokVLavomExtVrch = new Blok(paPreplBlok);
        blokVLavomExtVrch.reset();
        Blok blokVPravomExtVrch = new Blok(paPreplBlok);
        blokVPravomExtVrch.reset();
        int pom;
        int pom2 = 1;
        for (Zaznam prech : paPreplBlok) {
            pom = prech.hashCode() >>> internyVrchol.getaHlbkaVrchola();
            pom = pom & pom2;
            if (pom > 0) {
                blokVPravomExtVrch.vlozZaznam(prech);
            } else {
                blokVLavomExtVrch.vlozZaznam(prech);
            }
            blokVLavomExtVrch.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
            blokVPravomExtVrch.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
        }

        //ak je otec trasformovany pravym synom oca
        //DEBUGUJ CI SA VOLA EQUALS NA EXTERNEOM VRCHOLE - vtedy to je OK
        if (internyVrchol.aOtec != null && internyVrchol.aOtec.aPravySyn != null && internyVrchol.aOtec.aPravySyn.equals(paExternyVrchol)) {
            internyVrchol.aOtec.setaPravySyn(internyVrchol);
        } else if (internyVrchol.aOtec != null && internyVrchol.aOtec.aLavySyn != null && internyVrchol.aOtec.aLavySyn.equals(paExternyVrchol)) {
            internyVrchol.aOtec.setaLavySyn(internyVrchol);
        }

        ExterneVrcholyVznikTrasfExtNaInt ret = new ExterneVrcholyVznikTrasfExtNaInt();
        //bud nevznikne ziadny prepleneny alebo vznikne prave jejen prepleneny
        ExternyVrchol lavyxternyVrchol = new ExternyVrchol(paExternyVrchol.aHlbkaVrchola + 1, internyVrchol, -1, 0);
        ExternyVrchol pravyxternyVrchol = new ExternyVrchol(paExternyVrchol.aHlbkaVrchola + 1, internyVrchol, -1, 0);
        lavyxternyVrchol.setaPocetPlatnychZaznamov(blokVLavomExtVrch.getPocetPlatnychZaznamovBloku());
        pravyxternyVrchol.setaPocetPlatnychZaznamov(blokVPravomExtVrch.getPocetPlatnychZaznamovBloku());
        internyVrchol.setaLavySyn(lavyxternyVrchol);
        internyVrchol.setaPravySyn(pravyxternyVrchol);
        if (!blokVLavomExtVrch.jeBlokPreplneny() && !blokVPravomExtVrch.jeBlokPreplneny()) {
            ret.aLavyExternyVrchol = lavyxternyVrchol;
            ret.aPravyExternyVrchol = pravyxternyVrchol;
            ret.aBlokVLavomExtVrchole = blokVLavomExtVrch;
            ret.aBlokVPracomExternomVrchole = blokVPravomExtVrch;
            ret.aDosloZnovaKPreplneniu = false;
        } else if (blokVLavomExtVrch.jeBlokPreplneny()) {
            ret.aPreplnenyExternyVrchol = lavyxternyVrchol;
            ret.aPreplnenyBlok = blokVLavomExtVrch;
            ret.aPreplnenyBlok.setaCisloBloku(paPreplBlok.getaCisloBloku());
            ret.aPreplnenyBlok.setCisloZretazenehoBloku(paPreplBlok.getaCisloZretazenehoBloku());
            ret.aPreplnenyExternyVrchol.setaCisloBloku(paPreplBlok.getaCisloBloku());
            ret.aDosloZnovaKPreplneniu = true;
        } else if (blokVPravomExtVrch.jeBlokPreplneny()) {
            ret.aPreplnenyExternyVrchol = pravyxternyVrchol;
            ret.aPreplnenyBlok = blokVPravomExtVrch;
            ret.aPreplnenyBlok.setaCisloBloku(paPreplBlok.getaCisloBloku());
            ret.aPreplnenyBlok.setCisloZretazenehoBloku(paPreplBlok.getaCisloZretazenehoBloku());
            ret.aPreplnenyExternyVrchol.setaCisloBloku(paPreplBlok.getaCisloBloku());
            ret.aDosloZnovaKPreplneniu = true;
        } else {
            throw new IllegalStateException("Nensastala sni jedna z nasledujucich situacii :"
                    + "Bud nevznikne ziadny prepleneny alebo vznikne prave jejen prepleneny");
        }
        if (paExternyVrchol.equals(aKoren)) {
            aKoren = internyVrchol;
        }
        return ret;
    }

    public static class ExterneVrcholyVznikTrasfExtNaInt {

        private ExternyVrchol aLavyExternyVrchol;
        private ExternyVrchol aPravyExternyVrchol;
        private Blok aBlokVLavomExtVrchole;
        private Blok aBlokVPracomExternomVrchole;
        private boolean aDosloZnovaKPreplneniu;
        private ExternyVrchol aPreplnenyExternyVrchol;
        private Blok aPreplnenyBlok;

        public ExternyVrchol getaLavyExternyVrchol() {
            return aLavyExternyVrchol;
        }

        public ExternyVrchol getaPravyExternyVrchol() {
            return aPravyExternyVrchol;
        }

        public Blok getaBlokVLavomExtVrchole() {
            return aBlokVLavomExtVrchole;
        }

        public Blok getaBlokVPracomExternomVrchole() {
            return aBlokVPracomExternomVrchole;
        }

        public boolean dosloZnovaKPreplneniu() {
            return aDosloZnovaKPreplneniu;
        }

        public ExternyVrchol getPreplnenyExternyVrchol() {
            return aPreplnenyExternyVrchol;
        }

        public Blok getaPreplnenyBlok() {
            return aPreplnenyBlok;
        }
    }
}

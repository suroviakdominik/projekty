package com.suroviak.dominik.zooAplication.WorkWithFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Created by Dominik on 3.3.2015.
 */
public class SerializationWorker<T> {

    private String aEndOfFiles;
    private String aNameOfFile;
    private String aPath = null;

    public SerializationWorker(String paEndOfFiles) {
        aEndOfFiles = paEndOfFiles;
    }

    public void setNameOfFile(String paNameOfFile) {
        this.aNameOfFile = paNameOfFile;
    }


    public void setPath(String paPath) {
        aPath = paPath;
    }


    public T readObjetctFromFile() throws IOException, ClassNotFoundException {
        T ret = null;

        FileInputStream stream = null;
        ObjectInputStream reader = null;
        try {
            String oddelovac;
            if (aEndOfFiles.equals("")) {
                oddelovac = "";
            } else {
                oddelovac = ".";
            }
            File f = new File(aPath + aNameOfFile + oddelovac + aEndOfFiles);
            stream = new FileInputStream(f);
            reader = new ObjectInputStream(stream);
            ret = (T) reader.readObject();
        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        } catch (ClassNotFoundException ex) {
            throw ex;
        } finally {
            if (reader != null)
                reader.close();
            if (stream != null)
                stream.close();

        }
        return ret;
    }

    public boolean saveObjectToFile(T paObject) throws IOException {
        FileOutputStream stream = null;
        ObjectOutputStream writer=null;
        try {
            File f = new File(aPath+aNameOfFile+"."+aEndOfFiles);
            stream = new FileOutputStream(f);
            writer=new ObjectOutputStream(stream);
            writer.writeObject(paObject);

        } catch (FileNotFoundException ex) {
            throw ex;

        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                writer.close();
                stream.close();
                return true;
            } catch (IOException ex) {
               throw ex;
            }
        }

    }
}

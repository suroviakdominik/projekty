

.MODEL small
.STACK 100h
.DATA
Postupnost DW 6,10,4,3,-2,1
PostupnostRozdielCisel DW 5 dup (?);o jedntku menej ako dlzka postupnosti
NacitanaPostupnost DW 20 dup(?)
NacitanaPostupnostRozdielCisel DW 19 dup (?)
DlzkaPostupnosti DW 6
DesiatPriNas DW 10
PopisovacSuboru DW ?
ZapisanyZnakDoSuboru DB ? 
VypZadajteDlzkupostupnosti DB 'Zadajte dlzku potupnosti ktoru chcete nacitat(2-20): $' 
DlzkaNacitanejPostupnosti DW ?
MenoSuboru DB 'TextSub.txt',0
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
NajmensiRozdiel DW ?
NajmensiRozdnacPost DW ?
.CODE
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret

Read ENDP 

;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo
                                                                                                                         
Write ENDP

;-------------------------------------------------------------------------------------PROCEDURA ZAPIS ZNAKU Z REGISTRA AX DO SUBORU FUNGUJUCA---------------------------------------------------------------------------
WriteFile PROC 
      push dx
      push cx
      push ax
      push bx
      push di
      mov cx,0
          
     
    
     cmp ax,0
     jl VypisMinu
     
     
     KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        push cx
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        loop VypisCisl
        
       pop di
       pop bx
       pop ax
       pop cx
       pop dx
       ret

     VypisMinu:
        push ax
        push cx
        mov al,'-'
        mov bx,PopisovacSuboru
        mov cx,1                                                                           
        mov ZapisanyZnakDoSuboru,al                                                                                           
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        pop ax
        neg ax
        jmp KonvertujNaDekCisl

WriteFile ENDP
;-----------------------------------PROCEDURA ZMAZE OBSAH TERMINALU-----------------------------------------------
ClrScr PROC
	push ax
	
	mov ah,0
	mov al,3
	int 10h
	
	pop ax
	
	ret
ClrScr ENDP

;------------------------------------------PROCEDURA VYPISE PRVKY POSTUPNOSTI, KTOREJ OFFSET ODOVZDAME V REGISTRI BX A DLZKU V REGISTRI CX-------------------------------------------
VypisPostupnosti PROC
push ax
push bx
push cx
push di

mov di,0

  Vypisuj:
    mov ah,0
    mov al,[bx+di]
    call Write
    inc di
    inc di
    mov dl,' '
    mov ah,2
    int 21h
  loop Vypisuj
pop di
pop cx
pop bx
pop ax
ret
ENDP VypisPostupnosti

ZadanieA PROC
 push ax
 push bx
 push cx
 push dx
 push si
 push di
          mov cx,DlzkaPostupnosti
 
          sub cx,1;lebo ked bude di na predposlednom tak je koniec
          mov si,2
          mov di,0
          
          CyklusRozdielCisel:         
          mov ax,Postupnost[di]
          mov bx,Postupnost[si]
          sub ax,bx

          cmp ax,0
          jge CyklPokr
          not ax
          add ax,1

        CyklPokr:
          mov PostupnostRozdielCisel[di],ax
          inc di
          inc di
          inc si
          inc si
          loop CyklusRozdielCisel
         
        ;mov bx,offset Postupnost
        ;mov cx,DlzkaPostupnosti  
        ;call VypisPostupnosti  

        mov bx,offset PostupnostRozdielCisel
        mov cx,DlzkaPostupnosti
        sub cx,1  
        call VypisPostupnosti  
        
        mov NajmensiRozdiel,32767
        mov di,0
        mov si,0
        mov cx,DlzkaPostupnosti
        sub cx,2
        CyklHladNajmensie:
        mov ax,PostupnostRozdielCisel[di]
        cmp ax,PostupnostRozdielCisel[di+2]
        cmp ax,NajmensiRozdiel
        jge Pokr
        mov NajmensiRozdiel,ax
       Pokr:
        inc di
        inc di
        loop CyklHladNajmensie

       mov ax,NajmensiRozdiel
       call Write
pop di
pop si
pop dx
pop cx
pop bx
pop ax
ret
ENDP ZadanieA

ZadanieC PROC
push ax
push bx
push cx
push dx
push si
push di
mov cx,0;vytvori subor pre zapis
        mov dx,offset MenoSuboru
        mov ah,3Ch
        int 21h
        mov PopisovacSuboru,AX
        
        mov di,0
        mov cx,DlzkaPostupnosti 
        dec cx
        ZapisDoSUboru:
           
           mov ax,PostupnostRozdielCisel[di]
           inc di    
           inc di 
           call WriteFile
           push cx
           mov cx,1
           mov ZapisanyZnakDoSuboru,' '
           mov bx,PopisovacSuboru    
           mov dx, offset ZapisanyZnakDoSuboru
           mov ah,40h
           int 21h
           pop cx
        loop ZapisDoSUboru

        mov bx,PopisovacSuboru
        mov ah,3Eh
        int 21h
  pop di
  pop si
  pop dx
  pop cx
  pop bx
  pop ax
  ret 
ENDP ZadanieC
;-----------------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call ZadanieA
          call ZadanieC
          
          

          mov dx, offset VypZadajteDlzkupostupnosti
          mov ah,9
          int 21h
          call Read
          mov DlzkaNacitanejPostupnosti,ax
          
          mov cx,ax
          mov di,0
        

        NacitajPostupnost:
          call Read
          mov NacitanaPostupnost[di],ax
          inc di
          inc di
        loop NacitajPostupnost
          
          
          mov cx,DlzkaNacitanejPostupnosti
          cmp DlzkaNacitanejPostupnosti,1
          jle Koniec 
         
          sub cx,1;lebo ked bude di na predposlednom tak je koniec
          mov si,2
          mov di,0
          
          CyklusRozdielCise:         
          mov ax,NacitanaPostupnost[di]
          mov bx,NacitanaPostupnost[si]
          sub ax,bx

          cmp ax,0
          jge CyklPok
          not ax
          add ax,1

        CyklPok:
          mov NacitanaPostupnostRozdielCisel[di],ax
          inc di
          inc di
          inc si
          inc si
          loop CyklusRozdielCise
         
         

        mov bx,offset NacitanaPostupnostRozdielCisel
        mov cx,DlzkaNacitanejPostupnosti
        sub cx,1  
        call VypisPostupnosti 
         
        mov NajmensiRozdnacPost,32767
        mov di,0
        mov si,0
        mov cx,DlzkaNacitanejPostupnosti
        sub cx,2
       CyklHladNajmensi:
        mov ax,NacitanaPostupnostRozdielCisel[di]
        cmp ax,NacitanaPostupnostRozdielCisel[di+2]
        cmp ax,NajmensiRozdnacPost
        jge Pok
        mov NajmensiRozdnacPost,ax
       Pok:
        inc di
        inc di
        loop CyklHladNajmensi

       mov ax,NajmensiRozdnacPost
       call Write  
Koniec:    
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































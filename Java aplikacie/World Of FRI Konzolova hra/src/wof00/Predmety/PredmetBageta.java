/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class PredmetBageta implements IPredmet, Serializable {

    private String aNazov;
    private int aVelkost;

    public PredmetBageta(String paNazov, int paVelkost) {
        aNazov = paNazov;
        aVelkost = paVelkost;
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return "Bageta po pouziti pridava zivot. Velkost: " + aVelkost;
    }

    @Override
    public String pouzi(Hrac paHrac) {
        paHrac.zvysZivot(aVelkost);
        paHrac.odstranZInventara(aNazov);
        return "Zivot sa vam zvysil o: " + aVelkost;
    }

}

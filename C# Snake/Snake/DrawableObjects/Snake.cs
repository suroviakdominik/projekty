﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake.DrawableObjects
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        Non
    };

    public enum SnakeSpeed
    {
        Normal,
        Fast,
        AccelerateAfterPartMove
    }
    public class Snake : IDrawableObject
    {
        public const int SpeedInitTimes = 5;///pociatocna rychlost hada - posunie sa  za 1/5 sekundy o jednu cast
        public const int InitSizeOfSnake = 10;///pociatocna velkost hada

        protected LinkedList<SnakePartMovementInfo> aPositionsOfSnakeParts;///pozicie jednotlivych casti hada a ich smery
        protected float aPartMovement;///velkost pohybu v px vykonana od poslednej zmeny smeru pohybu
        protected Boolean aSnakeStartMove;///ci sa had zacal pohybovat
        //player movement speed

        protected Texture2D aPlayerTexture;///obrazok jednej casti hada
        protected SnakeSpeed aSnakeSpeedInfo;///aktualna rychlost hada
        protected SpriteFont aFont;///pismo pre vykreslovanie score
        protected int aSnakeSpeed;///rychlost hada cinitel


        public float PlayerLastUpdateMoveSize { get; set; }///rychlost hada pri posledne vykonanom update
        protected GraphicsDevice GRaphDev { get; set; }///platno kde kreslive
        public int Score { get; set; }///dosiahnute skore

        ///<summary>
        /// konstruktor
        /// </summary>
        /// <param name="paGraphicsDevice">platno</param>
        public Snake(GraphicsDevice paGraphicsDevice)
        {
            GRaphDev = paGraphicsDevice;
            SnakeSpeedInfo = SnakeSpeed.Normal;
            aPositionsOfSnakeParts = new LinkedList<SnakePartMovementInfo>();
        }

        /// <summary>
        /// ohranicena oblast hlavy hada
        /// </summary>
        public Rectangle SnakeBox
        {
            get
            {
                return new Rectangle((int)(aPositionsOfSnakeParts.First.Value.CurrentPartPosition.X), (int)aPositionsOfSnakeParts.First.Value.CurrentPartPosition.Y,
                    Width, Height);
            }
        }

        /// <summary>
        /// Nastavenie aktualneho pohybu hada
        /// </summary>
        public Direction ActualFirstPartDirection
        {
            get { return aPositionsOfSnakeParts.First.Value.ActualDirection; }
            set
            {
                aPositionsOfSnakeParts.First.Value.ActualDirection = value;
                aPositionsOfSnakeParts.First.Value.NextDirection = value;
            }
        }

        /// <summary>
        /// aktualna rychlost hada - normalne alebo rychlo
        /// </summary>
        protected SnakeSpeed SnakeSpeedInfo
        {
            get { return aSnakeSpeedInfo; }
            set
            {
                if (value == SnakeSpeed.Fast)
                {
                    aSnakeSpeed = 2;
                }
                else
                {
                    aSnakeSpeed = 1;
                }
                aSnakeSpeedInfo = value;
            }
        }

        /// <summary>
        /// sirka jednej casti hada v px
        /// </summary>
        public int Width
        {

            get { return aPlayerTexture.Width; }

        }

        /// <summary>
        /// vyska jednej casti hada v px
        /// </summary>
        public int Height
        {
            get { return aPlayerTexture.Height; }
        }

        /// <summary>
        /// vrat zoznam vsetkych casti hada
        /// </summary>
        public LinkedList<SnakePartMovementInfo> PositionsOfSnakeParts
        {
            get { return aPositionsOfSnakeParts; }
        }

        /// <summary>
        /// zvys velkost hada o jednu cast
        /// </summary>
        public void grow()
        {
            Score += 10;
            SnakePartMovementInfo last = aPositionsOfSnakeParts.Last();
            SnakePartMovementInfo nextPart;
            if (last.ActualDirection == Direction.Up)
            {
                nextPart = new SnakePartMovementInfo(last.ActualDirection, last.CurrentPartPosition.X, last.CurrentPartPosition.Y + Height);
            }
            else if (last.ActualDirection == Direction.Down)
            {
                nextPart = new SnakePartMovementInfo(last.ActualDirection, last.CurrentPartPosition.X, last.CurrentPartPosition.Y - Height);
            }
            else if (last.ActualDirection == Direction.Right)
            {
                nextPart = new SnakePartMovementInfo(last.ActualDirection, last.CurrentPartPosition.X - Width, last.CurrentPartPosition.Y);
            }
            else
            {
                nextPart = new SnakePartMovementInfo(last.ActualDirection, last.CurrentPartPosition.X + Width, last.CurrentPartPosition.Y);
            }
            nextPart.OneMovementFinish = false;
            aPositionsOfSnakeParts.AddLast(nextPart);
        }

        /// <summary>
        /// inicializuj pociatocnu poziciu hada
        /// </summary>
        /// <param name="position">pociatocna pozicia</param>
        public void InitializeStartState(Vector2 position)
        {
            aPositionsOfSnakeParts.Clear();
            Score = 0;
            aPartMovement = 0;
            aSnakeStartMove = false;
            aPositionsOfSnakeParts.AddFirst(new SnakePartMovementInfo(new Vector2(0, 0), Direction.Non));
            aPositionsOfSnakeParts.First.Value.CurrentPartPosition = new Vector2(position.X + 3 * Width, position.Y + (float)Width / 2);
            var predchodca = aPositionsOfSnakeParts.First.Value;
            for (int i = 1; i < InitSizeOfSnake; i++)
            {
                aPositionsOfSnakeParts.AddLast(new SnakePartMovementInfo(Direction.Non,
                                                                     predchodca.CurrentPartPosition.X - Width,
                                                                     aPositionsOfSnakeParts.First.Value.CurrentPartPosition.Y));
                if (aPositionsOfSnakeParts.Last.Previous != null)
                    predchodca = aPositionsOfSnakeParts.Last.Value;
            }
        }

        /// <summary>
        /// naciatanie dat, ktore sa budu vykreslovat, prehravar ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aPlayerTexture = content.Load<Texture2D>("player");
            aFont = content.Load<SpriteFont>("SMFont");
        }

        /// <summary>
        /// zastav pohyb hada
        /// </summary>
        public void stopSnakeMovement()
        {
            aSnakeStartMove = false;
        }

        /// <summary>
        /// vrat true ak bol vykonany jeden pohyb - pohyb o jednu cast hada -> aPartMovement bolo > Width
        /// </summary>
        public bool VykonanyJedenPohyb { get; set; }

        /// <summary>
        /// posun hada, pripadne zmen jeho smer pohybu
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            VykonanyJedenPohyb = false;
            //move about width for 0.5 second
            PlayerLastUpdateMoveSize = aSnakeSpeed * SpeedInitTimes * Width * (float)gameTime.ElapsedGameTime.TotalSeconds;//rychlost hada
            Direction newDirection;
            newDirection = checkKeyboardState();//skontroluj ci nebolo nieco stlacene
            //ak sa zmenil smer prveho prvku
            if (newDirection != Direction.Non && newDirection != ActualFirstPartDirection && ValidInput(newDirection))
            {
                if (ActualFirstPartDirection == Direction.Non)//had sa este nepohyboval, ale zacina sa
                {
                    initMovementStart(newDirection);//ak sa had doteraz nepohyboval, a bola stlacena klavesa, tak mu nastaci casti tak aby sa rozpohybovali
                }
                else
                {
                    aPositionsOfSnakeParts.First.Value.NextDirection = newDirection;//naplanuj ze sa had po presunuti o jednu cast pohybovat dalsim smerom
                }
            }

            aPartMovement += aSnakeStartMove ? PlayerLastUpdateMoveSize : 0;//zvys part movement
            PlayerLastUpdateMoveSize = aPartMovement > Width ? PlayerLastUpdateMoveSize - (aPartMovement - Width) : PlayerLastUpdateMoveSize;//ak by sa had posunul po posunuti o viac ako jedno policko, zaokruhli aby sa posunul presne o jedno
            if (aPartMovement >= Width)//ak sa had nezacal pohybovat alebo uz presiel velkost jedneho policka
            {
                aPartMovement = 0;//zacina pohyb o dalsiu cast
                //povol zmenit smer pohybu, kedze som presiel velkost jedneho policka
                foreach (var iter in aPositionsOfSnakeParts)
                {
                    iter.OneMovementFinish = true;
                }
                VykonanyJedenPohyb = true;
                if (SnakeSpeedInfo == SnakeSpeed.Normal)//ak had ide normalnou rychlostou naplanuj, ze ak bude stlacene dalsie tlacidlo v rovnakom smere hada tak sa po presunuti casti zrychli
                {
                    SnakeSpeedInfo = SnakeSpeed.AccelerateAfterPartMove;
                }
                else if (SnakeSpeedInfo == SnakeSpeed.AccelerateAfterPartMove)
                {
                    SnakeSpeedInfo = SnakeSpeed.Fast;
                }
            }
            RecalculatePositionOfAllSnakeParts();//posun hada, pripadne ak je to povolene tak zmen jeho smer na dalsi
            moveToScreenIfPartIsOverTheScreen();//ak had presiel za obrazovku vykresli ho ako vychadza na druhej strane
        }

        /// <summary>
        /// urobenie aby sa had zjavil po prekroceni hranic obrazovky na druhej strane
        /// vytvori sa pomocna cast, ktora dojde za hranicu obrazovky a zanikne
        /// normalna cast sa presunie na druhu stranu
        /// </summary>
        protected void moveToScreenIfPartIsOverTheScreen()
        {
            LinkedListNode<SnakePartMovementInfo> procNode = aPositionsOfSnakeParts.First;
            while (procNode != null)
            {
                if (procNode.Value.ActualDirection == Direction.Right &&
                    procNode.Value.CurrentPartPosition.X + Width >= GRaphDev.Viewport.Width)
                {
                    if (procNode.Value.CrossOverScreenPart && procNode.Value.CurrentPartPosition.X >= GRaphDev.Viewport.Width)
                    {
                        var pom = procNode.Previous;
                        aPositionsOfSnakeParts.Remove(procNode);
                        procNode = pom;
                    }
                    else if (!procNode.Value.CrossOverScreenPart)
                    {
                        //Console.WriteLine("Vytvoril som cross over part RIGHT");
                        float kolkoPrekrociloSirku = procNode.Value.CurrentPartPosition.X + Width - GRaphDev.Viewport.Width;
                        procNode.Value.CrossOverScreenPart = true;
                        var pozPoPresune = new Vector2(-Width + kolkoPrekrociloSirku, procNode.Value.CurrentPartPosition.Y);
                        var presuNaZac = new SnakePartMovementInfo(pozPoPresune, procNode.Value.ActualDirection);
                        presuNaZac.NextDirection = procNode.Value.NextDirection;
                        aPositionsOfSnakeParts.AddBefore(procNode, new LinkedListNode<SnakePartMovementInfo>(presuNaZac));
                    }
                }
                else if (procNode.Value.ActualDirection == Direction.Left && procNode.Value.CurrentPartPosition.X <= 0)
                {
                    if (procNode.Value.CrossOverScreenPart && procNode.Value.CurrentPartPosition.X <= -Width)
                    {
                        var pom = procNode.Previous;
                        aPositionsOfSnakeParts.Remove(procNode);
                        procNode = pom;
                    }
                    else if (!procNode.Value.CrossOverScreenPart)
                    {
                        //Console.WriteLine("Vytvoril som cross over part LEFT");
                        float kolkoPrekrociloSirku = Math.Abs(procNode.Value.CurrentPartPosition.X);
                        procNode.Value.CrossOverScreenPart = true;
                        var pozPoPresune = new Vector2(GRaphDev.Viewport.Width - kolkoPrekrociloSirku,
                            procNode.Value.CurrentPartPosition.Y);
                        var presuNaZac = new SnakePartMovementInfo(pozPoPresune, procNode.Value.ActualDirection);
                        presuNaZac.NextDirection = procNode.Value.NextDirection;
                        aPositionsOfSnakeParts.AddBefore(procNode, new LinkedListNode<SnakePartMovementInfo>(presuNaZac));
                    }
                }
                else if (procNode.Value.ActualDirection == Direction.Down &&
                         procNode.Value.CurrentPartPosition.Y + Height >= GRaphDev.Viewport.Height)
                {
                    if (procNode.Value.CrossOverScreenPart &&
                        procNode.Value.CurrentPartPosition.Y >= GRaphDev.Viewport.Height)
                    {
                        var pom = procNode.Previous;
                        aPositionsOfSnakeParts.Remove(procNode);
                        procNode = pom;
                        //Console.WriteLine("Zrusil som cross over part DOWN");
                    }
                    else if (!procNode.Value.CrossOverScreenPart)
                    {
                        //Console.WriteLine("Vytvoril som cross over part DOWN");
                        float kolkoPrekrocivloVysku = procNode.Value.CurrentPartPosition.Y + Height -
                                                      GRaphDev.Viewport.Height;
                        procNode.Value.CrossOverScreenPart = true;
                        var pozPoPresune = new Vector2(procNode.Value.CurrentPartPosition.X, -Height + kolkoPrekrocivloVysku);
                        var presuNaZac = new SnakePartMovementInfo(pozPoPresune, procNode.Value.ActualDirection);
                        presuNaZac.NextDirection = procNode.Value.NextDirection;
                        aPositionsOfSnakeParts.AddBefore(procNode, new LinkedListNode<SnakePartMovementInfo>(presuNaZac));
                    }
                }
                else if (procNode.Value.ActualDirection == Direction.Up && procNode.Value.CurrentPartPosition.Y <= 0)
                {
                    if (procNode.Value.CrossOverScreenPart && procNode.Value.CurrentPartPosition.Y <= -Height)
                    {
                        var pom = procNode.Previous;
                        aPositionsOfSnakeParts.Remove(procNode);
                        procNode = pom;
                        //Console.WriteLine("Zrusil som cross over part UP");
                    }
                    else if (!procNode.Value.CrossOverScreenPart)
                    {
                        //Console.WriteLine("Vytvoril som cross over part UP");
                        float kolkoPrekrociloVysku = Math.Abs(procNode.Value.CurrentPartPosition.Y);
                        procNode.Value.CrossOverScreenPart = true;
                        var pozPoPresune = new Vector2(procNode.Value.CurrentPartPosition.X,
                            GRaphDev.Viewport.Height - kolkoPrekrociloVysku);
                        var presuNaZac = new SnakePartMovementInfo(pozPoPresune, procNode.Value.ActualDirection);
                        presuNaZac.NextDirection = procNode.Value.NextDirection;
                        aPositionsOfSnakeParts.AddBefore(procNode, new LinkedListNode<SnakePartMovementInfo>(presuNaZac));
                    }
                }
                if (procNode != null)
                    procNode = procNode.Next;
            }
        }

        /// <summary>
        /// posun hada o aktualnu velkost pohybu 
        /// a ak je to mozne ->oneMOvementFinish = true, nastav aktualny smer na dalsi smer(zmen smer pohybu)
        /// </summary>
        protected void RecalculatePositionOfAllSnakeParts()
        {
            if (!aSnakeStartMove)
            {
                return;
            }
            Direction backwarDirection = aPositionsOfSnakeParts.First.Value.NextDirection;
            foreach (var snakePartIter in aPositionsOfSnakeParts)
            {
                MovePart(snakePartIter.ActualDirection, snakePartIter);
                if (snakePartIter.OneMovementFinish && !snakePartIter.CrossOverScreenPart)
                {
                    snakePartIter.ActualDirection = snakePartIter.NextDirection;
                    snakePartIter.NextDirection = backwarDirection;
                    backwarDirection = snakePartIter.ActualDirection;
                    snakePartIter.OneMovementFinish = false;
                }
            }
        }

        /// <summary>
        /// posun cast v zadanom smere o aktualnu rychlost
        /// </summary>
        /// <param name="paDirection">v smere</param>
        /// <param name="paActualPartInfo">cast, ktoru posuvam</param>
        protected void MovePart(Direction paDirection, SnakePartMovementInfo paActualPartInfo)
        {
            if (paDirection == Direction.Up)
            {
                paActualPartInfo.CurrentPartPosition = new Vector2(paActualPartInfo.CurrentPartPosition.X, paActualPartInfo.CurrentPartPosition.Y - PlayerLastUpdateMoveSize);
            }
            else if (paDirection == Direction.Down)
            {
                paActualPartInfo.CurrentPartPosition = new Vector2(paActualPartInfo.CurrentPartPosition.X, paActualPartInfo.CurrentPartPosition.Y + PlayerLastUpdateMoveSize);
            }
            else if (paDirection == Direction.Left)
            {
                paActualPartInfo.CurrentPartPosition = new Vector2(paActualPartInfo.CurrentPartPosition.X - PlayerLastUpdateMoveSize, paActualPartInfo.CurrentPartPosition.Y);
            }
            else if (paDirection == Direction.Right)
            {
                paActualPartInfo.CurrentPartPosition = new Vector2(paActualPartInfo.CurrentPartPosition.X + PlayerLastUpdateMoveSize, paActualPartInfo.CurrentPartPosition.Y);
            }
        }

        /// <summary>
        /// ak sa had este nezacal pohybovat ale uz sa ma zacat, tak ho uved do pohybu
        /// </summary>
        /// <param name="newDirection"></param>
        protected void initMovementStart(Direction newDirection)
        {
            aSnakeStartMove = true;
            int i = 0;
            foreach (var iter in aPositionsOfSnakeParts)
            {
                iter.ActualDirection = Direction.Right;
                if (i == 1 && newDirection != Direction.Right)
                {
                    if (aPositionsOfSnakeParts.First.Next != null)
                        aPositionsOfSnakeParts.First.Next.Value.NextDirection = newDirection;
                }
                i++;
            }
            ActualFirstPartDirection = newDirection;
        }

        /// <summary>
        /// skontroluj ci nebolo stlacene niektore s tlacidiel pohybu na klavesnici
        /// </summary>
        /// <returns></returns>
        private Direction checkKeyboardState()
        {
            Direction newDirection;
            KeyboardState currentKeyboardState = Keyboard.GetState();
            GamePadState currentGamePadState = GamePad.GetState(PlayerIndex.One);
            if (currentKeyboardState.IsKeyDown(Keys.Left) || currentGamePadState.DPad.Left == ButtonState.Pressed)
            {
                newDirection = Direction.Left;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.Right) ||
                     currentGamePadState.DPad.Right == ButtonState.Pressed)
            {
                newDirection = Direction.Right;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.Up) ||
                     currentGamePadState.DPad.Up == ButtonState.Pressed)
            {
                newDirection = Direction.Up;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.Down) ||
                     currentGamePadState.DPad.Down == ButtonState.Pressed)
            {
                newDirection = Direction.Down;
            }
            else
            {
                newDirection = Direction.Non;
                SnakeSpeedInfo = SnakeSpeed.Normal;

            }
            return newDirection;
        }

        /// <summary>
        /// kontroluje aby sa had nemohol zacat pohybovat proti jeho aktualnemu smeru
        /// </summary>
        /// <param name="paDirection">smer ktory sa ma skotrolovat oproti aktualnemu smeru</param>
        /// <returns></returns>
        protected bool ValidInput(Direction paDirection)
        {
            if (ActualFirstPartDirection == Direction.Non && paDirection == Direction.Left)
            {
                return false;
            }

            if (ActualFirstPartDirection == Direction.Up && paDirection == Direction.Down)
            {
                return false;
            }

            if (ActualFirstPartDirection == Direction.Down && paDirection == Direction.Up)
            {
                return false;
            }

            if (ActualFirstPartDirection == Direction.Right && paDirection == Direction.Left)
            {
                return false;
            }

            if (ActualFirstPartDirection == Direction.Left && paDirection == Direction.Right)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// vykreslenie hada na platno
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(aFont,"Score "+ Score, Vector2.Zero, Color.Green, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            float i = 1;
            float odp = (float)1 / aPositionsOfSnakeParts.Count / 2;
            foreach (var iter in aPositionsOfSnakeParts)
            {
                spriteBatch.Draw(aPlayerTexture, iter.CurrentPartPosition, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, i);
                i -= odp;
            }
            /*spriteBatch.Draw(aPlayerTexture, aPositionsOfSnakeParts[0].CurrentPartPosition, null, Color.White, 0f, Vector2.Zero, 1f,
                SpriteEffects.None, 0f);*/
        }

        /// <summary>
        /// pomocna trieda, ktora obashuje vsetky dolezite udaje o kazdej casti hada
        /// </summary>
        public class SnakePartMovementInfo
        {
            private Vector2 aCurrentPartPosition;
            private Direction aActualDirection;
            private Direction aNextDirection;
            private bool aOneMovementFinish;
            public bool CrossOverScreenPart { get; set; }

            public SnakePartMovementInfo(Direction actualDirection, float positionX, float positionY)
            {
                aActualDirection = actualDirection;
                aCurrentPartPosition = new Vector2(positionX, positionY);
                aNextDirection = actualDirection;
                aOneMovementFinish = false;
            }

            public SnakePartMovementInfo(Vector2 currentPartPosition, Direction actualDirection)
            {
                aCurrentPartPosition = currentPartPosition;
                aActualDirection = actualDirection;
                NextDirection = actualDirection;
                aOneMovementFinish = false;
            }

            public Vector2 CurrentPartPosition
            {
                get { return aCurrentPartPosition; }
                set { aCurrentPartPosition = value; }
            }

            public Direction ActualDirection
            {
                get { return aActualDirection; }
                set
                {
                    aActualDirection = value;
                    NextDirection = value;
                }
            }

            public Direction NextDirection
            {
                get { return aNextDirection; }
                set { aNextDirection = value; }
            }

            public bool OneMovementFinish
            {
                get { return aOneMovementFinish; }
                set { aOneMovementFinish = value; }
            }
        }

    }
}

/*
            if (FirstPartPosition.X > canvasWidth)
            {
                FirstPartPosition.X = 0;
            }

            if (FirstPartPosition.X < 0 - Width)
            {
                FirstPartPosition.X = canvasWidth;
            }

            if (FirstPartPosition.Y > canvasHeight)
            {
                FirstPartPosition.Y = 0;
            }

            if (FirstPartPosition.Y < 0 - Height)
            {
                FirstPartPosition.Y = canvasHeight;
            }*/

//player.FirstPartPosition.X = MathHelper.Clamp(player.FirstPartPosition.X, 0, GraphicsDevice.Viewport.Width - player.Width);

//player.FirstPartPosition.Y = MathHelper.Clamp(player.FirstPartPosition.Y, 0, GraphicsDevice.Viewport.Height - player.Height);*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import EditorsForInfo_Files.InfoSuboryENUM;
import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Dominik
 */
public class UploadDataToServer {

    private ArrayList<Info_About_Dirs_File> aInfoSubory;
    private IndeterminateProgressBar aProgressBar;

    /**
     * 
     * @param paInfoFiles - subory Info_About_DIRS_FILE popisujuce polozky
     */
    public UploadDataToServer(ArrayList<Info_About_Dirs_File> paInfoFiles) {
        aInfoSubory = paInfoFiles;

    }

   /**
    * 
    * @param paInfoFiles subory Info_About_DIRS_FILE popisujuce polozky
    * @param progressBar - progress bar, ktory zobrazuje priebeh uploadu
    */
    UploadDataToServer(ArrayList<Info_About_Dirs_File> paInfoFiles, IndeterminateProgressBar progressBar) {
        aInfoSubory = paInfoFiles;
        aProgressBar = progressBar;
    }

    /**
     * Metoda, ktora skopiruje priecinok Source do priecinka Target
     * @param paPathOfSourcecest cesta priecinka, ktory chceme skopirovat
     * @param paPathOfTarget - priecinok, kde sa priecinok skopiruje
     * @throws IOException 
     */
    private void copySourceDirectoryToTargetDirectory(String paPathOfSource, String paPathOfTarget) throws IOException {
        File source = new File(paPathOfSource);
        File target = new File(paPathOfTarget);
        try {
            FileUtils.copyDirectory(source, target);
        } catch (IOException ex) {
            Logger.getLogger(UploadDataToServer.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("MyLogFiles: Pripravenie dát pre update bolo neúspešné: \nChyba pri kopírovaní súboru: " + paPathOfSource + "d o dočasného priečinka.");
            throw ex;
        }
    }

    /**
     * Metoda ktora skpiruje subor do pricinka na ceste druheho parametre
     * @param paPathOfSource- cest suboru, ktory chceme skopirovat
     * @param paPathOfTarget- miesto, kde sa subor skopiruje
     * @throws IOException 
     */
    private void copySourceFileToTargetDirectory(String paPathOfSource, String paPathOfTarget) throws IOException {
        File source = new File(paPathOfSource);
        File target = new File(paPathOfTarget);
        try {
            FileUtils.copyFileToDirectory(source, target);
        } catch (IOException ex) {
            Logger.getLogger(UploadDataToServer.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("MyLogFiles: Pripravenie dát pre update bolo neúspešné: \nChyba pri kopírovaní súboru: " + paPathOfSource + "d o dočasného priečinka.");
            throw ex;
        }
    }

    /**
     * Zisti, ktore subory je potrebne uploadnut na server, t.j tie, ktore boli
     * v nedavne dobe zmenene a tieto zmeny neboli este nahrane na server
     * a skopiruje ich do docasneho priecinka.
     * @throws IOException 
     */
    public void prepareDataForUpdate() throws IOException {
        if (aInfoSubory.size() != 0) {

            try {
                FileUtils.deleteDirectory(new File(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER));
                DirWorker dw = new DirWorker(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER);
                dw.vytvorAdresarAkNeexistuje();
            } catch (IOException ex) {
                Logger.getLogger(UploadDataToServer.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                throw ex;
            }
            for (Info_About_Dirs_File prech : aInfoSubory) {
                for (String prechItem : prech.getaItemsInfo().keySet()) {
                    if (prech.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile() == null) {//!!!!!!!!!!SKONTROLOVAT CI DATE JE ROVNY NULL- IBA VTEDY UPDAOVAT: prech.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile()==null!!!!!!!!!!!!!!!!!!
                        String relativePath = prech.getaItemsInfo().get(prechItem).getRelativePathOfFilesOfurrentItemInDevice();

                        if (relativePath != null) {
                            copySourceDirectoryToTargetDirectory(InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativePath,
                                    InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER + relativePath);
                            System.out.println("MyLogFiles:File :" + relativePath + " was successfully copied to temporary folder.");
                        } else {
                            
                        }

                    }
                }

            }

            File dOfInDirFil = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoSubory.get(0).getaRelativePathOfThisFileInDevice());
            File filesInD[] = dOfInDirFil.listFiles();
            for (File prech : filesInD) {
                if (!prech.isDirectory()) {
                    try {
                        FileUtils.copyFileToDirectory(new File(prech.getAbsolutePath()),
                                new File(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER + aInfoSubory.get(0).getaRelativePathOfThisFileInDevice()));
                        System.out.println("MyLogFiles:File :" + prech.getAbsolutePath() + " was successfully copied to temporary folder.");
                    } catch (IOException ex) {
                        Logger.getLogger(UploadDataToServer.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                        System.err.println("MyLogFilesERROR: Vytvaranie docasnych data k uploadu zlyhalo. Chyba pri kopirovani do docasneho priecinka.");
                    }
                }
            }

            FileWorker fw = new FileWorker(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER + aInfoSubory.get(0).getaRelativePathOfThisFileInDevice() + "main");
            String domainsOfInfoFiles = "";
            for (Info_About_Dirs_File prech : aInfoSubory) {
                domainsOfInfoFiles += prech.getServerDomainOfThisFile() + "||";
            }
            try {
                fw.writeDataToFile(domainsOfInfoFiles);
                System.out.println("MyLogFiles: Main subor bol uspesne vytvoreny.");
            } catch (IOException ex) {
                Logger.getLogger(UploadDataToServer.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                System.err.println("MyLogFilesERROR: Main subor sa nepodarilo vyrvoriť.");
            }
            System.out.println("MyLogFiles: Datasu pripravene k uploadu na server.");

        }

    }

    /**
     * nahra data nachadzajuce sa v docasnom priecinku na server, tak ze ich
     * skopiruje na vybranu sietovu jednotku odkazujucu na tento server.
     * @throws IOException 
     */
    public void uploadPreparedData() throws IOException {
        System.out.println("MyLogFiles:Upload to server start.");
        DirWorker dw = new DirWorker(InfoSuboryENUM.aNameOfNetworkDriveThatIseConnectToServerDomain);

        if (dw.existDir()) {
            dw = new DirWorker(dw.getAbsolutePath() + InfoSuboryENUM.aPathInNetworkDrive + InfoSuboryENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM());
            dw.vytvorAdresarAkNeexistuje();

            double fullLengthInBytes = FileUtils.sizeOfDirectory(new File(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER));
            double progressNow = 0;
            File f = new File(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER + aInfoSubory.get(0).getaRelativePathOfThisFileInDevice());
            File filesToUpload[] = f.listFiles();
            for (int i = 0; i < filesToUpload.length; i++) {

                if (filesToUpload[i].isFile()) {
                    progressNow += FileUtils.sizeOf(filesToUpload[i]) / fullLengthInBytes;
                    copySourceFileToTargetDirectory(filesToUpload[i].getAbsolutePath(), dw.getAbsolutePath());
                    aProgressBar.setProgress((int) (progressNow * 100));
                } else {
                    for (File prech : filesToUpload[i].listFiles()) {
                        progressNow += FileUtils.sizeOf(prech) / fullLengthInBytes;
                        copySourceDirectoryToTargetDirectory(prech.getAbsolutePath(), dw.getAbsolutePath() + "/" + filesToUpload[i].getName() + "/" + prech.getName());
                        aProgressBar.setProgress((int) (progressNow * 100));
                    }
                }
                System.out.println("MyLogFiles:File :" + filesToUpload[i] + " was successfully upload to server.");
            }
            aProgressBar.setProgress(100);

            System.out.println("MyLogFiles:Data was successfully uploaded to server.");
        } else {
            System.err.println("MyLogFiles:Data cannot be updated to server. No network drive with Sign: Z");
        }
    }

    /**
     * ak uspesn prebehol upload dat, tak datam, ktore boli prave aktualizovane
     * sa nastavi atribut, ze uz boli aktualizovane. Obsahovo datove
     * subory (serializovane objekty Info_About_DIRS_File) sa musia nachadzat
     * v priecinku zadanom ako parameter + relativna cesta odkazovych datovych suborov
     * @param rootFolder 
     */
    public void setDateAndSaveInfoFile(String rootFolder) {
        Date d = new Date();
        for (Info_About_Dirs_File prech : aInfoSubory) {
            for (String prechItem : prech.getaItemsInfo().keySet()) {
                if (prech.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile() == null) {//!!!!!!!!!!SKONTROLOVAT CI DATE JE ROVNY NULL- IBA VTEDY UPDAOVAT: prech.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile()==null!!!!!!!!!!!!!!!!!!
                    prech.getaItemsInfo().get(prechItem).setDateOfEditOrCreateFile(d);
                }
            }
        }

        SerializationWorker sw = new SerializationWorker(InfoSuboryENUM.aEndOfInfoFile);
        String paNameOfInfoFile = null;
        for (Info_About_Dirs_File prech : aInfoSubory) {
            for (InfoSuboryENUM walker : InfoSuboryENUM.values()) {
                if (walker.getSignOfDir().equals(prech.getNameOfDirForThisInfoFile())) {
                    paNameOfInfoFile = walker.getaNameOfInfoFile();
                    sw.setNameOfFile(paNameOfInfoFile);
                    sw.setObjectThatIsSerializable(prech);
                    sw.setPath(rootFolder
                            + walker.getaRelativePathOfDirecotryWhereIAM());
                    sw.saveObjectToFile();
                }
            }
        }
        
    }

}

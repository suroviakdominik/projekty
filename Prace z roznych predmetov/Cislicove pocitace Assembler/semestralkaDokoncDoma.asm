BYTE 0xC0;0
BYTE 0xF9 ;1
BYTE 0xA4;2
BYTE 0xB0;3
BYTE 0x99;4
BYTE 0x92;5
BYTE 0x82;6
BYTE 0xF8;7
BYTE 0x80;8
BYTE 0x90;9
BYTE 0x88;A
BYTE 0x83;B
BYTE 0xC6;C
BYTE 0xA1;D
BYTE 0x86;E
BYTE 0x8E,F
BYTE 0xFF,nic

mvi b,0x01
mvi d,0x00
str b,d

mvi b,0x00
mvi d,0x00
str b,d



Hlavnyprogram:

cal nacitanie
cal porovnanie
cal Posuvanie
vypisss:
cal Vypisnadispleje

mvi b,0x0B
out 0x0004,b
inn c,0x0001
cmi c,0x0F
jnz Hlavnyprogram


jmp vypisss


;----------------------------------------------------------------------------------------------------------------------------------------------------
;konvertovanie a praca s displejmi
Scitovanie:

pus b
pus c
pus d



mvi b,0x00
ldr c,b
adc c,a
jcy Akpretecie

str b,c
jmp navr



Akpretecie:
str b,c
mvi b,0x01
ldr c,b
adi c,1
str b,c

navr:
mvi b,0x00
ldr d,b
mvi b,0x01
ldr d,b

pop d
pop c
pop b
ret


Posuvanie:
;rozdeli binarne cislo na styri stvorice, ktore sa ulozia do pametovych miesty 0x02 az 0x05
;a neskor sa pouziju na vypis na displej

pus a
pus b
pus c
pus d

mvi b,0x00
ldr a,b
mvi c,0x0F
and a,c
mvi b,0x02
str b,a

mvi b,0x00
ldr a,b
mvi c,0xF0
and a,c
shr a,4
mvi b,0x03
str b,a

mvi b,0x01
ldr a,b
mvi c,0x0F
and a,c
mvi b,0x04
str b,a

mvi b,0x01
ldr a,b
mvi c,0xF0
and a,c
shr a,4
mvi b,0x05
str b,a

mvi b,0x02
ldr a,b
mvi b,0x03
ldr a,b
mvi b,0x04
ldr a,b
mvi b,0x05
ldr a,b

pop d
pop c
pop b
pop a

ret

Vypisnadispleje:
;procedura, vypisuje hodnoty z pametovych miest 0x02 az 0x05 na jednotlive displeje v 16kovej sus.
;vypisovanie na 1.displej sprava

pus a
pus b
pus c
pus d

mvi b,0x0E
out 0x02,b


mvi b,0x02
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b


;vypisovanie na 2.displej sprava
mvi b,0x0D
out 0x02,b
mvi b,0x03
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

;vypisovanie na 3.displej sprava
mvi b,0x0B
out 0x02,b
mvi b,0x04
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

;vypisovanie na 4.displej sprava
mvi b,0x07
out 0x02,b
mvi b,0x05
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

pop d
pop c
pop b
pop a
 
ret
;-------------------------------------------------------------------------------------------------------------------------------------------------------
;praca s klavesnicou
nacitanie:
mvi b,0x0E 
out 0x0004,b
inn c,0x0001
cmi c,0x0F
jnz Navrat

mvi b,0x0D
out 0x0004,b
inn c,0x0001
cmi c,0x0F
jnz Navrat
jmp nacitanie
Navrat:

ret

porovnanie:

pus a
pus b
pus c
pus d

cmi b,0x0E
jzr riadok1
cmi b,0x0D
jzr riadok2
cmi b,0x0B
jzr riadok3

riadok1:
cmi c,0x07 ;dole vpravo
jzr pripocitaj1
cmi c,0x0B
jzr pripocitaj2
cmi c,0x0D
jzr pripocitaj4
cmi c,0x0E
jzr pripocitaj8


riadok2:
cmi c,0x07 ; vpravo
jzr pripocitaj16
cmi c,0x0B
jzr pripocitaj32
cmi c,0x0D
jzr pripocitaj64
cmi c,0x0E
jzr pripocitaj128

riadok3:
cmi c,0x07 ; vpravo
adi a,255
navraat:

pop d
pop c
pop b
pop a

ret

pripocitaj1:
mvi a,1
cal Scitovanie
jmp navraat

pripocitaj2:
mvi a,2
cal Scitovanie
jmp navraat

pripocitaj4:
mvi a,4
cal Scitovanie
jmp navraat

pripocitaj8:
mvi a,8
cal Scitovanie
jmp navraat

pripocitaj16:
mvi a,16
cal Scitovanie
jmp navraat

pripocitaj32:
mvi a,32
cal Scitovanie
jmp navraat

pripocitaj64:
mvi a,64
cal Scitovanie
jmp navraat

pripocitaj128:
mvi a,128
cal Scitovanie
jmp navraat

delenie:

ret


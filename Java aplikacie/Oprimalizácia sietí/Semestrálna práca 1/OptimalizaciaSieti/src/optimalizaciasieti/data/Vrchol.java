/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti.data;

import java.util.Objects;



/**
 *
 * @author Dominik
 */
public class Vrchol implements Comparable<Vrchol>{
    private Integer aID;
    private Mesto aMesto;

    public Vrchol(int aID, Mesto aMesto) {
        this.aID = aID;
        this.aMesto = aMesto;
    }

    public int getaID() {
        return aID;
    }

    public void setaID(int aID) {
        this.aID = aID;
    }

    public Mesto getaMesto() {
        return aMesto;
    }

    public void setaMesto(Mesto aMesto) {
        this.aMesto = aMesto;
    }

    @Override
    public int compareTo(Vrchol o) {
        if(aMesto!=null && o.aMesto!=null){
        return this.aMesto.compareTo(o.aMesto);
        }else if(aMesto==null&&o.aMesto==null)
        {
            return 0;
        }
        else if(aMesto==null){
            return 1;
        }else
        {
            return -1;
        }
        
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.aID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vrchol other = (Vrchol) obj;
        if (!Objects.equals(this.aID, other.aID)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        if(aMesto!=null)
        {
          return "Vrchol{ID "+aID+" ;Mesto: "+aMesto.getaNameOfCity()+"}";    
        }
        return "Vrchol{Krizovatka ;ID "+aID+"}"; 
    }

    
    
}

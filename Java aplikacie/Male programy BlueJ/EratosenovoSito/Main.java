
import java.util.Scanner;

public class Main
{
    
  public static void main(String [] args)
    {
       Scanner scan=new Scanner(System.in); 
       System.out.println("Zadaj hornu hranicu cisel");
       int lHornahranica=scan.nextInt();
       EratostenovoSito eratSito=new EratostenovoSito(lHornahranica);
       eratSito.nacitajPole();
       eratSito.najdiPrvocisla();
       eratSito.vypisPrvocisla();
    }
}    

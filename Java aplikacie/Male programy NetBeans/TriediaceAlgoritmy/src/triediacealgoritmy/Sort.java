/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triediacealgoritmy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Dominik
 */
public abstract class Sort {
    public static List<Double> bubbleSort(List<Double> paData)
    {
        Double[] a;
        a=paData.<Double>toArray(new Double[paData.size()]);
        for (int i = 0; i < a.length-1; i++) {
            for (int j = 0; j < a.length-i-1; j++) {
                if(a[j]>a[j+1])
                {
                    Double pom=a[j+1];
                    a[j+1]=a[j];
                    a[j]=pom;
                }
            }
        }
        return Arrays.asList(a);
    }
    
    public static List<Double> javaSort(List<Double> paData)
    {
        Collections.sort(paData);
        return paData;
    }
    
    /** 
 * Razeni slevanim (od nejvyssiho)
 * @param array pole k serazeni
 * @param aux pomocne pole stejne delky jako array
 * @param left prvni index na ktery se smi sahnout
 * @param right posledni index, na ktery se smi sahnout
 */
public static void mergeSort(int[] array, int[] aux, int left, int right) {
   if(left==right) return;
   int middle=(right-left)/2;
    mergeSort(array, aux, left, middle);
    mergeSort(array, aux, middle+1, right);
    merge(array, aux, left, right);
    
    for (int i = left; i <= right; i++) {
        array[i]=aux[i];
    }
}    

/**
 * Slevani pro Merge sort 
 * @param array pole k serazeni
 * @param aux pomocne pole (stejne velikosti jako razene)
 * @param left prvni index, na ktery smim sahnout
 * @param right posledni index, na ktery smim sahnout
 */
private static void merge(int[] array, int[] aux, int left, int right) {
    int middleIndex = (left + right)/2;
    int leftIndex = left; 
    int rightIndex = middleIndex + 1;
    int auxIndex = left;
    while(leftIndex <= middleIndex && rightIndex <= right) {
        if (array[leftIndex] >= array[rightIndex]) {
            aux[auxIndex] = array[leftIndex++];
        } else {
            aux[auxIndex] = array[rightIndex++];
        }
        auxIndex++;
    }
    while (leftIndex <= middleIndex) {
        aux[auxIndex] = array[leftIndex++];
        auxIndex++;
    }
    while (rightIndex <= right) {
        aux[auxIndex] = array[rightIndex++];
        auxIndex++;
    }
}
}

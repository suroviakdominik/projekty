package linearne.štrukurované.datové.typy.GUI;

import java.awt.*;
import java.awt.event.*;//musime importovat preto lebo podbalicky sa neimportuju
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import linearne.štrukurované.datové.typy.*;
import linearne.štrukurované.datové.typy.Objekty.*;

/**
 * Trieda predstavuej šablónu objektu, ktorý má za úlohu vzkreslenie ADT
 * užívateľovi , zabezpečenie interakcie s užívateľom, vykonávanie jdenotlivých
 * akcíi zadaných užívateľom.
 *
 * @author suroviak3
 */
public class GUI extends JFrame {

    private final Front aFront;
    private final Zasobnik aZasobnik;
    private final Zoznam aZoznam;
    private int aPoziciaOknaX;
    private int aPoziciaOknaY;
    private IObjekt aVkladanyObjekt;
    private ILinearDataTypy aAktualnyTab;

    /**
     * Konštruktor triedy GUI vytvóri nové okno, jednotlivým atribútom priradí
     * hodnoty a nakoniec toto okno zobrazí na pracovnej ploche. Okno má
     * rozlíšenie 80 percent z rozlíšenia pracovnej plochy a počas behu programu
     * sa dá jeho veľkosť meniť.
     */
    public GUI() {
        this.aFront = new Front(this);
        this.aZasobnik = new Zasobnik(this);
        this.aZoznam = new Zoznam(this);
        aVkladanyObjekt = null;
        //this.setLocation( 100, 100 );//umiestnenie okna
        // this.setSize( 800, 500 );//a velkost okna,ale na tvrdo,nemozno menit rozmery
        //nahradime inym kodom
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension d = t.getScreenSize();//zisti maximalne rozlisenie obrazovky
        //  System.out.println("DPI = " + t.getScreenResolution() + "  *  Screen : " + d.width + " x " + d.height );    

        final int OKNO_CAST = 80;                         // percento z celej plochy obrazovky-urci velkost okna podla maximalneho rozlisenia nasej obrazovky
        aPoziciaOknaX = d.width * (100 - OKNO_CAST) / 100 / 2;
        aPoziciaOknaY = d.height * (100 - OKNO_CAST) / 100 / 2;
        this.setLocation(d.width * (100 - OKNO_CAST) / 100 / 2, d.height * (100 - OKNO_CAST) / 100 / 2); //umiestnenie okna//height,width -rozlisenie obrazovky na danom pc
        this.setSize(d.width * OKNO_CAST / 100, d.height * OKNO_CAST / 100);//velkost okna

        //  umozni ihned ukoncenie cez ikonu okna     
        //  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//nespyta sa nas ci chceme ukoncit  
        // ukoncenie cez udalost WindowEvent,  ak bude prihlasena        
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        //  prihlasenie  WindowEvent                                                                                    
        this.addWindowListener(new WindowAdapter() {//Windows Adapter je anonymna trieda                                 
            @Override
            public void windowClosing(WindowEvent e) {
                akcia_koniec();
            }   //Windows Event je anonymna trieda              
        });

        JTabbedPane tabPane1 = new JTabbedPane();
        Klavesnica klav = new Klavesnica();
        tabPane1.addKeyListener(klav);

        inicializujHorneMenu();
        inicializujTabFront(tabPane1, klav);
        inicializujTabZasobnik(tabPane1, klav);
        inicializujTabZoznam(tabPane1, klav);
        this.add(tabPane1, BorderLayout.CENTER);

        this.setResizable(true);//aby sme mohli zmenit velkost okna potiahnutim mysou
        this.setTitle("Lineárne štruktúrované datové typy");//nadpis hlavneho okna nasho problemu
        this.setVisible(true);
        this.toFront();//okno zviditelnime a dame ho navrch okien   

    }

    /**
     * Metóda nastaví hodnotu atribútu aAktuanyTab, aby Gui vedel, ktorý ADT ho
     * práve používa
     *
     * @param paTyp: ILinearDataTypy-ADT aktuálne zobrazený v okne GUI
     */
    public void setAktualnePouzivanyTab(ILinearDataTypy paTyp) {
        if (paTyp instanceof Front) {
            aAktualnyTab = aFront;
        }
        if (paTyp instanceof Zasobnik) {
            aAktualnyTab = aZasobnik;
        }
        if (paTyp instanceof Zoznam) {
            aAktualnyTab = aZoznam;
        }
    }

    /**
     * Metóda inicializuje plochu, ktorá bude vykresľovaná v hlavnom okne Gui a
     * bude zobrazovať tlačidlá frontu a Prvky Frontu
     *
     * @param paTabbedPane-hlavný tabbed pane, kde vložíme nový tab pre front
     * @param klav - klávesnica, ktorú používame pri fronte ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicializujTabFront(JTabbedPane paTabbedPane, Klavesnica klav) {
        JPanel panelFrontTab = new JPanel(new BorderLayout(2, 2));
        panelFrontTab.add(BorderLayout.CENTER, new Platno(aFront));
        paTabbedPane.addTab("Front", panelFrontTab);
        inicTlacidlaFront(panelFrontTab, klav);

    }

    /**
     * Metóda pridá do tabu frontu tlačidlá, ktoré bude obsahovať
     *
     * @param panelFrontTab-tab pane frontu
     * @param klav - klávesnica, ktorú používame pri fronte ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicTlacidlaFront(JPanel panelFrontTab, Klavesnica klav) {
///////////////////////////////////////////////////////////////OVLADANIE  TLACIDLAMI////////////////////////////////////////////////////////////////////
        //vytvoríme pomocnú lištu,  ktorú   posadíme   na    SEVER     hlavného formulára
        //a na lištu budeme sádzať jednotlivé tlačidlá,  ku ktorým prihlásime potrebne udalosti
        //najprv lista
        //  lista,  butoniky v strede        
//-------------------------lista pre tlacidla tabu Front-----------------------------
        JPanel pnListaButtonsFront = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //  lista bude hore
        pnListaButtonsFront.setBackground(Color.LIGHT_GRAY);
        panelFrontTab.add(BorderLayout.NORTH, pnListaButtonsFront);
        //-------------------------lista pre tlacidla tabu Front KONIEC-----------------------------

        //----------------------------Tlacidlo Mohutnost------------------------------//
        JButton btMohutnost = new JButton("Mohutnost"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btMohutnost.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_mohutnost(aFront);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btMohutnost.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btMohutnost);
        //----------------------------Tlacidlo Mohutnost KOniec------------------------------//

        //----------------------------Tlacidlo Prehliadka------------------------------//
        JButton btPrehliadka = new JButton("Prehliadka"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrehliadka.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_Prehliadka(aFront);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrehliadka.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btPrehliadka);
        //----------------------------Tlacidlo Prehliadka KOniec------------------------------//

        //----------------------------Tlacidlo Je Prazdny------------------------------//
        JButton btJePrazny = new JButton("Je prazdny"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btJePrazny.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_jePrazdny(aFront);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btJePrazny.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btJePrazny);
        //----------------------------Tlacidlo Je Prazdny KOniec------------------------------//

        //----------------------------Tlacidlo  Pridaj Na Koniec------------------------------//
        JButton btPrNaKon = new JButton("Pridaj na Koniec"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrNaKon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_FrontPridajNaKoniec();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrNaKon.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btPrNaKon);
        //----------------------------Tlacidlo Pridaj Na Koniec KOniec------------------------------//

        //----------------------------Tlacidlo  Odober zo zaciatku------------------------------//
        JButton btOdoberZoZac = new JButton("Odober zo zaciatku"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdoberZoZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_FrontOdoberZoZaciatku();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdoberZoZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btOdoberZoZac);
        //----------------------------Tlacidlo  Odober zo zaciatkuc KOniec------------------------------//

        //----------------------------Tlacidlo  Citaj Zaciatok------------------------------//
        JButton btCitajZac = new JButton("Citaj Zaciatok"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_citajZaciatok(aFront);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btCitajZac);
        //----------------------------Tlacidlo  Citaj Zaciatok KOniec------------------------------//

        //----------------------------Tlacidlo  Citaj Zaciatok------------------------------//
        JButton btCitajKon = new JButton("Citaj Koniec"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajKon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_FrontCitajKoniec();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajKon.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btCitajKon);
        //----------------------------Tlacidlo  Citaj Zaciatok KOniec------------------------------//

        //----------------------------Tlacidlo  Zrus------------------------------//
        JButton btZrus = new JButton("Zrus"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btZrus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zrus(aFront);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btZrus.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsFront.add(btZrus);
        //----------------------------Tlacidlo  Zrus KOniec------------------------------//

    }

    /**
     * Metóda inicializuje plochu, ktorá bude vykresľovaná v hlavnom okne Gui a
     * bude zobrazovať tlačidlá zoznamu a prvky zoznamu
     *
     * @param paTabbedPane-hlavný tabbed pane, kde vložíme nový tab pre front
     * @param klav - klávesnica, ktorú používame pri fronte ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicializujTabZoznam(JTabbedPane paTabbedPane, Klavesnica klav) {
        JPanel panelTabZoznam = new JPanel(new BorderLayout(2, 2));
        panelTabZoznam.add(BorderLayout.CENTER, new Platno(aZoznam));
        paTabbedPane.addTab("Zoznam", panelTabZoznam);
        inicTlacidlaZoznam(panelTabZoznam, klav);

    }

    /**
     * Metóda pridá do tabu zoznam tlačidlá, ktoré bude obsahovať
     *
     * @param panelFrontTab-tab pane zoznam
     * @param klav - klávesnica, ktorú používame pri zoznam ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicTlacidlaZoznam(JPanel panelTabZoznam, Klavesnica klav) {
        //////////////////////////////////////////////////////////////OVLADANIE  TLACIDLAMI////////////////////////////////////////////////////////////////////
        //vytvoríme pomocnú lištu,  ktorú   posadíme   na    SEVER     hlavného formulára
        //a na lištu budeme sádzať jednotlivé tlačidlá,  ku ktorým prihlásime potrebne udalosti
        //najprv lista
        //  lista,  butoniky v strede
        //-------------------------lista pre tlacidla tabu Front-----------------------------
        JPanel pnListaButtonsZoznam = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //  lista bude hore
        pnListaButtonsZoznam.setBackground(Color.LIGHT_GRAY);
        panelTabZoznam.add(BorderLayout.PAGE_START, pnListaButtonsZoznam);

        JPanel pnListaButtonsZoznam2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pnListaButtonsZoznam2.setBackground(Color.LIGHT_GRAY);
        panelTabZoznam.add(BorderLayout.SOUTH, pnListaButtonsZoznam2);
        //-------------------------lista pre tlacidla tabu Front KONIEC-----------------------------

        //a teraz  tlačidlá 
        //----------------------------Tlacidlo Mohutnost------------------------------//
        JButton btMohutnost = new JButton("Mohutnost"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btMohutnost.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_mohutnost(aZoznam);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btMohutnost.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btMohutnost);
        //----------------------------Tlacidlo Mohutnost KOniec------------------------------//

        //----------------------------Tlacidlo Prehliadka------------------------------//
        JButton btPrehliadka = new JButton("Prehliadka"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrehliadka.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_Prehliadka(aZoznam);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrehliadka.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btPrehliadka);
        //----------------------------Tlacidlo Prehliadka KOniec------------------------------//

        //----------------------------Tlacidlo je Prazdny------------------------------//
        JButton btPrAuto = new JButton("Je Prazdny"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrAuto.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_jePrazdny(aZoznam);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrAuto.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btPrAuto);
         //----------------------------Tlacidlo je Prazdny Koniec------------------------------//

        //----------------------------Tlacidlo Nastav Aktualny Na Zaciatok------------------------------//
        JButton btNasAktNaZac = new JButton("Set akt na zac"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btNasAktNaZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_setAktualnyNaZaciatok();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btNasAktNaZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btNasAktNaZac);
         //----------------------------Tlacidlo Nastav Aktualny Na Zaciatok Koniec------------------------------//

        //----------------------------Tlacidlo Nastav Aktualny Na Nasledujuci------------------------------//
        JButton btNasAktNaNasl = new JButton("Set akt na nasl"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btNasAktNaNasl.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_setAktualnyNaNasledujuci();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btNasAktNaNasl.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btNasAktNaNasl);
         //----------------------------Tlacidlo Nastav Aktualny Na Nasledujuci Koniec------------------------------//

        //----------------------------Tlacidlo Zapis na zaciatok------------------------------//
        JButton btZapisNaZac = new JButton("Zapis na zac"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btZapisNaZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zapisNaZaciatok();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btZapisNaZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btZapisNaZac);
         //----------------------------Tlacidlo Zapis na zaciatok Koniec------------------------------//

        //----------------------------Tlacidlo Zapis na aktualny------------------------------//
        JButton btZapNaAkt = new JButton("Zapis na akt"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btZapNaAkt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zapisNaAktualny();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btZapNaAkt.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btZapNaAkt);
         //----------------------------Tlacidlo Zapis na aktualny Koniec------------------------------//

        //----------------------------Tlacidlo Citaj zaciatok------------------------------//
        JButton btCitajZaciatok = new JButton("Citaj Zac"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajZaciatok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_citajZaciatok(aZoznam);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajZaciatok.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btCitajZaciatok);
         //----------------------------Tlacidlo Citaj zaciatok Koniec------------------------------//

        //----------------------------Tlacidlo Citaj aktualny------------------------------//
        JButton btCitajAktual = new JButton("Citaj Akt"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajAktual.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_citajAktualny();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajAktual.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btCitajAktual);
         //----------------------------Tlacidlo Citaj aktualny Koniec------------------------------//

        //----------------------------Tlacidlo Pridaj Na Zaciatok------------------------------//
        JButton btPridajNaZac = new JButton("Pridaj Na Zac"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPridajNaZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pridajNaZaciatok();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPridajNaZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btPridajNaZac);
         //----------------------------Tlacidlo Pridaj Na Zaciatok Koniec------------------------------//

        //----------------------------Tlacidlo Pridaj Za Aktualny------------------------------//
        JButton btPridajZaAkt = new JButton("Pridaj Za Akt"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPridajZaAkt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pridajZaAktualny();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPridajZaAkt.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btPridajZaAkt);
         //----------------------------Tlacidlo Pridaj Za Aktualny Koniec------------------------------//

        //----------------------------Tlacidlo Odober za Aktualnym------------------------------//
        JButton btOdoberZaAkt = new JButton("Odober za Akt"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdoberZaAkt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_odoberZaAktualnym();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdoberZaAkt.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btOdoberZaAkt);
         //----------------------------Tlacidlo Odober za Aktualnym Koniec------------------------------//

        //----------------------------Tlacidlo Odober zo Zaciatku------------------------------//
        JButton btOdoberNaZac = new JButton("Odober zo Zac"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdoberNaZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_odoberZoZaciatku();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdoberNaZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam.add(btOdoberNaZac);
        //----------------------------Tlacidlo Odober zo Zaciatku Koniec------------------------------//

        //----------------------------Tlacidlo  Zrus------------------------------//
        JButton btZrus = new JButton("Zrus"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btZrus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zrus(aZoznam);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btZrus.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonsZoznam2.add(btZrus);
        //----------------------------Tlacidlo  Zrus KOniec------------------------------//
    }

    /**
     * Metóda inicializuje plochu, ktorá bude vykresľovaná v hlavnom okne Gui a
     * bude zobrazovať tlačidlá zásobníka a prvky zásobníka
     *
     * @param paTabbedPane-hlavný tabbed pane, kde vložíme nový tab pre zásobník
     * @param klav - klávesnica, ktorú používame pri zásobníku ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicializujTabZasobnik(JTabbedPane paTabbedPane, Klavesnica klav) {
        JPanel panelJednaTabuZasobnik = new JPanel(new BorderLayout(2, 2));
        panelJednaTabuZasobnik.add(BorderLayout.CENTER, new Platno(aZasobnik));
        paTabbedPane.addTab("Zasobnik", panelJednaTabuZasobnik);
        inicTlacidlaZasobnik(panelJednaTabuZasobnik, klav);
    }

    /**
     * Metóda pridá do tabu zásobník tlačidlá, ktoré bude obsahovať
     *
     * @param panelFrontTab-tab pane zásobník
     * @param klav - klávesnica, ktorú používame pri zásobníku ako reakciu na
     * stlačenie tlačidiel
     */
    private void inicTlacidlaZasobnik(JPanel panelJednaTabuZasobnik, Klavesnica klav) {
        ///////////////////////////////////////////////////////////////OVLADANIE  TLACIDLAMI////////////////////////////////////////////////////////////////////
        //vytvoríme pomocnú lištu,  ktorú   posadíme   na    SEVER     hlavného formulára
        //a na lištu budeme sádzať jednotlivé tlačidlá,  ku ktorým prihlásime potrebne udalosti
        //najprv lista
        //  lista,  butoniky v strede
        //-------------------------lista pre tlacidla tabu Front-----------------------------
        JPanel pnListaButtonZasobnik = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //  lista bude hore
        pnListaButtonZasobnik.setBackground(Color.LIGHT_GRAY);
        panelJednaTabuZasobnik.add(BorderLayout.NORTH, pnListaButtonZasobnik);
        //-------------------------lista pre tlacidla tabu Front KONIEC-----------------------------

        //----------------------------Tlacidlo Mohutnost------------------------------//
        JButton btMohutnost = new JButton("Mohutnost"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btMohutnost.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_mohutnost(aZasobnik);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btMohutnost.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btMohutnost);
        //----------------------------Tlacidlo Mohutnost KOniec------------------------------//

        //----------------------------Tlacidlo Prehliadka------------------------------//
        JButton btPrehliadka = new JButton("Prehliadka"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrehliadka.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_Prehliadka(aZasobnik);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrehliadka.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btPrehliadka);
        //----------------------------Tlacidlo Prehliadka KOniec------------------------------//

        //----------------------------Tlacidlo Je Prazdny------------------------------//
        JButton btPrAuto = new JButton("Je prazdny"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrAuto.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_jePrazdny(aZasobnik);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrAuto.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btPrAuto);
        //----------------------------Tlacidlo Je Prazdny KOniec------------------------------//

        //----------------------------Tlacidlo  Pridaj Na Koniec------------------------------//
        JButton btPrNaKon = new JButton("Vloz na koniec"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btPrNaKon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_vlozNaKoniec();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btPrNaKon.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btPrNaKon);
        //----------------------------Tlacidlo Pridaj Na Koniec KOniec------------------------------//

        //----------------------------Tlacidlo  Odober zo zaciatku------------------------------//
        JButton btOdoberZoZac = new JButton("Vyber z konca"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btOdoberZoZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_vyberZKonca();
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btOdoberZoZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btOdoberZoZac);
        //----------------------------Tlacidlo  Odober zo zaciatkuc KOniec------------------------------//

        //----------------------------Tlacidlo  Citaj Zaciatok------------------------------//
        JButton btCitajZac = new JButton("Citaj Vrchol"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btCitajZac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_citajZaciatok(aZasobnik);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btCitajZac.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btCitajZac);
        //----------------------------Tlacidlo  Citaj Zaciatok KOniec------------------------------//

        //----------------------------Tlacidlo  Zrus------------------------------//
        JButton btZrus = new JButton("Zrus"); 	//  vytvorenie tlacitka 

        //  udalost tlacitka - pouzitie anonymnej vnutornej triedy                         
        btZrus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_zrus(aZasobnik);
            }
        });

        //  udalost klavesnice   -   ak “sme na tlacitku”,  chceme vsetky moznosti klavesnice
        btZrus.addKeyListener(klav);//ak sme na tlacitku tak chceme aby nam stale fungovala klavesnica  

        pnListaButtonZasobnik.add(btZrus);
        //----------------------------Tlacidlo  Zrus KOniec------------------------------//
    }

    /**
     * Metóda inicializuje horné menu hlavného okna GUI, ktoré bude obsahovať
     * položky Súbor a Pomoc
     */
    private void inicializujHorneMenu() {
        ///////////////////////////////////////////////////////////////////////HORNE MENU//////////////////////////////////////////////////////////////////////////
        //Ovládanie  pomocou  menu
        //musíme vytvoriť   “neviditeľnú lištu pre menu”   a   priradiť ju pre nášmu formulár
        //  lista pre horne menu
        JMenuBar mb = new JMenuBar();
        this.setJMenuBar(mb);

        //a potom  popridávať jednotlivé  hlavne položky menu
        //JMenu su vodorovne polozky horneho menu
        //JItem su zvisle polozky jednotlivych vodorovnych poloziek horneho menu
        //  polozky menu na hornej liste
        JMenu mnSubor = new JMenu("Súbor");
        mb.add(mnSubor);

        JMenuItem mniSubor_Koniec = new JMenuItem("Koniec programu");      // polozka v menu
        mniSubor_Koniec.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            @Override
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_koniec();

            }     // 
        });
        mnSubor.add(mniSubor_Koniec);

        mnSubor.addSeparator();

        JMenuItem mniSubor_citZozSuboru = new JMenuItem("Nacitaj zo suboru");      // polozka v menu
        mniSubor_citZozSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_nacitajZoSuboru();

            }     // 
        });
        mnSubor.add(mniSubor_citZozSuboru);
        mnSubor.addSeparator();

        JMenuItem mniSubor_zapisDoSuboru = new JMenuItem("Zapis do suboru");      // polozka v menu
        mniSubor_zapisDoSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                akcia_zapisDoSuboru();

            }     // 

        });
        mnSubor.add(mniSubor_zapisDoSuboru);
        mnSubor.addSeparator();

        //dalsia polozka menu na hornej liste    
        JMenu mnPomoc = new JMenu("Pomoc");
        mb.add(mnPomoc);

        JMenuItem mniPomoc_Pomoc = new JMenuItem("Pomoc");
        mniPomoc_Pomoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_pomoc();
            }
        });
        mnPomoc.add(mniPomoc_Pomoc);

        mnPomoc.addSeparator();							//  separator

        JMenuItem mniPomoc_Info = new JMenuItem("O programe");
        mniPomoc_Info.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                akcia_info();
            }
        });
        mnPomoc.add(mniPomoc_Info);
    }

    /**
     * Metóda zobrazí výzvu na vybranie objektu typu IObjekt ak vyoknávame
     * nejakú akciu, ktorá má objekt ako vstupný parameter
     */
    private void oknoButtonVyberObjektu() {
        final JDialog ramik = new JDialog(); //okno
        ramik.setLayout(new FlowLayout(FlowLayout.CENTER));

        final JButton btPridajKnihu = new JButton("Pridaj Knihu"); //tlacitku s nazvem

        btPridajKnihu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieKnihy();
                ramik.dispose();
            }

        });

        ramik.add(btPridajKnihu);

        final JButton btPridajAutomobil = new JButton("Pridaj Automobil"); //tlacitku s nazvem
        btPridajAutomobil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieAutomobilu();
                ramik.dispose();
            }
        });

        ramik.add(btPridajAutomobil);

        final JButton btPridajOsobu = new JButton("Pridaj Osobu"); //tlacitku s nazvem
        btPridajOsobu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_pridanieOsoby();
                ramik.dispose();
            }
        });

        ramik.add(btPridajOsobu);

        ramik.pack(); //prizpusob velikost okna
        ramik.setLocation(aPoziciaOknaX + this.getWidth() / 2 - ramik.getWidth() / 2, aPoziciaOknaY + this.getHeight() / 2 - ramik.getHeight() / 2); //levy horni roh bude na souradnici [100, 100]
        ramik.setModal(true);
        ramik.setVisible(true); //zobraz okno   

    }

    //--------------------------------------------Pridavanie objektu do ADT-------------------------------
    /**
     * Metóda vytvóri nový objekt typu Osoba a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieOsoby() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie osoby' ");
        pridanie(new Osoba());//priadenie novej osoby do evidencie
        this.repaint();
    }

    /**
     * Metóda vytvóri nový objekt typu Kniha a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieKnihy() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie knihy' "); 
        pridanie(new Kniha());
        this.repaint();
    }

    /**
     * Metóda vytvóri nový objekt typu Automobil a volá metódu pridanie, ktorá
     * nastaví atribút vkladaný aVkladanyObjekt na výtvórený objekt
     */
    private void akcia_pridanieAutomobilu() {
        //JOptionPane.showMessageDialog(this, "tu sa vykoná metóda 'Pribadnie automobilu' ");
        pridanie(new Automobil());
        this.repaint();
    }

    /**
     * Metóda nastaví hodnotu atribútu aVkladanýObjekt na hodnotu prijatú ako
     * parameter
     *
     * @param paObjekt:IObjekt - objekt ktorý sa priradí atribútu
     */
    private void pridanie(IObjekt paObjekt) {
        if (paObjekt.napln() == true) {
            aVkladanyObjekt = paObjekt;
        } else {
            aVkladanyObjekt = null;
        }

    }

//////////////////////////////////////////////////////AKCIE, KTORE DOKAZE PROGRAM VYKONAT/////////////////////////////////////////////////
    /**
     * Metóda predstavuej akciu, ktorá sa vyokná ak v hlavnom menu klikneme na
     * položku súbor a načítaj zo súboru. Na základe atribútu aAktualnyTab
     * rozhodne, ktorý ADT má načítať zo súboru
     */
    private void akcia_nacitajZoSuboru() {
        try {
            if (JOptionPane.showConfirmDialog(this,
                    "Načitanie prvkov spôsobí vymazanie aktuálnych prvkov. Chcete naozaj načítať prvky zo súboru", "Načitanie zo súboru",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                    == JOptionPane.YES_OPTION) {
                //  this.dispose(); 
                aAktualnyTab.nacitajPrvkyZoSuboru();
            }
            this.repaint();

        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Súbor sa nepodarilo otvoriť. Bol vymazaný, nic nebolo ulozene alebo upravený užívatelom priamo. ", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(null, "Nepodarilo sa načitať prvky. Opakujte načitanie./n Ak sa chyba opakuje kontaktujte vydávateľa.", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Súbor nebol nájdený. Bol vymazany uzivatelom priamo z adresara kde sa subory ukladaju. ", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        } catch (NoSuchElementException ex) {
            JOptionPane.showMessageDialog(null, "Súbory boli pozmenené. Niektoré súbory boli pozmenené. Nepodarilo sa načítať všetky prvky. ", "ERROR=pozmeneny subor", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Metóda predstavuej akciu, ktorá sa vyokná ak v hlavnom menu klikneme na
     * položku súbor a zapíš zo súboru. Na základe atribútu aAktualnyTab
     * rozhodne, ktorý ADT má zapísať zo súboru
     */
    private void akcia_zapisDoSuboru() {
        try {
            if (!aAktualnyTab.jePrazdny()) {
                aAktualnyTab.zapisPrvkyDoSuboru();
                JOptionPane.showMessageDialog(this, "Všetky prvky boli úspešne uložené do súboru.");
            } else {
                JOptionPane.showMessageDialog(null, "ADŠ je prázdna. Nie je čo uložiť.", "ERROR=uloženie ADŠ", JOptionPane.ERROR_MESSAGE);
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR=Zápis do súboru", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda predstavuej akciu, ktorá sa vyokná ak v hlavnom menu klikneme na
     * položku pomoc a O Programe. Zobrazí autorov programu.
     */
    private void akcia_info() // info()vola sa pri klknuti na pomoc
    {
        JOptionPane.showMessageDialog(this,
                "                                                                   "
                + "                                                                   \n\n"
                + "GUI Základný Návrh: Jozef Kopecký \n"
                + "Program  : Dominik Suroviak, Patrik Kruželák, Branislav Juriš\n\n",
                "Lineárne štruktúrované datové typy  -  O programe", JOptionPane.INFORMATION_MESSAGE);//nadpis okna akcie info 
        this.repaint();
    }

    /**
     * Metóda vypíše základné operácie programu a operácie, ktoré sú spoločné
     * pre všetky ADT. Túto akciu vyvoláme tak, že v hlavnom menu dáme Pomoc a
     * pomoc
     */
    private void akcia_pomoc() // pomoc()
    {
        JOptionPane.showMessageDialog(this,
                "                                                                   "
                + "                                                                   \n\n"
                + "V           Aktuálny počet prvkov ADT.... \n"
                + "E           Je prázdny ........ \n"
                + "R           Čítaj začiatok pri fronte a zozname, %citaj vrchol pri zásobníku.... \n"
                + "P           Prehliadka .... \n"
                + "S           Ulož ADT do súboru .... \n"
                + "L           Načitaj ADT zo súboru .... \n"
                + "Z           Zruš prvky ADŠ .... \n"
                + ". . . . \\n\nn"
                + "F1    pomoc\n\n"
                + "Esc   koniec\n\n\n"
                + "\n\n\n\n\n",
                "Lineárne štruktúrované datové typy  -  Pomoc", JOptionPane.INFORMATION_MESSAGE); //Nadpis okna nasho problemu 

        this.repaint();
    }

    /**
     * Metóda ukonči program, predtým sa však opýta, či ho chceme naozaj
     * ukončiť. MOžno ju vyvolať stlačením klávesy ESC,krížikom a cez hlavné
     * menu.
     */
    private void akcia_koniec() // koniec()
    {
        if (JOptionPane.showConfirmDialog(this,
                "Chceš skutočne ukončit program", "Ukončenie",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            System.exit(0);
        }
        this.repaint();
    }

    //SPOLOCNE AKCIE PRE ADT
    /**
     * Metóda vypíśe aktuálny počet prvkov aktuálne zobrazeného ADT
     *
     * @param paTyp:ILinearDataTypy - ADŠ, ktorá sa práve pouźíva
     */
    private void akcia_mohutnost(ILinearDataTypy paTyp) {
        JOptionPane.showMessageDialog(this, "Mohutnosť/Počet prvkov:  " + paTyp.mohutnost());
        this.repaint();
    }

    /**
     * Metóda nás informuje o tom či je aktuálne používaná ADŠ prázdna
     *
     * @param paTyp:ILinearDataTypy - ADŠ, ktorá sa práve pouźíva
     */
    private void akcia_jePrazdny(ILinearDataTypy paTyp) {
        if (paTyp.jePrazdny()) {
            JOptionPane.showMessageDialog(this, "Štruktúra JE prázdna");
        } else {
            JOptionPane.showMessageDialog(this, "Štruktúra NIE JE prázdna");
        }
        this.repaint();
    }

    /**
     * Metóda vráti objekt nachádzajúci sa na začiatku zoznamu, frontu alebo
     * prvok nachádzajúci sa na vrchole zásobníka podľa aktuálne pouźívanej sa
     * ADŠ
     *
     * @param paTyp:ILinearDataTypy - ADŠ, ktorá sa práve pouźíva
     */
    private void akcia_citajZaciatok(ILinearDataTypy paTyp) {
        try {
            paTyp.citajZaciatok().zobraz();
        } catch (NullPointerException | ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "V štruktúre nie je vložený zatiaľ žiadny prvok. Vložte prvok", "ERROR Prázdna štruktúra", JOptionPane.ERROR_MESSAGE);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Súbor nebol nájdený. Bol vymazaný alebo upravený užívatlom priamo.", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Nepodarilo sa načitať prvky. Opakujte načitanie./n Ak sa chyba opakuje kontaktujte vydávateľa.", "ERROR=subor sa nepodarilo otvoriť", JOptionPane.ERROR_MESSAGE);
        }
        this.repaint();

    }

    /**
     * Metóda zobrazí všetky prvky(objekty typu IObjetk) ADŠ, ktorá sa práve
     * používa
     *
     * @param paLinTyp:ILinearDataTypy - ADŠ, ktorá sa práve pouźíva
     */
    private void akcia_Prehliadka(ILinearDataTypy paLinTyp) {
        paLinTyp.uplnaPrehliadka(aPoziciaOknaX, aPoziciaOknaY, this.getHeight(), this.getWidth());
    }

    private void akcia_zrus(ILinearDataTypy paLinTyp) {
        if (JOptionPane.showConfirmDialog(this,
                "Chceš skutočne zrušiť ADŠ?", "Zrušenie ADŠ",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            paLinTyp.zrus();
        }

        this.repaint();
    }

    //SPOLOCNE AKCIE PRE ADT KONIEC
    //AKCIE FRONT
    /**
     * Metóda vyvolá metódu ADT Front na pridanie prvku, ktorý sa predtým načíta
     * vyzvou pre užívateľ na koniec frontu
     */
    @SuppressWarnings("empty-statement")
    private void akcia_FrontPridajNaKoniec() {
        oknoButtonVyberObjektu();
        if (aVkladanyObjekt != null) {
            try {
                aFront.pridajNaKoniec(aVkladanyObjekt);
                aVkladanyObjekt = null;
                this.repaint();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Prvok sa nepodarilo pridať do frontu", "ERROR=Prvok sa Nepridal", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    /**
     * Metóda vyvolá metódu ADT Front na odobranie prvku ,ktorý bol do frontu
     * vložený ako prvý, odoberie ho a zobrazí používateľovi
     */
    private void akcia_FrontOdoberZoZaciatku() {
        try {
            aFront.odoberZoZaciatku().zobraz();
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Front je prázdny. Vložte prvok aby ste ho mohli odobrat", "ERROR Prázdny Front", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Front je prázdny. Neni čo odobrať.", "ERROR Prázdny Front", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Prvok sa nepodarilo odobrať z frontu", "ERROR=Prvok sa Neodobral", JOptionPane.ERROR_MESSAGE);;
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Nemozno odobrat zo zaciatku", "ERROR=Softvérová chyba", JOptionPane.ERROR_MESSAGE);
        }
        this.repaint();
    }

    /**
     * Metóda vyvolá metódu ADT Front na ciatnie prvku ,ktorý bol do frontu
     * vložený ako posledný a zobrazí používateľovi
     */
    private void akcia_FrontCitajKoniec() {
        try {
            aFront.citajKoniec().zobraz();
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Nemozno nacitat koniec suboru", "ERROR=Softvérová chyba", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Front je prázdny. Vložte prvok", "ERROR Prázdny Front", JOptionPane.ERROR_MESSAGE);
        }
        this.repaint();
    }
    //AKCIE FRONT KONIEC

    //AKCIE ZASOBNIK
    /**
     * Metóda vyvolá metódu ADT Zásobník na pridanie prvku na vrchol zásobníka,
     * ktorého atribúty, zadá uživateľ pomocou pomocného oknna a pridá ho do
     * zásobníka.
     */
    private void akcia_vlozNaKoniec() {
        oknoButtonVyberObjektu();
        if (aVkladanyObjekt != null) {
            aZasobnik.vloz(aVkladanyObjekt);
            aVkladanyObjekt = null;
        }

        this.repaint();
    }

    /**
     * Metóda vyvolá metódu ADT Zásobník na odobranie prvku z vrcholu zásobníka,
     * odoberie ho a odobraný prvok zobrazí užívateľovi
     *
     */
    private void akcia_vyberZKonca() {
        try {
            aZasobnik.vyber().zobraz();
            this.repaint();
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Zasobnik je prázdny. Vložte prvok", "ERROR Prázdny Zasobnik", JOptionPane.ERROR_MESSAGE);
        }
    }
    //AKCIE ZASOBNIK KONIEC   

    //AKCIE ZOZNAM
    /**
     * Metóda vyvolá metódu ADT Zoznam nastavAktualnyNaZaciatok(). Ak sa nedá
     * ukazovateľ aktuálneho nastaviť, upozorní užívateľa.
     *
     */
    private void akcia_setAktualnyNaZaciatok() {
        if (!aZoznam.jePrazdny()) {
            aZoznam.nastavAktualnyNaZaciatok();
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Zoznam je prázdny, hodnotu aktuálneho neińi možné nastaviť na začiatok", "ERROR=Prázdny zoznam", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam nastavAktualnyNaNasledujuci(), ktorá
     * nastaví hodnotu aktuálneho prvku v zoname na nasledujúci. Ak sa nedá
     * ukazovateľ aktuálneho nastaviť, upozorní užívateľa.
     *
     */
    private void akcia_setAktualnyNaNasledujuci() {
        try {
            aZoznam.nastavAktualnyNaNasledujuci();
            this.repaint();
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "Iterator sa uz nenachadza v zozname. Prekrocil velkost zoznamu.", "ERROR=Iterator prekrocil velkost zoznamu", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam zapisNaZaciatok(aVkladanyObjekt), ktorá
     * nahradí prvok nachádzajúci sa na začiatku iným prvkom Ak sa to nedá ,
     * informuje uživateľa chybovou hláškou.
     *
     */
    private void akcia_zapisNaZaciatok() {

        if (!aZoznam.jePrazdny()) {
            oknoButtonVyberObjektu();
            if (aVkladanyObjekt != null) {
                try {
                    aZoznam.zapisNaZaciatok(aVkladanyObjekt);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Zoznam je zatial prazdny", "ERROR Prázdny Zoznam", JOptionPane.ERROR_MESSAGE);
                }
                aVkladanyObjekt = null;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Zoznam je zatial prazdny", "ERROR Prázdny Zoznam", JOptionPane.ERROR_MESSAGE);
        }

        this.repaint();
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam zzapisNaAktualny(aVkladanyObjekt), ktorá
     * nahradí prvok nachádzajúci sa na indexe aktuálneho prvku iným prvkom Ak
     * sa to nedá , informuje uživateľa chybovou hláškou.
     *
     */
    private void akcia_zapisNaAktualny() {

        if (!aZoznam.jePrazdny()) {
            if (aZoznam.getaAktualnyPrvok() != -1) {
                oknoButtonVyberObjektu();

                if (aVkladanyObjekt != null) {
                    aZoznam.zapisNaAktualny(aVkladanyObjekt);
                    aVkladanyObjekt = null;
                }

            } else {
                JOptionPane.showMessageDialog(null, "Index aktualneho prvku nie je nastaveny", "ERROR Nenastavený aktuálny prvok", JOptionPane.ERROR_MESSAGE);
            }
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Zoznam je zatial prazdny", "ERROR Prazdny zoznam", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Metóda vyvolá metódu ADT Zoznam citajAktualny(), ktorá vráti
     * prvok(objekt) nachádzajúci sa na aktuálnej pozícii. Ak sa to nedá ,
     * informuje uživateľa chybovou hláškou.
     *
     */
    private void akcia_citajAktualny() {
        try {
            aZoznam.citajAktualny().zobraz();
        } catch (RuntimeException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR Nenastavený aktuálny prvok", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Metóda vyvolá metódu ADT Zoznam pridajNaZaciatok(aVkladanyObjekt), ktorá
     * vloží do zoznamu na začiatok nový prvok. Ak sa to nedá , informuje
     * uživateľa chybovou hláškou.
     *
     */
    private void akcia_pridajNaZaciatok() {
        oknoButtonVyberObjektu();
        if (aVkladanyObjekt != null) {
            aZoznam.pridajNaZaciatok(aVkladanyObjekt);
            aVkladanyObjekt = null;
        }
        this.repaint();
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam pridajZaAktualny(IObjekt), ktorá vloží do
     * zoznamu za pozíciu aktuálneho prvku nový prvok. Ak sa to nedá , informuje
     * uživateľa chybovou hláškou.
     *
     */
    private void akcia_pridajZaAktualny() {
        try {
            aZoznam.pridajZaAktualny(null);
            aZoznam.odoberZaAkt();
            oknoButtonVyberObjektu();
            if (aVkladanyObjekt != null) {
                aZoznam.pridajZaAktualny(aVkladanyObjekt);
                aVkladanyObjekt = null;
            }
            this.repaint();
        } catch (RuntimeException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERROR Nenastavený aktuálny prvok", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam odoberZoZac()), ktorá odoberie zo zoznamu
     * prvok nachádzajúci sa na začiatku zoznamu. Okrem toho tento prvok ešte
     * raz zobrazí.
     */
    private void akcia_odoberZoZaciatku() {
        if (!aZoznam.jePrazdny()) {
            aZoznam.odoberZoZac().zobraz();
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Do zoznamu nebol zatial vlozeny ziadny prvok. Nie je co odobrat.", "ERROR Nevlozeny ziadny prvok", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metóda vyvolá metódu ADT Zoznam odoberZaAkt(), ktorá odoberie zo zoznamu
     * prvok nachádzajúci sa na aktuálnej pozícii ukazovateľa. Okrem toho tento
     * prvok ešte raz zobrazí.
     */
    private void akcia_odoberZaAktualnym() {
        if (aZoznam.getaAktualnyPrvok() != -1) {
            aZoznam.odoberZaAkt().zobraz();
            this.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Aktualny prvok nebol este nastaveny. Nastavte ho.", "ERROR Nenastaveny aktualny prvok", JOptionPane.ERROR_MESSAGE);
        }
    }

    //AKCIE ZOZNAM KONIEC  
    /**
     * Trieda rozhoduje o tom, ktorá akcia bude vyvolaná, ak bolo stlačené na
     * klávvesnici nejaké tlačidlo.
     */
    private class Klavesnica extends KeyAdapter // vnorena trieda Klavesnica
    {

        @Override
        public void keyPressed(KeyEvent e) // ked stlacim klavesu, tak vznikne nejaka instancia  triedy KeyEvent a vlozi sa do tato instancia bude parametrom tejto funkcie
        {
            //  System.out.println( "Stisol som " + e.getKeyCode() );   // kontrolny vypis o stisku klavesy

            switch (e.getKeyCode()) {                             // vyber cinnosti podla klavesy

                case KeyEvent.VK_ESCAPE:
                    akcia_koniec();
                    break;

                case KeyEvent.VK_F1:
                    akcia_pomoc();
                    break;

                case KeyEvent.VK_V:
                    akcia_mohutnost(aAktualnyTab);
                    break;

                case KeyEvent.VK_E:
                    akcia_jePrazdny(aAktualnyTab);
                    break;

                case KeyEvent.VK_R:
                    akcia_citajZaciatok(aAktualnyTab);
                    break;

                case KeyEvent.VK_P:
                    akcia_Prehliadka(aAktualnyTab);
                    break;

                case KeyEvent.VK_S:
                    akcia_zapisDoSuboru();
                    break;

                case KeyEvent.VK_L:
                    akcia_nacitajZoSuboru();
                    break;

                case KeyEvent.VK_Z:
                    akcia_zrus(aAktualnyTab);
                    break;

                // tu budeme pridavat
            } // switch
        }  // keyPressed
    }  // Klavesnica

    /**
     * Metóda main spustí program
     *
     * @param args
     */
    public static void main(String[] args) {

        JOptionPane.showMessageDialog(new GUI(),
                "\nVitajte ...\n\n( Pomoc zobrazíte stlačením klávesy F1 ) \n\n", "Štart programu", JOptionPane.INFORMATION_MESSAGE);

    }

}

package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;

/**
 * Created by Dominik on 29.3.2015.
 */
public class RoadPoint {
    private Suradnica aSuradnica;
    private TypBodu aTypBodu = TypBodu.BODCESTYPRENAVIGACIUDEFAULT;


    public RoadPoint(Suradnica paSuradnica, TypBodu paTypBodu) {
        this.aSuradnica = paSuradnica;
        aTypBodu = paTypBodu;
    }

    public TypBodu getTypBodu() {
        return aTypBodu;
    }

    public Suradnica getSuradnica() {
        return aSuradnica;
    }

    public void setSuradnica(Suradnica paSuradnica) {
        aSuradnica = paSuradnica;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoadPoint)) return false;

        RoadPoint that = (RoadPoint) o;

        if (!aSuradnica.equals(that.aSuradnica)) return false;
        if (aTypBodu != that.aTypBodu) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = aSuradnica.hashCode();
        result = 31 * result + aTypBodu.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Bod: " + aSuradnica.toString();
    }

    public enum TypBodu implements Serializable {
        OBJEKTALEBOZVIERA,
        BODCESTYPRENAVIGACIUDEFAULT,
        BODCESTYVYTVORENYPOUZIVATELOM;

    }

    public static class Suradnica implements Serializable {

        private Coordinate aLatitude;
        private Coordinate aLongitude;

        public Suradnica(Coordinate aLatitude, Coordinate aLongitude) {
            this.aLatitude = aLatitude;
            this.aLongitude = aLongitude;
        }

        public Coordinate getaLatitude() {
            return aLatitude;
        }

        public Coordinate getaLongitude() {
            return aLongitude;
        }

        @Override
        public String toString() {
            return "Suradnica{" +
                    "aLatitude=" + aLatitude +
                    ", aLongitude=" + aLongitude +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Suradnica)) return false;

            Suradnica suradnica = (Suradnica) o;

            if (!aLatitude.equals(suradnica.aLatitude)) return false;
            if (!aLongitude.equals(suradnica.aLongitude)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = aLatitude.hashCode();
            result = 31 * result + aLongitude.hashCode();
            return result;
        }
    }


    public static class Coordinate implements Serializable {
        private String aDegree;
        private String aMinutes;
        private String aSeconds;
        private double aDecRepresentation;
        private boolean isSetDecValue;


        public Coordinate(String aDegree, String aMinutes, String aSeconds) {
            this.aDegree = aDegree;
            this.aMinutes = aMinutes;
            this.aSeconds = aSeconds;
            isSetDecValue = false;
        }

        public Coordinate(double paDecRepresentationCoordinate) {
            aDecRepresentation = paDecRepresentationCoordinate;
            isSetDecValue = true;
        }

        public double getDECRepresentationOfCoordinate() {
            if (isSetDecValue) {
                return aDecRepresentation;
            }
            double deg = Double.parseDouble(aDegree);
            double min = Double.parseDouble(aMinutes);
            double sec = Double.parseDouble(aSeconds);
            return deg + min / 60 + sec / 3600;
        }

        public String getaDegree() {
            return aDegree;
        }

        public String getaMinutes() {
            return aMinutes;
        }

        public String getaSeconds() {
            return aSeconds;
        }


        @Override
        public String toString() {
            if (!isSetDecValue)
                return aDegree + ":" + aMinutes + ":" + aSeconds;
            else {
                return "" + aDecRepresentation;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Coordinate)) return false;

            Coordinate that = (Coordinate) o;

            if (!isSetDecValue) {
                if (!aDegree.equals(that.aDegree)) return false;
                if (!aMinutes.equals(that.aMinutes)) return false;
                if (!aSeconds.equals(that.aSeconds)) return false;
            } else {
                if (this.getDECRepresentationOfCoordinate() != that.getDECRepresentationOfCoordinate())
                    return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = aDegree.hashCode();
            result = 31 * result + aMinutes.hashCode();
            result = 31 * result + aSeconds.hashCode();
            return result;
        }
    }


}

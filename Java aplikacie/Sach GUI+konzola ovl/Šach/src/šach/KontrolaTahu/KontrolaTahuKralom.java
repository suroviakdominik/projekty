/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import šach.hra.Hrac;

/**
 * Kontroluje ťah kráľom na základe aktuálnych a nových súradníc.
 *
 * @author Dominik
 */
public class KontrolaTahuKralom implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráčom , ktorý
     * vykonáva ťah.
     *
     * @param paNovaXPozicia
     * @param paNovaYPozicia
     * @param paHrac
     */
    public KontrolaTahuKralom(int paNovaXPozicia, int paNovaYPozicia, Hrac paHrac) {
        this.aNovaXPozicia = paNovaXPozicia;
        this.aNovaYPozicia = paNovaYPozicia;
        this.aHrac = paHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    /**
     * skontroluje ťah kráľom. Pri kontrole použije ďalšie metódy tejto triedy,
     * ktorých prístupový typ je private. Ak je ťah platný vráti true, inak
     * vráti false.
     *
     * @return true ak je ťah platný, inak false.
     */
    @Override
    public boolean skontrolujTah() {

        if (((Math.abs(aSucasnaXPozicia - aNovaXPozicia) == 1) || (Math.abs(aSucasnaXPozicia - aNovaXPozicia) == 0)) && ((Math.abs(aSucasnaYPozicia - aNovaYPozicia) == 1) || ((Math.abs(aSucasnaYPozicia - aNovaYPozicia) == 0)))) {
            if ((aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][aNovaXPozicia] == null) || (!aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][aNovaXPozicia].getFarba().equals(aHrac.getFarbaFigurok()))) {
                return moznoSaPresunutBezOhrozenia();
            }
        }

        return false;
    }

    /**
     * kontroluje, či nebudem po presune ohrozený protihráčovou figúrkou.
     *
     * @return Vráti false ak som ohrozený, inak vráti true.
     */
    private boolean moznoSaPresunutBezOhrozenia() {
        boolean moznoSaPosunutBezOhrozenia;
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyPesiakom();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyKonom();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyDamou();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyVezou();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyKralovnou();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        moznoSaPosunutBezOhrozenia = nieSomOhrozenyKralom();
        if (moznoSaPosunutBezOhrozenia == false) {
            return false;
        }
        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia pešiakom.
     *
     * @return Ak by bol kráľ ohrozený pešiakom vráti false, inak true.
     */
    private boolean nieSomOhrozenyPesiakom() {
        if (aHrac.getFarbaFigurok().equals("red")) {
            if (((aNovaYPozicia + 1) >= 0) && ((aNovaYPozicia + 1) <= 7) && ((aNovaXPozicia + 1) >= 0) && ((aNovaXPozicia + 1) <= 7)) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia + 1] != null) {
                    if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia + 1].getMeno().equals("Pešiak")) {
                        if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia + 1].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) {
                            return false;
                        }
                    }
                }
            }

            if (((aNovaYPozicia + 1) >= 0) && ((aNovaYPozicia + 1) <= 7) && ((aNovaXPozicia - 1) >= 0) && ((aNovaXPozicia - 1) <= 7)) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia - 1] != null) {
                    if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia - 1].getMeno().equals("Pešiak")) {
                        if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia + 1][aNovaXPozicia - 1].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) {
                            return false;
                        }
                    }
                }
            }
        } else if (aHrac.getFarbaFigurok().equals("green")) {
            if (((aNovaYPozicia - 1) >= 0) && ((aNovaYPozicia - 1) <= 7) && ((aNovaXPozicia + 1) >= 0) && ((aNovaXPozicia + 1) <= 7)) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia + 1] != null) {
                    if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia + 1].getMeno().equals("Pešiak")) {
                        if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia + 1].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) {
                            return false;
                        }
                    }
                }
            }
            if (((aNovaYPozicia - 1) >= 0) && ((aNovaYPozicia - 1) <= 7) && ((aNovaXPozicia - 1) >= 0) && ((aNovaXPozicia - 1) <= 7)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia - 1] != null)) {
                    if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia - 1].getMeno().equals("Pešiak")) {
                        if (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia - 1][aNovaXPozicia - 1].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia koňom.
     *
     * @return Ak by bol kráľ ohrozený koňom vráti false, inak true.
     */
    private boolean nieSomOhrozenyKonom() {
        for (int i = aNovaYPozicia - 2; i <= aNovaYPozicia + 2; i++) {
            for (int j = aNovaXPozicia - 2; j <= aNovaXPozicia + 2; j++) {
                if (!(aNovaXPozicia == j) && !(aNovaYPozicia == i) && !(i == j)) {
                    try {
                        if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals("Kôň") && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())))) {
                            return false;
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        break;
                    }
                }
            }
        }
        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia dámou.
     *
     * @return Ak by bol kráľ ohrozený dámou vráti false, inak true.
     */
    private boolean nieSomOhrozenyDamou() {
        boolean lpNasielSomInuFigurkuVSmerOsiXAY = false;
        boolean lpNasielSomInuFigurkuProtiOsiXVSmerOsiY = false;
        int j = aNovaXPozicia;
        int k = aNovaXPozicia;
        for (int i = aNovaYPozicia + 1; (i <= 7); i++) {

            j += 1;
            if ((!lpNasielSomInuFigurkuVSmerOsiXAY) && (j <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals("Dáma")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuVSmerOsiXAY = true;
                }
            }

            k -= 1;
            if ((!lpNasielSomInuFigurkuProtiOsiXVSmerOsiY) && (k >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getMeno().equals("Dáma")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuProtiOsiXVSmerOsiY = true;
                }
            }
        }

        boolean lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY = false;
        boolean lpNasielSomInuFigurkuProtiOsiXAProtiOsiY = false;
        j = aNovaXPozicia;
        k = aNovaXPozicia;
        for (int i = aNovaYPozicia - 1; (i >= 0); i--) {

            j += 1;
            if ((!lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY) && (j <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals("Dáma")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY = true;
                }
            }

            k -= 1;
            if ((!lpNasielSomInuFigurkuProtiOsiXAProtiOsiY) && (k >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getMeno().equals("Dáma")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuProtiOsiXAProtiOsiY = true;
                }
            }
        }
        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia vežou.
     *
     * @return Ak by bol kráľ ohrozený vežou vráti false, inak true.
     */
    private boolean nieSomOhrozenyVezou() {
        int j = aNovaYPozicia - 2;//aby som isiel naraz aj proti osi y
        boolean bolUzNajdenyVSmereOsiX = false;
        boolean bolUzNajdenyVSmereOsiY = false;
        boolean bolUzNajdenyProtiSmeruOsiX = false;
        boolean bolUzNajdenyProtiSmeruOsiY = false;

        for (int i = aNovaYPozicia + 1; ((i <= 7) || (j >= 0)); i++, j--) {

            if ((!bolUzNajdenyVSmereOsiY) && (i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia].getMeno().equals("Veza"))) {
                return false;
            } else if ((!bolUzNajdenyProtiSmeruOsiY) && (j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia].getMeno().equals("Veza"))) {
                return false;
            } else if ((j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null)) {
                bolUzNajdenyProtiSmeruOsiY = true;
            } else if ((i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null)) {
                bolUzNajdenyVSmereOsiY = true;
            }

        }

        j = aNovaXPozicia - 2;

        for (int i = aNovaXPozicia + 1; ((i <= 7) || (j >= 0)); i++, j--) {

            if ((!bolUzNajdenyVSmereOsiX) && (i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i].getMeno().equals("Veza"))) {
                return false;
            } else if ((!bolUzNajdenyProtiSmeruOsiX) && (j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j].getMeno().equals("Veza"))) {
                return false;
            } else if ((i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null)) {
                bolUzNajdenyVSmereOsiX = true;
            } else if ((j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null)) {
                bolUzNajdenyProtiSmeruOsiX = true;
            }

        }
        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia kráľovnou.
     *
     * @return Ak by bol kráľ ohrozený kráľovnou vráti false, inak true.
     */
    private boolean nieSomOhrozenyKralovnou() {
        int j = aNovaYPozicia - 1;//aby som isiel naraz aj proti osi y
        boolean bolUzNajdenyVSmereOsiX = false;
        boolean bolUzNajdenyVSmereOsiY = false;
        boolean bolUzNajdenyProtiSmeruOsiX = false;
        boolean bolUzNajdenyProtiSmeruOsiY = false;

        for (int i = aNovaYPozicia + 1; ((i <= 7) || (j >= 0)); i++, j--) {
            if ((!bolUzNajdenyVSmereOsiY) && (i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia].getMeno().equals("Kralovna"))) {
                return false;
            } else if ((!bolUzNajdenyProtiSmeruOsiY) && (j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia].getMeno().equals("Kralovna"))) {
                return false;
            } else if ((j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[j][aNovaXPozicia] != null)) {
                bolUzNajdenyProtiSmeruOsiY = true;
            } else if ((i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][aNovaXPozicia] != null)) {
                bolUzNajdenyVSmereOsiY = true;
            }

        }
        j = aNovaXPozicia - 1;
        for (int i = aNovaXPozicia + 1; ((i <= 7) || (j >= 0)); i++, j--) {

            if ((!bolUzNajdenyVSmereOsiX) && (i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i].getMeno().equals("Kralovna"))) {
                return false;
            } else if ((!bolUzNajdenyProtiSmeruOsiX) && (j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j].getMeno().equals("Kralovna"))) {
                return false;
            } else if ((i <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][i] != null)) {
                bolUzNajdenyVSmereOsiX = true;
            } else if ((j >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][j] != null)) {
                bolUzNajdenyProtiSmeruOsiX = true;
            }

        }

        boolean lpNasielSomInuFigurkuVSmerOsiXAY = false;
        boolean lpNasielSomInuFigurkuProtiOsiXVSmerOsiY = false;
        int l = aNovaXPozicia;
        int k = aNovaXPozicia;
        for (int i = aNovaYPozicia + 1; (i <= 7); i++) {

            l += 1;
            if ((!lpNasielSomInuFigurkuVSmerOsiXAY) && (l <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][l] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][l].getMeno().equals("Kralovna")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][l].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuVSmerOsiXAY = true;
                }
            }

            k -= 1;
            if ((!lpNasielSomInuFigurkuProtiOsiXVSmerOsiY) && (k >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getMeno().equals("Kralovna")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuProtiOsiXVSmerOsiY = true;
                }
            }
        }

        boolean lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY = false;
        boolean lpNasielSomInuFigurkuProtiOsiXAProtiOsiY = false;
        l = aNovaXPozicia;
        k = aNovaXPozicia;
        for (int i = aNovaYPozicia - 1; (i >= 0); i--) {

            l += 1;
            if ((!lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY) && (l <= 7) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][l] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][l].getMeno().equals("Kralovna")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][l].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuVSmerOsiXAProtiOsiY = true;
                }
            }

            k -= 1;
            if ((!lpNasielSomInuFigurkuProtiOsiXAProtiOsiY) && (k >= 0) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k] != null)) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getMeno().equals("Kralovna")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][k].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                    return false;
                } else {
                    lpNasielSomInuFigurkuProtiOsiXAProtiOsiY = true;
                }
            }
        }

        return true;
    }

    /**
     * čiastková metóda, ktorá sa použije na kontrolovanie ohrozenia kráľom.
     *
     * @return Ak by bol kráľ ohrozený kráľom vráti false, inak true.
     */
    private boolean nieSomOhrozenyKralom() {
        for (int i = aNovaYPozicia - 1; i <= aNovaYPozicia + 1; i++) {
            for (int j = aNovaXPozicia - 1; j <= aNovaXPozicia + 1; j++) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia)) {
                    break;
                }
                try {
                    if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals("Kral")) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getaProtiHrac().getFarbaFigurok()))) {
                        return false;
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    break;
                }
            }
        }
        return true;

    }
}

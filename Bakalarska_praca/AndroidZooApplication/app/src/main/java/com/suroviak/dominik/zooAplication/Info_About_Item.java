package com.suroviak.dominik.zooAplication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;


/**
 * @author Dominik
 */
public class Info_About_Item implements Serializable {
    private ArrayList<String> aDomainsOfFilesInCurrentItem = null;
    private String aPathOfFilesOfurrentItemInDevice = null;
    private Date aDateOfEditOrCreateFile = null;
    private String aOriginalName = null;
    private String aParentDirectory = null;
    private double aSuradnicaZemepisnaDlzka = -1;
    private double aSuradnicaZemepisnaSirka = -1;
    private String aSuradnicaZemSirkaText = null;
    private String aSuradniceZemDlzkaText = null;
    private static final long serialVersionUID = 1L;

    public Info_About_Item(String paOriginalName, String paParentDirectory, String paPathOfFilesOfurrentItemInDevice) {
        aOriginalName = paOriginalName;
        aPathOfFilesOfurrentItemInDevice = paPathOfFilesOfurrentItemInDevice;
        aParentDirectory = paParentDirectory;
        aDomainsOfFilesInCurrentItem = new ArrayList<>();
    }

    public void setaZemepisnaDlzka(double paSuradnicaZemepisnaDlzka) {
        this.aSuradnicaZemepisnaDlzka = paSuradnicaZemepisnaDlzka;
    }

    public void setaZemepisnaSirka(double paSuradnicaZemepisnaSirka) {
        this.aSuradnicaZemepisnaSirka = paSuradnicaZemepisnaSirka;
    }


    public void setDateOfEditOrCreateFile(Date paDateOfEditOrCreateFile) {
        this.aDateOfEditOrCreateFile = paDateOfEditOrCreateFile;
    }

    public void setOriginalName(String paOriginalName) {
        this.aOriginalName = paOriginalName;
    }

    public void setParentDirectory(String paParentDirectory) {
        this.aParentDirectory = paParentDirectory;
    }

    public ArrayList<String> getDomainsOfFilesInCurrentItem() {
        return aDomainsOfFilesInCurrentItem;
    }

    public String getRelativePathOfFilesOfurrentItemInDevice() {
        return aPathOfFilesOfurrentItemInDevice;
    }

    public Date getDateOfEditOrCreateFile() {
        return aDateOfEditOrCreateFile;
    }

    public String getOriginalName() {
        return aOriginalName;
    }

    public String getParentDirectory() {
        return aParentDirectory;
    }

    public double getZemepisnaDlzka() {
        return aSuradnicaZemepisnaDlzka;
    }

    public double getZemepisnaSirka() {
        return aSuradnicaZemepisnaSirka;
    }

    public void setSuradnicaZemSirkaText(String aSuradnicaZemSirkaText) {
        this.aSuradnicaZemSirkaText = aSuradnicaZemSirkaText;
    }

    public void setSuradniceZemDlzkaText(String aSuradniceZemDlzkaText) {
        this.aSuradniceZemDlzkaText = aSuradniceZemDlzkaText;
    }

    public String getZemSirkaText() {
        return aSuradnicaZemSirkaText;
    }

    public String getZemDlzkaText() {
        return aSuradniceZemDlzkaText;
    }

    @Override
    public String toString() {
        return aOriginalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Info_About_Item)) return false;

        Info_About_Item that = (Info_About_Item) o;

        if (Double.compare(that.aSuradnicaZemepisnaDlzka, aSuradnicaZemepisnaDlzka) != 0)
            return false;
        if (Double.compare(that.aSuradnicaZemepisnaSirka, aSuradnicaZemepisnaSirka) != 0)
            return false;
        if (!aDateOfEditOrCreateFile.equals(that.aDateOfEditOrCreateFile)) return false;
        if (!aDomainsOfFilesInCurrentItem.equals(that.aDomainsOfFilesInCurrentItem)) return false;
        if (!aOriginalName.equals(that.aOriginalName)) return false;
        if (aParentDirectory != null ? !aParentDirectory.equals(that.aParentDirectory) : that.aParentDirectory != null)
            return false;
        if (!aPathOfFilesOfurrentItemInDevice.equals(that.aPathOfFilesOfurrentItemInDevice))
            return false;
        if (aSuradnicaZemSirkaText != null ? !aSuradnicaZemSirkaText.equals(that.aSuradnicaZemSirkaText) : that.aSuradnicaZemSirkaText != null)
            return false;
        if (aSuradniceZemDlzkaText != null ? !aSuradniceZemDlzkaText.equals(that.aSuradniceZemDlzkaText) : that.aSuradniceZemDlzkaText != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = aDomainsOfFilesInCurrentItem.hashCode();
        result = 31 * result + aPathOfFilesOfurrentItemInDevice.hashCode();
        result = 31 * result + aDateOfEditOrCreateFile.hashCode();
        result = 31 * result + aOriginalName.hashCode();
        result = 31 * result + (aParentDirectory != null ? aParentDirectory.hashCode() : 0);
        temp = Double.doubleToLongBits(aSuradnicaZemepisnaDlzka);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(aSuradnicaZemepisnaSirka);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (aSuradnicaZemSirkaText != null ? aSuradnicaZemSirkaText.hashCode() : 0);
        result = 31 * result + (aSuradniceZemDlzkaText != null ? aSuradniceZemDlzkaText.hashCode() : 0);
        return result;
    }
}


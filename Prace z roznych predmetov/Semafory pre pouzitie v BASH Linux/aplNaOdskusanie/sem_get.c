#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
int pociatocnaHodnotaSemafora = atoi(argv[1]);
//printf("i:  %d  \n",pociatocnaHodnotaSemafora);na debugovanie

int pocetVytvorenychSemaforov= 1;
key_t klucSemafora=getpid();
int idSemafora = semget(klucSemafora,pocetVytvorenychSemaforov,0666 | IPC_CREAT);
  if (idSemafora < 0){
        printf ("chyba pri vytarani semafora. Exit. \n");
        return -1;
    }
printf("ID Semafora:  %d  \n",idSemafora);
int nastavilaSa = semctl(idSemafora,0,SETVAL,pociatocnaHodnotaSemafora);
if(nastavilaSa<0)
{
printf ("chyba pri nastavovani pociatocnej hodnoty semafora. Semafor bude zmazany.Exit. \n");
semctl(idSemafora,0,IPC_RMID);
return -1;
}
printf("Hodnota semafora:  %d  \n",semctl(idSemafora,0,GETVAL));

return idSemafora;
}
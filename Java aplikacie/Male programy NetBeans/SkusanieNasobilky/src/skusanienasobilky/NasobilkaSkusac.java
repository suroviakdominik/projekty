/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skusanienasobilky;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class NasobilkaSkusac {

    private Integer aCinitel1;
    private Integer aCinitel2;
    private Random aGenerator;
    private int aPocetPrikladovVTeste;
    private int aZostavaVygenerovat;
    private int aPocetSpravnch;

    public NasobilkaSkusac(int paPocetPrikladovVTeste) {
        aGenerator = new Random();
        aPocetPrikladovVTeste = paPocetPrikladovVTeste;
        aZostavaVygenerovat = paPocetPrikladovVTeste;
        aPocetSpravnch = 0;
    }

    public int getaPocetPrikladovVTeste() {
        return aPocetPrikladovVTeste;
    }

    public void setaPocetPrikladovVTeste(int aPocetPrikladovVTeste) {
        this.aPocetPrikladovVTeste = aPocetPrikladovVTeste;
    }

    public Integer generuCinitel1() {
        aCinitel1 = aGenerator.nextInt(50) - 25;
        return aCinitel1;
    }

    public Integer generuCinitel2() {
        aCinitel2 = aGenerator.nextInt(50) - 25;
        return aCinitel2;
    }

    public boolean jeSpravnyVysledok(Integer paVysledok) {
        aZostavaVygenerovat--;
        if ((aCinitel1 * aCinitel2) == paVysledok) {
            aPocetSpravnch++;
            return true;
        }
        return false;
    }

    public void reset() {
        aZostavaVygenerovat = aPocetPrikladovVTeste;
        aPocetSpravnch = 0;
    }

    public int getaZostavaVygenerovat() {
        return aZostavaVygenerovat;
    }

    public int getaPocetSpravnch() {
        return aPocetSpravnch;
    }
    
    
}

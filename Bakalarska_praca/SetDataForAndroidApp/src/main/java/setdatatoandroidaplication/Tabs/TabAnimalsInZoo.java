/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JTextFieldWithHint;
import MyGUIJComponent.JTextAreaWithHint;
import GeoLocationComponents.JLatLonPanel;
import EditorsForInfo_Files.InfoSuboryENUM;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import com.suroviak.dominik.zooAplication.Navigation.BodNaCeste;
import com.suroviak.dominik.zooAplication.Navigation.Cesta;
import com.suroviak.dominik.zooAplication.Navigation.Coordinate;
import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.Dimension;
import java.awt.Font;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JFileChooser;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;

import setdatatoandroidaplication.DataTransferer;

/**
  *Úlohou tejto triedy je poskytnúť používateľovi grafické rozhranie 
  * pre vytváranie a editáciu existujúcich zvierat. 
 * @author Dominik
 */
public class TabAnimalsInZoo extends Tab {

    private JComboBox<Info_About_Item> aCategoryList = null;
    private JComboBox<Info_About_Item> aAnimalList = null;
    private JTextAreaWithHint aTextArea = null;
    private JTextField aPathOfChoosenPicture = null;
    private JTextFieldWithHint aNameOfNewCategory = null;
    private JTextFieldWithHint aNameOfNewAnimal = null;
    private JPanel aShowerOfAnimalPicturePanel = null;
    private JLatLonPanel aSuradniceZemDlzka = null;
    private JLatLonPanel aSuradniceZemSirka = null;
        private JButton aButtonDeletePicture=null;
         private Info_About_Item aLastSelectedAnimal = null;
    public final int PREFFERED_HIGHT = 30, PREFFERED_WIDTH = 160, MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 20, MARGIN_RIGHT = 20;

   

    /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabAnimalsInZoo(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNazovTabu, paCommandProcessingForTab);
        initializeMainTabPanelComponents();
    }

    /**
     * inicializuje vizuálne komponenty tabu
     */
    private void initializeMainTabPanelComponents() {
        JPanel strPanel = getMiddleMainTabPanel();
        strPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);

        c.gridy = 0;
        c.weightx = 0.33;
        c.weighty = 0.05;
        addComponentsForAnimalCategoryCreate(strPanel, c, aGlobalKeyListener);
        c.gridy = 1;
        c.weightx = 0.33;

        addComponentsForAnimalCreate(strPanel, c, aGlobalKeyListener);

        c.gridy = 2;
        c.weightx = 0.33;
        addComponentsForAnimalGEOPositionOnMap(strPanel, c, aGlobalKeyListener);

        c.gridy = 3;
        c.fill = GridBagConstraints.BOTH;
        addComponentsForAnimalPictureAdd(strPanel, c,aGlobalKeyListener);
        c.weighty = 0.80;
        c.gridy = 4;
        c.gridx = 0;
        c.gridwidth = 2;
        c.weightx = 0.5;
        addComponentsForAnimalDescriptionEdit(strPanel, c, aGlobalKeyListener);
    }

    /**
     * Metóda vytvorí vizuálne komponenty pre vytváranie kategórií zvierat
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalCategoryCreate(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {

        try {
            Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
            JLabel label = new JLabel("Vyberte si jednu z exitujúcich kategórií: ");
            label.addKeyListener(paKeyListener);
            label.setFont(FONT);
            label.setPreferredSize(d);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            label.setBorder(BorderFactory.createCompoundBorder(border,
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            paMainTabPanel.add(label, c);

            aCategoryList = new JComboBox<>();
            aCategoryList.setPreferredSize(d);
            updateCategories();
            aCategoryList.setFont(FONT);
            aCategoryList.setEditable(false);
            aCategoryList.addKeyListener(paKeyListener);
            paMainTabPanel.add(aCategoryList, c);
            aCategoryList.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    if (aTextArea.boliZmenyUlozene()) {
                        updateAnimals();
                    } else {
                        doYouWantToSaveAllChanges((Info_About_Item) aAnimalList.getSelectedItem());
                        updateAnimals();
                    }
                }

            });

            aCategoryList.getEditor().getEditorComponent().addKeyListener(paKeyListener);

            aNameOfNewCategory = new JTextFieldWithHint("Zadajte názov novej kategórie a stlačte ENTER: ");
            //textF.setPreferredSize(new Dimension(PREFFERED_WIDTH + 100, PREFFERED_HIGHT));
            aNameOfNewCategory.setFont(FONT);
            aNameOfNewCategory.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, PREFFERED_HIGHT));

            paMainTabPanel.add(aNameOfNewCategory, c);
            aNameOfNewCategory.addKeyListener(paKeyListener);
            aNameOfNewCategory.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent ke) {
                    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                        aNameOfNewCategory.setIsTextNeeded(true);
                        if (aTextArea.boliZmenyUlozene()) {
                            actionAddNewAnimalCategory();
                        } else {
                            doYouWantToSaveAllChanges((Info_About_Item) aAnimalList.getSelectedItem());
                            actionAddNewAnimalCategory();
                        }
                    }
                }
            });

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "TabPridajZvieraChyba\n metóda:pridajComboBoxVyberKategorie(...)\n" + e.getMessage(), "ERROR=Vypis Kategorie", JOptionPane.ERROR_MESSAGE);
        }
    }

   /**
     * Metóda vytvorí vizuálne komponenty pre vytváranie  zvierat
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalCreate(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        try {
            Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
            JLabel label = new JLabel("Vyberte si jedno z existujúcich zvierat :");
            label.setFont(FONT);
            label.setPreferredSize(d);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            label.setBorder(BorderFactory.createCompoundBorder(border,
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            paMainTabPanel.add(label, c);

            aAnimalList = new JComboBox<>();
            //aAnimalList.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
            updateAnimals();
            if (aAnimalList.getSelectedItem() != null) {
                aLastSelectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
            }
            aAnimalList.setEditable(false);
            aAnimalList.setPreferredSize(d);
            aAnimalList.addKeyListener(paKeyListener);
            aAnimalList.setFont(FONT);
            paMainTabPanel.add(aAnimalList, c);
            aAnimalList.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (aTextArea.boliZmenyUlozene()) {
                        updateGEOCoordinates();
                        updatePicture();
                        updateTextArea();

                    } else {
                        doYouWantToSaveAllChanges(aLastSelectedAnimal);
                        updateGEOCoordinates();
                        updatePicture();
                        updateTextArea();
                    }
                    aPathOfChoosenPicture.setText("");
                    aLastSelectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
                }

            });

            aNameOfNewAnimal = new JTextFieldWithHint("Zadajte názov nového zvieraťa a stlačte ENTER: ");
            aNameOfNewAnimal.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, PREFFERED_HIGHT));
            aNameOfNewAnimal.setFont(FONT);
            paMainTabPanel.add(aNameOfNewAnimal, c);

            aAnimalList.getEditor().getEditorComponent().addKeyListener(paKeyListener);
            label.addKeyListener(paKeyListener);
            aNameOfNewAnimal.addKeyListener(paKeyListener);
            aNameOfNewAnimal.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent ke) {
                    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                        aNameOfNewAnimal.setIsTextNeeded(true);
                        if (aTextArea.boliZmenyUlozene()) {
                            actionAddNewAnimal();
                        } else {
                            doYouWantToSaveAllChanges(aLastSelectedAnimal);
                            actionAddNewAnimal();
                        }
                    }
                }
            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "TabPridajZvieraChyba\n metóda:pridajComboBoxNazovvierata(...)\n" + e.getMessage(), "ERROR=Vypis Kategorie", JOptionPane.ERROR_MESSAGE);
        }

    }

     /**
     * Metóda vytvorí vizuálne komponenty pre zadanie súraníc  zvierata na mape
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalGEOPositionOnMap(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
        JLabel l = new JLabel("Zemepisná šírka, zemepisná dĺžka: ");
        l.setFont(FONT);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        paMainTabPanel.add(l, c);
        l.setPreferredSize(d);
        aSuradniceZemSirka = new JLatLonPanel(InfoSuboryENUM.MIN_LAT, InfoSuboryENUM.MAX_LAT);
        aSuradniceZemSirka.setPreferredSize(d);
        aSuradniceZemSirka.setFont(FONT);
        paMainTabPanel.add(aSuradniceZemSirka, c);

        aSuradniceZemDlzka = new JLatLonPanel(InfoSuboryENUM.MIN_LON, InfoSuboryENUM.MAX_LON);
        aSuradniceZemDlzka.setPreferredSize(d);
        aSuradniceZemDlzka.setFont(FONT);
        paMainTabPanel.add(aSuradniceZemDlzka, c);

        aSuradniceZemDlzka.addFocusListener(new GEDataFocusSave(aSuradniceZemDlzka));
        aSuradniceZemSirka.addFocusListener(new GEDataFocusSave(aSuradniceZemSirka));

        if (aAnimalList.getSelectedItem() == null) {
            aSuradniceZemDlzka.setEnabled(false);
            aSuradniceZemSirka.setEnabled(false);
        }
        updateGEOCoordinates();
    }

     /**
     * Metóda vytvorí vizuálne komponenty pre vloženie obrazku  zvierata
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalPictureAdd(JPanel strPanel, GridBagConstraints c, KeyListener paKeyListener) {
        Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
        JButton bt1 = new JButton("Vyber obrázok");
        bt1.addKeyListener(paKeyListener);
        bt1.setFont(FONT);
        bt1.setPreferredSize(d);
        strPanel.add(bt1, c);

        aPathOfChoosenPicture = new JTextField();
        aPathOfChoosenPicture.addKeyListener(paKeyListener);
        aPathOfChoosenPicture.setEditable(false);
        aPathOfChoosenPicture.setPreferredSize(d);
        aPathOfChoosenPicture.setFont(FONT);
        strPanel.add(aPathOfChoosenPicture, c);

        bt1.setSize(100, 100);
        bt1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                actionChangePictureOfSelectedAnimal();
            }
        });

        c.gridheight = 2;
        aShowerOfAnimalPicturePanel = new JPanel(new BorderLayout());
        JScrollPane pane = new JScrollPane(aShowerOfAnimalPicturePanel);
        pane.setPreferredSize(null);
        pane.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, WIDTH));
        pane.setBackground(Color.LIGHT_GRAY);
        strPanel.add(pane, c);
        updatePicture();
        c.gridheight = 1;
    }

     /**
     * Metóda vytvorí vizuálne komponenty pre vloženie popisu  zvierata
     * @param paMainTabPanel- panel do ktoreho sa vlozia komponenty
     * @param c - objekt GridBagConstraints, ktory uklada komonenty na panel
     * @param paKeyListener - globalny keylistener aplikacie
     */
    private void addComponentsForAnimalDescriptionEdit(JPanel paMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        aTextArea = new JTextAreaWithHint();
        //aTextArea.setColumns(30);
        //Dimension d = new Dimension(300, 300);
        //aTextArea.setPreferredSize(d);
        aTextArea.setLineWrap(true);
        aTextArea.addKeyListener(paKeyListener);
        aTextArea.setFont(FONT);

        JScrollPane lpScrollPaneOfTextArea = new JScrollPane(aTextArea);
        lpScrollPaneOfTextArea.addKeyListener(paKeyListener);
        lpScrollPaneOfTextArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        lpScrollPaneOfTextArea.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        aTextArea.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        paMainTabPanel.add(lpScrollPaneOfTextArea, c);
        updateTextArea();
        c.gridy = 5;
        c.weighty = 0.01;

        JButton btSaveText = new JButton("Ulož zmeny vykonané v texte");
        btSaveText.addKeyListener(paKeyListener);
        btSaveText.setFont(FONT);

        paMainTabPanel.add(btSaveText, c);
        btSaveText.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionChangeDescriptionOfSellectedAnimal((Info_About_Item) aAnimalList.getSelectedItem());
            }
        });

        //AK BY SOM CHCEL OBRAZKY MAZAT- ODKOENTOVAT
        /*
        c.gridwidth=1;
        c.gridx=2;
        aButtonDeletePicture=new JButton("Odstráň obrázok");
        aButtonDeletePicture.setFont(FONT);
        aButtonDeletePicture.addKeyListener(paKeyListener);
        aButtonDeletePicture.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                aDataTransferer.addItemInfo("DELETE_PICTURE_ANIMAL", (Info_About_Item)aAnimalList.getSelectedItem());
                aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.UPRAV);
                updatePicture();
                aDataTransferer.removeAllData();
            }
        });
        strPanel.add(aButtonDeletePicture,c);*/
    }

    /**
     * zobraz všetky aktuálne vytvorene kategorie
     */
    private void updateCategories() {

        if (aCategoryList != null) {
            aCategoryList.removeAllItems();
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_ANIMAL_CATEGORIES", new Info_About_Item(null, null, null));
            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aCategoryList.addItem(prech);

            }
            DataTransferer.getInstance().removeAllData();
        }
        //this.getParent().repaint();
    }

    /**
     * zobraz všetky aktuálne vytvorene zvierata zvolenej kategorie
     */
    private void updateAnimals() {

        if (aCategoryList != null
                && aCategoryList.getSelectedItem() != null && aAnimalList != null) {

            String keyOfSelectedCategory = getKeyOfSelectedCategory();
            aDataTransferer.addString("GET_ANIMAL_OF_CHOOSEN_CATEGORY", keyOfSelectedCategory);

            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.DAJDATA);
            aAnimalList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aAnimalList.addItem(prech);
            }
            updateGEOCoordinates();
            updatePicture();
            updateTextArea();
        }

        aDataTransferer.removeAllData();

        //this.getParent().repaint();
    }

    /**
     * zobraz súradnice vybraného zvieraťa
     */
    private void updateGEOCoordinates() {
        if (aAnimalList != null && aAnimalList.getSelectedItem() != null) {
            if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                aSuradniceZemDlzka.setEnabled(true);
                aSuradniceZemSirka.setEnabled(true);
                aDataTransferer.removeAllData();
                aDataTransferer.addItemInfo("GEO_POSITION_ANIMAL", (Info_About_Item) aAnimalList.getSelectedItem());

                aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.DAJDATA);
                if (aDataTransferer.getString("RET_LATITUDE") != null && aDataTransferer.getString("RET_LONGITUDE") != null) {
                    aSuradniceZemDlzka.setData(DataTransferer.getInstance().getString("RET_LONGITUDE"));
                    aSuradniceZemSirka.setData(DataTransferer.getInstance().getString("RET_LATITUDE"));
                } else {
                    aSuradniceZemDlzka.restore();
                    aSuradniceZemSirka.restore();
                }
            }

        } else {
            if (aSuradniceZemSirka != null && aSuradniceZemDlzka != null) {
                aSuradniceZemDlzka.restore();
                aSuradniceZemSirka.restore();
                aSuradniceZemDlzka.setEnabled(false);
                aSuradniceZemSirka.setEnabled(false);
            }
        }
    }

    /**
     * zobraz popis vybraného zvieraťa
     */
    private void updateTextArea() {

        if (aTextArea != null && aAnimalList.getSelectedItem() != null) {
            aTextArea.setDefaultText("Vložte popis vybraného zvieraťa");
            aTextArea.setEditable(true);
            aTextArea.setEnabled(true);
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_TEXT_ANIMAL", (Info_About_Item) aAnimalList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.DAJDATA)) {
                if (aDataTransferer.getString("RET_TEXT_ANIMAL").length() != 0) {
                    aTextArea.setText(DataTransferer.getInstance().getString("RET_TEXT_ANIMAL"));
                    aTextArea.setJeUznejakyTextnacitany(true);
                    aTextArea.setSelectionStart(0);
                    aTextArea.setSelectionEnd(0);
                } else {
                    aTextArea.setJeUznejakyTextnacitany(false);
                }
                DataTransferer.getInstance().removeAllData();
            } else {
                JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "ERROR: Text_data was not load. ", "ERROW: Load text data", JOptionPane.ERROR_MESSAGE);
            }

        } else if (aTextArea != null) {
            aTextArea.setDefaultText("Vyberte zviera pre úpravu");
            aTextArea.setEditable(false);
            aTextArea.setEnabled(false);

        }
        this.getParent().repaint();
    }

    /**
     * zobraz obrázok vybraného zvieraťa
     */
    private void updatePicture() {
        if (aShowerOfAnimalPicturePanel != null && aAnimalList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_IMAGE_ANIMAL", (Info_About_Item) aAnimalList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.DAJDATA)) {
                aShowerOfAnimalPicturePanel.removeAll();
                JLabel picLabel = new JLabel(new ImageIcon(DataTransferer.getInstance().getImage("RET_IMAGE_ANIMAL")));
                aShowerOfAnimalPicturePanel.add(picLabel, BorderLayout.CENTER);
            } else {
                aShowerOfAnimalPicturePanel.removeAll();
            }
        } else if (aShowerOfAnimalPicturePanel != null) {
            aShowerOfAnimalPicturePanel.removeAll();
        }
        this.getParent().repaint();
    }

    /**
     * 
     * @return 
     */
    private String getKeyOfSelectedCategory() {

        aDataTransferer.addItemInfo("GET_ANIMAL_CATEGORIES", new Info_About_Item(null, null, null));
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA)) {
            String paKeyOfCategory = null;

            for (String prech : DataTransferer.getInstance().getAllInfoAboutItemKeys()) {
                if (aCategoryList.getSelectedItem().equals(aDataTransferer.getItemInfo(prech))) {
                    paKeyOfCategory = prech;
                    break;
                }
            }
            return paKeyOfCategory;
        }
        System.err.println("ERROR MyLogFiles: getKeyOfSelectedCategory return false in class: "+this.getClass().getSimpleName());
        return null;
    }

    /**
     * vytvorí novú kategóriu zvierat
     */
    private void actionAddNewAnimalCategory() {

        //nastav data do transfera dat
        aDataTransferer.addString("NEW_CATEGORY", TabAnimalsInZoo.this.aNameOfNewCategory.getText());
        aDataTransferer.addString("ANIMALS_CATEGORY", "");
        //vykonaj
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.PRIDAJ)) {
            Info_About_Item createdItem=aDataTransferer.getItemInfo("CREATED_ANIMAL_CATEGORY");
            updateCategories();
            if (createdItem!= null) {
                aCategoryList.setSelectedItem(createdItem);
            }
            aNameOfNewCategory.setIsTextNeeded(false);
            JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Category: " + TabAnimalsInZoo.this.aNameOfNewCategory.getText() + " was created.");

        } else {
            JOptionPane.showMessageDialog(TabAnimalsInZoo.this,
                    "Cattegory can not be create, becouse of the text field is empty or category exist already.",
                    "Create category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * vytvorí nové zviera v zvolenej kategorii
     */
    private void actionAddNewAnimal() {
        //nastav data do transfera dat

        String paKeyOfCategory = getKeyOfSelectedCategory();
        //vykonaj
        aDataTransferer.addString("CHOOSEN_CATEGORY", paKeyOfCategory);
        aDataTransferer.addString("NEW_ANIMAL", TabAnimalsInZoo.this.aNameOfNewAnimal.getText());

        if (paKeyOfCategory != null && aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.PRIDAJ)) {
            Info_About_Item createdAnimal=aDataTransferer.getItemInfo("CREATED_ANIMAL");
            updateAnimals();
            if (createdAnimal != null) {
                aAnimalList.setSelectedItem(createdAnimal);
            }
            aLastSelectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
            aNameOfNewAnimal.setIsTextNeeded(false);
            JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Animal: " + TabAnimalsInZoo.this.aNameOfNewAnimal.getText() + " was created.");

        } else {
            JOptionPane.showMessageDialog(TabAnimalsInZoo.this,
                    "Animal can not be create, becouse of the text field: new animal is empty\n or animal already exists or category is not chosen.",
                    "Animal category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * zmeni obrazok vybranemu zvieratu
     */
    private void actionChangePictureOfSelectedAnimal() {
        if (aAnimalList.getSelectedItem() != null) {
            final JFileChooser chooser = new JFileChooser();

            chooser.setCurrentDirectory(new File(aLastVisitedDir));

            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.showOpenDialog(null);
            if (chooser.getSelectedFile() != null) {
                String path = chooser.getSelectedFile().getAbsolutePath();
                chooser.setCurrentDirectory(new File(path));
                String filename = chooser.getSelectedFile().getName();
                String separatedName[] = filename.split("\\.");
                if (separatedName.length == 2 && (separatedName[1].toLowerCase().equals("jpg"))
                        || (separatedName[1].toLowerCase().equals("png")) || (separatedName[1].toLowerCase().equals("gif"))) {
                    File f = new File(path);
                    long a = f.length() / 1000;
                    if (a < 600) {
                        try {
                            aLastVisitedDir = path;
                            aPathOfChoosenPicture.setText(path);
                            BufferedImage myPicture = ImageIO.read(new File(path));
                            aShowerOfAnimalPicturePanel.removeAll();
                            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
                            aShowerOfAnimalPicturePanel.add(picLabel, BorderLayout.CENTER);

                            aDataTransferer.addItemInfo("EDITED_ANIMAL", (Info_About_Item) aAnimalList.getSelectedItem());
                            aDataTransferer.addString("EDIT_PICTURE_PATH", path);
                            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.UPRAV)) {
                                TabAnimalsInZoo.this.getParent().repaint();
                            } else {
                                JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "ERROR(chyba aplikácie): Obrázok sa nepodarilo nahrať.\n"
                                        + " Trieda: TabPridajZviera", "ERROR- Chyba aplikácie. ", JOptionPane.ERROR_MESSAGE);
                            }

                        } catch (IOException ex) {
                            Logger.getLogger(TabAnimalsInZoo.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Obrázky môžu mať maximálnu veľkosť 600 kB.\n"
                                + " Vami vybraný obrázok má veľkosť: " + a
                                + " kB", "ERROR- Obrázok zaberá príliš mnoho miesta. ", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Obrázky môžu byť iba formátu: jpg, png, gif", "ERROR- Zly formát obrázka", JOptionPane.ERROR_MESSAGE);
                }

            }
        } else {
            JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Nie je zvolené zviera, ktorého obrázok je portrebné zmeniť. Najskôr vyberte zviera.");
        }
        aDataTransferer.removeAllData();
    }

    /**
     * zmeni popis vybraneho zvierata
     * @param paKomu -zviera, ktoreho popis sa meni
     */
    private void actionChangeDescriptionOfSellectedAnimal(Info_About_Item paKomu) {
        if (aAnimalList.getSelectedItem() != null) {
            aDataTransferer.addItemInfo("EDITED_ANIMAL", paKomu);
            aDataTransferer.addString("EDITED_TEXT", aTextArea.getText());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.UPRAV)) {
                JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Text was successfully save.");
            } else {
                JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "ERROR: Text was not save. ", "ERROW: Save text", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Annimal must be choosed, before you try to save text.",
                    "No animal choosen",
                    JOptionPane.WARNING_MESSAGE);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aTextArea.setJeUznejakyTextnacitany(true);
        aDataTransferer.removeAllData();
    }

    private void doYouWantToSaveAllChanges(Info_About_Item paKomu) {
        if (JOptionPane.showConfirmDialog(this,
                "Zmeny v popise zvieraťa neboli uložené a po prepnutí budú stratené. \nChcete tieto dáta pred vykonaním akcie uložiť?", "Uloženie popisu",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            actionChangeDescriptionOfSellectedAnimal(paKomu);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aNameOfNewAnimal.setIsTextNeeded(false);
        aNameOfNewAnimal.setIsTextNeeded(false);
    }

    @Override
    public void updateAllTabData() {
        Info_About_Item lpSelectedAnimal = null;
        Info_About_Item lpSelectedCategory = null;
        if (aCategoryList != null && aAnimalList != null) {
            lpSelectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
            lpSelectedCategory = (Info_About_Item) aCategoryList.getSelectedItem();
        }
        updateCategories();
        updateAnimals();
        if (lpSelectedAnimal != null && lpSelectedCategory != null) {
            aCategoryList.setSelectedItem(lpSelectedCategory);
            aAnimalList.setSelectedItem(lpSelectedAnimal);
            aLastSelectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
        }
    }

    /**
     * Trieda, ktora pri zadavanisuradnic zistuje, ci boli vsetky udaje spravne
     * Ak su udaje spravne, ulozi ich a priradi vybranemu zvieratu
     */
    private class GEDataFocusSave extends FocusAdapter {

        private JLatLonPanel aLatLonPanelThatGetEvent;

        private GEDataFocusSave(JLatLonPanel paJLatLonPanel) {
            aLatLonPanelThatGetEvent = paJLatLonPanel;
        }

        @Override
        public void focusLost(FocusEvent fe) {
            if (aSuradniceZemSirka.areSetValidValues() && aSuradniceZemDlzka.areSetValidValues()) {
                if (aSuradniceZemDlzka.areValuesInAreatOfZOO() && aSuradniceZemSirka.areValuesInAreatOfZOO()) {
                    Cesta paOfflinCesta = TabOfflineMapa.initCestaObject();
                    if (paOfflinCesta != null) {

                        String lat[] = aSuradniceZemSirka.getTextRepresentation().split(":");
                        String lon[] = aSuradniceZemDlzka.getTextRepresentation().split(":");
                        Coordinate latCoord = new Coordinate(lat[0], lat[1], lat[2]);
                        Coordinate lonCoord = new Coordinate(lon[0], lon[1], lon[2]);
                        BodNaCeste b = TabOfflineMapa.pridajNovyObjekt(paOfflinCesta, latCoord, lonCoord);
                        if (b != null) {
                            aDataTransferer.removeAllData();
                            aDataTransferer.addItemInfo("EDITED_ANIMAL", (Info_About_Item) aAnimalList.getSelectedItem());

                            aDataTransferer.addDouble("SET_LONGITUDE", aSuradniceZemDlzka.getPartOfGeoPoint());
                            aDataTransferer.addString("SET_LONGITUDE", aSuradniceZemDlzka.getTextRepresentation());

                            aDataTransferer.addDouble("SET_LATITUDE", aSuradniceZemSirka.getPartOfGeoPoint());
                            aDataTransferer.getInstance().addString("SET_LATITUDE", aSuradniceZemSirka.getTextRepresentation());

                            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.UPRAV);
                            aDataTransferer.removeAllData();

                            System.out.println("DEBUG: Zviera bolo umietnené na mapu na súradnice: <" + latCoord.getDECRepresentationOfCoordinate() + ", "
                                    + lonCoord.getDECRepresentationOfCoordinate());

                        }
                    }
                } else {

                    JOptionPane.showMessageDialog(TabAnimalsInZoo.this, "Zviera sa nepodarilo umiestnit na mapu:\n"
                            + "Zadaná súradnica neodpovedá bodu nachádzajúcemu sa v areáli zoo.\n"
                            + "Zemepisná Šírka rozmedzie: < "
                            +InfoSuboryENUM.MIN_LAT_DEGREE+": "
                            +InfoSuboryENUM.MIN_LAT_MINUTES+":"
                            + InfoSuboryENUM.MIN_LAT_SECONDS+" ; "
                            +InfoSuboryENUM.MAX_LAT_DEGREE+": "+InfoSuboryENUM.MAX_LAT_MINUTES+":"
                            + InfoSuboryENUM.MAX_LAT_SECONDS+" >\n "
                            + "Zemepisná dĺžka rozmedzie: < "
                            +InfoSuboryENUM.MIN_LON_DEGREE
                            +": "+InfoSuboryENUM.MIN_LON_MINUTES+":"
                            + InfoSuboryENUM.MIN_LON_SECONDS+" ; "
                            +InfoSuboryENUM.MAX_LON_DEGREE+": "+InfoSuboryENUM.MAX_LAT_MINUTES+":"
                            + InfoSuboryENUM.MAX_LON_SECONDS+" >",
                            "ERROR: neumiestnene zviera.",
                            JOptionPane.INFORMATION_MESSAGE);

                }
            }

        }
    }
}

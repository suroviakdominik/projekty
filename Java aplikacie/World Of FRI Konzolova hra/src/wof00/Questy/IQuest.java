/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Questy;

import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public interface IQuest {

    public String getNazov();

    public String getPopis();

    public void prijmi(Hrac paHrac);

    public void ukonci(Hrac paHrac);
}

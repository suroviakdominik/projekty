/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Hra;

import java.io.Serializable;
import wof00.Miestnost.Miestnost;
import java.util.ArrayList;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.Miestnost;
import wof00.Miestnost.MiestnostBufet;

import wof00.Miestnost.MiestnostLabak;
import wof00.Miestnost.MiestnostVytah;
import wof00.Miestnost.MiestnostZabezpecena;
import wof00.Osoby.OsobaPredavackavBufete;
import wof00.Osoby.OsobaUcitel1;
import wof00.Predmety.PredmetBageta;
import wof00.Predmety.PredmetBaterka;
import wof00.Predmety.PredmetCinka;
import wof00.Predmety.PredmetHodinky;
import wof00.Predmety.PredmetIndex;
import wof00.Predmety.PredmetKluc;
import wof00.Predmety.PredmetMapa;
import wof00.Predmety.PredmetNavleky;
import wof00.Predmety.PredmetNotebook;
import wof00.Predmety.PredmetObriaMucha;
import wof00.Predmety.PredmetOvladac;
import wof00.Predmety.PredmetOvladac2;
import wof00.Predmety.PredmetPortalGun;
import wof00.Questy.QuestBcTitul;

/**
 *
 * @author Dominik
 */
public class MapaHry implements Serializable {

    private IMiestnost aStartovaciaMiestnost;
    private ArrayList<IMiestnost> aZoznamMiestnosti = new ArrayList<IMiestnost>();

    /**
     * Vytvori mapu hry a inicializuje miestnosti - mapu hry.
     */
    public MapaHry() {
        Miestnost terasa;
        Miestnost vratnica;
        MiestnostZabezpecena IC;
        Miestnost chodbaA;
        Miestnost chodbaB;
        Miestnost ucebna;
        MiestnostVytah vytah;
        Miestnost WC;
        Miestnost maleschody;
        Miestnost velkeschody;
        MiestnostLabak labakB;
        Miestnost chodbaDoLabaku;
        MiestnostBufet bufet;
        MiestnostLabak FRA12;
        MiestnostLabak FRA13;
        Miestnost chodba1;
        Miestnost chodba2;
        Miestnost chodbaS;

        // vytvorenie miestnosti
        terasa = new Miestnost("terasa", "terasa - hlavny vstup na fakultu");
        vratnica = new Miestnost("vratnica", "vratnica");
        IC = new MiestnostZabezpecena("IC", "IC");
        chodbaA = new Miestnost("chodbaA", "chodbaA");
        chodbaB = new Miestnost("chodbaB", "chodbaB");
        ucebna = new Miestnost("ucebna", "ucebna");
        vytah = new MiestnostVytah("vytah", "vytah na bloku A");
        WC = new Miestnost("WC", "WC");
        maleschody = new Miestnost("maleschody", "maleschody");
        velkeschody = new Miestnost("velkeschody", "velkeschody");
        labakB = new MiestnostLabak("labakB", "labakB");
        chodbaDoLabaku = new Miestnost("chodbaDoLabaku", "chodbaDoLabaku");
        bufet = new MiestnostBufet("bufet", "bufet, ktory obsahuje mnozstvo tovaru");
        FRA12 = new MiestnostLabak("FRA12", "FRA12");
        FRA13 = new MiestnostLabak("FRA13", "FRA13");
        // inicializacia miestnosti = nastavenie vychodov
        /*terasa.nastavVychody(null, aula, labak, bufet,null,null);
         aula.nastavVychody(null, null, null, terasa,null,null);
         bufet.nastavVychody(null, terasa, null, null,null,null);
         labak.nastavVychody(terasa, kancelaria, null, null,null,null);
         kancelaria.nastavVychody(null, null, null, labak,poschodie,suteren);
         suteren.nastavVychody(null,null,null,null,kancelaria,null);
         poschodie.nastavVychody(null, null, null, null, null, kancelaria);*/
        terasa.nastavVychody(vratnica);
        terasa.nastavVychody(bufet);

        vratnica.nastavVychody(IC);
        vratnica.nastavVychody(chodbaA);
        vratnica.nastavVychody(chodbaB);
        vratnica.nastavVychody(terasa);

        IC.nastavVychody(vratnica);
        IC.nastavVychody(ucebna);

        ucebna.nastavVychody(IC);

        chodbaB.nastavVychody(vratnica);
        chodbaB.nastavVychody(labakB);

        labakB.nastavVychody(chodbaB);

        chodbaA.nastavVychody(vratnica);
        chodbaA.nastavVychody(WC);
        chodbaA.nastavVychody(velkeschody);
        chodbaA.nastavVychody(maleschody);
        chodbaA.nastavVychody(vytah);
        chodbaA.nastavVychody(chodbaDoLabaku);

        WC.nastavVychody(chodbaA);

        velkeschody.nastavVychody(chodbaA);

        maleschody.nastavVychody(chodbaA);
        maleschody.nastavVychody(bufet);

        bufet.nastavVychody(maleschody);
        bufet.nastavVychody(terasa);

        vytah.nastavVychody(chodbaA);

        chodbaDoLabaku.nastavVychody(chodbaA);
        chodbaDoLabaku.nastavVychody(FRA12);
        chodbaDoLabaku.nastavVychody(FRA13);
        FRA12.nastavVychody(chodbaDoLabaku);
        FRA13.nastavVychody(chodbaDoLabaku);

        chodba1 = new Miestnost("chodba1", "chodba na 1 poschodi");
        chodba2 = new Miestnost("chodba2", "chodba na 2 poschodi");
        chodbaS = new Miestnost("chodbaS", "suteren");

        aZoznamMiestnosti.add(terasa);
        aZoznamMiestnosti.add(vratnica);
        aZoznamMiestnosti.add(IC);
        aZoznamMiestnosti.add(chodbaA);
        aZoznamMiestnosti.add(chodbaB);
        aZoznamMiestnosti.add(ucebna);
        aZoznamMiestnosti.add(vytah);
        aZoznamMiestnosti.add(WC);
        aZoznamMiestnosti.add(velkeschody);
        aZoznamMiestnosti.add(maleschody);
        aZoznamMiestnosti.add(labakB);
        aZoznamMiestnosti.add(chodbaDoLabaku);
        aZoznamMiestnosti.add(bufet);
        aZoznamMiestnosti.add(FRA12);
        aZoznamMiestnosti.add(FRA13);
        aZoznamMiestnosti.add(chodba1);
        aZoznamMiestnosti.add(chodba2);
        aZoznamMiestnosti.add(chodbaS);

        PredmetHodinky hodinky = new PredmetHodinky("hodinky");
        terasa.vlozPredmet(hodinky);

        PredmetNotebook notebook = new PredmetNotebook("notebook", "notebook", Boolean.TRUE);
        terasa.vlozPredmet(notebook);

        PredmetBaterka baterka = new PredmetBaterka("baterka", "nabita baterka");
        terasa.vlozPredmet(baterka);

        PredmetCinka cinka = new PredmetCinka();
        terasa.vlozPredmet(cinka);

        PredmetObriaMucha obriaMucha = new PredmetObriaMucha();
        WC.vlozPredmet(obriaMucha);

        PredmetNavleky navleky = new PredmetNavleky();
        terasa.vlozPredmet(navleky);

        PredmetPortalGun portalgun = new PredmetPortalGun("portalGun");
        vratnica.vlozPredmet(portalgun);

        PredmetBageta bageta = new PredmetBageta("bageta", 25);
        terasa.vlozPredmet(bageta);

        OsobaPredavackavBufete predavacka = new OsobaPredavackavBufete(bufet);
        bufet.vlozOsobu(predavacka);

        PredmetMapa mapa = new PredmetMapa(aZoznamMiestnosti);
        bufet.vlozPredmet(mapa);

        PredmetOvladac ovladac = new PredmetOvladac();
        vytah.vlozPredmet(ovladac);

        PredmetOvladac2 normalnyovladac = new PredmetOvladac2();
        vytah.vlozPredmet(normalnyovladac);

        PredmetKluc kluc = new PredmetKluc("kluc", "kluc IC");
        chodbaA.vlozPredmet(kluc);

        FRA12.vlozOsobu(new OsobaUcitel1("ucitel"));
        FRA13.vlozOsobu(new OsobaUcitel1("ucite2"));

        IC.vlozPredmet(new QuestBcTitul());

        setStart(vytah);

    }

    private void setStart(IMiestnost paMiestnost) {
        aStartovaciaMiestnost = paMiestnost;
    }

    public IMiestnost dajStartovaciuMiestnost() {
        return aStartovaciaMiestnost;
    }

    public IMiestnost najdiMiestnost(String paNazov) {
        IMiestnost najdena = null;
        for (IMiestnost prechadzac : aZoznamMiestnosti) {
            if (prechadzac.getaNazov().equals(paNazov)) {
                najdena = prechadzac;
            }
        }
        return najdena;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom.Solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import rbstrom.RBStrom.Vrchol;


/**
 *
 * @author Dominik
 */
public class Pridavac {

    public static void pridajSynaOcovi(Vrchol paSyn, Vrchol paOtec) {
        if (paOtec != null && paSyn != null) {
            if (paSyn.compareTo(paOtec) < 1) {
                paOtec.setaLavySyn(paSyn);
                paSyn.setaOtec(paOtec);
            } else if (paSyn.compareTo(paOtec) > 0) {
                paOtec.setaPravySyn(paSyn);
                paSyn.setaOtec(paOtec);
            } else {
                throw new IllegalArgumentException("Vrcholy, ktore ste vlozili do metody maju rovnaku hodnotu kluca");
            }
        } else {
            throw new IllegalArgumentException("Hodnota oca a syna nesmie byt null");
        }
    }

    public static boolean nastalaIregularita(Vrchol paPridanyVrchol) {
        if (paPridanyVrchol != null && paPridanyVrchol.getaOtec() != null) {
            if (paPridanyVrchol.jeCerveny() && paPridanyVrchol.getaOtec().jeCerveny()) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new IllegalArgumentException("Hodnota parametra nesmie byt null. Otec vrchola v parametri nesmie byt null");
        }
    }

    public static boolean jeBratOcaVkladanehoCierny(Vrchol paOtecVkladaneho) {
        if (paOtecVkladaneho != null&&paOtecVkladaneho.getaOtec()!=null) {
            if (BVSAlgoritmy.jeVrcholPravymSynomOca(paOtecVkladaneho)) {
                if (paOtecVkladaneho.getaOtec().getaLavySyn() == null || !paOtecVkladaneho.getaOtec().getaLavySyn().jeCerveny()) {
                    return true;
                } else {
                    return false;
                }
            }else{
                if (paOtecVkladaneho.getaOtec().getaPravySyn() == null || !paOtecVkladaneho.getaOtec().getaPravySyn().jeCerveny()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if(paOtecVkladaneho==null){
            throw new IllegalArgumentException("Prameter(vrchol) nesmie mat hodnotu null");
        }else{
            throw new IllegalArgumentException("Prameter(vrchol) nesmie byt bez oca. param.getoco()=null");
        }
    }

    
}

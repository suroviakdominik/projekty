/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarryhoalgoritmus;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Dominik
 */
public final class Vypocet 
{
    private int aPocetHran;
    private int aPocetVrcholov;
    private int aAktualnyVrchol;
    private Scanner scan;
    private File file;
    private Hrana[] aPoleHran;
    private boolean[] aPoleNavstivenychVrcholov;
    
    
     public Vypocet() throws FileNotFoundException
    {
        file=new File("graf1.TXT");
        scan=new Scanner(file);
        
        
        
     }       

    public int getaPocetVrcholov() {
        return aPocetVrcholov;
    }
    
     
     
       public void nacitajTextovyDokument()
       {
           aPocetVrcholov=scan.nextInt();
           aPoleNavstivenychVrcholov=new boolean[aPocetVrcholov];
           aPocetHran=scan.nextInt();
           aPoleHran=new Hrana[aPocetHran];
           System.out.println();
           System.out.println();
           for(int i=0;i<aPocetHran;i++)
           {
              aPoleHran[i]=new Hrana();
              aPoleHran[i].setLeftPartOfEdge(scan.nextInt());
              aPoleHran[i].setRightPartOfEdge(scan.nextInt()) ; 
           }
           scan.close();
           
        }
       
          public int vratPouzitelnuHranu()
          {
              int zapamatanieSiPrvejHranyPrvehoPrichodu=0;
              int i;
              boolean zapamatatSiHranuPrvehoPrichodu=true;
              
             for(i=0;i<aPoleHran.length;i++)
             {
                             if(!(aPoleHran[i].isLeftToRight())&&(aPoleHran[i].getLeftPartOfEdge()==aAktualnyVrchol))//kontrola ci je prejdena z lava doprava a ci je hranaa, ktora obsahuje aktualny vrchol
                             {
                                 if(!aPoleHran[i].isFirstComing())//kontrola ci je prveho prichodu: ak nie je podmienka plati (vrati true) 
                                 {
                                     if(aPoleNavstivenychVrcholov[aPoleHran[i].getRightPartOfEdge()-1]==true)//zistuje ci uz bol vrchol navstiveny: vrati true ak bol
                                     {
                                        aPoleHran[i].setLeftToRight(true);
                                        aAktualnyVrchol=aPoleHran[i].getRightPartOfEdge();
                                     }
                                     else if(aPoleNavstivenychVrcholov[aPoleHran[i].getRightPartOfEdge()-1]==false)//zistuje ci uz bol vrchol navstiveny: vrati true ak nebol
                                     {
                                         aPoleNavstivenychVrcholov[aPoleHran[i].getRightPartOfEdge()-1]=true;//zaznaci ze vrchol uz bol navstiveny
                                         aPoleHran[i].setFirstComing(true);//zaznaci ze je to hrana prveho prichodu
                                         aPoleHran[i].setLeftToRight(true);//zaznaci ze je prejdena z lava doprava
                                         aAktualnyVrchol=aPoleHran[i].getRightPartOfEdge();
                                     }
                                     return i;

                                 }
                                 if(zapamatatSiHranuPrvehoPrichodu)//zapamata si prvu hranu prveho prichodu, ak neexistuje ina hrana, pouzije sa tato
                                 {
                                     zapamatanieSiPrvejHranyPrvehoPrichodu=i;
                                     zapamatatSiHranuPrvehoPrichodu=false;//zabezpeci aby si zapamatovalo len prvu hranu prveho prichodu z mnoziny hran
                                    
                                 }


                              }
                             else if(!(aPoleHran[i].isRightToLeft())&&(aPoleHran[i].getRightPartOfEdge()==aAktualnyVrchol))//kontrola ci je prejdena z prava dolava a ci je hranaa, ktora obsahuje aktualny vrchol(je vyhovujuca hrana)
                             {
                                 if(!aPoleHran[i].isFirstComing())//kontrola ci je prveho prichodu
                                 {
                                     if(aPoleNavstivenychVrcholov[aPoleHran[i].getLeftPartOfEdge()-1]==true)//zistuje ci vrchol uz bol navstiveny
                                     {
                                        aPoleHran[i].setRightToLeft(true);//ak vrchol uz bol navstiveny, tak iba nastavi ze sme ho teraz presli sprava dolava
                                        aAktualnyVrchol=aPoleHran[i].getLeftPartOfEdge();//aktualizuje aAktualnyVrchol
                                     
                                     }
                                     else if(aPoleNavstivenychVrcholov[aPoleHran[i].getLeftPartOfEdge()-1]==false)//zistuje ci vrchol nebol navstiveny
                                     {
                                         aPoleNavstivenychVrcholov[aPoleHran[i].getLeftPartOfEdge()-1]=true;//zaznaci ze sme ho navstivili
                                         aPoleHran[i].setFirstComing(true);// zaznaci, ze je to hrana prveho prichodu
                                         aPoleHran[i].setRightToLeft(true);//zaznaci, ze sme ho presli sprava dolava
                                         aAktualnyVrchol=aPoleHran[i].getLeftPartOfEdge();//aktualizuje aAktualnyVrchol
                                     }
                                     return i;//vrati index polaHran, ktoru hranu sme pouzili
                                     
                                 }
                                 
                                 if(zapamatatSiHranuPrvehoPrichodu)//zapamata si prvu hranu prveho prichodu, ak neexistuje ina hrana, pouzije sa tato
                                 {
                                     zapamatanieSiPrvejHranyPrvehoPrichodu=i;
                                     zapamatatSiHranuPrvehoPrichodu=false;//zabezpeci aby si zapamatovalo len prvu hranu prveho prichodu z mnoziny hran
                                     
                                 }
                             }
                             
             }
                 if(!zapamatatSiHranuPrvehoPrichodu)//ak si zapamatovalo hranu prveho prichodu, a v mnozine H sa nenchadza ina hrana ktoru by sme mohli pouzit, pouzije sa hrana prveho prichodu
                 {
                   Hrana hranaPrvehoPrichodu=aPoleHran[zapamatanieSiPrvejHranyPrvehoPrichodu];
                     if((hranaPrvehoPrichodu.getLeftPartOfEdge()==aAktualnyVrchol)&&!(hranaPrvehoPrichodu.isLeftToRight()))//zistuje ktory vrchol bude novym aktualnym vrcholom
                     {
                         hranaPrvehoPrichodu.setLeftToRight(true);
                         aAktualnyVrchol=hranaPrvehoPrichodu.getRightPartOfEdge();
                     }
                     else if((hranaPrvehoPrichodu.getRightPartOfEdge()==aAktualnyVrchol)&&!(hranaPrvehoPrichodu.isRightToLeft()))
                     {
                         hranaPrvehoPrichodu.setRightToLeft(true);
                         aAktualnyVrchol=hranaPrvehoPrichodu.getLeftPartOfEdge();
                     }
                     
                     return zapamatanieSiPrvejHranyPrvehoPrichodu;
                 }
                 else
                 {
                     System.out.println(aAktualnyVrchol);
                     aAktualnyVrchol=0;
                     return zapamatanieSiPrvejHranyPrvehoPrichodu;
                 }
                  
                 
                 
          }
          
          
          public void vykonajAlgoritmus(int paZVrchola)
          {
              aAktualnyVrchol=paZVrchola;
              boolean ukonci=true;
              do
              {
                int i=vratPouzitelnuHranu();
                if(aAktualnyVrchol!=0)
                {
                        if(aPoleHran[i].getLeftPartOfEdge()==aAktualnyVrchol)  
                        {
                            System.out.println(aPoleHran[i].getRightPartOfEdge()+" ,{"+aPoleHran[i].getRightPartOfEdge()+", "+aAktualnyVrchol+"}");
                        }
                        else 
                        {
                             System.out.println(aPoleHran[i].getLeftPartOfEdge()+" ,{"+aPoleHran[i].getLeftPartOfEdge()+", "+aAktualnyVrchol+"}");
                        }
                        
                }
                else if(aAktualnyVrchol==0)
                {
                 ukonci=false;
                }
              }
              
              while(ukonci);
              
          }
       
         
          
     
}

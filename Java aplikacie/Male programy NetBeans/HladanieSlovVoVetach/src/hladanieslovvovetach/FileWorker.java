/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hladanieslovvovetach;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {
    public static String readDataFromFile(File f, String paOddelovacRiadkov)
    {
        Reader reader=null;
        try {
            String ret="";
            reader = new InputStreamReader(new FileInputStream(f),"UTF-8");
            BufferedReader bR=new BufferedReader(reader);
            String loadedLine=null;
            while((loadedLine=bR.readLine())!=null)
            {
                ret+=loadedLine+paOddelovacRiadkov;
            }   return ret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                if(reader!=null)
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return null;
    }
}

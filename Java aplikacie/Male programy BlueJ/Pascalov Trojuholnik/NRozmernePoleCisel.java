

public class NRozmernePoleCisel
{
   private int [][] aPoleCisel;
   private int aPocetPrvkov;
   private int aPripocitaj;
   public NRozmernePoleCisel(int paPocetPrvkov)
   {
       aPocetPrvkov=paPocetPrvkov;
       if (aPocetPrvkov%2==0)
       {
           aPripocitaj=aPocetPrvkov-1;
       }
       else
       {
           aPripocitaj=aPocetPrvkov-1;
       }    
       aPoleCisel=new int [aPocetPrvkov][aPocetPrvkov+aPripocitaj];
   }
   
      public void vlozHodnotuDoNRozmernehoPola(int i, int j,int paCislo)
      {
          aPoleCisel[i][j]=paCislo;
      }
      
      public int dajPocetPrvkov()
      {
          return aPocetPrvkov;
      }  
      
      public void vypisPole()
      {
          for (int i=0;i<aPocetPrvkov;i++)
          {
              for (int j=0;j<aPocetPrvkov+aPripocitaj;j++)
              {
                  if (aPoleCisel[i][j]==0)
                  {
                      System.out.print("\t");
                  }
                  else
                  {
                      System.out.print(aPoleCisel[i][j]+"\t ");
                  }
                  
              }
              System.out.println();
          }
      }
      
      public void vypisPrvok(int i,int j)
      {
          System.out.print(aPoleCisel[i][j]+"\t");
      }   
      
      public int dajPripocitane()
      {
          return aPripocitaj;
      }   
      
      public int zistiHodnotuIndexu(int i,int j)
      {
         return  aPoleCisel[i][j];
      }
}     
      
      
      
      
                  


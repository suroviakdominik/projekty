/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import java.awt.Color;

/**
 * Trieda Figurka je abstraktná trieda a predstavuje triedu zhŕňajúcu spoločné
 * vlastnosti všetkých figúrok ako sú napríklad meno, farba, umiestnenie ...
 *
 * @author Dominik
 */
public abstract class Figurka {

    private String aMeno;
    private String aFarba;
    private int aX;
    private int aY;

    /**
     *
     * @param paMeno
     * @param paFarba
     */
    public Figurka(String paMeno, String paFarba) {
        aMeno = paMeno;
        aFarba = paFarba;

    }

    /**
     * priradí atribútom aX a aY hodnoty prijaté v parametri, ktoré sa neskôr
     * použijú na vykreslenie figúrky.
     *
     * @param paX -x-sova suradnica figúrky
     * @param paY -y-lonová súradnica figúrky
     */
    public void umiestniFigurku(int paX, int paY) {
        aX = paX;
        aY = paY;
    }

    /**
     * vráti meno figúrky, ktoré je uložené v atribúte aMeno.
     *
     * @return meno figúrky
     */
    public String getMeno() {
        return aMeno;
    }

    /**
     * vráti farbu figúrky, ktorá je uložená v atribúte aFarba.
     *
     * @return farba figúrky
     */
    public String getFarba() {
        return aFarba;
    }

    /**
     * vráti aktuálnu x-sovú pozíciu figúrky.
     *
     * @return x-sová pozíciá
     */
    public int getX() {
        return aX;
    }

    /**
     * vráti aktuálnu y-lonovú pozíciu figúrky.
     *
     * @return y-lonová pozíciá
     */
    public int getY() {
        return aY;
    }

    /**
     * je abstraktná metóda, ktorá je prepísaná v každej triede dediacej Figurku
     * a má za úlohu vykresliť figúrku na plátno.
     *
     * @return
     */
    public abstract void nakresli();

    /**
     * je abstraktná metóda, ktorá vracia grafické objekty(kruh,štvorec...)
     * potrebné na vykreslenie figúrky.
     *
     * @return grafické objekty z ktorých sa figúrka skladá
     */
    public abstract IGrafickeObjekty[] getGrafickeObjektyFigurky();

    /**
     * abstraktná metóda vracajúca popis jednotlivej figúrky. Je prekrytá v
     * každom priamom potomkovi.
     *
     * @return popis figúrky vo forme stringu
     */
    public abstract String getPopis();

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.MiestnostVytah;

/**
 *
 * @author Dominik
 */
public class PredmetOvladac2 implements IPredmet, Serializable {

    private int aPozicia = 1;
    private boolean aSmer = true;
    ArrayList<String> poschodia = new ArrayList<String>();
    Scanner scan = new Scanner(System.in);

    @Override
    public String getNazov() {
        return "ovladac2";
    }

    @Override
    public String getPopis() {
        return "ovladac vytahu, ktory nas dostane na zadane poschodie";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        if (paHrac.dajPoziciu() instanceof MiestnostVytah) {
            MiestnostVytah vytah = (MiestnostVytah) paHrac.getaMapaHry().najdiMiestnost("vytah");

            if (vytah.dajVychod("chodba1") != null) {
                aPozicia = 2;
            } else if (vytah.dajVychod("chodba2") != null) {
                aPozicia = 3;
            } else if (vytah.dajVychod("chodbaS") != null) {
                aPozicia = 0;
            }

            int vyber;
            poschodia.add("chodbaS");
            poschodia.add("chodbaA");
            poschodia.add("chodba1");
            poschodia.add("chodba2");

            System.out.println("0- chodbaS");
            System.out.println("1- chodbaA");
            System.out.println("2- chodba1");
            System.out.println("3- chodba2");
            System.out.println("Zadajte poschodie: ");
            System.out.print("> ");
            if (scan.hasNextInt()) {

                vyber = scan.nextInt();
                IMiestnost aktualna = paHrac.getaMapaHry().najdiMiestnost(poschodia.get(aPozicia));
                IMiestnost naAkePoschodie = paHrac.getaMapaHry().najdiMiestnost(poschodia.get(vyber));
                if (naAkePoschodie != null) {
                    vytah.zmazVychod(aktualna.getaNazov());
                    vytah.nastavVychody(naAkePoschodie);
                    naAkePoschodie.nastavVychody(vytah);
                    aPozicia = vyber;
                } else {
                    System.out.println("Take poschodie tu nie je.");
                }
            } else {
                System.out.println("Vasu volbu neviem spracovat. Zadajte cislo prislusne poschodiu.");
            }
            return "Ste na poschodi " + aPozicia + "\n" + vytah.dajPopisVychodov();
        } else {
            return "Ovladac mozno pouzit iba vo vytahu.";
        }
    }
}

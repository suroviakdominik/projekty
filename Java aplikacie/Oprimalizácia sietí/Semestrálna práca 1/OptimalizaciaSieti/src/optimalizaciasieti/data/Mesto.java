/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti.data;

import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class Mesto implements Comparable<Mesto>{
    private String aCityID;
    private String aNameOfCity;
    private int aPopulationCount;

    public Mesto(String aCityID, String aNameOfCity, int aPopulationCount) {
        this.aCityID = aCityID;
        this.aNameOfCity = aNameOfCity;
        this.aPopulationCount = aPopulationCount;
    }

    public String getaCityID() {
        return aCityID;
    }

    public String getaNameOfCity() {
        return aNameOfCity;
    }

    public int getaPopulationCount() {
        return aPopulationCount;
    }

    @Override
    public String toString() {
        return aCityID + "\t" + aNameOfCity + "\t" + aPopulationCount + "\n";
    }

    @Override
    public int compareTo(Mesto o) {
        if(this.getaPopulationCount()==o.getaPopulationCount())
        {
            return 0;
        }
        if(this.getaPopulationCount()>o.getaPopulationCount())
        {
            return -1;
        }
        return 1;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.aCityID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Mesto other = (Mesto) obj;
        if (!Objects.equals(this.aCityID, other.aCityID)) {
            return false;
        }
       
        return true;
    }
    
    
}

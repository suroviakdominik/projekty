set pagesize 60
set linesize 123
column zamestnanec.meno HEADING "Meno" FORMAT A15;
column zamestnanec.priezvisko heading "Priezvisko" FORMAT A20;
column zamestnanec.pracovna_pozicia heading "Pracovna pozicia" FORMAT A30;
column predajna.nazov_Predajne heading "Nazov Predajne" FORMAT A30
column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad zamestnancov jednotlivych predajni' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia:" datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
meno,priezvisko,pracovna_pozicia,nazov_predajne 
from ZAMESTNANEC,PREDAJNA 
where  PREDAJNA.ID_PREDAJNE=ZAMESTNANEC.ID_PREDAJNE
order by nazov_predajne;
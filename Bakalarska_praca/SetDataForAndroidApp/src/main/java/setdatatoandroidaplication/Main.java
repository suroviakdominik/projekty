/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication;

import EditorsForInfo_Files.InfoSuboryENUM;
import Server.IndeterminateProgressBar;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * Obsahuje iba statickú metódu main(String args[]). 
 * V rámci tejto metódy sa vytvára inštancia triedy GUI, 
 * ktorá vytvorí základné grafické rozhranie. 
 * @author Dominik
 */
public class Main {

    /**
     * metóda vytvorí grafické používateľské rozhranie
     * @param args the command line arguments
     */
    public static void main(String[] args){
        //Ak chcem aby aplikácia vypadala Windowsacky odkomentuj
        /*try {
         UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            
         } catch (ClassNotFoundException ex) {
         Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
         } catch (InstantiationException ex) {
         Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IllegalAccessException ex) {
         Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
         } catch (UnsupportedLookAndFeelException ex) {
         Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
         }*/
        System.out.println(InfoSuboryENUM.MIN_LAT);
        System.out.println(InfoSuboryENUM.MAX_LAT);
        System.out.println(InfoSuboryENUM.MIN_LON);
        System.out.println(InfoSuboryENUM.MAX_LON);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                
                 GUI gui = new GUI();
            }
        });
    }
    

}


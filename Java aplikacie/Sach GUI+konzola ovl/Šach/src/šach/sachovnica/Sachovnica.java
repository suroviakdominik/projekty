/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica;

import šach.sachovnica.figurky.Figurka;

import Grafika.*;

/**
 * Má za úlohu vykresliť na plátno šachovnicu a figúrky uložené v dvojrozmernom
 * poli.
 *
 * @author Dominik
 */
public class Sachovnica {

    /**
     * vykreslí na plátno šachovnicu
     *
     */
    public void vykreslenieSachovnice() {
        boolean lpStriedaniePolicok = false;
        Obdlznik obdlznik;

        for (int i = 0; i < 8; i++) {
            lpStriedaniePolicok = !lpStriedaniePolicok;
            for (int j = 0; j < 8; j++) {
                if (lpStriedaniePolicok) {
                    obdlznik = new Obdlznik(50, 50, j * 50, i * 50, "black");
                    lpStriedaniePolicok = false;
                } else {
                    obdlznik = new Obdlznik(50, 50, j * 50, i * 50, "white");
                    lpStriedaniePolicok = true;
                }
                obdlznik.zobraz();
            }
        }
    }

    /**
     * vykreslí na plátno figúrky nachádzajúce sa v dvojrozmernom poli
     *
     * @param paMapaFigurok - dvojrozmerné pole figúrok
     */
    public void vykresliFigurky(Figurka[][] paMapaFigurok) {
        for (int i = 0; i < paMapaFigurok.length; i++) {
            for (int j = 0; j < paMapaFigurok.length; j++) {
                if (paMapaFigurok[i][j] != null) {
                    paMapaFigurok[i][j].umiestniFigurku(j * 50 + 10, i * 50 + 20);
                    paMapaFigurok[i][j].nakresli();

                }
            }

        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import Grafika.Obdlznik;

/**
 * Trieda Kon je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Kon extends Figurka {

    private IGrafickeObjekty[] aPoleGrafickychObjektovKona = new IGrafickeObjekty[3];

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba nového koňa
     */
    public Kon(String paFarba) {
        super("Kôň", paFarba);
    }

    /**
     * je metóda, ktorá vykreslí koňa na plátno.
     */
    @Override
    public void nakresli() {

        aPoleGrafickychObjektovKona[0] = new Obdlznik(15, 30, this.getX(), this.getY() - 10, this.getFarba());
        aPoleGrafickychObjektovKona[1] = new Obdlznik(30, 10, this.getX(), this.getY() - 10, this.getFarba());
        aPoleGrafickychObjektovKona[2] = new Obdlznik(30, 5, this.getX(), this.getY() + 2, this.getFarba());
        aPoleGrafickychObjektovKona[0].zobraz();
        aPoleGrafickychObjektovKona[1].zobraz();
        aPoleGrafickychObjektovKona[2].zobraz();

    }

    /**
     * je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie koňa.
     *
     * @return grafické objekty z ktorých sa skladá kôň
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aPoleGrafickychObjektovKona;
    }

    /**
     * metóda vracajúca popis koňa.
     *
     * @return popis koňa vo forme String
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Kôň/Jazdec" + "\n";
        popis += "Pohyb: šikmý skok ma každé tretie pole inej farby ako práve stojí" + "\n";
        popis += "Vyhadzovanie: šikmý skok ma každé tretie pole inej farby ako práve stojí" + "\n";
        popis += "Ine: pri pohybe a vyhadzovaní môže prejsť cez/ponad inú figúrku" + "\n";
        return popis;
    }
}

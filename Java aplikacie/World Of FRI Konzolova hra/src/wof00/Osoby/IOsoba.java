/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Osoby;

import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Predmety.IPredmet;

/**
 *
 * @author suroviak3
 */
public interface IOsoba {

    public String getaMeno();

    public void oslov();

    public void rozhovor(Hrac paHrac, String paOdpoved);

    public boolean jeOdpoved(String paOdpoved);
}

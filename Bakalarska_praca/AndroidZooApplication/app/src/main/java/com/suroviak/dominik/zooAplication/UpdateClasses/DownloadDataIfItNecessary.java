package com.suroviak.dominik.zooAplication.UpdateClasses;

import android.util.Log;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Dominik on 15.2.2015.
 */
public class DownloadDataIfItNecessary {
    private ServerWorker aServerWorker;
    private FileWorker aFileWorker;
    private ArrayList<Info_About_Dirs_File> aDownloadedInfoFiles;
    private ArrayList<Info_About_Dirs_File> aInfoFilesInDevice;
    private String aTempDirectory;
    private Info_About_Dirs_File aCurrenntlyUpdatedInfoFiles=null;

    private long aFullLengthToDownload;
    private boolean aFullLengthHasBeenComputed = false;
    private long aLengthOfDataThatWasDownloaded;
    private IProgressBarUpdate aCallFromActivity;

    public DownloadDataIfItNecessary(IProgressBarUpdate paUpdateActivity) {
        this.aServerWorker = new ServerWorker();
        aDownloadedInfoFiles = new ArrayList<Info_About_Dirs_File>();
        aInfoFilesInDevice = new ArrayList<Info_About_Dirs_File>();
        aFileWorker = new FileWorker();
        aTempDirectory = Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER + Info_Dirs_File_ENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM();
        aFullLengthToDownload = 0;
        aLengthOfDataThatWasDownloaded = 1;
        aCallFromActivity = paUpdateActivity;
    }

    public void createInfo_Anout_Dirs_Object() {
        try {
            aCallFromActivity.setText("Loading domain files objects");
            aFileWorker = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER);
            aFileWorker.createDirectory();
            aFileWorker.setaAbsolutePath(aTempDirectory);
            aFileWorker.createDirectory();
            File f = new File(Info_Dirs_File_ENUM.ADRESSMAINFILEONSERVER);

            aServerWorker.downloadDataFromURLToUrlComputeDirectory(Info_Dirs_File_ENUM.ADRESSMAINFILEONSERVER,
                    aTempDirectory + f.getName());

            aFileWorker = new FileWorker(aTempDirectory + f.getName());
            String domainsOfInfoFiles[] = aFileWorker.readDataFromFile().split("\\|\\|");
            aFileWorker.deleteFile();
            for (int i = 0; i < domainsOfInfoFiles.length; i++) {
                f = new File(domainsOfInfoFiles[i]);
                if (aServerWorker.isDomainValidDomain(domainsOfInfoFiles[i])) {
                    aServerWorker.downloadDataFromURLToUrlComputeDirectory(domainsOfInfoFiles[i],
                            aTempDirectory + f.getName());
                }
            }

            vytvorInfoSuboryZoStiahnutychInfoSuborov();
            vytvorInfoSuboryZInfoSuborovVZariadeni();


        } catch (IOException e) {
            e.printStackTrace();
            return;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
    }

    public void update() throws IOException {
        if (aDownloadedInfoFiles != null && aInfoFilesInDevice != null)
            compareInfoFilesAndUpdateIfAreNotUpdated();
    }

    public long computeSizeOfFilesToDownload() throws IOException {
        if (aDownloadedInfoFiles != null && aInfoFilesInDevice != null) {
            aCallFromActivity.setText("Computing length of data to download");
            vypocitajVelkostSuborovKtoreJePotrebneStiahnut();
            return aFullLengthToDownload;
        }
        return 10000;
    }

    private void vytvorInfoSuboryZoStiahnutychInfoSuborov() throws IOException, ClassNotFoundException {
        Info_About_Dirs_File iADF = null;
        File f = new File(aTempDirectory);
        SerializationWorker sw = new SerializationWorker("");
        sw.setPath(aTempDirectory);

        for (File prech : f.listFiles()) {

            if (prech.getName().endsWith("." + Info_Dirs_File_ENUM.ENDOFINFOFILE)) {
                sw.setNameOfFile(prech.getName());
                iADF = (Info_About_Dirs_File) sw.readObjetctFromFile();
                if (iADF == null) {
                    throw new IOException("Pokus o vytvorenie objektu: Info_About_Dirs_File zo stiahnutych suborov bol neuspesny.");
                }
                aDownloadedInfoFiles.add(iADF);

            }
        }
    }

    private void vytvorInfoSuboryZInfoSuborovVZariadeni() throws IOException, ClassNotFoundException {
        Info_About_Dirs_File iADF = null;
        File f = new File(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM());
        SerializationWorker sw = new SerializationWorker("");
        sw.setPath(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM());

        for (File prech : f.listFiles()) {

            if (prech.getName().endsWith("." + Info_Dirs_File_ENUM.ENDOFINFOFILE)) {
                sw.setNameOfFile(prech.getName());
                iADF = (Info_About_Dirs_File) sw.readObjetctFromFile();
                if (iADF == null) {
                    throw new IOException("Pokus o vytvorenie objektu: Info_About_Dirs_File zo stiahnutych suborov bol neuspesny.");
                }
                aInfoFilesInDevice.add(iADF);

            }
        }
    }

    private void compareInfoFilesAndUpdateIfAreNotUpdated() throws IOException {
        for (Info_About_Dirs_File prech : aDownloadedInfoFiles) {
            aCurrenntlyUpdatedInfoFiles=prech;
            Info_About_Dirs_File iADFInDevice = findInfoFileInDevice(prech.getNameOfDirForThisInfoFile());
            if (iADFInDevice != null) {

                compareItemsOfInfoFilesAndUpdateIfAreNotUpdated(prech.getaItemsInfo(), iADFInDevice.getaItemsInfo());
            } else {
                return;
            }

        }
        kopirujInfoSuboryDOPriecinkaAplikacie();
        aCallFromActivity.setProgressStatus(100);
    }

    private Info_About_Dirs_File findInfoFileInDevice(String paDirNameOfInfoFile) {
        for (Info_About_Dirs_File prech : aInfoFilesInDevice) {
            if (prech.getNameOfDirForThisInfoFile().equals(paDirNameOfInfoFile))
                return prech;
        }
        return null;
    }


    private void compareItemsOfInfoFilesAndUpdateIfAreNotUpdated(HashMap<String, Info_About_Item> paItemsOfDownloadInfoFile, HashMap<String, Info_About_Item> paItemsOfDeviceInfoFile) throws IOException {
        String paDirOfInfoFile = aFileWorker.getName();
        File f;

        for (String prech : paItemsOfDownloadInfoFile.keySet()) {
            if (paItemsOfDownloadInfoFile.get(prech).getRelativePathOfFilesOfurrentItemInDevice() != null) {

                if (paItemsOfDeviceInfoFile.get(prech) == null) {
                    for (String domain : paItemsOfDownloadInfoFile.get(prech).getDomainsOfFilesInCurrentItem()) {
                        f = new File(domain);
                        FileWorker fw = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                                + paItemsOfDownloadInfoFile.get(prech).getRelativePathOfFilesOfurrentItemInDevice());
                        fw.createDirectory();
                        try {
                            aServerWorker.downloadDataFromURLToUrlComputeDirectory(domain, Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                                    + paItemsOfDownloadInfoFile.get(prech).getRelativePathOfFilesOfurrentItemInDevice() + f.getName());

                            if (aCallFromActivity != null && aFullLengthHasBeenComputed) {
                                aLengthOfDataThatWasDownloaded += aServerWorker.getContentsLength(domain);
                                int status = (int) (100 * (double) aLengthOfDataThatWasDownloaded / aFullLengthToDownload);
                                aCallFromActivity.setProgressStatus(status);
                            }
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                            Log.e("MyLogFilesERROR:","Súbor sa nepodarilo stiahnuť");
                            /*aCurrenntlyUpdatedInfoFiles.getaItemsInfo().remove(prech);
                            SerializationWorker<Info_About_Dirs_File> sw=new SerializationWorker<>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
                            sw.setPath(Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER+aCurrenntlyUpdatedInfoFiles.getaRelativePathOfThisFileInDevice());
                            if(Info_Dirs_File_ENUM.getNameOfInfoFileObject(aCurrenntlyUpdatedInfoFiles)!=null) {
                                sw.setNameOfFile(Info_Dirs_File_ENUM.getNameOfInfoFileObject(aCurrenntlyUpdatedInfoFiles));
                                try {
                                    sw.saveObjectToFile(aCurrenntlyUpdatedInfoFiles);

                                } catch(Exception exSerial){
                                    exSerial.printStackTrace();
                                }

                            }*/
                        }
                    }
                    stiahniSuborPreNavigaciu();
                } else if (paItemsOfDownloadInfoFile.get(prech).getDateOfEditOrCreateFile().compareTo(paItemsOfDeviceInfoFile.get(prech).getDateOfEditOrCreateFile()) > 0) {
                    for (String domain : paItemsOfDownloadInfoFile.get(prech).getDomainsOfFilesInCurrentItem()) {
                        f = new File(domain);
                        try {
                            aServerWorker.downloadDataFromURLToUrlComputeDirectory(domain, Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                                    + paItemsOfDownloadInfoFile.get(prech).getRelativePathOfFilesOfurrentItemInDevice() + f.getName());
                            if (aCallFromActivity != null && aFullLengthHasBeenComputed) {
                                aLengthOfDataThatWasDownloaded += aServerWorker.getContentsLength(domain);
                                int status = (int) (100 * (double) aLengthOfDataThatWasDownloaded / aFullLengthToDownload);
                                aCallFromActivity.setProgressStatus(status);
                            }
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                            Log.e("MyLogFilesERROR:","Súbor sa nepodarilo stiahnuť");
                            aCurrenntlyUpdatedInfoFiles.getaItemsInfo().remove(prech);
                            /*SerializationWorker<Info_About_Dirs_File> sw=new SerializationWorker<>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
                            sw.setPath(Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER+aCurrenntlyUpdatedInfoFiles.getaRelativePathOfThisFileInDevice());
                            if(Info_Dirs_File_ENUM.getNameOfInfoFileObject(aCurrenntlyUpdatedInfoFiles)!=null) {
                                sw.setNameOfFile(Info_Dirs_File_ENUM.getNameOfInfoFileObject(aCurrenntlyUpdatedInfoFiles));
                                try {
                                    sw.saveObjectToFile(aCurrenntlyUpdatedInfoFiles);

                                } catch(Exception exSerial){
                                    exSerial.printStackTrace();
                                }

                            }*/
                        }
                    }
                    stiahniSuborPreNavigaciu();
                }
            }
        }


    }

    private void stiahniSuborPreNavigaciu() throws IOException {
        ServerWorker sw = new ServerWorker();
        sw.downloadDataFromURLToUrlComputeDirectory(Info_Dirs_File_ENUM.ADRESS_OFFLINE_MAP_FILE_ON_SERVER,
                Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER + Info_Dirs_File_ENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE + Info_Dirs_File_ENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD);
    }

    private void kopirujInfoSuboryDOPriecinkaAplikacie() {
        for (Info_Dirs_File_ENUM walker : Info_Dirs_File_ENUM.values()) {
            String pathInTempFold = Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER + walker.getaRelativePathOfDirecotryWhereIAM()
                    + walker.getaNameOfInfoFile()
                    + "." + Info_Dirs_File_ENUM.ENDOFINFOFILE;
            String sourceTarget = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + walker.getaRelativePathOfDirecotryWhereIAM()
                    + walker.getaNameOfInfoFile()
                    + "." + Info_Dirs_File_ENUM.ENDOFINFOFILE;
            FileWorker.copyFile(pathInTempFold, sourceTarget);
        }
        FileWorker.copyFile(Info_Dirs_File_ENUM.TEMPORARY_DEVICE_FOLDER +
                        Info_Dirs_File_ENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE +
                        Info_Dirs_File_ENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD,

                Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER +
                        Info_Dirs_File_ENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE +
                        Info_Dirs_File_ENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD
        );
    }


    private void vypocitajVelkostSuborovKtoreJePotrebneStiahnut() throws IOException {

        for (Info_About_Dirs_File prech : aDownloadedInfoFiles) {
            Info_About_Dirs_File iADFInDevice = findInfoFileInDevice(prech.getNameOfDirForThisInfoFile());
            if (iADFInDevice != null) {

                for (String prechItem : prech.getaItemsInfo().keySet()) {
                    if (prech.getaItemsInfo().get(prechItem).getRelativePathOfFilesOfurrentItemInDevice() != null) {

                        if (iADFInDevice.getaItemsInfo().get(prechItem) == null) {
                            for (String domain : prech.getaItemsInfo().get(prechItem).getDomainsOfFilesInCurrentItem()) {

                                aFullLengthToDownload += aServerWorker.getContentsLength(domain);
                            }
                        } else if (prech.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile().compareTo(iADFInDevice.getaItemsInfo().get(prechItem).getDateOfEditOrCreateFile()) > 0) {
                            for (String domain : prech.getaItemsInfo().get(prechItem).getDomainsOfFilesInCurrentItem()) {
                                File f = new File(domain);
                                aFullLengthToDownload += aServerWorker.getContentsLength(domain);
                            }
                        }
                    }
                }

            }


        }
        aFullLengthHasBeenComputed = true;
    }


}
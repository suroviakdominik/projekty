﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Snake.DrawableObjects
{
    interface IDrawableObject
    {
        /**
         * kazdy objekt ktory sa chce vykreslit, musi najskor nacitat komponenty ktore chce vykreslit
         * pomocou ContentManagera
         */
        void LoadContent(ContentManager content);

        /**
         * objekt na obrazovke moze menit svoj vzhlad na zaklade pouzivatelovych vstupov
         */
        void Update(GameTime gameTime);
        void Draw(SpriteBatch drawer);
    }
}

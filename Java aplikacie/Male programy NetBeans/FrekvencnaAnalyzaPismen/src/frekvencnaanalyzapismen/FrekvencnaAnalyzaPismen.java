/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frekvencnaanalyzapismen;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dominik
 */
public class FrekvencnaAnalyzaPismen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Map.Entry<Character,Integer>> a=FileWorker.readDataFromFile(new File("./popis.txt"));
        long fullCount=0;
        for(Map.Entry<Character,Integer> prech:a)
        {
            fullCount+=prech.getValue();
        }
        String ret="";
        for(Map.Entry<Character,Integer> prech:a)
        {
            ret+="Char: "+prech.getKey()+" Count: "+prech.getValue()+" Percent: "+(double)prech.getValue()/fullCount*100+"\n";
        }
        System.out.println(ret);
        System.out.println("Pocet vsetkych: "+fullCount);
        FileWorker.writeDataToFile(new File("./vystup.txt"), ret);
    }
    
}

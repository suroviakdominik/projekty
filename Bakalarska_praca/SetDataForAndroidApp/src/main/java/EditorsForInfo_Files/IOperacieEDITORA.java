/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import setdatatoandroidaplication.Tabs.Tab;

/**
 * Interface definujuci zakladne rozhranie editorov.    Editory su pouzivane zalozkami
 * . V ramci editorov sa data nacitavaju, vytvaraju sa nove polozky.
 * Kazdy edito ma objekt Info_About_DIRS_FIle, ktory obsahuje vsetky polozky a informacie
 * o suboroch, ktore danej polozke zodpovedaju
 * @author Dominik
 */
public interface IOperacieEDITORA {
    public boolean addNewItem();
    public boolean deleteItem();
    public boolean editItem(); 
    public boolean saveInfoFile();
    public boolean getDataOfInfoFile();
    public Info_About_Dirs_File getInfoFileThatIEdit();
    
    
}

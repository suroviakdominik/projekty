﻿using System;
using System.Collections.Generic;
using System.Net;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Snake.DrawableObjects;

namespace Snake.Network
{
    public class NetworkingClient
    {
        private NetClient aClient;///klient
        private IPAddress aIpAdress;///ip adresa servera
        private Game1 aGame;

        ///hra

        public LinkedList<PositionMessage> aPositionMessages { get; set; }

        ///spravy o zmene pozicie ziskane pri poslednom update dat zo servera
        public event EventHandler<NewFoodGeneratedEventArgs> EPridajJedlo;///udalost ak online hrac zjedol jedlo skor ako ja
        public NetPeerStatus ClientStatus { get { return aClient.Status; } }///aktualny status klienta

        ///<summary>
        /// konstruktor
        /// </summary>
        public NetworkingClient(string paServerIpAdress, Game1 paGame)
        {
            aGame = paGame;
            aIpAdress = IPAddress.Parse(paServerIpAdress);
            var config = new NetPeerConfiguration("snake");
            aClient = new NetClient(config);
            aPositionMessages = new LinkedList<PositionMessage>();
        }


        /// <summary>
        /// trida ktora bude ako parameter posluchacom pri vyvolani udalosti
        /// </summary>
        public class NewFoodGeneratedEventArgs : EventArgs
        {
            private Vector2 aVector;
            public NewFoodGeneratedEventArgs(float x, float y)
            {
                aVector = new Vector2(x, y);
            }

            public Vector2 Vector
            {
                get { return aVector; }
            }
        }

        /// <summary>
        /// vyvolanie udalosti pri generovani noveho jedla
        /// </summary>
        /// <param name="e"></param>
        protected virtual void vytvorenieNovehoJedla(NewFoodGeneratedEventArgs e)
        {
            if (EPridajJedlo != null)
            {
                EPridajJedlo(this, e);
            }
        }

        /// <summary>
        /// spusti klienta- pripoji k serveru
        /// </summary>
        public void startClient()
        {
            aClient.Start();
            aClient.Connect(aIpAdress.ToString(), 800);
        }

        /// <summary>
        /// cita dat, ktore dostal zo servera 
        /// </summary>
        public void readData()
        {
            aPositionMessages.Clear();
            NetIncomingMessage message;
            while ((message = aClient.ReadMessage()) != null)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        //Console.WriteLine("Data has been received from server: " + message.SenderEndPoint.Address);
                        processIncomingData(message);
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        // handle connection status messages
                        message.ReadString();
                        Console.WriteLine(@"Status change");
                        switch (message.SenderConnection.Status)
                        {
                            case NetConnectionStatus.Connected:
                                Console.WriteLine(@"User connected" + message.SenderEndPoint);
                                break;
                            case NetConnectionStatus.Disconnected:
                                Console.WriteLine(@"User disconnected");
                                aGame.deleteOnlinePlayer(message.SenderEndPoint);
                                break;
                        }
                        break;

                    case NetIncomingMessageType.DebugMessage:
                        Console.WriteLine(message.ReadString());
                        break;
                    /* .. */
                    default:
                        //Console.WriteLine("unhandled message with type: "
                        // + message.MessageType);
                        break;
                }
            }
        }

        /// <summary>
        /// spracuje data ziskane pri cirani zo servera
        /// </summary>
        /// <param name="message"></param>
        private void processIncomingData(NetIncomingMessage message)
        {
            IPEndPoint ip = message.ReadIPEndPoint();
            TypDat dataTyp = (TypDat)message.ReadByte();
            switch (dataTyp)
            {
                case TypDat.ZmenaPozicie:
                    Direction dir = (Direction)message.ReadByte();
                    bool moveSnakeAboutPartWidth = message.ReadBoolean();
                    var posMess = new PositionMessage(dir, moveSnakeAboutPartWidth, message.ReadFloat(), ip);
                    aPositionMessages.AddLast(posMess);
                    break;
                case TypDat.VygenerovaneNoveJedlo:
                    float x = message.ReadFloat();
                    float y = message.ReadFloat();
                    aGame.getSnake(ip).grow();
                    vytvorenieNovehoJedla(new NewFoodGeneratedEventArgs(x, y));
                    //Console.WriteLine("Nove jedlo: " + x + " , " + y);
                    break;

                case TypDat.PripojenieHraca:
                    Console.WriteLine(@"Pripojenie hraca" + ip);
                    aGame.addOnlinePlayer(ip);
                    break;

                case TypDat.OdpojenieHraca:
                    Console.WriteLine(@"Odpojenie hraca " + ip);
                    aGame.deleteOnlinePlayer(ip);
                    break;
                case TypDat.PrehraHraca:
                    Console.WriteLine(@"Prehra hraca " + ip);
                    aGame.deleteOnlinePlayer(ip);
                    break;
            }
        }

        /// <summary>
        /// odosle data o novej pozicii jedla na server
        /// </summary>
        /// <param name="type"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void sendData(byte type, float x, float y)
        {
            NetOutgoingMessage outMess = aClient.CreateMessage();
            outMess.Write(type);
            outMess.Write(x);
            outMess.Write(y);
            aClient.SendMessage(outMess, NetDeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        /// odpojenie klienta zo servera
        /// </summary>
        public void stopClient()
        {
            aClient.Disconnect("Client was disconnected from server");
        }

        /// <summary>
        /// odoslanie dat o pohybe hada na server
        /// </summary>
        /// <param name="type"></param>
        /// <param name="smer"></param>
        /// <param name="paPrepocitavalSom"></param>
        /// <param name="sizeOfMove"></param>
        public void sendData(byte type, byte smer, bool paPrepocitavalSom, float sizeOfMove)
        {
            NetOutgoingMessage outMess = aClient.CreateMessage();
            outMess.Write(type);
            outMess.Write(smer);
            outMess.Write(paPrepocitavalSom);
            outMess.Write(sizeOfMove);
            aClient.SendMessage(outMess, NetDeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        /// odoslanie dat na server
        /// </summary>
        /// <param name="type"></param>
        public void sendData(byte type)
        {
            NetOutgoingMessage outMess = aClient.CreateMessage();
            outMess.Write(type);
            aClient.SendMessage(outMess, NetDeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        /// instancia je sprava ziskana zo servera o zmene pozicie online hada
        /// </summary>
        public class PositionMessage
        {
            public PositionMessage(Direction direction, bool paMoveSnakePartWidth, float sizeOfMovement, IPEndPoint fomClient)
            {
                Direction = direction;
                SizeOfMovement = sizeOfMovement;
                FomClient = fomClient;
                MoveSnakeAboutPartWidth = paMoveSnakePartWidth;
            }

            public Direction Direction { get; set; }
            public float SizeOfMovement { get; set; }
            public IPEndPoint FomClient { get; set; }
            public bool MoveSnakeAboutPartWidth { get; set; }
        }

    }
}

;Na��ta� vetu z kl�vesnice a ukon�i� ju klasicky interpunk�n�m znamienkom ( . ? ! ).
;a) Na konzolu vyp�sa� slovom o ak� vetu sa jedn� (Oznamovacia, Rozkazovacia, Opytovacia).
;b) Toto ozn�menie zap�sa� do s�boru.
;c) N�js� vo vete najdlh�ie slovo a vyp�sa� ho na konzolu.

Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Retaz1 DB 15 dup(?)
Retaz DB 9 DUP(?)
DesiatPriNas DW 10
PopisovacSuboru DW (?)
MenoSuboru DB 'Subor.txt',0 
UkoncovaciZnakVety DB 0
Oznamovacia DB 0
Opytovacia DB 0
Rozkazovacia DB 0
IndexNajdlhsiehoSlova DW ?
PocetZnakovNajdenehoSlova DW 0
Chyba DB 0
VypisOz DB 'Zadana veta je oznamovacia.$'
VypisOp DB 'Zadana veta je opytovacia.$'
VypisRo DB 'Zadana veta je rozkazovacia.$'
errorVypChybaUkon DB 'Vo vete chyba ukoncovaci znak!$'
.CODE;//zacina kodovy segment
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo
                                                                                                                         
Write ENDP

Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 

;..................................................PROCEDURA NACITA RETAZEC S TERMINALU A ULOZI HO DO PREMENNEJ RETAZ1..........................
NacitajRetazec PROC
         push cx
         push ax
         mov Retaz1,13
         mov DX,offset Retaz1;do registra dx ulozime offset Retazca
         mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
         int 21h
           
         mov DL,0AH;uloz do registra posun dole
         mov AH,2;do registra AH uloz cislo sluzby
         int 21h
         
         mov ch,0
         mov cl,Retaz1[1]
         jcxz Na
        CyklusVypVlavo:
            mov DL,0DH;uloz do registra posun vlavo
            mov AH,2
            int 21h
            mov ax,0
         loop CyklusVypVlavo
         
       Na:
         pop ax
         pop cx
         ret
ENDP NacitajRetazec

;..............................................PROCEDURA VYPISE RETAZEC RETAZ1 POCNUC INDEXOM 2.....................................
VypRetazec PROC
push cx
push dx
push ax


mov di,2
mov ch,0
mov cl,Retaz1[1]
jcxz Kon
Vypisuj:
mov dl,Retaz1[di]
mov ah,2
int 21h
inc di
loop Vypisuj
Kon: 
    pop ax
    pop dx
    pop cx
    ret
ENDP VypRetazec

;.....................................PROCEDURA ZMAZE PREDCHADZAJUCI OBSAH TERMINALU.......................................................
ClrScr PROC
	push ax
	
	mov ah,0
	mov al,3
	int 10h
	
	pop ax
	
	ret
ClrScr ENDP

ZadanieA PROC
push ax
push bx
push cx
push dx
push si
push di
          call NacitajRetazec
          call VypRetazec 
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
         
          mov di,2
          NajdiUkoncovaciZnakVety:
             cmp Retaz1[di],'.'
             jz NasielSaUkonZnak
             cmp Retaz1[di],'?'
             jz NasielSaUkonZnak
             cmp Retaz1[di],'!'
             jz NasielSaUkonZnak
             cmp Retaz1[di],0Dh
             jnz InkremDi
             jmp ChybaUkoncovaciZnak
           InkremDi:
             inc di
             jmp NajdiUkoncovaciZnakVety
            
           NasielSaUkonZnak:
             mov al,Retaz1[di]
             mov UkoncovaciZnakVety,al
             cmp UkoncovaciZnakVety,'.'
             jz Oznamov
             cmp UkoncovaciZnakVety,'?'
             jz Opytovac
             cmp UkoncovaciZnakVety,'!'
             jz Rozkazov 
             
             Oznamov:
             mov Oznamovacia,1
             mov dx, offset VypisOz
             jmp Spat
             Rozkazov:
             mov Rozkazovacia,1
             mov dx, offset VypisRo
             jmp Spat
             Opytovac:
             mov Opytovacia,1
             mov dx, offset VypisOp
             jmp Spat 
             
          ChybaUkoncovaciZnak:
                mov Chyba,1
                mov dx, offset errorVypChybaUkon
                
          Spat:
           mov ah,9
           int 21h
           pop di
           pop si
           pop dx
           pop cx
           pop bx
           pop ax
           ret
ENDP ZadanieA

ZadanieB PROC
push ax
push bx
push cx
push dx
push si
push di
          mov dx,offset MenoSuboru
          mov ah,3Ch
          mov cx,0
          int 21h
          mov PopisovacSuboru,ax
          ;---------------------------------------------ZAPISE DO SUBORU POCET BYTOV ZADANYCH V REGISTRIcx-----------------------------
          mov bx,PopisovacSuboru
          mov ch,0
          mov cx, 22
          cmp Opytovacia,1
          jnz Next1
          mov dx, offset VypisOp
          jmp Zapis
         Next1:
          cmp Oznamovacia,1
          jnz Next2
          mov dx, offset VypisOz
          jmp Zapis
         Next2:
          cmp Rozkazovacia,1
          jnz Next3
          mov dx, offset VypisRo
          jmp Zapis
         Next3:
            mov dx,offset errorVypChybaUkon
          
          Zapis:
              mov ah,40h
              int 21h
          ;-------------------------------------ZATVOR SUBOR-------------------------------------------------------
          mov bx,PopisovacSuboru
          mov ah,3Eh
          int 21h
           
           pop di
           pop si
           pop dx
           pop cx
           pop bx
           pop ax
           ret
ENDP ZadanieB
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je retaz, nemozno ju priradit rovno do ds ak ds,@data 
          
          call ZadanieA
          call ZadanieB
          mov di,2
          
          mov ch,0
          mov cl,Retaz1[1]
          Hladaj:
            mov si,0
            
            ZistiVelkostSlova:
              cmp Retaz1[di],0Dh
                jz KonHladania
              cmp Retaz1[di],' '
              jz JeNajdlhSlovo
              inc si
              inc di
              jmp ZistiVelkostSlova 
              
             JeNajdlhSlovo:
                
                cmp si,PocetZnakovNajdenehoSlova
                jg NoveNajdlSlovo
                jmp ContHlad
              NoveNajdlSlovo:
              mov IndexNajdlhsiehoSlova,di
              sub IndexNajdlhsiehoSlova,si
              mov PocetZnakovNajdenehoSlova,si
             ContHlad:
             inc di
          loop Hladaj
          
          KonHladania:
          mov ax,IndexNajdlhsiehoSlova  
          call Write

          
          mov ax,PocetZnakovNajdenehoSlova 
          call Write
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

/**
 *
 * @author Dominik
 */
public class Distance {
    /**
     * vypocita pribliznu vzdialenost dvoch vodob zadanych ako geo koordinaty
     * @param lat1 - zemepisna sirka bod 1
     * @param lon1 - zemepisna dlzka bod 1
     * @param lat2 - zemepisna sirka bod 2
     * @param lon2 - zemepisna dlzka bod 2
     * @param unit - jednotky, v ktorych sa vrati vzdialenost (napr. metre)
     * @return vzdialenost dvoch bodov v jednotkach urcenych parametrom
     */
     public static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        else if(unit =='M')
        {
            dist = dist * 1.609344;
            dist=dist*1000;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double deg2rad(double deg) {
	  return (deg * Math.PI / 180.0);
	}
	 
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
	  return (rad * 180 / Math.PI);
	}
}

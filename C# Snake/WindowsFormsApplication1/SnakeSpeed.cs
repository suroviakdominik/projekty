﻿namespace SnakeServer
{
    public enum SnakeSpeed
    {
        Normal,
        Fast,
        FastUnchanged,
        NormalUnchanged
    }
}
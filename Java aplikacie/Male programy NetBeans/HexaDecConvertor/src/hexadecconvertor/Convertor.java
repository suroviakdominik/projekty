/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hexadecconvertor;

/**
 *
 * @author Dominik
 */
public abstract class Convertor {
    public static String convertDecToHex(String paDec)
    {
        try
        {
            Integer a=Integer.parseInt(paDec);
            return Integer.toHexString(a);
        }catch(NumberFormatException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static String convertHEXToDec(String paHex)
    {
        try
        {
            Integer a=Integer.parseInt(paHex,16);
            return a.toString();
            
        }catch(NumberFormatException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeoLocationComponents;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


/**
 *
 * @author Dominik
 */
public class JNumberTextField<N> extends JTextField implements FocusListener {

    private Double aMinValue;
    private Double aMaxValue;
    private String aDefaultText;
    private boolean aIsSetValidaValue;
    private boolean aCheckForIntegerType;
    private Double aValueBeforeChange;

    public JNumberTextField(double paMinValue, double paMaxValue, String paDefaultText, boolean checkForIntegerTypeButNotDouble) {
        super();
        aCheckForIntegerType = checkForIntegerTypeButNotDouble;
        aIsSetValidaValue = false;
        aValueBeforeChange = null;
        aMinValue = paMinValue;
        aMaxValue = paMaxValue;
        aDefaultText = paDefaultText;
        setText(aDefaultText);
        this.addFocusListener(this);
    }

    public boolean isSetValidaValue() {
        return aIsSetValidaValue;
    }
    
    public void setIsSetValidValue(boolean paIsValidValue)
    {
        aIsSetValidaValue=paIsValidValue;
    }

    @Override
    public void focusGained(FocusEvent fe) {
        if (getText().equals(aDefaultText)) {
            setText("");
        } else {
            aValueBeforeChange = Double.parseDouble(getText());
        }
    }

    @Override
    public void focusLost(FocusEvent fe) {

        if (!getText().equals("") && !getText().equals(aDefaultText)) {
            try {
                if (!aCheckForIntegerType) {
                    Double.parseDouble(getText());
                    if (Double.parseDouble(getText()) < aMaxValue && Double.parseDouble(getText()) > aMinValue) {
                        aIsSetValidaValue = true;
                        setText("" + Double.parseDouble(getText()));
                    } else {
                        throw new NumberFormatException();
                    }
                }
                else
                {
                    Integer.parseInt(getText());
                    if (Integer.parseInt(getText()) < aMaxValue && Integer.parseInt(getText()) > aMinValue) {
                        aIsSetValidaValue = true;
                        setText("" + Integer.parseInt(getText()));
                    } else {
                        throw new NumberFormatException();
                    }
                }

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Zadaný údaj musí byť číslo v rozmedzí: " + aMinValue + "-" + aMaxValue, "Not Number", JOptionPane.INFORMATION_MESSAGE);
                if (aValueBeforeChange != null) {
                    this.setText("" + aValueBeforeChange);
                } else {
                    this.setText(aDefaultText);
                }
                aIsSetValidaValue = false;
            }
        } else if (getText().equals("")) {
            setText(aDefaultText);
        }
    }

}

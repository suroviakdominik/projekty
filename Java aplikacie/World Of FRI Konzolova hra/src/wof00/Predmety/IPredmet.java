/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public interface IPredmet {

    public String getNazov();

    public String getPopis();

    public String pouzi(Hrac paHrac);
}

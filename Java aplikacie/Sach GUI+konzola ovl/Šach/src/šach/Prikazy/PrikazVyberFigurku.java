/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import šach.hra.Hrac;
import šach.sachovnica.figurky.Figurka;

/**
 * Použije sa po zadaní príkazu vyber a vyberie figúrku s ktorou chce hráč alebo
 * zistiť o nej informácie. Pri príkazoch ako choď a info je nutné použiť
 * najskôr príkaz vyber a vybrať príslušnú figúrku.
 *
 * @author Dominik
 */
public class PrikazVyberFigurku implements IPrikaz {

    /**
     * priradí hráčovi figúrku do atribútu aVybranaFigurka figúrku ktorá sa
     * nachádza na šachovnici na súradniciach prijatých parametrom lpParameter.
     * Vždy vracia false keďže hráči sa po zadaní tohto príkazu nestriedajú.
     *
     * @param paHracKtoryZadavaPrikazy - hráč zadávajúci príkazy
     * @param lpParameter - súradnice figúrky na šachovnici
     * @return false vždy
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {

        if (lpParameter != null) {
            String[] lpPoleSuradnic = new String[2];
            try {
                lpPoleSuradnic[0] = Character.toString(lpParameter.charAt(0));
                lpPoleSuradnic[1] = Character.toString(lpParameter.charAt(1));
            } catch (StringIndexOutOfBoundsException ex) {
                System.out.println("Nezadali ste suradnice kam sa chcete posunut. Zopakujte prikaz a zadajte suradnice.");
                return false;
            }
            int lpYlonovaSuradnica;
            int lpXsovaSuradnica;

            Figurka lpVybranaFigurka;
            try {
                lpYlonovaSuradnica = Integer.parseInt(lpPoleSuradnic[0]) - 1;
                lpXsovaSuradnica = Integer.parseInt(lpPoleSuradnic[1]) - 1;
            } catch (NumberFormatException ex) {
                System.out.println("Zadali ste nespravne suradnice. Zopakujte volbu.");
                return false;
            }

            if ((lpYlonovaSuradnica < 8) && (lpYlonovaSuradnica >= 0) && (lpXsovaSuradnica < 8) && (lpXsovaSuradnica >= 0) && (paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpYlonovaSuradnica][lpXsovaSuradnica] != null)) {
                if (paHracKtoryZadavaPrikazy.getFarbaFigurok().equals(paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpYlonovaSuradnica][lpXsovaSuradnica].getFarba())) {
                    paHracKtoryZadavaPrikazy.setaVybranaFigurka(paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpYlonovaSuradnica][lpXsovaSuradnica]);
                    System.out.println("Vybrali ste si figurku: " + paHracKtoryZadavaPrikazy.getaVybranaFigurka().getMeno() + " z pozicie: Y: " + (lpYlonovaSuradnica + 1) + " X: " + (lpXsovaSuradnica + 1));

                } else {
                    System.out.println("Superovu figurku si nemozete vybrat. Zopakujte prikaz.");
                }
            } else {
                System.out.println("Dana pozicia neexistuje alebo sa na nej nenachadza ziadna figurka.");
            }
        } else {
            System.out.println("Ziadny paratmeter. Zopakujte vyber a zadajte suradnice.");
        }

        return false;
    }

}

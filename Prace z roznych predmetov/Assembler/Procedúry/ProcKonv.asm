.Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
DesiatPriNas DW 10
Postupnost DW 8,10,-5,12,3,6
dlzkaPostupnost DW 6
Ano DW 'A'
Nie DW 'N' 

.CODE;//zacina kodovy segment
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret

Read ENDP 

Write PROC 
      push dx
      push cx
      push ax
      mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
          
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        mov di,0
        pop ax
        pop cx
        pop dx
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo

Write ENDP
        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          
          mov cx,dlzkapostupnost
          call Read
          mov di,0
          mov bx,offset Postupnost
       Cyklus:
            cmp ax,[bx+di] 
            jz VypisAno
             inc di
             inc di
             loop Cyklus
             jmp  VypisNie
        
        VypisAno:
            mov dl,'A'
            mov ah,2 
            int 21h 
            jmp InicDlzkPost
        
        VypisNie:
            mov dl,'N'
            mov ah,2 
            int 21h    
            
       
         InicDlzkPost:
            
          mov cx,dlzkaPostupnost
          
          mov di,0
          mov bx,offset Postupnost
          mov ax,[Postupnost]
          mov si,0          

        CyklusMin:
            cmp ax,[bx+di] 
            jg AktualizujAX
            inc di
            inc di
            loop CyklusMin
            jmp VypisMin            

         AktualizujAX:
            mov ax,[bx+di]
            mov si,di
            inc di
            inc di
            
            jmp CyklusMin 
            
            
        VypisMin:
             call Write
 
        VypisIndex:
           mov DL,0AH;uloz do registra posun dole
           mov AH,2;do registra AH uloz cislo sluzby
           int 21h
           mov DL,0DH;uloz do registra posun vlavo
           mov AH,2
           int 21h
           mov ax,0
           shr si,1
           mov ax,si
           call Write

        
           
        
      
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































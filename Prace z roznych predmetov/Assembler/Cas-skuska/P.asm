;---------------------------------------------------------------ZADANIE-POSTUPNOST-------------------------------------------------------------------
;a) Na��tajte �asov� �daj vo form�te hh:mm:ss ak je vo validnom form�te ukon�ite program, ak nie opakujte vstup.
;b) Preve�te �as na min�ty (sekundy zaokr�hlite) a vyp�te na obrazovku.
;c) �as v min�tach z �lohy b) zap�te do textov�ho s�boru. 

.Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
MenoSuboru DB 'TextSub.txt',0
PopisovacSuboru DW ?
ZapisanyZnakDoSuboru DB ?
Retaz1 DB 11 dup (?) 
PomocnaRetaz DB 3 dup (?)
DesiatPriNas DW 10
CasVMinutach DW (?)
SestdesiatPriNas DB 60
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
VypZadajteCas DB 'Zadajte cas vo formate HH:MM:SS. Zadajte aj dvojbodky. Cas: $'
VypMinutach DB 'Cas v minutach: $'
errorVypZleZadanyCas DB 'Cas ste zadali v nespravnom formate alebo zle. Format je: hh:mm:ss. $'
.CODE;//zacina kodovy segment
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 

WriteFile PROC 
      push dx
      push cx
      push ax
      push bx
      mov cx,0
          
     
    
     
      KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        push cx
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        loop VypisCisl
        
       
       pop bx
       pop ax
       pop cx
       pop dx
       ret

     

WriteFile ENDP
;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

                                                                                                                         
Write ENDP

;------------------------------------------------PROCEDURA KONVERZIA CISLA V ASCII TYPU WORD KODE NA BINARNE CISLO----------------------------------------------
KonverziaZDesCislaNaDvojCislo PROC
;vstupnymi parametrami su OFFSET retazec ukonceny koncovym znakom 0Dh do registra BX a offset od ktoreho zacinaju cisla v retazci do registra DI
;skonvertovne cislo vracia v registri ax
          push cx
          push dx
          push di
          mov dx,0
          mov ax,0
          cmp byte ptr [bx+di],'-'
          jz CisloJeZaporn
          mov dl,0 
          
KonvertujRetazecNaCisl:
           
          
          mov cl,[bx+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporn
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          push dx
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          pop dx
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCisl
          

CisloJeZaporn:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov dl, 1
          jmp KonvertujRetazecNaCisl

RozhodniCiJeCisloZaporn:
          cmp dl,1
          jz DvojkovyDoplno
          mov di,0
          mov cx,0
          NavratSPro:
          pop di
          pop dx 
          pop cx
          ret
DvojkovyDoplno:
         neg ax;dvojkovy doplnok zmeni znamienko cisla
         jmp NavratSPro

KonverziaZDesCislaNaDvojCislo ENDP
;------------------------------PROCEDURA NACITA CASOVY UDAJ VO FORMATE HH:MM:SS
NacitajCasovyUdaj PROC
push ax
push dx
    mov Retaz1[0],9
    mov dx, offset Retaz1
    mov ah,0Ah
    int 21h
 pop dx
 pop ax
 ret
ENDP NacitajCasovyUdaj
;---------------------------------PROCEDURA ZISTI CI JE ZADANY CAS NACITANY V SPRAVNOM TVARE----------------
;vracia 1 v registri si ak doslo k chybe,0 ak nedoslo k chybe
KONTROLANACITANEHOCASU PROC
push ax
push bx
push cx
push dx
push di
 

 KOntrolaHodin:
    mov bl,Retaz1[2]
    mov PomocnaRetaz[0],bl
    mov bl,Retaz1[3]
    mov PomocnaRetaz[1],bl
    mov PomocnaRetaz[2],0Dh
    mov bx,offset PomocnaRetaz
    mov di,0
    call KonverziaZDesCislaNaDvojCislo
    cmp ax,0
    jl ZleNacitanyCas
    cmp ax,24
    jg ZleNacitanyCas
 
  KOntrolaMinut:
    mov bl,Retaz1[5]
    mov PomocnaRetaz[0],bl
    mov bl,Retaz1[6]
    mov PomocnaRetaz[1],bl
    mov PomocnaRetaz[2],0Dh
    mov bx,offset PomocnaRetaz
    mov di,0
    call KonverziaZDesCislaNaDvojCislo
    cmp ax,0
    jl ZleNacitanyCas
    cmp ax,60
    jge ZleNacitanyCas

  KOntrolaSekund:
    mov bl,Retaz1[8]
    mov PomocnaRetaz[0],bl
    mov bl,Retaz1[9]
    mov PomocnaRetaz[1],bl
    mov PomocnaRetaz[2],0Dh
    mov bx,offset PomocnaRetaz
    mov di,0
    call KonverziaZDesCislaNaDvojCislo
    cmp ax,0
    jl ZleNacitanyCas
    cmp ax,60
    jge ZleNacitanyCas
   
  KontrolaDvojbodiek:
   cmp Retaz1[4],':'
   jnz ZleNacitanyCas
    cmp Retaz1[7],':'
    jnz ZleNacitanyCas   
    mov si,0
    jmp VratSaSpat
    
 
 ZleNacitanyCas:
  mov si,1
 VratSaSpat:  
 pop di
 pop dx
 pop cx
 pop bx
 pop ax
 ret
ENDP KONTROLANACITANEHOCASU

VypisRetazec PROC
push ax
push dx
push di
  mov DL,0AH;uloz do registra posun dole
  mov AH,2;do registra AH uloz cislo sluzby
  int 21h
  mov di,2
 Vypisuj:
  cmp Retaz1[di],0DH 
  jz Spat
  mov dl,Retaz1[di]
  mov ah,2
  int 21h
  inc di
  jmp Vypisuj
  
  Spat:
  pop di
  pop dx
  pop ax
  ret
ENDP VypisRetazec
;----------------------------------------ZADANIE A------------------------------------------
ZadanieA PROC
push ax
push bx
push cx
push dx
push si
push di
ZadajteCas: 
          mov dx,offset VypZadajteCas
          mov ah,9
          int 21h
          call NacitajCasovyUdaj      
          call KONTROLANACITANEHOCASU 
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          cmp si,0
          jnz Chyba
          jmp OK
      Chyba:
        mov dx,offset errorVypZleZadanyCas
        mov ah,9
        int 21h
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h
        jmp ZadajteCas

     OK:
      call VypisRetazec
pop di
pop si
pop dx
pop cx
pop bx
pop ax
ret
ENDP ZadanieA

ZadanieB PROC
push ax
push bx
push cx
push dx
push si
push di   
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov bl,Retaz1[5]
          mov PomocnaRetaz[0],bl
          mov bl,Retaz1[6]
          mov PomocnaRetaz[1],bl
          mov PomocnaRetaz[2],0Dh
          mov bx,offset PomocnaRetaz
          mov di,0
          call KonverziaZDesCislaNaDvojCislo
          add CasVMinutach,ax
          
          mov bl,Retaz1[2]
          mov PomocnaRetaz[0],bl
          mov bl,Retaz1[3]
          mov PomocnaRetaz[1],bl
          mov PomocnaRetaz[2],0Dh
          mov bx,offset PomocnaRetaz
          mov di,0
          call KonverziaZDesCislaNaDvojCislo
          mul SestdesiatPriNas
          add CasVMinutach,ax

          mov bl,Retaz1[8]
          mov PomocnaRetaz[0],bl
          mov bl,Retaz1[9]
          mov PomocnaRetaz[1],bl
          mov PomocnaRetaz[2],0Dh
          mov bx,offset PomocnaRetaz
          mov di,0
          call KonverziaZDesCislaNaDvojCislo
          cmp ax,30
          jge ZaokruhliNahor
          jmp NavratSProc
       ZaokruhliNahor:
          add CasVMinutach,1
       NavratSProc:
         mov dx, offset VypMinutach
         mov ah,9
         int 21h
         mov ax,CasVMinutach
         call Write
         pop di
         pop si
         pop dx
         pop cx
         pop bx
         pop ax
         ret
ENDP ZadanieB

ZadanieC PROC
push ax
push bx
push cx
push dx
push si
push di
           mov cx,0;vytvori subor pre zapis
           mov dx,offset MenoSuboru
           mov ah,3Ch
           int 21h
           mov PopisovacSuboru,AX
        
           ;..zapiseminuty do suboru..
           mov ax,CasVMinutach
           call WriteFile
          ;..zatvori subor..
          mov bx,PopisovacSuboru
          mov ah,3Eh
          int 21h
pop di
pop si
pop dx
pop cx
pop bx
pop ax
ret
ENDP ZadanieC
;------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call ZadanieA
          call ZadanieB
          call ZadanieC
           
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































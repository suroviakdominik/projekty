/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.data;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class Nehnutelnost {

    private Integer aSupisneCislo;
    private String aAdresa;
    private List<Osoba> aUbytovany;
    private ListVlastnictva aZapNaLV;

    private final Logger LoggerNehnutelnost = Logger.getLogger(this.getClass().getName());

    public Nehnutelnost(Integer aSupisneCislo, String aAdresa) {
        this.aSupisneCislo = aSupisneCislo;
        this.aAdresa = aAdresa;
        aUbytovany = new LinkedList<>();
        LoggerNehnutelnost.setLevel(Level.OFF);
    }

    public Integer getaSupisneCislo() {
        return aSupisneCislo;
    }

    public String getaAdresa() {
        return aAdresa;
    }

    public void setaAdresa(String aAdresa) {
        this.aAdresa = aAdresa;
    }

    public ListVlastnictva getaZapNaLV() {
        return aZapNaLV;
    }

    /**
     * nasta LV, na ktorom sa nachadzam
     * @param aZapNaLV 
     */
    public void setaZapNaLV(ListVlastnictva aZapNaLV) {
        this.aZapNaLV = aZapNaLV;
    }

    /**
     *
     * @param paOsoba
     * @return false-uz je tu ubytovany, nepridaval som ho znova true- pridal
     * som ho ako noveho ubytovaneho
     */
    public boolean pridajNovehoNaUbytovanie(Osoba paOsoba) {
        if (aUbytovany.contains(paOsoba)) {
            LoggerNehnutelnost.log(Level.INFO, "Nepridal som do nehnutelnosti noveho obyvatela, lebo uz raz obyvatelom je");
            return false;
        }
        LoggerNehnutelnost.log(Level.INFO, "Pridal som do nehnutelnosti noveho obyvatela.");
        aUbytovany.add(paOsoba);
        paOsoba.setaTrvaleBydlisko(this);
        return true;
    }

    /**
     * Zrusi osobe zo zadanym RC trvale bydlisko v tejto nehnutelnosti.
     * Odstrani zo zoznamu ubytovanych v tejto triede a osobe
     * nastavi trvale bydlisko na null
     * @param paRodneCislo
     * @return 
     */
    public Osoba odoberObyvatela(String paRodneCislo) {
        for (Iterator<Osoba> iterator = aUbytovany.iterator(); iterator.hasNext();) {
            Osoba next = iterator.next();
            if (next.getaRodneCislo().equals(paRodneCislo)) {
                iterator.remove();
                next.setaTrvaleBydlisko(null);
                LoggerNehnutelnost.log(Level.INFO, "Osoba bola odobrana z nehnutelnosti. Uz ju neobyva.");
                return next;
            }
        }
        LoggerNehnutelnost.log(Level.INFO, "Osoba neobyva nehbutelnost. Nebola odobrana, lebo tu nebyva.");
        return null;
    }

    /**
     * ak ideme mazat nehnutelnost, tak zrusime vsetkym, ktory
     * ju obyvaju trvaly pobyt
     * @return zozban ubytovanych
     */
    public List<Osoba> zrusUbytovanychAkRusisNehnutelnost() {
        for (Osoba prech : aUbytovany) {
            prech.setaTrvaleBydlisko(null);
        }
        return aUbytovany;
    }

    /**
     * 
     * @return zoznam obyvatelob nehnutelnosti
     */
    public String vypisObyvatelov() {
        String ret="";
        for(Osoba prech:aUbytovany){
            ret+=prech.vypisIbaKontaktneUdaje();
        }
        return ret;
    }

    @Override
    public String toString() {
        String ret = "+++++++++++++++++NEHNUTELNOST+++++++++++++++++\n";
        ret += "Supisne cislo: " + aSupisneCislo + "\n";
        ret += "Adresa: " + aAdresa + "\n";
        if (aZapNaLV != null) {
            ret += "Cislo LV: " + aZapNaLV.getaCisloLV() + "\n";
            ret += "      KU: " + aZapNaLV.getaKU().toString() + "\n";
        }
        ret+="Pocet obyvatelov: "+aUbytovany.size()+"\n";
        ret += "++++++++++++++++++++++++++++++++++++++++++++++++\n\n";
        return ret;
    }

}

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId = 0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
        $sql = "SELECT idKoment FROM komenty";
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row["idKoment"] > $this->biggestId) {
                    $this->biggestId = $row["idKoment"];
                }
            }
        }
    }

    function zmazPouzivatela() {
        $a = htmlspecialchars($_POST["mazPouzMail"]);
        $sqlFindUsID=sprintf("SELECT id FROM pouzivatelia WHERE email='%s'",
                $this->conn->real_escape_string($a));
        
        $result = mysqli_query($this->conn, $sqlFindUsID);
        
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
               $id=$row["id"];
            }
        }
        else
        {
            echo "Uzivatel zo zadanym mailom neexistuje.";
            return;
        }
        
        
        if($a!="admin@zoo.sk")
        {
        $sql1= sprintf("DELETE FROM komenty WHERE idUzivatel='%s'",
                $this->conn->real_escape_string($id));
        if ($this->conn->query($sql1) === TRUE)
        {
             echo "Komenty pouzivatela boli odstranene.";
        }
        else
        {
            echo "Chyba pri mazani komentov uzivatela: " . $this->conn->error;
        }
        $sql = sprintf("DELETE FROM pouzivatelia WHERE email='%s'",
                $this->conn->real_escape_string($a));
        echo "</br>";
        if ($this->conn->query($sql) === TRUE) {
            echo "Pouzivatel bol odstraneny.";
        } else {
            echo "Chyba pri mazani uzivatela(Pravdepodobne zle zadany email.): " . $this->conn->error;
        }
        }
        else
        {
            echo "Administratorske konto nemozete vymazat. Moze ho vymazat alebo pozmenit len spravca databazy.";
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

if(isset($_SESSION["login"])&&$_SESSION["login"]==0)
{
    $db=new DBPraca();
    $db->zmazPouzivatela();
    $db->ukonciPristup();
}
else
{
    echo "Pravo mazat uzivatelov ma iba ADMINISTRATOR po prihlaseni.";
}
?>
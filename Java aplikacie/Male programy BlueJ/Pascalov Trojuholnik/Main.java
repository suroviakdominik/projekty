import java.util.Scanner;
public class Main
{
 public static void main()
 {
   Scanner scan=new Scanner(System.in);
   System.out.println("Zadajte cislo n, po ktore chcete rozvinut Pascalov Trojuholnik: ");
   int n=scan.nextInt();
   NRozmernePoleCisel poleCisel=new NRozmernePoleCisel(n);
   PascalovTrojuholnik PascTroj=new PascalovTrojuholnik(poleCisel);
   PascTroj.nacitajPascalovTrojuholnik();
   System.out.println("Pascalov trojuholnik po cislo "+n+" je: ");
   poleCisel.vypisPole();
   
   
}
}

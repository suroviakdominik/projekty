package wof00.VstupVystup;

import wof00.Hra.Hrac;
import wof00.Vykonavac.IVykonavac;

/**
 * Trieda prikaz implemntuje casti prikazu, ktore moze hrac zadat. V tejto
 * verzii prikaz tvoria dve slova: nazov prikazu a druhe slovo. Napriklad prikaz
 * "chod juh" tvoria dva retazce "chod" ako nazov prikazu a "juh" ako oznacenie
 * smeru.
 *
 * Predpoklada sa, ze prikaz je skontrolovany, t.j., ze nazov prikazu je znamy.
 * Pre neznamy prikaz je jeho nazov nastaveny na hodnotu <null>.
 *
 * Ak prikaz nema parameter, potom ma druhe slovo hodnotu <null>.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 * @author lokalizacia: Lubomir Sadlon, Jan Janech
 * @version 2012.02.21
 */
public class Prikaz {

    private String aNazovPrikazu;
    private String aParameter;
    private IVykonavac aVykonavac;

    /**
     * Inicializuje slova prikazu dvomi zadanymi parametrami. Jeden alebo oba
     * parametre mozu mat hodnotu <null>.
     *
     * @param paNazovPrikazu prve slovo - nazov prikazu, null, ak je prikaz
     * neznamy.
     * @param paParameter druhe slovo prikazu.
     */
    public Prikaz(IVykonavac paVykonavac, String paPrikaz, String paParameter) {
        aVykonavac = paVykonavac;
        aNazovPrikazu = paPrikaz;
        aParameter = paParameter;
    }

    /**
     * @return prve slovo - nazov prikazu.
     */
    public String dajNazov() {
        return aNazovPrikazu;
    }

    /**
     * @return druhe slovo - parameter prikazu.
     */
    public String dajParameter() {
        return aParameter;
    }

    /**
     * @return true, ak je prikaz neznamy.
     */
    public boolean jeNeznamy() {
        return (aVykonavac == null);
    }

    /**
     * @return true, ak prikaz ma parameter.
     */
    public boolean maParameter() {
        return (aParameter != null);
    }

    // implementacie prikazov:
    /**
     * Vypise text pomocnika do terminaloveho okna. Text obsahuje zoznam moznych
     * prikazov.
     */
    private void vypisNapovedu() {
        System.out.println("Zabludil si. Si sam. Tulas sa po fakulte.");
        System.out.println();
        System.out.println("Mozes pouzit tieto prikazy:");
        System.out.println("   chod ukonci pomoc");
    }

    /**
     * Prevezne prikaz a vykona ho.
     *
     * @param paPrikaz prikaz, ktory ma byt vykonany.
     * @return true ak prikaz ukonci hru, inak vrati false.
     */
    public boolean vykonajPrikaz(Hrac paHrac) {
        boolean jeKoniec = false;

        if (jeNeznamy()) {
            if ((paHrac.masZivot() && (this.jeNeznamy()))) {
                System.out.println("Nerozumiem, co mas na mysli...");
            }
            return false;
        } else {
            if ((this.dajNazov().equals("zdvihni")) || (this.dajNazov().equals("poloz"))) {
                paHrac.zvysSkusenost(1);
                System.out.println("Skusenosti hraca sa zvysili o: 1");
            }
            return aVykonavac.vykonaj(paHrac, this.dajParameter());
        }

    }

    /**
     * Ukonci hru. Skotroluje cely prikaz a zisti, ci je naozaj koniec hry.
     * Prikaz ukoncenia nema parameter.
     *
     * @return true, if this command quits the game, false otherwise.
     * @return true, ak prikaz konci hru, inak false.
     */
    private boolean ukonciHru() {
        if (maParameter()) {
            System.out.println("Ukonci, co?");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Vypise privitanie hraca do terminaloveho okna.
     */
    private void vypisPrivitanie(Hrac paHrac) {
        System.out.println();
        System.out.println("Ahoj: " + paHrac.getMeno());
        System.out.println("Vitaj v hre World of FRI!");
        System.out.println("World of FRI je nova, neuveritelne nudna adventura.");
        System.out.println("Zadaj 'pomoc' ak potrebujes pomoc.");
        System.out.println();
        paHrac.dajPoziciu().vypisInfoMiestnosti();
    }

}

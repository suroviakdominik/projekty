/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meteheuristikasimulatedannealing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class DataLoader {

    private double[][] aMatrix;
    private String[] aNamesOfCities;
    private int aCountOfProcessingCities;

    public DataLoader(int countOfCities) {
        aMatrix = new double[countOfCities][countOfCities];
        aNamesOfCities = new String[countOfCities];
        aCountOfProcessingCities = countOfCities;
    }

    public boolean loadMatrixOfDistances(File paFileWithMatrixData) {
        FileWorker fileWorker = new FileWorker(paFileWithMatrixData);
        try {
            fileWorker.startRead();
            String loadedLine;
            while ((loadedLine = fileWorker.readNextLine()) != null) {
                if (fileWorker.getPocNacitanychRiadkov() > aMatrix.length) {
                    return true;
                }
                spracujRiadok(loadedLine, fileWorker.getPocNacitanychRiadkov() - 1);
            }
            return true;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fileWorker.stopRead();
        }
        return false;
    }

    private void spracujRiadok(String paRiadok, int paSpracRiadok) {
        paRiadok = paRiadok.replaceAll(",", ".");
        String[] riadokData = paRiadok.split("\\s");
        for (int j = 0; j < aMatrix[0].length; j++) {
            aMatrix[paSpracRiadok][j] = Double.parseDouble(riadokData[j]);
        }
    }

    public boolean loadNamesOfCities(File paFileWithNamesOFCities) {
        FileWorker fileWorker = new FileWorker(paFileWithNamesOFCities);
        try {
            fileWorker.startRead();
            String loadedLine;
            if ((loadedLine = fileWorker.readNextLine()) != null) {
                //nazvy miest by mali byt v subore v jednom riadku
                String names[] = loadedLine.split("\\s");
                for (int i = 0; i < aCountOfProcessingCities; i++) {
                    aNamesOfCities[i] = names[i];
                }
                if (aNamesOfCities.length == aMatrix.length) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Chyba - subor s nazvami neobsahuje pozadovany pocet nazvov");
                }
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fileWorker.stopRead();
        }
        return false;
    }

    public double getDistance(int i, int j) {
        return aMatrix[i][j];
    }

    public String getNameOfCity(int i) {
        return aNamesOfCities[i];
    }

    public int getCountOfAllCities() {
        return aCountOfProcessingCities;
    }

    public int getMinDistanceCityIndexFromCityIndex(int paCityIndexFrom, List<Integer> paUsedIndex) {
        double min = Double.MAX_VALUE;
        int minIndex = -1;
        for (int j = 0; j < aCountOfProcessingCities; j++) {
            if (paCityIndexFrom != j && !paUsedIndex.contains(j) && aMatrix[paCityIndexFrom][j] < min) {
                minIndex = j;
                min = aMatrix[paCityIndexFrom][j];
            }
        }
        return minIndex;
    }

    public String vypisMatVzd() {
        StringBuilder ret = new StringBuilder(100000);
        for (int i = 0; i < aCountOfProcessingCities; i++) {
            for (int j = 0; j < aCountOfProcessingCities; j++) {
                ret.append(aMatrix[i][j] + "\t");
            }
            ret.append("\n\r");
        }
        return ret.toString();
    }
}

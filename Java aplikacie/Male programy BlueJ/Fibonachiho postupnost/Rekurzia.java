

public class Rekurzia
{
  
  public static int Faktorial(int paKolke,int paN,int paQ)
  {
      
      if(paKolke==0)
      {
          return 1;
      }
      
      else 
      {
          if (paKolke==1)
          {
              return paN;
          }
          
          return Faktorial(--paKolke,paN+paQ,paN);
      } 
  }
}  

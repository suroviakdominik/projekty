/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skokdodialkytripokusy;

/**
 *
 * @author Dominik
 */
public class Sportovec implements Comparable<Sportovec> {

    private String aMeno;
    private Double aPrvySkok;
    private Double aDruhySkok;
    private Double aTretiSkok;

    public Sportovec(String aMeno, Double aPrvySkok, Double aDruhySkok, Double aTretiSkok) {
        this.aMeno = aMeno;
        this.aPrvySkok = aPrvySkok;
        this.aDruhySkok = aDruhySkok;
        this.aTretiSkok = aTretiSkok;
    }

    public String getaMeno() {
        return aMeno;
    }

    public Double getaPrvySkok() {
        return aPrvySkok;
    }

    public Double getaDruhySkok() {
        return aDruhySkok;
    }

    public Double getaTretiSkok() {
        return aTretiSkok;
    }

    @Override
    public int compareTo(Sportovec o) {
        Double maxsSkok = 0d;
        if (aPrvySkok.compareTo(aDruhySkok) > 0) {
            if (aPrvySkok.compareTo(aTretiSkok) > 0) {
                maxsSkok = aPrvySkok;
            } else {
                maxsSkok = aTretiSkok;
            }
        } else {

            if (aDruhySkok.compareTo(aTretiSkok) > 0) {
                maxsSkok = aDruhySkok;
            } else {
                maxsSkok = aTretiSkok;
            }

        }
        
        Double maxsSkok2 = 0d;
        if (o.aPrvySkok.compareTo(o.aDruhySkok) > 0) {
            if (o.aPrvySkok.compareTo(o.aTretiSkok) > 0) {
                maxsSkok2 = o.aPrvySkok;
            } else {
                maxsSkok2 = o.aTretiSkok;
            }
        } else {

            if (o.aDruhySkok.compareTo(o.aTretiSkok) > 0) {
                maxsSkok2 = o.aDruhySkok;
            } else {
                maxsSkok2 = o.aTretiSkok;
            }

        }
        return maxsSkok2.compareTo(maxsSkok);
    }

}

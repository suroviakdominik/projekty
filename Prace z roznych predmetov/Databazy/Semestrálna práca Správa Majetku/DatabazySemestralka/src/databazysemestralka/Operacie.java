/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databazysemestralka;

import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dominik
 */
public enum Operacie {
    
    Operacia1_1("1. zobrazenie aktuálneho majetku jednotlivých oddelení podľa zadaných kritérií ( podľa typu" +
                "majetku, doby nadobudnutia, katedry /fakulty / univerzity, aktuálnej ceny ) spolu s prepočtom" +
                "celkovej ceny majetku, ",
                 "OP1_ZOBRAZ_CELK__CENU_MAJETOK"),
    Operacia1_2("","OP1_ZOBRAZ_AKTUAL_MAJETOK"),
    Operacia2("2. zoznam priradeného majetku konkrétnej osobe v zadanom čase" +
                "podľa zadaných kritérií(pozor, jedna osoba môže pracovať na rôznych oddeleniach včase)–" +
                "vypíšte aj informáciu o stave majetku,",
                "OP2_ZOZNAM_PRIR_MAJETK_OS"),
    Operacia3("3. vypíšte štatistiku spotreby jednotlivých typov spotrebného materiálu jednotlivých oddelení " +
              "podľa zadaných kritéri. ",
              " OP3_STAT_SPOT_MAT"),
    Operacia4("4. vypíšte štatistiku spoľahlivosti tlačiarní arozmnožovacej techniky " +
              "vzhľadom na potrebu ich servisu (tieto hodnoty je potrebné normalizovať vzhľadomna dobu od nákupu po definovaný" +
              "čas)–buď konkrétneho majetku, alebo modelu=typ_tlaciarne ako takého,",
              "OP4_STAT_SPOL_TLAC"),
    Operacia5("5.vypíšte štatistiku tlačiarní vzhľadom na jednotkovú cenu tlače za zadané obdobie podľa zadaných kritérií",
              "OP5_STAT_TLAC_VZHL_TL"),
    Operacia6("6. vypíšte  vývoj  spotreby  materiálu  a nákladov  na  prevádzku  jednotlivých  typov  tlačiarní " +
              "a rozmnožovacej techniky za zadané obdobie a zadanou presnosťou (mesačne, polročne,  ročne)",
              "OP6_NAKL_PREV_TLAC"),
    Operacia7("7. výpis troch najporuchovejších zariadení v !!definovaných kategóriách (desktop, notebook, disk," +
              "...",
              "OP_7_NAJPOR_ZAR"),
    Operacia8("8. výpis troch aktuálne najdrahších serverov v celej organizácií (s rešpektovaním amortizácie," +
              "nákupu náhradných dielov, dodatočných modulov, apod.), ",
              "OP_8_NAJDR_SERV_V_CELEJ_ORG"),
    Operacia9("9.  výpis technických zariadení pre každú organizačnú zložku (napr. katedru, fakultu), ktoré ani po 5tich rokoch" +
              "prevádzky nepotrebovali žiaden servis (napr. náhradný diel, opravu), ",
              "OP_9_PO_5_ROK_NEP_OPR"),
    Operacia10("10. -výpis zariadení, ktoré musia prejsť v nasledujúcom období (obdobie definované parametrom, " +
               "napr. 1 mesiac, 3 mesiace, rok, ...) servisnou kontrolou",
               "OP_10_vyp_zar_na_serv"),
    Operacia11("11.ku každému zariadeniu vypísať celkovú cenu opráv, ktoré boli vykonané počas záručnej opravy.",
               "OP_11_cen_opr_poc_zar_opr"),
    Operacia12("12.vyhľadávanie predražených komponentov vdefinovanom období –porovnanie cien totožných, " +
               "resp. porovnateľných komponentov, ktoré kúpili jednotlivé organizačné zložky v totožnom " +
               "období: napr. FRI kupovala (totožné) disky za polovičnú cenu ako EF vroku 2015,",
               "OP_12_predr_komp"),
    Operacia13("13.Výpis majetku na vyradenie po jednotlivých organizačných zložkách (dôvod vyradenia: vek," +
               "opotrebenie, poškodenie, ...),",
               "OP_13_vyp_maj_na_vyrad"),
    Operacia14("14.sledovanie počtu zariadení (napr. tlačiarní) vdefinovanom oddelení v čase",
               "OP_14_sled_poc_zar"),
    Operacia15("15.výpis majetku spolu jeho aktuálnou cenou pre konkrétnu osobu (spolu s históriou rešpektujúc" +
               "opravy a amortizáciu), spravi: vypise majetok a amortizovana cena + cena rozsireni, pre jednu konkretnu osobu",
               "OP_15_vyp_maj_s_akt_cenou"),
    Operacia16("16.výpis osoby, na ktorú je napísaný najväčší majetok (po jednotlivých organizačných zložkách),",
               "OP_16_OS_na_kt_nap_najv_maj");
    
    private String popis;
    private String procedura;
    
    private Operacie(String popis, String procedura){
        this.popis = popis;
        this.procedura = procedura;
    }
    
    public DefaultTableModel vykonaj(String... params) throws SQLException{
        return Core.vykonaj(procedura, params);
    }

    @Override
    public String toString() {
        return popis+"\n"+ "Volana procedura: "+procedura;
    }
}

package com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.suroviak.dominik.zooAplication.ActivityClass.AnimalFullInfoActivity;
import com.suroviak.dominik.zooAplication.Adapters.ExpandableListAdapterCategories;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CategoriesFragment extends Fragment {

    private String aNameOfRootDownloadDir;

    private Info_About_Dirs_File aInfoFileCategories;
    private Info_About_Dirs_File aInfoAnimalFile;

    ExpandableListAdapterCategories listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FileWorker fw = new FileWorker();
        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.CATEGORIES.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.CATEGORIES.getaNameOfInfoFile());
        try {
            aInfoFileCategories = (Info_About_Dirs_File) sw.readObjetctFromFile();
            sw.setNameOfFile(Info_Dirs_File_ENUM.ANIMALS.getaNameOfInfoFile());
            aInfoAnimalFile = (Info_About_Dirs_File) sw.readObjetctFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        pripravDataPreList();
        listAdapter = new ExpandableListAdapterCategories(this.getActivity(), listDataHeader, listDataChild, aInfoFileCategories, aInfoAnimalFile);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_categories_main_menu, container, false);
        // get the listview


        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Intent i = new Intent(CategoriesFragment.this.getActivity(), AnimalFullInfoActivity.class);
                String a = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                i.putExtra("KEY_OF_SELECTED_ANIMAL", listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                startActivity(i);
                return false;
            }
        });
        // setting list adapter
        expListView.setAdapter(listAdapter);

        return rootView;
    }


    private void pripravDataPreList() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        int i = 0;
        for (String prechCat : aInfoFileCategories.getaItemsInfo().keySet()) {
            if (prechCat.contains("CA")) {
                listDataHeader.add(prechCat);
                List animalsOfCat = new ArrayList<String>();
                for (String prechAnim : aInfoAnimalFile.getaItemsInfo().keySet()) {
                    if (aInfoAnimalFile.getaItemsInfo().get(prechAnim).getParentDirectory().contains(prechCat)) {
                        animalsOfCat.add(prechAnim);
                    }
                }
                listDataChild.put(prechCat, animalsOfCat);

                i++;
            }
        }
    }
}

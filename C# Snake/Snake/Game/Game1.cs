﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Snake.DrawableObjects;
using Snake.DrawableObjects.Menu;
using Snake.Game;
using Snake.Network;
using SnakeServer;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Color = Microsoft.Xna.Framework.Color;
using GamOvMenuOptEventArgs = Snake.DrawableObjects.GamOvMenuOptEventArgs;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Snake
{
    /// <summary>
    /// Zakladna slucka hry, ktora riadi prepinanie stavovo hry a prekreslovanie na zaklade stavu
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public const int SleepTimeMenuItemChoose = 100;///kolko cakat, ked pouzivatel stlaci klavesu - aby sa nebralo pri jednom stlaceni akoby ju stlacil viac krat
        private const int ServerPortNumber = 800;//cislo portu servera
        private const int ServerUpdateIntervalMilis = 10;//server bude citat spravy od klientov kazdych ServerUpdateIntervalMilis milisekund

        GraphicsDeviceManager aGraphics;//platno
        SpriteBatch aSpriteBatch;//spriteBatch ktory zabezpeci male volanie io operacii na graficku kart - najskor sa vsetko nahromadi a az potom vykresluje
        // Represents the aSnake
        // Keyboard states used to determine key presses
        public StavHry GameState { get; set; }///stav v ktorom sa hra aktualne nachadza
        private DrawableObjects.Snake aSnake;///had
        private Food aSnakeFood;///jedlo

        private StartScreen aStartScreen;///start menu obrazovka
        private GameOverScreen aGameOverScreen;///game over obrazovka
        private MultiplayerMenu aMultiplMenu;///multiplayer menu obrazovka
        private WaitingForPlayersMenu aWaitMenu;///cakanie na druheho hraca obrazovka
        private WinScreen aWinScreen;///obrazovka ak hrac vyhral multiplayer zapas
        private ScoreScreen aScoreScreen;///obrazovka zobrazujuca skore

        //private Song aSong;///hudba ktora hra pri hre
        private SoundEffect aEatSoundEffect;///efekt, ked had zje jedlo
        //private SoundEffectInstance aMoveSoundEffect;///hudba pohybu

        private NetworkingClient aClient;///multiplayer klient
        private ServerNetworking aServer;///multiplayer server
        private Dictionary<IPEndPoint, OnlineSnake> aOnlinePlayers;///zoznam online hracov

        /// <summary>
        /// konstruktor
        /// </summary>
        public Game1()
        {
            aGraphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            aOnlinePlayers = new Dictionary<IPEndPoint, OnlineSnake>();
            aGraphics.PreferredBackBufferWidth = 819;  // set this value to the desired width of your window
            aGraphics.PreferredBackBufferHeight = 609;   // set this value to the desired height of your window
            //aGraphics.IsFullScreen = true;
            // this game is split screen, so divide the aspect ratio by 2.

            //this.IsFixedTimeStep = false;
            //this.aGraphics.SynchronizeWithVerticalRetrace = false;
            /*this.IsFixedTimeStep = true;
            this.TargetElapsedTime = new System.TimeSpan(0,0,0,0,33); // 33ms = 30fps*/
        }

        /// <summary>
        /// prida online hraca kedsa pripoji do multiplayer hry
        /// vytvori online hada
        /// a umiestni ho na obrazovku
        /// </summary>
        /// <param name="paOnlinePlayerIp"></param>
        public void addOnlinePlayer(IPEndPoint paOnlinePlayerIp)
        {
            Random r = new Random();
            var c = new Color(
               (byte)r.Next(0, 255) + 50,
               (byte)r.Next(0, 120),
               (byte)r.Next(0, 255));
            OnlineSnake onlSnake = new OnlineSnake(c, GraphicsDevice);
            onlSnake.LoadContent(Content);
            Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);//+(aOnlinePlayers.Count+1)*onlSnake.Height);
            onlSnake.InitializeStartState(playerPosition);
            aOnlinePlayers.Add(paOnlinePlayerIp, onlSnake);
            //Console.WriteLine("PocetONLINE HRACOV: " + aOnlinePlayers.Count);
        }

        /// <summary>
        /// odstrani online hada
        /// </summary>
        /// <param name="paOnlinePlayerIp"></param>
        public void deleteOnlinePlayer(IPEndPoint paOnlinePlayerIp)
        {
            aOnlinePlayers.Remove(paOnlinePlayerIp);
            if (aOnlinePlayers.Count == 0)
            {
                GameState = StavHry.Win;
            }
        }

        /// <summary>
        /// vrati hada, ktory je priradeny danemu online hracovi
        /// </summary>
        /// <param name="paClientIp"></param>
        /// <returns></returns>
        public OnlineSnake getSnake(IPEndPoint paClientIp)
        {
            return aOnlinePlayers[paClientIp];
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.InitializeStartState will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            aStartScreen = new StartScreen(GraphicsDevice);
            aGameOverScreen = new GameOverScreen(GraphicsDevice);
            aMultiplMenu = new MultiplayerMenu(GraphicsDevice);
            aWaitMenu = new WaitingForPlayersMenu(GraphicsDevice);
            aWinScreen = new WinScreen(GraphicsDevice);
            aScoreScreen = new ScoreScreen(GraphicsDevice);

            aStartScreen.OptionSelectedEvent += myStartMenuEventHandler;
            aGameOverScreen.OptionSelectedEvent += gameOverMenuEventHandler;
            aMultiplMenu.OptionSelectedEvent += multMenuEventHandler;
            aWinScreen.OptionSelectedEvent += winScreenEventHandler;
            aWaitMenu.OptionSelectedEvent += waitSreenEventHandler;
            aScoreScreen.OptionSelectedEvent += scoreScreenEventHandler;

            aSnake = new DrawableObjects.Snake(GraphicsDevice);
            aSnakeFood = new Food(aGraphics);
            GameState = StavHry.StartMenu;
            // Set a constant aSnake move speed
            base.Initialize();
        }

        /// <summary>
        /// ak pouzivatel vybral v skore menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scoreScreenEventHandler(object sender, ScoreMenuOptEventArgs e)
        {
            if (e.SelOption == ScoreOptions.StartMenu)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.StartMenu;
            }
            else
            {
                throw new ArgumentException("Not implemented yet");
            }
        }

        /// <summary>
        /// ak pouzivatel vybral vo wait menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void waitSreenEventHandler(object sender, WaitMenuOptEventArgs e)
        {
            if (e.SelOption == WaitingForPlayersOptions.Cancel)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.MultiPlayerMenu;
                if (aServer != null)
                    aServer.stopServer();
                if (aClient != null)
                    aClient.stopClient();
            }
        }

        /// <summary>
        /// ak pouzivatel vybral vo win menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void winScreenEventHandler(object sender, WinMenuOptEventArgs e)
        {
            if (e.SelOption == WinOptions.StartMenu)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.StartMenu;
            }
            else if (e.SelOption == WinOptions.SaveScore)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                tryToSaveScore();
            }
            else
            {
                throw new ArgumentException("Not implemented yet");
            }
        }

        /// <summary>
        /// ak pouzivatel vybral v multiplayer menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void multMenuEventHandler(object sender, DrawableObjects.Menu.MultMenuOptEventArgs multMenuOptEventArgs)
        {
            aOnlinePlayers.Clear();
            if (aClient != null)
                aClient.stopClient();

            if (aServer != null)
                aServer.stopServer();


            if (multMenuOptEventArgs.SelOption == MultiplayerOptions.StartMenu)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.StartMenu;
            }
            else if (multMenuOptEventArgs.SelOption == MultiplayerOptions.CreateServer)
            {
                aServer = new ServerNetworking(ServerPortNumber);
                aServer.startServer();
                aServer.startListening(ServerUpdateIntervalMilis);

                aClient = new NetworkingClient(multMenuOptEventArgs.VPNIp, this);
                aClient.startClient();
                aSnakeFood.AddNewFoodPositionListener(aClient);
                GameState = StavHry.WaitingForAnotherPlayers;
            }
            else if (multMenuOptEventArgs.SelOption == MultiplayerOptions.JoinToServer)
            {
                aClient = new NetworkingClient(multMenuOptEventArgs.VPNIp, this);
                aClient.startClient();
                aSnakeFood.AddNewFoodPositionListener(aClient);
                GameState = StavHry.WaitingForAnotherPlayers;
            }
            else
            {
                throw new ArgumentException("Not implemented yet");
            }
        }

        /// <summary>
        /// ak pouzivatel vybral v game-over menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void gameOverMenuEventHandler(object sender, GamOvMenuOptEventArgs gamOvMenuOptEventArgs)
        {
            if (gamOvMenuOptEventArgs.SelOption == GameOverOptions.Exit)
            {
                Exit();
            }
            else if (gamOvMenuOptEventArgs.SelOption == GameOverOptions.StartMenu)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.StartMenu;
            }
            else if (gamOvMenuOptEventArgs.SelOption == GameOverOptions.SaveScore)
            {
                tryToSaveScore();
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.StartMenu;
            }
            else
            {
                throw new ArgumentException("Not implemented yet");
            }
        }

      
        /// <summary>
        /// ak pouzivatel vybral v start menu nejaku moznost vyvola sa tato metoda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void myStartMenuEventHandler(object sender, StartMenuOptEventArgs e)
        {
            if (e.SelOption == StartMenuOptions.Exit)
                Exit();
            else if (e.SelOption == StartMenuOptions.SinglePlayer)
            {
                GameState = StavHry.PlayingSinglePlayer;
                Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);
                aSnake.InitializeStartState(playerPosition);
            }
            else if (e.SelOption == StartMenuOptions.Multiplayer)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.MultiPlayerMenu;
                Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);
                aSnake.InitializeStartState(playerPosition);
                aMultiplMenu.resetVPNIPToDefault();
            }
            else if (e.SelOption == StartMenuOptions.Score)
            {
                Thread.Sleep(SleepTimeMenuItemChoose);
                GameState = StavHry.ScoreScreen;
            }
            else
            {
                throw new ArgumentException("Not implemented yet");
            }
        }

        /// <summary>
        /// pokusi sa ulozit skore do databazy, ak sa to nepodari dialog ze sa nepodarilo pripojit k db
        /// </summary>
        private void tryToSaveScore()
        {
            if (DatabaseManager.checkForDatabaseConnectivity())
            {
                string name = DataDialog.showDialog("Name: ", "Name for score");
                if (DataDialog.Result == DialogResult.OK)
                {
                    DatabaseManager.addNewSingleplayerScore(name, aSnake.Score);
                }
            }
            else
            {
                MessageBox.Show(@"Database connection failed" + Environment.NewLine + @"Data can not be save to database",
                    @"Database connection error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Load the aSnake resources
            aStartScreen.LoadContent(Content);
            aGameOverScreen.LoadContent(Content);
            aMultiplMenu.LoadContent(Content);
            aWaitMenu.LoadContent(Content);
            aWinScreen.LoadContent(Content);
            aScoreScreen.LoadContent(Content);

            aSnake.LoadContent(Content);
            aSnakeFood.LoadContent(Content);
            aEatSoundEffect = Content.Load<SoundEffect>("eat");
            //aSong = Content.Load<Song>("music");//alebo pouzi music alebo jungle
            //aMoveSoundEffect = Content.Load<SoundEffect>("move").CreateInstance();
            //aMoveSoundEffect.Volume = 0.5f;
            //aMoveSoundEffect.IsLooped = true;
            //aMoveSoundEffect.Play();
            aSnakeFood.GenerateNewFoodPosition();
            //MediaPlayer.Play(aSong);
            //MediaPlayer.IsRepeating = true;
            // Create a new SpriteBatch, which can be used to Draw textures.
            aSpriteBatch = new SpriteBatch(GraphicsDevice);
        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            Content.Unload();
            if (aClient != null)
                aClient.stopClient();
            if (aServer != null)
                aServer.stopServer();
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (true)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                    Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    GameState = StavHry.StartMenu;
                }

                if (GameState == StavHry.StartMenu)
                {
                    aStartScreen.Update(gameTime);
                }
                else if (GameState == StavHry.GameOver)
                {
                    aGameOverScreen.Update(gameTime);
                }
                else if (GameState == StavHry.MultiPlayerMenu)
                {
                    aMultiplMenu.Update(gameTime);
                }
                else if (GameState == StavHry.WaitingForAnotherPlayers)
                {
                    aWaitMenu.Update(gameTime);
                    aClient.readData();
                    if (aOnlinePlayers.Count > 0)
                    {
                        GameState = StavHry.PlayingMultiplayer;
                    }
                }
                else if (GameState == StavHry.Win)
                {
                    aWinScreen.Update(gameTime);
                }
                else if (GameState == StavHry.ScoreScreen)
                {
                    aScoreScreen.Update(gameTime);
                }
                else if (GameState == StavHry.PlayingSinglePlayer || GameState == StavHry.PlayingMultiplayer)
                {
                    if (GameState == StavHry.PlayingMultiplayer)
                    {
                        aClient.readData();
                        foreach (var iter in aClient.aPositionMessages)
                        {
                            if (aOnlinePlayers.ContainsKey(iter.FomClient))
                            {
                                aOnlinePlayers[iter.FomClient].PlayerLastUpdateMoveSize = iter.SizeOfMovement;
                                aOnlinePlayers[iter.FomClient].NewSnakeDirection = iter.Direction;
                                aOnlinePlayers[iter.FomClient].VykonanyJedenPohyb = iter.MoveSnakeAboutPartWidth;
                                aOnlinePlayers[iter.FomClient].Update(gameTime);
                            }
                        }
                    }
                    aSnake.Update(gameTime);

                    if (GameState == StavHry.PlayingMultiplayer && aSnake.ActualFirstPartDirection != Direction.Non)
                    {
                        aClient.sendData((byte)TypDat.ZmenaPozicie, (byte)aSnake.ActualFirstPartDirection, aSnake.VykonanyJedenPohyb, aSnake.PlayerLastUpdateMoveSize);
                    }

                    if (CollisionChecker.foodCollision(aSnakeFood.FoodBox, aSnake.SnakeBox))
                    {
                        Vector2 v = aSnakeFood.GenerateNewFoodPosition();
                        if (GameState == StavHry.PlayingMultiplayer)
                            aClient.sendData((byte)TypDat.VygenerovaneNoveJedlo, v.X, v.Y);
                        aSnake.grow();
                        aEatSoundEffect.Play();
                    }
                    else if (CollisionChecker.snakeCollision(aSnake))
                    {
                        Console.WriteLine(@"Snake collision");
                        aSnake.stopSnakeMovement();
                        if (GameState == StavHry.PlayingMultiplayer)
                        {
                            aClient.sendData((byte)TypDat.PrehraHraca);
                        }
                        GameState = StavHry.GameOver;
                    }
                    base.Update(gameTime);
                }
            }
        }



        /// <summary>
        /// This is called when the game should Draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            // Start drawing

            aSpriteBatch.Begin(sortMode: SpriteSortMode.FrontToBack);
            if (GameState == StavHry.GameOver)
            {
                aGameOverScreen.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.MultiPlayerMenu)
            {
                aMultiplMenu.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.StartMenu)
            {
                aStartScreen.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.ScoreScreen)
            {
                aScoreScreen.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.PlayingMultiplayer || GameState == StavHry.PlayingSinglePlayer)
            {
                // Draw the Player and food
                aSnake.Draw(aSpriteBatch);
                foreach (DrawableObjects.Snake sn in aOnlinePlayers.Values)
                {
                    sn.Draw(aSpriteBatch);
                }
                aSnakeFood.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.WaitingForAnotherPlayers)
            {
                aWaitMenu.Draw(aSpriteBatch);
            }
            else if (GameState == StavHry.Win)
            {
                aWinScreen.Draw(aSpriteBatch);
            }
            // Stop drawing
            aSpriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}

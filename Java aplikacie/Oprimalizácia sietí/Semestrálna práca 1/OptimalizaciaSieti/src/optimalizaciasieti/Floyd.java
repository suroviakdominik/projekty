/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import optimalizaciasieti.data.Vrchol;
import java.util.List;
import java.util.Map;
import optimalizaciasieti.data.Hrana;

/**
 *
 * @author Dominik
 */
public class Floyd {

    private double[][] aMaticaVzdialenosti;
    private List<Vrchol> aVrcholy;
    private Integer[][] aMaticaPredchodcov;
    private Map<Integer, Hrana> aHrany;
    private int aPodmatica;
    private List<Hrana> aHranyZaloha;

    public Floyd(List<Vrchol> paNodes, Map<Integer, Hrana> paHrany,int paVyberPodmaticuN) {
        aPodmatica=paVyberPodmaticuN;
        aMaticaVzdialenosti = new double[paNodes.size()][paNodes.size()];
        Collections.sort(paNodes);
        aVrcholy=paNodes;
        for (int i = 0; i < aVrcholy.size(); i++) {
            System.out.println(aVrcholy.get(i));
        }
        aMaticaPredchodcov = new Integer[paNodes.size()][paNodes.size()];
        aHranyZaloha=new ArrayList<>(paHrany.values());
        aHrany = paHrany;
        for (int i = 0; i < aMaticaVzdialenosti.length; i++) {
            for (int j = 0; j < aMaticaVzdialenosti[i].length; j++) {
                aMaticaVzdialenosti[i][j] = Double.MAX_VALUE;
            }
        }
    }

    public void inicializuj() {
        System.out.println("\nFloyd: Matica vzdialenosti bude velkosti: "+aVrcholy.size()+" x "+aVrcholy.size());
        System.out.println("Priblizna doba vypoctu priemerneho pocitaca: "+(Math.pow(aVrcholy.size(), 3)/260000000)/60+" minut");
        Vrchol skumVrchol;
        int j = -1;
        int pocetVsHran=aHrany.size();
        int pocetUspesnePrejdHran = 0;
        for (int i = 0; i < aVrcholy.size(); i++) {
            skumVrchol = aVrcholy.get(i);
            for (Hrana prech : getHranyObsahujuceVrchol(skumVrchol)) {
                if (skumVrchol.equals(prech.getaZacVrhol())) {
                    j = aVrcholy.indexOf(prech.getaKoncVrchol());
                    aMaticaVzdialenosti[i][j] = prech.getaVzdialenost();
                    aMaticaVzdialenosti[j][i] = prech.getaVzdialenost();
                    aMaticaPredchodcov[i][j]=i;
                    aMaticaPredchodcov[j][i]=j;
                    pocetUspesnePrejdHran++;
                } else if (skumVrchol.equals(prech.getaKoncVrchol())) {
                    j = aVrcholy.indexOf(prech.getaZacVrhol());
                    aMaticaVzdialenosti[j][i] = prech.getaVzdialenost();
                    aMaticaVzdialenosti[i][j] = prech.getaVzdialenost();
                    aMaticaPredchodcov[j][i]=j;
                    aMaticaPredchodcov[i][j]=i;
                    pocetUspesnePrejdHran++;
                } else {
                    System.out.println("Chyba: Inicializuj Floydov algoritmus. Hrana obsahuje vrchol ale equals neplati.");
                }
                aHrany.remove(prech.getaIDHrany());
            }
        }
        for (int i = 0; i < aMaticaVzdialenosti.length; i++) {
            aMaticaVzdialenosti[i][i] = 0;
            aMaticaPredchodcov[i][i] = i;
        }
        System.out.println("Inicializacia Floyd: Uspesne prejdenych hran: " + pocetUspesnePrejdHran + " z " + pocetVsHran);
        matVzdTToFile();
    }

    private ArrayList<Hrana> getHranyObsahujuceVrchol(Vrchol paVrchol) {
        ArrayList<Hrana> ret = new ArrayList<>();
        for (Integer prech : aHrany.keySet()) {
            if (aHrany.get(prech).getaZacVrhol().equals(paVrchol) || aHrany.get(prech).getaKoncVrchol().equals(paVrchol)) {
                ret.add(aHrany.get(prech));
            }
        }
        return ret;
    }

    private void matVzdTToFile() {
        String ret = "";
        System.out.println("Pripravujem maticu vzdialenosti na vypis.....");
        int pocetDUrcDlzkouHrany = 0;
        for (int i = 0; i < aPodmatica; i++) {
            for (int j = 0; j < aPodmatica; j++) {
                if (aMaticaVzdialenosti[i][j] == Double.MAX_VALUE) {
                    ret=ret.concat( "NON \t");
                } else {
                    ret=ret.concat(String.format("%.2f\t", aMaticaVzdialenosti[i][j]));
                    pocetDUrcDlzkouHrany++;
                }
            }
            ret=ret.concat("\n");
        }
        System.out.println("Matica vzdialenosti ma urcenych: "+pocetDUrcDlzkouHrany+" dlzok z: "
                + aVrcholy.size()*aVrcholy.size()+" dlzok");
        FileWorker.writeDataToFile(new File("matVzd.txt"), ret,false);
    }
    
    private void matPredchToFile() {
         System.out.println("Pripravujem maticu predchodcov na vypis.....");
        String ret = "";
        int pocetPlatnychPredchodcov = 0;
        for (int i = 0; i < aPodmatica; i++) {
            for (int j = 0; j < aPodmatica; j++) {
                if (aMaticaPredchodcov[i][j] == null) {
                    ret=ret.concat( "NOWY\t");
                } else {
                    ret=ret.concat(String.format("%d\t", aMaticaPredchodcov[i][j]+1));
                    pocetPlatnychPredchodcov++;
                }
            }
            ret=ret.concat("\n");
        }
        System.out.println("Matica precdhodcov ma urcenych: "+pocetPlatnychPredchodcov+" predchodcob z: "+aVrcholy.size()*aVrcholy.size()+" precdhodcov");
        FileWorker.writeDataToFile(new File("matPredch.txt"), ret,false);
    }
    
    private void matPouzHranToFile(){
        int indexAktual;
        int indexPredch;
        
        for (int i = 0; i < aPodmatica; i++) {
            for (int j = 0; j < aPodmatica; j++) {
                if(i!=j){
                    indexAktual=j;
                    while(indexAktual!=i){
                        indexPredch=aMaticaPredchodcov[i][indexAktual];
                        Vrchol zac=aVrcholy.get(indexAktual);
                        Vrchol konc=aVrcholy.get(indexPredch);
                        if(!aHrany.containsValue(new Hrana(1,zac,konc))){
                            Hrana h=aHranyZaloha.get(aHranyZaloha.indexOf(new Hrana(1,zac,konc)));
                            aHrany.put(h.getaIDHrany(),h);
                        }
                        indexAktual=indexPredch;
                    }
                }
            }
        }
        StringBuilder sB=new StringBuilder(250000);
        for(Hrana prech:aHrany.values()){
            sB.append(prech.toString());
        }
        FileWorker.writeDataToFile(new File("pouzHrany.txt"),sB.toString(),false);
    }
    
    public void spustiVypocet(){
        for (int k = 0; k < aVrcholy.size(); k++) {
            System.out.println("Priebeh: "+k+" z "+aVrcholy.size());
            for (int i = 0; i < aVrcholy.size(); i++) {
                if(i!=k)
                for (int j = 0; j < aVrcholy.size(); j++) {
                    if(j!=k)
                    if(aMaticaVzdialenosti[i][j]>aMaticaVzdialenosti[i][k]+aMaticaVzdialenosti[k][j])
                    {
                        aMaticaVzdialenosti[i][j]=aMaticaVzdialenosti[i][k]+aMaticaVzdialenosti[k][j];
                        aMaticaPredchodcov[i][j]=aMaticaPredchodcov[k][j];
                    }
                }
            }
        }
        matVzdTToFile();
        matPredchToFile();
        matPouzHranToFile();
    }
}

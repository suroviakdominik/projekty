import java.util.*;

/**
 * Trida simuluje automat na jizdenky
 * 
 * @author Jirka Krhanek
 * @version 2006-01-02
 */
public class Automat {
  
  public static ArrayList jizdenky = new ArrayList();   // verejna staticka kolekce objednanych jizdenek
  public static ArrayList mince = new ArrayList();   // verejna staticka kolekce vhozenych minci
  private static Jizdenka novaJizdenka;   // staticka promenna typu Jizdenka
  private static String vypis;   // staticky retezec

/**
 * Metoda objedna nPocet jizdenek o nHodnote
 * 
 * @param nHodnota hodnota objednanych jizdenek
 * @param nPocet pocet jizdenek
 * @return pokud se objednavaka podari vraci true, jinak false
 */
  public static boolean priobjednatJizdenky(int nHodnota, int nPocet) 
  {
    
    if (nHodnota > 0 && nPocet > 0)   // podminka: hodnota i pocet jizdenek musi byt vetsi nez 0
    {
      novaJizdenka = new Jizdenka(nHodnota);   // vytvorime novou jizdenku o nHodnote
      jizdenky.add(novaJizdenka);   // pote jizdenku priradime do kolekce objednanych jizdenek
      return true;   // podminka probehla uspesne a proto muzeme metode priradit booleanovskou navratovou hodnotu true
    }
    return false;  // pri nesplneni podminky vratime false
  }
    
/**
 * Metoda vraci cenu objednavky
 * 
 * @return cena objedn�vky
 */
  public static int cenaObjednavky()   // metoda scita hodnoty jizdenek 
  {
    int nCena = 0;   // nastaveni vychozi ceny objednavky na 0
    for (int i = 0; i < jizdenky.size(); i++)   // pomoci cyklu projdeme kolekci jizdenek
    {
      nCena += ((Jizdenka)jizdenky.get(i)).vratHodnotu();   // pricitame jednodlive jizdenky z kolekce
    }
    return nCena;   // vratime celkovou cenu jizdenek
  }
    
/**
 * Metoda vraci hodnotu vhozenych penez
 * 
 * @return vraci hodnotu vhozenych penez
 */
  public static int vhozenoPenez()   // metoda scita hodnoty minci
  {
    int nVhozeno = 0;   // nastaveni vychozi ceny minci na 0
    for (int i = 0; i < mince.size(); i++)   // pomoci cyklu projdeme kolekci minci
    {
      nVhozeno += ((Mince)mince.get(i)).vratHodnotu();   // pricitame jednodlive mince z kolekce
    }
    return nVhozeno;   // vratime celkovou cenu minci
  }
    
/**
 * Metoda simuluje vhozeni mince do automatu.
 * Pokud je vhozeno vice nebo rovno penez nez je cena
 * objednavky automat vytiskne jizdenky a pripadne
 * vraci preplatek
 * 
 * @param hodnota hodnota vhozenych penez
 * @return pokud se vhozeni podari vraci true, jinak false
 */
  public static boolean vhoditMinci(Mince coin) 
  {
      mince.add(coin);  // do kolekce ulozime vhozenou minci
      int nHodnota = coin.vratHodnotu();   // zjisti hodnotu vhozene mince
         // a pokud je vhozeno dost nebo vic penez tak automaticky zacit tisknout jizdenky a vratit zbytek
      if (vhozenoPenez() >= cenaObjednavky())   // pokud hodnota minci prevysi nebo vyrovna hodnotu objednavky muzem pokracovat
      {
        // lokalni promenna typu String
        String vytisknuteJizdenky = "";
        for (int i = 0; i < jizdenky.size(); i++)   // cyklus pro vypsani vsech objednanych jizdenek
        {                
          vytisknuteJizdenky += ((Jizdenka)jizdenky.get(i)).vratHodnotu() + ",";   // predava do promenne retezec
        }
        
        int nZbytek = vhozenoPenez() - cenaObjednavky();   // tento rozdil na spocita kolik mame vratit penez
           // zbytek penez musime rozdelit podle skutecnych hodnot minci, tedy 1, 2, 5, 10, 20
           // teoreticky muzeme vynechat hodnotu 20 korun, protoze tato situace nastane jen v pripade, ze uzivatel hodi do automatu o 20 korun vic, nez musi
           // coz mu ale nedovoluje metoda vhoditMinci(), jelikoz se provede ihned jak ma dostatek penez pro zapalceni objednavky
           // zacneme tedy az od 10 korun a jelikoz datovy typ int je colocisleny, tak kdyz zbytek vydelime 10, dostaneme pocet desetikorun co nam muze vratit.
           // napr.: 13 / 10 = 1
       int n20 = nZbytek / 20;
           // zjistime zbytek po celociselnem deleleni pomoci operatoru modulo %
           // napr.: 13 % 10 = 3
       nZbytek %= 20;   // citelnejsi: nZbytek = nZbytek % 10; a nebo: nZbytek = nZbytek - (n10 * 10);
           // to same provedeme s ostatnimi mincemi      
        int n10 = nZbytek / 10;            
        nZbytek %= 10;   
        int n5 = nZbytek / 5; 
        nZbytek %= 5;   
        int n2 = nZbytek / 2; 
        nZbytek %= 2;
        int n1 = nZbytek / 1; 
        nZbytek %= 1;
        
        // lokalni promenna typu String
        String vraceneMince = "";
        // pomoci cyklku for predame do retezce pocet jednotlivych minci
        for (int i = 0; i < n20; i++)   // n20 vypiseme do retezce cislo 20, tedy hodnotu mince
          vraceneMince += "20,";
        // to same s ostatnimi mincemi
        for (int i = 0; i < n10; i++)   
          vraceneMince += "10,";
        for (int i = 0; i < n5; i++) 
          vraceneMince += "5,";
        for (int i = 0; i < n2; i++) 
          vraceneMince += "2,";
        for (int i = 0; i < n1; i++) 
          vraceneMince += "1,";
      
      // zavolame metodu, ktera slouci obe promenne typu String
      sloucitInformace(vytisknuteJizdenky, vraceneMince);
      mince.clear();   // kdyz jsme vratili zbyle penize, muzeme kolekci mince vycistit    
                       // pokud tak neucinime a zavolame metoru stornovatObjednavku, vrati nam penize kter jsme vhodili do automatu, ikdyz uz mame jizdenky koupene      
      return true;   // pokud probehla zadana podminka if v poradku, vratime true
    }
       // pokud ne vratime false
    return false;
  }

/**
 * Metoda stornuje objednavku.
 * Pokud jsou vhozeny nejake mince, tak jsou vraceny
 */
  public static void stornovatObjednavku()   // stornuje objednavku a tedy vrati mince
  {
    jizdenky.clear();   // vymaze kolekci s objedannyma jizdenkama
    if (mince.size() != 0)   // jestlize jsou vhozeny mince pak nastane vypis
    {
      System.out.println("Automat vr�til tyto mince:");
    }
    while (mince.size() != 0)   // cyklus se provadi tak dlouho dokud jsou vhozene mince jeste v automatu
    {
      System.out.println(((Mince)mince.get(0)).vratHodnotu());   // vypise hodnotu mince
      mince.remove(0);   // odstrani minci
    }
  }
  
/**
 * Metoda slucuje informace o vytisknutych jizdenkach a vracenych mincich
 * 
 * @param vytisknuteJizdenky retezec vytisknutych jizdenek
 * @param vraceneMince retezec vracenych minci
 */
  public static void sloucitInformace(String vytisknuteJizdenky, String vraceneMince)
  {
      // zretezeni retezcu a predani do instancni promenne vypis
      vypis = "\n  Automat vytiskl tyto j�zdenky:\n  " + vytisknuteJizdenky + "\n\n  Automat vr�til tyto mince:\n  " + vraceneMince;    
  }

/**
 * Metoda vraci informace o vytisknutych jizdenkach a vracenych minci
 * 
 * @return vraci vypis o jizdenkach a mincich
 */
  public static String vratInformace()
  {
    // navratova hodnota
    return vypis;  
  }
}

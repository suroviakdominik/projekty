/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyGUIJComponent;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTextField;

/**
 * JText field, ktoremu je mozne zadat HINT (prednastaveny text)
 * @author Dominik
 */
public class JTextFieldWithHint extends JTextField implements FocusListener, MouseListener {

    private String aTextHint = null;
    private boolean aIsChoosen = false;
    private boolean aIsTextNeeded = false;
    public boolean aCheckIfIsTextDouble=false;

    public JTextFieldWithHint(String paTextHint) {
        super();
        super.addFocusListener(this);
        super.addMouseListener(this);
        aTextHint = paTextHint;
        setText(aTextHint);
    }

    public String getTextHint() {
        return aTextHint;
    }

    
    
    @Override
    public void focusGained(FocusEvent fe) {
        if (!aIsTextNeeded) {
            this.setText("");
            aIsChoosen = true;
        }

    }

    @Override
    public void focusLost(FocusEvent fe) {
        if(aCheckIfIsTextDouble&&!JTextFieldWithHint.this.getText().equals(""))
        {
            if(kontolaNaDoubleVstup())
            {
                aIsTextNeeded=true;
            }
            else
            {
                aIsTextNeeded=false;
            }
        }
        
        if (!aIsTextNeeded) {
            this.setText(aTextHint);
            aIsChoosen = false;
        }
        
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        if(!aIsTextNeeded)
        this.setText("");
    }

    @Override
    public void mouseExited(MouseEvent me) {
         if(aCheckIfIsTextDouble&&!JTextFieldWithHint.this.getText().equals(""))
        {
            if(kontolaNaDoubleVstup())
            {
                aIsTextNeeded=true;
            }
            else
            {
                aIsTextNeeded=false;
            }
        }
        
        if (!this.aIsChoosen&&!aIsTextNeeded) {
            this.setText(aTextHint);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {

    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    /**
     * 
     * @param aIsTextNeeded -true -ak je text potrebny
     *                      fale - ak nie je text potrebny, nahradi sa hintom
     */
    public void setIsTextNeeded(boolean aIsTextNeeded) {
        this.aIsTextNeeded = aIsTextNeeded;
    }

    /**
     * 
     * @param paCheckIfIsTextDouble true- udaje sa budu kontrolovat na typ double 
     */
    public void setCheckIfIsTextDouble(boolean paCheckIfIsTextDouble) {
        this.aCheckIfIsTextDouble = paCheckIfIsTextDouble;
    }

    
    
    /**
     * Skontroluje ci je zadany udaj cislo
     * @return true- udaj v text fielde je cislo 
     */
    public boolean kontolaNaDoubleVstup()
    {
        if(!getText().equals("")&&!getText().equals(aTextHint))
        {
            try
            {
                Double.parseDouble(getText());
            }
            catch(NumberFormatException ex)
            {
                return false;
            }
        }
        return true;
    }
}

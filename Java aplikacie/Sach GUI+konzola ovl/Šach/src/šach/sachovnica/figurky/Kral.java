/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import Grafika.Obdlznik;
import Grafika.Trojuholnik;

/**
 * Trieda Kral je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Kral extends Figurka {

    private IGrafickeObjekty[] aVykreslenieKrala;

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba nového kráľa
     */
    public Kral(String paFarba) {
        super("Kral", paFarba);
        aVykreslenieKrala = new IGrafickeObjekty[5];
    }

    /**
     * je metóda, ktorá vykreslí kráľa na plátno.
     */
    public void nakresli() {
        aVykreslenieKrala[0] = new Obdlznik(40, 20, this.getX() - 5, this.getY(), this.getFarba());
        aVykreslenieKrala[1] = new Obdlznik(30, 10, this.getX(), this.getY() - 10, this.getFarba());
        aVykreslenieKrala[2] = new Trojuholnik(10, 10, this.getX() + 5, this.getY() - 20, this.getFarba());
        aVykreslenieKrala[3] = new Trojuholnik(10, 10, this.getX() + 15, this.getY() - 20, this.getFarba());
        aVykreslenieKrala[4] = new Trojuholnik(10, 10, this.getX() + 25, this.getY() - 20, this.getFarba());

        for (int i = 0; i < 5; i++) {
            aVykreslenieKrala[i].zobraz();
        }

    }

    /**
     * je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie kráľa.
     *
     * @return grafické objekty z ktorých sa skladá kráľ
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aVykreslenieKrala;
    }

    /**
     * metóda vracajúca popis kráľa.
     *
     * @return popis kráľa vo forme String
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Kráľ" + "\n";
        popis += "Pohyb: všetkými smermi avšak iba o jedno pole" + "\n";
        popis += "Vyhadzovanie: všetkými smermi o jedno pole" + "\n";
        popis += "Ine: najdôležitejšia figúrka hry, ak bol vyhodený, hra končí" + "\n";
        return popis;
    }
}

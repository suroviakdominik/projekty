package com.suroviak.dominik.zooAplication.FragmentsInfoAboutZoo;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.suroviak.dominik.zooAplication.Connectivity;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;

/**
 * Created by Dominik on 5.3.2015.
 */

public class Base_Info_Fragment extends Fragment {
    private FileWorker aFileWorker;
    private String aNameOfRootDownloadDir;
    private Info_About_Dirs_File aInfoFileInfo_About_ZOO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_info, container, false);


        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.INFO.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.INFO.getaNameOfInfoFile());
        try {
            aInfoFileInfo_About_ZOO = (Info_About_Dirs_File) sw.readObjetctFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        aFileWorker = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER +
                aInfoFileInfo_About_ZOO.getaItemsInfo().get("I0").getRelativePathOfFilesOfurrentItemInDevice() + "popis.txt");
        String nacitaneInfo = "";
        try {
            nacitaneInfo = aFileWorker.readDataFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String parsInfo[] = nacitaneInfo.split("\n");
        TextView tw = (TextView) rootView.findViewById(R.id.name_zoo);
        tw.setText(parsInfo[0]);
        tw = (TextView) rootView.findViewById(R.id.countAnim);
        tw.setText(parsInfo[1]);
        tw = (TextView) rootView.findViewById(R.id.countPav);
        tw.setText(parsInfo[2]);
        tw = (TextView) rootView.findViewById(R.id.email);
        tw.setText(parsInfo[4]);
        final TextView twMObile = (TextView) rootView.findViewById(R.id.phobe_numb);
        twMObile.setText(parsInfo[3]);
        twMObile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textOnClick(twMObile);
            }
        });
        final TextView twWebPage = (TextView) rootView.findViewById(R.id.web);
        twWebPage.setText(parsInfo[5]);
        twWebPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textOnClick(twWebPage);
            }
        });
        ImageButton v = (ImageButton) rootView.findViewById(R.id.callToZooButton);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                akcia(v, twMObile.getText().toString());
            }
        });

        ImageButton iBWP = (ImageButton) rootView.findViewById(R.id.webPageOfZooButton);
        iBWP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                akcia(v, twWebPage.getText().toString());
            }
        });
        return rootView;
    }

    public void textOnClick(View v) {
        TextView vieewText = (TextView) v;
        if (vieewText.getId() == R.id.web) {
            otvorStranku(vieewText.getText().toString());
        } else {
            volaj(vieewText.getText().toString());
        }
    }

    public void akcia(View v, String param) {
        switch (v.getId()) {
            case R.id.callToZooButton:
                volaj(param);
                break;

            case R.id.webPageOfZooButton:
                otvorStranku(param);
                break;


        }
    }

    private void otvorStranku(String param) {
        if (Connectivity.isConnected(this.getActivity())) {
            String url = param;
            Intent i = new Intent(Intent.ACTION_VIEW);
            if (!url.startsWith("https://") && !url.startsWith("http://")) {
                url = "http://" + url;
            }
            i.setData(Uri.parse(url));
            startActivity(i);

        } else {
            Toast.makeText(this.getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void volaj(String param) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + param));
        startActivity(callIntent);
    }


}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentiaichznamky;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Dominik
 */
public class ParserDat {

    private String[] aDataPreParsovanie;
    private String aNeplatneRiadky;
    private List<RiadokTabulky> aPlatneRiadky;

    public ParserDat(String[] aDataPreParsovanie) {
        this.aDataPreParsovanie = aDataPreParsovanie;
        aPlatneRiadky = new ArrayList<>();

    }

    public void parsuj() {
        aPlatneRiadky.clear();
        aNeplatneRiadky = "";
        for (int i = 0; i < aDataPreParsovanie.length; i++) {
            if (aDataPreParsovanie[i].matches("^\\s*\\S+ +("
                    + RiadokTabulky.Znamka.A.getaZnamka() + "|"
                    + RiadokTabulky.Znamka.B.getaZnamka() + "|"
                    + RiadokTabulky.Znamka.C.getaZnamka() + "|"
                    + RiadokTabulky.Znamka.D.getaZnamka() + "|"
                    + RiadokTabulky.Znamka.E.getaZnamka() + "|"
                    + RiadokTabulky.Znamka.FX.getaZnamka() + "|"
                    + ")")) {
                int koniecIndexMeno = najdiKoniecIndexMeno(aDataPreParsovanie[i]);
                RiadokTabulky r = new RiadokTabulky(
                        aDataPreParsovanie[i].substring(0, koniecIndexMeno).trim(),
                        dajZnamku(aDataPreParsovanie[i].substring(koniecIndexMeno, aDataPreParsovanie[i].length()).trim()));
                aPlatneRiadky.add(r);
            } else {
                aNeplatneRiadky += aDataPreParsovanie[i] + "\n";
            }
        }
    }

    private RiadokTabulky.Znamka dajZnamku(String paText) {
        switch (paText) {
            case "A":
                return RiadokTabulky.Znamka.A;
            case "B":
                return RiadokTabulky.Znamka.B;
            case "C":
                return RiadokTabulky.Znamka.C;
            case "D":
                return RiadokTabulky.Znamka.D;
            case "E":
                return RiadokTabulky.Znamka.E;
            default:
                return RiadokTabulky.Znamka.FX;

        }
    }

    private int najdiKoniecIndexMeno(String data) {
        int i = 0;
        Pattern p = Pattern.compile("^\\s*\\S+");
        Matcher m = p.matcher(data);
        m.find();
        return m.end();
    }

    public List<RiadokTabulky> getDataTabulky() {
        return aPlatneRiadky;
    }

    public String getNeplatneRiadky() {
        return aNeplatneRiadky;
    }
}

<pre><p>; Vystupna konverzia cisla typu single real (32 bitov).

.MODEL small
.Stack 100h
.Data
MaxCislic EQU 100
CisloBCD DB MaxCislic DUP (0)
IOBuffer DB 5 dup(0)
.Code
.386
PUBLIC Real32
Int16 PROC
; vypis celeho cisla typu word
; cislo je v ax
; modifikuje ax, bx, cx, dx, di
	mov bx, offset IOBuffer
	mov di,0
	mov cx,10
Cyklus:	mov dx,0
	div cx
	mov [bx+di], dl
	inc di
	cmp ax,0
	jnz Cyklus
Vypis:	mov dl,[bx+di-1]
	or dl, 30h
	mov ah,2
	int 21h
	dec di
	cmp di,0
	jnz Vypis
	ret
Int16 ENDP

Mantisa	PROC
; prevedie des. cast Cisla do BCD kodu v premennej CisloBCD, kde najvyssia cislica je pred des. ciarkou
; v Cislo zostane cela cast
; vstupny parameter: si je pocet miest mantisy
; vystupny parameter: si je pocet BCD cislic
; modifikuje ax, bx, cx, dx, di
	mov dl,2
	mov bx,1; kolko cislic BCD uz mam
DalsiBitMantisy:
	mov di, offset CisloBCD

	; skopiruj dalsiu cislicu mantisy do najvyssej BCD cislice
	shr Cislo,1
	adc byte ptr [di],0

	; vydel BCD cislo dvoma
	mov cx,bx
	mov ah,0
Delenie:	mov al,[di]
	aad; des. uprava ax
	div dl
	mov [di],al
	inc di
	loop Delenie
	mov [di],ah; odloz posledny zvysok
	inc bx
	dec si
	jnz DalsiBitMantisy
	dec bx
	mov si, bx
	ret
Mantisa	ENDP

Real32 PROC pascal Cislo:dword
	mov cx,word ptr [Cislo+2]
	; vypis minus, ak ide o zaporne cislo
	test cx,8000h
	jz Kladne
	mov dl,'-'
	mov ah, 2
	int 21h
Kladne:	; vyber exponent
	and cx,0111111110000000b
	shr cx,7
	sub cx,127; cx je exponent
	; vynuluj znamienko a exponent
	mov ebx,Cislo
	and ebx,007FFFFFh
	; pridaj najvyssi bit mantisy
	or ebx,00800000h;
	mov Cislo, ebx
	mov si,23; pocet bitov mantisy
	; posun des. ciarku o exponent
	sub si,cx
	call Mantisa; prevedie des. cast Cislo do BCD kodu, v Cislo zostane cela cast, v si pocet BCD cislic
	mov ax,word ptr [Cislo]
	call int16; vypise celu cast
	mov dl,'.'
	mov ah,2
	int 21h
; vypis mantisy
	mov di, offset CisloBCD
	inc di
	dec si
	mov cx,si
VypisM:	mov dl,[di]
	or dl,30h
	mov ah,2
	int 21h
	inc di
	loop VypisM
	ret
Real32 ENDP
END


	






</p></pre>
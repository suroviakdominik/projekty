/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import java.util.HashMap;
import šach.hra.Hrac;

/**
 *
 * @author Dominik
 */
public class KontrolorTahov {

    private HashMap<String, IKontrolaTahu> aKontroloryTahu;
    private Hrac aHrac;

    /**
     * Pre každú figúrku je definovaná iná trieda, ktorá kontroluje jej ťah. Pri
     * vytváraní týchto tried je však potrebné poznať nové umietnenie figurky,
     * ktoré vstupuje ako parameter pri vytváraní objektu. Tieto triedy však
     * majú spoločný Interface. Konštruktor triedy KontrolorTahov má preto za
     * úlohu vytvoriť inštancie týchto tried a uložiť ich do kontajneru s
     * ktorého je ich možné dostať pomocou jednotlivých kľúčov.
     *
     * @param paHrac - hráč, ktorého ťah sa kontroluje
     * @param paNoveXUMiestnenie -posun v smere x
     * @param paNoveYUmiestnenie -posun v smere y
     */
    public KontrolorTahov(Hrac paHrac, int paNoveXUMiestnenie, int paNoveYUmiestnenie) {
        aHrac = paHrac;
        aKontroloryTahu = new HashMap<>();
        aKontroloryTahu.put("Dáma", new KontrolaTahuDamou(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));
        aKontroloryTahu.put("Kôň", new KontrolaTahuKonom(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));
        aKontroloryTahu.put("Kral", new KontrolaTahuKralom(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));
        aKontroloryTahu.put("Kralovna", new KontrolaTahuKralovnou(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));
        aKontroloryTahu.put("Pešiak", new KontrolaTahuPesiakom(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));
        aKontroloryTahu.put("Veza", new KontrolaTahuVezou(paNoveXUMiestnenie, paNoveYUmiestnenie, aHrac));

    }

    /**
     * rozhodne, ktorý kontrolor ťahov sa má použiť a následne skontroluje ťah.
     *
     * @param paNazovVybranejFigurky - figúrka, s ktorou sa vykonáva ťah
     * @return true,ak je ťah platný, inak false
     */
    public boolean skontrolujTah(String paNazovVybranejFigurky, int paNoveXUMiestnenie, int paNoveYUmiestnenie) {
        return aKontroloryTahu.get(paNazovVybranejFigurky).skontrolujTah();
    }
}

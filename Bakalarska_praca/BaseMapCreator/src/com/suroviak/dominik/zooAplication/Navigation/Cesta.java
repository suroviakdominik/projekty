/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class Cesta implements Serializable{
    private List<BodNaCeste> aBodyNaCeste;
    private static final long serialVersionUID = 3L;

    public Cesta() {
        aBodyNaCeste=new ArrayList<>();
    }
    
    public void pridajNovyBodNaCestu(BodNaCeste paNovyBod)
    {
        if(!aBodyNaCeste.contains(paNovyBod))
        aBodyNaCeste.add(paNovyBod);
        else
        {
            System.out.println("DEBUG: Nepridal som bod na cestu lebo uz existuje.");
        }
    }
    
    public void odstranBodZCesty(BodNaCeste paBodNaOdstranenie)
    {
        aBodyNaCeste.remove(paBodNaOdstranenie);
    }
   
    public int getPocetBodovNaCeste()
    {
        return aBodyNaCeste.size();
    }
    
    public BodNaCeste getBodNaCeste(int paIndexBodu)
    {
        return aBodyNaCeste.get(paIndexBodu);
    }
    
    
    
    public BodNaCeste getBodNaCeste(Coordinate lat,Coordinate lon)
    {
        for(BodNaCeste prech:aBodyNaCeste)
        {
            if(prech.getaLatitude().equals(lat)&&prech.getaLongitude().equals(lon))
            {
                return prech;
            }
        }
        System.out.println("ERROR: Cesta: getBodNaCeste(Coord,Coord)- vracia null hodnotu");
        return null;
    }
}

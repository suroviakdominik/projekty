

public class Hra
{
private MyslimSiCislo aLahka;
private MyslimSiCislo aStredna;
private MyslimSiCislo aTazka;
private MyslimSiCislo aNightMare;  


public  Hra()
   {
       aLahka = new MyslimSiCislo(10,0,7);
       aStredna = new MyslimSiCislo(100,0,10);
       aTazka = new MyslimSiCislo(1000,0,10);
       aNightMare = new MyslimSiCislo(10000,0,10);
   }
  
   
    public void volba1()
    {
        aLahka.getHighScore();
        aLahka.hraj();
    }

    public void volba2()
    {
        aStredna.getHighScore();
        aStredna.hraj();
    }

    public void volba3()
    {
        aTazka.getHighScore();
        aTazka.hraj();
    }

    public void volba4()
    {
        aNightMare.getHighScore();
        aNightMare.hraj();
    }

}
    
   
﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Snake.DrawableObjects;
using Snake.Network;

namespace Snake
{
    public class Food : IDrawableObject
    {
        private Texture2D aFooTexture2D;///obrazok predstavujuci jedlo
        private Vector2 aActualFoodPosition;/// pozicia kde bude jedlo vykreslene
        private GraphicsDeviceManager aGraphicsManager;///manager, ktory vie informacie o platne na ktore kreslime

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphics"></param>
        public Food(GraphicsDeviceManager graphics)
        {
            aActualFoodPosition = new Vector2(0, 0);
            aGraphicsManager = graphics;
        }

        /// <summary>
        /// sirka obrazka jedla v px
        /// </summary>
        private int FoodWidth
        {
            get { return aFooTexture2D.Width; }
        }


        /// <summary>
        /// vyska obrazka jedla v px
        /// </summary>
        private int FoodHeight
        {
            get { return aFooTexture2D.Height; }
        }

        /// <summary>
        /// oblast ohranicenia jedla
        /// </summary>
        public Rectangle FoodBox
        {
            get
            {
                return new Rectangle((int)aActualFoodPosition.X, (int)aActualFoodPosition.Y,
                    FoodWidth - (int)(FoodWidth / 5), FoodHeight - (int)(FoodHeight / 5));
            }
        }

        /// <summary>
        /// naciatanie medii -obrazkov, hudby ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aFooTexture2D = content.Load<Texture2D>("food");
        }

        /// <summary>
        /// update jedla
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {

        }

        /// <summary>
        /// vygeneruje suradnice, kde bude umiestnene nove jedlo
        /// </summary>
        /// <returns>suradnice umiestnenia noveho jedla</returns>
        public Vector2 GenerateNewFoodPosition()
        {
            Random gen = new Random();
            int newX = gen.Next((int)(aGraphicsManager.GraphicsDevice.Viewport.Width / FoodWidth));
            int newY = gen.Next((int)(aGraphicsManager.GraphicsDevice.Viewport.Height / FoodHeight));
            aActualFoodPosition = new Vector2(newX * FoodWidth, newY * FoodHeight);
            return aActualFoodPosition;
        }

        /// <summary>
        /// vykreslenie jedla na obrazovku
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(aFooTexture2D, aActualFoodPosition, null, Color.White, 0f, Vector2.Zero, 1f,
                SpriteEffects.None, 1f);
        }

        /// <summary>
        /// umiestni jedlo na suradnicu zadanu parametrami
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void umiestniJedloNaPoziciu(float x, float y)
        {
            aActualFoodPosition = new Vector2(x, y);
        }

        /// <summary>
        /// pokial hrame multiplayer a jedlo sa umiestnilo protihrac zjedol jedlo vyvola sa tato udalost po prijati dat zo servera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void zmenaJedlaEventHandler(object sender, EventArgs e)
        {
            NetworkingClient.NewFoodGeneratedEventArgs even = (NetworkingClient.NewFoodGeneratedEventArgs)e;
            umiestniJedloNaPoziciu(even.Vector.X, even.Vector.Y);
        }

        /// <summary>
        /// prida posluchac zmeny pozicie noveho jedla pre multiplayer hru
        /// </summary>
        /// <param name="client"></param>
        public void AddNewFoodPositionListener(NetworkingClient client)
        {
            client.EPridajJedlo += new EventHandler<NetworkingClient.NewFoodGeneratedEventArgs>(zmenaJedlaEventHandler);
        }
    }
}
create or replace TRIGGER BACKUPDELETEFAKTURA 
AFTER DELETE ON FAKTURA 
REFERENCING OLD AS STARY NEW AS NOVY 
FOR EACh ROW
BEGIN
  Insert into BACKUPFAKTURA
  values (:stary.cislo_fa,:stary.osobne_cislo,:stary.id_zakaznika,
  :stary.datum_vystavenia,:stary.datum_splatnosti,:stary.cena_za_fakturu,:stary.stav_faktury,user);
END;
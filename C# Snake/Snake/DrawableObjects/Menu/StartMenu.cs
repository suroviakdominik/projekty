﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Snake.DrawableObjects;
using Snake.DrawableObjects.Menu;

namespace Snake
{
    /// <summary>
    /// enum polozky menu v start menu
    /// </summary>
    public enum StartMenuOptions
    {
        SinglePlayer,
        Multiplayer,
        Score,
        Exit
    }

    /// <summary>
    /// ak pouzivatel zvoli najeku polozku z menu, vyvola sa udalost, ktora dostane ako parameter objekt tejto triedy
    /// </summary>
    public class StartMenuOptEventArgs : EventArgs
    {
        private StartMenuOptions aSelOption;
        public StartMenuOptEventArgs(StartMenuOptions paSelectedOption)
        {
            aSelOption = paSelectedOption;
        }

        public StartMenuOptions SelOption
        {
            get { return aSelOption; }
        }
    }
    /// <summary>
    /// Trieda, ktora zobrazuje hlavne menu hry 
    /// </summary>
    class StartScreen : IDrawableObject
    {

        private const int VerticalGap = 10;///vertikalna medzera menu poloziek
        private GraphicsDevice aGraphicsDevice;///obrazovka, ktora sluzi ako platno
        private Texture2D aOptionFrame;///ramik pre polozku menu
        private SpriteFont aFont;///pismo pre polozku menu
        private int aCountOfOptions;///pocet enum moznosti menu
        private List<Vector2> aPositionsOfOptions;///zoznam suradnic, kde vykreslovat polozky menu
        private List<string> aOptionNames;//mena jednotlivych poloziek menu
                                          /// <summary>
                                          /// aktualne zvolena moznost v start menu - este nepotvrdena
                                          /// </summary>
        public StartMenuOptions SelectedOption { get; set; }
        private float aTimeFromLastUpdate;///cas od posledneho updatu zvolenej polozky menu

        public event EventHandler<StartMenuOptEventArgs> OptionSelectedEvent;/// udalost ktora sa vyvola ked pouzivatel vyberie jednu z menu

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public StartScreen(GraphicsDevice graphics)
        {
            aGraphicsDevice = graphics;
            aPositionsOfOptions = new List<Vector2>();
            aOptionNames = new List<string>();
            aOptionNames.Add("Singleplayer");
            aOptionNames.Add("Multiplayer");
            aOptionNames.Add("Score");
            aOptionNames.Add("Exit");

            SelectedOption = StartMenuOptions.SinglePlayer;
            aCountOfOptions = Enum.GetValues(typeof(StartMenuOptions)).Length;
        }

        /// <summary>
        /// sirka ramika menu polozky
        /// </summary>
        public float OptFrameWidth
        {
            get { return aOptionFrame.Width; }
        }

        /// <summary>
        /// vyska ramika menu polozky
        /// </summary>
        public float OptFrameHeight
        {
            get { return aOptionFrame.Height; }
        }


        protected virtual void OnMyEvent(StartMenuOptEventArgs e)
        {
            if (OptionSelectedEvent != null)
            {
                OptionSelectedEvent(this, e);
            }
        }

        /// <summary>
        /// nacita polozky, ktore sa budu vykreslovat, prehravat ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aOptionFrame = content.Load<Texture2D>("start");
            aFont = content.Load<SpriteFont>("SMFont");
            int pom = aCountOfOptions / 2;
            Vector2 firstOptPos = new Vector2((float)aGraphicsDevice.Viewport.Width / 2 - OptFrameWidth / 2,
                                             aGraphicsDevice.Viewport.Height / 2 - pom * VerticalGap - pom * OptFrameHeight);

            for (int i = 0; i < aCountOfOptions; i++)
            {
                aPositionsOfOptions.Add(new Vector2(firstOptPos.X, firstOptPos.Y + i * VerticalGap + i * OptFrameHeight));
            }
        }

        /// <summary>
        /// Update vybranej moznosti- ak pouzivatel stlacil sipku nadol alebo nahor
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
            {
                OnMyEvent(new StartMenuOptEventArgs(SelectedOption));
            }
            aTimeFromLastUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (aTimeFromLastUpdate > 0.11)
            {

                if (Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                {
                    if (SelectedOption != 0)
                    {
                        SelectedOption--;
                    }
                    else
                    {
                        SelectedOption = (StartMenuOptions)Enum.GetValues(typeof(StartMenuOptions)).GetValue(aCountOfOptions - 1);
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Down) ||
                         GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                {
                    if (SelectedOption != (StartMenuOptions)Enum.GetValues(typeof(StartMenuOptions)).GetValue(aCountOfOptions - 1))
                    {
                        SelectedOption++;
                    }
                    else
                    {
                        SelectedOption = 0;
                    }
                }
                aTimeFromLastUpdate = 0;
            }
        }

        /// <summary>
        /// Vykreslenie poloziek menu
        /// </summary>
        /// <param name="drawer"></param>
        public void Draw(SpriteBatch drawer)
        {

            for (int i = 0; i < aPositionsOfOptions.Count; i++)
            {
                Color optCol = Color.White;
                if ((StartMenuOptions)Enum.GetValues(typeof(StartMenuOptions)).GetValue(i) == SelectedOption)
                {
                    optCol = Color.Red;
                }
                Vector2 stringSize = aFont.MeasureString(aOptionNames[i]);
                drawer.Draw(aOptionFrame, aPositionsOfOptions[i], null, optCol, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                Vector2 stringPos = new Vector2(aPositionsOfOptions[i].X + OptFrameWidth / 2 - stringSize.X / 2,
                    aPositionsOfOptions[i].Y + OptFrameHeight / 2 - stringSize.Y / 2);
                drawer.DrawString(aFont, aOptionNames[i], stringPos, Color.Black);
            }
        }

    }
}

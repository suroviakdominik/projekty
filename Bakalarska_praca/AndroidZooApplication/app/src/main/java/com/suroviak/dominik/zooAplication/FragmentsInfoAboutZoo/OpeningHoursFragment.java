package com.suroviak.dominik.zooAplication.FragmentsInfoAboutZoo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;

/**
 * Created by Dominik on 5.3.2015.
 */
public class OpeningHoursFragment extends Fragment {
    private String aNameOfRootDownloadDir;
    private Info_About_Dirs_File aInfoFileInfo_About_ZOO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_opening_hours_info, container, false);
        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.INFO.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.INFO.getaNameOfInfoFile());
        try {
            aInfoFileInfo_About_ZOO = (Info_About_Dirs_File) sw.readObjetctFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        FileWorker fw = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER +
                aInfoFileInfo_About_ZOO.getaItemsInfo().get("I1").getRelativePathOfFilesOfurrentItemInDevice() + "popis.txt");
        String nacitaneInfo = "";
        try {
            nacitaneInfo = fw.readDataFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String parsInfo[] = nacitaneInfo.split(";;;");
        nastavOtvaracieHodiny(rootView, parsInfo);
        return rootView;
    }

    private void nastavOtvaracieHodiny(View rootView, String parsInfo[]) {
        TextView tw = (TextView) rootView.findViewById(R.id.ponOd);
        tw.setText(parsInfo[0].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.ponDo);
        tw.setText(parsInfo[0].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.UtOd);
        tw.setText(parsInfo[1].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.UtDo);
        tw.setText(parsInfo[1].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.strOd);
        tw.setText(parsInfo[2].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.strDo);
        tw.setText(parsInfo[2].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.stvrOd);
        tw.setText(parsInfo[3].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.stvrDo);
        tw.setText(parsInfo[3].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.piaOd);
        tw.setText(parsInfo[4].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.piaDo);
        tw.setText(parsInfo[4].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.soOd);
        tw.setText(parsInfo[5].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.soDo);
        tw.setText(parsInfo[5].split("\\|")[1]);

        tw = (TextView) rootView.findViewById(R.id.neOd);
        tw.setText(parsInfo[6].split("\\|")[0]);
        tw = (TextView) rootView.findViewById(R.id.neDo);
        tw.setText(parsInfo[6].split("\\|")[1]);


    }
}


import java.util.ArrayList;
/**
 * Skuskovy priklad
 * Banka ma k adrese vztah agregacie
 * Banka je kompoziciou k bankomatu
 * 
 */
public class Banka
{
   private int aKod;
   private Bankomat[] aBankomaty;
   private Adresa aAdresa;
   private int aPocetBankomatov;
   private ArrayList<Ucet> aUcty;
   private ArrayList<Klient> aKlienti;
   private int aPocetVytvorenychUctov;
  
   public Banka(int paKod,
                int paMaxPocetBankomatov,
                Adresa paAdresa)
   {
       aKod=paKod;
       aAdresa=paAdresa;
       aBankomaty=new Bankomat[paMaxPocetBankomatov];
       aBankomaty[0]=new Bankomat(aAdresa,10,10,10,10,10,10,10);//ked zanikne banka, zaniknu aj jej bankomaty(kompozicia)
       aPocetBankomatov=1;
       aKlienti= new ArrayList <Klient>();//ked zanikne banka, zaniknu aj jej klenti(kompozicia)
       aUcty= new ArrayList <Ucet>();//ked zanikne banka,zaniknu aj jej ucty(kompozicia)
       aPocetVytvorenychUctov=0;
   }

        public ArrayList<Klient> dajKlientov()
        {
            return aKlienti;
        }
    
        public ArrayList<Ucet> dajUcty()
        {
            return aUcty;
        }
    
       public int getPocetBankomatov()
       {
            return aPocetBankomatov;
       }
    
       public int getKod()
       {
            return aKod;
       }    
   
   public Bankomat zriadBankomat(Adresa paAdresa)
   {
       if (aPocetBankomatov< aBankomaty.length)
       {
           aBankomaty[aPocetBankomatov]= new Bankomat (paAdresa,10,10,10,10,10,10,10);
           aPocetBankomatov++;
           return aBankomaty[aPocetBankomatov-1];
       }
       
       else 
       return null;
        
   } 
   
   public Bankomat bankomatMaxSuma()
   {
   int najBankomat=0;
   int najSuma=aBankomaty[0].sumaVBankomate();
       for (int i=0; i<aPocetBankomatov;i++)
      {
        if (aBankomaty[i].sumaVBankomate()> najSuma)
        {
            najSuma=aBankomaty[i].sumaVBankomate();
            najBankomat=i;
        }   
    }
   return aBankomaty[najBankomat]; 
  }
  
  public Bankomat bankomatMinSuma()
   {
    int minBankomat=0;
    int minSuma=aBankomaty[0].sumaVBankomate();
       for (int i=0; i<aPocetBankomatov;i++)
      {
        if (aBankomaty[i].sumaVBankomate()<minSuma)
        {
            minSuma=aBankomaty[i].sumaVBankomate();
            minBankomat=i;
        }   
      }
    return aBankomaty[minBankomat]; 
  }
  
  public Klient zriadUcet(Osoba paOsoba)
  {
   Ucet novyU=new Ucet(0,++aPocetVytvorenychUctov,this);//kompozicia riadi zivotny cyklus
   aUcty.add(novyU);
   Klient k=existujeKlient(paOsoba);
   if (k==null) 
   {
      k=new Klient(paOsoba,this);
      aKlienti.add(k);
   }
   k.pridajUcet(novyU);
   return k;
  }  
  
  private Klient existujeKlient(Osoba paOsoba)
  {
      for (int i=0;i<aKlienti.size();i++)
      {
          Klient k=aKlienti.get(i);
          if(k.dajOsobu()==paOsoba)
          //if(aKlienti.get(i).dajOsobu()==paOsoba)//anonymnyobjekt,aklient.get(i) volame priamo.
          {
              return k;
          } 
      }    
      return null;
  }  
  
 
}
               

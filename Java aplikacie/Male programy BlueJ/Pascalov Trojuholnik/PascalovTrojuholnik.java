

public class PascalovTrojuholnik
{
  private NRozmernePoleCisel aNRozmernePole; 
   public PascalovTrojuholnik(NRozmernePoleCisel paNRozmernePoleCisel)
   {
      aNRozmernePole=paNRozmernePoleCisel;
   }
   
   public void nacitajPascalovTrojuholnik()
   {
       int pocetPrvkov=aNRozmernePole.dajPocetPrvkov();
       int premennaNul1=0;
       int premennaNul2=0;
       if ((aNRozmernePole.dajPocetPrvkov())%2==0)
      {
          premennaNul1=(pocetPrvkov+aNRozmernePole.dajPripocitane())/2;
          premennaNul2=(pocetPrvkov+aNRozmernePole.dajPripocitane())/2;
      }
      
      else
      {
          premennaNul1=(pocetPrvkov+aNRozmernePole.dajPripocitane()-1)/2;
          premennaNul2=(pocetPrvkov+aNRozmernePole.dajPripocitane()-1)/2;
      }            
       
         for (int i=0;i<pocetPrvkov;i++)
         {
             if (i>0)
             {
               premennaNul1--;
               premennaNul2++;
             }
             
             for (int j=0;j<pocetPrvkov+aNRozmernePole.dajPripocitane();j++)
              {
                 if((j==premennaNul1)||(j==premennaNul2))
                 {
                        aNRozmernePole.vlozHodnotuDoNRozmernehoPola(i,j,1) ;
                 }
                     
                 else
                 {
                         aNRozmernePole.vlozHodnotuDoNRozmernehoPola(i,j,0);
                 }
                 
                }
            }
            
        for (int i=1;i<pocetPrvkov-1;i++)   
         {
            int zabezpecenieDruhejJednotky=1;
             for (int j=0;j<pocetPrvkov+aNRozmernePole.dajPripocitane();j++)
            {
              if (i==1)
              {
                 if (aNRozmernePole.zistiHodnotuIndexu(i,j)!=0)
                 {
                     int lp=aNRozmernePole.zistiHodnotuIndexu(i,j)+aNRozmernePole.zistiHodnotuIndexu(i,j+2);
                     aNRozmernePole.vlozHodnotuDoNRozmernehoPola(i+1,j+1,lp);
                 }
              }  
              else if ((aNRozmernePole.zistiHodnotuIndexu(i,j)!=0)&&(aNRozmernePole.zistiHodnotuIndexu(i,j)!=zabezpecenieDruhejJednotky)) 
              {
                int lp=aNRozmernePole.zistiHodnotuIndexu(i,j)+aNRozmernePole.zistiHodnotuIndexu(i,j-2);
                aNRozmernePole.vlozHodnotuDoNRozmernehoPola(i+1,j-1,lp);
                zabezpecenieDruhejJednotky=zabezpecenieDruhejJednotky-1;
              }
                 
            }
         }
   }
}
    
               

     
       
    

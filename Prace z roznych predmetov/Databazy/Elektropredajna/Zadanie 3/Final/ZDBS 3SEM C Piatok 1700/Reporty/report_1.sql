set pagesize 60;
set linesize 123;

column predajna.nazov_predajne heading "Predajna";
column tovar.nazov heading "Nazov Tovaru";
column tovar.cena heading "Cena";
column tovar.mnozstvo heading "Mnozstvo";
column datum new_value datum noprint




ttitle left datum center 'Prehlad aktualnych  skladovych zasob: ' right;


SELECT to_char (sysdate, 'DD.MM.RRRR') datum, predajna.nazov_predajne Predajna , nazov, cena,mnozstvo from tovar join Predajna using (id_predajne) order by nazov_predajne, nazov, cena;

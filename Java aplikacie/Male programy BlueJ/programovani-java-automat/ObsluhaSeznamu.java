import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Trida obsluhuje seznam prepinacu ze tridy Gui 
 * a implementuje rozhrani ActionListener
 * 
 * @author Jirka Krhanek
 * @version 2006-05-10
 */
class ObsluhaSeznamu implements ActionListener
{   
    // deklarace instancni promenne
    private Gui gui;

/**
 * Konstruktor pro ObsluhaSeznamu
 * 
 * @param gui inicializuje instancni promenou gui
 */
    public ObsluhaSeznamu(Gui gui)
    {
        // prirazeni parametru gui instancni promenne gui teto tridy
        this.gui = gui;
    }

/**
 * Metoda provede akci, kdyz se vyskytne udalost
 * 
 * @param e udalost
 */
    public void actionPerformed(ActionEvent e)
    {
        // volani metody z objektu gui
        gui.vypoctiCenuJizdenky();
    }
}
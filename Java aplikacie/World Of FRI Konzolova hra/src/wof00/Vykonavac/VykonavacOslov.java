/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacOslov implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        if (paHrac.dajPoziciu().dajOsobu(paPrikaz) == null) {
            System.out.println("Taka osoba sa v tejto miestnosti nenachadza.");
        } else {
            paHrac.setaRozhovor(paHrac.dajPoziciu().dajOsobu(paPrikaz));
            paHrac.dajPoziciu().dajOsobu(paPrikaz).oslov();

        }
        return false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentiaichznamky;

import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class RiadokTabulky implements Comparable<RiadokTabulky> {

    private String aStudent;
    private Znamka aZnamka;

    @Override
    public int compareTo(RiadokTabulky o) {
        return o.aZnamka.compareTo(this.aZnamka);
    }

    public RiadokTabulky(String aStudent, Znamka aZnamka) {
        this.aStudent = aStudent;
        this.aZnamka = aZnamka;
    }

    

    public String getaStudent() {
        return aStudent;
    }

    public Znamka getaZnamka() {
        return aZnamka;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.aStudent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RiadokTabulky other = (RiadokTabulky) obj;
        if (!Objects.equals(this.aStudent, other.aStudent)) {
            return false;
        }
        return true;
    }
    
    

    public enum NazvyStlpcov {

        STUDENT("Študent") {

                    @Override
                    public Object dajHodnotu(RiadokTabulky get) {
                        return get.aStudent;
                    }

                },
        ZNAMKA("Známka") {

                    @Override
                    public Object dajHodnotu(RiadokTabulky get) {
                        return get.aZnamka;
                    }

                };

        public abstract Object dajHodnotu(RiadokTabulky get);
        private String aNazovStlpca;

        private NazvyStlpcov(String paNazovStlpca) {
            aNazovStlpca = paNazovStlpca;
        }

        public String getaNazovStlpca() {
            return aNazovStlpca;
        }

    }
    
    public enum Znamka
    {
        A("A",1),
        B("B",1.5),
        C("C",2),
        D("D",2.5),
        E("E",3),
        FX("FX",4);
        
        private String aZnamka;
        private double aCiselnaHodnota;

        private Znamka(String aZnamka, double aCiselnaHodnota) {
            this.aZnamka = aZnamka;
            this.aCiselnaHodnota = aCiselnaHodnota;
        }

        public String getaZnamka() {
            return aZnamka;
        }

        public double getaCiselnaHodnota() {
            return aCiselnaHodnota;
        }
        
        
    }

}

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    

    function zmazZviera() {
        $a = htmlspecialchars($_POST["idMazZv"]);
        
        $sql = sprintf("DELETE FROM zviera WHERE id=%d",
                $a);

        if ($this->conn->query($sql) === TRUE) {
            echo "Zviera bolo odstranene.";
        } else {
            echo "Chyba pri mazani zvierata: " . $this->conn->error;
        }
        
        
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

if (isset($_SESSION["login"]) && $_SESSION["login"] == 0) {
$db=new DBPraca();
$db->zmazZviera();
$db->ukonciPristup();
}
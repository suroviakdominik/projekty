/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.hra.InicializaciaHry;

import Grafika.Platno;
import java.io.FileNotFoundException;

import šach.hra.Hra;
import šach.hra.Hrac;
import šach.spusteniehry.Menu;
import šach.Ine.PracaSoSuborom;
import šach.sachovnica.Sachovnica;
import šach.sachovnica.figurky.Figurka;

/**
 * má za úlohu vytvoriť už samotnú hru, vykresliť plátno a jednotlivé figúrky, a
 * poskytnúť používateľovi možnosť ovládania týchto príkazov. Implemetnuje
 * interface IInicializator.
 *
 * @author Dominik
 */
public class InicializatorUlozenejHry implements IInicializator {

    private String aNazovSuboru;
    private Figurka[][] aSuborovaMapaFigurok;
    private Sachovnica aSachovnica;
    private Menu aMenu;

    /**
     * vytvorí objekt typu Sachovnica a dvojrozmerné pole figúrok do ktorého
     * budú umiestnené jednotlivé figúrky. Okrem toho inicializuje atribút
     * aMenu, ktoré dostane ako parameter.
     *
     * @param paMenu -menu hry, ktoré už je vytvorené
     */
    public InicializatorUlozenejHry(Menu paMenu) {
        aSachovnica = new Sachovnica();
        aSuborovaMapaFigurok = new Figurka[8][8];
        aMenu = paMenu;
    }

    /**
     * vytvorí objekty triedy Hráč , nasledovne vytvorí objekt triedy Hra a
     * vytvorí plátno na ktoré budú vykresľované figúrky. Všetky údaje načíta zo
     * súboru, kde bola predtým uložená hra. Ak nemožno danú hru inicializovať,
     * t. j. v tomto súbore nie je zatiaľ uložená žiadna hra, vráti false.
     *
     * @return -true ak hru inicializuje, false ak neinicializuje.
     */
    @Override
    public boolean inicializuj() {
        PracaSoSuborom lpPracaSoSuborom;
        try {
            lpPracaSoSuborom = new PracaSoSuborom(aNazovSuboru, aSuborovaMapaFigurok);
        } catch (FileNotFoundException ex) {
            return false;
        }
        try {
            lpPracaSoSuborom.nacitajTextovyDokument();
        } catch (Exception ex) {
            System.out.println("Súbor je poškodený alebo v ňom nie je zatiaľ uložená žiadna hra.");
            return false;
        }

        Hrac lpHracA = new Hrac(lpPracaSoSuborom.dajMenoHracaA(), lpPracaSoSuborom.dajFarbuFigurokHracaA(), aSuborovaMapaFigurok);
        System.out.println("Hráč " + lpHracA.getMeno() + " má farbu " + lpHracA.getFarbaFigurok());
        Hrac lpHracB = new Hrac(lpPracaSoSuborom.dajMenoHracaB(), lpPracaSoSuborom.dajFarbuFigurokHracaB(), aSuborovaMapaFigurok);
        lpHracA.setaProtiHrac(lpHracB);
        lpHracB.setaProtiHrac(lpHracA);
        System.out.println("Hráč " + lpHracB.getMeno() + " má farbu " + lpHracB.getFarbaFigurok());

        lpHracA.setaJeNaTahu(!lpPracaSoSuborom.dajJeNaTahuHracA());
        lpHracB.setaJeNaTahu(lpPracaSoSuborom.dajJeNaTahuHracA());
        Platno.dajPlatno();
        Hra nacitanaHra = new Hra(lpHracA, lpHracB, aMenu, lpPracaSoSuborom.getaHodiny(), lpPracaSoSuborom.getaMinuty(), lpPracaSoSuborom.getaSekundy());
        vykresliGrafickeObjekty();
        nacitanaHra.hraj();
        return true;
    }

    /**
     * priradí atribútu tejto triedy súbor s ktorého sa bude uložená hra
     * načítávať.
     *
     * @param paSubor - súbor s ktorého sa bude hra načitávať
     */
    public void vlozSuborSUlozenouPoziciou(String paSubor) {
        aNazovSuboru = paSubor;
    }

    /**
     * vykreslí grafické objekty na plátno.
     */
    private void vykresliGrafickeObjekty() {
        aSachovnica.vykreslenieSachovnice();
        aSachovnica.vykresliFigurky(aSuborovaMapaFigurok);
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Hra.Hrac;
import wof00.VstupVystup.Prikaz;
import wof00.Vykonavac.VykonavacPoloz;
import wof00.Vykonavac.VykonavacZdvihni;

/**
 *
 * @author Dominik
 */
public class PredmetCinka implements IPredmet, Serializable {

    @Override
    public String getNazov() {
        return "cinka";
    }

    @Override
    public String getPopis() {
        return "Cinka, ktora pri zdvihuti a polozeni zvysuje vase skusenosti o 5.";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        Prikaz prikazPoloz = new Prikaz(new VykonavacPoloz(), "poloz", "cinka");
        Prikaz prikazZdvihni = new Prikaz(new VykonavacZdvihni(), "zdvihni", "cinka");
        String ukon = "";
        if (paHrac.maPredmet("cinka")) {
            prikazPoloz.vykonajPrikaz(paHrac);
            prikazZdvihni.vykonajPrikaz(paHrac);
            paHrac.zvysSkusenost(5);

        } else if (paHrac.dajPoziciu().zistiPredmet("cinka") != null) {
            prikazZdvihni.vykonajPrikaz(paHrac);
            prikazPoloz.vykonajPrikaz(paHrac);
            paHrac.zvysSkusenost(5);

        }
        return "Cvicite s cinkou. Skúsenosti hráča sa zvýšili o: " + 5;
    }

}

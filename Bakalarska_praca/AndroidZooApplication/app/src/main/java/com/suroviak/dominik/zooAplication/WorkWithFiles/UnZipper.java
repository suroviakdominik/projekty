package com.suroviak.dominik.zooAplication.WorkWithFiles;

/**
 * Created by Dominik on 11.3.2015.
 */

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Dominik Suroviak
 */
public class UnZipper {
    private String aZipLocationPath;
    private String aDecompresLocationPath;
    private Activity aCallFrom = null;
    private ArrayList<String> aExtractedDirectories;


    public UnZipper(String zipLocationPath, String decompressLocationPath) {
        this.aZipLocationPath = zipLocationPath;
        this.aDecompresLocationPath = decompressLocationPath;
        aExtractedDirectories = new ArrayList<String>();
    }

    public UnZipper(Activity callerActivity, String zipLocationPath, String decompressLocationPath) {
        this.aZipLocationPath = zipLocationPath;
        this.aDecompresLocationPath = decompressLocationPath;
        aCallFrom = callerActivity;
        aExtractedDirectories = new ArrayList<String>();
    }

    public String getZipLocationPath() {
        return aZipLocationPath;
    }

    public String getDecompresLocationPath() {
        return aDecompresLocationPath;
    }

    public ArrayList<String> getExtractedDirectories() {
        return aExtractedDirectories;
    }

    public void setaZipLocationPath(String aZipLocationPath) {
        this.aZipLocationPath = aZipLocationPath;
    }

    public void setaDecompresLocationPath(String paDecompresLocationPath) {
        this.aDecompresLocationPath = paDecompresLocationPath + "/";
    }

    public String getFileNameWithoutZipEnd() {
        int index = aZipLocationPath.indexOf(".zip");
        String ret = "";
        for (int i = 0; i < index; i++) {
            ret += aZipLocationPath.charAt(i);
        }
        return ret;
    }

    public void unzip() {
        if (aCallFrom != null) {
            aCallFrom.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(aCallFrom, "Extracting Files. Please, wait a minute", Toast.LENGTH_LONG).show();
                }
            });

        }
        ZipInputStream lpZIS = null;
        File f = null;
        byte buffer[] = new byte[1024];
        try {
            lpZIS = new ZipInputStream(new FileInputStream(aZipLocationPath));
            ZipEntry entry = null;

            while ((entry = lpZIS.getNextEntry()) != null) {
                if (entry.isDirectory()) {

                    f = new File(aDecompresLocationPath + entry.getName());
                    f.mkdirs();
                    aExtractedDirectories.add(entry.getName());
                } else {
                    extractFile(lpZIS, aDecompresLocationPath + entry.getName());
                }


                lpZIS.closeEntry();
            }


            lpZIS.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ZOOAppUnZipper", "Exception in Unzipper class(unzip())." + e.getMessage());
        } finally {
            try {
                if (lpZIS != null)
                    lpZIS.close();

            } catch (IOException e) {
                e.printStackTrace();
                Log.e("ZOOAppUnZipper", "Exception in Unzipper class(unzip())." + e.getMessage());
            }
        }

        if (aCallFrom != null) {
            aCallFrom.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(aCallFrom, "Files was extracted.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Author of below method
     *
     * @author www.codejava.net
     */
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[1024];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie;

import dynamickehasovanie.struktura.DynamickeHesovanie;
import dynamickehasovanie.data.Osoba;
import dynamickehasovanie.struktura.Blok;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random r = new Random();
        /*for (int i = 0; i < 500; i++) {
         int pocetZaznamov = r.nextInt(5) + 1;
         int hlbka = r.nextInt(31) + 5;
         System.out.println("Pocet zaznamov v bloku: " + pocetZaznamov);
         System.out.println("Maximalna hlbka trie stromu: " + hlbka);
         DynamickeHesovanie<Osoba> dH = new DynamickeHesovanie<>(pocetZaznamov, Osoba.getImplmentatorTohtoZaznamu(), hlbka);

         Osoba os;
         int pocVloz = 0;
         for (int j = 0; j < 100000; j++) {
         os = new Osoba(r.nextInt(30000));
         if (dH.vloz(os)) {
         pocVloz++;
         }
         }
         //System.out.println(dH.sekvencnyVypisSuboru());
         System.out.println("Generujem: " + i + " z " + 500);
         System.out.println("Uspesne som vlozil: " + pocVloz);
         int nachadzaSa = dH.pocetNachadzajucichSaVBlokkoch();
         System.out.println("V DH ich je: " + nachadzaSa);
         System.out.println("Rozdiel: " + (nachadzaSa - pocVloz) + "\n\n");
         if (pocVloz - nachadzaSa != 0) {
         throw new IllegalStateException("For King: Test nepresiel");
         }
         dH.uvolniSubory();
         }
         System.out.println("JUPIIIIIIIIIIIII");
         }*/

        int pocetExperimentov = r.nextInt(2000) + 100;
        for (int i = 0; i < pocetExperimentov; i++) {
            int pocetZaznamov = r.nextInt(5) + 5;
            int hlbka = r.nextInt(31) + 5;
            System.out.println("Experiment: " + i + " z " + (pocetExperimentov - 1));
            System.out.println("Pocet zaznamov v bloku: " + pocetZaznamov);
            System.out.println("Maximalna hlbka trie stromu: " + hlbka);
            List<Integer> osoby = new ArrayList<>();
            DynamickeHesovanie<Osoba> dH = new DynamickeHesovanie<>(pocetZaznamov, Osoba.getImplmentatorTohtoZaznamu(), hlbka, new File("OSOBY"));
            int rozsahRodCisla = r.nextInt(20000) + 10000;
            long time = System.currentTimeMillis();
            int pocVyg = 0;
            for (int j = 0; j < 30000; j++) {
                /*int rod_cislo ;
                 do{
                 rod_cislo = r.nextInt(rozsahRodCisla);
                 }while(dH.hladaj(new Osoba(rod_cislo))!=null);*/
                int rod_cislo = r.nextInt(rozsahRodCisla);

                Osoba os = new Osoba(rod_cislo);
                if (dH.vloz(os, false) == DynamickeHesovanie.STAV_VKLADANIA.VLOZENY) {
                    osoby.add(rod_cislo);
                }
                pocVyg++;
                if (pocVyg % 10000 == 0) {
                    System.out.println(pocVyg + " z 30000");
                }
            }
            System.out.println("VKLADANIE OK");
            System.out.println("Cas na naplnenie: " + (double) (System.currentTimeMillis() - time) / 1000);
            System.out.println("Podarilo sa vlozit: " + osoby.size());
            if (dH.pocetNachadzajucichSaVBlokkoch() != osoby.size() || osoby.size() != dH.getaAktualnyPocetPlatnychZaznamovVStrukture()) {
                throw new IllegalStateException();
            }

            //TEST HLADANIA
            for (int j = 0; j < osoby.size(); j++) {
                Osoba os = new Osoba(osoby.get(j));
                if (dH.hladaj(os) == null) {
                    throw new IllegalStateException("Vlozeny prvok sa nenasiel");
                }
            }
            System.out.println("HLADANIE OK");
            /*dH.ulozTrieDoSuboru();
             System.out.println("Trie strom:\n" + dH.getaTrieStrom().vypisTrieStromuLevelOrder());
             System.out.println(TrieStrom.vytvorTrieStromZoSuboru(new File("trie.txt")).vypisTrieStromuLevelOrder());*/
            int posledneMazanyVrchol = -1;
            while (osoby.size() > 0) {
                int indexOdstrRod_Cislo = r.nextInt(osoby.size());
                //System.out.println("ODSTRANUJEM: "+osoby.get(indexOdstrRod_Cislo));
                Osoba os = new Osoba(osoby.get(indexOdstrRod_Cislo));
                posledneMazanyVrchol = osoby.get(indexOdstrRod_Cislo);
                if (dH.odstran(os) == null) {
                    //System.out.println(dH.getaTrieStrom().vypisTrieStromuLevelOrder());
                    //System.out.println(dH.sekvencnyVypisSuboru());
                    int a = 1;
                    dH.odstran(os);
                    throw new IllegalStateException();
                }
                //System.out.println(dH.sekvencnyVypisSuboru());
                //System.out.println(dH.getaTrieStrom().vypisTrieStromuLevelOrder());
                osoby.remove(indexOdstrRod_Cislo);
            }

            if (dH.pocetNachadzajucichSaVBlokkoch() != osoby.size() || osoby.size() != dH.getaAktualnyPocetPlatnychZaznamovVStrukture()) {
                throw new IllegalStateException();
            }
            
            if (dH.pocetNachadzajucichSaVBlokkoch() != 0) {
                //dH.odstran(new Osoba(0));
                //System.out.println(dH.getaTrieStrom().vypisTrieStromuLevelOrder());
                System.out.println(dH.sekvencnyVypisSuboru());
                throw new IllegalStateException("Zostalo: " + dH.pocetNachadzajucichSaVBlokkoch());
            }
            if (dH.getaTrieStrom().getPocetPrvkov() != 0) {
                //System.out.println(dH.getaTrieStrom().vypisTrieStromuLevelOrder());
                throw new IllegalStateException("Trie strom mal mat pocet vrcholov 0. Zostalo: " + dH.getaTrieStrom().getPocetPrvkov());
            } else {
                System.out.println("Pocet Vrcholov TRIE: " + dH.getaTrieStrom().getPocetPrvkov());
            }
            System.out.println("MAZANIE OK");
            System.out.println("V hl. zostalo blokov: " + dH.aFilemanagerHlavnehoSuboru.getPocetBlokovVSubore(new Blok(pocetZaznamov, Osoba.getImplmentatorTohtoZaznamu(), true).getVelkostBlokuVBajtoch()));
            System.out.println("V prepl. zostalo blokov: " + dH.aFileManagerPreplnSuboru.getPocetBlokovVSubore(new Blok(pocetZaznamov, Osoba.getImplmentatorTohtoZaznamu(), true).getVelkostBlokuVBajtoch()));
            System.out.println("V dH zostalo: " + dH.pocetNachadzajucichSaVBlokkoch());
            //System.out.println(dH.sekvencnyVypisSuboru());
            System.out.println("\n\n");
            int pocVL = 0;
            int pocOd = 0;
            for (int k = 0; k < 20000; k++) {
                if (r.nextInt(2) == 1) {
                    int roden_cislo = r.nextInt(rozsahRodCisla);
                    if (dH.vloz(new Osoba(roden_cislo), false) == DynamickeHesovanie.STAV_VKLADANIA.VLOZENY) {
                        pocVL++;
                        osoby.add(roden_cislo);
                        if (pocVL % 1000 == 0) {
                            System.out.println("VLOZ: " + pocVL);
                        }
                    }
                } else if(osoby.size() > 0){
                    int rodne_cislo_index = r.nextInt(osoby.size());
                    if (dH.odstran(new Osoba(osoby.get(rodne_cislo_index))) != null) {
                        osoby.remove(rodne_cislo_index);
                        pocOd++;
                        if (pocOd % 1000 == 0) {
                            System.out.println("ODSTRAN: " + pocOd);
                        }
                    }
                }
            }
            System.out.println(osoby.size());
            for (int j = 0; j < osoby.size(); j++) {
                if(dH.odstran(new Osoba(osoby.get(j)))==null){
                    throw new IllegalStateException();
                };
            }
            dH.uvolniSubory();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*DynamickeHesovanie<Osoba> dH = new DynamickeHesovanie<>(2, Osoba.getImplmentatorTohtoZaznamu(), 1);
         Osoba os1 = new Osoba(3);
         dH.vloz(os1);
         os1 = new Osoba(1);
         dH.vloz(os1);
         os1 = new Osoba(2);
         dH.vloz(os1);
         os1 = new Osoba(2);
         dH.odstran(os1);
         System.out.println(dH.vypisTrieStromuLevelOrder());
         System.out.println(dH.sekvencnyVypisSuboru());
         dH.uvolniSubory();*/
    }
}

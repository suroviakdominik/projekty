

public class ElementarneStlpcoveOperacie
{
  private double [] pomocnePole;
  private PoctySMaticami aPocSMat;
    public ElementarneStlpcoveOperacie(PoctySMaticami paPocSMat)
    {
        aPocSMat=paPocSMat;
    }
                                                      
    
    public void vymenStlpce(int i,int j)//ity.stlpec za jty.stlpec
     {
         pomocnePole=new double[aPocSMat.dajPocetStlpcov()];
             for (int n=0;n<aPocSMat.dajPocetRiadkov();n++)
             {
                pomocnePole[n]=aPocSMat.zistiHodnotuCopyPola(n,i);
                aPocSMat.vlozHodnotuDoCopyPola(n,i,aPocSMat.zistiHodnotuCopyPola(n,j));
                aPocSMat.vlozHodnotuDoCopyPola(n,j,pomocnePole[n]);
             }
             System.out.println("Vymenil som "+(i+1)+". stlpec za "+(j+1)+". stlpec");
     }
     
    public void pripocitajNasoboItehoStlpcaKJetemuStlpcu(int i,int j,int s)//1.stlpec,2.stlpec
    {
           double vysDel=(aPocSMat.zistiHodnotuCopyPola(s,j)/aPocSMat.zistiHodnotuCopyPola(s,i))*(-1);
           for (int l=0;l<aPocSMat.dajPocetRiadkov();l++)
           {
                        double nasobItyRiadok=aPocSMat.zistiHodnotuCopyPola(l,i)*vysDel;
                        aPocSMat.vlozHodnotuDoCopyPola(l,j,(aPocSMat.zistiHodnotuCopyPola(l,j)+nasobItyRiadok));
           }
           if (vysDel!=0)
           {
              System.out.println("K "+(j+1)+". stlpcu som pripocital "+Zaokruhli.zaokruhluj(vysDel)+"-nasobok "+(i+1)+". stlpca");
           }
    } 
    
    public void pripocitajItyStlpecKJetemuStlpcu(int i,int j)
    {
        for (int p=0;p<aPocSMat.dajPocetRiadkov();p++)
        {
           aPocSMat.vlozHodnotuDoCopyPola(p,j,(aPocSMat.zistiHodnotuCopyPola(p,i)+aPocSMat.zistiHodnotuCopyPola(p,j))); 
        }
        System.out.println(+(i+1)+". stlpec som pripocital k "+(j+1)+". stlpcu");
    }   
    
    public void vynasobStlpcecKonstantou(int paItyStlpec, int paCinitel)
    {
        for (int j=0;j<aPocSMat.dajPocetStlpcov();j++)
        {
          aPocSMat.vlozHodnotuDoCopyPola(j,paItyStlpec,(paCinitel*aPocSMat.zistiHodnotuCopyPola(j,paItyStlpec))); 
        }
    }
}

﻿namespace Snake.Game
{
    public enum StavHry
    {
        StartMenu,
        MultiPlayerMenu,
        Paused,
        GameOver,
        PlayingSinglePlayer,
        PlayingMultiplayer,
        WaitingForAnotherPlayers,
        Win,
        ScoreScreen
    }
}

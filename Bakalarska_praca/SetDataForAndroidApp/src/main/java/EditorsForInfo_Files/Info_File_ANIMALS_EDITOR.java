/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.ScaleImage;
import WorkWithFile.SerializationWorker;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import setdatatoandroidaplication.DataTransferer;

/**
 * Editor, ktory edituje zvierata 
 * @author Dominik
 */
public class Info_File_ANIMALS_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTranferer;

    /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce zvierata
     *                      a popis ich dat
     */
    public Info_File_ANIMALS_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aDataTranferer = DataTransferer.getInstance();
        aInfoAboutDirsFile=paFilesInfo;
    }

    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return
     */
    @Override
    public boolean getDataOfInfoFile() {
        Info_About_Item lpDataForAnimal;
        if (aDataTranferer.getItemInfo("GET_IMAGE_ANIMAL") != null) {
            lpDataForAnimal = aDataTranferer.getItemInfo("GET_IMAGE_ANIMAL");

            BufferedImage im = getPictureOfSelectedAnimalIfExists(lpDataForAnimal);
            if (im != null) {
                aDataTranferer.removeAllData();
                aDataTranferer.addImage("RET_IMAGE_ANIMAL", im);
                return true;
            } else {
                return false;
            }

        } else if (aDataTranferer.getItemInfo("GEO_POSITION_ANIMAL") != null) {
            lpDataForAnimal = aDataTranferer.getItemInfo("GEO_POSITION_ANIMAL");
            return getGEOCoordinates(lpDataForAnimal);
        } else if (aDataTranferer.getItemInfo("GET_TEXT_ANIMAL") != null) {
            lpDataForAnimal = aDataTranferer.getItemInfo("GET_TEXT_ANIMAL");
            return getDescriptionOfSelectedAnimal(lpDataForAnimal);
        } else if (aDataTranferer.getString("GET_ANIMAL_OF_CHOOSEN_CATEGORY") != null) {
            String paKtorejKategorie = aDataTranferer.getString("GET_ANIMAL_OF_CHOOSEN_CATEGORY");
            return getAnimalsOfSelectedCategory(paKtorejKategorie);
        }
        return false;
    }

     /**
     * Vytvorenie noveho zvierata. Data dostaneme zo zalozky pomocou objektu DataTransferer
     * @return true - zviera sa podarilo vytvorit
     */
    @Override
    public boolean addNewItem() {
        String nazovNovZvier = aDataTranferer.getString("NEW_ANIMAL");
        String doKtorejCat = aDataTranferer.getString("CHOOSEN_CATEGORY");
        aDataTranferer.removeAllData();
        if (nazovNovZvier.equals("")) {
            return false;
        }
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).getOriginalName().equals(nazovNovZvier)) {
                return false;
            }
        }
        String relativePath;
        relativePath = aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice() 
                + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + InfoSuboryENUM.FILE_SEPARATOR + (aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + aInfoAboutDirsFile.getNumberOfNextItem() + InfoSuboryENUM.FILE_SEPARATOR );

        Info_About_Item newInfo = new Info_About_Item(nazovNovZvier, doKtorejCat, relativePath);
        aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + aInfoAboutDirsFile.getNumberOfNextItem(),newInfo);
        DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                +InfoSuboryENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM()
                + InfoSuboryENUM.ANIMALS.getSignOfDir()
                +InfoSuboryENUM.FILE_SEPARATOR  + (InfoSuboryENUM.ANIMALS.getSignOfDir() + aInfoAboutDirsFile.getNumberOfNextItem()));
        dw.vytvorAdresarAkNeexistuje();
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                +relativePath  
                +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        fw.createFileIfNonExists();
        System.out.println(InfoSuboryENUM.ANIMALS.getServerDIRDomainOfThisFile()
                + relativePath + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
        newInfo.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.ANIMALS.getServerDIRDomainOfThisFile()
                + relativePath + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        aInfoAboutDirsFile.raiseNumberOfNextNewItem();
        aDataTranferer.removeAllData();
        aDataTranferer.addItemInfo("CREATED_ANIMAL", newInfo);
        System.out.println("MyLogFiles: New animal with name " + newInfo.getOriginalName()
                + "\n has been created in editor"+this.getClass().getSimpleName());
        return saveInfoFile();
    }

      /**
     * Zmaze zviera, ktore daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer
     * @return true -zviera sa podarilo zmazat
     */
    @Override
    public boolean deleteItem() {
        Info_About_Item choosenAnimal = aDataTranferer.getItemInfo("SELECTED_ANIMAL_TO_DELETE");
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).equals(choosenAnimal)) {
                File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                        + aAktualneUdajeInfoSuboru.get(prech).getRelativePathOfFilesOfurrentItemInDevice());
                try {
                    aAktualneUdajeInfoSuboru.remove(prech);
                    saveInfoFile();
                    delete(f);
                } catch (IOException ex) {
                    Logger.getLogger(Info_File_ANIMALS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                    return false;
                }
                System.out.println("MyLogFiles: Animal: " + choosenAnimal.getOriginalName()
                        + "\n has been deleted in editor"+this.getClass().getSimpleName());
                return true;
            }
        }
        return false;
    }

    /**
     * Upravi data zvierara, ktore daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -zviera sa podarilo upravit(zmenit data)
     */
    @Override
    public boolean editItem() {
        Info_About_Item lpEditedAnimal = aDataTranferer.getItemInfo("EDITED_ANIMAL");
        String path = aDataTranferer.getString("EDIT_PICTURE_PATH");
        String text = aDataTranferer.getString("EDITED_TEXT");
        
        if (lpEditedAnimal == null) {
            
             if (aDataTranferer.getItemInfo("DELETE_PICTURE_ANIMAL") != null) {
                lpEditedAnimal = aDataTranferer.getItemInfo("DELETE_PICTURE_ANIMAL");
                lpEditedAnimal.setDateOfEditOrCreateFile(null);
                saveInfoFile();
                return deletePicture(lpEditedAnimal);

            }
             return false;
        }
        lpEditedAnimal.setDateOfEditOrCreateFile(null);
        if (path != null) {
            return editPicture(path, lpEditedAnimal);
        } else if (text != null) {

           return editDescriptionOfSelectedAnimal(text,lpEditedAnimal);
        } else {
            return  editGeoCoordinate(lpEditedAnimal);
        }

        

    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.ANIMALS.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM());
        sw.saveObjectToFile();
        System.out.println("MyLogFiles: Info_About_Dirs_File in editor: "
                        +this.getClass().getSimpleName()
                        +" has been saved.");
        return true;
    }

    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
    @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }

    private void deleteExistsPictureThatWillBeReplaced(Info_About_Item paDeletedAnimal, String paDirectoryWithPicture, String paEndOfPictureFile) {
        File f = new File(paDirectoryWithPicture);
        if (f.listFiles().length == 0) {
            return;
        }
        for (File prech : f.listFiles()) {
            if (!prech.isDirectory() && prech.getName().contains(InfoSuboryENUM.NAME_OF_PICTURE_FILE)) {
                paDeletedAnimal.getDomainsOfFilesInCurrentItem().remove(InfoSuboryENUM.ANIMALS.getServerDIRDomainOfThisFile()
                        + paDeletedAnimal.getRelativePathOfFilesOfurrentItemInDevice()
                        + InfoSuboryENUM.NAME_OF_PICTURE_FILE
                        + "." + paEndOfPictureFile);
                prech.delete();
                System.out.println(InfoSuboryENUM.FILE_SEPARATOR );
                return;
            }
        }
    }

    private String getEndOfFile(Path paPathOfFile) {
        String fileName = paPathOfFile.getFileName().toString();
        try {
            return fileName.split("\\.")[1].toLowerCase();
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }

    }

    private BufferedImage getPictureOfSelectedAnimalIfExists(Info_About_Item paSelectedAnimal) {
        String relativeTargetPath = paSelectedAnimal.getRelativePathOfFilesOfurrentItemInDevice();
        if (relativeTargetPath == null) {
            return null;
        }
        File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativeTargetPath);
        if (f.listFiles().length == 0) {
            f = null;
        } else {
            for (File prech : f.listFiles()) {
                if (prech.getName().split("\\.").length > 1 && ((prech.getName().split("\\.")[1].toLowerCase()).equals("jpg")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("png")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("gif"))) {
                    f = new File(prech.getAbsolutePath());
                    break;
                } else {
                    f = null;
                }
            }
        }
        BufferedImage myPicture = null;
        if (f != null) {

            try {
                myPicture = ImageIO.read(f);

            } catch (IOException ex) {
                Logger.getLogger(Info_File_ANIMALS_EDITOR.class
                        .getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();

                return null;
            }
        }
        System.out.println("MyLogFiles: Picture of  animal: "
                + paSelectedAnimal.getOriginalName() + " \nhas been set set to data transfer"
                + " object in editor:" + this.getClass().getSimpleName() + "\n");
        return myPicture;
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    private boolean getGEOCoordinates(Info_About_Item paWhoAnimal) {
        aDataTranferer.addString("RET_LATITUDE", paWhoAnimal.getZemSirkaText());
        aDataTranferer.addString("RET_LONGITUDE", paWhoAnimal.getZemDlzkaText());
        System.out.println("\nMyLogFiles: Geo coordinate of animal: "
                + paWhoAnimal.getOriginalName() + "\n has been set to data transfer "
                + "object in editor:" + this.getClass().getSimpleName() + "\n");
        return true;

    }

    private boolean getDescriptionOfSelectedAnimal(Info_About_Item paSelectedAnimal) {
        String ret = paSelectedAnimal.getRelativePathOfFilesOfurrentItemInDevice();
        aDataTranferer.removeAllData();
        if (ret != null) {
            FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                    + ret + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            fw.createFileIfNonExists();
            try {
                String textData = fw.readDataFromFile();
                aDataTranferer.addString("RET_TEXT_ANIMAL", textData);
            } catch (IOException ex) {
                Logger.getLogger(Info_File_ANIMALS_EDITOR.class.getName()).
                        log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return false;
            }
            System.out.println("MyLogFiles: Description of animal: "
                    + paSelectedAnimal.getOriginalName() + "\n has been set to data transfer object "
                    + "in editor:" + this.getClass().getSimpleName() + "\n");
            return true;
        } else {
            return false;
        }
    }

    private boolean getAnimalsOfSelectedCategory(String paSelectedCategoryKey) {
        if (paSelectedCategoryKey != null) {
            aDataTranferer.removeAllData();
            if (aAktualneUdajeInfoSuboru.size() == 0) {
                return false;
            }
            for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
                if (aAktualneUdajeInfoSuboru.get(prech).getParentDirectory().equals(paSelectedCategoryKey)) {
                    aDataTranferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
                }
            }
            System.out.println("\nMyLogFiles: Animals of selected category \n"
                    + "has been set to data transfer object in editor:" + this.getClass().getSimpleName());
            return true;
        }
        aDataTranferer.removeAllData();
        return false;
    }

    private boolean editPicture(String paPathOfNewPicture, Info_About_Item paEditedAnimal) {
        String relativePath = paEditedAnimal.getRelativePathOfFilesOfurrentItemInDevice();
        try {

            Path source = FileSystems.getDefault().getPath(paPathOfNewPicture);
            deleteExistsPictureThatWillBeReplaced(paEditedAnimal, InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativePath, getEndOfFile(source));
            Path target = FileSystems.getDefault().getPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                    + relativePath
                    + InfoSuboryENUM.NAME_OF_PICTURE_FILE+ "." + getEndOfFile(source));
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            ScaleImage.scale(target.toString(), getEndOfFile(source));

            String domainOfFile = (InfoSuboryENUM.ANIMALS.getServerDIRDomainOfThisFile()
                    + relativePath + InfoSuboryENUM.NAME_OF_PICTURE_FILE 
                    + "." + getEndOfFile(source));
            if(!paEditedAnimal.getDomainsOfFilesInCurrentItem().contains(domainOfFile))
            {
                paEditedAnimal.getDomainsOfFilesInCurrentItem().add(domainOfFile);
            }

        } catch (IOException ex) {
            Logger.getLogger(Info_File_ANIMALS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        System.out.println("\nMyLogFiles: Picture has been replaced by new picture '\n"
                + "for animal: "+paEditedAnimal.getOriginalName()+" in editor: "+this.getClass().getSimpleName());
        saveInfoFile();
        return true;
    }
    
    private boolean editDescriptionOfSelectedAnimal(String paNewText,Info_About_Item paEditedAnimal)
    {
         FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                 + paEditedAnimal.getRelativePathOfFilesOfurrentItemInDevice()
                 + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            try {
                fw.writeDataToFile(paNewText);

            } catch (IOException ex) {
                Logger.getLogger(Info_File_ANIMALS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            System.out.println("\nMyLogFiles: Description of animal: "
                    +paEditedAnimal.getOriginalName()+"\n has been updated"
                + " in editor: "+this.getClass().getSimpleName());
            saveInfoFile();
            return true;
    }
    
    private boolean editGeoCoordinate(Info_About_Item paEditedAnimal)
    {
        Double lon = aDataTranferer.getDouble("SET_LONGITUDE");
            if (lon != null) {
                paEditedAnimal.setaZemepisnaDlzka(lon);
                paEditedAnimal.setSuradniceZemDlzkaText(aDataTranferer.getString("SET_LONGITUDE"));
            }

            Double lat = aDataTranferer.getDouble("SET_LATITUDE");
            if (lat != null) {
                paEditedAnimal.setaZemepisnaSirka(lat);
                paEditedAnimal.setSuradnicaZemSirkaText(aDataTranferer.getString("SET_LATITUDE"));
            }
            
            System.out.println("\nMyLogFiles: Geo coordinate of animal: "
                    +paEditedAnimal.getOriginalName()+"\n has been updated"
                + " in editor: "+this.getClass().getSimpleName());
            saveInfoFile();
            return true;
    }
    
    private boolean deletePicture(Info_About_Item paEditedObject) {
        for (String prech : paEditedObject.getDomainsOfFilesInCurrentItem()) {
            if (prech.contains(InfoSuboryENUM.NAME_OF_PICTURE_FILE)) {
                File f1 = new File(prech);
                File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                        + paEditedObject.getRelativePathOfFilesOfurrentItemInDevice()
                        + f1.getName());
                f.delete();
                return true;
            }
        }
        return false;
    }
}


/**
 * Trieda vykonáva elementárne riadkové operácie na matici
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class ElementarneRiadkoveOperacie
{
    private double [] pomocnePole;
    private PoctySMaticami aPocSMat;
  /**
     * Konštruktor načíta predtým vytvórenú maticu
     */
    public ElementarneRiadkoveOperacie(PoctySMaticami paPocSMat)
  {
     aPocSMat=paPocSMat;
  }
                                                      
     /**
      * metóda vymení riadok nachádzajúce sa na indexoch i a j
      * @param i riadok
      * @param j riadok, ktorý sa má vymeniť s i-tym riadkom
      */
     public void vymenRiadky(int i,int j)
     {
         pomocnePole=new double[aPocSMat.dajPocetStlpcov()];
             for (int n=0;n<aPocSMat.dajPocetStlpcov();n++)
             {
                pomocnePole[n]=aPocSMat.zistiHodnotuCopyPola(i,n);
                aPocSMat.vlozHodnotuDoCopyPola(i,n,aPocSMat.zistiHodnotuCopyPola(j,n));
                aPocSMat.vlozHodnotuDoCopyPola(j,n,pomocnePole[n]);
             }
             System.out.println("Vymenil som "+(i+1)+". riadok za "+(j+1)+". riadok");
     }
     
    /**
     * metóda vydelí dva riadky j/i podľa prvku v stĺpci nachádzajúceho sa na s-tom mieste, výsledok delenie použije pri násobení i-teho riadku a pripočítaní k j-temu riadku
     * @param i nasobený riadok
     * @param j riadok ku ktorému sa pripočíta nácobok i-teho riadku
     * @param s stĺpec ktorý sa bude deliť
     */
     public void pripocitajNasoboItehoRiadkuKJetemuRiadku(int i,int j,int s)
    {
           double vysDel=((aPocSMat.zistiHodnotuCopyPola(j,s)/aPocSMat.zistiHodnotuCopyPola(i,s))*(-1));
           for (int l=0;l<aPocSMat.dajPocetStlpcov();l++)
           {
                        double nasobItyRiadok=aPocSMat.zistiHodnotuCopyPola(i,l)*vysDel;
                        aPocSMat.vlozHodnotuDoCopyPola(j,l,Zaokruhli.zaokruhluj((aPocSMat.zistiHodnotuCopyPola(j,l)+nasobItyRiadok)));
           }
           if (vysDel!=0)
           {
              System.out.println("K "+(j+1)+". riadku som pripocital "+vysDel+"-nasobok "+(i+1)+". riadku"); 
           }
    } 
    
    /**
     * metóda pripočíta i-ty riadok k j-temu riadku
     * @param i riadok
     * @param j riadok, ktorý vznikol pripočítaním i-teho riadku k predchádzajúcemu j-temu riadku
     */
    public void pripocitajItyRiadokKJetemuRiadku(int i,int j)
    {
        for (int p=0;p<aPocSMat.dajPocetStlpcov();p++)
        {
           aPocSMat.vlozHodnotuDoCopyPola(j,p,(aPocSMat.zistiHodnotuCopyPola(i,p)+aPocSMat.zistiHodnotuCopyPola(j,p))); 
        }
        System.out.println(+(i+1)+". riadok som pripocital k "+(j+1)+". riadku");
    } 
    
     /**
     * metóda vynásobí paItyRiadok paCinitelom
     * @param paItyRiadok riadok
     * @param paCinitel činiteľ
     */
    public void vynasobRiadokKonstantou(int paItyRiadok, int paCinitel)
    {
        for (int j=0;j<aPocSMat.dajPocetStlpcov();j++)
        {
          aPocSMat.vlozHodnotuDoCopyPola(paItyRiadok,j,(paCinitel*aPocSMat.zistiHodnotuCopyPola(paItyRiadok,j))); 
        }
    }
    
    
    
}
            
            
            
            
            
 
 
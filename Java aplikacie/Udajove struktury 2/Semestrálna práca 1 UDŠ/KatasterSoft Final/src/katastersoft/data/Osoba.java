/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.data;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class Osoba {

    private String aRodneCislo;
    private String aMeno;
    private String aPriezvisko;
    private Nehnutelnost aTrvaleBydlisko;
    private List<ListVlastnictva> aLVNaKtorychJeOsoba;

    private final Logger LoggerOsoba = Logger.getLogger(this.getClass().getName());

    public Osoba(String aRodneCislo, String aMeno, String aPriezvisko) {
        this.aRodneCislo = aRodneCislo;
        this.aMeno = aMeno;
        this.aPriezvisko = aPriezvisko;
        aLVNaKtorychJeOsoba = new LinkedList<>();
        LoggerOsoba.setLevel(Level.OFF);
    }

    public String getaMeno() {
        return aMeno;
    }

    public void setaMeno(String aMeno) {
        this.aMeno = aMeno;
    }

    public String getaPriezvisko() {
        return aPriezvisko;
    }

    public void setaPriezvisko(String aPriezvisko) {
        this.aPriezvisko = aPriezvisko;
    }

    public String getaRodneCislo() {
        return aRodneCislo;
    }

    public Nehnutelnost getaTrvaleBydlisko() {
        return aTrvaleBydlisko;
    }

    /**
     * ubytuje osobu v nehnutelnosti nastavi jej nove trvale bydlisko a vlozi ho
     * do zoznamu ubytovanych v nehnutelnosti
     *
     * @param paTrvaleBydlisko
     */
    public void setaTrvaleBydlisko(Nehnutelnost paTrvaleBydlisko) {
        this.aTrvaleBydlisko = paTrvaleBydlisko;
        if (paTrvaleBydlisko != null) {
            paTrvaleBydlisko.pridajNovehoNaUbytovanie(this);
        }
    }

    /**
     * prida LV, na ktrorom ma osoba majetkovy podiel
     *
     * @param paLV
     * @return false- ak uz ma dany list vlastnictva
     */
    public boolean pridajNovyLV(ListVlastnictva paLV) {
        if (aLVNaKtorychJeOsoba.contains(paLV)) {
            LoggerOsoba.log(Level.INFO, "Osoba akymsi 'zazrakom':) uz tento LV vlastni");
            return false;
        }
        LoggerOsoba.log(Level.INFO, "Pridal som osobe novy LV, na ktorom je zapisana.");
        aLVNaKtorychJeOsoba.add(paLV);
        return true;
    }

    /**
     *Odoberie LV zo zoznamu LV, kde mala osoaba majetkovy podiel
     * @param paLV
     * @return false- ak uz ma dany list vlastnictva
     */
    public ListVlastnictva odoberLV(ListVlastnictva paLV) {
        if (aLVNaKtorychJeOsoba.contains(paLV)) {
            LoggerOsoba.log(Level.INFO, "Osobe som odstranil LV");
            return aLVNaKtorychJeOsoba.remove(aLVNaKtorychJeOsoba.indexOf(paLV));
        }
        LoggerOsoba.log(Level.INFO, "Osobe som neodstranil LV, pretoze predtym nebol vytvoreny zaznam, ktory by hovoril ze ho vlastnila");
        return null;
    }

    /**
     * Predtym ako ideme odstranovate osobu, tak 
     * jej musime zrusit majetkove podiely vo vsetkych LV
     * v ktorych ich mala
     */
    public void predOdstranenimOsoby() {
        if (aTrvaleBydlisko != null) {
            aTrvaleBydlisko.odoberObyvatela(aRodneCislo);
        }
        while (aLVNaKtorychJeOsoba.size() > 0) {
            aLVNaKtorychJeOsoba.get(0).odoberMajetkovyPodiel(aRodneCislo);
        }
    }

    /**
     * 
     * @return zoznam LV, na ktorych ma osoba majetkovy podiel
     */
    public List<ListVlastnictva> getLVOsoby() {
        return aLVNaKtorychJeOsoba;
    }

    /**
     * Tato metoda narozdiel od toString() nevypisuje LV, kde ma osoaba majetkovy podiel
     * @return  
     */
    public String vypisIbaKontaktneUdaje() {
        String ret = "______________________________OSOBA___________________________________________________________________\n";
        ret += "Meno: " + aMeno + "\tPriezvisko: " + aPriezvisko + " \tRC: " + aRodneCislo + "\n"
                + "Pocet LV: " + aLVNaKtorychJeOsoba.size() + "\n";
        if (aTrvaleBydlisko != null) {
            ret += "Trvaly pobyt: \n" + aTrvaleBydlisko.toString();
        } else {
            ret += "Trvaly pobyt:NEURCENY \n";
        }
        return ret;
    }

    @Override
    public String toString() {
        String ret = "______________________________OSOBA___________________________________________________________________\n";
        ret += "Meno: " + aMeno + "\tPriezvisko: " + aPriezvisko + " \tRC: " + aRodneCislo + "\n"
                + "Pocet LV: " + aLVNaKtorychJeOsoba.size() + "\n"
                + "Trvaly pobyt: \n" + aTrvaleBydlisko.toString();
        for (ListVlastnictva aLVNaKtorychJeOsoba1 : aLVNaKtorychJeOsoba) {
            ret += aLVNaKtorychJeOsoba.toString();
        }
        ret += "\n__________________________________________________________________________________________________________\n\n";
        return ret;
    }
}

﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Snake.DrawableObjects
{
    public class OnlineSnake : Snake
    {
        private Direction aNewSnakeDirection;///smer ktorym sa naposledy pohyboval onlien hrac
        private Color aColorOfSnake;///farba hada
        
        ///<summary>
        /// konstruktor
        /// </summary>
        /// /// <param name="paColorOfSnake">farba online hraca</param>
        /// /// <param name="paGraphicsDevice">platno na ktore kreslim</param>
        public OnlineSnake(Color paColorOfSnake, GraphicsDevice paGraphicsDevice)
            : base(paGraphicsDevice)
        {
            aColorOfSnake = paColorOfSnake;
            aNewSnakeDirection = Direction.Non;
        }

        /// <summary>
        /// nastaci smer, ktorym sa bude had pri najblizsom update pohybovat
        /// </summary>
        public Direction NewSnakeDirection
        {
            get { return aNewSnakeDirection; }
            set { aNewSnakeDirection = value; }
        }
        /// <summary>
        /// posunie online hada v smere pozicie, ktorou sa pohyboval naposledy
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (aNewSnakeDirection != ActualFirstPartDirection)
            {
                if (ActualFirstPartDirection == Direction.Non)
                {
                    initMovementStart(aNewSnakeDirection);
                }
                else
                {
                    aPositionsOfSnakeParts.First.Value.NextDirection = aNewSnakeDirection;
                }
            }
            //posun v smere, ktory som dostal updatom
            RecalculatePositionOfAllSnakeParts();
            if (VykonanyJedenPohyb)
            {
                // ak mi prisla zo servera sprava, ze had zmenil smer pohybu po presunuti, potom zmenim smer
                PlayerLastUpdateMoveSize = 0;
                foreach (var iter in aPositionsOfSnakeParts)
                {
                    iter.OneMovementFinish = true;
                }
                RecalculatePositionOfAllSnakeParts();
            }
            moveToScreenIfPartIsOverTheScreen();
        }

        /// <summary>
        /// vykreslenie online hada na obrazovku
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            Vector2 stringSize = aFont.MeasureString("Score: "+Score);
            Vector2 pos = new Vector2(GRaphDev.Viewport.Width-stringSize.X,0);
            spriteBatch.DrawString(aFont, "Score: "+Score,pos, aColorOfSnake, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            foreach (var iter in aPositionsOfSnakeParts)
            {
                spriteBatch.Draw(aPlayerTexture, iter.CurrentPartPosition, null, aColorOfSnake, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }
            /*spriteBatch.Draw(aPlayerTexture, aPositionsOfSnakeParts[0].CurrentPartPosition, null, Color.White, 0f, Vector2.Zero, 1f,
                SpriteEffects.None, 0f);*/
        }
    }
}

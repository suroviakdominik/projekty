/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacUloz2 implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        try {
            zapis(paHrac);
        } catch (IOException ex) {
            Logger.getLogger(VykonavacUloz2.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    private void zapis(Hrac paHrac) throws IOException {
        FileOutputStream fileOut = new FileOutputStream("serial.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);

        out.writeObject(paHrac);

        fileOut.close();
        out.close();

    }

}

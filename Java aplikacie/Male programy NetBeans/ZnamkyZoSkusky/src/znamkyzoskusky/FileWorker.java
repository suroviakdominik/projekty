/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyzoskusky;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {

   public static String readDataFromFile(File f,String paOddelovacRiadkov)
   {
       String ret="";
       Reader r=null;
       try {
           r= new InputStreamReader(new FileInputStream(f),"UTF-8");
           BufferedReader read= new BufferedReader(r);
           String actualLine=null;
           while((actualLine=read.readLine())!=null)
           {
               ret+=actualLine+paOddelovacRiadkov;
           }
       } catch (UnsupportedEncodingException ex) {
           Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
           ex.printStackTrace();
       } catch (FileNotFoundException ex) {
           Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
           ex.printStackTrace();
       } catch (IOException ex) {
           Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
           ex.printStackTrace();
       }
       return ret;
   }
    
}

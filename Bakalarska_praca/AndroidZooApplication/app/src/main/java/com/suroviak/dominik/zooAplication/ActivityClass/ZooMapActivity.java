package com.suroviak.dominik.zooAplication.ActivityClass;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.Navigation.DisplayableMapObjects;
import com.suroviak.dominik.zooAplication.Navigation.MyLocationProvider;
import com.suroviak.dominik.zooAplication.Navigation.Navigator;
import com.suroviak.dominik.zooAplication.Navigation.Road;
import com.suroviak.dominik.zooAplication.Navigation.RoadPoint;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZooMapActivity extends ActionBarActivity implements OnMapReadyCallback {
    //MAPA ATRIBUTY
    private final int DURATION_OF_ANIMATION_MOVE_MARKER = 1000;//millis
    private final float SOUTH_WEST_LAT_PICTURE = 48.156235f;
    private final float SOUTH_WEST_LON_PICTURE = 17.062357f;
    private final float NORTH_EAST_LAT_PICTURE = 48.163998f;
    private final float NORTH_EAST_LON_PICTURE = 17.081198f;
    private final float ROTATE = -84.7f;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Menu aMenu;
    private RoadPoint aVybranyBodNaMape;
    private PridaneObjektyMapy aPridaneObjektyMapy;
    private Bundle bundle;

    private Road aCesta;
    private Navigator aNavigator;

    private MyLocationProvider aLocationProvider;
    private Location aMyCurrentLocation;
    //KONIEC MAPA ATRIBUTY

    //VYHLADAVANIE ATRIBUTY
    private Map<String, String> aDataForSuggestions;
    private boolean aSuggAdWasSet = false;
    private MatrixCursor aCursorSuggestionsForSearch;
    private SearchView aSearchView;
    private DisplayableMapObjects aZobrazitelneObjekty;
    //KONIEC VYHLADAVANIE ATRIBUTY
    /*  DEFAULT PICTURE DATA
        private final float SOUTH_WEST_LAT_PICTURE = 48.156235f;
        private final float SOUTH_WEST_LON_PICTURE = 17.062357f;
        private final float NORTH_EAST_LAT_PICTURE = 48.163998f;
        private final float NORTH_EAST_LON_PICTURE = 17.081198f;
        private final float ROTATE = -84.7f;
    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoo_map);
        if (savedInstanceState != null) {
            aPridaneObjektyMapy = new PridaneObjektyMapy();

            if (savedInstanceState.getSerializable("SHOWED_MAP_OBJECTS") != null) {
                bundle = savedInstanceState;

            }
        } else {
            aPridaneObjektyMapy = new PridaneObjektyMapy();
        }

        aZobrazitelneObjekty = new DisplayableMapObjects();
        aDataForSuggestions = aZobrazitelneObjekty.getNamesOfAllObjects();
        inicializujUdajePreNavigovanie();
        if (aCesta != null) {
            aNavigator = new Navigator(aCesta);

        }
        setUpMapIfNeeded();
        aLocationProvider = new MyLocationProvider(this);
        if (!aLocationProvider.canGetLocation()) {
            aLocationProvider.showSettingsAlert();
        }


        LinearLayout layoutOjectCatPanel = (LinearLayout) findViewById(R.id.object_panel);
        final Map<String, String> dataForPanel = aZobrazitelneObjekty.getNazvyKategoriiObjektov();
        for (String prech : dataForPanel.keySet()) {
            if (aZobrazitelneObjekty.getObjektyZvolenejKategorie(prech).size() > 0) {
                Button b = new Button(this);
                b.setText(dataForPanel.get(prech));
                //b.setBackgroundColor(Color.parseColor("#c8fffffd"));
                b.setTextColor(Color.WHITE);
                b.setLayoutParams(new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        stlaceneTlacidloKategObjektov((Button) v);
                    }
                });
                layoutOjectCatPanel.addView(b);
            }
        }
        Log.e("MyLog/DEBUG: ", "OnCreate of zoo map activity");
    }

    private void stlaceneTlacidloKategObjektov(Button b) {
        if (aPridaneObjektyMapy.getaZobrazeneObjektyKategoria() == null || !aPridaneObjektyMapy.getaZobrazeneObjektyKategoria().equals(b.getText().toString())) {
            aPridaneObjektyMapy.setaZobrazeneObjektyKategoria(b.getText().toString());
            aPridaneObjektyMapy.odstranZnackzZvolKategorie();
            final Map<String, String> dataForPanel = aZobrazitelneObjekty.getNazvyKategoriiObjektov();
            for (String prech : dataForPanel.keySet()) {
                if (b.getText().equals(dataForPanel.get(prech))) {

                    for (String prechObjektyZvolKat : aZobrazitelneObjekty.getObjektyZvolenejKategorie(prech).keySet()) {
                        LatLng coordinate = new LatLng(aZobrazitelneObjekty.getLatitudeOfObject(prechObjektyZvolKat), aZobrazitelneObjekty.getLongitudeOfObject(prechObjektyZvolKat));
                        Marker m = mMap.addMarker(new MarkerOptions()
                                .position(coordinate)
                                .title(aZobrazitelneObjekty.getObjektyZvolenejKategorie(prech).get(prechObjektyZvolKat))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                        m.showInfoWindow();
                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
                        mMap.animateCamera(yourLocation);
                        aPridaneObjektyMapy.pridajZnackuZvolKategorie(m);
                        zobrazMarkerInfo(m);
                    }
                    break;
                }
            }
        } else {
            aPridaneObjektyMapy.odstranZnackzZvolKategorie();
            aPridaneObjektyMapy.setaZobrazeneObjektyKategoria("");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.e("MyLog/DEBUG: ", "OnNewintent of zoo map activity");
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            /**
             * Use this query to display search results like
             * 1. Getting the data from SQLite and showing in listview
             * 2. Making webrequest and displaying the data
             * For now we just display the query only
             */
            //ak nepouzivam zadavanie hlasom,zmaz nasleujuce 3 riadky
            aSearchView.setEnabled(false);
            aSearchView.setQuery(query, false);
            aSearchView.setEnabled(true);
            Toast.makeText(this, query, Toast.LENGTH_LONG).show();

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri detailUri = intent.getData();
            String key = detailUri.getLastPathSegment();
            aSearchView.setEnabled(false);
            aSearchView.setQuery(aDataForSuggestions.get(key), false);
            aSearchView.setEnabled(true);
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(SOUTH_WEST_LAT_PICTURE, SOUTH_WEST_LON_PICTURE))
                    .title(aDataForSuggestions.get(key)));
            m.showInfoWindow();
            aPridaneObjektyMapy.pridajZnacku(m);
            LatLng coordinate = new LatLng(aZobrazitelneObjekty.getLatitudeOfObject(key), aZobrazitelneObjekty.getLongitudeOfObject(key));
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 16);
            mMap.animateCamera(yourLocation);
            animateMarker(m, new LatLng(aZobrazitelneObjekty.getLatitudeOfObject(key), aZobrazitelneObjekty.getLongitudeOfObject(key)), false);
            //zobrazenie a vypis info o bode
            Toast.makeText(this, "Search query: " + aDataForSuggestions.get(key), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("MyLog/DEBUG", "OnResume of zoo map activity");
        setUpMapIfNeeded();
        aLocationProvider.connnect();
        System.gc();

    }

    @Override
    protected void onPause() {
        super.onPause();
        aLocationProvider.disconnect();

        Log.e("MyLog/DEBUG", "OnPause of zoo map activity");
        //mMap.clear();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Call the super. You never know :)
        super.onSaveInstanceState(savedInstanceState);

        // debug
        Log.e("DEBUG", "onSaveInstanceState of ZooMapActivity");

        savedInstanceState.putSerializable("SHOWED_MAP_OBJECTS", aPridaneObjektyMapy.getSuradniceZobrazenychZnaciek());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        /*LatLngBounds newarkBounds = new LatLngBounds(
                new LatLng(48.15613611111111, 17.062258333333332),       // South west corner
                new LatLng(48.1639, 17.0810));      // north east corner*/
        LatLngBounds newarkBounds = new LatLngBounds(
                new LatLng(SOUTH_WEST_LAT_PICTURE, SOUTH_WEST_LON_PICTURE),       // South west corner
                new LatLng(NORTH_EAST_LAT_PICTURE, NORTH_EAST_LON_PICTURE));      // north east corner
        GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.zoo_map))
                .positionFromBounds(newarkBounds);
        newarkMap.transparency(0f);
        newarkMap.bearing(ROTATE);
        Log.e("MyLog/DEBUG", "OnMapReadyOf zoo map activity");


// Add an overlay to the map, retaining a handle to the GroundOverlay object.
        //imageOverlay=mMap.addGroundOverlay(newarkMap);
        mMap.addGroundOverlay(newarkMap);
        setUpMap();
    }


    public void handleNewLocation(Location location) {
        Log.e(ZooMapActivity.class.getSimpleName(), "Handled location: " + location.toString());
        if (aMyCurrentLocation == null || location.getLatitude() != aMyCurrentLocation.getLatitude() || location.getLongitude() != aMyCurrentLocation.getLongitude()) {
            aMyCurrentLocation = location;
            if (aVybranyBodNaMape != null) {
                aPridaneObjektyMapy.odstranVsetkyCesty();
                najdiNajkratsiuCestu(null);
            }

            Toast.makeText(this, "Prepocitavama cestu", Toast.LENGTH_SHORT).show();
        }
        TextView nepresnost = (TextView) findViewById(R.id.map_nepresnost_info);
        String text = "Nepresnosť: \n" + location.getAccuracy() + " m";
        final SpannableString spannableString = new SpannableString(text);
        int position = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '\n') {
                position = i;
                break;
            }
        }
        spannableString.setSpan(new RelativeSizeSpan(2.0f), position + 1, text.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        nepresnost.setText(spannableString);
//titleTextView.setText(spannableString.toString());


        Log.e("MyLogs/DEBUD", "Location update was successfull in" + this.getClass().getSimpleName());
    }


    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
            mapFragment.getMapAsync(this);
            if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                View btnMyLocation = ((View) mapFragment.getView().findViewById(1).getParent()).findViewById(2);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(80, 80); // size of button in dp
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                params.setMargins(10, 0, 0, 10);
                btnMyLocation.setLayoutParams(params);
            }
            mMap = mapFragment.getMap();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    LinearLayout ll = (LinearLayout) ZooMapActivity.this.findViewById(R.id.actions_navig);
                    ll.setVisibility(View.GONE);
                    aPridaneObjektyMapy.odstranVsetkyCesty();
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    zobrazMarkerInfo(marker);
                    return false;
                }


            });
            if (bundle != null) {
                HashMap<String, RoadPoint.Suradnica> surZnac = (HashMap<String, RoadPoint.Suradnica>) bundle.getSerializable("SHOWED_MAP_OBJECTS");
                for (String prech : surZnac.keySet()) {
                    Marker m = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(surZnac.get(prech).getaLatitude().getDECRepresentationOfCoordinate(), surZnac.get(prech).getaLongitude().getDECRepresentationOfCoordinate()))
                            .title(prech));
                    aPridaneObjektyMapy.pridajZnacku(m);
                }

                Log.e("MyLog/DEBUG", surZnac.toString());
            }

            Log.e("MyLog/DEBUG", "Map was set in zoo map activity");

        }
    }


    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        if (aCesta != null) {
        /*for(int i=0;i<aCesta.getPocetVrcholovNaCeste();i++) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(aCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate(),
                    aCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate())).title("ROAD_POINT").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        }*/

        }

    }

    public void najdiNajkratsiuCestu(View view) {
        if (aMyCurrentLocation != null) {
            if (mMap != null) {
                RoadPoint mojaPozVrchol = aNavigator.najdiHranuNaKtoruMoznoUmiestnitMojuPolohu(aMyCurrentLocation);

                if (mojaPozVrchol != null) {
                    List<RoadPoint> cesta = aNavigator.najdiNajkratsiuCestu(mojaPozVrchol, aVybranyBodNaMape);
                    aNavigator.odstranHranuKdeSomUmietnilMojuPolohu(mojaPozVrchol);
                    PolylineOptions opt = new PolylineOptions();
                    for (RoadPoint prech : cesta) {
                        opt.add(new LatLng(prech.getSuradnica().getaLatitude().getDECRepresentationOfCoordinate(),
                                prech.getSuradnica().getaLongitude().getDECRepresentationOfCoordinate()));
                    }
                    // Get back the mutable Polyline
                    Polyline polyline = mMap.addPolyline(opt);
                    vypisVzdialenostOdMojejPozicie(cesta);
                    aPridaneObjektyMapy.pridajCestu(polyline);

                } else {
                    Toast.makeText(this, "Pre navigovanie sa musíte nachádzať v areáli zoo vo vonkajšom priestore.", Toast.LENGTH_LONG).show();
                }
            }

        }
    }


    private void vypisVzdialenostOdMojejPozicie(List<RoadPoint> paBodyCesty) {
        TextView vzdialenost = (TextView) findViewById(R.id.map_vzdialenost_info);
        String text = "Vzdialenosť : \n" + (int) aNavigator.vypocitajVzdialenostCesty(paBodyCesty) + " m";
        final SpannableString spannableString = new SpannableString(text);
        int position = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '\n') {
                position = i + 1;
                break;
            }
        }
        spannableString.setSpan(new RelativeSizeSpan(2.0f), position, text.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        vzdialenost.setText(spannableString);
    }

    //POLOŽKY Action Bar Start
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_zoo_map, menu);
        aMenu = menu;
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        aSearchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        aSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        aSearchView.setSubmitButtonEnabled(true);
        aSearchView.setQueryRefinementEnabled(true);

        aSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {


                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                loadData(query);

                return true;

            }

        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar searchsuggestionsitem clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    /**
     * pre vyhladavanie
     *
     * @param query
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void loadData(String query) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            // Load data from list to aCursorSuggestionsForSearch
            String[] columns = new String[]{"_id", "name_of_object", SearchManager.SUGGEST_COLUMN_INTENT_DATA};
            String[] temp = new String[]{"" + 0, "default", ""};

            aCursorSuggestionsForSearch = new MatrixCursor(columns);
            int i = 0;
            for (String prech : aDataForSuggestions.keySet()) {

                if (!query.equals("") && aDataForSuggestions.get(prech).toLowerCase().startsWith(query.toLowerCase())) {
                    temp[0] = "" + i;
                    temp[1] = aDataForSuggestions.get(prech);
                    temp[2] = prech;
                    aCursorSuggestionsForSearch.addRow(temp);
                    i++;
                }


            }

            // Alternatively load data from database
            //Cursor aCursorSuggestionsForSearch = db.rawQuery("SELECT * FROM table_name", null);
            if (!aSuggAdWasSet) {
                SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

                final SearchView search = (SearchView) aMenu.findItem(R.id.action_search).getActionView();

                //search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

                search.setSuggestionsAdapter(new ExampleAdapter(this, aCursorSuggestionsForSearch));
            }

        }
    }

    private void zobrazMarkerInfo(Marker paMarker) {
        if (aMyCurrentLocation != null) {
            if (mMap != null) {
                RoadPoint mojaPozVrchol = aNavigator.najdiHranuNaKtoruMoznoUmiestnitMojuPolohu(aMyCurrentLocation);
                if (mojaPozVrchol != null) {

                    RoadPoint.Suradnica s = new RoadPoint.Suradnica(new RoadPoint.Coordinate(paMarker.getPosition().latitude),
                            new RoadPoint.Coordinate(paMarker.getPosition().longitude));
                    aVybranyBodNaMape = new RoadPoint(s, RoadPoint.TypBodu.BODCESTYPRENAVIGACIUDEFAULT);
                    List<RoadPoint> cesta = aNavigator.najdiNajkratsiuCestu(mojaPozVrchol, aVybranyBodNaMape);
                    LinearLayout ll = (LinearLayout) ZooMapActivity.this.findViewById(R.id.actions_navig);
                    ll.setVisibility(View.VISIBLE);
                    vypisVzdialenostOdMojejPozicie(cesta);
                }

            }

        }
        //KONIEC
    }

    private void inicializujUdajePreNavigovanie() {
        File f = new File(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE + Info_Dirs_File_ENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD);
        if (f.exists()) {
            FileWorker fw = new FileWorker(f.getAbsolutePath());
            String loadedText = null;
            aCesta = new Road();

            try {
                loadedText = fw.readDataFromFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (loadedText != null) {
                String body[] = loadedText.split("\\|\\|");

                String bodSoSusedmi[] = null;
                for (int i = 0; i < body.length; i++) {
                    if (!body[i].equals("\n")) {
                        bodSoSusedmi = body[i].split("\\|");
                        String coordinatLat = bodSoSusedmi[0].split(";;;")[0];
                        String coordinatLon = bodSoSusedmi[0].split(";;;")[1];
                        String lat[] = coordinatLat.split(":");
                        String lon[] = coordinatLon.split(":");
                        RoadPoint.Coordinate coordLat = new RoadPoint.Coordinate(lat[0], lat[1], lat[2]);
                        RoadPoint.Coordinate coordLon = new RoadPoint.Coordinate(lon[0], lon[1], lon[2]);
                        RoadPoint novyBod = new RoadPoint(new RoadPoint.Suradnica(coordLat, coordLon), RoadPoint.TypBodu.BODCESTYPRENAVIGACIUDEFAULT);
                        aCesta.pridajNovyBodNaCestu(novyBod);
                    }

                }
                double vzdialenost = 0;
                for (int i = 0; i < body.length; i++) {
                    if (!body[i].equals("\n")) {
                        bodSoSusedmi = body[i].split("\\|");
                        String coordinatLat = bodSoSusedmi[0].split(";;;")[0];
                        String coordinatLon = bodSoSusedmi[0].split(";;;")[1];

                        String lat[] = coordinatLat.split(":");
                        String lon[] = coordinatLon.split(":");
                        RoadPoint.Coordinate coordLat = new RoadPoint.Coordinate(lat[0], lat[1], lat[2]);
                        RoadPoint.Coordinate coordLon = new RoadPoint.Coordinate(lon[0], lon[1], lon[2]);
                        RoadPoint bodKuKtoremuPridavamSusedov = aCesta.getBodNaCeste(new RoadPoint.Suradnica(coordLat, coordLon));
                        for (int j = 1; j < bodSoSusedmi.length; j++) {
                            coordinatLat = bodSoSusedmi[j].split(";;;")[0];
                            coordinatLon = bodSoSusedmi[j].split(";;;")[1];
                            vzdialenost = Double.parseDouble(bodSoSusedmi[j].split(";;;")[2]);
                            lat = coordinatLat.split(":");
                            lon = coordinatLon.split(":");
                            coordLat = new RoadPoint.Coordinate(lat[0], lat[1], lat[2]);
                            coordLon = new RoadPoint.Coordinate(lon[0], lon[1], lon[2]);
                            RoadPoint sused = aCesta.getBodNaCeste(new RoadPoint.Suradnica(coordLat, coordLon));
                            aCesta.pridajSusedaZadanemuBodu(bodKuKtoremuPridavamSusedov, sused, vzdialenost);
                        }
                    }

                }
            }
        }
    }

    private void animateMarker(final Marker marker, final LatLng toPosition,
                               final boolean hideMarker) {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = DURATION_OF_ANIMATION_MOVE_MARKER;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                        marker.setPosition(toPosition);
                        zobrazMarkerInfo(marker);
                    }
                }
            }
        });
    }


    private class ExampleAdapter extends CursorAdapter implements Serializable {

        //private List items;

        private TextView text;

        public ExampleAdapter(Context context, Cursor cursor) {

            super(context, cursor, false);
        }


        @Override
        public Object getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            // Show list searchsuggestionsitem data from aCursorSuggestionsForSearch
            int cursorPos = cursor.getPosition();
            text.setText((String) cursor.getString(cursor.getColumnIndex("name_of_object")));

            // Alternatively show data direct from database
            //text.setText(aCursorSuggestionsForSearch.getString(aCursorSuggestionsForSearch.getColumnIndex("column_name")));

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.searchsuggestionsitem, parent, false);

            text = (TextView) view.findViewById(R.id.text);

            return view;

        }


    }


    private class PridaneObjektyMapy implements Serializable {
        private ArrayList<Polyline> aZobrazeneCesty;
        private ArrayList<Marker> aZobrazeneZnacky;
        private ArrayList<Marker> aZobrazeneZnackyKategoriaObjektov;
        private String aZobrazeneObjektyKategoria;

        private PridaneObjektyMapy() {
            this.aZobrazeneCesty = new ArrayList<>();
            this.aZobrazeneZnacky = new ArrayList<>();
            aZobrazeneZnackyKategoriaObjektov = new ArrayList<>();
            aZobrazeneObjektyKategoria = null;
        }

        private void setaZobrazeneObjektyKategoria(String pakatZobrObjekty) {
            aZobrazeneObjektyKategoria = pakatZobrObjekty;
        }

        private String getaZobrazeneObjektyKategoria() {
            return aZobrazeneObjektyKategoria;
        }

        private void pridajZnackuZvolKategorie(Marker paPridmarker) {
            aZobrazeneZnackyKategoriaObjektov.add(paPridmarker);
        }

        private void pridajZnacku(Marker paPridavanymarker) {
            aZobrazeneZnacky.add(paPridavanymarker);
        }

        private void pridajCestu(Polyline papridavanaCesta) {
            odstranVsetkyCesty();
            aZobrazeneCesty.add(papridavanaCesta);
        }

        private void odstranVsetkyCesty() {
            for (int i = 0; i < aZobrazeneCesty.size(); i++) {
                aZobrazeneCesty.get(i).remove();
            }
            aZobrazeneCesty.clear();
        }

        private void odstranZnackzZvolKategorie() {
            for (Marker prech : aZobrazeneZnackyKategoriaObjektov) {
                prech.remove();
            }
            aZobrazeneZnackyKategoriaObjektov.clear();
        }

        private void odstranVsetkyZnacky() {
            for (int i = 0; i < aZobrazeneZnacky.size(); i++) {
                aZobrazeneZnacky.get(i).remove();
            }
            aZobrazeneZnacky.clear();
        }


        private HashMap<String, RoadPoint.Suradnica> getSuradniceZobrazenychZnaciek() {
            HashMap<String, RoadPoint.Suradnica> ret = new HashMap<>();
            for (int i = 0; i < aZobrazeneZnacky.size(); i++) {
                RoadPoint.Coordinate lat = new RoadPoint.Coordinate(aZobrazeneZnacky.get(i).getPosition().latitude);
                RoadPoint.Coordinate lon = new RoadPoint.Coordinate(aZobrazeneZnacky.get(i).getPosition().longitude);
                RoadPoint.Suradnica s = new RoadPoint.Suradnica(lat, lon);
                ret.put(aZobrazeneZnacky.get(i).getTitle(), s);
            }
            return ret;
        }

        private void getZobrazeneCesty() {

        }
    }
    //-----------------------------------------------------*******************************-----------------------------------------
    /**
     * PRE editovanie rozmerov a kde sa ma zobrazit obrazkova mapa- Odkomentuj aj v xml subore tlacidla
     *
     * // pre vylepsenie pozicie obrazka
     private boolean zvysujem=true;
     private float menimO=0.0001f;
     private float southWestlat=48.156136f;
     private float southWestlon=17.062258f;

     private float northEastlat=48.163998f;
     private float northEastlon= 17.081198f;

     private float rotate =-84.7f;
     private float rotujemO=0.1f;
     private GroundOverlay imageOverlay;
     // Koniec vylepsenia obrazka
     *
     public void prepniZvysO(View v)
     {
     if(v.getId()==R.id.plus)
     {
     zvysujem=true;
     }
     else
     {
     zvysujem=false;
     }

     if(zvysujem==true)
     {

     Toast.makeText(this,"zvysim o:"+menimO,Toast.LENGTH_SHORT).show();
     }
     else
     {

     Toast.makeText(this,"zmensim o:"+menimO,Toast.LENGTH_SHORT).show();
     }


     }

     public void zmenOverlayMapy(View v)
     {

     switch (v.getId())
     {
     case R.id.sv:
     if(zvysujem)
     northEastlon+=menimO;
     else
     northEastlon-=menimO;

     break;
     case R.id.s:
     if(zvysujem)
     northEastlat+=menimO;
     else
     northEastlat-=menimO;
     break;
     case R.id.jz:
     if(zvysujem)
     southWestlon+=menimO;
     else
     southWestlon-=menimO;
     break;
     case R.id.j:
     if(zvysujem)
     southWestlat+=menimO;
     else
     {
     southWestlat-=menimO;
     }
     break;
     case R.id.r:
     if(zvysujem)
     rotate+=rotujemO;
     else
     {
     rotate-=rotujemO;
     }


     }
     if(v.getId()!=R.id.r) {
     LatLngBounds newarkBounds = new LatLngBounds(
     new LatLng(southWestlat, southWestlon),       // South west corner
     new LatLng(northEastlat, northEastlon));
     Log.e("MyLogSouthWESTLONLAT",southWestlon+", "+southWestlat);
     Log.e("MyLogNORTHEASTLONLAT",northEastlon+", "+northEastlat);
     imageOverlay.setPositionFromBounds(newarkBounds);
     }
     else
     {
     imageOverlay.setBearing(rotate);
     Log.e("MyLogLonLat Rotation: ",""+rotate);
     }
     }*/
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy.Objekty;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import java.io.PrintWriter;

/**
 *
 * @author Dominik
 */
public class Kniha implements IObjekt, Serializable {

    private static boolean navrat;//na zaklade stlacenia tlacidla v modifikacnom okne sa nastavi atribut navrat
    private String autor;
    private String nazov;
    private int rokvydania;//rok narodenia

    /**
     * Metóda nastaví hodnoty atribútov objektu. Hodnoty budú načitané z okna,
     * ktoré pouźívateľ vyplní.
     *
     * @return
     */
    @Override
    public boolean napln() {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 200);
        ramik.setResizable(false);
        ramik.setTitle("Kniha  *  naplnenie");

        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(4, 2, 5, 10));

        //	so  vstupnými poľami
        final JTextField txtAutor = new JTextField();
        final JTextField txtNazov = new JTextField();
        final JTextField txtRok = new JTextField();

        // 	a dvoma tlačidlami
        JButton btUlozit = new JButton("Uložiť");
        btUlozit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    autor = txtAutor.getText().trim();
                    if (autor.length() < 2) {
                        throw new IllegalArgumentException(
                                "\n     neprípustná dĺžka   / min. 2 znaky /");
                    }

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka autor", JOptionPane.ERROR_MESSAGE);
                    txtAutor.requestFocus();
                    return;
                }

                try {
                    nazov = txtNazov.getText().trim();
                    if (nazov.length() < 2) {
                        throw new IllegalArgumentException("\n neprípustná dĺžka   / min. 2 znaky /");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka nazov", JOptionPane.ERROR_MESSAGE);
                    txtNazov.requestFocus();
                    return;
                }

                try {
                    rokvydania = Integer.parseInt(txtRok.getText().trim());
                    if (rokvydania < 1901 || rokvydania > 2099) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný rozsah / 1901-2099 /");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null,
                            ex + "\n     údaj musí byť celočíselný",
                            "Chyba roku vydania", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba roku vydania", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                }

                navrat = true;
                ramik.dispose();

            }
        });

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                navrat = false;
                ramik.dispose();

            }
        });

        //	a popisnými poľami,    a všetko poskladáme
        ramik.add(new JLabel("   Autor      :  ", JLabel.RIGHT));
        ramik.add(txtAutor);
        ramik.add(new JLabel("   Nazov            :  ", JLabel.RIGHT));
        ramik.add(txtNazov);
        ramik.add(new JLabel("   Rok vydania   :  ", JLabel.RIGHT));
        ramik.add(txtRok);

        ramik.add(btUlozit);
        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda zobrazí okno na načitanie hodnôt atribútorv objektu a na základe
     * zadanýcho hodnôt nastaví atribúty tohto objektu.
     *
     * @return boolean - true ak načítalo, false ak nenačitalo
     */
    @Override
    public boolean zobraz() {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 200);
        ramik.setResizable(false);
        ramik.setTitle("Kniha  *  Udaje");
        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(4, 2, 5, 10));

        ramik.add(new JLabel("   Autor     :  ", JLabel.RIGHT));
        ramik.add(new JLabel(nazov));
        ramik.add(new JLabel("   Nazov           :  ", JLabel.RIGHT));
        ramik.add(new JLabel(autor));
        ramik.add(new JLabel("   Rok vydania   :  ", JLabel.RIGHT));
        ramik.add(new JLabel(rokvydania + ""));

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                navrat = false;
                ramik.dispose();
            }
        });

        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda načita hodnoty atribúto zo súboru, ktorý bol predtým uložený
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param panazov-názov súboru z ktorého sa má načítať
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void nacitajZoSuboru(File paPodadresar, String panazov) throws FileNotFoundException {
        File file = new File(paPodadresar, panazov);
        Scanner scan = new Scanner(file);

        autor = scan.nextLine();
        nazov = scan.nextLine();
        rokvydania = scan.nextInt();

        scan.close();
    }

    /**
     * Metóda zapíše hodnoty atribútov objektu do súboru, ktorý budeme neskôr
     * používať pre načitanie objektu zo súboru
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param paCisloSuboru-poradie uloženia súboru
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void zapisDoSuboru(File paPodadresar, int paCisloSuboru) throws FileNotFoundException {

        File subor = new File(paPodadresar, "K" + (paCisloSuboru + 1000) + ".txt");
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(subor); // otvoríme súbor C:/text1.txt na zápis. Ak v ňom niečo bolo, tak sa to vymaže.
            pw.println(autor); // napíšeme do súboru "Jano" a čakáme na ďalší zápis konci tohto riadku
            pw.println(nazov); // napíšeme do súboru "Javák", ideme do nového riadku a čakáme
            pw.println(rokvydania); // ideme do nového riadku a čakáme

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("SDo suboru sa nepodarilo ulozit objekt. Kniha Zapis");
        } finally {
            if (pw != null) {
                pw.close(); // uložíme a zavrieme súbor C:/text1.txt
            }
        }
    }

    /**
     * Metóda vráti vo forme Stringu popis objektu Kniha
     *
     * @return String-kniha popis
     */
    @Override
    public String toString() {
        String s = "";
        s += "Objekt: Kniha, " + "Nazov: " + nazov + ", Autor: " + autor + ", Rok vydania: " + rokvydania;
        return s;
    }
}

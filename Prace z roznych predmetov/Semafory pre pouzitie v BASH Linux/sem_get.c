#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

//FUNKCIA MAIN
//*************************************************
/// Aplikacia pre vytvorenie semafora
/// Prikaz musi byt v tvare nazov programu,hodnota
/// Hodnota predstavuju argument
/// Hodnota predstavuje cislo ktoreho hodnotu nadobudne semafor pri vytvoreni
/// \param argv predstavuje pole argumentov teda pozostava z hodnoty.
/// \return id_semafora v pripade uspesneho ukoncenia, -1 v pripade chyby.
int main(int argc, char *argv[])
{
int pociatocnaHodnotaSemafora = atoi(argv[1]);
//printf("i:  %d  \n",pociatocnaHodnotaSemafora);na debugovanie

int pocetVytvorenychSemaforov= 1;
key_t klucSemafora=getpid();
int idSemafora = semget(klucSemafora,pocetVytvorenychSemaforov,0666 | IPC_CREAT);
  if (idSemafora < 0){
        printf ("chyba pri vytarani semafora. Exit. \n");
        return -1;
    }
printf("%d\n",idSemafora);
int nastavilaSa = semctl(idSemafora,0,SETVAL,pociatocnaHodnotaSemafora);
if(nastavilaSa<0)
{
printf ("chyba pri nastavovani pociatocnej hodnoty semafora. Semafor bude zmazany.Exit. \n");
semctl(idSemafora,0,IPC_RMID);
return -1;
}
//printf("Hodnota semafora:  %d  \n",semctl(idSemafora,0,GETVAL));

return idSemafora;
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skokdodialky;

import java.util.Comparator;

/**
 *
 * @author Dominik
 */
public class RiadokTabulky {
    private String aMenoSportovca;
    private double aDlzkaSkoku;
    public static Comparator<RiadokTabulky> podlaMena=new Comparator<RiadokTabulky>() {

        @Override
        public int compare(RiadokTabulky o1, RiadokTabulky o2) {
            return o1.aMenoSportovca.compareTo(o2.aMenoSportovca);
        }
    };
    
    public static Comparator<RiadokTabulky> podlaSkoku=new Comparator<RiadokTabulky>() {

        @Override
        public int compare(RiadokTabulky o1, RiadokTabulky o2) {
            if(o1.aDlzkaSkoku<o2.aDlzkaSkoku)
            {
                return 1;
            }
            else if(o1.aDlzkaSkoku>o2.aDlzkaSkoku)
            {
                return -1;
            }
            return 0;
        }
    };

    public RiadokTabulky(String aMenoSportovca, double aDlzkaSkoku) {
        this.aMenoSportovca = aMenoSportovca;
        this.aDlzkaSkoku = aDlzkaSkoku;
    }

    public String getaMenoSportovca() {
        return aMenoSportovca;
    }

    public double getaDlzkaSkoku() {
        return aDlzkaSkoku;
    }
    
    
    
    public enum NazvyStlpcov
    {
        SPORTOVECMENO("Meno sportovca"){

            @Override
            public Object dajHodnotu(RiadokTabulky paRiadok) {
                return paRiadok.aMenoSportovca;
            }
            
        },
        SPORTOVECSKOK("Dlzka skoku"){

            @Override
            public Object dajHodnotu(RiadokTabulky paRiadok) {
                return paRiadok.aDlzkaSkoku;
            }
            
        };
        private String aStlpecNazov;

        private NazvyStlpcov(String aStlpecNazov) {
            this.aStlpecNazov = aStlpecNazov;
        }
        
        public abstract Object dajHodnotu(RiadokTabulky paRiadok);

        public String getaStlpecNazov() {
            return aStlpecNazov;
        }
        
        
    }
}

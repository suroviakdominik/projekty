/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Trieda, ktorej instancie predstavuju body cesty
 * @author Dominik
 */
public class BodNaCeste implements Serializable{

    private Coordinate aLatitude;
    private Coordinate aLongitude;
    private final List<Sused> aSusedneBodyTohtoBodu;
    private TypBodu aTypBodu=TypBodu.BODCESTYPRENAVIGACIUDEFAULT;
    private static final long serialVersionUID = 5L;
    

    /**
     * Vytvori novy bod, ktory je mozne umiestnit do cesty
     * @param aLatitude- zemepisna sirka noveho bodu
     * @param aLongitude- zemepisna dlzka noveho bodu
     * @param paTypBodu - typ bodu
     */
    public BodNaCeste(Coordinate aLatitude, Coordinate aLongitude,TypBodu paTypBodu) {
        this.aLatitude = aLatitude;
        this.aLongitude = aLongitude;
        aSusedneBodyTohtoBodu = new ArrayList<>();
        aTypBodu=paTypBodu;
    }

    /**
     * 
     * @return typ bodu 
     */
    public TypBodu getTypBodu()
    {
        return aTypBodu;
    }
    
    /**
     * nastav typ bodu
     * @param paTypBodu - typ bodu, ktory sa nastavi 
     */
    public void setTypBodu(TypBodu paTypBodu)
    {
        aTypBodu=paTypBodu;
    }
    
    /**
     * 
     * @return  zemepisna sirka
     */
    public Coordinate getaLatitude() {
        return aLatitude;
    }

    /**
     * 
     * @return zemepisna dlzka
     */
    public Coordinate getaLongitude() {
        return aLongitude;
    }

    /**
     * 
     * @return pocet susedov tohto
     */
    public int getPocetSusedov() {
        return aSusedneBodyTohtoBodu.size();
    }

   /**
    * vrati suedny bod nachadzjuci sa na indexe zadanom ako parameter
    * @param paIndexSusedaVPoli - index suseda v poli
    * @return bodNa ceste, ktory je susedom tohto bodu na indexe param
    */
    public BodNaCeste getSusednyBod(int paIndexSusedaVPoli) {

        return aSusedneBodyTohtoBodu.get(paIndexSusedaVPoli).getSusednyBod();

    }
    
    /**
     * vrati objekt typu sused
     * @param paIndexSusedaVPoli-index v poli kde sa ma sused nachadzat
     * @return - objekt typu Sused nachadzajuci sa na indexe param
     */
    public Sused getSused(int paIndexSusedaVPoli)
    {
        return aSusedneBodyTohtoBodu.get(paIndexSusedaVPoli);
    }

    /**
     * nastav zemepisnu sirku
     * @param aLatitude -zemepisna sirka 
     */
    public void setLatitude(Coordinate aLatitude) {
        this.aLatitude = aLatitude;
    }

    /**
     * nastav zemepisnu dlzku
     * @param aLatitude -zemepisnu dlzka 
     */
    public void setLongitude(Coordinate aLongitude) {
        this.aLongitude = aLongitude;
    }

    /**
     * prida noveho suseda tomuto bodu
     * @param paNovySused - sused, ktory bude pridany tomuto bodu
     */
    public void pridajNovySusednyBod(BodNaCeste paNovySused) {
        Sused lpSused=new Sused(paNovySused);
        if (!paNovySused.equals(this)&&!aSusedneBodyTohtoBodu.contains(lpSused)) {
            aSusedneBodyTohtoBodu.add(lpSused);
            System.out.println("DEBUG: Novy sused bodu pridany");
        } else {
            System.out.println("DEBUG: Pridavany sused uz je susedom tohto bodu.");
        }
    }

    /**
     * odstrani suseda zadaneho ako parameter
     * @param paSusedNaOdstranenie - sused na odstranenie
     */
    public void odstranSusedstvo(Sused paSusedNaOdstranenie) {
        aSusedneBodyTohtoBodu.remove(paSusedNaOdstranenie);
        System.out.println("DEBUG: Sused tohto bodu bol úspešne odtránený.");
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.aLatitude);
        hash = 41 * hash + Objects.hashCode(this.aLongitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BodNaCeste other = (BodNaCeste) obj;
        if (!Objects.equals(this.aLatitude, other.aLatitude)) {
            return false;
        }
        return Objects.equals(this.aLongitude, other.aLongitude);
    }

   
    /**
     * vrati list vsetkych susedov tohto bodu
     * @return susedia tohto bodu
     */
    public List<Sused> getSusedov()
    {
        List<Sused> surSused= new ArrayList<>();
        for(Sused prech:aSusedneBodyTohtoBodu)
        {
            surSused.add(prech);
        }
        return surSused;
    }

    @Override
    public String toString() {
        return "Lat: "+aLatitude+"; Long: "+aLongitude;
    }
    
    /**
     * ENUM predstavujuci vsetky mozne typy bodov
     */
    public enum TypBodu implements Serializable
    {
        OBJEKTALEBOZVIERA,
        BODCESTYPRENAVIGACIUDEFAULT,
        BODCESTYVYTVORENYPOUZIVATELOM;
        private static final long serialVersionUID = 7L;
    }
    
    

}

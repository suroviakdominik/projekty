/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidencianehnutelnosti;

import dynamickehasovanie.data.GPSPozicia;
import dynamickehasovanie.data.Zaznam;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Dominik
 */
public class GPSTableModel extends AbstractTableModel {

    private GPSPozicia[] aGPS;
    private String[] aColumnNames = {"Platnost","Zemepisná dĺžka","Pologuľa dĺžka","Zemepisná šírka","Pologuľa šírka"};

    public GPSTableModel(GPSPozicia[] paGPS) {
        aGPS = paGPS;
    }

    @Override
    public int getRowCount() {
        return aGPS.length;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }
    
    public List<GPSPozicia> getGPSPozicie(){
        return Arrays.<GPSPozicia>asList(aGPS);
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return aGPS[rowIndex].getPlatnostPopis();
            case 1:
                return aGPS[rowIndex].getaZemepisnaDlzkaHodnota();
            case 2:
                return  aGPS[rowIndex].getaZemepisnaDlzka();
            case 3:
                 return aGPS[rowIndex].getaZemSirkaHodnota();
            case 4:
                 return  aGPS[rowIndex].getaZemepisnaSirka();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return aColumnNames[column];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                aGPS[rowIndex].setPlatny((Zaznam.Platnost)aValue);
                break;
            case 1:
                aGPS[rowIndex].setaZemepisnaDlzkaHodnota((Double)aValue);
                break;
            case 2:
                aGPS[rowIndex].setaZemepisnaDlzka((Character)aValue);
                break;
            case 3:
                 aGPS[rowIndex].setaZemSirkaHodnota((Double)aValue);
                break;
            case 4:
                 aGPS[rowIndex].setaZemepisnaSirka((Character)aValue);
                break;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    
}

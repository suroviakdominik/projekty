/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import Grafika.Obdlznik;

/**
 * Trieda Veza je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Veza extends Figurka {

    private IGrafickeObjekty[] aGrafickeObjektyVeze;

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba novej veže
     */
    public Veza(String paFarba) {
        super("Veza", paFarba);
        aGrafickeObjektyVeze = new IGrafickeObjekty[5];
    }

    /**
     * je metóda, ktorá vykreslí vežu na plátno.
     */
    @Override
    public void nakresli() {
        aGrafickeObjektyVeze[0] = new Obdlznik(30, 10, this.getX(), this.getY() + 10, this.getFarba());
        aGrafickeObjektyVeze[1] = new Obdlznik(20, 10, this.getX() + 5, this.getY(), this.getFarba());
        aGrafickeObjektyVeze[2] = new Obdlznik(5, 10, this.getX() + 5, this.getY() - 10, this.getFarba());
        aGrafickeObjektyVeze[3] = new Obdlznik(3, 10, this.getX() + 14, this.getY() - 10, this.getFarba());
        aGrafickeObjektyVeze[4] = new Obdlznik(5, 10, this.getX() + 20, this.getY() - 10, this.getFarba());
        for (int i = 0; i < 5; i++) {
            aGrafickeObjektyVeze[i].zobraz();
        }
    }

    /**
     * je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie veže.
     *
     * @return grafické objekty z ktorých sa skladá veža
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aGrafickeObjektyVeze;
    }

    /**
     * metóda vracajúca popis veže.
     *
     * @return popis veže vo forme String
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Veža" + "\n";
        popis += "Pohyb: po riadkoch a stĺpcoch" + "\n";
        popis += "Vyhadzovanie: po riadkoch a stĺpcoch" + "\n";
        popis += "Ine: pri pohybe a vyhadzovaní nesmie prejsť cez/ponad inú figúrku" + "\n";
        return popis;
    }

}

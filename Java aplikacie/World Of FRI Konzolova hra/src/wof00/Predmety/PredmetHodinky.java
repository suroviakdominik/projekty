/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;
import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author suroviak3
 */
public class PredmetHodinky implements IPredmet, Serializable {

    String aNazov;

    public PredmetHodinky(String paNazov) {
        this.aNazov = paNazov;
    }

    @Override
    public String getPopis() {
        Date cas = new Date();
        DateFormat formatcasu = DateFormat.getTimeInstance();
        return "Prave je: " + formatcasu.format(cas);
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String pouzi(Hrac paHrac) {
        return getPopis();
    }
}

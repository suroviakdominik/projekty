/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import EditorsForInfo_Files.IOperacieEDITORA;
import EditorsForInfo_Files.Info_File_NEWS_EDITOR;
import EditorsForInfo_Files.Info_File_ANIMALS_EDITOR;
import EditorsForInfo_Files.Info_File_CATEGORIES_EDITOR;
import EditorsForInfo_Files.Info_File_EVENTS_EDITOR;
import EditorsForInfo_Files.Info_File_INFO_EDITOR;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import java.io.File;

/**
 * Enum obsahujuci konfiguracne data aplikacie
 * @author Dominik
 */
public enum  InfoSuboryENUM{
    //!!!!!!!!!!!!!!!!!!!NIC CO TU UZ JE, NEMENIT, IBA AK BY STE SA ROZHODLI SUBORY VYTVARAT OD ZACIATKU(PRE NOVU ZOO), ALEBO MOZNE PRIDAT NOVY INFO SUBOR!!!!!!!!!!!!!!!!!!!!!!!!!
    //!!!!!!!!!!!!!!!!!!!NIC CO TU UZ JE, NEMENIT, IBA AK BY STE SA ROZHODLI SUBORY VYTVARAT OD ZACIATKU(PRE NOVU ZOO), ALEBO MOZNE PRIDAT NOVY INFO SUBOR!!!!!!!!!!!!!!!!!!!!!!!!!
    NEWS("NEWS","info_about_file_news","N","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//N-news
    ANIMALS("ANIMALS","info_about_file_animals","A","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//A-animals
    CATEGORIES("CATEGORIES","info_about_file_categories","C","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//C-categories
    EVENTS("EVENTS","info_about_file_events","E","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//E-events
    INFO("INFO","info_about_file_info","I","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/"),//I-info about zoo
    OBJECTS("OBJECTS","info_about_file_objects","O","Files/","http://st.fri.uniza.sk/~suroviak3/ZooBratislava/");//O-objects
    
    
    private final String aKeyName;
    private final String aNameOfInfoFile;
    private final String aSignOfDir;
    private final String aRelativePathOfDirecotryWhereIAM;
    private final String aServerDIRDomainOfThisFile;
    public static final String  ROOT_DEVICE_FOLDER ="./";//for work
    public static final String  TEMPORARY_DEVICE_FOLDER=ROOT_DEVICE_FOLDER+"TEMP/";//for update
    public static final String aEndOfInfoFile = "i";
    public static String aNameOfNetworkDriveThatIseConnectToServerDomain="Z:/";
    public static final String aPathInNetworkDrive="public_html/ZooBratislava/";
    public static final String RELATIVE_PATH_OF_OFFLINE_MAP_FILE="Files/";
    public static final String NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE = "cesta.bin";
    public static final String NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_ANDROID = "cesta.and";
    public static final String ADRESS_OFFLINE_MAP_FILE_ON_SERVER="http://st.fri.uniza.sk/~suroviak3/ZooBratislava/Files/cesta.bin";
    public static final String NAME_OF_FILE_WITH_DESCRIPTION="popis.txt";
    public static final String NAME_OF_PICTURE_FILE="picture";
    public static final String FILE_SEPARATOR="/";
    
    public static final double MIN_LAT_DEGREE=48;
    public static final double MIN_LAT_MINUTES=9;
    public static final double MIN_LAT_SECONDS=12;
    public static final double MAX_LAT_DEGREE=48;
    public static final double MAX_LAT_MINUTES=10;
    public static final double MAX_LAT_SECONDS=00;
    public static final double MIN_LAT = MIN_LAT_DEGREE+MIN_LAT_MINUTES/60+MIN_LAT_SECONDS/3600;
    public static final double MAX_LAT = MAX_LAT_DEGREE+MAX_LAT_MINUTES/60+MAX_LAT_SECONDS/3600;
    
    public static final double MIN_LON_DEGREE=17;
    public static final double MIN_LON_MINUTES=3;
    public static final double MIN_LON_SECONDS=53;
    public static final double MAX_LON_DEGREE=17;
    public static final double MAX_LON_MINUTES=4;
    public static final double MAX_LON_SECONDS=44;
    public static final double MIN_LON = MIN_LON_DEGREE+MIN_LON_MINUTES/60+MIN_LON_SECONDS/3600;
    public static final double MAX_LON = MAX_LON_DEGREE+MAX_LON_MINUTES/60+MAX_LON_SECONDS/3600;
    
    /*public static final double MIN_LAT = 48.155555555555;
    public static final double MAX_LAT = 48.166666666666;
    public static final double MIN_LON = 17.064825;
    public static final double MAX_LON = 17.077777777777;*/
   
    private InfoSuboryENUM(String paKeyName, String paNameOfFile,String paSignOfDir,String paPathOfDirecotryWhereIAM,String paServerDomainOfThisFile)
    {
        aKeyName=paKeyName;
        aNameOfInfoFile = paNameOfFile;
        aSignOfDir=paSignOfDir;
        aRelativePathOfDirecotryWhereIAM=paPathOfDirecotryWhereIAM;
        aServerDIRDomainOfThisFile=paServerDomainOfThisFile;
        File f=new File(ROOT_DEVICE_FOLDER+aRelativePathOfDirecotryWhereIAM);
        if(!f.exists())
        {
            f.mkdirs();
        }
        f=new File(TEMPORARY_DEVICE_FOLDER);
        if(!f.exists())
        {
            f.mkdirs();
        }
        
    }
    
    public static void setNetworkDrive(String paDrive)
    {
        aNameOfNetworkDriveThatIseConnectToServerDomain=paDrive;
    }
    
    public IOperacieEDITORA getInstanceOfEditorWithNewInfoFile()//metoda iba pre PC
    {
        switch(aKeyName)
        {
            case "CATEGORIES":
                return new Info_File_CATEGORIES_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
               
            case "ANIMALS":
                return new Info_File_ANIMALS_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
                       
            case "NEWS":
                return new Info_File_NEWS_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
                       
            case "EVENTS":
                return new Info_File_EVENTS_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
                       
            case "INFO":
                return new Info_File_INFO_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
            case "OBJECTS":
                return new Info_File_OBJECTS_EDITOR(new Info_About_Dirs_File(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile, aRelativePathOfDirecotryWhereIAM,aSignOfDir));
                       
           default: return null;
        }
    }
    
    public IOperacieEDITORA getInstanceOfEditorWithExistingInfoFile(Info_About_Dirs_File paInfoFile)//metoda iba pre PC
    {
        paInfoFile.setDomainOfThisFile(aServerDIRDomainOfThisFile+aRelativePathOfDirecotryWhereIAM+aNameOfInfoFile+"."+aEndOfInfoFile);
        switch(aKeyName)
        {
            case "CATEGORIES":
                
                return new Info_File_CATEGORIES_EDITOR(paInfoFile);
                
            case "ANIMALS":
                return new Info_File_ANIMALS_EDITOR(paInfoFile);
                       
            case "NEWS":
                return new Info_File_NEWS_EDITOR(paInfoFile);
                       
            case "EVENTS":
                return new Info_File_EVENTS_EDITOR(paInfoFile);
                       
            case "INFO":
                return new Info_File_INFO_EDITOR(paInfoFile);
            case "OBJECTS":
                return new Info_File_OBJECTS_EDITOR(paInfoFile);
                       
           default: return null;
        }
        
    }

    public String getaNameOfInfoFile() {
        return aNameOfInfoFile;
    }

    public String getKeyName() {
        return aKeyName;
    }

    public String getSignOfDir() {
        return aSignOfDir;
    }

    public String getaRelativePathOfDirecotryWhereIAM() {
        return aRelativePathOfDirecotryWhereIAM;
    }

    public String getServerDIRDomainOfThisFile() {
        return aServerDIRDomainOfThisFile;
    }
    
    
    
    
}

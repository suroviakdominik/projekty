﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeServer
{
    public partial class Form1 : Form
    {
        ServerNetworking aServerLidgren;
        public Form1()
        {
            InitializeComponent();
        }

        protected virtual void MyStartMenuEventHandler(object sender, EventArgs e)
        {
            ServerNetworking.MessProcEventArgs even = (ServerNetworking.MessProcEventArgs)e;
            AppendTextBox(even.SelOption);
        }

        public void AppendTextBox(string value)
        {
            if (InvokeRequired)
            {
                try
                {
                    this.Invoke(new Action<string>(AppendTextBox), new object[] { value });
                }
                catch (Exception)
                {
                    Console.WriteLine("INFO: Vlakno listenServer skoncilo skor ako bolo nieco vypisane.");
                }
                return;
            }
            try
            {
                textBox2.AppendText(value);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Vlakno, ktore posiela data na vypis bolo ukoncene skor ako sa podarilo urobit posledny vypis:" +ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            aServerLidgren = new ServerNetworking(Int32.Parse(textBox1.Text));
            aServerLidgren.aNewMessageProcessed += new EventHandler<ServerNetworking.MessProcEventArgs>(MyStartMenuEventHandler);
            aServerLidgren.startServer();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            aServerLidgren.stopServer();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            aServerLidgren.startListening(1);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = checkBox1.Checked;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
        }
    }
}

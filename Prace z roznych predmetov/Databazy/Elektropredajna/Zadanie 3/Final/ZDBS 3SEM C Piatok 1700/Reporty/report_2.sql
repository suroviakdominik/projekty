set pagesize 60
set linesize 123

column faktura.cislo_FA  heading "cislo faktury";
column tovar.nazov heading "Nazov Tovaru";
column tova.katalogoveCislo heading "Katalogove Cislo"



ttitle left datum center 'Vypis tovarov, u ktorych je potrebne znizit cenu : ' right;
column datum new_value datum noprint



SELECT  Faktura.CISLO_FA,nazov,Tovar.katalogovecislo,count(*)As Pocet,Kategoria.MIN_POCET FROM 
Tovar,Kategoria,VYFAKTUROVANYTOVAR,Faktura
where Kategoria.id_kategorie=Tovar.id_kategorie
AND Tovar.KATALOGOVECISLO = VYFAKTUROVANYTOVAR.KATALOGOVECISLO
AND Faktura.CISLO_FA=VYFAKTUROVANYTOVAR.CISLO_FA
AND add_months(sysdate,-12)<datum_vystavenia
GROUP by Faktura.CISLO_FA,nazov,Tovar.katalogovecislo,Kategoria.MIN_POCET
having count(*) < Kategoria.MIN_POCET;

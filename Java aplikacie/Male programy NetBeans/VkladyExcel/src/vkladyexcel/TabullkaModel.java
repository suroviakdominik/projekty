/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vkladyexcel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Dominik
 */
public class TabullkaModel extends AbstractTableModel{
    private List<RiadokTabulky> aRiadkyTabulky;
    private Double sumaVkladov=0d;

    public TabullkaModel() {
        aRiadkyTabulky=new ArrayList<>();
    }

    public TabullkaModel(List<RiadokTabulky> aRiadkyTabulky) {
        this.aRiadkyTabulky = aRiadkyTabulky;
        vypocitajSucetVkladov();
    }
    
    private void vypocitajSucetVkladov()
    {
        sumaVkladov=0d;
        for (int i = 0; i < aRiadkyTabulky.size(); i++) {
            sumaVkladov+=aRiadkyTabulky.get(i).getaVyskaVkladu();
        }
    }
    
    @Override
    public int getRowCount() {
        return aRiadkyTabulky.size();
    }

    @Override
    public int getColumnCount() {
        return RiadokTabulky.NazvyStlpcov.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       return RiadokTabulky.NazvyStlpcov.values()[columnIndex].dajHodnotu(aRiadkyTabulky.get(rowIndex));
    }

    @Override
    public String getColumnName(int column) {
        return RiadokTabulky.NazvyStlpcov.values()[column].getaNazovStlpca();
        
    }
    
    public void pridajRiadok(RiadokTabulky paRiadok)
    {
        aRiadkyTabulky.add(paRiadok);
        fireTableDataChanged();
        SerializationWorker.<List<RiadokTabulky>>writeObjectToFile(new File("./data.bin"),aRiadkyTabulky);
        vypocitajSucetVkladov();
    }

    public List<RiadokTabulky> getaRiadkyTabulky() {
        return aRiadkyTabulky;
    }

    public Double getSumaVkladov() {
        return sumaVkladov;
    }
    
    
}

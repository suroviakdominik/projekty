
public class ElementarneRiadkoveOperacie
{
    private double [] pomocnePole;
    private PoctySMaticami aPocSMat;
    public ElementarneRiadkoveOperacie(PoctySMaticami paPocSMat)
    {
        aPocSMat=paPocSMat;
    }
                                                      
     public void vymenRiadky(int i,int j)//ity.riadok za jty.riadok
     {
         pomocnePole=new double[aPocSMat.dajPocetStlpcov()];
             for (int n=0;n<aPocSMat.dajPocetStlpcov();n++)
             {
                pomocnePole[n]=aPocSMat.zistiHodnotuCopyPola(i,n);
                aPocSMat.vlozHodnotuDoCopyPola(i,n,aPocSMat.zistiHodnotuCopyPola(j,n));
                aPocSMat.vlozHodnotuDoCopyPola(j,n,pomocnePole[n]);
             }
             System.out.println("Vymenil som "+(i+1)+". riadok za "+(j+1)+". riadok");
     }
     
    public void pripocitajNasoboItehoRiadkuKJetemuRiadku(int i,int j,int s)//1.riadok,2.riadok
    {
           double vysDel=((aPocSMat.zistiHodnotuCopyPola(j,s)/aPocSMat.zistiHodnotuCopyPola(i,s))*(-1));
           for (int l=0;l<aPocSMat.dajPocetStlpcov();l++)
           {
                        double nasobItyRiadok=aPocSMat.zistiHodnotuCopyPola(i,l)*vysDel;
                        aPocSMat.vlozHodnotuDoCopyPola(j,l,Zaokruhli.zaokruhluj((aPocSMat.zistiHodnotuCopyPola(j,l)+nasobItyRiadok)));
           }
           if (vysDel!=0)
           {
              System.out.println("K "+(j+1)+". riadku som pripocital "+vysDel+"-nasobok "+(i+1)+". riadku"); 
           }
    } 
    
    public void pripocitajItyRiadokKJetemuRiadku(int i,int j)
    {
        for (int p=0;p<aPocSMat.dajPocetStlpcov();p++)
        {
           aPocSMat.vlozHodnotuDoCopyPola(j,p,(aPocSMat.zistiHodnotuCopyPola(i,p)+aPocSMat.zistiHodnotuCopyPola(j,p))); 
        }
        System.out.println(+(i+1)+". riadok som pripocital k "+(j+1)+". riadku");
    } 
    
    public void vynasobRiadokKonstantou(int paItyRiadok, int paCinitel)
    {
        for (int j=0;j<aPocSMat.dajPocetStlpcov();j++)
        {
          aPocSMat.vlozHodnotuDoCopyPola(paItyRiadok,j,(paCinitel*aPocSMat.zistiHodnotuCopyPola(paItyRiadok,j))); 
        }
    }
    
    
    
}
            
            
            
            
            
 
 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evidencianehnutelnosti.trie_vykreslovanie;

import dynamickehasovanie.triestrom.TrieStrom;
import evidencianehnutelnosti.trie_vykreslovanie.StromPlatno;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicOptionPaneUI;

/**
 *
 * @author Dominik
 */
public class VykreslenieTrieStromu extends JDialog {

    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    private JPanel aMainPanel;
    private StromPlatno aStromPlatno;
    private JScrollPane aScrollStromPlatno;
    private JPanel aHornyPanelTlacidiel;
    private int returnStatus;
    private TrieStrom aTrieStrom;

    public VykreslenieTrieStromu(java.awt.Frame parent, boolean modal, TrieStrom paStrom) {
        super(parent, modal);
        this.setSize(new Dimension(800, 800));
        this.setPreferredSize(new Dimension(800, 800));
        this.setLayout(new BorderLayout());
        aTrieStrom = paStrom;
        initComponents();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }

    private void initComponents() {
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        aMainPanel = new JPanel(new BorderLayout());
        aMainPanel.setBackground(Color.LIGHT_GRAY);
        aStromPlatno = new StromPlatno(aTrieStrom);
        aStromPlatno.setBackground(Color.white);
        aHornyPanelTlacidiel = new JPanel(new BasicOptionPaneUI.ButtonAreaLayout(true, 10));
        aHornyPanelTlacidiel.setBackground(Color.DARK_GRAY);
        aScrollStromPlatno = new JScrollPane(aStromPlatno, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        aMainPanel.add(aScrollStromPlatno, BorderLayout.CENTER);
        aMainPanel.add(aHornyPanelTlacidiel, BorderLayout.NORTH);
        pridajTlacidlaZoom();
        this.getContentPane().add(aMainPanel, BorderLayout.CENTER);
    }
    
    private void pridajTlacidlaZoom() {
        JButton bPribliz = new JButton("+");
        JButton bOddial = new JButton("-");
        aMainPanel.add(bPribliz, BorderLayout.EAST);
        aMainPanel.add(bOddial, BorderLayout.WEST);
        bPribliz.addActionListener((ActionEvent e) -> {
            aStromPlatno.setPreferredSize(new Dimension(aStromPlatno.getWidth() + 200, aStromPlatno.getHeight() + 100));
            //aStromPlatno.aktualizujData();
            aStromPlatno.revalidate();
        });
        bOddial.addActionListener((ActionEvent e) -> {
            aStromPlatno.setPreferredSize(new Dimension(aStromPlatno.getWidth() - 200, aStromPlatno.getHeight() - 100));
            //aStromPlatno.aktualizujData();
            aStromPlatno.revalidate();
        });
    }

    private void closeDialog(WindowEvent evt) {
        doClose(RET_CANCEL);
    }

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    public int getReturnStatus() {
        return returnStatus;
    }

}

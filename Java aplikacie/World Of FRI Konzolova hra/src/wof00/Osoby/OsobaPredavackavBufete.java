/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Osoby;

import wof00.Hra.Hrac;
import java.util.ArrayList;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.MiestnostBufet;
import wof00.Predmety.IPredmet;
import wof00.Predmety.PredmetBageta;
import wof00.Predmety.PredmetKasa;
import wof00.Predmety.PredmetNapoj;

/**
 *
 * @author suroviak3
 */
public class OsobaPredavackavBufete implements IOsoba {

    private String aMeno;
    private ArrayList<String> aZoznamOspovedi;
    private IMiestnost aSucasnaMiestnost;
    private PredmetKasa aKasa;
    private String aVybranyTovar = null;

    public OsobaPredavackavBufete(IMiestnost paMiestnost) {
        aMeno = "bufetarka";
        aZoznamOspovedi = new ArrayList<String>();
        aSucasnaMiestnost = paMiestnost;
        if (aSucasnaMiestnost instanceof MiestnostBufet) {
            MiestnostBufet bufet = (MiestnostBufet) aSucasnaMiestnost;
            aKasa = bufet.dajKasu();
            pridajTovar(new PredmetBageta("minibageta", 5), 15, 2);
            pridajTovar(new PredmetBageta("velkabageta", 10), 30, 2);
            pridajTovar(new PredmetNapoj("napoj", 10), 30, 2);
        }

    }

    @Override
    public String getaMeno() {
        return aMeno;
    }

    @Override
    public void oslov() {
        System.out.println("Dobry den, co si date ?");
        System.out.println("A - minibageta");
        System.out.println("B - velkabageta");
        System.out.println("C - napoj");
        System.out.println("U - ukoncirozhovor");
        aZoznamOspovedi.add("A");
        aZoznamOspovedi.add("B");
        aZoznamOspovedi.add("C");
        aZoznamOspovedi.add("U");

    }

    @Override
    public void rozhovor(Hrac paHrac, String paOdpoved) {
        IPredmet vybranyPredmet = null;

        if (paOdpoved.equals("U")) {
            paHrac.setaRozhovor(null);
            System.out.println("Rozhovor ukonceny");
        }

        if (paOdpoved.equals("A")) {
            vybranyPredmet = aKasa.getPredmet("minibageta");
            aVybranyTovar = "minibageta";
        } else if (paOdpoved.equals("B")) {
            vybranyPredmet = aKasa.getPredmet("velkabageta");
            aVybranyTovar = "velkabageta";
        } else if (paOdpoved.equals("C")) {
            vybranyPredmet = aKasa.getPredmet("napoj");
            aVybranyTovar = "napoj";
        }

        if (paHrac.getaPenazenka() < aKasa.zistiCenu(vybranyPredmet)) {
            System.out.println("Nemate dostatok penazi, tovar si nemozete kupit.");
        } else if (aVybranyTovar != null) {
            if (aKasa.zistiMnozstvo(aVybranyTovar) > 0) {
                System.out.println(aKasa.pouzi(paHrac));
                paHrac.zdvihniPredmet(vybranyPredmet);
            } else {
                System.out.println("Tovar uz nemame. Obiednam ho.");
                objednajTovar(aVybranyTovar, 2);
            }
        } else {
            System.out.println("Taky tovar tu nepredavame.");
        }

    }

    @Override
    public boolean jeOdpoved(String paOdpoved) {
        for (String prechadzac : aZoznamOspovedi) {
            if (prechadzac.equals(paOdpoved)) {
                return true;
            }

        }
        return false;
    }

    private void pridajTovar(IPredmet paPredmet, Integer paCena, Integer paMnozstvo) {
        if (aKasa != null) {
            aKasa.pridajTovar(paPredmet, paCena, paMnozstvo);
        }
    }

    private void objednajTovar(String paNazov, int paMnozstvo) {
        aKasa.doplnZasobyTovaru(paNazov, paMnozstvo);
    }

    public String getVybranyTovar() {
        return aVybranyTovar;
    }
}

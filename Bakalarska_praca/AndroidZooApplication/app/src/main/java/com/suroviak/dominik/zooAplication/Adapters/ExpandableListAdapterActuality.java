package com.suroviak.dominik.zooAplication.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.suroviak.dominik.zooAplication.Info_About_Item;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dominik on 13.3.2015.
 */
public class ExpandableListAdapterActuality extends BaseExpandableListAdapter {
    private Context _context;
    private List<Info_About_Item> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<Info_About_Item, List<String>> _listDataChild;

    public ExpandableListAdapterActuality(Context context, List<Info_About_Item> listDataHeader,
                                          HashMap<Info_About_Item, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_actuality, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_aktuality, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(_listDataHeader.get(groupPosition).getOriginalName());

        String fullaNameOfPicture = "";
        ImageView icon = (ImageView) convertView.findViewById(R.id.lblistheader_image);
        Info_About_Item item = _listDataHeader.get(groupPosition);
        for (int i = 0; i < item.getDomainsOfFilesInCurrentItem().size(); i++) {
            String fullDomain = item.getDomainsOfFilesInCurrentItem().get(i);
            if (fullDomain.contains("picture")) {
                File f = new File(fullDomain);
                fullaNameOfPicture = f.getName();
                break;
            }
        }
        File f = new File(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                + item.getRelativePathOfFilesOfurrentItemInDevice()
                + fullaNameOfPicture);

        if (f.exists() && f.isFile()) {
            Drawable d = Drawable.createFromPath(f.getAbsolutePath());
            icon.setImageDrawable(d);
        } else {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            lblListHeader.setLayoutParams(param);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

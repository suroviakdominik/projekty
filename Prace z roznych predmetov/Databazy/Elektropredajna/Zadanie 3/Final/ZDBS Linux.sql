SELECT * FROM ADRESA;

SELECT meno,priezvisko FROM ZAMESTNANEC JOIN PREDAJNA
USING(id_Predajne);

SELECT PREDAJNA.NAZOV_PREDAJNE,meno,priezvisko FROM ZAMESTNANEC JOIN PREDAJNA --prv� select od konzultanta
USING(id_Predajne)
ORDER BY PREDAJNA.NAZOV_PREDAJNE;

select B.nazov, A.nazov --2. slect od konzultanta
FROM
(SELECT katalogoveCisloODPTovaru, nazov FROM Tovar join OdporucaneTovary on (Tovar.katalogoveCislo = OdporucaneTovary.katalogoveCisloODPTovaru)) A
left join
(select A.nazov, C.katalogoveCisloODPTovaru FROM  (SELECT katalogoveCislo, nazov FROM Tovar) A 
left join (SELECT katalogoveCislo, katalogoveCisloODPTovaru FROM OdporucaneTovary ) C
on A.katalogoveCislo  = C.katalogoveCislo)B
on A.katalogoveCisloODPTovaru  = B.katalogoveCisloODPTovaru
GROUP BY B.nazov, A.nazov;




SELECT priezvisko,meno,faktura.cislo_fa --treti select od konzultanta
FROM ZAKAZNIK JOIN FAKTURA
using(ID_ZAKAZNIKA)
ORDER BY PRIEZVISKO,meno;

SELECT Predajna.nazov_predajne,Faktura.cislo_fa --stvrty select od konzultanta
FROM PREDAJNA JOIN ZAMESTNANEC USING(id_predajne) JOIN FAKTURA USING (OSOBNE_CISLO)
ORDER BY NAZOV_PREDAJNE;

SELECT PREDAJNA.NAZOV_PREDAJNE,nazov_kat,nazov,TOVAR.CENA --piaty select od konzultanta
FROM KATEGORIA JOIN TOVAR using(id_kategorie) JOIN  PREDAJNA using(id_predajne)
WHERE(nazov_kat = '&&Premenna')
ORDER BY CENA,nazov;

 
select nazov_predajne, nazov, cena,mnozstvo from tovar join Predajna using (id_predajne) order by nazov_predajne, nazov, cena;--1.select zadanie

SELECT  Faktura.CISLO_FA,nazov,Tovar.katalogovecislo,count(*)As Pocet,Kategoria.MIN_POCET FROM --2. select zadanie
Tovar,Kategoria,VYFAKTUROVANYTOVAR,Faktura
where Kategoria.id_kategorie=Tovar.id_kategorie
AND Tovar.KATALOGOVECISLO=VYFAKTUROVANYTOVAR.KATALOGOVECISLO
AND Faktura.CISLO_FA=VYFAKTUROVANYTOVAR.CISLO_FA
AND to_date(trunc(sysdate,'DD')) - to_date(datum_vystavenia,'DD.MM.RRRR') <90
GROUP by Faktura.CISLO_FA,nazov,Tovar.katalogovecislo,Kategoria.MIN_POCET
having count(*) < Kategoria.MIN_POCET;

SELECT nazov_kat, nazov, VyfakturovanyTovar.mnozstvo as Predane, cena as AktualnaCena, (cena - (cena*0.01)*5) as NovaCena--3. select zadanie
FROM Kategoria join Tovar on (Kategoria.id_kategorie = Tovar.id_kategorie) join VyfakturovanyTovar on
(Tovar.katalogoveCislo = VyfakturovanyTovar.katalogoveCislo) ORDER BY nazov_kat;

select nazov_kat,typ.nazov_typu,nazov,cena,sum(VYfakturovanyTovar.mnozstvo),sum(cena),mnozZaKat,cenZaKat,celkoveTrzbyPredajne--4. select zadanie
from faktura join vyfakturovanyTovar using (cislo_fa,osobne_cislo)
join tovar using(katalogoveCislo)
join PREDAJNA on(Tovar.id_predajne=Predajna.id_predajne)
join kategoria using (id_kategorie)
join Typ using(id_Typu)
join (select id_kategorie,sum(VYfakturovanyTovar.mnozstvo) as mnozZaKat,sum(cena) as cenZaKat from faktura 
              join vyfakturovanyTovar using (cislo_fa,osobne_cislo)
              join tovar using(katalogoveCislo)
              join kategoria using (id_kategorie)
              group by id_kategorie) using(id_kategorie)
join (select id_predajne as id_pred_CT,sum(faktura.CENA_ZA_FAKTURU)as celkoveTrzbyPredajne 
      from predajna join zamestnanec 
      using(id_predajne) 
      join faktura 
      using(osobne_cislo)
      group by id_predajne)
on(id_pred_CT = id_predajne)
group by(nazov_kat,typ.nazov_typu,nazov,cena,mnozZaKat,cenZaKat,celkoveTrzbyPredajne)
order by nazov_kat;



--piaty select zadanie
select max(faktura.cena_za_fakturu),predajna.nazov_predajne,substr(faktura.datum_vystavenia,4,2) as mesiac
from tovar,vyfakturovanytovar,kategoria, faktura,Predajna,Zamestnanec
where tovar.KATALOGOVECISLo=vyfakturovanytovar.KATALOGOVECISLO
and tovar.id_kategorie=kategoria.ID_KATEGORIE
and faktura.cislo_fa = vyfakturovanytovar.CISLO_FA
and Predajna.id_predajne = Zamestnanec.id_predajne
AND   Zamestnanec.osobne_cislo = Faktura.osobne_cislo
and kategoria.ID_KATEGORIE='6K864OK735'
and substr(faktura.datum_vystavenia,7,2) = '13'
group by predajna.nazov_predajne,faktura.cena_za_fakturu,faktura.datum_vystavenia
order by mesiac;


--SELECT Predajna.nazov_predajne,sum(tovar.cena)as Trzby
-- Predajna,Zamestnanec,Faktura,VYFAKTUROVANYTOVAR,TOVAR,Kategoria
--where Predajna.id_predajne = Zamestnanec.id_predajne
--AND   Zamestnanec.osobne_cislo = Faktura.osobne_cislo
--AND   Faktura.cislo_fa=VYFAKTUROVANYTOVAR.CISLO_FA
--AND   VYFAKTUROVANYTOVAR.KATALOGOVECISLO=TOVAR.KATALOGOVECISLO
--AND   TOVAR.ID_KATEGORIE=Kategoria.ID_KATEGORIE
--GROUP BY Predajna.nazov_predajne;

--select sum(cena) as TrzbyZaKat FROM kategoria, tovar, vyfakturovanytovar, faktura
--WHere (kategoria.id_kategorie = tovar.ID_KATEGORIE
--AND tovar.KATALOGOVECISLO = vyfakturovanytovar.KATALOGOVECISLO
--AND vyfakturovanytovar.cislo_fa = faktura.CISLO_FA
--and kategoria.ID_KATEGORIE = '6K864OK735'
--and vyfakturovanytovar.MNOZSTVO > 0);

--piaty select zadanie
select max(faktura.cena_za_fakturu),predajna.nazov_predajne,substr(faktura.datum_vystavenia,4,2) as mesiac
from tovar,vyfakturovanytovar,kategoria, faktura,Predajna,Zamestnanec
where tovar.KATALOGOVECISLo=vyfakturovanytovar.KATALOGOVECISLO
and tovar.id_kategorie=kategoria.ID_KATEGORIE
and faktura.cislo_fa = vyfakturovanytovar.CISLO_FA
and Predajna.id_predajne = Zamestnanec.id_predajne
AND   Zamestnanec.osobne_cislo = Faktura.osobne_cislo
and kategoria.ID_KATEGORIE='6K864OK735'
and substr(faktura.datum_vystavenia,7,2) = '13'
group by predajna.nazov_predajne,faktura.cena_za_fakturu,faktura.datum_vystavenia
order by mesiac;

SELECT katalogoveCislo, nazov, nazov_predajne, mnozstvo, minimum_na_sklade, minimum_na_sklade - mnozstvo as objednat--6. select zadanie 
FROM Tovar join Predajna on (Tovar.id_predajne = Predajna.id_predajne) 
where(mnozstvo < minimum_na_sklade) ORDER BY objednat ASC;--ASC vzostupne, DESC by bolo zostupne


select nazov,cisFaktZadTov,--select 7 zadanie
case 
when nazov = 'Indesit SIAA' then VyfakturovanyTovar.mnozstvo-1
else VyfakturovanyTovar.mnozstvo
END as mnozstvo
from VYfakturovanyTovar,Tovar, (select vyfakturovanytovar.CISLO_FA as cisFaktZadTov
from tovar,vyfakturovanytovar,faktura
where tovar.KATALOGOVECISLO=vyfakturovanytovar.KATALOGOVECISLO
and  vyfakturovanytovar.CISLO_FA=faktura.CISLO_FA
and tovar.nazov = 'Indesit SIAA'
group by(nazov,vyfakturovanytovar.cislo_fa))
where cisFaktZadTov=VYfakturovanyTovar.CISLO_FA
and VYfakturovanyTovar.KATALOGOVECISLO =Tovar.KATALOGOVECISLO
and ((tovar.nazov = 'Indesit SIAA' and VyfakturovanyTovar.mnozstvo-1>0)OR (tovar.nazov != 'Indesit SIAA'))
group by nazov,VyfakturovanyTovar.mnozstvo,cisFaktZadTov;



select nazov,cisFaktZadTov,--select 7 zadanie varianta pomocou decode
decode(Tovar.NAZOV,'Indesit SIAA',VyfakturovanyTovar.mnozstvo-1,VyfakturovanyTovar.mnozstvo)as mnozstvo
from VYfakturovanyTovar,Tovar, (select vyfakturovanytovar.CISLO_FA as cisFaktZadTov
from tovar,vyfakturovanytovar,faktura
where tovar.KATALOGOVECISLO=vyfakturovanytovar.KATALOGOVECISLO
and  vyfakturovanytovar.CISLO_FA=faktura.CISLO_FA
and tovar.nazov = 'Indesit SIAA'
group by(nazov,vyfakturovanytovar.cislo_fa))
where cisFaktZadTov=VYfakturovanyTovar.CISLO_FA
and VYfakturovanyTovar.KATALOGOVECISLO =Tovar.KATALOGOVECISLO
and ((tovar.nazov = 'Indesit SIAA' and VyfakturovanyTovar.mnozstvo-1>0)OR (tovar.nazov != 'Indesit SIAA'))
group by nazov,VyfakturovanyTovar.mnozstvo,cisFaktZadTov;

--select cislo_fa,count(stav_faktury) from faktura join vyfakturovanyTovar using(CISLO_FA)
--group by(faktura.STAV_FAKTURY);

--select * 
--from tovar join vyfakturovanytovar using(katalogovecislo) 
        --   join     (select nazov as nZT,katalogovecislo as id_zadTovaru,cislo_fa as cisloFaZadtovaru 
               -- from tovar join vyfakturovanytovar using(KATALOGOVECISLO)
             --   where UPPER(tovar.nazov) = 'ACEC ALIEN') on(cislo_fa=cisloFaZadtovaru)
--left join OdporucaneTovary on(id_zadTovaru = OdporucaneTovary.KATALOGOVECISLO);

select cislo_fa,VyfakturovanyTovar.katalogovecislo,VyfakturovanyTovar.id_predajne,VyfakturovanyTovar.OSOBNE_CISLO --8 select zadanie ? neviem ci je spravne
from odporucanetovary join   (select katalogovecislo as katC_zadTovaru, id_predajne as id_predZadTov
                                      from tovar 
                                      where UPPER(tovar.nazov) = 'MYS' group by(katalogovecislo,id_predajne)) on(OdporucaneTovary.KatalogoveCISLOODPTovaru = katC_zadTovaru)
                                      join VyfakturovanyTovar ON 
                                      ((odporucanetovary.KATALOGOVECISLO=VyfakturovanyTovar.katalogovecislo 
                                      OR VyfakturovanyTovar.katalogovecislo=katC_zadTovaru) AND odporucanetovary.ID_PREDAJNE=VyfakturovanyTovar.ID_PREDAJNE)
where(katC_zadTovaru<>VyfakturovanyTovar.katalogovecislo)
AND(id_predZadTov<>VyfakturovanyTovar.id_predajne) 
;
                              
select meno,priezvisko,kategoria.nazov_kat,sum(cena_Za_Fakturu)--9. select
from zamestnanec join faktura using(osobne_cislo)
join vyfakturovanyTovar using (cislo_fa)
join tovar using(katalogoveCislo)
join kategoria using (id_kategorie)
where faktura.DATUM_VYSTAVENIA >='01.01.2014'
and   faktura.DATUM_VYSTAVENIA<=sysdate
group by meno,priezvisko,kategoria.nazov_kat
order by kategoria.nazov_kat;


SELECT meno,priezvisko,kategoria.nazov_kat,sum(cena_Za_Fakturu)
from zamestnanec join faktura using(osobne_cislo)
join vyfakturovanyTovar using (cislo_fa)
join tovar using(katalogoveCislo)
join kategoria using (id_kategorie)
where faktura.DATUM_VYSTAVENIA >='&&Premenna'
and   faktura.DATUM_VYSTAVENIA<=sysdate
group by meno,priezvisko,kategoria.nazov_kat
order by kategoria.nazov_kat;
--VARIABLE f NUMBER     FUNGUJE IBA V PUTTY
-- n Char(5)
--EXECUTE :n:='N852U';
--call (TRZBY_ZISKANE_OD_ZAKAZNIKA(:n,:f));
--Print (f)
--REPORTY




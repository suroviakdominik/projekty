/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00;

import wof00.VstupVystup.NazvyPrikazov;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author suroviak3
 */
public class NazvyPrikazovTest {

    NazvyPrikazov prikazyNazvy = new NazvyPrikazov();

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void jePrikazTestUkonci() {
    }

    {
        assertTrue(prikazyNazvy.jePrikaz("ukonci"));
    }

    @Test
    public void jePrikazTestSkonci() {
    }

    {
        assertFalse(prikazyNazvy.jePrikaz("skonci"));
    }

    @Test
    public void jePrikazTestChod() {
        assertTrue(prikazyNazvy.jePrikaz("chod"));
    }

    @Test
    public void jePrikazTestPomoc() {
        assertTrue(prikazyNazvy.jePrikaz("pomoc"));
    }

}

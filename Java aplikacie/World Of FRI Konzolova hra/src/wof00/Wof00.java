package wof00;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import wof00.Hra.Hra;
import java.util.Scanner;
import wof00.Hra.Hrac;

/**
 * Hlavna trieda hry WoF s metodou main - spustanie v NB
 *
 * @author Lubomir Sadlon
 * @version 21.2.2012
 */
public class Wof00 {

    private static Hra aHra;
    private static ArrayList<String> aNacitanie = new ArrayList<String>();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner nacitajUlozenie = new Scanner(System.in);
        System.out.println("Nacitat ?: A/A1/N");
        String odpoved = nacitajUlozenie.nextLine();
        boolean spustiHruOdZaciatku = true;
        if (odpoved.equals("A")) {
            if (nacitaj()) {
                aHra = new Hra(aNacitanie.get(0), Integer.parseInt(aNacitanie.get(1)), Integer.parseInt(aNacitanie.get(2)), aNacitanie.get(3));
                aHra.hraj();
                spustiHruOdZaciatku = false;
            }
        } else if (odpoved.equals("A1")) {
            aHra = new Hra(nacitajhSerializovany());
            if (aHra != null) {
                aHra.hraj();
                spustiHruOdZaciatku = false;
            }
        }

        if (spustiHruOdZaciatku) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Zadajte vaše meno: ");
            String menoHraca = scan.nextLine();
            Hra hra = new Hra(menoHraca);
            hra.hraj();
        }
    }

    private static boolean nacitaj() throws IOException {
        File file = new File("stav.txt");
        if (!file.exists()) {
            return false;
        }
        Scanner citac = new Scanner(file);
        while (citac.hasNextLine()) {
            aNacitanie.add(citac.nextLine());
        }
        return true;
    }

    private static Hrac nacitajhSerializovany() throws IOException, ClassNotFoundException {
        if (!new File("serial.ser").exists()) {
            return null;
        }
        FileInputStream fileIn = new FileInputStream("serial.ser");

        ObjectInputStream in = new ObjectInputStream(fileIn);
        Hrac h = (Hrac) in.readObject();

        fileIn.close();
        in.close();
        return h;
    }
}

;---------------------------------------------------------------ZADANIE-POSTUPNOST-------------------------------------------------------------------
;a) Ulo�te do pam�te postupnos� cel�ch ��sel v rozsahu <0,255> a na obrazovku vyp�te tie z nich, ktor� s� delite�n� tromi.
;b) ��sla z �lohy a) zap�te do textov�ho s�boru.
;c) Pomocou proced�ry Real32 (je na moodli) vyp�te s dan�ch ��sel aritmetick� priemer.
;(Real32 vie vypisova� desatinn� ��sla)

.Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
;.......potrebne k vypisu realnehoo cials.....
MaxCislic EQU 100
CisloBCD DB MaxCislic DUP (0)
IOBuffer DB 5 dup(0)

VypisPriemer DB 'Priemer cisel postupnosti delitelnyc delitelom: $'
VypisCislaDelitelneDelitelom DB 'Delitelom su delitelne cisla postupnosti: $'
Postupnost DB 1,2,5,6,7,8,9,10,11,12,13,33
PostupnostciseldelN DB 15 dup (?)
DlzkaPostupnosti DW 12
DlzkaPostupnostiDelDelitelom DW ? 
Delitel DB (3)
SucetClenovPostupnosti DW (0)
MenoSuboru DB 'TextSub.txt',0 
DesiatPriNas DW 10
VysledokPriemer DD (?)
PopisovacSuboru DW ?
ZapisanyZnakDoSuboru DB ?
;------------------------------------------------------------------------------------------------------------------------------------------------------
.CODE;//zacina kodovy segment
.386
;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
      loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

                                                                                                                         
Write ENDP

WriteFile PROC 
      push dx
      push cx
      push ax
      push bx
      mov cx,0
          
     
    
     
      KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        push cx
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        loop VypisCisl
        
       
       pop bx
       pop ax
       pop cx
       pop dx
       ret

     

WriteFile ENDP

;---------------------------------------------------------PROCEDURA NA VYPIS REALNEHO CISLA DO TERMINALU-----------------------------------------
Int16 PROC
; vypis celeho cisla typu word
; cislo je v ax
; modifikuje ax, bx, cx, dx, di
	mov bx, offset IOBuffer
	mov di,0
	mov cx,10
Cyklu:	mov dx,0
	div cx
	mov [bx+di], dl
	inc di
	cmp ax,0
	jnz Cyklu
Vypis:	mov dl,[bx+di-1]
	or dl, 30h
	mov ah,2
	int 21h
	dec di
	cmp di,0
	jnz Vypis
	ret
Int16 ENDP

Mantisa	PROC
; prevedie des. cast Cisla do BCD kodu v premennej CisloBCD, kde najvyssia cislica je pred des. ciarkou
; v Cislo zostane cela cast
; vstupny parameter: si je pocet miest mantisy
; vystupny parameter: si je pocet BCD cislic
; modifikuje ax, bx, cx, dx, di
	mov dl,2
	mov bx,1; kolko cislic BCD uz mam
DalsiBitMantisy:
	mov di, offset CisloBCD

	; skopiruj dalsiu cislicu mantisy do najvyssej BCD cislice
	shr Cislo,1
	adc byte ptr [di],0

	; vydel BCD cislo dvoma
	mov cx,bx
	mov ah,0
Delenie:	mov al,[di]
	aad; des. uprava ax
	div dl
	mov [di],al
	inc di
	loop Delenie
	mov [di],ah; odloz posledny zvysok
	inc bx
	dec si
	jnz DalsiBitMantisy
	dec bx
	mov si, bx
	ret
Mantisa	ENDP

Real32 PROC pascal Cislo:dword
	mov cx,word ptr [Cislo+2]
	; vypis minus, ak ide o zaporne cislo
	test cx,8000h
	jz Kladne
	mov dl,'-'
	mov ah, 2
	int 21h
Kladne:	; vyber exponent
	and cx,0111111110000000b
	shr cx,7
	sub cx,127; cx je exponent
	; vynuluj znamienko a exponent
	mov ebx,Cislo
	and ebx,007FFFFFh
	; pridaj najvyssi bit mantisy
	or ebx,00800000h;
	mov Cislo, ebx
	mov si,23; pocet bitov mantisy
	; posun des. ciarku o exponent
	sub si,cx
	call Mantisa; prevedie des. cast Cislo do BCD kodu, v Cislo zostane cela cast, v si pocet BCD cislic
	mov ax,word ptr [Cislo]
	call int16; vypise celu cast
	mov dl,'.'
	mov ah,2
	int 21h
; vypis mantisy
	mov di, offset CisloBCD
	inc di
	dec si
	mov cx,si
VypisM:	mov dl,[di]
	or dl,30h
	mov ah,2
	int 21h
	inc di
	loop VypisM
	ret 
Real32 ENDP

;---------------------------------------------------PROCEDURA NAJDENIE CISEL POSTUPNOSTI DELITELNYCH CISLOM  v premennej Delitel-------------
;parametre-offset postupnost v registri bx, dlzka postupnosti v registri cx
;Vystup-v postupnostciseldelN,a dlzka postupnostciseldelN v registri si
Delitelnost PROC
push ax
push bx
push cx
push dx
push di


  mov di,0
  mov si,0
  
  Hladaj:
   mov ah,0
   mov al,[bx+di]
   div delitel
   cmp ah,0
   jz JeDelitelneDelitelom
   jmp HladajDalej
   
   JeDelitelneDelitelom:
     mov dl,Postupnost[di]
     mov PostupnostciseldelN[si],dl
     inc si
   
   HladajDalej:
    inc di
    loop Hladaj  


pop di
pop dx
pop cx
pop bx
pop ax
ret
ENDP Delitelnost
;------------------------------------------PROCEDURA VYPISE PRVKY POSTUPNOSTI, KTOREJ OFFSET ODOVZDAME V REGISTRI BX A DLZKU V REGISTRI CX-------------------------------------------
VypisPostupnosti PROC
push ax
push bx
push cx
push di

mov di,0

  Vypisuj:
    mov ah,0
    mov al,[bx+di]
    call Write
    inc di
    mov dl,' '
    mov ah,2
    int 21h
  loop Vypisuj
pop di
pop cx
pop bx
pop ax
ret
ENDP VypisPostupnosti

ZadanieA PROC
push ax
push bx
push cx
push dx
push si
push di
mov cx,DlzkaPostupnosti
         mov bx, offset Postupnost
         call Delitelnost
         
         mov DlzkaPostupnostiDelDelitelom,si
         mov cx,si
         mov bx, offset postupnostciseldelN
         mov dx, offset VypisCislaDelitelneDelitelom
         mov ah,9
         int 21h
         call VypisPostupnosti
         mov DL,0AH;uloz do registra posun dole
         mov AH,2;do registra AH uloz cislo sluzby
         int 21h
  pop di
  pop si
  pop dx
  pop cx
  pop bx
  pop ax
  ret
ENDP ZadanieA

ZadanieB PROC
push ax
push bx
push cx
push dx
push si
push di
mov cx,0;vytvori subor pre zapis
        mov dx,offset MenoSuboru
        mov ah,3Ch
        int 21h
        mov PopisovacSuboru,AX
        
        mov di,0
        mov cx,DlzkaPostupnostiDelDelitelom;poor!!!!! dlzkapostupnostidel.. je nastavovana volanim procedury zadanie A
        ZapisDoSUboru:
           mov ah,0
           mov al,postupnostciseldelN[di]
           inc di     
           call WriteFile
        loop ZapisDoSUboru

        mov bx,PopisovacSuboru
        mov ah,3Eh
        int 21h
  pop di
  pop si
  pop dx
  pop cx
  pop bx
  pop ax
  ret 
ENDP ZadanieB

ZadanieC PROC
   push ax
   push bx
   push cx
   push dx
   push si
   push di
           mov cx,DlzkaPostupnostiDelDelitelom
           mov di,0
           
           VzpocitajSucetClenoPost:
             mov ax,SucetClenovPostupnosti
             mov dl,postupnostciseldelN[di]
             mov dh,0
             add ax,dx
             mov SucetClenovPostupnosti,ax
             inc di
           loop VzpocitajSucetClenoPost
           
           fild DlzkaPostupnostiDelDelitelom
           fild SucetClenovPostupnosti
           fdiv st(0),st(1)
           fstp VysledokPriemer
           push VysledokPriemer
           mov dx, offset VypisPriemer
           mov ah,9
           int 21h
           call Real32
  pop di
  pop si
  pop dx
  pop cx
  pop bx
  pop ax
  ret 
ENDP ZadanieC
;------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
         
         
       call ZadanieA
       call ZadanieB
       call ZadanieC
           
          
           
        
      
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































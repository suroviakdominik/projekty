/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Hra;

import java.io.Serializable;
import java.util.HashMap;
import wof00.Hra.MapaHry;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.MiestnostLabak;
import wof00.Osoby.IOsoba;
import wof00.Osoby.IOsoba;
import wof00.Predmety.IPredmet;
import wof00.Predmety.PredmetNavleky;

/**
 *
 * @author Dominik
 */
public class Hrac implements Serializable {

    private int aPenazenka;
    private int aZivot;
    private String aMeno;
    private int aExperience = 0;
    private IMiestnost aSucasnaPozicia;
    private HashMap<String, IPredmet> aInventar;
    private MapaHry aMapaHry;
    private boolean aMamNaVleky = false;
    private IOsoba aRozhovor;

    public Hrac(String paMenoHraca, int penazenka, int zivot, MapaHry aMapa) {
        this.aPenazenka = penazenka;
        this.aZivot = zivot;
        this.aMapaHry = aMapa;
        this.aMeno = paMenoHraca;
        aInventar = new HashMap<String, IPredmet>();
    }

    public Hrac(String menoHraca, MapaHry paMapa) {
        aMapaHry = paMapa;
        aMeno = menoHraca;
        aPenazenka = 100;
        aZivot = 100;
        aInventar = new HashMap<String, IPredmet>();
    }

    public void setaMeno(String aMeno) {
        this.aMeno = aMeno;
    }

    public MapaHry getaMapaHry() {
        return aMapaHry;

    }

    public void chodDoMiestnosti(String paMenoVychodu) {
        IMiestnost novaMiestnost;
        if ((paMenoVychodu.equals("modryPortalGun") || (paMenoVychodu.equals("cervenyPortalGun")))) {
            IMiestnost portalGunMiestnost = dajPoziciu().dajVychod(paMenoVychodu);
            novaMiestnost = portalGunMiestnost.dajVychod(paMenoVychodu);
        } else {
            novaMiestnost = dajPoziciu().dajVychod(paMenoVychodu);
        }

        if (novaMiestnost == null) {
            System.out.println("Tam nie je vychod, alebo nie je vytvoreny opacny portal.");

        } else {
            if ((novaMiestnost instanceof MiestnostLabak) && !(this.isaMamNaVleky())) {
                System.out.println("Nemate navleky. Pri vstupe do labbaku si musite obut navleky.");
            } else if (novaMiestnost.jePristupna(this)) {
                this.zvysSkusenost(1);
                System.out.println("Skusenosti hraca sa zvysili o: 1");
                setPoziciu(novaMiestnost);
                dajPoziciu().vypisInfoMiestnosti();
                this.odpocitajZivot();
            }

        }
    }

    public void obutSiNavleky() {
        aMamNaVleky = true;
        aInventar.remove("navleky");
        aInventar.put("obutenavleky", new PredmetNavleky());

    }

    public void vyzutNavleky() {
        aMamNaVleky = false;
        aInventar.remove("obutenavleky");
        aInventar.put("navleky", new PredmetNavleky());
    }

    public boolean isaMamNaVleky() {
        return aMamNaVleky;
    }

    public void setPoziciu(IMiestnost paMiestnost) {
        aSucasnaPozicia = paMiestnost;
    }

    public IMiestnost dajPoziciu() {
        return aSucasnaPozicia;
    }

    public void odpocitajZivot() {
        aZivot -= 5;
    }

    public void zvysZivot(int paOKolko) {
        aZivot += paOKolko;
    }

    public int dajZivot() {
        return aZivot;
    }

    public void zvysSkusenost(int paKolko) {
        aExperience += paKolko;
    }

    public int dajExperience() {
        return aExperience;
    }

    public String getMeno() {
        return aMeno;
    }

    public boolean masZivot() {

        return aZivot > 0 ? true : false;
    }

    @Override
    public String toString() {
        String vysledok = "Hráč: " + aMeno;
        vysledok += "\n" + "Život: " + aZivot;
        vysledok += "\n" + "Peňaženka: " + aPenazenka;
        return vysledok;
    }

    public void zdvihniPredmet(IPredmet paPredmet) {
        aInventar.put(paPredmet.getNazov(), paPredmet);
    }

    public String zobrazInventar() {
        String predmety = "Predmety hraca: ";
        for (String predmet : aInventar.keySet()) {
            predmety = predmety + " " + predmet;
        }
        return predmety;
    }

    public int getaPenazenka() {
        return aPenazenka;
    }

    public void odpocitajPeniaze(int paKolko) {
        aPenazenka -= paKolko;
    }

    public IPredmet dajPredmet(String paNazov) {
        return aInventar.get(paNazov);
    }

    public void polozPredmet(String paNazov) {
        aSucasnaPozicia.vlozPredmet(aInventar.get(paNazov));
        aInventar.remove(paNazov);
    }

    public void odstranZInventara(String paPredmet) {
        aInventar.remove(paPredmet);
    }

    public boolean maPredmet(String paNazov) {
        if (aInventar.containsKey(paNazov)) {
            return true;
        }
        return false;
    }

    public IOsoba getaRozhovor() {
        return aRozhovor;
    }

    public void setaRozhovor(IOsoba aRozhovor) {
        this.aRozhovor = aRozhovor;
    }
}

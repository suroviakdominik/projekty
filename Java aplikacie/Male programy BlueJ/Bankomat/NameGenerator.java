
import java.util.Random;
public class NameGenerator
{
   public static final char[] samohlasky= new char[]{'a','e','i','y','o','u'};
    public static final char[] spoluhlasky= new char[]{'p','t','k','c','s','h','f','b','d','g','z'};    

   private int aMinLength;
   private int aMaxLength;
   private Random aGeneruj;
  
   public char GenerateChar(boolean paVowel)
   {
       return paVowel?spoluhlasky[aGeneruj.nextInt(spoluhlasky.length)]:samohlasky[aGeneruj.nextInt(samohlasky.length)];
   }
   
   public NameGenerator(int paMinLength, int paMaxLength)
   {
     aMinLength=paMinLength;
     aMaxLength=paMaxLength;
     aGeneruj=new Random();
   }  
   
   
   public String generuj()
   {
      String result="";
      boolean spoluhlasky=aGeneruj.nextDouble()<0.5;
      char c;
      int length= aGeneruj.nextInt((aMaxLength+1-aMinLength)+aMinLength);
      
      result +=Character.toUpperCase(GenerateChar(spoluhlasky));
      spoluhlasky= !spoluhlasky;
      for (int i=0;i<length-1;i++,spoluhlasky=!spoluhlasky)
      result +=GenerateChar(spoluhlasky);
      
      return result;
   }   
          
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import setdatatoandroidaplication.DataTransferer;

/**
 *   * Editor, ktory edituje UDALOSTI
 * @author Dominik
 */
public class Info_File_EVENTS_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTransferer;

     /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce udalosti
     *                      a popis ich dat
     */
    public Info_File_EVENTS_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aInfoAboutDirsFile = paFilesInfo;
        aDataTransferer = DataTransferer.getInstance();
    }

    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return
     */
    @Override
    public boolean getDataOfInfoFile() {
        if (aDataTransferer.getItemInfo("GET_DATA_OF_SELECTED_EVENT") == null) {
            return getAllEvents();
        } else {

            Info_About_Item selectedEvents = aDataTransferer.getItemInfo("GET_DATA_OF_SELECTED_EVENT");
            return getDataOfSelectedEvents(selectedEvents);
        }

    }

     /**
     * Vytvorenie novej udalosti. Data dostaneme zo zalozky pomocou objektu DataTransferer
     * @return true - udalost sa podarilo vytvorit
     */
    @Override
    public boolean addNewItem() {
        String relativeParhOfItemWithoutNumber = aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + InfoSuboryENUM.FILE_SEPARATOR 
                + aInfoAboutDirsFile.getNameOfDirForThisInfoFile();
        DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + relativeParhOfItemWithoutNumber 
                + aInfoAboutDirsFile.getNumberOfNextItem());
        dw.vytvorAdresarAkNeexistuje();

        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + relativeParhOfItemWithoutNumber + aInfoAboutDirsFile.getNumberOfNextItem()
                + InfoSuboryENUM.FILE_SEPARATOR + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
        String dataToFile = aDataTransferer.getString("NEW_EVENT_TIME") + "\n"
                + aDataTransferer.getString("NEW_EVENT_TEXT");
        try {
            fw.writeDataToFile(dataToFile);
        } catch (IOException ex) {
            Logger.getLogger(Info_File_EVENTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return false;
        }

        Info_About_Item newEv = new Info_About_Item(aDataTransferer.getString("NEW_EVENT_NAME"),
                null,
                relativeParhOfItemWithoutNumber
                + aInfoAboutDirsFile.getNumberOfNextItem()
                + InfoSuboryENUM.FILE_SEPARATOR);

        newEv.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.EVENTS.getServerDIRDomainOfThisFile()
                + newEv.getRelativePathOfFilesOfurrentItemInDevice()
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
        aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + aInfoAboutDirsFile.getNumberOfNextItem(),
                newEv);
        aDataTransferer.removeAllData();
        aInfoAboutDirsFile.raiseNumberOfNextNewItem();
        System.out.println("MyLogFiles: New event with name "
                + newEv.getOriginalName()
                + "\n has been created in editor: "
                + this.getClass().getSimpleName());
        saveInfoFile();

        aDataTransferer.addItemInfo("CREATED_EVENT", newEv);
        return true;
    }

      /**
     * Zmaze udalost, ktoru daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer
     * @return true -udalost sa podarilo zmazat
     */
    @Override
    public boolean deleteItem() {
        Info_About_Item eventToDelete = aDataTransferer.getItemInfo("SELECTED_EVENT_TO_DELETE");
        if (eventToDelete == null) {
            return false;
        }

        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).equals(eventToDelete)) {

                File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                        + aAktualneUdajeInfoSuboru.get(prech).getRelativePathOfFilesOfurrentItemInDevice());
                try {
                    aAktualneUdajeInfoSuboru.remove(prech);
                    System.out.println("MyLogFiles: Event with name "
                            + eventToDelete.getOriginalName()
                            + "\n has been deleted in editor: "
                            + this.getClass().getSimpleName());
                    saveInfoFile();
                    delete(f);
                } catch (IOException ex) {
                    Logger.getLogger(Info_File_EVENTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                    return false;
                }

                return true;
            }
        }
        return false;
    }

    /**
     * Upravi data udalosti, ktoru daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -udalost sa podarilo upravit(zmenit data)
     */
    @Override
    public boolean editItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.EVENTS.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.EVENTS.getaRelativePathOfDirecotryWhereIAM());
        sw.saveObjectToFile();
        System.out.println("MyLogFiles: Info_About_Dirs_File in editor: "
                + this.getClass().getSimpleName()
                + " has been saved.");
        return true;
    }

    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
    @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    private boolean getAllEvents() {
        aDataTransferer.removeAllData();
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            aDataTransferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
        }
        System.out.println("\nMyLogFiles: All events has been set to \n"
                + "data transferer " + " in editor: " + this.getClass().getSimpleName());
        return true;
    }

    private boolean getDataOfSelectedEvents(Info_About_Item paSelectedEvent) {

        aDataTransferer.removeAllData();
        aDataTransferer.addString("RET_EVENT_NAME", paSelectedEvent.getOriginalName());
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + paSelectedEvent.getRelativePathOfFilesOfurrentItemInDevice()
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
        String text;
        try {
            text = fw.readDataFromFile();
        } catch (IOException ex) {
            Logger.getLogger(Info_File_EVENTS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return false;
        }
        aDataTransferer.addString("RET_EVENT_TEXT", text);
        System.out.println("\nMyLogFiles: All data of event: "
                + paSelectedEvent.getOriginalName()
                + "\n has been set to"
                + "data transferer in editor: "
                + this.getClass().getSimpleName());
        return true;
    }
}

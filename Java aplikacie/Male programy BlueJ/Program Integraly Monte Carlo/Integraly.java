
import java.util.Random;
public class Integraly
{
  private double aDolnaHranica;
  private double aHornaHranica;
  private double aMaxFunkcnaHodnota;
    
         public Integraly (double pDolnahranica,
                           double pHornaHranica,
                           double pMaxFunkcnaHodnota)
         {
               aDolnaHranica=pDolnahranica;
               aHornaHranica=pHornaHranica;
               aMaxFunkcnaHodnota=pMaxFunkcnaHodnota;
         }  
     
          private double funkcia( double x)
          {
              return Math.abs(x);    
          }
          
          private double obsah()
          {
              return (aHornaHranica-aDolnaHranica)*aMaxFunkcnaHodnota;
          }
          
          public double spocitaj()
          {
              int pocitadloDobrych = 0;
              int n=10000000;
              Random rx= new Random();
              Random ry= new Random();
              for (int i=1; i<=n;i++)
              {
                  double x= rx.nextDouble();
                  double y= ry.nextDouble();
                  
                  double posX=((aHornaHranica-aDolnaHranica)*x)*aMaxFunkcnaHodnota;
                  double posY=aMaxFunkcnaHodnota*y;
                  
                  if (funkcia(posX)>=posY)
                  {   
                      pocitadloDobrych++;
                  }   
                     
               }   
              return pocitadloDobrych/(double)n*obsah();       
            
            
          }                    
       
}           
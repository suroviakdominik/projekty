<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
        $sql = "SELECT id FROM kategoria";
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row["idKoment"] > $this->biggestId) {
                    $this->biggestId = $row["idKoment"];
                }
            }
        }
    }

    function vypisPopis() {
        $zvID = htmlspecialchars($_POST["zvID"]);
        $sql = sprintf("SELECT id,idKategoria,nazov,popis,nazovObrazok FROM zviera WHERE id=%d",
                $this->conn->real_escape_string($zvID));
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                $nazov = $row["nazov"];
                $popis = $row["popis"];
                $katId= $row["idKategoria"];
                $obrazokNazov = $row["nazovObrazok"];
                if (isset($_SESSION["login"]) && $_SESSION["login"] == 0) {
                    $sql2=  sprintf("SELECT meno,popis FROM kategoria WHERE id=%d",
                            $this->conn->real_escape_string($katId));
                    $result2 = mysqli_query($this->conn, $sql2);
                    $row2 = mysqli_fetch_assoc($result2);
                    $menoKat=$row2["meno"];
                    $popisKat=$row2["popis"];
                     echo "<form method=\"POST\" action=\"upravKategoriu.php\">";
                     echo "<textarea name=\"nazovKat\"  class=\"upravaZvieraNazov\">" . "$menoKat" . "</textarea>";
                     echo "<textarea name=\"popisKat\"   class=\"upravaZviera\">" . "$popisKat" . "</textarea>";
                     echo "<input type=\"text\" name=\"idKat\" value=\"$katId\" hidden=\"\">";
                     echo "<input type=\"submit\" value=\"Uloz upravy\">";
                     echo "</form>";
                   
                            
                    echo "<br>";
                    $usID = $row["id"];
                    echo "<form method=\"POST\" action=\"upravZviera.php\">";
                    echo "<textarea name=\"nazov\"  class=\"upravaZvieraNazov\">" . "$nazov" . "</textarea>";
                    echo "<textarea name=\"popis\"   class=\"upravaZviera\">" . "$popis" . "</textarea>";
                    echo "<input type=\"text\" name=\"idUpZv\" value=\"$usID\" hidden=\"\">";
                    $namesOfPicures = scandir("../ObrazkyZvierat", 1);
                    echo "<select name=\"obrN\" onchange=\"vlozZviera(this.value)\">";
                    echo "<option value=\"\" selected>Vyber obrázok:</option>";
                    foreach ($namesOfPicures as $name) {
                        if ($name != "." && $name != "..")
                            echo "<option value=\"$name\">$name</option>";
                    }
                    echo "</select>";
                    echo "<br>";
                    echo "<input id=\"vybrZviera\" type=\"text\" name=\"obrN\" value=\"$obrazokNazov\" readonly>";
                    echo "<br>";
                    echo "<input type=\"submit\" value=\"Uloz upravy\">";
                    echo "</form>";



                    echo "<form id=\"deleteZviera\" method=\"POST\" action=\"zmazZviera.php\">";
                    echo "<input type=\"button\" value=\"Zmaz zviera\" onclick=\"istoZmazatZviera()\">";
                    echo "<input type=\"text\" name=\"idMazZv\" value=\"$usID\" hidden=\"\">";
                    echo "</form>";
                }

                if ($obrazokNazov != NULL) {
                    echo "<h1>$nazov</h1><img src=\"../ObrazkyZvierat/$obrazokNazov\"><p>$popis</p>";
                } else {
                    echo "<h1>$nazov</h1><p>$popis</p>";
                }
            }
        } else {
            echo "0 results";
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

$db = new DBPraca;
$db->vypisPopis();
$db->ukonciPristup();

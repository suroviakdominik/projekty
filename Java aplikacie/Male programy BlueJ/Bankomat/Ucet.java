
import java.text.DecimalFormat;
public class Ucet
{
  private double aStav;
  private int aCisloUctu;
  private Banka aBanka;
  private int Cislouctu;
  private double Zostatok;
     public Ucet (double paStav,
                  int paCisloUctu,
                   Banka paBanka)
     {
       aStav=paStav;
       aBanka=paBanka;
       aCisloUctu=paCisloUctu;
     }  

    public void vklad(int paSuma)
    {
      aStav+=paSuma;
    }
    
    public void vyber(int paSuma)
    {
      aStav-=paSuma;
    }
  
    public Banka dajBanku()
    {
        return aBanka;
    }
    
    public double dajStav()
    {
        return aStav;
    }   
    
    public String vypis()
    {
      DecimalFormat dfUcet=new DecimalFormat("0000000000");
      DecimalFormat dfKod=new DecimalFormat("0000");
      DecimalFormat dfSuma=new DecimalFormat(".00");
      return dfUcet.format(aCisloUctu) +"/"+
             dfKod.format(aBanka.getKod())+":"+
             dfSuma.format(aStav);
      
    }   
}

    
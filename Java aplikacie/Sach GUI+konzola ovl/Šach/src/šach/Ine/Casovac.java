/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Ine;

import Grafika.Platno;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author Dominik
 */
public class Casovac {

    private static int aSekundy;
    private static int aMinuty;
    private static int aHodiny;
    private Timer aCasovac;

    /**
     * Pri volaní konštruktora sa priradí počiatočný čas od ktorého sa začne
     * trvanie hry počítať. Naplnia sa atribúty aHodiny, aMinuty, aSekundy.
     * Napríklad ak vytvoríme novú hru, tak sa všetky atribúty nastavia na nulu
     * , keďže zatiaľ odohraný čas v hre je 0:0:0.
     *
     * @param paHodiny - počiatočný stav hodín časovača
     * @param paMinuty - počiatočný stav minút časovača
     * @param paSekundy - počiatočný stav sekúnd časovača
     */
    public Casovac(int paHodiny, int paMinuty, int paSekundy) {
        aSekundy = paSekundy;
        aMinuty = paMinuty;
        aHodiny = paHodiny;
    }

    /**
     * vráti minúty, ktoré boli danou šachovou partiou strávené.
     *
     * @return vráti hodnotu atribútu aMinuty
     */
    public static int getMinuty() {
        return aMinuty;
    }

    /**
     * vráti sekundy, ktoré časovač aktuálne má napočítané.
     *
     * @return vráti hodnotu atribútu aSekundy
     */
    public static int getSekundy() {
        return aSekundy;
    }

    /**
     * vráti hodiny, ktoré boli danou šachovou partiou strávené/ súčasný stav
     * hodín časovača.
     *
     * @return vráti hodnotu atribútu aHodiny
     */
    public static int getHodiny() {
        return aHodiny;
    }

    /**
     * vytvorí objekt typu Timer a priradí ho do atribútu aCasovac, ktorý príjme
     * ako parametre o koľko ms má procesor vykonať akciu a odpočúvadlo, čo je
     * objekt ktorý kontroluje stav jednotlivých atribútov a či boli vykonané
     * nejaké zmeny(v našom prípade rast času). Pošle mu správu start(), ktorá
     * spustí počítanie času.
     */
    public void startCasuj() {
        int delay = 1000; //milliseconds
        Timer t = new Timer(delay, odpocuvadlo);
        this.aCasovac = t;
        aCasovac.start();
    }

    /**
     * ukončí počítanie času tak, že pošle správu Timeru aCasovac.stop().
     */
    public void stopCasuj() {
        this.aCasovac.stop();
    }

    ActionListener odpocuvadlo = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            aSekundy++;
            if (aSekundy == 60) {
                aSekundy = 0;
                aMinuty++;
            }
            if (aMinuty == 60) {
                aHodiny++;
                aMinuty = 0;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    JLabel l = Platno.dajPlatno().getJLabel();
                    l.setOpaque(true);
                    l.setText("Odohraný čas v hre: " + aHodiny + " : " + aMinuty + (" : ") + aSekundy);
                }
            });

        }
    };
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafika;

/**
 *
 * @author Dominik
 */
public interface IGrafickeObjekty {

    public void zobraz();

    public void skry();

    public void posunVpravo();

    public void posunVlavo();

    public void posunHore();

    public void posunDole();

    public void posunVodorovne(int paVzdialenost);

    public void posunZvisle(int paVzdialenost);

    public void pomalyPosunVodorovne(int paVzdialenost);

    public void pomalyPosunZvisle(int paVzdialenost);

    public void zmenFarbu(String paFarba);

}

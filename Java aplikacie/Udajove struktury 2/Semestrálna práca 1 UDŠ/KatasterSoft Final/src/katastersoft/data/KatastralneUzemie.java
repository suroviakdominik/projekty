/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.data;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import katastersoft.rbstrom.BVSAlgoritmy;
import katastersoft.rbstrom.RBStrom;
import katastersoft.rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public class KatastralneUzemie {

    private Integer aCisloKU;
    private String aNazovKU;
    private RBStrom<Integer, Nehnutelnost> aNehnutelsnostiKatastra;
    private RBStrom<Integer, ListVlastnictva> aLVvKU;

    private final Logger LoggerKU = Logger.getLogger(this.getClass().getName());

    public KatastralneUzemie(Integer aCisloKU, String aNazovKU) {
        this.aCisloKU = aCisloKU;
        this.aNazovKU = aNazovKU;
        aNehnutelsnostiKatastra = new RBStrom<>();
        aLVvKU = new RBStrom<>();
        LoggerKU.setLevel(Level.OFF);
    }

    /**
     * 
     * @return cisloKU
     */
    public Integer getaCisloKU() {
        return aCisloKU;
    }

    /**
     * 
     * @return nazovKu
     */
    public String getaNazovKU() {
        return aNazovKU;
    }

    /**
     *
     * @param paLV
     * @return true-LV sa podarilo pridat false- LV sa nepodarilo pridat, lebo
     * uz je V Strome
     */
    public boolean pridajNovyLV(ListVlastnictva paLV) {
        if (aLVvKU.obsahujeKluc(paLV.getaCisloLV())) {
            LoggerKU.log(Level.INFO, "Nepodarilo sa pridat LV, lebo uz existuje.");
            return false;
        }
        aLVvKU.pridaj(paLV.getaCisloLV(), paLV);
        if (aLVvKU.obsahujeKluc(paLV.getaCisloLV())) {
            LoggerKU.log(Level.INFO, "Novy LV pridany do KU");
            return true;
        }
        throw new IllegalStateException("LV sa nepodarilo pridat aj napriek tomu,"
                + "ze sa este medzi LV KU nenachadza. CHYBA RB.");
    }

    /**
     *
     * @param paCisloLV
     * @return null-ak sa LV nenechadza v danom KU inak: odstranene LV
     */
    public ListVlastnictva odoberLV(Integer paCisloLV) {
        if (!aLVvKU.obsahujeKluc(paCisloLV)) {
            LoggerKU.log(Level.INFO, "Nepodarilo sa odstranit LV, lebo v danom KU taky neexistuje.");
        }
        ListVlastnictva odstr = aLVvKU.odstran(paCisloLV);
        if (!aLVvKU.obsahujeKluc(paCisloLV)) {
            LoggerKU.log(Level.INFO, "Podarilo sa odstranit LV z KU");
            odstr.odoberMajetkovePodielyVsetkymOsobam();
            return odstr;
        }
        throw new IllegalStateException("LV sa nachaza v RB aj napriek tomu,"
                + "ze sme ho uz zmazali. CHYBA RB");
    }

    /**
     * 
     * @param paNehnutelnost
     * @return false-ak sa uz nehnutelnost zo zadanym sup. cislom v katastri nachadza
     *          true-inak;
     */
    public boolean pridajNovuNehnutelnost(Nehnutelnost paNehnutelnost) {
        if (aNehnutelsnostiKatastra.obsahujeKluc(paNehnutelnost.getaSupisneCislo())) {
            return false;
        }
        aNehnutelsnostiKatastra.pridaj(paNehnutelnost.getaSupisneCislo(), paNehnutelnost);
        if (aNehnutelsnostiKatastra.obsahujeKluc(paNehnutelnost.getaSupisneCislo())) {
            return true;
        }
        throw new IllegalStateException("Nehnutelnost sa nepodarilo pridat aj napriek tomu,"
                + "ze sa este medzi nehnutelnostami KU nenachadza. CHYBA RB.");
    }

    /**
     * !!!POOZOR - ak by sme chceli dokodit aby sa dala nehnutelnost odstranovat
     * priamo z KU, Potom je potrebne dokodit to aby bola nehnutelnost zmazana z
     * LV, kde je zapisana Nevolaj vsak metodu
     * ListVlastnictva.odstanNehnutelnostZLV, vznikne zacyklenie medzi metodami.
     *
     * @param paSupisneCislo
     * @return null-ak nehnutelnost zo zadanym sup cislom v KU neexistuje
     */
    public Nehnutelnost odstranExistujucNehnutelnost(Integer paSupisneCislo) {
        if (!aNehnutelsnostiKatastra.obsahujeKluc(paSupisneCislo)) {
            return null;
        }
        Nehnutelnost odstr = aNehnutelsnostiKatastra.odstran(paSupisneCislo);
        if (!aNehnutelsnostiKatastra.obsahujeKluc(paSupisneCislo)) {
            return odstr;
        }
        throw new IllegalStateException("Nehnutelnost sa nachaza v RB aj napriek tomu,"
                + "ze sme ju uz zmazali. CHYBA RB");
    }

    /**
     * 
     * @return zoznam LV nachadzajucich sa v KU
     */
    public List<Vrchol> getLV() {
        return BVSAlgoritmy.getInorder(aLVvKU.getKoren());
    }

    /**
     * 
     * @return zoznam nehnutelnosti nachadzajucich sa v KU
     */
    public List<Vrchol> getNehnutelnosti() {
        List<Vrchol> ret = BVSAlgoritmy.getInorder(aNehnutelsnostiKatastra.getKoren());
        Collections.sort(ret);
        return ret;
    }

    /**
     * 
     * @param supisneCislo
     * @return nehnutelnost zo zadanym sup cislom
     *          null - ak nie je v katastri nehn zo zadanym sup. cislom
     */
    public Nehnutelnost getNehnutelnost(Integer supisneCislo) {
        return aNehnutelsnostiKatastra.dajHodnotu(supisneCislo);
    }

    /**
     * 
     * @param paCisloLV
     * @return LV zo zadanym cislom LV
     *          null - ak nie je v katastri LV zo zadanym cislom
     */
    public ListVlastnictva getListVlastnictva(int paCisloLV) {
        return aLVvKU.dajHodnotu(paCisloLV);
    }

    /**
     * 
     * @param cisloLV
     * @return true - v KU existuje LV zo zadanym cislom
     */
    public boolean existujeListVlastnictva(Integer cisloLV) {
        return aLVvKU.obsahujeKluc(cisloLV);
    }

    /**
     * 
     * @param paSupCislo
     * @return true -  v KU existuje nehn. zo zadanym sup.cislom
     */
    public boolean existujeNehnutelnost(Integer paSupCislo) {
        return aNehnutelsnostiKatastra.obsahujeKluc(paSupCislo);
    }

    /**
     * Prida novu nehnutelnost do KU na zadany LV
     * @param nova
     * @param cisloLV
     * @return true- ak sa podarilo pridat
     *         false- inak, napr. uz sa taka nehnutelnost v KU nachadza na inom LV
     */
    public boolean pridajNehnutelnostNaLV(Nehnutelnost nova, Integer cisloLV) {
        return aLVvKU.dajHodnotu(cisloLV).pridajNehnutelnost(nova);
    }

    /**
     * 
     * @param paCisloLV
     * @return retazec obsahujuci informacie o LV zo zadanym cislom
     */
    public String vypisZListuVlastnictva(Integer paCisloLV) {
        return aLVvKU.dajHodnotu(paCisloLV).toString();
    }

    /**
     * 
     * @param paSupCislo
     * @return retazec obsahujuci informacie o nehnutelnosti v zadanom KU
     */
    public String vypisNehnutelnostZKU(Integer paSupCislo) {
        String ret = "";
        if (aNehnutelsnostiKatastra.obsahujeKluc(paSupCislo)) {
            ret += aNehnutelsnostiKatastra.dajHodnotu(paSupCislo).toString();
        }
        return ret;
    }

    /**
     * v pripade ze rusime niektore KU, tak povodne LV sa presunu do noveho KU, 
     * ktorym je this. Metoda presunie LV zo odstrKU do tohto KU
     * @param lpOdstrKU 
     */
    public void pridajLVRusenehoKU(KatastralneUzemie lpOdstrKU) {
        Random gen = new Random();
        ListVlastnictva presuvany = null;
        for (Vrchol prech : BVSAlgoritmy.getInorder(lpOdstrKU.aLVvKU.getKoren())) {
            presuvany = (ListVlastnictva) prech.getData();
            presunNehnutelnostiPriMazaniKU(presuvany, gen);
            int paKlucLVPoPrelozeni = presuvany.getaCisloLV();
            while (aLVvKU.obsahujeKluc(paKlucLVPoPrelozeni)) {
                paKlucLVPoPrelozeni=gen.nextInt(Integer.MAX_VALUE);
            }
            presuvany.setaCisloLV(paKlucLVPoPrelozeni);
            presuvany.setaKU(this);
            pridajNovyLV(presuvany);
        }
    }

  
    private void presunNehnutelnostiPriMazaniKU(ListVlastnictva paLV, Random gen) {
        for (Nehnutelnost prech : paLV.getNehnutelnosti()) {
            int paKlucPoPrelozeni = prech.getaSupisneCislo();
            while (aNehnutelsnostiKatastra.obsahujeKluc(paKlucPoPrelozeni)) {
                paKlucPoPrelozeni = gen.nextInt(Integer.MAX_VALUE);
            }
            aNehnutelsnostiKatastra.pridaj(paKlucPoPrelozeni, prech);
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KatastralneUzemie other = (KatastralneUzemie) obj;
        if (!Objects.equals(this.aCisloKU, other.aCisloKU)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String ret = "";
        ret += aNazovKU;
        ret += ", ID: " + aCisloKU;
        return ret;
    }

}

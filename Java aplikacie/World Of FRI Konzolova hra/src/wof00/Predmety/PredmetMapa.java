/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;

/**
 *
 * @author Dominik
 */
public class PredmetMapa implements IPredmet, Serializable {

    private ArrayList<IMiestnost> aZoznamMiestnosti;
    private IMiestnost[][] aMaticaVzdialenosti;
    private int aPocetVrcholov;
    private Hrac aHrac;

    public PredmetMapa(ArrayList<IMiestnost> paZoznamMiestnosti) {
        aZoznamMiestnosti = paZoznamMiestnosti;
        aPocetVrcholov = paZoznamMiestnosti.size();
        aMaticaVzdialenosti = new IMiestnost[aPocetVrcholov][aPocetVrcholov];

    }

    private void inicializacia(Hrac paHrac) {
        aHrac = paHrac;
        IMiestnost aSucasnaPozicia = paHrac.dajPoziciu();
        for (int i = 0; i < aPocetVrcholov; i++) {
            for (int j = 0; j < aPocetVrcholov; j++) {
                aMaticaVzdialenosti[i][j] = aZoznamMiestnosti.get(j);
                if ((aMaticaVzdialenosti[i][j].getaNazov()).equals(aSucasnaPozicia.getaNazov())) {
                    aMaticaVzdialenosti[i][j].setaDlzkaZoSucasnejDoTejtoMiestnosti(0);
                    aMaticaVzdialenosti[i][j].setaPredchodca(null);
                } else {
                    aMaticaVzdialenosti[i][j].setaDlzkaZoSucasnejDoTejtoMiestnosti(Integer.MAX_VALUE);
                    aMaticaVzdialenosti[i][j].setaPredchodca(null);
                }
            }
        }
    }

    private int urcRadiaciPrvok(int paVRiadku) {
        int stlpec = -5;
        int minDlzkaVRiadku = Integer.MAX_VALUE;
        for (int i = 0; i < aPocetVrcholov; i++) {
            if ((aMaticaVzdialenosti[paVRiadku][i] != null) && ((aMaticaVzdialenosti[paVRiadku][i].dlzkaZatNajdNajkrCesZoSucVrch()) < minDlzkaVRiadku)) {
                minDlzkaVRiadku = aMaticaVzdialenosti[paVRiadku][i].dlzkaZatNajdNajkrCesZoSucVrch();
                stlpec = i;
            }
        }

        if (stlpec != -5) {
            for (int i = (paVRiadku + 1); i < aPocetVrcholov; i++) {
                aMaticaVzdialenosti[i][stlpec] = null;
            }
        }

        return stlpec;
    }

    private void najdiNajkretsieCesty()//Dijkstrovym algoritmom
    {
        IMiestnost lpRiadiaciPrvok;
        for (int i = 1; i < aPocetVrcholov; i++) {
            int stlpecSRiadPrvkom = urcRadiaciPrvok(i - 1);

            if (stlpecSRiadPrvkom != -5) {
                lpRiadiaciPrvok = aMaticaVzdialenosti[i - 1][stlpecSRiadPrvkom];

                for (int j = 0; j < aPocetVrcholov; j++) {

                    IMiestnost lpSucasnaPoziciaPrechadzaca = aMaticaVzdialenosti[i][j];
                    if (lpSucasnaPoziciaPrechadzaca != null) {
                        if (lpRiadiaciPrvok.dajVychod(lpSucasnaPoziciaPrechadzaca.getaNazov()) != null) {
                            if ((lpRiadiaciPrvok.dlzkaZatNajdNajkrCesZoSucVrch() + 1) < (aMaticaVzdialenosti[i - 1][j].dlzkaZatNajdNajkrCesZoSucVrch())) {
                                lpSucasnaPoziciaPrechadzaca.setaDlzkaZoSucasnejDoTejtoMiestnosti(lpRiadiaciPrvok.dlzkaZatNajdNajkrCesZoSucVrch() + 1);
                                lpSucasnaPoziciaPrechadzaca.setaPredchodca(lpRiadiaciPrvok);
                            } else {
                                lpSucasnaPoziciaPrechadzaca.setaDlzkaZoSucasnejDoTejtoMiestnosti(aMaticaVzdialenosti[i - 1][j].dlzkaZatNajdNajkrCesZoSucVrch());
                                lpSucasnaPoziciaPrechadzaca.setaPredchodca(aMaticaVzdialenosti[i - 1][j].dajPredchodcuVrchola());
                            }
                        }

                    }

                }
            } else {
                return;
            }

        }
    }

    private void vypisMatice() {
        for (int i = 0; i < aPocetVrcholov; i++) {
            for (int j = 0; j < aPocetVrcholov; j++) {
                if (aMaticaVzdialenosti[i][j] != null) {
                    System.out.print(aMaticaVzdialenosti[i][j].dlzkaZatNajdNajkrCesZoSucVrch() + "/");
                    if (aMaticaVzdialenosti[i][j].dajPredchodcuVrchola() != null) {
                        System.out.print(aMaticaVzdialenosti[i][j].dajPredchodcuVrchola().getaNazov() + "\t");
                    } else {
                        System.out.print("........" + "\t");
                    }

                } else {
                    System.out.print("    " + "\t" + "\t");
                }
            }
            System.out.println();
        }
    }

    private int najdiIndexVrchola(IMiestnost paPredchodca) {
        int indexKdeSaMiestnostNachadza = -5;
        for (int i = 0; i < aPocetVrcholov; i++) {
            if (aZoznamMiestnosti.get(i).getaNazov().equals(paPredchodca.getaNazov())) {
                indexKdeSaMiestnostNachadza = i;
            }
        }
        return indexKdeSaMiestnostNachadza;
    }

    private String zistiNajkratsiuCestuDoMiestnosti(IMiestnost paDoMiestnosti) {
        ArrayList najkratsiaCesta = new ArrayList<String>();
        int indexPolaStringov = aPocetVrcholov;
        int index;
        String vyslednyretazec = "";
        index = najdiIndexVrchola(paDoMiestnosti);
        for (int j = 0; !(aZoznamMiestnosti.get(index).getaNazov().equals(aHrac.dajPoziciu().getaNazov())); j++) {
            najkratsiaCesta.add(aZoznamMiestnosti.get(index).getaNazov());
            index = najdiIndexVrchola(aZoznamMiestnosti.get(index).dajPredchodcuVrchola());
            --indexPolaStringov;
        }
        najkratsiaCesta.add(aHrac.dajPoziciu().getaNazov());

        for (int j = (najkratsiaCesta.size() - 1); j >= 0; j--) {
            if (j != 0) {
                vyslednyretazec += najkratsiaCesta.get(j) + " - ";
            } else {
                vyslednyretazec += najkratsiaCesta.get(j);
            }

        }
        return vyslednyretazec;
    }

    private String vypisZoznamMiestnosti() {
        String miestnosti = "";
        for (int i = 0; i < aZoznamMiestnosti.size(); i++) {
            if (i != (aZoznamMiestnosti.size() - 1)) {
                miestnosti += aZoznamMiestnosti.get(i).getaNazov() + ", ";
            } else {
                miestnosti += aZoznamMiestnosti.get(i).getaNazov();
            }
        }
        return miestnosti;
    }

    @Override
    public String getNazov() {
        return "mapa"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPopis() {
        return "mapa, ktora najde najkratsiu cestu zo sucasnej do akejkolvek inej miestnosti, kde taka cesta existuje";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        inicializacia(paHrac);
        najdiNajkretsieCesty();
        Scanner scan = new Scanner(System.in);
        System.out.println("Nachadzate sa v miestnosti: " + paHrac.dajPoziciu().getaNazov());
        System.out.println("Miestnosti kam mozete ist su: " + vypisZoznamMiestnosti());
        System.out.println("Zadajte miestnost kam chcete zistit najkratsiu cestu: ");
        System.out.print("> ");
        String miestnost = scan.nextLine();
        int index;
        for (int i = 0; i < aZoznamMiestnosti.size(); i++) {
            if (miestnost.equals(aZoznamMiestnosti.get(i).getaNazov())) {
                if (aZoznamMiestnosti.get(i).dlzkaZatNajdNajkrCesZoSucVrch() == Integer.MAX_VALUE) {
                    return "Cesta do tejto mietnosti neexistuje.";
                } else {
                    index = i;
                    return "Najkratsie cesta z aktualnej miestnosti: " + paHrac.dajPoziciu().getaNazov() + " do miestnosti: " + aZoznamMiestnosti.get(i).getaNazov() + " je: " + "\n" + zistiNajkratsiuCestuDoMiestnosti(aZoznamMiestnosti.get(index));
                }
            }
        }

        return "Taka miestnost neexistuje.";

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.TreeMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rbstrom.RBStrom.Vrchol;
import rbstrom.Solver.BVSAlgoritmy;

/**
 *
 * @author Dominik
 */
public class RBStromTest {

    private RBStrom<Integer, Integer> aMyRBStrom;
    private TreeMap<Integer, Integer> aRBJava;
    private Random aGenerator;
    private List<Integer> aUzRazOdstranene;

    public RBStromTest() {
        aMyRBStrom = new RBStrom<>();
        aRBJava = new TreeMap<>();
        aGenerator = new Random();
        aUzRazOdstranene = new ArrayList<>();
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    /**
     * Test of pridaj method, of class RBStrom.
     */
    @Test
    public void testPridaj() {
        System.out.println("\n***********************TEST PRIDAVANIE*************************");
        int pocetPrid = 10000+aGenerator.nextInt(40000);
        System.out.println("Generujem: " + pocetPrid);
        for (int i = 0; i < pocetPrid; i++) {
            int paPridKluc = aGenerator.nextInt(70000);
            int paPridHodnota = aGenerator.nextInt(50000);
            aMyRBStrom.pridaj(paPridKluc, paPridHodnota);
            aRBJava.put(paPridKluc, paPridHodnota);
            assertTrue(aMyRBStrom.jePocetCiernychDoListovRovnaky());
        }
        assertTrue(aMyRBStrom.skontrolujDvaCerveneZaSebou());
        System.out.println("Pocet prvkov MYRB: " + aMyRBStrom.getaPocetPrvkov() + " Pocet prvkov Java RB: " + aRBJava.size());
        assertEquals(aMyRBStrom.getaPocetPrvkov(), aRBJava.size());
        System.out.println("*******************TEST PRIDAVANIE KONIEC**********************\n");
    }

    /**
     * Test of odstran method, of class RBStrom.
     */
    @Test
    public void testOdstran() {
        System.out.println("\n***********************TEST ODSTRANOVANIE*************************");
        int pocetPrid = 10000+aGenerator.nextInt(40000);
        for (int i = 0; i < pocetPrid; i++) {
            int paPridKluc = aGenerator.nextInt(70000);
            int paPridHodnota = aGenerator.nextInt(50000);
            aMyRBStrom.pridaj(paPridKluc, paPridHodnota);
            aRBJava.put(paPridKluc, paPridHodnota);
        }
        System.out.println("Pred odstranenim je v strome: " + aMyRBStrom.getaPocetPrvkov() + " prvkov");
        int pocetOdstr = aGenerator.nextInt(100000);
        int podariloSaOdstranit = 0;
        System.out.println("Skusim odstranit: " + pocetOdstr);
        for (int i = 0; i < pocetOdstr; i++) {
            int odstrKluc = aGenerator.nextInt(70000);
            if (aMyRBStrom.odstran(odstrKluc)) {
                podariloSaOdstranit++;
            }
            aRBJava.remove(odstrKluc);
            assertTrue(aMyRBStrom.jePocetCiernychDoListovRovnaky());
            
        }
        assertTrue(aMyRBStrom.skontrolujDvaCerveneZaSebou());
        assertEquals(aMyRBStrom.getaPocetPrvkov(), aRBJava.size());
        System.out.println("Odstranil som: " + podariloSaOdstranit);
        System.out.println("Po odstraneni je v strome: " + aMyRBStrom.getaPocetPrvkov());
        System.out.println("*******************TEST ODSTRANOVANIE KONIEC**********************\n");
    }

    /**
     * Test of odstran method, of class RBStrom.
     */
    @Test
    public void testNahodnePridavanieAOdoberanie() {
        System.out.println("\n***********************TEST PRIDAVANIE-ODSTRANOVANIE*************************");
        int pocetVykonOperacii = aGenerator.nextInt(40000);
        List<Integer> lpPridane = new ArrayList<>();
        System.out.println("Pokusim sa vykonat: "+pocetVykonOperacii);
        for (int i = 0; i < pocetVykonOperacii; i++) {
            int operacia = aGenerator.nextInt(2);
            if (operacia == 0) {//pridaj
                int pridKluc = aGenerator.nextInt(100000);
                aRBJava.put(pridKluc, 1);
                aMyRBStrom.pridaj(pridKluc, 1);
                lpPridane.add(pridKluc);
                assertTrue(aMyRBStrom.jePocetCiernychDoListovRovnaky());
                assertEquals(aMyRBStrom.getaPocetPrvkov(), aRBJava.size());
            } else if (lpPridane.size()>0&&operacia == 1) {//odstran
                int odstr = aGenerator.nextInt(lpPridane.size());
                aRBJava.remove(lpPridane.get(odstr));
                aMyRBStrom.odstran(lpPridane.get(odstr));
                assertTrue(aMyRBStrom.jePocetCiernychDoListovRovnaky());
                assertEquals(aMyRBStrom.getaPocetPrvkov(), aRBJava.size());
            } else if(operacia>1){
                fail("Vygenroval som cislo, ktoremu nie je pridelena operacia"+operacia);
            }
        }
        assertEquals(aMyRBStrom.getaPocetPrvkov(), aRBJava.size());
        assertTrue(aMyRBStrom.skontrolujDvaCerveneZaSebou());
        System.out.println("*******************TEST PRIDAVANIE-ODSTRANOVANIE KONIEC**********************\n");
        List<Vrchol> inOrderMyTree=BVSAlgoritmy.getInorder(aMyRBStrom.getKoren());
        NavigableSet<Integer> paSet=aRBJava.descendingKeySet();
        int pocitadlo=inOrderMyTree.size()-1;
        for (Iterator<Integer> iterator = paSet.iterator(); iterator.hasNext();) {
            Integer next = iterator.next();
            if(!next.equals(inOrderMyTree.get(pocitadlo).getaKluc())){
                fail("Kontrola inOrder zlyhala");
            }
            pocitadlo--;
        }
    }
}

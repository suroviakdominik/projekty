package com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.suroviak.dominik.zooAplication.Adapters.ExpandableListAdapterActuality;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dominik on 9.2.2015.
 */
public class AktualityFragment extends Fragment {
    private String[] names_of_categories = null;
    private String aNameOfRootDownloadDir;
    private Info_About_Dirs_File aInfoFileActualitues;
    ExpandableListAdapterActuality listAdapter;
    ExpandableListView expListView;
    List<Info_About_Item> listDataHeader;
    HashMap<Info_About_Item, List<String>> listDataChild;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_aktuality_main_menu, container, false);


        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.NEWS.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.NEWS.getaNameOfInfoFile());
        try {
            aInfoFileActualitues = (Info_About_Dirs_File) sw.readObjetctFromFile();
            prepareListData();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        // get the listview
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        // preparing list data


        listAdapter = new ExpandableListAdapterActuality(this.getActivity(), listDataHeader, listDataChild);


        expListView.setAdapter(listAdapter);

        return rootView;
    }


    /*
     * Preparing the list data
     */
    private void prepareListData() throws IOException {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding child data
        int i = 0;
        for (String prech : aInfoFileActualitues.getaItemsInfo().keySet()) {
            listDataHeader.add(aInfoFileActualitues.getaItemsInfo().get(prech));
            List<String> top250 = new ArrayList<String>();
            FileWorker fw = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                    + aInfoFileActualitues.getaItemsInfo().get(prech).getRelativePathOfFilesOfurrentItemInDevice() + "popis.txt");
            top250.add(fw.readDataFromFile());
            listDataChild.put(listDataHeader.get(i), top250);
            i++;
        }

    }
}

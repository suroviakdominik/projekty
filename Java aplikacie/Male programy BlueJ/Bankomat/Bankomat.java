    

public class Bankomat
{
    public static final int id5=0;
    public static final int id10=1;
    public static final int id20=2;
    public static final int id50=3;
    public static final int id100=4;
    public static final int id200=5;
    public static final int id500=6;
    
    public static final int[] NominalneHodnoty={5,10,20,100,200,500};
    private int[] aPoctyBankoviek;
    private Adresa aAdresa;
    
    public Bankomat(Adresa paAdresa,//adresa je podriadena bankomatu(agregacia).Bankomat jednej banky mozr byt umiestneny na viacerych adresach.
                    int paPocet5,
                    int paPocet10,
                    int paPocet20,
                    int paPocet50,
                    int paPocet100,
                    int paPocet200,
                    int paPocet500)
    {
         aAdresa=paAdresa;
        
         aPoctyBankoviek= new int[7];
        
         aPoctyBankoviek [id5] = paPocet5;
         aPoctyBankoviek [id10] = paPocet10;
         aPoctyBankoviek [id20] =  paPocet20;
         aPoctyBankoviek [id50] =  paPocet50;
         aPoctyBankoviek [id100] =  paPocet100;
         aPoctyBankoviek [id200] =  paPocet200;
         aPoctyBankoviek [id500] =  paPocet500;
    }
    
    public Adresa getAdrresu()
    {
        return aAdresa;
    }
    
    public void pridajBankovky(int paIndex,
                               int paKolko)
    {
        aPoctyBankoviek[paIndex]+=paKolko;
    }
    
    public int pocetBankoviek(int paIndex)
    {
        return aPoctyBankoviek[paIndex];
    }
  
    public int sumaBankoviek(int paIndex)
    {
        return aPoctyBankoviek[paIndex] *NominalneHodnoty[paIndex];
    } 
   
    public int sumaVBankomate()
    {
        int suma=0;
        for (int i=id5;i<=id500;i++)
        {
            suma+=sumaBankoviek(i);
        }    
        return suma;
    }

    public int minVBankomate()
    {
       for (int i=id5;i<=id500;i++)
       {
           if (aPoctyBankoviek[i]!=0)
           return NominalneHodnoty[i];
           
       }
       return 0;
    } 
    
    public int[] vyberSumy(int paSuma)
    {
        int[] nic=new int[7];
        int [] vysledok= new int [7];
        if ((paSuma%5==0) && (paSuma>=minVBankomate()) && (paSuma<=sumaVBankomate()))
        {
            int i=id500;
            while((paSuma>0) && (i>=id5))
            {
               if ((paSuma>=NominalneHodnoty[i]) && (pocetBankoviek(i)>0))
               {
                   vysledok[i]++;
                   aPoctyBankoviek[i]--;
                   paSuma-=NominalneHodnoty[i];
               }  
               else
                  i--;
            }    
               
            if (paSuma!=0)
               {
                   for (int j=id5;j<=id500;i++)
                   {
                       aPoctyBankoviek[i]+=vysledok[i];
                   }
                   return nic;
               }
               
               else 
               {
                   return vysledok;
               }
           }
        
        else 
        {
          return vysledok;
        }
    }
}
        
        /**
        Skusane riesenie doma:
        if (paKolko>sumaVBankomate())
        {
            System.out.println("Pozadovana suma sa v bankomate nenachadza");
        }
        
        else if (paKolko==sumaVBankomate())
        {
           int lpVybrataSuma=sumaVBankomate();
            for (int i=id5;i==id500;i++)
          {
             aPoctyBankoviek[i]=0;
          } 
          return lpVybrataSuma;
        }  
        else if  (paKolko<sumaVBankomate())
        {
           int novaSuma=0;
           for (int i=id5;i==id5;i++)
           {
                 for (int j=1;novaSuma<=paKolko;j++)
               {   
                    novaSuma+=aPoctyBankoviek[i]*NominalneHodnoty[j];
               }   
             return novaSuma;     
           } 
       
        }
        return 0;
        */
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
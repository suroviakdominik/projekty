/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;

/**
 *
 * @author Dominik
 */
public class VykonavacProfil implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        System.out.println("Meno: " + paHrac.getMeno());
        System.out.println("Život: " + paHrac.dajZivot());
        System.out.println("Peniaze: " + paHrac.getaPenazenka());
        System.out.println("Skúsenosti :" + paHrac.dajExperience());
        System.out.println(paHrac.zobrazInventar());

        return false;
    }

}

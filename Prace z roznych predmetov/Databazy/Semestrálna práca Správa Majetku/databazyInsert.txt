create or replace procedure insert_aktivita(
cas_uskutocnenia varchar2,
osobne_cislo integer,
id_typu_aktivity integer,
id_materialu integer,
id_majetok integer,
cena number,
popis varchar2,
pocet_materialu integer,
trvanie_aktivity integer
) as
begin
  insert into aktivita
  values(to_date(cas_uskutocnenia,'DD.MM.YYYY'),osobne_cislo,id_typu_aktivity,id_materialu,id_majetok,cena,popis,pocet_materialu,trvanie_aktivity);
end;

create or replace procedure insert_majetok(
id_oddelenia integer, 
nakupna_cena number,
datum_nakupu varchar2,
zivotnost number,
zaruka number,
servisne_obdobie number,
id_typu_majetku number,
popis Varchar2
) as
begin
  insert into majetok(id_oddelenia,nakupna_cena,datum_nakupu,zivotnost,zaruka,sevisne_obdobie,id_typu_majetku,popis) 
  values(id_oddelenia,nakupna_cena,to_date(datum_nakupu,'DD.MM.YYYY'),zivotnost,zaruka,servisne_obdobie,id_typu_majetku,popis);
end;

create or replace procedure insert_material(
id_oddelenia integer,
id_typu_materialu integer,
nakupna_cena number,
pocet integer
) as
begin
  insert into material_na_aktivitu values(null,id_oddelenia,id_typu_materialu,nakupna_cena,pocet);
end;

create or replace procedure insert_osoby(pa_rc osoby.rodne_cislo%type, meno Varchar2, priezvisko Varchar2) as
begin
  insert into osoby values(Person(pa_rc,meno,priezvisko));
end;

create or replace procedure insert_pracovnik(id_typu_prac Integer,rodne_cislo osoby.rodne_cislo%TYPE) as
begin
  insert into pracovnik values((SELECT MAX(osobne_cislo)+1 FROM pracovnik),id_typu_prac,rodne_cislo);
end;

create or replace procedure insert_tlaciaren(
id_oddelenia integer, 
nakupna_cena number,
datum_nakupu varchar2,
zivotnost number,
zaruka number,
servisne_obdobie number,
id_typu_majetku number,
popis Varchar2,
typ varchar2,
pamodel varchar2
) as
begin
 insert into majetok(id_majetok,id_oddelenia,nakupna_cena,datum_nakupu,zivotnost,zaruka,sevisne_obdobie,id_typu_majetku,popis) 
  values((SELECT MAX(id_majetok)+1 FROM majetok),id_oddelenia,nakupna_cena,to_date(datum_nakupu,'DD.MM.YYYY'),zivotnost,zaruka,servisne_obdobie,id_typu_majetku,popis);
  insert into infotlaciaren values((SELECT MAX(id_majetok) FROM majetok),xmltype('<info><typ>'||typ||'</typ><vytlacene>0</vytlacene><model>'||pamodel||'</model></info>'));
end;

create or replace procedure insert_typ_aktivity(pa_popis Varchar2) as
begin
  insert into typ_aktivity values((select MAX(typ_aktivity.ID_TYPU_AKTIVITY)+1 from typ_aktivity), pa_popis);
end;

create or replace procedure insert_typ_majetok(pa_popis Varchar2) as
begin
  insert into typ_majetku values((select MAX(id_typu_majetku)+1 from Typ_majetku), pa_popis);
end;

create or replace procedure insert_typ_materialu(pa_popis Varchar2,jeSpotrebny Varchar2) as
begin
  insert into typ_materialu values((select MAX(id_typu_materialu)+1 from typ_materialu),pa_popis,jeSpotrebny);
end;

create or replace procedure insert_typ_oddelenia(pa_popis Varchar2) as
begin
  insert into typ_oddelenia values((select MAX(id_typu_odd)+1 from Typ_oddelenia), pa_popis);
end;

create or replace procedure insert_typ_pracovnika(pa_popis Varchar2) as
begin
  insert into typ_pracovnika values((select MAX(id_typu_prac)+1 from Typ_pracovnika), pa_popis);
end;

create or replace procedure insert_zodpovednost(od_kedy Varchar2, osobne_cislo integer, id_majetok Integer, do_kedy Varchar2) as
begin
  insert into zodpovednost values(to_date(od_kedy,'DD.MM.YYYY'), osobne_cislo, id_majetok, to_date(do_kedy,'DD.MM.YYYY'));
end;


create or replace procedure odstran_vsetky_kontakty_Osobe(pa_rc Varchar2)AS
osa person;
begin
  select value(p) into osa from osoby p where p.rodne_cislo = pa_rc;
  
  update osoby 
  set kontakty = pole_kontaktov()
  WHERE rodne_cislo = pa_rc;
end;

create or replace procedure Pridaj_Kontakt_Osobe(pa_rc Varchar2, typ_kont Varchar2, kontakt Varchar2)AS
os pole_kontaktov;
osa person;
begin
  select value(p) into osa from osoby p where p.rodne_cislo = pa_rc;
  osa.pridaj_kontakt(os,typ_kont, kontakt);
  update osoby 
  set kontakty = os
  WHERE rodne_cislo = pa_rc;
end;

create or replace PROCEDURE vypis_kontaktov_osoby(p_cursor OUT types.ref_cursor, pa_rod_cislo Varchar2) AS 
BEGIN
   open p_cursor for
      select meno,priezvisko,value(p).vypis_kontaktov() as kontakty from osoby p
      where p.rodne_cislo = pa_rod_cislo;
END;

create or replace procedure vyrad_majetok(pa_id_majetok varchar2)
is
begin
  update majetok
  set datum_vyradenia = sysdate,
      stav_majetku = 'V'
  where id_majetok = pa_id_majetok;
end;

create or replace type Person is Object(
rodne_cislo Char(11),
meno Varchar2(50),
priezvisko Varchar2(50),
kontakty pole_kontaktov,
CONSTRUCTOR FUNCTION Person( p_rod_cislo Varchar2,p_meno varchar2, p_priezvisko varchar2) RETURN SELF AS RESULT,
MEMBER FUNCTION vypis return Varchar2,
MEMBER FUNCTION vypis_kontaktov return Varchar2,
Member PROCEDURE pridaj_kontakt(pole in out pole_kontaktov,typ Varchar2, kont Varchar2),
Member PROCEDURE odober_kontakt(p_index integer)
);

create or replace type pole_kontaktov is table of t_kontakt;

create or replace TYPE kontakt as OBJECT(
typ varchar2(20),
hodnota varchar2(50));

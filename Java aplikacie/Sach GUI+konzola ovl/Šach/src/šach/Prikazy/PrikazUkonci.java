/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import šach.hra.Hrac;

/**
 * Objekt triedy sa zavolá po zadaní príkazu ukonci. Objekt ukončuje hru.
 *
 * @author Dominik
 */
public class PrikazUkonci implements IPrikaz {

    /**
     * nastaví modelu hráča atribút aJeStále vo hre na false. Vždy vráti true,
     * pretože načítavanie príkazov po tomto príkaze končí.
     *
     * @param paHracKtoryZadavaPrikazy -hráč, ktorý je na ťahu
     * @param lpParameter -žiadny
     * @return true-vždy
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {
        paHracKtoryZadavaPrikazy.getaProtiHrac().setaProtiHrac(null);
        return true;
    }

}

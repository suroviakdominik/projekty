/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heuristika;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import javax.swing.JComponent;

/**
 *
 * @author Dominik
 */
public class Platno extends JComponent
{
    Batoh batoh;        //  atribut problemu
    
    
    public Platno( Batoh paBatoh ) 
    {
        this.batoh = paBatoh;   
    }
        
    public void paintComponent(Graphics g)    // MUSI sa volat presne takto
    {
        Graphics2D gg = (Graphics2D) (g);
        
          //  vykreslenie nasho problemu
           Dimension d = getSize();
           Point p = getLocation();
           if(batoh.jeInicializovany())
           batoh.zobrazSa( gg, p.x, p.y,d.width,d.height);
        gg.setFont( new Font( Font.MONOSPACED, Font.BOLD, 12) );//nastavi typ pisma
        gg.setColor(Color.DARK_GRAY);
        gg.drawString("Dominik Suroviak", getWidth()-120, getHeight()-20 );
    
    }
}

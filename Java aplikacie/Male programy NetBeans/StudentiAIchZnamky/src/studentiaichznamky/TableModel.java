/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentiaichznamky;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Dominik
 */
public class TableModel extends AbstractTableModel {

    private List<RiadokTabulky> aDataTabulky;

    public TableModel() {
        aDataTabulky = new ArrayList<>();
    }

    public void setaDataTabulky(List<RiadokTabulky> aDataTabulky) {
        this.aDataTabulky = aDataTabulky;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return RiadokTabulky.NazvyStlpcov.values()[column].getaNazovStlpca();
    }

    @Override
    public int getRowCount() {
        return aDataTabulky.size();
    }

    @Override
    public int getColumnCount() {
        return RiadokTabulky.NazvyStlpcov.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return RiadokTabulky.NazvyStlpcov.values()[columnIndex].dajHodnotu(aDataTabulky.get(rowIndex));
    }

}

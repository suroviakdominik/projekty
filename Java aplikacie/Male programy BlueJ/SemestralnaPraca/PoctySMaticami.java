import java.util.Scanner;
public class PoctySMaticami
{
    private Matica aMatica;
    private double [][] copyPole;
    private  ElementarneRiadkoveOperacie Ero;
    private  ElementarneStlpcoveOperacie Eso;
    public PoctySMaticami(Matica paMatica)
    {
        aMatica=paMatica;
        Ero = new  ElementarneRiadkoveOperacie(this);
        Eso= new ElementarneStlpcoveOperacie(this);
    }

    public int dajPocetRiadkov()
    {
        return copyPole.length;
    }

    public int dajPocetStlpcov()
    {
       return copyPole[0].length;
    }

    public double zistiHodnotuCopyPola(int i,int j)
    {
        return copyPole[i][j]; 
    }

    public void vlozHodnotuDoCopyPola(int i,int j,double hodnota)
    {
        copyPole[i][j]=hodnota;
    }

    public void nacitajcopyPole()
    {
        Scanner scan=new Scanner(System.in);
        for (int i=0;i<dajPocetRiadkov();i++)
        {
            for (int j=0;j<dajPocetStlpcov();j++)
            {
                copyPole[i][j]=scan.nextDouble();
            }
        }
    }

    
    public void vypisCopyPola()
    {
        for (int i=0;i<dajPocetRiadkov();i++)
        {
            for (int j=0;j<dajPocetStlpcov();j++)
            {
                System.out.print(copyPole[i][j]+"\t");
            }
            System.out.println();  
        }
    }

    public void upravMaticuNaTrojuholnikovuMaticu()
    {  
        copyPole=new double[aMatica.dajPocetRiadkov()][aMatica.dajPocetStlpcov()];
        for (int i=0;i<aMatica.dajPocetRiadkov();i++)
        {
            for (int j=0;j<aMatica.dajPocetStlpcov();j++)
            {
                copyPole[i][j]=aMatica.dajPrvokPola(i,j);
            }
        }

        int lpPokialPojdem;
        if(dajPocetRiadkov()>dajPocetStlpcov())
        {
            lpPokialPojdem=dajPocetStlpcov();
        }
        else
        {
           lpPokialPojdem=dajPocetRiadkov();
        }
        
        
        for(int i=0;i<lpPokialPojdem;i++)
        {
            
            if ((copyPole[i][i]!=0))
            { 
                for (int j=(i+1);j<dajPocetRiadkov();j++)
                {
                    Ero.pripocitajNasoboItehoRiadkuKJetemuRiadku(i,j,i);
                }
            }

            else 
            {
                int r;
                for (r=(i+1);r<dajPocetRiadkov();r++)
                {
                    if (copyPole[r][i]!=0)
                    {
                        Ero.vymenRiadky(i,r);
                        --i;
                        break;
                    }
                    else if((r+1)==dajPocetRiadkov())
                    {
                        for (int s=(i+1);s<dajPocetStlpcov();s++)
                        {
                            if (copyPole[i][s]!=0)
                            {
                                Eso.vymenStlpce(i,s);
                                --i;
                                break;
                            }
                        }
                    }
                }
            }
           
        }
        System.out.println("Trojuholníková matica je: ");
        vypisCopyPola();
    }

    public void vypocitajDeterminantMatice()
    {
        copyPole=new double[aMatica.dajPocetRiadkov()][aMatica.dajPocetStlpcov()];
        for (int i=0;i<aMatica.dajPocetRiadkov();i++)
        {
            for (int j=0;j<aMatica.dajPocetStlpcov();j++)
            {
                copyPole[i][j]=aMatica.dajPrvokPola(i,j);
            }
        }
        double vysledok=1;
        if (dajPocetRiadkov()==dajPocetStlpcov())
        {
            for (int r=0;r<dajPocetRiadkov();r++)
            {
                int i=r;
                int pocitadlonenulovychriadkov=0; 
                if (copyPole[r][i]!=0)
                {
                    for (int j=i+1;j<dajPocetStlpcov();j++)
                    {
                        Eso.pripocitajNasoboItehoStlpcaKJetemuStlpcu(i,j,i);
                    }

                }

                else 
                {
                    int a;
                    for (a=(r+1);a<dajPocetRiadkov();a++)
                    {
                        if (copyPole[a][r]!=0)
                        {
                            Ero.vymenRiadky(r,a);
                            ++pocitadlonenulovychriadkov;
                            vysledok*=(-1);
                            break;
                        }
                        else if((a+1)==dajPocetRiadkov())
                        {
                            vysledok*=(-1);
                            for (int s=(r+1);s<dajPocetStlpcov();s++)
                            {
                                if (copyPole[r][s]!=0)
                                {
                                    Eso.vymenStlpce(s,r);
                                    ++pocitadlonenulovychriadkov;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (pocitadlonenulovychriadkov!=0)
                {
                    --r;
                }
                else
                {
                    for (int s=r+1;s<dajPocetStlpcov();s++)
                    {
                        copyPole[s][i]=0;
                    }
                }

            }
            for (int i=0;i<dajPocetRiadkov();i++)
            {
                vysledok*=copyPole[i][i];
            }
            System.out.println();
            System.out.println("Veľkosť determinantu vami zadanej matice je: "+Zaokruhli.zaokruhluj(vysledok)); 
            
            }
        else 
        {
            System.out.println("Vami zadaná matica nie je štvorcová. Determinant je možné vypočítať len zo štvorcovej matice");
        }
    }

    public void scitajMatice()
    {
        copyPole=new double[aMatica.dajPocetRiadkov()][aMatica.dajPocetStlpcov()];
        System.out.println("Zadajte maticu ktorú chcete pripočítať k prvej matici");
        nacitajcopyPole();
        for (int i=0;i<dajPocetRiadkov();i++)
        {
            for (int j=0;j<dajPocetStlpcov();j++)
            {
                copyPole[i][j]=aMatica.dajPrvokPola(i,j)+copyPole[i][j];     
            }
        }
        System.out.println("Výsledok: ");
        vypisCopyPola();
    }

    public void nasobenieMatic(int paPocetStlpcov)
    {
        copyPole=new double[aMatica.dajPocetRiadkov()][paPocetStlpcov];
        Matica cinitel=new Matica(dajPocetRiadkov(),paPocetStlpcov); 
        System.out.println("Zadajte prvky druheho pola");
        Scanner scan=new Scanner(System.in);
        for (int i=0;i<dajPocetRiadkov();i++)
        {
            for(int j=0;j<paPocetStlpcov;j++)
            {
               cinitel.vlozPrvokDoMatice(i,j,scan.nextInt());  
            }
        }
        
        for(int k=0;k<dajPocetRiadkov();k++)
        {
         double vysledok=0;   
            for(int l=0;l<paPocetStlpcov;l++)
            {
                for (int m=0;m<dajPocetRiadkov();m++)
                {
                    vysledok=vysledok+(aMatica.dajPrvokPola(k,m)*cinitel.dajPrvokPola(m,l));
                }
                copyPole[k][l]=vysledok;
                vysledok=0;
            }
        }
        System.out.println("Výsledok: ");
        vypisCopyPola();
    }
}

    






































.MODEL small
.STACK 100h
.DATA
Retaz DB 10 dup(?)
Postupnost DW 10 dup (?)
SkonvertovaneCisloSBinNaAscii DB 10 dup(?)
JednoCisloNaKonv DB 6 dup (?)
MenoSuboru DB 'Subor.txt',0
PopisovacSuboru DW (-1)
errorOtvSub DB 'Subor sa nepodarilo pre citanie otvorit!$'
DesiatPrinas DW 10
.CODE
;-----------------------------------PROCEDURA OTVORI SUBOR PRE CITANIE,VSTUPNY PARAMETER-offset MenaSuboru do DX---------------------------------------
OpenFileForRead PROC 
        push ax
        mov al,0
        mov ah,3Dh
        int 21h
        mov PopisovacSuboru,ax;ak subor s menom este neexistuje v ax bude 2, pri pokuse citania zo suboru sa bude citat z klavesnice
        lahf;skopiruje register priznakov flags do registra ah
        AND ah,00000001b;vsetky ostatne az na carryflag vynuluje, ak cf=1 subor pre citanie sa nepodarilo otvorit
        
        cmp ah,1; ak je cf=1 vypise chybovu hlasku
        je VypisChybHlasku
        
        
        pop ax
        ret
  
 VypisChybHlasku:
        mov dx,offset errorOtvSub
        mov ah, 9
        int 21h 
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h 
        
        pop ax
        ret
        
OpenFileForRead ENDP

;------------------------------------------------PROCEDURA KONVERZIA CISLA V ASCII TYPU WORD KODE NA BINARNE CISLO----------------------------------------------
KonverziaZDesCislaNaDvojCislo PROC
;vstupnymi parametrami su OFFSET retazec s ascii kodmi ukonceny koncovym znakom 0Dh do registra BX a offset od ktoreho zacinaju cisla v retazci do registra DI
;skonvertovne cislo vracia v registri ax
          push cx
          push dx
          push di
          mov dx,0
          mov ax,0
          cmp byte ptr [bx+di],'-'
          jz CisloJeZaporne
          mov dl,0 
          
KonvertujRetazecNaCislo:
           
          
          mov cl,[bx+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          push dx
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          pop dx
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
          

CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov dl, 1
          jmp KonvertujRetazecNaCislo

RozhodniCiJeCisloZaporne:
          cmp dl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
NavratSProc:
          pop di
          pop dx 
          pop cx
          ret
DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla
         jmp NavratSProc

KonverziaZDesCislaNaDvojCislo ENDP

;-----------------------------------------PROCEDURA KONVERZIA CISLA V BINARNOM TVARE NA DESIATKOVE CISLO V ASCII KODE---------------------------------
;zapise binarne cislo do retazca/pola a to tak ze ho prekonvertuje na desiatkove cislo v ascii kode
;vstupny parameter je offset retazca, ktory konvertujeme v registri BX, binarne cislo ktore chceme skonvertovat v registri AX
;a index v poli od ktoreho sa bude do pola zapisovat cislo v registri di
; v registri dx vracia pocet vlozenych znakov
KonverziaZBinCislaNaDesiat PROC
         push ax
         push bx
         push cx
         push si
         push di
         mov cx,0
         cmp ax,0
         mov dx,0
         mov si,0
         jnge VypisMinus
KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc si
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
          
          
         
VypisCislo:
        
        pop ax;vypise skonvertovane do retazca ktory je ako parameter
        add ax,'0'
        mov byte ptr [bx+di],al
        inc di
        loop VypisCislo
        mov byte ptr [bx+di],0Dh
        mov dx,si
        pop di
        pop si
        pop cx
        pop bx
        pop ax
        ret

VypisMinus:
        mov byte ptr [bx+di],'-'
        inc di
        neg ax
        inc si
        jmp KonvertujNaDekCislo
KonverziaZBinCislaNaDesiat ENDP

;-----------------------------------------------------------HLAVNY PROGRAM------------------------------------------------------
Zaciatok:
        mov ax,@DATA
        mov ds,ax
       ;---------------------------------OTVORENIE SUBORU PRE CITANIE POMOCOU PROCEDURY----------------------------------------
        mov dx, offset MenoSuboru
        call OpenFileForRead
        
        
        ;--------------------------------CITANIE ZO SUBORU---------------------------------------------------
        ;Retaz naplni ascii znakmi zo suboru
        mov bx,PopisovacSuboru;ak je popisovac suboru 2, tak znaky musime zadat z klavesnice lebo subor sa pre citanie nepodarilo otvorit
        mov cx,7;pocet bytov, ktore sa budu kopirovat do retazca(vratane medzier=1B)
        mov dx, offset Retaz;offset pola kde sa budu znaky kopirovat
        mov ah,3Fh
        int 21h 
        
        
       ;-----------------------------------KOnverzia cisel nacitanych zo suboru na binarne cisla a ich ulozenie do Postupnost----------------------
        
        mov Retaz[7],0;na koniec nacitaneho retazca dame 0 aby sme vedeli kde retaz  Konci
        mov si,0
        mov bx,0
        KonvertAsciiToBin:
               cmp Retaz[si],0
               je KonKonvert
               mov cx,0
              
              KoniecCisla:
              cmp Retaz[si],' '
              je Konvert
              mov dl,Retaz[si]
              push si
              mov si,cx
              mov [JednoCisloNaKonv+si],dl
              pop si
              inc si
              inc cx
              jmp KoniecCisla
              
              Konvert:
                  push si
                  mov si,cx
                  mov JednoCisloNaKonv[si],0Dh
                  pop si
                  push bx
                  mov bx,offset JednoCisloNaKonv
                  mov di,0
                  call KonverziaZDesCislaNaDvojCislo
                  pop bx
                  mov Postupnost[bx],ax
                  mov cx,0
                  add bx,2
                  inc si
                  jmp KonvertAsciiToBin
        KonKonvert:
              mov bx, offset SkonvertovaneCisloSBinNaAscii
              mov di,0
              call KonverziaZBinCislaNaDesiat
              mov si,0
              mov cx,dx
        VypZoSuboru:
               mov dl,SkonvertovaneCisloSBinNaAscii[si]
               mov ah,2
               int 21h
               inc si
               loop VypZoSuboru
        ;-------------------------------ZATVORENIE SUBORU PRE CITANIE----------------------------------------
         mov bx,PopisovacSuboru
         mov ah,3Eh
         int 21h
        
       ;------------------------------CYKLUS NA VYPIS ZNAKOV Z POLA Retaz-----------------------------------
       ; mov di,0
        ;mov cx,8
        ;Cyklus:
            ;mov dl,[Retaz+di]
            ;mov ah,2
            ;int 21h
            ;inc di
            ;loop Cyklus
             
        
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok

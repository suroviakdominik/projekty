/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafika;

import java.awt.*;
import java.awt.geom.*;

/**
 * Kruh, s ktorym mozno pohybovat a nakresli sa na platno.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 1.0 (15 July 2000)
 */
public class Elipsa implements IGrafickeObjekty {

    private int aOsX;
    private int aOsY;
    private int aXLavyHorny;
    private int aYLavyHorny;
    private String aFarba;
    private boolean aJeViditelny;

    /**
     * Vytvor novy kruh preddefinovanej farby na preddefinovanej pozicii.
     */
    public Elipsa(int paOsX, int paOsY, int paXLavyHornyRoh, int paYLavyHornyRoh, String paFarba) {
        aOsX = paOsX;
        aOsY = paOsY;
        aXLavyHorny = paXLavyHornyRoh;
        aYLavyHorny = paYLavyHornyRoh;
        aFarba = paFarba;
        aJeViditelny = false;
    }

    /**
     * (Kruh) Zobraz sa.
     */
    @Override
    public void zobraz() {
        aJeViditelny = true;
        nakresli();
    }

    /**
     * (Kruh) Skry sa.
     */
    @Override
    public void skry() {
        zmaz();
        aJeViditelny = false;
    }

    /**
     * (Kruh) Posun sa vpravo o pevnu dlzku.
     */
    @Override
    public void posunVpravo() {
        posunVodorovne(20);
    }

    /**
     * (Kruh) Posun sa vlavo o pevnu dlzku.
     */
    @Override
    public void posunVlavo() {
        posunVodorovne(-20);
    }

    /**
     * (Kruh) Posun sa hore o pevnu dlzku.
     */
    @Override
    public void posunHore() {
        posunZvisle(-20);
    }

    /**
     * (Kruh) Posun sa dole o pevnu dlzku.
     */
    @Override
    public void posunDole() {
        posunZvisle(20);
    }

    /**
     * (Kruh) Posun sa vodorovne o dlzku danu parametrom.
     */
    @Override
    public void posunVodorovne(int paVzdialenost) {
        zmaz();
        aXLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Kruh) Posun sa zvisle o dlzku danu parametrom.
     */
    @Override
    public void posunZvisle(int paVzdialenost) {
        zmaz();
        aYLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Kruh) Posun sa pomaly vodorovne o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunVodorovne(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aXLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Kruh) Posun sa pomaly zvisle o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunZvisle(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aYLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Kruh) Zmen priemer na hodnotu danu parametrom. Priemer musi byt
     * nezaporne cele cislo.
     */
    public void zmenOsi(int paOsX, int paOsY) {
        zmaz();
        aOsX = paOsX;
        aOsY = paOsY;
        nakresli();
    }

    /**
     * (Kruh) Zmen farbu na hodnotu danu parametrom. Nazov farby musi byt po
     * anglicky. Mozne farby su tieto: cervena - "red" zlta - "yellow" modra -
     * "blue" zelena - "green" fialova - "magenta" cierna - "black" biela -
     * "white" hneda - "brown"
     */
    @Override
    public void zmenFarbu(String paFarba) {
        aFarba = paFarba;
        nakresli();
    }

    /*
     * Draw the circle with current specifications on screen.
     */
    private void nakresli() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.draw(this, aFarba, new Ellipse2D.Double(aXLavyHorny, aYLavyHorny,
                    aOsX, aOsY));

        }
    }

    /*
     * Erase the circle on screen.
     */
    private void zmaz() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

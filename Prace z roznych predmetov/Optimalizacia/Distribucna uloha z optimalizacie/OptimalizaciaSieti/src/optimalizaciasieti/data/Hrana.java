/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti.data;

/**
 *
 * @author Dominik
 */
public class Hrana {
    private int aIDHrany;
    private Vrchol aZacVrhol;
    private Vrchol aKoncVrchol;
    private double aVzdialenost;

    public Hrana(int paIDHrany,Vrchol aZacVrhol, Vrchol aKoncVrchol) {
        this.aZacVrhol = aZacVrhol;
        this.aKoncVrchol = aKoncVrchol;
        aIDHrany=paIDHrany;
        aVzdialenost=-1;
    }

    public Hrana(int aIDHrany, Vrchol aZacVrhol, Vrchol aKoncVrchol, double aVzdialenost) {
        this.aIDHrany = aIDHrany;
        this.aZacVrhol = aZacVrhol;
        this.aKoncVrchol = aKoncVrchol;
        this.aVzdialenost = aVzdialenost;
    }
    
    

    public int getaIDHrany() {
        return aIDHrany;
    }

    public Vrchol getaZacVrhol() {
        return aZacVrhol;
    }

    public Vrchol getaKoncVrchol() {
        return aKoncVrchol;
    }

    public double getaVzdialenost() {
        return aVzdialenost;
    }

    public void setaVzdialenost(double aVzdialenost) {
        this.aVzdialenost = aVzdialenost;
    }
    
    

    @Override
    public String toString() {
        return "Hrana{"+aIDHrany + "\tZ:  " + aZacVrhol + "\t\tDO:   " + aKoncVrchol + "\t\t\t  ---Length: "+aVzdialenost+"}\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.aIDHrany;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hrana other = (Hrana) obj;
        if (this.aIDHrany != other.aIDHrany) {
            if(aZacVrhol==other.aZacVrhol&&aKoncVrchol==other.aKoncVrchol||
                    aZacVrhol==other.aKoncVrchol&&aKoncVrchol==other.aZacVrhol)
            {
                return true;
            }
            return false;
        }
        return true;
    }
    
    
    
}

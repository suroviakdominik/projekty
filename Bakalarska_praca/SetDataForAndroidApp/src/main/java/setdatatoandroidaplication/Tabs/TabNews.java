/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JTextFieldWithHint;
import MyGUIJComponent.JTextAreaWithHint;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import static java.awt.image.ImageObserver.WIDTH;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import setdatatoandroidaplication.DataTransferer;

/**
 *ktorá ma za úlohu editáciu a vytváranie nových aktualít. Aktuality popisujú aktuálne dianie v zoologickej záhrade
 * @author Dominik
 */
public class TabNews extends Tab {

    private JComboBox<Info_About_Item> aActualityList;
    private JTextAreaWithHint aTextArea = null;
    private JTextField aPathOfChoosenPicture = null;
    private JTextFieldWithHint aNameOfNewActuality = null;
    private JPanel aZobrazovacObrazkov = null;
    private Info_About_Item aLastSelectedActuality;
    
    public final int PREFFERED_HIGHT = 30, PREFFERED_WIDTH = 160, MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 20, MARGIN_RIGHT = 20;
    
   
     /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabNews(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNazovTabu,paCommandProcessingForTab);
        initializeMainTabPanelComponents();
        aDataTransferer=DataTransferer.getInstance();
    }

    /**
     * inicializuje vsetky vizualne komponenty tohto tabu
     */
    private void initializeMainTabPanelComponents() {
        JPanel strPanel = getMiddleMainTabPanel();
        strPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);

        c.gridy = 0;
        c.weightx = 0.33;

        addComponentForActualityCreate(strPanel, c, aGlobalKeyListener);

        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
        addComponentsToActualityPictureEditAndShow(strPanel, c, aGlobalKeyListener);
        c.weighty = 0.85;
        c.gridy = 2;
        c.gridx = 0;
        c.gridwidth = 2;
        c.weightx = 0.5;
        addComponentsForActualityDescriptionChange(strPanel, c, aGlobalKeyListener);
    }

    /**
     * prida vizualne komponenty v ramci ktorych mozne vyberat existujuce
     * a vytvarat nove aktuality
     * @param paMiddleMainTabPanel- panel, kde budu komponenty umiestnene
     * @param c - GridBagConstraint - rozlozovac komponentov
     * @param paKeyListener - globalny keylistener udalosti
     */
    private void addComponentForActualityCreate(JPanel paMiddleMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        try {
            Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
            JLabel label = new JLabel("Vyberte si jednu z existujúcich aktualít :");
            label.setFont(FONT);
            label.setPreferredSize(d);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            Border border = BorderFactory.createLineBorder(Color.BLACK);
            label.setBorder(BorderFactory.createCompoundBorder(border,
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            paMiddleMainTabPanel.add(label, c);

            aActualityList = new JComboBox<Info_About_Item>();

            updateActualities();
            aActualityList.setEditable(false);
            aActualityList.setPreferredSize(d);
            aActualityList.addKeyListener(paKeyListener);
            aActualityList.setFont(FONT);
            paMiddleMainTabPanel.add(aActualityList, c);
            aActualityList.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        if (aTextArea.boliZmenyUlozene()) {
                            updatePicture();
                            updateTextArea();

                        } else {
                            doYouWantToSaveDescriptionChanges();
                            updatePicture();
                            updateTextArea();
                        }
                        aPathOfChoosenPicture.setText("");
                        aLastSelectedActuality=(Info_About_Item)aActualityList.getSelectedItem();
                    }
                }
                
            });

            aNameOfNewActuality = new JTextFieldWithHint("Zadajte názov novej aktuality a stlačte ENTER: ");
            aNameOfNewActuality.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, PREFFERED_HIGHT));
            aNameOfNewActuality.setFont(FONT);
            paMiddleMainTabPanel.add(aNameOfNewActuality, c);

            aActualityList.getEditor().getEditorComponent().addKeyListener(paKeyListener);
            label.addKeyListener(paKeyListener);
            aNameOfNewActuality.addKeyListener(paKeyListener);
            aNameOfNewActuality.addKeyListener(new KeyAdapter() {

              
                @Override
                public void keyPressed(KeyEvent ke) {
                    if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                        aNameOfNewActuality.setIsTextNeeded(true);
                        if (aTextArea.boliZmenyUlozene()) {
                            actionAddNewActuality();
                        } else {
                            doYouWantToSaveDescriptionChanges();
                            actionAddNewActuality();
                        }
                    }
                }
            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "TabPridajZvieraChyba\n metóda:pridajComboBoxNazovvierata(...)\n" + e.getMessage(), "ERROR=Vypis Kategorie", JOptionPane.ERROR_MESSAGE);
        }

    }

     /**
     * prida vizualne komponenty v ramci ktorych mozne zobrazit a upravovat obrazok
     * aktuality
     * @param paMiddleMainTabPanel- panel, kde budu komponenty umiestnene
     * @param c - GridBagConstraint - rozlozovac komponentov
     * @param paKeyListener - globalny keylistener udalosti
     */
    private void addComponentsToActualityPictureEditAndShow(JPanel paMiddleMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        Dimension d = new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT);
        JButton bt1 = new JButton("Vyber obrázok");
        bt1.addKeyListener(paKeyListener);
        bt1.setFont(FONT);
        bt1.setPreferredSize(d);
        paMiddleMainTabPanel.add(bt1, c);

        aPathOfChoosenPicture = new JTextField();
        aPathOfChoosenPicture.addKeyListener(paKeyListener);
        aPathOfChoosenPicture.setEditable(false);
        aPathOfChoosenPicture.setPreferredSize(d);
        aPathOfChoosenPicture.setFont(FONT);
        paMiddleMainTabPanel.add(aPathOfChoosenPicture, c);

        bt1.setSize(100, 100);
        bt1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                actionChangePictureOfSelectedActuality();
            }
        });

        c.gridheight = 2;
        aZobrazovacObrazkov = new JPanel(new BorderLayout());
        JScrollPane pane = new JScrollPane(aZobrazovacObrazkov);
        pane.setPreferredSize(null);
        pane.setPreferredSize(new Dimension(2 * PREFFERED_WIDTH, WIDTH));
        pane.setBackground(Color.LIGHT_GRAY);
        paMiddleMainTabPanel.add(pane, c);
        updatePicture();
        c.gridheight = 1;
    }

     /**
     * prida vizualne komponenty v ramci ktorych mozne menit popis vybranej aktuality
     * @param paMiddleMainTabPanel- panel, kde budu komponenty umiestnene
     * @param c - GridBagConstraint - rozlozovac komponentov
     * @param paKeyListener - globalny keylistener udalosti
     */
    private void addComponentsForActualityDescriptionChange(JPanel paMiddleMainTabPanel, GridBagConstraints c, KeyListener paKeyListener) {
        aTextArea = new JTextAreaWithHint();
        //aTextArea.setColumns(30);
        //Dimension d = new Dimension(300, 300);
        //aTextArea.setPreferredSize(d);
        aTextArea.setLineWrap(true);
        aTextArea.addKeyListener(paKeyListener);
        aTextArea.setFont(FONT);

        JScrollPane lpScrollPaneOfTextArea = new JScrollPane(aTextArea);
        lpScrollPaneOfTextArea.addKeyListener(paKeyListener);
        lpScrollPaneOfTextArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        lpScrollPaneOfTextArea.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        aTextArea.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        paMiddleMainTabPanel.add(lpScrollPaneOfTextArea, c);
        updateTextArea();
        c.gridy = 4;
        c.weighty = 0.01;

        JButton btSaveText = new JButton("Ulož zmeny vykonané v texte");
        btSaveText.addKeyListener(paKeyListener);
        btSaveText.setFont(FONT);

        paMiddleMainTabPanel.add(btSaveText, c);
        btSaveText.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionChangeDescriptionOfSelectedActuality((Info_About_Item)aActualityList.getSelectedItem());
            }
        });

    }

    /**
     * zobrazi vo vybranom komponente vsetky ecistujuce aktuality
     */
    private void updateActualities() {
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.DAJDATA)) {
            aActualityList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aActualityList.addItem(prech);
            }
            
            updateTextArea();
            updatePicture();
            
        }
    }

    /**
     * zobrazi vo vybranom komponente popis vybranej aktuality
     */
    private void updateTextArea() {
        if (aTextArea != null && aActualityList.getSelectedItem() != null) {
            aTextArea.setDefaultText("Vložte popis vybranej aktuality");
            aTextArea.setEditable(true);
            aTextArea.setEnabled(true);
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_TEXT_ACTUALITY", (Info_About_Item)aActualityList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.DAJDATA)) {
                if (aDataTransferer.getString("RET_TEXT_ACTUALITY").length() != 0) {
                    aTextArea.setText(DataTransferer.getInstance().getString("RET_TEXT_ACTUALITY"));
                    aTextArea.setJeUznejakyTextnacitany(true);
                    aTextArea.setSelectionStart(0);
                    aTextArea.setSelectionEnd(0);
                } else {
                    aTextArea.setJeUznejakyTextnacitany(false);
                }
                aDataTransferer.removeAllData();
            } else {
                JOptionPane.showMessageDialog(this, "ERROR: Text_data was not load. ", "ERROW: Load text data", JOptionPane.ERROR_MESSAGE);
            }

        } else if (aTextArea != null) {
            aTextArea.setDefaultText("Vyberte objekt pre úpravu");
            aTextArea.setEditable(false);
            aTextArea.setEnabled(false);

        }
        this.getParent().repaint();
    }

    /**
     * zobrazi vo vybranom komponente obrazok vybranej aktuality, ak je 
     * k dispozicii
     */
    private void updatePicture() {
        if (aZobrazovacObrazkov != null && aActualityList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_IMAGE_ACTUALITY", (Info_About_Item) aActualityList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.DAJDATA)) {
                aZobrazovacObrazkov.removeAll();
                JLabel picLabel = new JLabel(new ImageIcon(DataTransferer.getInstance().getImage("RET_IMAGE_ACTUALITY")));
                aZobrazovacObrazkov.add(picLabel, BorderLayout.CENTER);
            } else {
                aZobrazovacObrazkov.removeAll();
            }
        } else if (aZobrazovacObrazkov != null) {
            aZobrazovacObrazkov.removeAll();
        }
        this.getParent().repaint();
    }

    
    /**
     * vytvori novu aktualitu
     */
    private void actionAddNewActuality() {

        aDataTransferer.addString("NEW_ACTUALITY", this.aNameOfNewActuality.getText());

        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.PRIDAJ)) {
            Info_About_Item newAcutality=aDataTransferer.getItemInfo("CREATED_ACTUALITY");
            aDataTransferer.removeAllData();
            updateActualities();
            aActualityList.setSelectedItem(newAcutality);
            aNameOfNewActuality.setIsTextNeeded(false);
            JOptionPane.showMessageDialog(this, "Actuality: " + this.aNameOfNewActuality.getText() + " was created.");

        } else {
            JOptionPane.showMessageDialog(this,
                    "Actuality can not be create, becouse of the text field is empty or actuality already exists.",
                    "New Actuality error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * zmeni obrazok vybranej aktuality
     */
    private void actionChangePictureOfSelectedActuality() {
        if (aActualityList.getSelectedItem() != null) {
            final JFileChooser chooser = new JFileChooser();

            chooser.setCurrentDirectory(new File(aLastVisitedDir));

            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.showOpenDialog(null);
            if (chooser.getSelectedFile() != null) {
                String path = chooser.getSelectedFile().getAbsolutePath();
                chooser.setCurrentDirectory(new File(path));
                String filename = chooser.getSelectedFile().getName();
                String separatedName[] = filename.split("\\.");
                if (separatedName.length == 2 && (separatedName[1].toLowerCase().equals("jpg"))
                        || (separatedName[1].toLowerCase().equals("png")) || (separatedName[1].toLowerCase().equals("gif"))) {
                    File f = new File(path);
                    long a = f.length() / 1000;
                    if (a < 600) {
                        try {
                            aLastVisitedDir = path;
                            aPathOfChoosenPicture.setText(path);
                            BufferedImage myPicture = ImageIO.read(new File(path));
                            aZobrazovacObrazkov.removeAll();
                            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
                            aZobrazovacObrazkov.add(picLabel, BorderLayout.CENTER);

                            aDataTransferer.addItemInfo("EDITED_ACTUALITY", (Info_About_Item) aActualityList.getSelectedItem());
                            aDataTransferer.addString("EDIT_PICTURE_PATH", path);
                            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.UPRAV)) {
                                this.getParent().repaint();
                            } else {
                                JOptionPane.showMessageDialog(this, "ERROR(chyba aplikácie): Obrázok sa nepodarilo nahrať.\n"
                                        + " Trieda: TabAktuality", "ERROR- Chyba aplikácie. ", JOptionPane.ERROR_MESSAGE);

                            }

                        } catch (IOException ex) {
                            Logger.getLogger(TabAnimalsInZoo.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Obrázky môžu mať maximálnu veľkosť 600 kB.\n"
                                + " Vami vybraný obrázok má veľkosť: " + a
                                + " kB", "ERROR- Obrázok zaberá príliš mnoho miesta. ", JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(this, "Obrázky môžu byť iba formátu: jpg, png, gif", "ERROR- Zly formát obrázka", JOptionPane.ERROR_MESSAGE);
                }

            }
        } else {
            JOptionPane.showMessageDialog(this, "Nie je zvolená aktualita, ktorej obrázok je portrebné zmeniť. Najskôr vyberte aktualitu.");
        }

    }

    /**
     * zmeni popis vzbranej aktuality
     * @param paKtorejAktualite - aktualita, ktorej popis sa ma zmenit
     */
    private void actionChangeDescriptionOfSelectedActuality(Info_About_Item paKtorejAktualite) {
        if (aActualityList.getSelectedItem() != null) {
            aDataTransferer.addItemInfo("EDITED_ACTUALITY",  paKtorejAktualite);
            aDataTransferer.addString("EDITED_TEXT", aTextArea.getText());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.UPRAV)) {
                JOptionPane.showMessageDialog(this, "Text was successfully save.");
            } else {
                JOptionPane.showMessageDialog(this, "ERROR: Text was not save. ", "ERROW: Save text", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Actuality must be choosed, before you try to save text.",
                    "No actuality choosen",
                    JOptionPane.WARNING_MESSAGE);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aTextArea.setJeUznejakyTextnacitany(true);

    }

    /**
     * opyta sa nas ci chceme ulozit zmeny teextu predtym ako budu stratene
     * @param paKomu - aktualita, ktoremu sa maju ulozit zmeny
     */
    private void doYouWantToSaveDescriptionChanges() {
        if (JOptionPane.showConfirmDialog(this,
                "Zmeny v popise aktuality neboli uložené a po prepnutí budú stratené. \nChcete tieto dáta pred vykonaním akcie uložiť?", "Uloženie popisu",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            actionChangeDescriptionOfSelectedActuality(aLastSelectedActuality);
        }
        aTextArea.setBoliZmenyUlozene(true);
        aNameOfNewActuality.setIsTextNeeded(false);
    }

    /**
     * aktualizuje vsetky udaje nachadzajuce sa v tabe
     */
    @Override
    public void updateAllTabData() {
        Info_About_Item lpSelectedAnimal = null;

        if (aActualityList != null) {
            lpSelectedAnimal = (Info_About_Item) aActualityList.getSelectedItem();

        }
        updateActualities();
        if (lpSelectedAnimal != null) {

            aActualityList.setSelectedItem(aActualityList);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyosjavp;

import java.util.Scanner;

/**
 *
 * @author Dominik
 */
public abstract class ConsoleWorker {
    public static String nacitajDataZKonzoly(String paCoNacitat,String regex)
    {
        String ret="";
        System.out.println("---------Nacitanie udajov z konzoly-----------");
        System.out.println("Zadajte "+paCoNacitat+" :");
        System.out.print("> ");
        Scanner scan = new Scanner(System.in);
        if(scan.hasNextLine())
        {
            ret=scan.nextLine();
            if(ret.matches(regex))
            {
                System.out.println("----------------------------------------------");
                return ret;
            }
        }
        System.out.println("!!!!!!!!Vami zadane data nie su platne!!!!!!!!");
        System.out.println("----------------------------------------------");
        return null;
    }
}

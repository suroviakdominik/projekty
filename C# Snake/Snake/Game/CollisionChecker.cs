﻿using System;
using Microsoft.Xna.Framework;

namespace Snake.Game
{
    public class CollisionChecker
    {
        /// <summary>
        /// skontroluj ci sa hlava zrazila z jedlom
        /// </summary>
        /// <param name="food"></param>
        /// <param name="snakeFirstPart"></param>
        /// <returns></returns>
        public static Boolean foodCollision(Rectangle food, Rectangle snakeFirstPart)
        {
            return food.Intersects(snakeFirstPart);
        }

        /// <summary>
        /// skontroluj ci had nabural do sameho seba
        /// </summary>
        /// <param name="paSnake"></param>
        /// <returns></returns>
        public static Boolean snakeCollision(DrawableObjects.Snake paSnake)
        {
            DrawableObjects.Snake.SnakePartMovementInfo headOfSnake = paSnake.PositionsOfSnakeParts.First.Value;
            Rectangle headRect = new Rectangle((int)(headOfSnake.CurrentPartPosition.X), (int)headOfSnake.CurrentPartPosition.Y,
                    paSnake.Width-2, paSnake.Height-2);
            Rectangle partRect;
            int i = 0;
            foreach (var iter in paSnake.PositionsOfSnakeParts)
            {
                if (i > 3)
                {
                    partRect = new Rectangle((int)(iter.CurrentPartPosition.X), (int)iter.CurrentPartPosition.Y,
                    paSnake.Width-2, paSnake.Height-2);
                    if (headRect.Intersects(partRect))
                    {
                        return true;
                    }
                }
                i++;
            }
            return false;
        }
    }
}
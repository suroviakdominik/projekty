

public class VlastnostiRelacie
{
  
  private Relacie aRelacie;
  private int dajPocetRiadkov;
  private int dajPocetStlpcov;
  
  public VlastnostiRelacie(Relacie paRelacie)
  {
      aRelacie=paRelacie;
      dajPocetRiadkov=aRelacie.dajPocetPrvkovMnozinyA();
      dajPocetStlpcov=aRelacie.dajPocetPrvkovMnozinyB();
         
  }
           
  public boolean reflexivnost()
  {
    int pocetRiadkovAleboStlpcov;
    if (dajPocetRiadkov>dajPocetStlpcov)
    {
      pocetRiadkovAleboStlpcov=dajPocetStlpcov;
    }
    else
    {
       pocetRiadkovAleboStlpcov=dajPocetRiadkov;
    }
      
    for (int i=0;i<pocetRiadkovAleboStlpcov;i++)
    {
            if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,i)==0)
            {
                return false;
            }
    }
    return true;
  }  
  
  public boolean symetrickost()
  {
      for (int i=0;i<dajPocetRiadkov;i++)
      {
          for (int j=0;j<dajPocetStlpcov;j++)
          {
              if ((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i)!=aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)))
              {
                  return false;
              }
          }   
      }
      return true;
  }   
  
  public boolean antisymetrickost()
  {
     for (int i=0;i<dajPocetRiadkov;i++)
      {
          for (int j=0;j<dajPocetStlpcov;j++)
          {
              if ((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i)) && (aRelacie.dajPrvokPolaA(i)!=aRelacie.dajPrvokPolaB(j)))
              {
                  return false;
              }
          }   
      } 
     return true;
  }   
  
  public boolean asymetrickost()
  {
       for (int i=0;i<dajPocetRiadkov;i++)
      {
          for (int j=0;j<dajPocetStlpcov;j++)
          {
              if (((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i))) && (aRelacie.dajPrvokPolaA(i)==aRelacie.dajPrvokPolaB(j)))
              {
                  return false;
              }
          }   
      } 
     return true;
  }
  
  public boolean tranzitivnost()
  {
       for (int i=0;i<dajPocetRiadkov;i++)
       {
          for (int j=0;j<dajPocetStlpcov;j++)
          {
             if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1)
             {
               for (int k=i+1;k<dajPocetRiadkov;k++)
               {
                   if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(k,j)==1)
                  {
                      if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(k,j)!=aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,k))
                      {
                          return false;
                      }
                  }   
               }
             }
          }
       }     
       return true;
  } 
}
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package suboroviny;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 *
 * @author Hawk
 */
public class myFileReader {

    private String path = "";
    private File file = null;

    public myFileReader(String path) {
        this.path = path;
        this.file = new File(this.path);
    }

    public String ReadFile() {
        String fileContent = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               fileContent+=str+"\n";
            }
            in.close();
            } catch (IOException e) {
            System.out.println("Reading the file failed...PROBLEM?");
            }
        System.out.println("Your file has been read");
        return fileContent;
    }

    public void WriteFile(String content) {
        try {
            Writer writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);
            writer.close();
        } catch(IOException ioe) {
            System.out.println("Writing in to the file failed...PROBLEM?");
        }
        System.out.println("Your file has been written");    
    }

    public ArrayList<String> CountLines() {
        ArrayList<String> riadky = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               riadky.add(str);
            }
            in.close();
        } catch (IOException e) {}
        return riadky;
    }

    public int CountChars() {
        int numChars = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null) {
               numChars+=str.length();
            }
            in.close();
        } catch (IOException e) {}
        return numChars;
    }

    public void FindWords() {

        String text = this.ReadFile();
        boolean bool = true;
        String slovo = "";
        int i=0;
        
        while(bool) {
            
            if((text.charAt(i)!=' ') && (text.charAt(i)!='\n')) {
                slovo+=text.charAt(i);
                i++;
            } else {
                if(!(slovo.contentEquals("")||slovo.contentEquals("\n"))) {
                    // co robime so samotnym slovom
                    System.out.println(">>"+slovo+"<<");
                    //-----------------------------
                }
                i++;
                slovo="";
            }
            if(i==(text.length()-1)) {
                    if(!(slovo.contentEquals("")||slovo.contentEquals("\n"))) {
                    //ak by sme mali v bufferi dake slovo
                    System.out.println(">>"+slovo+"<<");
                    //-----------------------------------
                }
                bool=false;
            }
        }

    }
}


////JOptionPane.showInternalMessageDialog(this, "Mrzi nas to!","Skoda, skus stastie nabuduce!", JOptionPane.INFORMATION_MESSAGE);
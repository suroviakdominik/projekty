set pagesize 60
set linesize 123
column Predajna.nazov_predajne heading "Nazov Predajne" FORMAT A20;
column faktura.cislo_fa heading "Cislo Faktury" FORMAT A20;
column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad faktur vystavenych v jednotlivych predajniach' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia: " datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
Predajna.nazov_predajne,Faktura.cislo_fa 
FROM PREDAJNA JOIN ZAMESTNANEC USING(id_predajne) JOIN FAKTURA USING (OSOBNE_CISLO)
ORDER BY NAZOV_PREDAJNE;

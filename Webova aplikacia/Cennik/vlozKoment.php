<?php
session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId=0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
                $sql = "SELECT idKoment FROM komenty";
                $result = mysqli_query($this->conn, $sql);
                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row["idKoment"]>$this->biggestId)
                        {
                            $this->biggestId=$row["idKoment"];
                        }
                    }
                    
                } 
            }

    
    function vlozitKomentar() {
        $idUser = $_SESSION["login"];
        echo $idUser;
        $this->biggestId+=1;
        $text = htmlspecialchars($_POST["textKom"]);
        echo $text;
        $sql = sprintf("INSERT INTO komenty(idKoment,idUzivatel,text)
        VALUES ($this->biggestId, %d,'%s')",
                $this->conn->real_escape_string($idUser),
                $this->conn->real_escape_string($text));

        if ($this->conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

$db = new DBPraca();
$db->findBiggestId();
if(isset($_SESSION["login"]))
{
  $db->vlozitKomentar();
}
 else {
 
}
$db->ukonciPristup();
header("Location: index.html");
?>
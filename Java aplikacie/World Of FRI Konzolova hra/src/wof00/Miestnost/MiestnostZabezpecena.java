/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Miestnost;

import java.util.HashMap;
import wof00.Hra.Hrac;
import wof00.Osoby.IOsoba;
import wof00.Predmety.IPredmet;

/**
 *
 * @author suroviak3
 */
public class MiestnostZabezpecena extends Miestnost {

    public MiestnostZabezpecena(String paNazov, String paPopis) {
        super(paNazov, paPopis);
    }

    @Override
    public boolean jePristupna(Hrac paHrac) {
        if ((paHrac.dajPredmet("kluc") != null) && (paHrac.dajPoziciu().getaNazov().equals("vratnica"))) {
            String heslo = paHrac.dajPredmet("kluc").pouzi(paHrac);
            if (heslo.equals("Spravne")) {
                return true;
            } else {
                System.out.println("Zle heslo.");
            }

        } else if (paHrac.dajPredmet("kluc") == null) {
            System.out.println("Nemate kluc.");
        } else if (!paHrac.dajPoziciu().getaNazov().equals("vratnica")) {
            return true;
        }
        return false;
    }
}

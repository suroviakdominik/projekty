<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    

    function vypisPopis() {
        
        $sql = "SELECT id,meno FROM kategoria";
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo "<option value=\"$row[id]\">$row[meno]</option>";
            }
        } else {
            echo "0 results";
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

$db=new DBPraca();
$db->vypisPopis();
$db->ukonciPristup();

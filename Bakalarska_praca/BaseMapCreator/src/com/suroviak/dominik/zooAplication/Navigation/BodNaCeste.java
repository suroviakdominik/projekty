/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class BodNaCeste implements Serializable{

    private Coordinate aLatitude;
    private Coordinate aLongitude;
    private final List<Sused> aSusedneBodyTohtoBodu;
    private TypBodu aTypBodu=TypBodu.BODCESTYPRENAVIGACIUDEFAULT;
    private static final long serialVersionUID = 5L;
    

    public BodNaCeste(Coordinate aLatitude, Coordinate aLongitude,TypBodu paTypBodu) {
        this.aLatitude = aLatitude;
        this.aLongitude = aLongitude;
        aSusedneBodyTohtoBodu = new ArrayList<>();
        aTypBodu=paTypBodu;
    }

    public TypBodu getTypBodu()
    {
        return aTypBodu;
    }
    
    public void setTypBodu(TypBodu paTypBodu)
    {
        aTypBodu=paTypBodu;
    }
    
    public Coordinate getaLatitude() {
        return aLatitude;
    }

    public Coordinate getaLongitude() {
        return aLongitude;
    }

    public int getPocetSusedov() {
        return aSusedneBodyTohtoBodu.size();
    }

   
    public BodNaCeste getSusednyBod(int paIndexSusedaVPoli) {

        return aSusedneBodyTohtoBodu.get(paIndexSusedaVPoli).getSusednyBod();

    }
    
    public Sused getSused(int paIndexSusedaVPoli)
    {
        return aSusedneBodyTohtoBodu.get(paIndexSusedaVPoli);
    }

    public void setLatitude(Coordinate aLatitude) {
        this.aLatitude = aLatitude;
    }

    public void setLongitude(Coordinate aLongitude) {
        this.aLongitude = aLongitude;
    }

    
    public void pridajNovySusednyBod(BodNaCeste paNovySused) {
        Sused lpSused=new Sused(paNovySused);
        if (!paNovySused.equals(this)&&!aSusedneBodyTohtoBodu.contains(lpSused)) {
            aSusedneBodyTohtoBodu.add(lpSused);
            System.out.println("DEBUG: Novy sused bodu pridany");
        } else {
            System.out.println("DEBUG: Pridavany sused uz je susedom tohto bodu.");
        }
    }

    public void odstranSusedstvo(Sused paSusedNaOdstranenie) {
        aSusedneBodyTohtoBodu.remove(paSusedNaOdstranenie);
        System.out.println("DEBUG: Sused tohto bodu bol úspešne odtránený.");
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.aLatitude);
        hash = 41 * hash + Objects.hashCode(this.aLongitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BodNaCeste other = (BodNaCeste) obj;
        if (!Objects.equals(this.aLatitude, other.aLatitude)) {
            return false;
        }
        return Objects.equals(this.aLongitude, other.aLongitude);
    }

   

    public List<Sused> getSusedov()
    {
        List<Sused> surSused= new ArrayList<>();
        for(Sused prech:aSusedneBodyTohtoBodu)
        {
            surSused.add(prech);
        }
        return surSused;
    }

    @Override
    public String toString() {
        return "Lat: "+aLatitude+"; Long: "+aLongitude;
    }
    
    public enum TypBodu implements Serializable
    {
        OBJEKTALEBOZVIERA,
        BODCESTYPRENAVIGACIUDEFAULT,
        BODCESTYVYTVORENYPOUZIVATELOM;
        private static final long serialVersionUID = 7L;
    }
    
   
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package skuska_1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 *
 * @author Hawk
 */
public class myFileReader {

    private String path = "";
    private File file = null;

    public myFileReader(String path) {
        this.path = path;
        this.file = new File(this.path);
    }

    public void WriteFile(String content) {
        try {
            Writer writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);
            writer.close();
        } catch(IOException ioe) {
            System.out.println("Writing in to the file failed...PROBLEM?");
        }
        System.out.println("Your file has been written");
    }




}



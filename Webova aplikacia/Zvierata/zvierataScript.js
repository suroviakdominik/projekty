/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;
            text = "<option value=\"-1\" disabled selected> Vyber kategóriu: </option>";
            var a = document.getElementById("kategorie").innerHTML = text + navrat;
        }

    }
    xmlhttp.open("POST", "vypisKategorie.php", true);
    xmlhttp.send();
});

function kategoriaZvolena(idKat)
{
    document.getElementById("zvierata").style.display = "initial";
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;
            text = "<option value=\"-1\" disabled selected> Vyber zviera: </option>";
            var a = document.getElementById("zvierata").innerHTML = text + navrat;
        }

    }
    xmlhttp.open("POST", "vypisZvierataPodlakategorie.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("katID=" + idKat);
    xmlhttp.send();
}

function popis(idZv)
{
    document.getElementById("zvierata").style.display = "initial";
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("popis").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "popisZviera.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("zvID=" + idZv);
}

function istoZmazatZviera()
{
    if (confirm('Skutočne chcete ostrániť toto zviera z databázy?'))
    {
        document.getElementById("deleteZviera").submit();
    }
}

function vlozZviera(val)
{
    document.getElementById("vybrZviera").value = val;
}


    
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import Grafika.Obdlznik;

/**
 * Trieda Kralovna je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Kralovna extends Figurka {

    private IGrafickeObjekty[] aVykreslenieKralovna;

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba novej kráľovnej
     */
    public Kralovna(String paFarba) {
        super("Kralovna", paFarba);
        aVykreslenieKralovna = new IGrafickeObjekty[4];
    }

    /**
     * je metóda, ktorá vykreslí kráľovnú na plátno.
     */
    @Override
    public void nakresli() {
        aVykreslenieKralovna[0] = new Obdlznik(30, 15, this.getX(), this.getY() + 5, this.getFarba());
        aVykreslenieKralovna[1] = new Obdlznik(20, 8, this.getX() + 5, this.getY() - 3, this.getFarba());
        aVykreslenieKralovna[2] = new Obdlznik(5, 15, this.getX() + 12, this.getY() - 18, this.getFarba());
        aVykreslenieKralovna[3] = new Obdlznik(10, 5, this.getX() + 10, this.getY() - 13, this.getFarba());
        for (int i = 0; i < 4; i++) {
            aVykreslenieKralovna[i].zobraz();
        }
    }

    /**
     * je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie kráľovnej.
     *
     * @return grafické objekty z ktorých sa skladá kráľovná
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aVykreslenieKralovna;
    }

    /**
     * metóda vracajúca popis kráľovnej.
     *
     * @return popis kráľovnej vo forme String
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Kráľovná" + "\n";
        popis += "Pohyb: všetkými možnými smermi" + "\n";
        popis += "Vyhadzovanie: všetkými možnými smermi" + "\n";
        popis += "Ine: pri pohybe a vyhadzovaní nesmie prejsť cez/ponad inú figúrku" + "\n";
        return popis;
    }
}



public class Policko
{
    private Stav aStav;
    private int aX;
    private int aY;
    private Smer aOdkialPrisiel;
    private Smer aNaposledySiel;
    
    public Policko(Stav paStav,int paX,int paY)
    {
        aStav=paStav;
        aX=paX;
        aY=paY;
        aOdkialPrisiel=null;
        aNaposledySiel=null;
    }
    
    
    public Stav dajStav()
    {
        return aStav;
    }
    
    public void nastavStav(Stav paStav)
    {
        aStav=paStav;
    }   
    
    public int dajX()
    {
        return aX;
    }
    
    public int dajY()
    {
        return aY;
    }    
    
    public String toString()
    {
        return aStav.toString();
    } 
    
    public Smer dajOdkialPrisiel()
    {
        return aOdkialPrisiel;
    }
    
    public void nastavOdkialPrisiel(Smer paSmer)
    {
        if (aOdkialPrisiel==null)
        {
           aOdkialPrisiel=paSmer;
        }
    }
    
    public Smer kamNaposledySiel()
    {
        return aNaposledySiel!=null?aNaposledySiel:aOdkialPrisiel.DOLAVA();
    }
    
    public void OdisielSmerom(Smer paSmer)
    {
        aNaposledySiel=paSmer;
        if(aOdkialPrisiel==aNaposledySiel)
        {
            aStav=Stav.PREHLADANE;
        }
    }
}


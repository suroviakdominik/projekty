/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import šach.hra.Hrac;
import šach.Ine.PracaSoSuborom;

/**
 * Uloží aktuálny stav hry.
 *
 * @author Dominik
 */
public class PrikazUloz implements IPrikaz {

    /**
     * vytvorí objekt triedy PracaSoSuborom, ktorý použije na zápis aktuálneho
     * stavu hry do súboru, ktorého názov si hráč zvolí po zadaní príkazu. Hráča
     * informuje o tom či bola hra uložená alebo nie.
     *
     * @param paHracKtoryZadavaPrikazy - hráč, ktorý je aktuálne na ťahu
     * @param lpParameter - pri tomto objekte nie je potrebný žiadny
     * @return
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Zadajte poziciu kam chcete hru ulozit.");
        System.out.println("1-ulozenaPozicia1");
        System.out.println("2-ulozenaPozicia2");
        System.out.print(">");
        int volba = scan.nextInt();
        PracaSoSuborom ukladanie;
        try {
            if (volba == 1) {
                ukladanie = new PracaSoSuborom("ulozenaPozicia1.txt", paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra());
                ukladanie.ulozDoSuboru(paHracKtoryZadavaPrikazy, paHracKtoryZadavaPrikazy.getaProtiHrac());
                System.out.println("Hru sa podarilo uspesne ulozit.");
            } else if (volba == 2) {
                ukladanie = new PracaSoSuborom("ulozenaPozicia2.txt", paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra());
                ukladanie.ulozDoSuboru(paHracKtoryZadavaPrikazy, paHracKtoryZadavaPrikazy.getaProtiHrac());
                System.out.println("Hru sa podarilo uspesne ulozit.");
            } else {
                System.out.println("Hru sa nepodarilo uspesne ulozit. Dana ulozna pozicia neexistuje. Ak chcete ulozit hru, zadajte znova prikaz ulozit.");
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrikazUloz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}

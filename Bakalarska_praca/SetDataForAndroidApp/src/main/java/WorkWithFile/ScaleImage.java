/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WorkWithFile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
/**
 *
 * @author aioobe Edit: Dominik
 */
public class ScaleImage {

    /**
     * Zmeni velkost obrazka na dane rozmery nachadzajuceho sa na ceste zadanej ako
     * parameter.
     * @param paPathOfScalingFiles - cesta obrazka, ktoreho rozmery sa maju zmenit
     * @param paEndOfPicture - typ obrazka, koncovka suboru (napr. jpg)
     */
    public static void scale(String paPathOfScalingFiles,String paEndOfPicture) {
        
        Image img = new ImageIcon(paPathOfScalingFiles).getImage();
        
        int oldWidthPicture=img.getWidth(null);
        int oldHeightPictureht=img.getHeight(null);
        int newWidth;
        int newHeight;
        if(oldWidthPicture>=oldHeightPictureht)
        {
            newWidth=500;
            newHeight=350;
        }
        else
        {
             newWidth=300;
             newHeight=400;
        }
        
        BufferedImage bi = new BufferedImage(newWidth,
                newHeight,
                BufferedImage.TYPE_INT_RGB);

        Graphics2D grph = (Graphics2D) bi.getGraphics();
        grph.scale((double)newWidth/oldWidthPicture,(double)newHeight/oldHeightPictureht);

        // everything drawn with grph from now on will get scaled.
        grph.drawImage(img, 0, 0, null);
        grph.dispose();
        img.flush();
        try {
            ImageIO.setUseCache(false);
            ImageIO.write(bi, paEndOfPicture, new File(paPathOfScalingFiles));
            
            
        } catch (IOException ex) {
            Logger.getLogger(ScaleImage.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
}

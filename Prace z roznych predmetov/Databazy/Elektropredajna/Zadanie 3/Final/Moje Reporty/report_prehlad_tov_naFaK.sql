set pagesize 123
set linesize 60
column predajna.nazov_predajne heading "Nazov predajna" FORMAT A30;
column zamestnanec.meno heading "Meno zamestnanca" FORMAT A20;
column zamestnanec.priezvisko heading "Priezvisko zamestnanca" FORMAT A20;
column faktura.cislo_fa HEADING "Cislo Faktury" FORMAT A15;
column faktura.datum_vystavenia HEADING "Datum vystavenia:" A14;
column faktura.datum_splatnosti HEADING "Datum splatnosti:" A14;
column faktura.stav_faktury HEADING "Stav faktury:" A15;
column tovar.nazov HEADING "Nazov Tovaru:" A14;

column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad vystavenych faktur' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia:" datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
predajna.nazov_predajne,zamestnanec.meno,zamestnanec.priezvisko,faktura.cislo_fa,faktura.datum_vystavenia,
faktura.datum_splatnosti,faktura.stav_faktury,tovar.nazov
from Predajna,ZAMESTNANEC,FAKTURA,VYFAKTUROVANYTOVAR,TOVAR
WHERE Predajna.ID_PREDAJNE=ZAMESTNANEC.ID_PREDAJNE
AND   ZAMESTNANEC.OSOBNE_CISLO=FAKTURA.OSOBNE_CISLO
AND   FAKTURA.CISLO_FA=VYFAKTUROVANYTOVAR.CISLO_FA
AND   FAKTURA.OSOBNE_CISLO = VYFAKTUROVANYTOVAR.OSOBNE_CISLO
AND   VYFAKTUROVANYTOVAR.KATALOGOVECISLO = TOVAR.KATALOGOVECISLO
AND   VYFAKTUROVANYTOVAR.ID_PREDAJNE = TOVAR.ID_PREDAJNE;


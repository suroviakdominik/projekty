/**
 * Trida Jizdenka popisuje jizdenku
 * 
 * @author Jirka Krhanek
 * @version 2006-01-02
 */
public class Jizdenka {
    private int hodnota;

/**
 * Konstruktor pro objekty tridy Jizdenka
 * 
 * @param nHodnota   hodnota vytvorene jizdenky
 */
  public Jizdenka(int nHodnota) 
  {
    hodnota = nHodnota;   // uchovava pouze hodnotu jizdenky
  }

/**
 * Tato metoda vraci hodnotu jizdenky
 * 
 * @return    hodnotu jizdenky
 */
  public int vratHodnotu() 
  {
    return hodnota;   // vrati hodnotu jizdenky
  }
}

package wof00.VstupVystup;

import java.util.HashMap;
import wof00.Vykonavac.IVykonavac;
import wof00.Vykonavac.VykonavacDokonci;
import wof00.Vykonavac.VykonavacChod;
import wof00.Vykonavac.VykonavacInfo;
import wof00.Vykonavac.VykonavacOdpoved;
import wof00.Vykonavac.VykonavacOslov;
import wof00.Vykonavac.VykonavacPoloz;
import wof00.Vykonavac.VykonavacPomoc;
import wof00.Vykonavac.VykonavacPouzi;
import wof00.Vykonavac.VykonavacPrijmi;
import wof00.Vykonavac.VykonavacProfil;
import wof00.Vykonavac.VykonavacUkonci;
import wof00.Vykonavac.VykonavacUloz;
import wof00.Vykonavac.VykonavacUloz2;
import wof00.Vykonavac.VykonavacZdvihni;

/**
 * Trieda NazvyPrikazov udrzuje zoznam nazvov platnych prikazov hry. Za ulohu ma
 * rozpoznavat platne prikazy.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 * @author lokalizacia: Lubomir Sadlon, Jan Janech
 * @version 2012.02.21
 */
public class NazvyPrikazov {

    private HashMap<String, IVykonavac> aVykonavace;

    // konstantne pole nazvov prikazov
     /*private static final String[] aPlatnePrikazy = {
     "chod", "ukonci", "pomoc"
     };*/
    /**
     * Inicializuje zoznam platnych prikazov.
     */
    public NazvyPrikazov() {
        aVykonavace = new HashMap<String, IVykonavac>();
        aVykonavace.put(Prikazy.pomoc.toString(), new VykonavacPomoc());
        aVykonavace.put(Prikazy.ukonci.toString(), new VykonavacDokonci());
        aVykonavace.put(Prikazy.chod.toString(), new VykonavacChod());
        aVykonavace.put(Prikazy.zdvihni.toString(), new VykonavacZdvihni());
        aVykonavace.put(Prikazy.poloz.toString(), new VykonavacPoloz());
        aVykonavace.put(Prikazy.info.toString(), new VykonavacInfo());
        aVykonavace.put(Prikazy.profil.toString(), new VykonavacProfil());
        aVykonavace.put(Prikazy.pouzi.toString(), new VykonavacPouzi());
        aVykonavace.put(Prikazy.oslov.toString(), new VykonavacOslov());
        aVykonavace.put(Prikazy.odpoved.toString(), new VykonavacOdpoved());
        aVykonavace.put(Prikazy.uloz.toString(), new VykonavacUloz());
        aVykonavace.put(Prikazy.uloz2.toString(), new VykonavacUloz2());
        aVykonavace.put(Prikazy.prijmi.toString(), new VykonavacPrijmi());
        aVykonavace.put(Prikazy.dokonci.toString(), new VykonavacDokonci());
    }

    /**
     * Kontroluje, ci nazov v parametri je platny prikaz.
     *
     * @return true ak je parameter platny prikaz, false inak.
     */
    public boolean jePrikaz(String paNazov) {
        return aVykonavace.containsKey(paNazov);
    }

    public IVykonavac dajVykonavac(String paNazov) {
        return aVykonavace.get(paNazov);
    }

    public static void vypisExistPrikazov() {
        for (int i = 0; i < Prikazy.values().length; i++) {
            System.out.print(Prikazy.values()[i] + "\t");
        }
        System.out.println();
    }

    public static enum Prikazy {

        pomoc("pomoc"),
        ukonci("ukonci"),
        chod("chod"),
        zdvihni("zdvihni"),
        prijmi("prijmi"),
        poloz("poloz"),
        info("info"),
        uloz2("uloz2"),
        profil("profil"),
        pouzi("pouzi"),
        oslov("oslov"),
        odpoved("odpoved"),
        uloz("uloz"),
        dokonci("dokonci");

        private String aPrikaz;

        private Prikazy(String paPrikaz) {
            aPrikaz = paPrikaz;
        }

        @Override
        public String toString() {
            return aPrikaz;
        }

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacPoloz implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paNazov) {
        IPredmet predmet = paHrac.dajPredmet(paNazov);
        if (predmet == null) {
            System.out.println("Taky predmet nemam");
        } else {
            paHrac.polozPredmet(paNazov);
            System.out.println("Polozili ste predmet: " + paNazov + " do miestnosti: " + paHrac.dajPoziciu().getaNazov());
        }

        return false;
    }
}

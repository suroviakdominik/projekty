/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.triestrom;

import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class ExternyVrchol extends Vrchol{
    private long aCisloBloku;
    private long aPocetPlatnychZaznamov;
    
    public ExternyVrchol(int paHlbkaVrchola, Vrchol paOtec, long paCisloBloku,long paPocetZaznamovNaZaciatku) {
        super(paHlbkaVrchola, paOtec, null, null);
        aCisloBloku = paCisloBloku;
        aPocetPlatnychZaznamov = paPocetZaznamovNaZaciatku;
    }

    public long getaCisloBloku() {
        return aCisloBloku;
    }

    public void setaCisloBloku(long aCisloBloku) {
        this.aCisloBloku = aCisloBloku;
    }

    public long getPocetPlatnychZaznamov() {
        return aPocetPlatnychZaznamov;
    }

    public void setaPocetPlatnychZaznamov(long aPocetBlokov) {
        this.aPocetPlatnychZaznamov = aPocetBlokov;
    }
    
    public void zvysAktualnyPocetZaznamov(){
        aPocetPlatnychZaznamov++;
    }
    
    public void znizAktualnyPocetZaznamov(){
        aPocetPlatnychZaznamov--;
    }
    
     @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExternyVrchol other = (ExternyVrchol) obj;
        if (this.aCisloBloku != other.aCisloBloku) {
            return false;
        }
        
        if(other.aPocetPlatnychZaznamov != this.aPocetPlatnychZaznamov){
            throw new IllegalStateException("Maili by sa rovnat.");
        }
        return true;
    }
    
    @Override
    public String toString() {
        return 1+"||"+aCisloBloku+"||"+aPocetPlatnychZaznamov;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("vybZv").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "vypisObrazkov.php", true);
    xmlhttp.send();
});

function vlozObr(val)
{
    document.getElementById("texPObr").value = val;
}


function zmazPouzivatela(el)
{
    if (confirm('Skutočne chcete ostrániť tohto používateľa z databázy?'))
    {
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                var navrat = xmlhttp.responseText;

                var a = document.getElementById("odpovedMazUz").innerHTML = navrat;
            }

        }
        xmlhttp.open("POST", "zmazPouzivatela.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var inputWithText = el.parentNode[0];

        xmlhttp.send("mazPouzMail=" + inputWithText.value);
    }
}

function zmenHeslo(el)
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("zmenaHesla").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "zmenaHesla.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var inputMail = el.parentNode[0];
    var inputNewPass = el.parentNode[1];

    xmlhttp.send("Mail=" + inputMail.value + "&noveHeslo=" + inputNewPass.value);
}

function vytvorNovuKategoriu(el)
{
    
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("vlozKateg").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "vlozKateg.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var inputMail = el.parentNode[0];
    var inputNewPass = el.parentNode[1];

    xmlhttp.send("nazovKat=" + inputMail.value + "&popisKat=" + inputNewPass.value);
}

function vlozZviera(el)
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("vlozZV").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "vlozZviera.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var inputkatZv = el.parentNode[1];
    var inputmenoZv = el.parentNode[2];
    var inputPopis = el.parentNode[3];
    var inputObr = el.parentNode[5];

    xmlhttp.send("katZv=" + inputkatZv.value + "&menoZv=" + inputmenoZv.value + "&obrZv=" + inputObr.value + "&popisZv=" + inputPopis.value);
}

function zmemOtvHod(el)
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var navrat = xmlhttp.responseText;

            var a = document.getElementById("zmenOtvHod").innerHTML = navrat;
        }

    }
    xmlhttp.open("POST", "zmenOtvHod.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var inputPoPiaOtv = el.parentNode[0];
    var inputPoPiaZatv = el.parentNode[1];
    var inputSoNeOtv = el.parentNode[2];
    var inputSoNeZatv = el.parentNode[3];

    xmlhttp.send("popiaotv=" + inputPoPiaOtv.value + "&popiazatv=" + inputPoPiaZatv.value + "&soneotv=" + inputSoNeOtv.value + "&sonezatv=" + inputSoNeZatv.value);
}


$(document).ready(function () {
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {


            var navrat = xmlhttp.responseText;

            var a = document.getElementsByClassName("kateg");
            for (var i = 0; i < a.length; i++)
            {
                var b="<option selected disabled>Vyber kategóriu</option>";
                a[i].innerHTML = b+navrat;
            }
        }

    }
    xmlhttp.open("POST", "vypisKat.php", true);

    xmlhttp.send();
});

function zmazKategoriu(el)
{
     if (confirm('Skutočne chcete ostrániť túto kategóriu z databázy?'))
    {
        var xmlhttp;
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                var navrat = xmlhttp.responseText;

                var a = document.getElementById("odpovedMazKat").innerHTML = navrat;
            }

        }
        xmlhttp.open("POST", "zmazKategoriu.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var select = el.parentNode[0];
        var option= select.selectedOptions[0];

        xmlhttp.send("idMazKat="+ option.value);
    }
}

function vlozKategoriuDoTextPola(e){
    var text=e.selectedOptions[0].text;
    
    document.getElementById("kategNZv").value=text;
}
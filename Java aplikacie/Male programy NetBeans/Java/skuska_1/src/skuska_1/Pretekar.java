/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package skuska_1;

/**
 *
 * @author Hawk
 */
public class Pretekar implements Comparable {
    String meno;
    int maxVykon;
    int sumaVysledkov;

    public int getSuma() {
        return this.sumaVysledkov;
    }

    public Pretekar(String pa, int pa2){
        this.meno=pa;
        this.maxVykon=pa2;
        //this.sumaVysledkov=pa3;
    }

    public void setMeno(String pa) {
        this.meno = pa;
    }

    public void setMaxVykon(int pa) {
        this.maxVykon = pa;
    }

    public int getMaxVykon() {
        return this.maxVykon;
    }

    public String getMeno() {
        return this.meno;
    }

    public int compareTo(Object o) {
        if(((Pretekar)o).getMaxVykon()>this.getMaxVykon()) return 1;
        if(((Pretekar)o).getMaxVykon()==this.getMaxVykon()) return 0;
        return -1;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommandProcessing;

import EditorsForInfo_Files.IOperacieEDITORA;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import WorkWithFile.FileWorker;
import WorkWithFile.SerializationWorker;
import java.util.HashMap;
import EditorsForInfo_Files.InfoSuboryENUM;
import WorkWithFile.DirWorker;

/**
 * Objekt tejto triedy nacita pri spusteni aplikacie vsetky subory typu
 * Info_About_DIRS_File a priradi ich editorom, ktore ich mozu editovat.
 *
 * @author Dominik
 */
public class LoadAndSaveInfoFiles {

    private SerializationWorker<Info_About_Dirs_File> aSerializationWorker = null;

    public LoadAndSaveInfoFiles() {

        aSerializationWorker = new SerializationWorker<Info_About_Dirs_File>(InfoSuboryENUM.aEndOfInfoFile);
    }

   /**
    * metoda nacita pri spusteni aplikacie vsetky subory typu
    * Info_About_DIRS_File a priradi ich editorom, ktore ich mozu editovat
    * @return Map dvojic objektov typu IEditor:Info_About_DIRS_File
    */
    public HashMap<String, IOperacieEDITORA> loadInfoFiles() {
        HashMap<String, IOperacieEDITORA> ret = new HashMap<>();

        for (InfoSuboryENUM walker : InfoSuboryENUM.values()) {
            FileWorker f = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER + walker.getaRelativePathOfDirecotryWhereIAM() + walker.getaNameOfInfoFile() + "." + InfoSuboryENUM.aEndOfInfoFile);
            aSerializationWorker.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + walker.getaRelativePathOfDirecotryWhereIAM());
            if (!f.existsFile()) {
                ret.put(walker.getKeyName(), walker.getInstanceOfEditorWithNewInfoFile());
                aSerializationWorker.setObjectThatIsSerializable(ret.get(walker.getKeyName()).getInfoFileThatIEdit());
                aSerializationWorker.setNameOfFile(walker.getaNameOfInfoFile());
                aSerializationWorker.saveObjectToFile();
                DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER + walker.getaRelativePathOfDirecotryWhereIAM() + walker.getSignOfDir());
                dw.vytvorAdresarAkNeexistuje();
                dw = null;
                f = null;
            } else {
                aSerializationWorker.setNameOfFile(walker.getaNameOfInfoFile());
                ret.put(walker.getKeyName(), walker.getInstanceOfEditorWithExistingInfoFile(aSerializationWorker.readObjetctFromFile()));
            }

        }
        return ret;
    }

}

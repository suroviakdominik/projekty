package com.suroviak.dominik.zooAplication.WorkWithFiles;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by Dominik on 15.2.2015.
 */
public class FileWorker {
    private String aAbsolutePath;
    private File aFile;

    public FileWorker() {

    }

    public FileWorker(String paAbsolutePathOfFile) {
        aAbsolutePath = paAbsolutePathOfFile;
        aFile = new File(paAbsolutePathOfFile);
    }

    public static void copyFile(String sourcePathOfFile, String targetPathOfFile) {


        InputStream in = null;
        OutputStream out = null;

        try {
            Log.i("ZOOApp", "copyFile() " + sourcePathOfFile);
            in = new FileInputStream(new File(sourcePathOfFile));


            out = new FileOutputStream(targetPathOfFile);

            byte[] buffer = new byte[102400];
            long total = 0;
            int read;
            while ((read = in.read(buffer)) != -1) {
                total += read;
                out.write(buffer, 0, read);
                if (total > 1024 * 1024) {
                    total = 0;
                    out.flush();
                }
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("ZOOApp", "Exception in copyFile() of " + sourcePathOfFile);
            Log.e("ZOOApp", "Exception in copyFile() " + e.toString());
        }
    }

    public String getaAbsolutePath() {
        return aAbsolutePath;
    }

    public void setaAbsolutePath(String paAbsolutePath) {
        this.aAbsolutePath = paAbsolutePath;
        aFile = new File(paAbsolutePath);
    }

    public boolean existsFile() {
        if (aFile.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public String readDataFromFile() throws IOException {
        String obsahSuboru = "";
        FileInputStream fstream = null;
        DataInputStream in = null;
        if (!isFile()) {
            throw new FileNotFoundException("The path is not a file path but directory path. You can't read the directory.");
        } else {

            try {
                // Open the file that is the first
                // command line parameter
                fstream = new FileInputStream(aAbsolutePath);
                // Get the object of DataInputStream
                in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                String strLine;
                //Read File Line By Line
                while ((strLine = br.readLine()) != null) {
                    // Print the content on the console

                    obsahSuboru += strLine + System.getProperty("line.separator");
                }

                //Close the input stream

                Log.e("DEBUG:", "Data was successfully read from file" + aAbsolutePath);
            } catch (Exception e) {//Catch exception if any
                throw e;
            } finally {
                if (in != null) {
                    in.close();
                }
                if (fstream != null) {
                    fstream.close();
                    Log.e("DEBUG:", "File: " + aAbsolutePath + " was successfully closse after reading.");
                }
            }
        }

        return obsahSuboru;

    }

    public void writeDataToFile(String paCoZapisat) throws IOException {
        if (!isFile()) {
            throw new FileNotFoundException("The path is not a file path but directory path. You can't write to directory.");
        } else {
            try {// if file doesnt exists, then create it
                if (!aFile.exists()) {
                    aFile.createNewFile();
                }

                FileWriter fw = new FileWriter(aFile.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(paCoZapisat);
                bw.flush();
                bw.close();
                fw.flush();
                fw.close();
                Log.d("MyLogFiles", "Data was successfully write to file" + aAbsolutePath);

            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }


    }

    public String getName() {
        if (aFile != null)
            return aFile.getName();

        return null;
    }

    public boolean createDirectory() {
        File f = new File(aAbsolutePath);
        if (!f.exists()) {
            f.mkdirs();
            return true;
        }
        return false;
    }

    /*
    return: true - if file is a file
            false - is directory
     */
    private boolean isFile() {
        if (aFile.isDirectory()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteFile() {
        if (aFile != null)
            return aFile.delete();
        return false;
    }
}

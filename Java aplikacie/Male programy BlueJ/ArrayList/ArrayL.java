
import java.util.ArrayList;
public class ArrayL
{
  private ArrayList<Integer> aArraylist;
  
  public ArrayL()
  {
      aArraylist=new ArrayList<>();
  }
  
  public void pridajPrvok(int prvok)
  {
      aArraylist.add(prvok);
  }
  
  public void odoberPrvok(int ktory)
  {
      aArraylist.remove(ktory);
  }
  
  public void vypisPrvky()
  {
      for (Integer cislo:aArraylist)
      {
          System.out.println(cislo);
      }
  }
}

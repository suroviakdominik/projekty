package com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.suroviak.dominik.zooAplication.ActivityClass.Info_About_Zoo_Activity;
import com.suroviak.dominik.zooAplication.R;


/**
 * Created by Dominik on 9.2.2015.
 */
public class InfoZOOFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_info_about_zoo_main_menu, container, false);


        ImageView logo = (ImageView) rootView.findViewById(R.id.imageViewInfoFragmentLogo);
        Button b = (Button) rootView.findViewById(R.id.buttonVisitorInfo);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Info_About_Zoo_Activity.class);
                startActivity(i);
            }
        });


        return rootView;
    }


}

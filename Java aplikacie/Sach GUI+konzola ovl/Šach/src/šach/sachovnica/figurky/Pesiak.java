/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.sachovnica.figurky;

import Grafika.IGrafickeObjekty;
import Grafika.Kruh;

/**
 * Trieda Pesiak je už trieda, ktorej objekt predstavuje konkrétnu figúrku. Je
 * potomkom triedy Figurka.
 *
 * @author Dominik
 */
public class Pesiak extends Figurka {

    private IGrafickeObjekty[] aGrafickeObjektyPesiaka = new IGrafickeObjekty[3];

    /**
     * Zavolá konštruktora predka a naplní ho parametrami prijatými v atribúte.
     * Vytvorí kontajner grafických objektov.
     *
     * @param paFarba - farba nového pešiaka
     */
    public Pesiak(String paFarba) {
        super("Pešiak", paFarba);
    }

    /**
     * je metóda, ktorá vykreslí pešiaka na plátno.
     */
    @Override
    public void nakresli() {
        for (int i = 0; i < 3; i++) {

            aGrafickeObjektyPesiaka[i] = new Kruh(20 - i * 5, this.getX() + 2 * i, this.getY() - 9 * i, this.getFarba());
            aGrafickeObjektyPesiaka[i].zobraz();

        }
    }

    /**
     * je metóda, ktorá vracia grafické objekty(kruh,štvorec...) potrebné na
     * vykreslenie pešiaka.
     *
     * @return grafické objekty z ktorých sa skladá pešiaka
     */
    @Override
    public IGrafickeObjekty[] getGrafickeObjektyFigurky() {
        return aGrafickeObjektyPesiaka;
    }

    /**
     * metóda vracajúca popis pešiaka.
     *
     * @return popis pešiaka vo forme String
     */
    @Override
    public String getPopis() {
        String popis = "";
        popis += "Meno: Pešiak" + "\n";
        popis += "Pohyb: moznost posunutia o dve polia dopredu pri prvom tahu;  jedno pole dopredu" + "\n";
        popis += "Vyhadzovanie: došikma o jedno pole proti smeru supera" + "\n";
        popis += "Ine: smie sa pohybovať len proti súperovým figúrkam; ak sa dostane na druhú stranu šachovnice, zmení sa na hráčom vybranú figúrku, ktorá je k dispozícii" + "\n";
        return popis;
    }

}

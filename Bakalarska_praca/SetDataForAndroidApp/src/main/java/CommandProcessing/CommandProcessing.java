/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommandProcessing;

import EditorsForInfo_Files.IOperacieEDITORA;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import EditorsForInfo_Files.InfoSuboryENUM;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import java.util.ArrayList;

/**
 * V amci tejto triedy sa pri poyiadavke zalozky o editovanie dat vyhodnoti,
 * ktory editor zavolat a ktoru operaciu nad nim.
 * @author Dominik
 */
public class CommandProcessing {

    private HashMap<String, IOperacieEDITORA> aActionsObjects;

    /**
     * Dvojice typu  InformacnySuborNazov:IEdditor
     * // informacny subor=NAZOV _INFO_ABOUT_DIRS_File, ktror zalozka pre update dat pozaduje
     * @param paActionsObjects 
     */
    public CommandProcessing(HashMap<String, IOperacieEDITORA> paActionsObjects) {
        aActionsObjects = paActionsObjects;
    }

    /**
     * 
     * @param paKtoryInfoSubor-info subor nad ktorym vykonat prikay
     * @param paPrikaz - prikay typu PrikazyENUM, ktory vykonat
     * @return true - uspesne vykonana opracia
     *         false - nevykonana oparacia
     */
    public boolean vykonajPrikaz(InfoSuboryENUM paKtoryInfoSubor, PrikazyENUM paPrikaz) {
        IOperacieEDITORA lpNadAkymObjektomVykonat;
        switch (paKtoryInfoSubor) {
            case CATEGORIES:
                lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            case NEWS:
                lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            case ANIMALS:
                lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            case EVENTS:
                lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            case INFO:
                lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            case OBJECTS:
                 lpNadAkymObjektomVykonat = aActionsObjects.get(paKtoryInfoSubor.getKeyName());
                break;
            default: lpNadAkymObjektomVykonat=null;
        }
        if(lpNadAkymObjektomVykonat==null)
        {
            /* System.err.println("Nepodstatné: LEN PRE INFORMACIU. Called method return false. Editor: "+paKtoryInfoSubor.toString());
             Logger.getLogger(CommandProcessing.class.getName()).log(Level.SEVERE,null , "Chyba: operáciu nad daným informačným súborom sa nepodarilo vykonať. OBJEKT PREDSTAVUJÚCI \n"
                     + "INFORMACNY! SA NEPODARILO NAJST.!!\n metoda: vykonajPrikaz(ExistujuceInfoSubory paKtoryInfoSubor, PrikazyENUM paPrikaz)");
            */return false;
        }
        return rozhodniAkyPrikazAVykonajHO(lpNadAkymObjektomVykonat, paPrikaz);
        
    }
    
    
    private boolean rozhodniAkyPrikazAVykonajHO(IOperacieEDITORA paNadCimVykonat,PrikazyENUM paPrikaz)
    {
        boolean navrat;
        switch(paPrikaz)
        {
            case PRIDAJ:
                navrat=paNadCimVykonat.addNewItem();
                break;
            case ODOBER:
                navrat=paNadCimVykonat.deleteItem();
                break;
            case UPRAV:
                navrat=paNadCimVykonat.editItem();
                break;
            case ULOZ:
                return paNadCimVykonat.saveInfoFile();
            case DAJDATA:
                navrat=paNadCimVykonat.getDataOfInfoFile();
                break;
            default: navrat=false;
        }
        if(navrat==false)
        {
            /*System.err.println("Nepodstatné: LEN PRE INFORMACIU.Called method return false. Prikaz: "+paPrikaz.toString()+" "+paNadCimVykonat.toString());
            Logger.getLogger(CommandProcessing.class.getName()).log(Level.SEVERE, null, "Chyba: operáciu nad daným informačným súborom sa nepodarilo vykonať. PRIKAZ ZLYHAL!!!\n"
                    + "metóda: rozhodniAkyPrikazAVykonajHO(IOperacieNadInfoSubormi paNadCimVykonat,PrikazyENUM paPrikaz)");
            */
            return false;
        }
        else
        {
            return true;
        }
    }

    public ArrayList<Info_About_Dirs_File> getInfoFiles()
    {
        ArrayList<Info_About_Dirs_File> ret= new ArrayList<>();
        for(String prech:aActionsObjects.keySet())
        {
            ret.add(aActionsObjects.get(prech).getInfoFileThatIEdit());
        }
        return ret;
    }
}

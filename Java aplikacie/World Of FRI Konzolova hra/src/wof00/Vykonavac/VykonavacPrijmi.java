/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;
import wof00.Questy.IQuest;

/**
 *
 * @author suroviak3
 */
public class VykonavacPrijmi implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        IQuest quest = (IQuest) paHrac.dajPoziciu().zistiPredmet(paPrikaz);

        if (quest == null) {
            System.out.println("Taky quest neexistuje");
        } else {
            quest.prijmi(paHrac);
        }
        return false;
    }

}

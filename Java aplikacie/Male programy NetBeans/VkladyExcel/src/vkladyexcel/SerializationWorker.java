/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vkladyexcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class SerializationWorker {
    public static <T> void writeObjectToFile(File f,T paObject)
    {
        ObjectOutputStream oOS=null;
        try {
            oOS = new ObjectOutputStream(new FileOutputStream(f));
            oOS.writeObject(paObject);
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oOS.close();
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static <T> T readObjectFromFile(File f)
    {
        ObjectInputStream oIS=null;
        try {
            oIS = new ObjectInputStream(new FileInputStream(f));
            return (T)oIS.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                if(oIS!=null)
                oIS.close();
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return null;
    }
}

package com.suroviak.dominik.zooAplication.Navigation;

import android.util.Log;

import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dominik on 29.3.2015.
 */
public class DisplayableMapObjects {
    private Info_About_Dirs_File aZvierata;
    private Info_About_Dirs_File aObjekty;
    private Info_About_Dirs_File akategorieObjektov;

    public DisplayableMapObjects() {
        nacitajZvierataInfoSubor();
        nacitajObjektyInfoSubor();
        nacitajKategorieObjektov();
    }

    private void nacitajZvierataInfoSubor() {
        String aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.ANIMALS.getaNameOfInfoFile());
        try {
            aZvierata = (Info_About_Dirs_File) sw.readObjetctFromFile();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void nacitajObjektyInfoSubor() {
        String aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.OBJECTS.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.OBJECTS.getaNameOfInfoFile());
        try {
            aObjekty = (Info_About_Dirs_File) sw.readObjetctFromFile();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void nacitajKategorieObjektov() {
        String aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.CATEGORIES.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.CATEGORIES.getaNameOfInfoFile());
        try {
            akategorieObjektov = (Info_About_Dirs_File) sw.readObjetctFromFile();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Map<String, String> getNamesOfAllObjects() {
        HashMap<String, String> ret = new HashMap<>();

        for (String prech : aZvierata.getaItemsInfo().keySet()) {
            if (aZvierata.getaItemsInfo().get(prech) != null &&
                    aZvierata.getaItemsInfo().get(prech).getZemepisnaDlzka() != -1 &&
                    aZvierata.getaItemsInfo().get(prech).getZemepisnaSirka() != -1) {
                ret.put(prech, aZvierata.getaItemsInfo().get(prech).getOriginalName());
            }
        }

        for (String prech : aObjekty.getaItemsInfo().keySet()) {
            if (aObjekty.getaItemsInfo().get(prech) != null &&
                    aObjekty.getaItemsInfo().get(prech).getZemepisnaDlzka() != -1 &&
                    aObjekty.getaItemsInfo().get(prech).getZemepisnaSirka() != -1) {
                ret.put(prech, aObjekty.getaItemsInfo().get(prech).getOriginalName());
            }
        }
        return ret;
    }

    public double getLatitudeOfObject(String paKeyOfObject) throws IllegalArgumentException {
        if (aObjekty.getaItemsInfo().get(paKeyOfObject) != null) {
            return aObjekty.getaItemsInfo().get(paKeyOfObject).getZemepisnaSirka();
        } else if (aZvierata.getaItemsInfo().get(paKeyOfObject) != null) {
            return aZvierata.getaItemsInfo().get(paKeyOfObject).getZemepisnaSirka();
        }
        throw new IllegalArgumentException();
    }

    public double getLongitudeOfObject(String paKeyOfObject) {
        if (aObjekty.getaItemsInfo().get(paKeyOfObject) != null) {
            return aObjekty.getaItemsInfo().get(paKeyOfObject).getZemepisnaDlzka();
        } else if (aZvierata.getaItemsInfo().get(paKeyOfObject) != null) {
            return aZvierata.getaItemsInfo().get(paKeyOfObject).getZemepisnaDlzka();
        }
        throw new IllegalArgumentException();
    }

    public Map<String, String> getNazvyKategoriiObjektov() {
        HashMap<String, String> ret = new HashMap<>();
        if (akategorieObjektov != null) {
            for (String prech : akategorieObjektov.getaItemsInfo().keySet()) {
                if (prech.contains("CO")) {
                    ret.put(prech, akategorieObjektov.getaItemsInfo().get(prech).getOriginalName());
                }
            }
        } else {
            Log.e("MyLog", "No categories info file load." + this.getClass().getSimpleName());
        }
        return ret;
    }

    public Map<String, String> getObjektyZvolenejKategorie(String paCategory) {
        HashMap<String, String> ret = new HashMap();
        for (String prech : aObjekty.getaItemsInfo().keySet()) {
            if (aObjekty.getaItemsInfo().get(prech).getParentDirectory() != null && aObjekty.getaItemsInfo().get(prech).getParentDirectory().equals(paCategory)) {
                if (aObjekty.getaItemsInfo().get(prech).getZemepisnaDlzka() != -1 && aObjekty.getaItemsInfo().get(prech).getZemepisnaSirka() != -1) {
                    ret.put(prech, aObjekty.getaItemsInfo().get(prech).getOriginalName());
                }
            }
        }
        return ret;
    }


}

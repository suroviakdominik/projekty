#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

//FUNKCIA MAIN
//**************************************************
/// Aplikacia, ktora sluzi ako operacia
/// wait nad danym semaforom to znamena ze znizuje 
/// jeho hodnotu o jednotku. Funkcia prijima len jeden
/// argument ktory je povinny a predstavuje identifikator 
/// semafora nad ktorym sa ma operacia wait vykonat.
/// \param argc vyjadruje pocet zadanych argumentov.
/// \param argv predstavuje pole argumentov v tomto pripade pozostava iba z identifikatora semafora.
/// \return 5 v pripade uspesneho ukoncenia ,-1 v pripade chyby.
int main(int argc, char *argv[])
{
int idSemafora = atoi(argv[1]);
struct sembuf wait[1] = {0,-1,0};//sem_num=0,sem_op=-1,sem_flg=0, pole obsahujuce prvky typu: struktura sembuf. {0,1,0} naplni strukturu nachadzajucu sa na indexe 0,
if(semop(idSemafora,&wait[0],1)<0)
{
printf("Error: Chyba pri wait semafora. Operaciu wait sa nepodarilo vykonat");
return -1;
}
//DEBUG printf("Nova hodnota semafora po vykonani wait:  %d  \n",semctl(idSemafora,0,GETVAL));
return 5;
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;


import EditorsForInfo_Files.InfoSuboryENUM;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileSystemView;


/**
 *
 * @author Dominik
 */
public class IndeterminateProgressBar extends JPanel implements ActionListener, PropertyChangeListener {

    private JProgressBar progressBar;
    private JButton startButton;
    private ArrayList<Info_About_Dirs_File> infoFiles;
    private Integer progress;
    private Task task;

    /**
     * vlakno, ktore zobrazuje priebeh stahovania v progress bare
     */
    class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */

        @Override
        public Void doInBackground() {
            Random random = new Random();
            progress = 0;
            //Initialize progress property.
            setProgress(0);
            //Sleep for at least one second to simulate "startup".
            try {
                Thread.sleep(1000 + random.nextInt(2000));
            } catch (InterruptedException ignore) {
            }
            while (progress < 100) {
                //Sleep for up to one second.
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ignore) {
                }
                //Make random progress.

                setProgress(Math.min(progress, 100));
            }
            startButton.setEnabled(true);
            progressBar.setVisible(false);
            IndeterminateProgressBar.this.getParent().getParent().setEnabled(true);

            JOptionPane.showMessageDialog(null, "Údaje boli úspešne nahrané na sever");
            return null;
        }

    }

    /**
     * Trieda uploadujuca data na server  zobrazujuca priebeh
     */
    public IndeterminateProgressBar() {
        super(new BorderLayout());

        //Create the demo's UI.
        startButton = new JButton("Nahraj data na server");
        startButton.setActionCommand("start");
        startButton.addActionListener(this);

        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setVisible(false);
        //Call setStringPainted now so that the progress bar height
        //stays the same whether or not the string is shown.
        progressBar.setStringPainted(true);

        JPanel panel = new JPanel();
        panel.add(startButton);
        panel.add(progressBar);

        add(panel);
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    }

    /**
     * Trieda uploadujuca data popisane subormi v liste na server  zobrazujuca priebeh uploadu
     */
    public IndeterminateProgressBar(ArrayList<Info_About_Dirs_File> paInfoFiles) {
        super(new BorderLayout());

        //Create the demo's UI.
        startButton = new JButton("Nahraj dáta na server");
        startButton.setActionCommand("start");
        startButton.addActionListener(this);
        startButton.setBackground(new Color(63, 122, 78));
        startButton.setForeground(Color.white);

        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setVisible(false);
        progressBar.setForeground(new Color(35, 212, 188));
        //Call setStringPainted now so that the progress bar height
        //stays the same whether or not the string is shown.
        progressBar.setStringPainted(true);

        JPanel panel = new JPanel();
        panel.add(startButton);
        panel.add(progressBar);

        add(panel);
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        infoFiles = paInfoFiles;
    }
    
    /**
     * Vyber sietovej jednotky v pocitaci, ktora je pripojena na server
     * @return true - sietova jednotka bola vybrana
     */
    public boolean vyberSietovejJednotky()
    {
         System.err.println("NON EXISTS NETWORK DRIVE");
         JOptionPane.showMessageDialog(this, "Nepripojená sieťová jednotka.\n Vyberte disk predstavujúci sieťovú jednotku. "
                             ,
                             "Nepripojená šieťová jednotka",
                             JOptionPane.ERROR_MESSAGE);
             
            final JFileChooser chooser = new JFileChooser("Vyberte sieťovú jednotku");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.showOpenDialog(null);
            if(chooser.getSelectedFile()==null)
            {
                return false;
            }
            else
            {
                File f= new File(chooser.getSelectedFile().getAbsolutePath()+InfoSuboryENUM.aPathInNetworkDrive);
                if(!chooser.getSelectedFile().isAbsolute()||!f.exists())
                {
                    return false;
                }
                else
                {
                    InfoSuboryENUM.setNetworkDrive(chooser.getSelectedFile().getAbsolutePath());
                }
            }
            return true;
    }

    /**
     * Zisti, ci je pripojena sietova jednotka. Ak nie je,
     * zobrazi okno, kde mame vybrat sietovu jednotku.
     * Ak sme vybrali korektnu sietovu jednotku spusti upload dat
     * @param ae - udalost, ktora zavolala metodu
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        File f= new File(InfoSuboryENUM.aNameOfNetworkDriveThatIseConnectToServerDomain);
        if(f.exists())
        {
            System.err.println("EXISTS NETWORK DRIVE"); 
            File nF=new File(f.getAbsolutePath()+InfoSuboryENUM.aPathInNetworkDrive);
            if(!nF.exists())
            {
                 
                 if(!vyberSietovejJednotky())
                 {
                     JOptionPane.showMessageDialog(this, "Nie je pripojená sieťová jednotka alebo nefunguje internetové pripojenie.\n "
                             + "Skontrolujte internetové pripojenie.\n Ak je pripojenie v poriadku,"
                                     + "vytvorte sieťovú jednotku odkazujúcu na server z aktualizáciami.",
                             "Nepripojená šieťová jednotka",
                             JOptionPane.ERROR_MESSAGE);
                 }
                 
                 
            }
            
        }
        else
        {
            if(!vyberSietovejJednotky())
            {
                JOptionPane.showMessageDialog(this, "Nepripojená šieťová jednotka",
                             "Nie je pripojená sieťová jednotka alebo nefunguje internetové pripojenie.\n "
                             + "Skontrolujte internetové pripojenie.\n Ak je pripojenie v poriadku,"
                                     + "vytvorte sieťovú jednotku odkazujúcu na server z aktualizáciami.", 
                             JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        progressBar.setIndeterminate(true);
        startButton.setEnabled(false);
        progressBar.setVisible(true);
        JOptionPane.showMessageDialog(this, "Pocas uploadu neupravujte data","Upload- neupravujte data",JOptionPane.WARNING_MESSAGE);
        this.getParent().getParent().setEnabled(false);
        

        if (infoFiles != null) {
            task = new Task();
            task.addPropertyChangeListener(this);
            task.execute();
            MyThread t = new MyThread();
            t.start();

        }
        //Instances of javax.swing.SwingWorker are not reusuable, so
        //we create new instances as needed.

    }

    /**
     * 
     * @param progress - percentualne mnozstvo aktualne uploadnutych dat
     */
    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    /**
     * - ak listener zisti zmenu hodoty atributu progress, update progress bar
     * @param pce listener, ktory yizituje ci bola zmenena hodnota atribut progress
     */
    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        if ("progress" == pce.getPropertyName()) {
            int progress = (Integer) pce.getNewValue();
            progressBar.setIndeterminate(false);
            progressBar.setValue(progress);

        }
    }

    /**
     * Vlakno, ktore stahuje data a nastavuje priebeh stahovania tomuto objektu
     */
    private class MyThread extends Thread {

        public void run() {

            try {
                UploadDataToServer up = new UploadDataToServer(infoFiles, IndeterminateProgressBar.this);
                up.prepareDataForUpdate();
                up.setDateAndSaveInfoFile(InfoSuboryENUM.TEMPORARY_DEVICE_FOLDER);
                up.uploadPreparedData();
                up.setDateAndSaveInfoFile(InfoSuboryENUM.ROOT_DEVICE_FOLDER);
            } catch (IOException ex) {
                Logger.getLogger(IndeterminateProgressBar.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                 JOptionPane.showMessageDialog(null, "AppError: Upload to server.","Niektoré údaje sa nepodarilo nahrať na sever",JOptionPane.ERROR_MESSAGE);
            }

        }
    }
}

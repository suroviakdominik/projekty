/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Dominik on 7.2.2015.
 */
public class FileWorker {

    private String aAbsolutePath;
    private File aFile;

   public FileWorker()
   {

   }

   public FileWorker(String paAbsolutePathOfFile) {
        aAbsolutePath=paAbsolutePathOfFile;
        aFile=new File(paAbsolutePathOfFile);
    }

    public void setaAbsolutePath(String aAbsolutePath) {
        this.aAbsolutePath = aAbsolutePath;
        aFile=new File(aAbsolutePath);
    }

    public String getaAbsolutePath() {
        return aAbsolutePath;
    }
    
    public boolean existsFile()
    {
        if(aFile.exists())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void createFileIfNonExists()
    {
        if(!aFile.exists())
        {
            try {
                aFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }
    
     /**
     *
     * @return
     * @throws IOException
     */
    public String readDataFromFile() throws IOException {
        String obsahSuboru="";
        if(!isFile())
        {
            throw new FileNotFoundException("The path is not a file path but directory path. You can't read the directory.");
        }
        else
        {   

            try{
                // Open the file that is the first
                // command line parameter
                FileInputStream fstream = new FileInputStream(aAbsolutePath);
                // Get the object of DataInputStream
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"));
                String strLine;
                //Read File Line By Line
                while ((strLine = br.readLine()) != null)   {
                    // Print the content on the console
                    obsahSuboru+=strLine+"\n";
                }
                //Close the input stream
                in.close();
                fstream.close();
                System.out.println("MyLogFiles: Data was successfully read from file"+aAbsolutePath);
            }catch (Exception e){//Catch exception if any
                throw e;
            }
        }
      
        return obsahSuboru;

    }
    
    /**
     *
     * @return
     * @throws IOException
     */
    public String readDataFromFile(String paOddelovacRiadkov) throws IOException {
        String obsahSuboru="";
        if(!isFile())
        {
            throw new FileNotFoundException("The path is not a file path but directory path. You can't read the directory.");
        }
        else
        {

            try{
                // Open the file that is the first
                // command line parameter
                FileInputStream fstream = new FileInputStream(aAbsolutePath);
                // Get the object of DataInputStream
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"));
                
               
                String strLine;
                //Read File Line By Line
                while ((strLine = br.readLine()) != null)   {
                    // Print the content on the console
                    obsahSuboru+=strLine+paOddelovacRiadkov;
                }
                //Close the input stream
                in.close();
                fstream.close();
                System.out.println("MyLogFiles: Data was successfully read from file"+aAbsolutePath);
            }catch (Exception e){//Catch exception if any
                throw e;
            }
        }
        return obsahSuboru;

    }
    
   


    public void writeDataToFile(String paCoZapisat) throws IOException {
        if(!isFile())
        {
            throw new FileNotFoundException("The path is not a file path but directory path. You can't write to directory.");
        }
        else
        {
            try {// if file doesnt exists, then create it
                if (!aFile.exists()) {
                    aFile.createNewFile();
                }
                
                FileWriter fw = new FileWriter(aFile.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(paCoZapisat);
                bw.close();
                fw.close();
                System.out.println("MyLogFiles: Data was successfully write to file"+aAbsolutePath);

            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }


    }

    
    
    private boolean isFile()
    {
        if(aFile.isDirectory())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

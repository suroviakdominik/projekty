/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.struktura;

import dynamickehasovanie.data.Zaznam;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class FileManager {

    private File aSubor;
    private RandomAccessFile aSpracovavacSuboru;
    private PriorityQueue<Long> aVolneBloky;

    public FileManager(File aSubor) {
        try {
            this.aSubor = aSubor;
            aSpracovavacSuboru = new RandomAccessFile(aSubor, "rw");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        aVolneBloky = new PriorityQueue<>(new Comparator<Long>() {

            @Override
            public int compare(Long o1, Long o2) {
                if (o1 > o2) {
                    return -1;
                } else if (o2 > o1) {
                    return 1;
                }
                return 0;
            }
        });
    }

    public void pridajNovyVolnyBlok(Blok paBlok) {

        if ((this.getPocetBlokovVSubore(paBlok.getVelkostBlokuVBajtoch()) - 1) == paBlok.getaCisloBloku()) {
            try {
                int pocetOdstr = 1;
                long pom = paBlok.getaCisloBloku();
                while (aVolneBloky.peek() != null && aVolneBloky.peek() == (pom - 1)) {
                    pocetOdstr++;
                    pom = aVolneBloky.remove();
                }
                long novaDlzka = this.getPocetBlokovVSubore(paBlok.getVelkostBlokuVBajtoch()) * paBlok.getVelkostBlokuVBajtoch() - pocetOdstr * paBlok.getVelkostBlokuVBajtoch();
                aSpracovavacSuboru.setLength(novaDlzka);
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            aVolneBloky.add(paBlok.getaCisloBloku());
        }

    }

    public void pridajNovyVolnyBlok(Long paCisloBloku) {
        aVolneBloky.add(paCisloBloku);
    }

    public long dajCisloBlokuKdeBudeUkladanyNovyBlok(int paVelkostBlokuVBajtoch) {
        long ret;
        if (aVolneBloky.size() > 0) {
            return aVolneBloky.poll();
        } else {
            ret = getPocetBlokovVSubore(paVelkostBlokuVBajtoch);
        }
        return ret;
    }

    public byte[] nacitajZaznamyBloku(Blok paBlok) {
        paBlok.resetZoznamZaznamov();
        try {
            byte[] d = new byte[paBlok.getVelkostBlokuVBajtoch()];
            aSpracovavacSuboru.seek(paBlok.getaCisloBloku() * paBlok.getVelkostBlokuVBajtoch());
            aSpracovavacSuboru.read(d);
            return d;
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return null;
    }

    public void zapisBlokNaDisk(Blok paBlok) {
        try {
            aSpracovavacSuboru.seek(paBlok.getaCisloBloku() * paBlok.getVelkostBlokuVBajtoch());
            aSpracovavacSuboru.write(paBlok.transformujBlokDoBinarnejPodoby());
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public long getPocetBlokovVSubore(long velkostBloku) {
        try {
            return aSpracovavacSuboru.length() / velkostBloku;
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return -1;
    }

    public void zavri() {
        try {
            aSpracovavacSuboru.close();
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean obsahujeVolnyBlok(Long i) {
        return aVolneBloky.contains(i);
    }

    public void ulozZoznamVolnychBlokovDoSuboru(File paFile) {
        StringBuilder dataNaZapis = new StringBuilder(10000);
        for (Long prech : aVolneBloky) {
            dataNaZapis.append(prech + "\n");
        }
        FileWorker.writeDataToFile(paFile, dataNaZapis.toString(), false);
    }

    public void nacitajVolneBlokyZoSuboru(File paFile) {
        aVolneBloky.clear();
        FileWorker fw = new FileWorker(paFile);
        try {
            if (!paFile.exists()) {
                paFile.createNewFile();
                return;
            }
            fw.startRead();
            String loadedLine;
            while ((loadedLine = fw.readNextLine()) != null) {
                aVolneBloky.add(Long.parseLong(loadedLine));
            }

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fw.stopRead();
        }
    }
}

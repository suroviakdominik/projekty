/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import linearne.štrukurované.datové.typy.GUI.GUI;
import linearne.štrukurované.datové.typy.Objekty.IObjekt;

/**
 * Trieda zoznam predstavue šablónu, podľa, ktorej môžu byť vytvárané objekty
 * typu ADT Zoznam. ADT zoznam je implementovaný pomocou poľa.
 *
 * @author Dominik
 */
public class Zoznam extends ILinearDataTypy {

    private IObjekt[] aPrvky;
    private int[] aNasledovniciPrvkov;
    private int[] aNasledovniciVolnych;

    private int aZaciatok;//index zaciatku v poli aPrvky
    private int aAktualnyPrvok;//index aktualneho v poli aPrvky
    private int aAktualnyPocetPrvkov;
    private int aPomocnyAktualnyPreVypis = -1;
    private int aVolny;//index miesta v poli aPrvky, ktore je volne 
    private final int MAX_POCET = 1000;
    private GUI aGui;

    /**
     * Konštruktor príjme ako parameter Gui,ktorý zabezpečuje zobrazenie zoznamu
     * a inicializuje atrbúty triedy zoznam na počiatočnú hodnotu- aPrvky =new
     * int[MAX_POCET], aGui = g; aNasledovniciPrvkov = new
     * int[MAX_POCET],aNasledovniciVolnych = new int[MAX_POCET], aZaciatok = -1;
     * aAktualnyPrvok = -1;aVolny=0;aAktualnyPocetPrvokv = 0; !!!Trieda Gui musí
     * obsahovať metódu setAktualnePouzivanyTab(ILinearDataTypy paTyp), ktorá v
     * prípade, že toto Gui používajú aj iné ADT, nastaví aktuálne používaný ADT
     * .
     *
     * @param g - trieda, ktorá sa postará o zobrazenie zásobníka užívateľovi.
     */
    //AK nepoužívae GUI, tak stači s tejto tiredy vymazať metódu zobrazSa(gg...), ostatné metódy 
    //by mali fungovať
    public Zoznam(GUI g) {
        aPrvky = new IObjekt[MAX_POCET];
        aNasledovniciPrvkov = new int[MAX_POCET];
        aNasledovniciVolnych = new int[MAX_POCET];
        aZaciatok = -1;
        aAktualnyPrvok = -1;
        aVolny = 0;
        aAktualnyPocetPrvkov = 0;
        aGui = g;
    }

    /**
     * Metóda zruši celý zoznam spoločne s prvkami, ktoré obsahuje
     */
    @Override
    public void zrus() {
        aPrvky = null;
        aAktualnyPocetPrvkov = 0;
        aNasledovniciPrvkov = null;
        aNasledovniciVolnych = null;
        aPrvky = new IObjekt[MAX_POCET];
        aNasledovniciPrvkov = new int[MAX_POCET];
        aNasledovniciVolnych = new int[MAX_POCET];
        aZaciatok = -1;
        aVolny = 0;
        aAktualnyPrvok = 0;
        aPomocnyAktualnyPreVypis = 0;
        aAktualnyPocetPrvkov = 0;
    }

    /**
     * Metóda vráti index začiatku(prvého prvku zoznamu) v poli aPrvky
     *
     * @return int-index prvého prvku zoznamu v poli
     */
    public int getaZaciatok() {
        return aZaciatok;
    }

    /**
     * Metóda vráti index aktuálneho prvku zoznamu v poli aPrvky
     *
     * @return int-index aktuálneho prvku zoznamu v poli aPrvky
     */
    public int getaAktualnyPrvok() {
        return aAktualnyPrvok;
    }

    /**
     * Metóda vráti index v poli aPrvky, na ktorý ešte nebol vložený žiadny
     * Prvok
     *
     * @return int-index v poli aPrvky, na ktorý ešte nebol vložený žiadny Prvok
     */
    public int getaVolny() {
        return aVolny;
    }

    /**
     * Metóda vrati aktuálny počet prvkov nachádzajúcich sa v zozname
     *
     * @return int-aktuálny počet prvkov nachádzajúcich sa v zozname
     */
    @Override
    public int mohutnost() {
        return aAktualnyPocetPrvkov;
    }

    /**
     * Metóda vrati true ak aktuálny počet prvkov zoznamu sa rovná 0. Inak vráti
     * false.
     *
     * @return boolean - true=zoznam je prázdny, inak false
     */
    @Override
    public boolean jePrazdny() {
        return aAktualnyPocetPrvkov == 0;
    }

    /**
     * Metóda nastaví aktuálny prvok na začiatočný
     *
     * @return
     */
    public void nastavAktualnyNaZaciatok() {
        aAktualnyPrvok = aZaciatok;
        aPomocnyAktualnyPreVypis = 0;
    }

    /**
     * Metóda nastaví aktuálny prvok za nasledujúci, ktorý nasleduje po ňom
     */
    public void nastavAktualnyNaNasledujuci() throws ArrayIndexOutOfBoundsException {
        aAktualnyPrvok = aNasledovniciPrvkov[aAktualnyPrvok];
        aPomocnyAktualnyPreVypis++;
    }

    /**
     * Metóda zmení hodnoty atribútov objektu na ktorý odkazuje aktuálny prvok
     *
     * @param paPrvok-nový objekt, ktorý zmení predchádzajúci
     */
    public void zapisNaAktualny(IObjekt paPrvok) {
        if (aAktualnyPrvok != -1) {
            aPrvky[aAktualnyPrvok] = paPrvok;
        }

    }

    /**
     * Metóda upraví objekt nachádzajúci sa na začiatku zonamu
     *
     * @param paPrvok-paPrvok, ktorý bude vložený na aktuálnu pozíciu
     * @throws java.lang.Exception-chyba ak je zoznam prázdny, nie je čo vložiť
     */
    public void zapisNaZaciatok(IObjekt paPrvok) throws Exception {
        if (jePrazdny()) {
            Exception zapis = new Exception("Zatial nebol vlozeny ziadny prvok na zaciatok");
            throw zapis;
        } else {
            aPrvky[aZaciatok] = paPrvok;
        }

    }

    /**
     * Metóda vráti objekt ktorý je na začiatku zoznamu
     *
     * @return IObjekt - objekt nachádzajúci sa na začiatku zoznamu
     */
    @Override
    public IObjekt citajZaciatok() {
        try {
            return aPrvky[aZaciatok];
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw ex;
        }
    }

    /**
     * Metóda vráti objekt na aktuálnej pozícii
     *
     * @return IObjekt - objekt na aktuálnej pozícii
     */
    public IObjekt citajAktualny() throws RuntimeException {
        if (aAktualnyPrvok != -1) {
            return aPrvky[aAktualnyPrvok];
        } else {
            throw new RuntimeException("Aktualny nie je nastaveny");
        }
    }

    /**
     * Metóda pridá prvok na začiatok zoznamu
     *
     * @param paPrvok :IObjekt - prvok(objekt typu IObjekt), ktorý bude pridaný
     */
    public void pridajNaZaciatok(IObjekt paPrvok) {
        try {
            aPrvky[aVolny] = paPrvok;
            aNasledovniciPrvkov[aVolny] = aZaciatok;
            aZaciatok = aVolny;
            aAktualnyPocetPrvkov++;
            if (aNasledovniciVolnych[aVolny] != 0) {
                int lpZalohVoln = aVolny;
                aVolny = aNasledovniciVolnych[aVolny];
                aNasledovniciVolnych[lpZalohVoln] = 0;
            } else {
                aVolny = aAktualnyPocetPrvkov;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Zoznam  je plný. Plný je ak je mohutnosť =" + MAX_POCET);
        }
    }

    /**
     * Metóda pridá objekt za aktuálny prvok , ak je nastavený
     *
     * @param paPrvok:IObjekt - prvok(objekt typu IObjekt), ktorý bude pridaný
     */
    public void pridajZaAktualny(IObjekt paPrvok) throws RuntimeException {

        try {
            aPrvky[aVolny] = paPrvok;
            aNasledovniciPrvkov[aVolny] = aNasledovniciPrvkov[aAktualnyPrvok];
            aNasledovniciPrvkov[aAktualnyPrvok] = aVolny;

            aAktualnyPocetPrvkov++;
            if (aNasledovniciVolnych[aVolny] != 0) {
                int lpZalohVoln = aVolny;
                aVolny = aNasledovniciVolnych[aVolny];
                aNasledovniciVolnych[lpZalohVoln] = 0;
            } else {
                aVolny = aAktualnyPocetPrvkov;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Aktualny nie je nastaveny alebo je zoznam plný. Plný je ak je mohutnosť =" + MAX_POCET);
        }

    }

    /**
     * Metóda odoberie prvok za aktuálnym prvkom
     *
     * @return IObjekt - prvok(objekt typu IObjekt)
     */
    public IObjekt odoberZaAkt() {
        IObjekt navrat;
        navrat = aPrvky[aNasledovniciPrvkov[aAktualnyPrvok]];
        int predchVolny = aVolny;

        aPrvky[aNasledovniciPrvkov[aAktualnyPrvok]] = null;//aby sme vedeli co vypisovat, ci to patri alebo nepatri do zoznamu

        aVolny = aNasledovniciPrvkov[aAktualnyPrvok];
        aNasledovniciVolnych[aVolny] = predchVolny;
        aNasledovniciPrvkov[aAktualnyPrvok] = aNasledovniciPrvkov[aNasledovniciPrvkov[aAktualnyPrvok]];
        --aAktualnyPocetPrvkov;
        return navrat;
    }

    /**
     * Metóda odoberie objekt zo začiatku a ako nový začiatok dá ak je to možné
     * jeho nasledovník
     *
     * @return IObjekt -odobraný prvok zo začiatku(Objekt typu IObjekt)
     */
    public IObjekt odoberZoZac() {

        IObjekt navrat;
        navrat = aPrvky[aZaciatok];
        aPrvky[aZaciatok] = null;

        int prechVolny = aVolny;
        aVolny = aZaciatok;
        aNasledovniciVolnych[aVolny] = prechVolny;
        aZaciatok = aNasledovniciPrvkov[aZaciatok];
        --aAktualnyPocetPrvkov;
        return navrat;
    }

    /**
     * Metóda zobrazí/vzkreslí zoznam a jeho prvky na plátno.
     *
     * @param gg - plátno kde bude zásobník zobrazený
     * @param poziciaX-pozicia x na plátne kde bude zoznam vykreslený, v
     * programe je násobená *10,čim dostaneme rozlíšenie plátna(šírka)
     * @param poziciaY -pozicia y na plátne kde bude totnam vykreslený, v
     * programe je násobená *10, čim dostaneme rozlíšenie plátna(výška)
     */
    @Override
    public void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY) {

        //gg.drawString(this.toString(), poziciaX, poziciaY);
        int width = poziciaX * 10;
        int height = poziciaY * 20;
        gg.drawString(new Date().toString(), 20, height - 20);
        aGui.setAktualnePouzivanyTab(this);

        int paPoziciaRectWidth = 5 * width / 100;
        for (int i = 0; paPoziciaRectWidth < width - 10 * width / 100; i++) {
            gg.drawRect(paPoziciaRectWidth = paPoziciaRectWidth + 3 * width / 100, height / 2 - (3 * width / 100) / 2, 3 * width / 100, 3 * width / 100);
            gg.drawString("" + i, paPoziciaRectWidth + 3 * width / 200, (height / 2 - (3 * width / 100) / 2) - 10);

        }

        if (aAktualnyPocetPrvkov > 0) {
            paPoziciaRectWidth = 5 * width / 100;
            int prechadzac = aZaciatok;
            for (int i = 0; i < aAktualnyPocetPrvkov && paPoziciaRectWidth < width - 10 * width / 100; i++) {
                paPoziciaRectWidth = paPoziciaRectWidth + 3 * width / 100;

                if (prechadzac == aAktualnyPrvok) {
                    gg.setColor(Color.RED);
                }

                gg.drawString(aPrvky[prechadzac].toString().substring(8, 9), paPoziciaRectWidth + 3 * width / 200, height / 2 - (3 * width / 100) / 2 + 3 * width / 200);
                prechadzac = aNasledovniciPrvkov[prechadzac];
                gg.setColor(Color.BLACK);
            }
        }

        if (aAktualnyPrvok == -1) {
            gg.drawString("Index Aktualny prvok:  Iterator nie je v zozname/Aktualny prvok nie je nastaveny.", 5 * width / 100, 5 * height / 100);

        } else {

            gg.drawString("Index Aktualny prvok:  " + aPomocnyAktualnyPreVypis, 5 * width / 100, 5 * height / 100);
        }
    }

    /**
     * Metóda načíta prvky, ktoré sme si predtým uložili do súboru z tohoto
     * súboru do zoznamu. Staré prvky sa stratia.
     *
     * @throws java.io.FileNotFoundException-chyba súbor sa nepodarilo
     * načitať,bol buď pozmenený v textovom editore užívateľom alebo žiadny ešte
     * nebol vytvorený
     */
    @Override
    public void nacitajPrvkyZoSuboru() throws FileNotFoundException, IOException {
        File podadresarUlozPrvkyZoznamu = new File("Zoznam ulozisko");
        if (!podadresarUlozPrvkyZoznamu.exists()) {
            throw new FileNotFoundException("Zatial neboli ulozene ziadne prvky");
        }
        String[] nazvyUlozSuborov = podadresarUlozPrvkyZoznamu.list();
        if (nazvyUlozSuborov.length == 0) {
            throw new FileNotFoundException();
        }
        zrus();

        pridajNaZaciatok(vytvorObjektZoSuboru(podadresarUlozPrvkyZoznamu, najdiSuborSporadovymCislom(nazvyUlozSuborov, 1000)));
        nastavAktualnyNaZaciatok();

        for (int i = 1; i < nazvyUlozSuborov.length; i++) {
            pridajZaAktualny(vytvorObjektZoSuboru(podadresarUlozPrvkyZoznamu, najdiSuborSporadovymCislom(nazvyUlozSuborov, 1000 + i)));
            nastavAktualnyNaNasledujuci();
        }
        aAktualnyPrvok = -1;
        aPomocnyAktualnyPreVypis = -1;
    }

    /**
     * Metóda uloží prvky, ktoré sa aktuálne nachádzaju v zásobníku do súboru.
     * Prvky, ktoré boli uložené predtým sa stratia(vymažú).
     *
     * @throws FileNotFoundException- chyba, ak sa nepodarilo vytvoriť súbor pre
     * zápis, kvôli tomu, že taký súbor už existuje.
     */
    @Override
    public void zapisPrvkyDoSuboru() throws FileNotFoundException {
        File podadresarUlozPrvkyZasobnika = new File("Zoznam ulozisko");
        if (!podadresarUlozPrvkyZasobnika.exists()) {
            podadresarUlozPrvkyZasobnika.mkdir();

        } else if (podadresarUlozPrvkyZasobnika.list().length > 0) {
            File prvky[] = podadresarUlozPrvkyZasobnika.listFiles();
            for (int i = 0; i < prvky.length; i++) {
                prvky[i].delete();
            }
        }
        int prechadzac = aZaciatok;

        for (int i = 0; i < aAktualnyPocetPrvkov; i++) {
            try {
                aPrvky[prechadzac].zapisDoSuboru(podadresarUlozPrvkyZasobnika, i);
            } catch (IOException ex) {
                FileNotFoundException fe = new FileNotFoundException("Zoznam=nepodarilo sa ulozit cely.");
                throw fe;
            }
            prechadzac = aNasledovniciPrvkov[prechadzac];
        }
    }

    /**
     * Metóda zobrazí v textovom poli všetky prvky zoznamu, ktoré s v ňom
     * aktuálne nachádzajú.
     *
     * @param poziciaX-pozícia x(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param poziciaY-pozícia y(vľavo hore) iného okna, ktoré vyvoláva túto
     * metódu
     * @param height-výška iného okna, ktoré vyvoláva túto metódu
     * @param width-šírka iného okna, ktoré vyvoláva túto metódu
     */
    @Override
    public void uplnaPrehliadka(int poziciaX, int poziciaY, int height, int width) {
        JDialog lpPrehliadka = new JDialog();
        lpPrehliadka.setTitle("Zoznam-uplna prehliadka");
        lpPrehliadka.setSize(500, 500);
        lpPrehliadka.setLayout(new BorderLayout());

        JTextArea lpTextovePole;
        JScrollPane lpPosuvatko;
        JPanel center = new JPanel();

        center.setLayout(new GridLayout(1, 1));
        lpPrehliadka.add(center, BorderLayout.CENTER);
        lpTextovePole = new JTextArea("Vsetky objekty a ich poradie od zaciatocneho prvku: \n");

        int prechadzac = aZaciatok;
        for (int i = 0; i < aAktualnyPocetPrvkov; i++) {
            lpTextovePole.append((i + 1) + ". " + aPrvky[prechadzac].toString() + "\n");
            prechadzac = aNasledovniciPrvkov[prechadzac];
        }

        lpTextovePole.setEditable(false);
        lpPosuvatko = new JScrollPane(lpTextovePole, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        center.add(lpPosuvatko);

        //lpPrehliadka.pack(); //prizpusob velikost okna
        lpPrehliadka.setLocation(poziciaX + width / 2 - lpPrehliadka.getWidth() / 2, poziciaY + height / 2 - lpPrehliadka.getHeight() / 2);
        lpPrehliadka.setModal(false);
        lpPrehliadka.setVisible(true); //zobraz okno 
    }

}

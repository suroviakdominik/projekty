/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacPouzi implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {

        IPredmet predmetHraca = paHrac.dajPredmet(paPrikaz);
        IPredmet predmetMiestnosti = paHrac.dajPoziciu().zistiPredmet(paPrikaz);
        if ((predmetHraca == null) && (predmetMiestnosti == null)) {
            System.out.println("Nemate taky predmet.");
        } else {
            IPredmet pouzityPredmet = predmetHraca == null ? predmetMiestnosti : predmetHraca;
            if (pouzityPredmet.equals(predmetMiestnosti)) {
                paHrac.zdvihniPredmet(pouzityPredmet);
                paHrac.dajPoziciu().zoberPredmet(predmetMiestnosti.getNazov());
            }
            System.out.println(pouzityPredmet.pouzi(paHrac));
        }
        return false;
    }

}

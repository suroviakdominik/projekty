package com.suroviak.dominik.zooAplication.Navigation;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dominik on 1.4.2015.
 */
public class Navigator {
    private Road aCesta;
    private List<Road.Hrana> aHrany;
    private List<Road.Vrchol> aVrcholy;

    public Navigator(Road aCesta) {
        this.aCesta = aCesta;
        this.aHrany = aCesta.getHrany();
        this.aVrcholy = aCesta.getVrcholy();
    }


    public List<RoadPoint> najdiNajkratsiuCestu(RoadPoint source, RoadPoint target) {
        Road.Vrchol zacCesty = aVrcholy.get(aVrcholy.indexOf(new Road.Vrchol(source, false)));
        Road.Vrchol konCesty = aVrcholy.get(aVrcholy.indexOf(new Road.Vrchol(target, false)));
        zacCesty.setaDoterazNajdenaNajkratsiaCesta(0);
        Road.Vrchol riadiVrchol = null;
        Road.Vrchol susedRiadiaceho = null;
        do {
            riadiVrchol = najdiRiadiaciVrchol();
            riadiVrchol.setaDefinitivnyVrchol(true);
            for (Road.Hrana prech : najdiHranyObsahujuceVrchol(riadiVrchol)) {
                susedRiadiaceho = prech.getaVrchol1().equals(riadiVrchol) ? prech.getaVrchol2() : prech.getaVrchol1();
                if (riadiVrchol.getaDoterazNajdenaNajkratsiaCesta() + prech.getaDistance() < susedRiadiaceho.getaDoterazNajdenaNajkratsiaCesta()) {
                    susedRiadiaceho.setaDoterazNajdenaNajkratsiaCesta(riadiVrchol.getaDoterazNajdenaNajkratsiaCesta() + prech.getaDistance());
                    susedRiadiaceho.setaVrcholZKtorehoSomPrisiel(riadiVrchol);
                }

            }

        }
        while (!riadiVrchol.equals(konCesty));

        return vyhodnotCestu(zacCesty, konCesty);

    }


    public RoadPoint najdiHranuNaKtoruMoznoUmiestnitMojuPolohu(Location paMojaPoloha) {
        double rozdielPridBodSusediaOdBodSused = Double.MAX_VALUE;//rozdiel hodnot |pridavany bod,sused1|+|pridavany bod,sused2|-|sused1,sused2|
        RoadPoint vybBod = new RoadPoint(new RoadPoint.Suradnica(new RoadPoint.Coordinate(paMojaPoloha.getLatitude()),
                new RoadPoint.Coordinate(paMojaPoloha.getLongitude())), RoadPoint.TypBodu.BODCESTYPRENAVIGACIUDEFAULT);
        Road.Vrchol vybVrchol = new Road.Vrchol(vybBod, false);
        if (aVrcholy.contains(vybVrchol)) {
            return aVrcholy.get(aVrcholy.indexOf(vybVrchol)).getaBodPrisluchajuciVrcholu();
        }

        Road.Vrchol vrchol1 = null;//mezi tieto body-1,2 bude pridany novy objekt
        Road.Vrchol vrchol2 = null;
        Location locPridBod = new Location("");
        locPridBod.setLongitude(paMojaPoloha.getLongitude());
        locPridBod.setLatitude(paMojaPoloha.getLatitude());
        double vzdialenostPridBodVrchol1 = 0;
        double vzdialenostPridBodVrchol2 = 0;
        for (int i = 0; i < aHrany.size(); i++) {
            Road.Hrana skumanaHrana = aHrany.get(i);
            LatLng latlng =
                    new LatLng(skumanaHrana.getaVrchol1().getaBodPrisluchajuciVrcholu().getSuradnica().getaLatitude().getDECRepresentationOfCoordinate(),
                            skumanaHrana.getaVrchol1().getaBodPrisluchajuciVrcholu().getSuradnica().getaLongitude().getDECRepresentationOfCoordinate());
            Location loc1 = new Location("");
            loc1.setLatitude(latlng.latitude);
            loc1.setLongitude(latlng.longitude);

            latlng =
                    new LatLng(skumanaHrana.getaVrchol2().getaBodPrisluchajuciVrcholu().getSuradnica().getaLatitude().getDECRepresentationOfCoordinate(),
                            skumanaHrana.getaVrchol2().getaBodPrisluchajuciVrcholu().getSuradnica().getaLongitude().getDECRepresentationOfCoordinate());
            Location loc2 = new Location("");
            loc2.setLatitude(latlng.latitude);
            loc2.setLongitude(latlng.longitude);
            vzdialenostPridBodVrchol1 = loc1.distanceTo(locPridBod);
            vzdialenostPridBodVrchol2 = loc2.distanceTo(locPridBod);
            if ((vzdialenostPridBodVrchol1 + vzdialenostPridBodVrchol2 - loc1.distanceTo(loc2)) < rozdielPridBodSusediaOdBodSused) {
                vrchol1 = skumanaHrana.getaVrchol1();
                vrchol2 = skumanaHrana.getaVrchol2();
                rozdielPridBodSusediaOdBodSused = vzdialenostPridBodVrchol1 + vzdialenostPridBodVrchol2 - loc1.distanceTo(loc2);
            }
        }
        if (vrchol1 != null && vrchol2 != null) {
            if (rozdielPridBodSusediaOdBodSused < 100) {
                aVrcholy.add(vybVrchol);
                aHrany.add(new Road.Hrana(vrchol1, vybVrchol, vzdialenostPridBodVrchol1));
                aHrany.add(new Road.Hrana(vrchol2, vybVrchol, vzdialenostPridBodVrchol2));

            } else {
                return null;
            }

        }
        return vybBod;
    }

    public void odstranHranuKdeSomUmietnilMojuPolohu(RoadPoint paBodMojejPolohy) {
        Road.Vrchol v = new Road.Vrchol(paBodMojejPolohy, false);
        for (int i = 0; i < aHrany.size(); i++) {
            Road.Hrana prech = aHrany.get(i);
            if (prech.getaVrchol1().equals(v) || prech.getaVrchol2().equals(v)) {
                aHrany.remove(prech);
            }
        }
        aVrcholy.remove(v);
    }

    public double vypocitajVzdialenostCesty(List<RoadPoint> bodyCesty) {
        double retVzd = 0;
        Location loc1 = new Location("");
        Location loc2 = new Location("");
        for (int i = 0; i < bodyCesty.size() - 1; i++) {
            loc1.setLatitude(bodyCesty.get(i).getSuradnica().getaLatitude().getDECRepresentationOfCoordinate());
            loc1.setLongitude(bodyCesty.get(i).getSuradnica().getaLongitude().getDECRepresentationOfCoordinate());
            loc2.setLatitude(bodyCesty.get(i + 1).getSuradnica().getaLatitude().getDECRepresentationOfCoordinate());
            loc2.setLongitude(bodyCesty.get(i + 1).getSuradnica().getaLongitude().getDECRepresentationOfCoordinate());
            retVzd += loc1.distanceTo(loc2);
        }
        return retVzd;
    }

    private List<RoadPoint> vyhodnotCestu(Road.Vrchol paZacCesty, Road.Vrchol paKoniecCesty) {
        ArrayList<RoadPoint> ret = new ArrayList<>();
        Road.Vrchol paKoniec = paKoniecCesty;
        if (paKoniec != null && paKoniec.getaVrcholZKtorehoSomPrisiel() != null) {
            ret.add(paKoniec.getaBodPrisluchajuciVrcholu());
            while (!paKoniec.getaVrcholZKtorehoSomPrisiel().equals(paZacCesty)) {
                ret.add(paKoniec.getaVrcholZKtorehoSomPrisiel().getaBodPrisluchajuciVrcholu());
                paKoniec = paKoniec.getaVrcholZKtorehoSomPrisiel();
            }
        }
        ret.add(paZacCesty.getaBodPrisluchajuciVrcholu());
        reset();
        return ret;
    }

    private void reset() {
        for (Road.Vrchol prech : aVrcholy) {
            prech.setaDefinitivnyVrchol(false);
            prech.setaDoterazNajdenaNajkratsiaCesta(Double.MAX_VALUE);
        }
    }

    private List<Road.Hrana> najdiHranyObsahujuceVrchol(Road.Vrchol paVrchol) {
        ArrayList<Road.Hrana> ret = new ArrayList<>();
        for (Road.Hrana prech : aHrany) {
            if (prech.getaVrchol1().equals(paVrchol) || prech.getaVrchol2().equals(paVrchol)) {
                ret.add(prech);
            }
        }
        return ret;
    }

    /**
     * Vrati index riadiaceho vrchola v poli vrcholov
     *
     * @return
     */
    private Road.Vrchol najdiRiadiaciVrchol() {
        double minDlzkaZPocBodu = Double.MAX_VALUE;
        Road.Vrchol ret = null;
        for (Road.Vrchol prech : aVrcholy) {
            if (!prech.isaDefinitivnyVrchol() && prech.getaDoterazNajdenaNajkratsiaCesta() < minDlzkaZPocBodu) {
                minDlzkaZPocBodu = prech.getaDoterazNajdenaNajkratsiaCesta();
                ret = prech;
            }
        }
        return ret;
    }
}

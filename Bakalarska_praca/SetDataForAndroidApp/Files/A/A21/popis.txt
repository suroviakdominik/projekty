

Anakonda velká obývá ve volné přírodě povodí Amazonky v Brazílii, Bolívii, Peru a v Kolumbii; dále se tento velký druh hada vyskytuje na Trinidadu, v Ekvádoru, Venezuely a Guyaně.

Dospělí jedinci dorůstají délky těla kolem 3 metrů (samečci), 5,2 metrů (samičky); maximálně mohou anakondy dorůst 5,8-8 metrů. Základní zbarvení je většinou olivově zelené s černými skvrnami, může se však měnit od téměř černého až po nažloutle zelené. Tak jako všichni hadi má i tato anakonda obě čelisti volně spojené, a je proto schopna pozřít rozměrnou kořist. Při plavání uzavírá anakonda velká nozdry.

Zařízení terária je podobné jako u anakondy žluté, kterou zde na webu rovněž popisujeme. Stejně jako vodní nádrž, mělo být větší (vodní oddíl by měl mít plochu větší zhruba o dalších 50-70 procent). Pro jednu anakondu velkou dlouhou čtyři metry je proto třeba upravit terárium o velikosti minimálně 300 x 200 x 200 centimetrů. K tomuto škrtiči je třeba vždy přistupovat se zvýšenou pozorností! Anakonda velká totiž nekouše dopředu, ale do stran! Tento druh je také mnohem agresivnější a nevypočitatelnější než příbuzná anakonda žlutá.

Anakonda velká má být v zajetí krmena jedenkrát týdně a podáváme ji myši, potkany, králíky a další kořist velkou podle rozměrů hada.

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;
import WorkWithFile.FileWorker;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import com.suroviak.dominik.zooAplication.Navigation.BodNaCeste;
import com.suroviak.dominik.zooAplication.Navigation.Cesta;
import com.suroviak.dominik.zooAplication.Navigation.Coordinate;
import com.suroviak.dominik.zooAplication.Navigation.Distance;

import com.suroviak.dominik.zooAplication.Navigation.Sused;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

/**
 * Zobrazuje a umoznuje editovat nami vytvorenu mapu pouzivanu pre navigaciu
 *
 * @author Dominik
 */
public class TabOfflineMapa extends Tab {

    private Cesta aCesta;
    private final int aPolomerZobrazeniaBodu = 12;
    private final double MIN_LAT = InfoSuboryENUM.MIN_LAT;
    private final double MAX_LAT = InfoSuboryENUM.MAX_LAT;
    private final double MIN_LON = InfoSuboryENUM.MIN_LON;
    private final double MAX_LON = InfoSuboryENUM.MAX_LON;
    private final Color COLOR_OF_SELECTED_POINT = Color.RED;
    private final Color COLOR_OF_NEW_NEIGHBOR_ADD_POINT = Color.ORANGE;
    private int aPolomerZobrazeniaBoduZvieraObjekt=15;

    /**
     * Inicializuje vsetky vizualne komponenty Tabu
     *
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto
     * tabe
     */
    public TabOfflineMapa(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNazovTabu, paCommandProcessingForTab);
        initializeMainTabPanelComponents();
        aCesta = initCestaObject();
        if (aCesta == null) {
            aCesta = new Cesta();
            JOptionPane.showMessageDialog(this, "Nenasla sa defaultna cesta", "Nenajdenny subor s ulozenou cestou", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }

        updateListOfExistsRoadPoint();

        if ((MAX_LAT - MIN_LAT) < (MAX_LON - MIN_LON)) {
            jPlatnoNaKreslenie.setPreferredSize(new Dimension(800, 1200));
        } else if ((MAX_LAT - MIN_LAT) == (MAX_LON - MIN_LON)) {
            jPlatnoNaKreslenie.setPreferredSize(new Dimension(800, 800));
        } else {
            jPlatnoNaKreslenie.setPreferredSize(new Dimension(1200, 800));
        }

        KeyBOard thisTabKeyList = new KeyBOard();

        addKeyListener(thisTabKeyList);
        for (Component prech : getMiddleMainTabPanel().getComponents()) {
            prech.addKeyListener(thisTabKeyList);
            prech.addKeyListener(paKeyListener);
        }
    }

    /**
     * Inicilizuje objekt typu cesta zo suboru, kde je ulozena
     *
     * @return Cesta - objekt typu cesta vytvoreny zo suboru z aktualnymi bodmi
     * a hranami
     */
    public static Cesta initCestaObject() {
        Cesta ret = null;
        File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE + InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);

        SerializationWorker<Cesta> worker = new SerializationWorker<>("");
        if (f.exists()) {

            worker.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE);
            worker.setNameOfFile(InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);
            ret = worker.readObjetctFromFile();
            System.out.println("DEBUG: Object Cesta was successfully created from file: " + InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);
        }
        return ret;
    }

    /**
     * inicializuje vsetky vizualne komponenty tohto tabu
     */
    private void initializeMainTabPanelComponents() {
        JPanel strPanel = getMiddleMainTabPanel();
        jComboBoxVytvoreneBody = new javax.swing.JComboBox();
        jComboBoxVytvoreneBody.setRenderer(new SelectedListCellRenderer());
        jComboBoxVytvoreneBody.setBackground(COLOR_OF_SELECTED_POINT);
        jComboBoxVytvoreneBody.setForeground(Color.WHITE);
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jZoznamSusediaVybranehoBodu = new javax.swing.JList();
        jZoznamSusediaVybranehoBodu.setCellRenderer(new SelectedListCellRenderer());
        jLabel4 = new javax.swing.JLabel();
        jTextLatminutes = new javax.swing.JTextField();
        jTextLatSeconds = new javax.swing.JTextField();
        jTextLonDegree = new javax.swing.JTextField();
        jTextLonMinutes = new javax.swing.JTextField();
        jTextLonSeconds = new javax.swing.JTextField();
        jButtonPridajNovyBod = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jComboVyberSusedaKtorehoChcemPridat = new javax.swing.JComboBox();
        jComboVyberSusedaKtorehoChcemPridat.setBackground(COLOR_OF_NEW_NEIGHBOR_ADD_POINT);
        jComboVyberSusedaKtorehoChcemPridat.setForeground(Color.BLACK);
        jButtonPridajSuseda = new javax.swing.JButton();
        jTextLatDegree = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jButtonOddial = new javax.swing.JButton();
        jButtonPribliz = new javax.swing.JButton();
        jPlatnoNaKreslenie = new javax.swing.JPanel() {
            public void paint(Graphics g) {
                super.paint(g);
                ourCustomPaintingMethod(g);
            }
        };
        jButtonOdstranBodZCesty = new javax.swing.JButton();

        jComboBoxVytvoreneBody.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vypisSusedovVybranehoBodu(evt);
            }
        });

        jLabel1.setText("Bod, ku ktorému môžem pridať  susedov:");

        jLabel2.setText("Susedia: ");

        jLabel3.setText("LATITUDE :");

        jZoznamSusediaVybranehoBodu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jListOfNeigborsOfSelectedRoadPointKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jZoznamSusediaVybranehoBodu);

        jLabel4.setText("LONGITUDE    :");

        jButtonPridajNovyBod.setText("Vytvor nový bod na ceste");
        jButtonPridajNovyBod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddNewRoadPointActionPerformed(evt);
            }
        });

        jLabel5.setText("Pridaj nového suseda: ");

        jComboVyberSusedaKtorehoChcemPridat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboCoosedPointThatMayBeAddedAsNeighbortActionPerformed(evt);
            }
        });

        jButtonPridajSuseda.setText("Pridaj suseda");
        jButtonPridajSuseda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddNeigborActionPerformed(evt);
            }
        });

        jPlatnoNaKreslenie.setBackground(Color.WHITE);
        jPlatnoNaKreslenie.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMapCanvasMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPlatnoNaKreslenieLayout = new javax.swing.GroupLayout(jPlatnoNaKreslenie);
        jPlatnoNaKreslenie.setLayout(jPlatnoNaKreslenieLayout);
        jPlatnoNaKreslenieLayout.setHorizontalGroup(
                jPlatnoNaKreslenieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 434, Short.MAX_VALUE)
        );
        jPlatnoNaKreslenieLayout.setVerticalGroup(
                jPlatnoNaKreslenieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 303, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(jPlatnoNaKreslenie);

        jButtonOdstranBodZCesty.setText("Odstrán Vybraný Bod- označený červenou farbou ");
        jButtonOdstranBodZCesty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoveSelectedRoadPointFromRoadActionPerformed(evt);
            }
        });

        jButtonOddial.setText("Oddiaľ");
        jButtonOddial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOddialActionPerformed(evt);
            }

            private void jButtonOddialActionPerformed(ActionEvent evt) {
                Dimension currentSize = jPlatnoNaKreslenie.getPreferredSize();
                int currentWidth = (int) currentSize.getWidth();
                int currentHeight = (int) currentSize.getHeight();
                jPlatnoNaKreslenie.setPreferredSize(new Dimension(currentWidth - 200, currentHeight - 200));
                jPlatnoNaKreslenie.revalidate();
            }
        });

        jButtonPribliz.setText("Priblíž");
        jButtonPribliz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPriblizActionPerformed(evt);
            }

            private void jButtonPriblizActionPerformed(ActionEvent evt) {
                Dimension currentSize = jPlatnoNaKreslenie.getPreferredSize();
                int currentWidth = (int) currentSize.getWidth();
                int currentHeight = (int) currentSize.getHeight();
                jPlatnoNaKreslenie.setPreferredSize(new Dimension(currentWidth + 200, currentHeight + 200));
                jPlatnoNaKreslenie.revalidate();

            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(strPanel);
        strPanel.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jButtonPridajNovyBod, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonOdstranBodZCesty, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jLabel5)
                                                        .addGap(100, 100, 100)
                                                        .addComponent(jComboVyberSusedaKtorehoChcemPridat, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, Short.MAX_VALUE)
                                                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jComboBoxVytvoreneBody, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE))))
                                        .addGap(22, 22, 22)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jButtonPridajSuseda, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                                                        .addGap(38, 38, 38))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGap(0, 0, Short.MAX_VALUE)
                                                                        .addComponent(jButtonPribliz)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jButtonOddial))
                                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE))
                                                        .addGap(63, 63, 63))))
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextLatDegree)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextLatminutes)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextLatSeconds)
                                        .addGap(78, 78, 78)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextLonDegree)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextLonMinutes)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextLonSeconds)
                                        .addGap(38, 38, 38))))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jComboBoxVytvoreneBody))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jScrollPane1)
                                                        .addGap(18, 18, 18))))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jButtonOddial)
                                                .addComponent(jButtonPribliz))
                                        .addGap(9, 9, 9)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jComboVyberSusedaKtorehoChcemPridat)
                                .addComponent(jLabel5)
                                .addComponent(jButtonPridajSuseda))
                        .addGap(30, 30, 30)
                        .addComponent(jButtonOdstranBodZCesty)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextLatminutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextLatSeconds, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextLonDegree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextLonMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextLonSeconds, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextLatDegree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonPridajNovyBod))
        );
    }

    /**
     * Kez listener pouzivajuci sa v ramci zalozky mapa
     */
    private class KeyBOard extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if ((e.getKeyCode() == KeyEvent.VK_S) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
                System.out.println("DEBUG: CTRL+S presed");
                jMenuSaveRoadActionPerformed(null);
            }

        }

    }

    /**
     * metoda, ktroa vykresli mapu na obrazovku v urcenom panele
     *
     * @param g
     */
    private void ourCustomPaintingMethod(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        int sirkaPlatna = jPlatnoNaKreslenie.getWidth();
        int vyskaPlatna = jPlatnoNaKreslenie.getHeight();
        double rozdielMinMaxLat = MAX_LAT * 10000 - MIN_LAT * 10000;
        double rozdielMinMaxLon = MAX_LON * 10000 - MIN_LON * 10000;

        //System.out.println("DEBUG: Paint method call on JPanel");
        //System.out.println("Width: " + sirkaPlatna + " Height: " + vyskaPlatna);
        if (aCesta.getPocetBodovNaCeste() != 0) {
            for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
                double lat = aCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate();
                double lon = aCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate();
                lat = lat * 10000 - MIN_LAT * 10000;
                lon = lon * 10000 - MIN_LON * 10000;
                int nasobitelX = sirkaPlatna / (int) rozdielMinMaxLon;
                int nasobitelY = vyskaPlatna / (int) rozdielMinMaxLat;

                double xPositionOfPoint = lon * nasobitelX;
                double yPositionOfPoint = vyskaPlatna - lat * nasobitelY;
                //System.out.println("xPosPoint: " + (int) xPositionOfPoint);
                //System.out.println("yPosPoint: " + (int) yPositionOfPoint);
                if (aCesta.getBodNaCeste(i).equals(jComboBoxVytvoreneBody.getSelectedItem())) {
                    g2.setColor(COLOR_OF_SELECTED_POINT);
                    g2.fillOval((int) xPositionOfPoint, (int) yPositionOfPoint, aPolomerZobrazeniaBodu, aPolomerZobrazeniaBodu);
                } else if (aCesta.getBodNaCeste(i).equals(jComboVyberSusedaKtorehoChcemPridat.getSelectedItem())) {
                    g2.setColor(COLOR_OF_NEW_NEIGHBOR_ADD_POINT);
                    g2.fillOval((int) xPositionOfPoint, (int) yPositionOfPoint, aPolomerZobrazeniaBodu, aPolomerZobrazeniaBodu);
                } else if (aCesta.getBodNaCeste(i).getTypBodu() == BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT) {
                    g2.setColor(Color.decode("#0e2f44"));
                    g2.fillOval((int) xPositionOfPoint, (int) yPositionOfPoint, aPolomerZobrazeniaBodu, aPolomerZobrazeniaBodu);

                } else if (aCesta.getBodNaCeste(i).getTypBodu() == BodNaCeste.TypBodu.OBJEKTALEBOZVIERA) {
                    g2.setColor(Color.decode("#40e0d0"));
                    g2.fillOval((int) xPositionOfPoint, (int) yPositionOfPoint, aPolomerZobrazeniaBoduZvieraObjekt, aPolomerZobrazeniaBoduZvieraObjekt);

                } else {
                    g2.setColor(Color.decode("#800080"));
                    g2.fillOval((int) xPositionOfPoint, (int) yPositionOfPoint, aPolomerZobrazeniaBodu, aPolomerZobrazeniaBodu);

                }
                
            }

            for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
                double lat = aCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate();
                double lon = aCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate();
                lat = lat * 10000 - MIN_LAT * 10000;
                lon = lon * 10000 - MIN_LON * 10000;
                int nasobitelX = sirkaPlatna / (int) rozdielMinMaxLon;
                int nasobitelY = vyskaPlatna / (int) rozdielMinMaxLat;

                double xPositionOfPoint = lon * nasobitelX;
                double yPositionOfPoint = vyskaPlatna - lat * nasobitelY;
                for (int j = 0; j < aCesta.getBodNaCeste(i).getPocetSusedov(); j++) {

                    lat = aCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getDECRepresentationOfCoordinate();
                    lon = aCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getDECRepresentationOfCoordinate();
                    lat = lat * 10000 - MIN_LAT * 10000;
                    lon = lon * 10000 - MIN_LON * 10000;
                    nasobitelX = sirkaPlatna / (int) rozdielMinMaxLon;
                    nasobitelY = vyskaPlatna / (int) rozdielMinMaxLat;

                    double xPositionOfPointSused = lon * nasobitelX;
                    double yPositionOfPointSused = vyskaPlatna - lat * nasobitelY;
                    g2.setColor(Color.GREEN);
                    g2.drawLine((int) xPositionOfPoint + aPolomerZobrazeniaBodu / 2, (int) yPositionOfPoint + aPolomerZobrazeniaBodu / 2,
                            (int) xPositionOfPointSused + aPolomerZobrazeniaBodu / 2, (int) yPositionOfPointSused + aPolomerZobrazeniaBodu / 2);
                }
            }
        }
    }

    /**
     *
     * vytvori novy bod cesty a zobrazi ho na mape
     *
     * @param evt - udalost, ktora bola vyvolane
     */
    private void jButtonAddNewRoadPointActionPerformed(java.awt.event.ActionEvent evt) {
        if (platneZadaneKoordinaty()) {
            Coordinate lat = new Coordinate(jTextLatDegree.getText(), jTextLatminutes.getText(), jTextLatSeconds.getText());
            Coordinate lon = new Coordinate(jTextLonDegree.getText(), jTextLonMinutes.getText(), jTextLonSeconds.getText());
            if (aCesta.getBodNaCeste(lat, lon) == null) {
                BodNaCeste vybBod = new BodNaCeste(lat, lon, BodNaCeste.TypBodu.BODCESTYVYTVORENYPOUZIVATELOM);
                aCesta.pridajNovyBodNaCestu(vybBod);
                updateListOfExistsRoadPoint();
                jMenuSaveRoadActionPerformed(null);
                jComboBoxVytvoreneBody.setSelectedItem(vybBod);
                jPlatnoNaKreslenie.repaint();
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Bod uz nie je potrebne vytvarat,\n  "
                    + "pretoze existuje \n",
                    "Bod uz existuje", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Niektory z koordinatov nie je cislo,\n  "
                    + "nie je vyplneny alebo nie je z rozsahu oblasti: \n"
                    + "LAT Interval: <" + MIN_LAT + ", " + MAX_LAT + ">\n"
                    + "LON Interval: <" + MIN_LON + " ," + MAX_LON + ">",
                    "Neplatne vstupne udaje", JOptionPane.WARNING_MESSAGE);
        }

    }

    /**
     * aktualizuje zoznam existujucich bodov cesty a vypis tieto body vo
     * vizualnom komponente
     */
    private void updateListOfExistsRoadPoint() {
        jComboBoxVytvoreneBody.removeAllItems();

        for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
            jComboBoxVytvoreneBody.addItem(aCesta.getBodNaCeste(i));
            jComboVyberSusedaKtorehoChcemPridat.addItem(aCesta.getBodNaCeste(i));
        }

        updateRoadPointThatMayBeAddAsNeighborsForSelectedRoadPoint();
    }

    /**
     * aktualizuje zoznam susedov, ktore mozu byt pridane ako susedia k
     * vybranemu bodu a tieto body zobrazi v urcenom komponente
     */
    private void updateRoadPointThatMayBeAddAsNeighborsForSelectedRoadPoint() {
        if (jComboBoxVytvoreneBody.getSelectedItem() != null) {

            jComboVyberSusedaKtorehoChcemPridat.removeAllItems();
            for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
                jComboVyberSusedaKtorehoChcemPridat.addItem(aCesta.getBodNaCeste(i));
            }
            jComboVyberSusedaKtorehoChcemPridat.removeItem(jComboBoxVytvoreneBody.getSelectedItem());
            BodNaCeste vybB = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
            for (Sused prech : vybB.getSusedov()) {
                jComboVyberSusedaKtorehoChcemPridat.removeItem(prech.getSusednyBod());
            }

        }

    }

    /**
     * vypise zoznam aktualnych susedov vybraneho bodu v urcenom komponente
     *
     * @param evt- udalost, ktora metodu vyvolala
     */
    private void vypisSusedovVybranehoBodu(java.awt.event.ActionEvent evt) {
        if (jComboBoxVytvoreneBody.getSelectedItem() != null) {
            jZoznamSusediaVybranehoBodu.removeAll();
            BodNaCeste vybranyBod = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
            Object susedia[] = vybranyBod.getSusedov().toArray();
            jZoznamSusediaVybranehoBodu.setListData(susedia);
            updateRoadPointThatMayBeAddAsNeighborsForSelectedRoadPoint();
            jPlatnoNaKreslenie.repaint();
        }
    }

    /**
     * prida noveho sueda vybranemu bodu
     *
     * @param evt - udalost, ktora metody vyvolala
     */
    private void jButtonAddNeigborActionPerformed(java.awt.event.ActionEvent evt) {
        if (jComboBoxVytvoreneBody.getSelectedItem() != null) {

            BodNaCeste pridavamSuseda = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();

            if (jComboVyberSusedaKtorehoChcemPridat.getSelectedItem() != null) {

                BodNaCeste pridavanySused = (BodNaCeste) jComboVyberSusedaKtorehoChcemPridat.getSelectedItem();
                if (pridavamSuseda.getTypBodu() == BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT && pridavanySused.getTypBodu() == BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT) {
                    JOptionPane.showMessageDialog(this, "Nemozno vytvorit susedstvo medzi defaultnymi bodmi", "Susedstvo nevytvorene", JOptionPane.WARNING_MESSAGE);
                } else {
                    pridavamSuseda.pridajNovySusednyBod(pridavanySused);
                    pridavanySused.pridajNovySusednyBod(pridavamSuseda);
                    jMenuItemDistancesActionPerformed(aCesta);
                    saveRoadToFile(aCesta);
                    vypisSusedovVybranehoBodu(evt);
                    jPlatnoNaKreslenie.repaint();

                }

            } else {
                JOptionPane.showMessageDialog(this, "Nezadany sused.\nNajskor zvolte suseda.",
                        "Nevybrany sused", JOptionPane.WARNING_MESSAGE);
            }

        }
    }

    /**
     * odstrani suseda vybraneho bodu, ak je to mozne
     *
     * @param evt - udalost, ktora metodu vyvolala
     */
    private void jListOfNeigborsOfSelectedRoadPointKeyPressed(java.awt.event.KeyEvent evt) {
        if (jZoznamSusediaVybranehoBodu.getSelectedValue() != null) {
            if (evt.getKeyCode() == KeyEvent.VK_DELETE) {

                BodNaCeste vybBod = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
                if (vybBod.getTypBodu() == BodNaCeste.TypBodu.OBJEKTALEBOZVIERA && vybBod.getPocetSusedov() == 1) {
                    JOptionPane.showMessageDialog(this, "Bod typu objekt zviera musí mať aspoň jedného suseda.\n"
                            + "Po odstránení by objektu/zviera´tu nezostal sused a nebola by možná navigácia k nemu.",
                            "Suseda nemožno odstrániť",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                }
                Sused susedKZmaz = (Sused) jZoznamSusediaVybranehoBodu.getSelectedValue();
                vybBod.odstranSusedstvo(susedKZmaz);
                if (vybBod.getTypBodu() != BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT || susedKZmaz.getSusednyBod().getTypBodu() != BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT) {
                    for (int i = 0; i < susedKZmaz.getSusednyBod().getPocetSusedov(); i++) {
                        if (susedKZmaz.getSusednyBod().getSused(i).getSusednyBod().equals(vybBod)) {
                            susedKZmaz.getSusednyBod().odstranSusedstvo(susedKZmaz.getSusednyBod().getSused(i));
                        }
                    }
                    jPlatnoNaKreslenie.repaint();
                    jMenuSaveRoadActionPerformed(null);
                    vypisSusedovVybranehoBodu(null);
                    System.out.println("DEBUG: Neighbor deleted");
                } else {
                    JOptionPane.showMessageDialog(this, "Susedstvo medzi dvomi tmavo modrými bodmi nemôžete rušiť, lebo sú defaultné.",
                            "Suseda nemožno odstrániť",
                            JOptionPane.WARNING_MESSAGE);
                }

            }
        }
    }

    /**
     * ulozi aktualnu cestu do suboru
     *
     * @param evt - udalost, ktora metodu vyvolala
     */
    private void jMenuSaveRoadActionPerformed(java.awt.event.ActionEvent evt) {
        saveRoadToFile(aCesta);
    }

    public static void saveRoadToFile(Cesta paRoad) {
        if (paRoad != null) {
            SerializationWorker worker = new SerializationWorker("");
            worker.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE);
            worker.setNameOfFile(InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);
            worker.setObjectThatIsSerializable(paRoad);
            worker.saveObjectToFile();
        } else {
            JOptionPane.showMessageDialog(null,
                    "Nepodarilo sa ulozit zmeny vytvorene na ceste.",
                    "Neulozena cesta",
                    JOptionPane.ERROR_MESSAGE);
        }
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE + InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_ANDROID);
        String text = "";
        for (int i = 0; i < paRoad.getPocetBodovNaCeste(); i++) {
            text += paRoad.getBodNaCeste(i).getaLatitude().getaDegree() + ":";
            text += paRoad.getBodNaCeste(i).getaLatitude().getaMinutes() + ":";
            text += paRoad.getBodNaCeste(i).getaLatitude().getaSeconds() + ";;;";
            text += paRoad.getBodNaCeste(i).getaLongitude().getaDegree() + ":";
            text += paRoad.getBodNaCeste(i).getaLongitude().getaMinutes() + ":";
            text += paRoad.getBodNaCeste(i).getaLongitude().getaSeconds() + "|";

            for (int j = 0; j < paRoad.getBodNaCeste(i).getPocetSusedov(); j++) {
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaDegree() + ":";
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaMinutes() + ":";
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaSeconds() + ";;;";
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaDegree() + ":";
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaMinutes() + ":";
                text += paRoad.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaSeconds() + ";;;";
                text += paRoad.getBodNaCeste(i).getSused(j).getVzdialenost() + "|";
            }
            text += "|";
        }
        try {

            fw.writeDataToFile(text);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error subor: mapa pre android  vytvorit", "ERROR: Subor mapa pre android nevytvoreny", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Odstrani vybrany bod z cesty
     *
     * @param evt - udalost, ktora metodu vyvolala
     */
    private void jButtonRemoveSelectedRoadPointFromRoadActionPerformed(java.awt.event.ActionEvent evt) {
        if (jComboBoxVytvoreneBody.getSelectedItem() != null) {
            BodNaCeste bNC = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
            if (bNC.getTypBodu() != BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT
                    && !existAnimalOrObjectWithThisGEOCoordintate(bNC.getaLatitude().getDECRepresentationOfCoordinate(), bNC.getaLongitude().getDECRepresentationOfCoordinate())) {
                BodNaCeste bodNaodstr = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
                for (int i = 0; i < bNC.getPocetSusedov(); i++) {
                    if (bodNaodstr.getSusednyBod(i).getTypBodu() == BodNaCeste.TypBodu.OBJEKTALEBOZVIERA) {
                        JOptionPane.showMessageDialog(this, "Aktuálne zvolený bod má suseda typu objekt/zviera,\n"
                                + "Mazať môžete iba vami vytvorené body na ceste-fialové, ktoré nemajú suseda zviera.",
                                "Nemožno mazať body, ktoré majú ako suseda zviera. Nebude k nim možná navigácia. \n"
                                + "Ak ho chcete vymazať, najskôr upravte mapu.",
                                JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                }
                for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
                    if (!aCesta.getBodNaCeste(i).equals(bodNaodstr)) {
                        for (int j = 0; j < aCesta.getBodNaCeste(i).getPocetSusedov(); j++) {
                            if (aCesta.getBodNaCeste(i).getSusednyBod(j).equals(bodNaodstr)) {
                                aCesta.getBodNaCeste(i).odstranSusedstvo(aCesta.getBodNaCeste(i).getSused(j));
                            }
                        }
                    }
                }
                aCesta.odstranBodZCesty(bodNaodstr);
                jMenuSaveRoadActionPerformed(null);
                updateListOfExistsRoadPoint();
                jPlatnoNaKreslenie.repaint();
                System.out.println("DEBUG: Bod uspesne odstraneny z cesty.");
            } else {
                JOptionPane.showMessageDialog(this, "Defaultné body a body priradene zvieratu alebo objektu- tmavo/slabo modré  nemožete mazať,\n"
                        + "Mazať môžete iba vami vytvorené body na ceste-fialové a body. Ak chcete odstranit bod zvierata/ objektu, potom\n"
                        + "nesmie byt priradeny ziadnemu zvieratu, resp. objektu.",
                        "Nemožno mazať defaultné body a bod priradene objektu alebo zvieratu",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private boolean existAnimalOrObjectWithThisGEOCoordintate(double lat, double lon) {
        SerializationWorker<Info_About_Dirs_File> s = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        s.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.ANIMALS.getaRelativePathOfDirecotryWhereIAM());
        s.setNameOfFile(InfoSuboryENUM.ANIMALS.getaNameOfInfoFile());
        Info_About_Dirs_File animals = s.readObjetctFromFile();
        s.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.OBJECTS.getaRelativePathOfDirecotryWhereIAM());
        s.setNameOfFile(InfoSuboryENUM.OBJECTS.getaNameOfInfoFile());
        Info_About_Dirs_File objects = s.readObjetctFromFile();
        if (animals != null && objects != null) {
            for (String prech : animals.getaItemsInfo().keySet()) {
                if (animals.getaItemsInfo().get(prech).getZemepisnaDlzka() == lon && animals.getaItemsInfo().get(prech).getZemepisnaSirka() == lat) {
                    return true;
                }
            }

            for (String prech : objects.getaItemsInfo().keySet()) {
                if (objects.getaItemsInfo().get(prech).getZemepisnaDlzka() == lon && objects.getaItemsInfo().get(prech).getZemepisnaSirka() == lat) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * prekreslenie platn pri zmene bodu, ktory moznno pridat ako sused
     *
     * @param evt
     */
    private void jComboCoosedPointThatMayBeAddedAsNeighbortActionPerformed(java.awt.event.ActionEvent evt) {
        jPlatnoNaKreslenie.repaint();
    }

    /**
     * osetrenie udalosti pri kliknuti na platno
     *
     * @param evt - udalost, ktora metodu vyvolala
     */
    private void jMapCanvasMousePressed(java.awt.event.MouseEvent evt) {
        int clickedX = evt.getX();
        int clickedY = evt.getY();
        int sirkaPlatna = jPlatnoNaKreslenie.getWidth();
        int vyskaPlatna = jPlatnoNaKreslenie.getHeight();
        double rozdielMinMaxLat = MAX_LAT * 10000 - MIN_LAT * 10000;
        double rozdielMinMaxLon = MAX_LON * 10000 - MIN_LON * 10000;

        System.out.println("DEBUG: Cliked on canvas: X:" + clickedX + " ,Y:" + clickedY);
        double xPositionOfPoint = -1;
        double yPositionOfPoint = -1;
        double minRozdielXClickXBod = Double.MAX_VALUE;
        double minRozdielYClickYBod = Double.MAX_VALUE;
        BodNaCeste vybranyBod = (BodNaCeste) jComboBoxVytvoreneBody.getSelectedItem();
        if (aCesta.getPocetBodovNaCeste() != 0) {
            for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
                double lat = aCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate();
                double lon = aCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate();
                lat = lat * 10000 - MIN_LAT * 10000;
                lon = lon * 10000 - MIN_LON * 10000;
                int nasobitelX = sirkaPlatna / (int) rozdielMinMaxLon;
                int nasobitelY = vyskaPlatna / (int) rozdielMinMaxLat;
                xPositionOfPoint = lon * nasobitelX;
                yPositionOfPoint = vyskaPlatna - lat * nasobitelY;
                xPositionOfPoint = (int) xPositionOfPoint;
                yPositionOfPoint = (int) yPositionOfPoint;

                if (Math.abs(xPositionOfPoint - clickedX) < minRozdielXClickXBod || Math.abs(yPositionOfPoint - clickedY) < minRozdielYClickYBod) {
                    if (Math.abs(xPositionOfPoint - clickedX) <= aPolomerZobrazeniaBodu && Math.abs(yPositionOfPoint - clickedY) <= aPolomerZobrazeniaBodu) {
                        minRozdielXClickXBod = Math.abs(xPositionOfPoint - clickedX);
                        minRozdielYClickYBod = Math.abs(yPositionOfPoint - clickedY);
                        vybranyBod = aCesta.getBodNaCeste(i);
                    }
                    if (aCesta.getBodNaCeste(i).getTypBodu()==BodNaCeste.TypBodu.OBJEKTALEBOZVIERA
                            &&Math.abs(xPositionOfPoint - clickedX) <= aPolomerZobrazeniaBoduZvieraObjekt 
                            && Math.abs(yPositionOfPoint - clickedY) <= aPolomerZobrazeniaBoduZvieraObjekt) {
                        minRozdielXClickXBod = Math.abs(xPositionOfPoint - clickedX);
                        minRozdielYClickYBod = Math.abs(yPositionOfPoint - clickedY);
                        vybranyBod = aCesta.getBodNaCeste(i);
                        break;
                    }
                }

            }
            if (SwingUtilities.isLeftMouseButton(evt)) {
                jComboBoxVytvoreneBody.setSelectedItem(vybranyBod);

            } else if (SwingUtilities.isRightMouseButton(evt)) {
                for (int i = 0; i < jComboVyberSusedaKtorehoChcemPridat.getItemCount(); i++) {
                    if (jComboVyberSusedaKtorehoChcemPridat.getItemAt(i).equals(vybranyBod)) {
                        jComboVyberSusedaKtorehoChcemPridat.setSelectedItem(vybranyBod);
                        break;
                    }
                }
            }

        }

    }

    /**
     * vypocita vzdialenost vybraneho bodu od suseda
     *
     * @param paCesta
     */
    private static void jMenuItemDistancesActionPerformed(Cesta paCesta) {
        for (int i = 0; i < paCesta.getPocetBodovNaCeste(); i++) {
            for (int j = 0; j < paCesta.getBodNaCeste(i).getPocetSusedov(); j++) {

                double distance = Distance.distance(paCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate(),
                        paCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate(),
                        paCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getDECRepresentationOfCoordinate(),
                        paCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getDECRepresentationOfCoordinate(),
                        'M');
                paCesta.getBodNaCeste(i).getSused(j).setVzdialenost(distance);

            }

        }

    }

    /**
     * prida novy objekt na cestu, t.j. novy bod cesty
     *
     * @param evt udalost, ktora metodu vyvolala
     */
    private void jButtonNovyObjektActionPerformed(java.awt.event.ActionEvent evt) {
        if (platneZadaneKoordinaty()) {
            Coordinate lat = new Coordinate(jTextLatDegree.getText(), jTextLatminutes.getText(), jTextLatSeconds.getText());
            Coordinate lon = new Coordinate(jTextLonDegree.getText(), jTextLonMinutes.getText(), jTextLonSeconds.getText());
            BodNaCeste bod = pridajNovyObjekt(aCesta, lat, lon);
            if (bod != null) {
                jMenuSaveRoadActionPerformed(null);

                updateListOfExistsRoadPoint();
                jComboBoxVytvoreneBody.setSelectedItem(bod);

                jPlatnoNaKreslenie.repaint();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Niektory z koordinatov nie je cislo,\n  "
                    + "nie je vyplneny alebo nie je z rozsahu oblasti: \n"
                    + "Zemepisná Šírka rozmedzie: < "
                    + InfoSuboryENUM.MIN_LAT_DEGREE + ": "
                    + InfoSuboryENUM.MIN_LAT_MINUTES + ":"
                    + InfoSuboryENUM.MIN_LAT_SECONDS + " ; "
                    + InfoSuboryENUM.MAX_LAT_DEGREE + ": " + InfoSuboryENUM.MAX_LAT_MINUTES + ":"
                    + InfoSuboryENUM.MAX_LAT_SECONDS + " >\n "
                    + "Zemepisná dĺžka rozmedzie: < "
                    + InfoSuboryENUM.MIN_LON_DEGREE
                    + ": " + InfoSuboryENUM.MIN_LON_MINUTES + ":"
                    + InfoSuboryENUM.MIN_LON_SECONDS + " ; "
                    + InfoSuboryENUM.MAX_LON_DEGREE + ": " + InfoSuboryENUM.MAX_LAT_MINUTES + ":"
                    + InfoSuboryENUM.MAX_LON_SECONDS + " >",
                    "Neplatne vstupne udaje", JOptionPane.WARNING_MESSAGE);

        }

    }

    /**
     * prida novy objekt na cestu, t.j. novy bod cesty typu objekt/zviera
     *
     * @param evt udalost, ktora metodu vyvolala
     */
    public static BodNaCeste pridajNovyObjekt(Cesta paCesta, Coordinate lat, Coordinate lon) {
        BodNaCeste vybBod = new BodNaCeste(lat, lon, BodNaCeste.TypBodu.OBJEKTALEBOZVIERA);
        double rozdielPridBodSusediaOdBodSused = Double.MAX_VALUE;//rozdiel hodnot |pridavany bod,sused1|+|pridavany bod,sused2|-|sused1,sused2|

        BodNaCeste bod1 = null;//mezi tieto body-1,2 bude pridany novy objekt
        BodNaCeste bod2 = null;

        for (int i = 0; i < paCesta.getPocetBodovNaCeste(); i++) {
            double vzdialenostPridBoduOdBoduVCeste = Distance.distance(paCesta.getBodNaCeste(i).getaLatitude().getDECRepresentationOfCoordinate(),
                    paCesta.getBodNaCeste(i).getaLongitude().getDECRepresentationOfCoordinate(),
                    vybBod.getaLatitude().getDECRepresentationOfCoordinate(),
                    vybBod.getaLongitude().getDECRepresentationOfCoordinate(),
                    'M');
            if (vzdialenostPridBoduOdBoduVCeste < 100) {
                for (int j = 0; j < paCesta.getBodNaCeste(i).getPocetSusedov(); j++) {
                    double vzdialenostPridavanehoBoduOdSuseda = Distance.distance(paCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getDECRepresentationOfCoordinate(),
                            paCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getDECRepresentationOfCoordinate(),
                            vybBod.getaLatitude().getDECRepresentationOfCoordinate(),
                            vybBod.getaLongitude().getDECRepresentationOfCoordinate(),
                            'M');
                    if (vzdialenostPridBoduOdBoduVCeste
                            + vzdialenostPridavanehoBoduOdSuseda
                            - paCesta.getBodNaCeste(i).getSused(j).getVzdialenost() < rozdielPridBodSusediaOdBodSused) {
                        bod1 = paCesta.getBodNaCeste(i);
                        bod2 = paCesta.getBodNaCeste(i).getSusednyBod(j);
                        rozdielPridBodSusediaOdBodSused = vzdialenostPridBoduOdBoduVCeste
                                + vzdialenostPridavanehoBoduOdSuseda
                                - paCesta.getBodNaCeste(i).getSused(j).getVzdialenost();
                    }

                }
            }
        }
        if (bod1 != null && bod2 != null) {
            if (rozdielPridBodSusediaOdBodSused < 3) {
                vybBod.pridajNovySusednyBod(bod1);
                vybBod.pridajNovySusednyBod(bod2);
                bod1.pridajNovySusednyBod(vybBod);
                bod2.pridajNovySusednyBod(vybBod);
                paCesta.pridajNovyBodNaCestu(vybBod);
                System.out.println("DEBUG: Novy objekt bol pridany na mapu medzi vypocitane body.");
                jMenuItemDistancesActionPerformed(paCesta);
                saveRoadToFile(paCesta);
                return vybBod;
            } else {
                vybBod.pridajNovySusednyBod(bod1);
                vybBod.pridajNovySusednyBod(bod2);
                bod1.pridajNovySusednyBod(vybBod);
                bod2.pridajNovySusednyBod(vybBod);
                paCesta.pridajNovyBodNaCestu(vybBod);
                System.out.println("DEBUG: Novy objekt bol pridany na mapu medzi vypocitane body.");
                jMenuItemDistancesActionPerformed(paCesta);
                saveRoadToFile(paCesta);

                JOptionPane.showMessageDialog(null, "Objekt bol pridany na mapu, ale nemusel byť umiestnený na správne miesto\n"
                        + "pretože zadaná súradnica objektu je príliš vzdialená od cesty.\n"
                        + "Skontrolujte susedov v zoo mape.",
                        "WARNING-objekt nemusel byt pridany na spravne miesto", JOptionPane.WARNING_MESSAGE);
                return vybBod;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nepodarilo sa pridat objekt na mapu.\n Prilis velka vzdialenost od cesty.\n Skontrolujte zadané súradnice",
                    "CHYBA-nepridany objekt na mapu", JOptionPane.WARNING_MESSAGE);
        }
        return null;
    }

    /**
     * pred tym ako zmenime serializovany objekt Road sa data ulozia do suboru v
     * takom tvare aby ich bolo mozne po zmene serializovaneho objektu Road
     * nacitat
     *
     * @param evt
     */
    private void jMenuItemSaveToTextFilePointsBeforeChSerializableClassesActionPerformed(java.awt.event.ActionEvent evt) {
        FileWorker fw = new FileWorker("./BackupChangeSerializable");
        String text = "";
        for (int i = 0; i < aCesta.getPocetBodovNaCeste(); i++) {
            text += aCesta.getBodNaCeste(i).getaLatitude().getaDegree() + ":";
            text += aCesta.getBodNaCeste(i).getaLatitude().getaMinutes() + ":";
            text += aCesta.getBodNaCeste(i).getaLatitude().getaSeconds() + ";;;";
            text += aCesta.getBodNaCeste(i).getaLongitude().getaDegree() + ":";
            text += aCesta.getBodNaCeste(i).getaLongitude().getaMinutes() + ":";
            text += aCesta.getBodNaCeste(i).getaLongitude().getaSeconds() + "|";

            for (int j = 0; j < aCesta.getBodNaCeste(i).getPocetSusedov(); j++) {
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaDegree() + ":";
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaMinutes() + ":";
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLatitude().getaSeconds() + ";;;";
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaDegree() + ":";
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaMinutes() + ":";
                text += aCesta.getBodNaCeste(i).getSusednyBod(j).getaLongitude().getaSeconds() + ";;;";
                text += aCesta.getBodNaCeste(i).getSused(j).getVzdialenost() + "|";
            }
            text += "|";
        }
        try {
            fw.writeDataToFile(text);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Error subor zaloha pred serializaciou sa nepodarilo vytvorit", "Zaloha nevytvorena", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * obnovy data po zmene v kode serializovaneho objektu typu Road
     *
     * @param evt - udalost, ktora metodu vyvolala
     */
    private void jMenuItemRestoreDataSavedBeforeChangeSerializzationCLassRoadyActionPerformed(java.awt.event.ActionEvent evt) {
        FileWorker fw = new FileWorker("./BackupChangeSerializable");
        String loadedText = null;
        aCesta = new Cesta();

        try {
            loadedText = fw.readDataFromFile();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Error data zo zalohy sa nepodarilo obnovit", "Chyba pri obnove dat", JOptionPane.ERROR_MESSAGE);
        }
        if (loadedText != null) {
            String body[] = loadedText.split("\\|\\|");

            String bodSoSusedmi[] = null;
            for (int i = 0; i < body.length; i++) {
                if (!body[i].equals("\n")) {
                    bodSoSusedmi = body[i].split("\\|");
                    String coordinatLat = bodSoSusedmi[0].split(";;;")[0];
                    String coordinatLon = bodSoSusedmi[0].split(";;;")[1];
                    String lat[] = coordinatLat.split(":");
                    String lon[] = coordinatLon.split(":");
                    Coordinate coordLat = new Coordinate(lat[0], lat[1], lat[2]);
                    Coordinate coordLon = new Coordinate(lon[0], lon[1], lon[2]);
                    BodNaCeste novyBod = new BodNaCeste(coordLat, coordLon, BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT);
                    aCesta.pridajNovyBodNaCestu(novyBod);
                }

            }
            for (int i = 0; i < body.length; i++) {
                if (!body[i].equals("\n")) {
                    bodSoSusedmi = body[i].split("\\|");
                    String coordinatLat = bodSoSusedmi[0].split(";;;")[0];
                    String coordinatLon = bodSoSusedmi[0].split(";;;")[1];
                    String lat[] = coordinatLat.split(":");
                    String lon[] = coordinatLon.split(":");
                    Coordinate coordLat = new Coordinate(lat[0], lat[1], lat[2]);
                    Coordinate coordLon = new Coordinate(lon[0], lon[1], lon[2]);
                    BodNaCeste bodKuKtoremuPridavamSusedov = aCesta.getBodNaCeste(coordLat, coordLon);
                    for (int j = 1; j < bodSoSusedmi.length; j++) {
                        coordinatLat = bodSoSusedmi[j].split(";;;")[0];
                        coordinatLon = bodSoSusedmi[j].split(";;;")[1];
                        lat = coordinatLat.split(":");
                        lon = coordinatLon.split(":");
                        coordLat = new Coordinate(lat[0], lat[1], lat[2]);
                        coordLon = new Coordinate(lon[0], lon[1], lon[2]);
                        BodNaCeste sused = aCesta.getBodNaCeste(coordLat, coordLon);
                        bodKuKtoremuPridavamSusedov.pridajNovySusednyBod(sused);
                    }
                }

            }
            jMenuItemDistancesActionPerformed(aCesta);
            updateListOfExistsRoadPoint();
            jPlatnoNaKreslenie.repaint();
        }
    }

    /**
     * skontroluje, ci su koordinta spravne zadane pri vytvarani noveho bodu
     *
     * @return true - koordinaty su validne
     */
    private boolean platneZadaneKoordinaty() {
        try {
            Integer.parseInt(jTextLatDegree.getText());
            Integer.parseInt(jTextLonDegree.getText());
            Double.parseDouble(jTextLatminutes.getText());
            Double.parseDouble(jTextLonMinutes.getText());
            Double.parseDouble(jTextLatSeconds.getText());
            Double.parseDouble(jTextLonSeconds.getText());

            if (jTextLatDegree.getText().equals("") || jTextLatSeconds.getText().equals("") || jTextLatminutes.getText().equals("")) {
                return false;
            }
            if (jTextLonDegree.getText().equals("") || jTextLonMinutes.getText().equals("") || jTextLonSeconds.getText().equals("")) {
                return false;
            }

            double decRepr = Integer.parseInt(jTextLatDegree.getText())
                    + Double.parseDouble(jTextLatminutes.getText()) / 60
                    + Double.parseDouble(jTextLatSeconds.getText()) / 3600;
            if (decRepr < MIN_LAT || decRepr > MAX_LAT) {
                return false;
            }
            decRepr = Integer.parseInt(jTextLonDegree.getText())
                    + Double.parseDouble(jTextLonMinutes.getText()) / 60
                    + Double.parseDouble(jTextLonSeconds.getText()) / 3600;
            if (decRepr < MIN_LON || decRepr > MAX_LON) {
                return false;
            }

            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * aktualizuje vsetky data zobrazovane v ramci tohto tabu
     */
    @Override
    public void updateAllTabData() {
        File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE + InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);

        SerializationWorker<Cesta> worker = new SerializationWorker<>("");
        if (f.exists()) {

            worker.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + InfoSuboryENUM.RELATIVE_PATH_OF_OFFLINE_MAP_FILE);
            worker.setNameOfFile(InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);
            aCesta = worker.readObjetctFromFile();
            System.out.println("DEBUG: Object Cesta was successfully created from file: " + InfoSuboryENUM.NAME_OF_SAVE_FILE_OFFLINE_ROAD_FOR_DEVICE);
        }
        updateListOfExistsRoadPoint();
        jPlatnoNaKreslenie.repaint();
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton jButtonOdstranBodZCesty;
    private javax.swing.JButton jButtonPridajNovyBod;
    private javax.swing.JButton jButtonPridajSuseda;
    private javax.swing.JComboBox jComboBoxVytvoreneBody;
    private javax.swing.JComboBox jComboVyberSusedaKtorehoChcemPridat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;

    private javax.swing.JPanel jPlatnoNaKreslenie;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextLatDegree;
    private javax.swing.JTextField jTextLatSeconds;
    private javax.swing.JTextField jTextLatminutes;
    private javax.swing.JTextField jTextLonDegree;
    private javax.swing.JTextField jTextLonMinutes;
    private javax.swing.JTextField jTextLonSeconds;
    private javax.swing.JList jZoznamSusediaVybranehoBodu;
    private javax.swing.JButton jButtonOddial;

    private javax.swing.JButton jButtonPribliz;

    // End of variables declaration     
    /**
     * Renderer pre list zobrazujuci akrualnycg susedov vybraneho bodu
     */
    private class SelectedListCellRenderer extends DefaultListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (value != null && value instanceof Sused) {
                Sused s = (Sused) value;
                if (s.getSusednyBod().getTypBodu() == BodNaCeste.TypBodu.BODCESTYVYTVORENYPOUZIVATELOM) {
                    c.setBackground(Color.decode("#800080"));
                    c.setForeground(Color.white);
                } else if (s.getSusednyBod().getTypBodu() == BodNaCeste.TypBodu.OBJEKTALEBOZVIERA) {
                    c.setBackground(Color.decode("#40e0d0"));
                    c.setForeground(Color.white);
                } else if (s.getSusednyBod().getTypBodu() == BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT) {
                    c.setBackground(Color.decode("#0e2f44"));
                    c.setForeground(Color.white);

                }
            } else if (value != null) {
                BodNaCeste s = (BodNaCeste) value;

                if (s.getTypBodu() == BodNaCeste.TypBodu.BODCESTYVYTVORENYPOUZIVATELOM) {
                    c.setBackground(Color.decode("#800080"));
                    c.setForeground(Color.white);
                } else if (s.getTypBodu() == BodNaCeste.TypBodu.OBJEKTALEBOZVIERA) {
                    c.setBackground(Color.decode("#40e0d0"));
                    c.setForeground(Color.white);
                } else if (s.getTypBodu() == BodNaCeste.TypBodu.BODCESTYPRENAVIGACIUDEFAULT) {
                    c.setBackground(Color.decode("#0e2f44"));
                    c.setForeground(Color.white);

                }
            }

            return c;
        }

    }
}

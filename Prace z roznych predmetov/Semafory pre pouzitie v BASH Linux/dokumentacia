   -------------------------------------------------------------------------------------------------
   |                                     ZILINSKA UNIVERZITA V ZILINE				   |
   |     				FAKULTA RIADENIA A INFORMATIKY 				   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |			   	SEMESTRALNA PRACA Z PREDMETU: OPERACNE SYSTEMY	        	   |
   |												   |
   |			        	SEMAFORY PRE POUZITIE V BASH-I				   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   | MENO A PRIEZVISKO: DOMINIK SUROVIAK				SKOLSKY ROK: 2014/2015	   |
   | STUDIJNA SKUPINA : 5ZI038									   |
   | CVICENIE         : PONDELOK 15:00								   |
   | CVICIACI         : doc. Ing. Penka Martincová, PhD.					   |
   -------------------------------------------------------------------------------------------------

   -------------------------------------------------------------------------------------------------
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |  UVOD:											   |
   |  												   |
   |  Pri skriptovani v Bash-i sa moze stat ze potrebujeme dva skripty medzi sebou synchronizovat. |
   |  Napriklad potrebujeme zabezpecit pristup dvoch skriptov k suboru, ktory menia a cez ktory ko-|
   |  munikuju.											   |
   |  Aj ked je mozne takuto komunikaciu riesit v Bash-i roznymi sposobmi, tak mojou ulohou tejto  |
   |  semestralnej prace bolo vytvorit synchronizacny prostriedok: semafory pre pouzitie v Bash-i. |
   |  Blizsi popis ohladom tejto prace najdete na nasledujucich stranach.			   |
   | 												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   --------------------------------------------------------------------------------------------------

   -------------------------------------------------------------------------------------------------
   |												   |
   |	1. ZADANIE										   |
   |												   |
   |	Implementacia bude pozostavat z dvoch casti:						   |
   |												   |
   |	1) Implementacia nasledovnych aplikacii (v jazyku C) 					   |
   |	- sem_get [hodnot]: aplikacia, ktora vytvori novy semafor, 				   |
   |	  so  zadanou pociatocnou hodnotou a na vystup  vypise 				   	   |
   |	  jeho unikatny identifikator 								   |
   |	- sem_del [id-semafora]: aplikacia, ktora zmaze dany semafor.				   |
   |	- sem_wait [id-semafora]: aplikacia, ktora sa pri zavolani				   |
   |	  bude spravat  ako operacia wait nad danym semaforom, t.j.				   |
   |	  bude blokovat, kym je  hodnota semafora nulova. 					   |
   |	- sem_post [id-semafora]: aplikacia, ktora sa bude spravat				   |
   |	  ako operacia  post nad danym semaforom, t.j. zvysi hodnotu.				   |
   |												   |
   |	2) Implementacia skriptov v bashi, ktore bude demonstrovat 				   |
   |	   funkcnost  aplikacii z bodu jedna (napr. hra bingo, atd.) 				   |
   |	Priklad pouzitia v Bash-i:   								   |
   |	sem=$(sem_get 2)									   |
   |	...											   |
   |	sem_wait ${sem}										   |
   |	...											   |
   |	sem_post ${sem}										   |
   |	...											   |
   |	sem_del ${sem}										   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   |												   |
   -------------------------------------------------------------------------------------------------

   --------------------------------------------------------------------------------------------------
   |												    |
   |	2. ZOZNAMENIE SA SO SEMAFORMI								    |
   |												    |
   |	Semafory pozname vacsinou ako zariadenie riadiace krizovatky v cestnej doprave, ktore brani |
   |	vzniku kolizie na krizovatke. Semafory su vsak aj synchromizacnymi prostriedkami procesov v |
   |	v operacnych systemoch.									    |
   |												    |	
   |	Predstavme si dva pararelne beziace procesy v operacnom systeme. Kazdemu beziacemu procesu  | 
   |	procesor priraduje na chvilu svoj cas(planovanie procesov). Co ak chcu nase dva pararelne   |
   |	beziace procesy navzajom komunikovat tak, ze si budu napriklad menit hodnotu spolocnej pre- |
   |	mennej. Napriklad proces(1) potrebuje na svoje dalsie pokracovanie udaje od procesu(2).     |
   |    Tu vznika problem ze nevieme ktory proces bude naplanpvany skor, to znamena, ze proces(2)   |
   |	nemusi stihnut nastavit procesu(1), pretoze najskor bude naplanovany proces(1). V tomto pri-|
   |	pade bude proces(1) pracovat s neaktualnymi datami. Kvoli tomu je potrebne tieto procesy    |
   |    synchronizovat. Synchronizovat znamena, ze proces(1) nebude pokracovat, kym mu proces(2)    |
   |    neda "signal", ze uz data nastavil, cim mu povie: "Mozes pokracovat proces(1). Data su aktu-|
   |	alne.											    |
   |	Jednym z takychtom synchronizacnych prostriedkov su prave semafory. Semafor je udajova 	    |
   |    struktura vacsinou obsiahnuta vo vacsine operacnych systemov nad ktorou sa vykonavaju 	    |
   |	operacie zvys hodnotu semafora(signal), cakaj na urcitu hodnotu semafora(wait).	 	    |			    
   |	Dolezite je vediet ze tieto operacie su atomicke, t.z. ze pocas ich vykonavania nemoze      |
   |	dojst k preplanovaniu procesov.								    |
   |	Znova sa vratme k nasim dvom procesom. Pociatocna hodnota semafora je 0. Vykonava sa proces |
   |	(1). Pred kodom, kde potrebuje mat aktualne udaje proces 1 zavola operaciu wait(1), t.j. ca-|
   |	kaj, kym nebude aktualna hodnota semafora=1. Proces sa v tomto pripade uz nadalej nebude    |
   |	planovat az kym sa hodnota semafora nezmeni a prida sa do frontu cakajucich procesov. popri |
   |	procesu(1) bezi proces(2). Proces(2) nastavi data do spolocnej premennej a posle signal().  |
   |	Signal zvysi hodnotu semafora na 1 cim povie procesu(1):"Mozes pokracovat. Data su aktualne"|
   |	Toto by bol v kratkosti popis semaforov. Vela veci vsak najdete na internete alebo v roznych|
   |	knihach.										    |
   |												    |
   --------------------------------------------------------------------------------------------------

   --------------------------------------------------------------------------------------------------
   |												    |
   |	3. POPIS PROGRAMOV SEM_WAIT, SEM_POST, SEM_GET, SEM_GET					    |
   |												    |
   |	- sem_get [hodnota]: aplikacia, ktora vytvori novy semafor,				    |
   |	  so  zadanou pociatocnou hodnotou a na vystup  vypise 					    |
   |	  jeho unikatny identifikator. Vracia id pri uspechu, inak -1.				    |
   |												    |
   |	- sem_del [id-semafora]: aplikacia, ktora zmaze semafor so				    |
   |      zadanym id. Vracia 0 pri uspechu, inak -1.						    |
   |												    |
   |	- sem_wait [id-semafora]: aplikacia, ktora sa pri zavolani				    |
   |	  bude spravat  ako operacia wait nad danym semaforom(podla id),			    |
   |	  t.j. bude blokovat, kym je  hodnota semafora nulova. 					    |
   |      Vracia 5 pri uspechu, inak -1 a vypise, ze doslo k chybe. 			     	    |
   |												    |
   |	- sem_post [id-semafora]: aplikacia, ktora sa bude spravat				    |
   |	  ako operacia  post nad danym semaforom(podla id), t.j. zvysi 				    |
   |	  hodnotu.										    |
   |												    |
   |	Tieto programy mozno pouzivat v Bash-i na synchronizaciu dvoch pararelne beziacich skriptov.|
   |	Priklad:										    |
   |	Je potrebne synchronizovat skript(1) a skript(2). skript(1) zapisuje do suboru a skript(2)  |
   |	to cita a dalej nejako spracuje. Najskor vytvorime semafor pomocou operacie sem_get.	    |
   |	Potom spustime skript(1).skript(1) zavola operaciu sem_wait(id) a v cykle cakame, kym nebude|
   |	navratova hodnota sem_wait=5. Ked bude 5 zapiseme do suboru cislo a pomocou sem_post(id)    |
   |	uvolnime semafor na co uz v tom case caka skript(2). Ked sme poslali sem_post navratova hod-|
   |	nota v skripte(2) v sem_wait bude 5, co signalizuje, ze mozeme citat aktualne data.         |
   |	Po skonceni behu skriptov semafor pomocou sem_del(id) vymazeme.				    |
   |												    |
   |												    |
   |												    |
   --------------------------------------------------------------------------------------------------

   --------------------------------------------------------------------------------------------------
   |												    |
   |	4. PRIPAD POUZITIA V BASH-I (Hra Hadana)						    |
   |												    |
   |	Na to aby som potvrdil funkcnost aplikacii: sem_wait, sem_get, sem_del, sem_post som sa    |
   |	rozhodol vytvorit v Bashi-i jednoduchu hru Hadana, ktora pouziva na synchronizaciu          |
   |	mnou vytvorene aplikacie pre pracu so semaformi v BASH-I.				    |
   |	Hra je zlozena zo 4 skriptov: riadiaciSkriptHra, hra, skriptHadaj, skriptGenerujCislo.      |
   |    riadiaciSkriptHra ma za ulohu iba jednoduche zobrazenie menu, v ktorom moze hrac hru 	    |
   |	ukoncit, zobrazit o nej info a spustit ju. Skripthra ma za ulohu inicializovat(spustit) hru.|
   |	Je ulohou je vytvorit semafor, spustit skripty: skriptHadaj, skriptGenerujCislo, ktore bezia|
   |	pararelne a po ukonceni hry zrusit semafor. Pociatocna hodnota semafora je 1.		    |
   |    											    |
   |	skriptGenerujCislo:									    |
   |	Ma za ulohu vygenerovat cislo, ktore budeme v skriptHadaj hadat. Skript najskor vola apli-  |
   |	kaciu sem_wait(spusti ju na pozadi) a v cykle caka kym sem_wait nevrati 5. Ked sem_wait     |
   |	vrati 5, zacne generovat cislo, ktore ulozi do suboru: cislo a zavola aplikaciu sem_post,   |
   |	ktora umozni skriptuHadaj zacat vykonavat svoju pracu. Aplikacia sem_post zvysi hodnotu     |
   |	semafora na 1, ak vo fronte cakajucich precesov nie je ziadny. Ak vo fronte niekto je,      |
   |    tak sem_post nezmeni hodnotu a uvolni proces cakajuci vo fronte. Pred generovanim cisla     |
   |	sa este program opyta ci je hodnota v subore: beziHra=1. Ak je jedna tak to znamena, ze     |
   |	pouzivatel chce ukoncit hru a skript sa ukonci a nebude dalej bezat.       	 	    |
   |	skriptHadaj:										    |
   |	Ma za ulohu komunikovat z uzivatelom a davat mu smer ktorym ma ist(vacsie / mensie cislo) . |
   |	Hra na zaciaku caka na nastavenie semafora na hodnotu 1 skriptom skriptGenerujCislo.        |
   |	Akonahle mu skript generujCislo umozni pokracovat(tym, ze vygeneruje cisl a zavola sem_post,|
   |	nacita cislo zo suboru a hrac hada nacitane cislo. Ak hrac uhadne cislo, zavola sa sem_post,|
   |	ktory oznami generatoru ze moze zacat generovat a takto to ide dookola az pokial hrac ne-   |
   |	zada "k". Ak zadal hrac "k", hra sa ukonci. 						    |
   |	Na to aby sme videli, ze semafory pracuju spravne, staci v skripte: skriptHadaj vo funkcii  |
   |    hadaj() v cykle while dat prec "#debug" pri "Vygen cislo v hre". Kedze sa pri hadani stale  | 
   |	cita cislo zo suboru, mozme pri vypisoch vidiet ze cislo sa nezmeni, kym nezavolame sem_post|
   --------------------------------------------------------------------------------------------------

   --------------------------------------------------------------------------------------------------
   |												    |
   |	5. ZAVER										    |
   |												    |
   |												    |
   |	Ako vidiet z prikladu hry, mnou vytvorene aplikacie sa daju pouzit na synchronizaciu skri-  |
   |	ptov v Bash-i. Aby ich pouzitie vsak malo zmysel, tak tieto skripty musia bezat pararelne.  |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   |												    |
   --------------------------------------------------------------------------------------------------









   
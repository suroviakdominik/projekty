<?php

session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function zaznamenajHlasovanie($stlpec) {
        $logID = $_SESSION["login"];
        $sql = "UPDATE pouzivatelia SET $stlpec=true WHERE id=$logID";

        if ($this->conn->query($sql) === TRUE) {
            //echo "Record updated successfully";
        } else {
            //echo "Error updating record: " . $this->conn->error;
        }
    }

    function jeMozneHlasovat($stlpec) {
        $logID = $_SESSION["login"];
        $sql = "SELECT $stlpec FROM pouzivatelia WHERE $stlpec=false AND id=$logID";
        $result = $this->conn->query($sql);

        if ($result->num_rows > 0) {
           return true;
        } else {
            return false;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

$rating = new ratings(htmlspecialchars($_POST['widget_id']));

# either return ratings, or process a vote

isset($_POST['fetch']) ? $rating->get_ratings() : $rating->vote();

class ratings {

    private $data_file = './ratings.data.txt';
    private $widget_id;
    private $data = array();

    function __construct($wid) {

        $this->widget_id = $wid;

        $all = file_get_contents($this->data_file);

        if ($all) {
            $this->data = unserialize($all);
        }
    }

    public function get_ratings() {
        if ($this->data[$this->widget_id]) {
            echo json_encode($this->data[$this->widget_id]);
        } else {
            $data['widget_id'] = $this->widget_id;
            $data['number_votes'] = 0;
            $data['total_points'] = 0;
            $data['dec_avg'] = 0;
            $data['whole_avg'] = 0;
            echo json_encode($data);
        }
    }

    public function vote() {
        # Get the value of the vote
        $db = new DBPraca();
        
        htmlspecialchars($_POST["widget_id"]) == "r1" ? $coHlasujem = 1 : $coHlasujem = 2;
        $jeMozneHlasovat=false;
        if($coHlasujem==1)
        {
            $jeMozneHlasovat=$db->jeMozneHlasovat("ZOoRateHlas");
        }
        else if($coHlasujem==2)
        {
            $jeMozneHlasovat=$db->jeMozneHlasovat("strankaRate");
        }
        
        if (isset($_SESSION["login"]) && $jeMozneHlasovat==true) {

            
            if ($coHlasujem == 1) {
                $db->zaznamenajHlasovanie("ZOoRateHlas");
            } else if ($coHlasujem == 2) {
                $db->zaznamenajHlasovanie("strankaRate");
            }
            $db->ukonciPristup();
            preg_match('/star_([1-5]{1})/', htmlspecialchars($_POST['clicked_on']), $match);
            $vote = $match[1];
            $ID = $this->widget_id;
# Update the record if it exists
            if ($this->data[$ID]) {
                $this->data[$ID]['number_votes'] += 1;
                $this->data[$ID]['total_points'] += $vote;
            }
# Create a new one if it does not
            else {
                $this->data[$ID]['number_votes'] = 1;
                $this->data[$ID]['total_points'] = $vote;
            }
            $this->data[$ID]['dec_avg'] = round($this->data[$ID]['total_points'] / $this->data[$ID]['number_votes'], 1);
            $this->data[$ID]['whole_avg'] = round($this->data[$ID]['dec_avg']);

            file_put_contents($this->data_file, serialize($this->data));
            $this->get_ratings();
        }
    }

}

?>

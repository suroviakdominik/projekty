package com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.suroviak.dominik.zooAplication.Adapters.ExpandableListAdapterEvents;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dominik on 9.2.2015.
 */
public class EventsFragment extends Fragment {

    ExpandableListAdapterEvents listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private String[] names_of_categories = null;
    private String aNameOfRootDownloadDir;
    private Info_About_Dirs_File aInfoFileEvents;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kal_udalosti_main_menu, container, false);


        FileWorker aFileWorker = new FileWorker();
        aNameOfRootDownloadDir = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.EVENTS.getaRelativePathOfDirecotryWhereIAM();
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(aNameOfRootDownloadDir);
        sw.setNameOfFile(Info_Dirs_File_ENUM.EVENTS.getaNameOfInfoFile());
        try {
            aInfoFileEvents = (Info_About_Dirs_File) sw.readObjetctFromFile();
            prepareListData();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        // get the listview
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        // preparing list data


        listAdapter = new ExpandableListAdapterEvents(this.getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        return rootView;
    }


    /*
     * Preparing the list data
     */
    private void prepareListData() throws IOException {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        int i = 0;
        Date nowDate = new Date();
        for (String prech : aInfoFileEvents.getaItemsInfo().keySet()) {
            {

                List<String> eventsAdapterData = new ArrayList<String>();
                FileWorker fw = new FileWorker(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                        + aInfoFileEvents.getaItemsInfo().get(prech).getRelativePathOfFilesOfurrentItemInDevice() + "popis.txt");
                String popisData = fw.readDataFromFile();
                Date eventsDate = new Date(popisData.split("\n")[0]);
                if (eventsDate.compareTo(nowDate) > 0) {
                    listDataHeader.add(aInfoFileEvents.getaItemsInfo().get(prech).getOriginalName());
                    eventsAdapterData.add(popisData);
                    listDataChild.put(listDataHeader.get(i), eventsAdapterData);
                    i++;
                }
            }

        }
    }
}

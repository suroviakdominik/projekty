/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.rbstrom;



import java.util.Objects;


/**
 *
 * @author Dominik
 * @param <K>
 * @param <V>
 */
public class RBStrom<K extends Comparable<K>, V>{

    private Vrchol aKoren;
    private int aPocetPrvkov;

    /**
     * Prida novy prvok do RB Stromu
     * @param paKluc-kluc pridavaneho prvku
     * @param paHodnota-hodnota
     * @return true-ak sa podarilo prvok pridat
     *         false- prvok sa nepodarilo pridat, lebo strom uz
     *                  obsahuje prvok s takymto klucom
     */
    public boolean pridaj(K paKluc, V paHodnota) {
        if (aKoren == null) {
            aKoren = new Vrchol(paKluc, paHodnota, false);
            aPocetPrvkov++;
            return true;
        }
        //vyttvorime si vrchol, ktory pridame
        Vrchol spracovavanyVrchol = new Vrchol(paKluc, paHodnota, true);
        //najdeme vrchol, ktory sa stane ocom pridavaneho
        Vrchol paOtecPridavaneho = BVSAlgoritmy.najdiVhodneUmiestnenieAlgoritmomBVS(spracovavanyVrchol, aKoren, true);
        //ak sa otec pridavaneho nenasiel, znamena to, ze vrchol zo zadanym klucom uz v strome je a
        //nemozno ho pridat
        if (paOtecPridavaneho != null) {
            aPocetPrvkov++;
            //vrcholu kotry sa stane ocom pridavaneho nastavime ako syna pridavany vrchol
            Pridavac.pridajSynaOcovi(spracovavanyVrchol, paOtecPridavaneho);
            while (spracovavanyVrchol != null) {
                //zistime, ci nejdu dva cervene za sebou
                if (Pridavac.nastalaIregularita(spracovavanyVrchol)) {
                    if (Pridavac.jeBratOcaVkladanehoCierny(spracovavanyVrchol.getaOtec())) {
                        restruktualizacia(spracovavanyVrchol);
                        spracovavanyVrchol = null;
                        //System.out.println("Restruktualizacia\n\n");
                    } else {
                        //System.out.println("Prefarbenie");
                        spracovavanyVrchol = prefarbenie(spracovavanyVrchol);
                    }
                } else {
                    return true;
                }
            }
        } else {
            return false;//lebo sa uz prvok v strome nachadza
        }
        return true;
    }

    /**
     * Odstrani prvok zo zadanym klucom
     * @param kluc
     * @return null- ak taky prevok v strome nie je
     *         odstraneny prvok- inak
     */
    public V odstran(K kluc) {
        //System.out.println("\n\n***********************ODSTRANOVANIE*********************************");
        //System.out.println("Odstranujem prvok s klucom: " + kluc);
        //Vytvorime si alias Vrchol, ktory ma kluc zadany v parametri - pre vyhladavenie
        Vrchol v = new Vrchol(kluc,null, true);
        //najdeme vrchol, ktory chceme odstranit
        Vrchol odstrVrchol = BVSAlgoritmy.najdiVhodneUmiestnenieAlgoritmomBVS(v, aKoren, false);
        //ret je odstraneny vrchol
        Vrchol ret;
        //ak sa Vrchol zo zadanzm klucom v strome nenachadza - odstrVrchol=null -> return null
        if (odstrVrchol != null) {
            //odstranime Vrchol ako v binarnom vyhladavacom strome, ak je odstr Koren-vrat odstr Vrchol
            ret = BVSAlgoritmy.oddstranAkoVBVS(odstrVrchol);
            if (ret.equals(aKoren)) {
                aKoren = null;
                aPocetPrvkov--;
                //System.out.println("*******************END ODSTRANOVANIE USPESNE**************************\n\n");
                return odstrVrchol.aHodnota;
            }
            /**
             * ak vznikol double black, vrati obnovFarby hodnotu true
             */
            Vrchol lpVrcholNaSpracovanie = null;
            //zistime ci vznikol double black Vrchol
            if ((lpVrcholNaSpracovanie = Odstranovac.zistiCiVznikolDoubleBlack(ret)) != null) {
                //System.out.println("DDDDDDDDDDDDDDDDDDDDOUBLE BLACK");
                //ak vznikol double black Vrchol - >upravujeme RB Strom
                while (lpVrcholNaSpracovanie != null && !lpVrcholNaSpracovanie.equals(aKoren)) {
                    lpVrcholNaSpracovanie = urciAkyPripadNastalAVyriesHo(lpVrcholNaSpracovanie);
                }
            }

            aPocetPrvkov--;
            //System.out.println("*******************END ODSTRANOVANIE USPESNE**************************\n\n");
            return ret.aHodnota;
        }
        //System.out.println("*******************ODSTRANOVANIE NEUSPESNE**************************\n\n");
        return null;
    }

    /**
     * Vrati prvok stromu, ktory ma priradeny kluc paKluc 
     * @param paKluc
     * @return null-ak taky prvok neexistuje
     *         najdeny prvok-inak
     */
    public V dajHodnotu(K paKluc){
        Vrchol v=new Vrchol(paKluc,null,true);
        Vrchol najdVrchol=BVSAlgoritmy.najdiVhodneUmiestnenieAlgoritmomBVS(v,aKoren, false);
        if(najdVrchol!=null){
            return najdVrchol.getData();
        }
        return null;
    }
    
    /**
     * 
     * @param paKluc
     * @return true- strom obsahuje prvok zo zadanym klucom
     *         false- takyto kluc sa v strome nenachadza
     */
    public boolean obsahujeKluc(K paKluc){
        Vrchol v=new Vrchol(paKluc,null,true);
        Vrchol najdVrchol=BVSAlgoritmy.najdiVhodneUmiestnenieAlgoritmomBVS(v,aKoren, false);
        if(najdVrchol!=null){
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @return vyska stromu
     */
     public int vyskaStromu() {
        return vypocetVyskyStromu(aKoren);
    }
    private int vypocetVyskyStromu(Vrchol paVrchol) {
        if (paVrchol == null) return -1;
        return 1 + Math.max(vypocetVyskyStromu(paVrchol.getaLavySyn()), vypocetVyskyStromu(paVrchol.getaPravySyn()));
    }
   
    /**
     * 
     * @return true - ak je pocet ciernych do listov rovnaky
     */
    public boolean jePocetCiernychDoListovRovnaky() {
        int black = 0;     
        Vrchol lpVrcholKoren = aKoren;
        while (lpVrcholKoren != null) {
            if (!lpVrcholKoren.jeCerveny()) {
                black++;
            }
            lpVrcholKoren = lpVrcholKoren.getaLavySyn();
        }
        return jePocetCiernychRovnaky(aKoren, black);
    }

   
    private boolean jePocetCiernychRovnaky(Vrchol paPrechVrchol, int ocakavanyPocetCiernych) {
        if (paPrechVrchol == null) {
            return ocakavanyPocetCiernych == 0;
        }
        if (!paPrechVrchol.jeCerveny()) {
            ocakavanyPocetCiernych--;
        }
        return jePocetCiernychRovnaky(paPrechVrchol.getaLavySyn(), ocakavanyPocetCiernych) && jePocetCiernychRovnaky(paPrechVrchol.getaPravySyn(), ocakavanyPocetCiernych);
    }

    /**
     * Vrati vrchol, ktory je korenom stromu
     * @return 
     */
    public Vrchol getKoren() {
        return aKoren;
    }

    
    /**
     * 
     * @return pocet prvkov nachadzajucich sa aktualne v strome
     */
    public int getaPocetPrvkov() {
        return aPocetPrvkov;
    }

    /**
     * vymaze vsetky prvky stromu
     */
    public void vymazVsetkyPrvkyStromu() {
        aKoren = null;
        aPocetPrvkov = 0;
    }

    
    private void restruktualizacia(Vrchol paVrcholRestruktualizacie) {
        boolean rotovaloSaOkoloKorena = false;
        boolean prebehliDveRotacie = true;
        if (BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie)
                && !BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie.getaOtec())) {

            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVlavo(paVrcholRestruktualizacie);
            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVpravo(paVrcholRestruktualizacie);

        } else if (!BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie)
                && BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie.getaOtec())) {

            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVpravo(paVrcholRestruktualizacie);
            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVlavo(paVrcholRestruktualizacie);

        } else if (BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie)
                && BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie.getaOtec())) {

            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVlavo(paVrcholRestruktualizacie.getaOtec());
            prebehliDveRotacie = false;
        } else if (!BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie)
                && !BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholRestruktualizacie.getaOtec())) {

            rotovaloSaOkoloKorena = BVSAlgoritmy.rotujVpravo(paVrcholRestruktualizacie.getaOtec());
            prebehliDveRotacie = false;
        } else {
            throw new IllegalStateException("Pri restruktualizacnii nenastala ani nejdna z moznosti.");
        }
        if (prebehliDveRotacie) {
            paVrcholRestruktualizacie.setaJeCervenyVrchol(false);
            paVrcholRestruktualizacie.getaLavySyn().setaJeCervenyVrchol(true);
            paVrcholRestruktualizacie.getaPravySyn().setaJeCervenyVrchol(true);
        } else {
            paVrcholRestruktualizacie.getaOtec().setaJeCervenyVrchol(false);
            paVrcholRestruktualizacie.getaOtec().getaLavySyn().setaJeCervenyVrchol(true);
            paVrcholRestruktualizacie.getaOtec().getaPravySyn().setaJeCervenyVrchol(true);
        }

        if (rotovaloSaOkoloKorena && prebehliDveRotacie) {
            aKoren = paVrcholRestruktualizacie;
        } else if (rotovaloSaOkoloKorena && !prebehliDveRotacie) {
            aKoren = paVrcholRestruktualizacie.getaOtec();
        }
    }

    /**
     *
     * @param paVrcholPrefarbenia
     * @return
     */
    private Vrchol prefarbenie(Vrchol paVrcholPrefarbenia) {
        if (paVrcholPrefarbenia == null || paVrcholPrefarbenia.getaOtec() == null || paVrcholPrefarbenia.getaOtec().getaOtec() == null) {
            throw new IllegalArgumentException("Pri prefarbeni musia existovat aspon dvaja poromkovia skumaneho vrcholu");
        }
        Vrchol dedo = paVrcholPrefarbenia.getaOtec().getaOtec();
        Vrchol pradedo = dedo.getaOtec();
        if (pradedo != null) {//ak nie je koren, zafarbi deda na cerveno
            dedo.setaJeCervenyVrchol(true);
        } else {
            dedo.setaJeCervenyVrchol(false);
        }
        dedo.getaPravySyn().setaJeCervenyVrchol(false);
        dedo.getaLavySyn().setaJeCervenyVrchol(false);
        if (pradedo != null && dedo.jeCerveny() && pradedo.jeCerveny()) {
            return dedo;
        }
        return null;
    }

    /**
     * https://classes.soe.ucsc.edu/cmps201/Fall07/slides/RBsli.pdf
     *
     * @param <K>
     * @param <V>
     * @param paDoubleBlackVrchol
     * @return
     */
    private Vrchol urciAkyPripadNastalAVyriesHo(Vrchol paDoubleBlackVrchol) {
        Vrchol bratDoubleVrchola = Odstranovac.getBratVrchola(paDoubleBlackVrchol);
        Vrchol pSynBrata = bratDoubleVrchola.getaPravySyn();
        Vrchol lSynBrata = bratDoubleVrchola.getaLavySyn();
        Vrchol otecDouble = paDoubleBlackVrchol.getaOtec();
        if (!BVSAlgoritmy.jeVrcholPravymSynomOca(paDoubleBlackVrchol)) {
            //System.out.println("------ODSTRAN UPRAVA DOUBLE BLACK:Odstraneny bol lavym synom oca-----------------");
            if (!otecDouble.jeCerveny() && bratDoubleVrchola.jeCerveny()) {//otec je cierny a brat je cerveny
                //System.out.println("Otec je cierny a brat je cerveny");
                otecDouble.setaJeCervenyVrchol(true);
                bratDoubleVrchola.setaJeCervenyVrchol(false);
                if (bratDoubleVrchola.getaOtec().getaOtec() == null) {
                    aKoren = bratDoubleVrchola;
                }
                BVSAlgoritmy.rotujVlavo(bratDoubleVrchola);
                return paDoubleBlackVrchol;
            }
            if (!bratDoubleVrchola.jeCerveny()) {//brat aj jeho synovia su cierny
                if ((pSynBrata == null || !pSynBrata.jeCerveny()) && (lSynBrata == null || !lSynBrata.jeCerveny())) {
                    //System.out.println("Brat a jeho synovia su cierny");
                    boolean povodnaFarbaOcaJeCervena = otecDouble.jeCerveny();
                    bratDoubleVrchola.setaJeCervenyVrchol(true);
                    otecDouble.setaJeCervenyVrchol(false);
                    if (!povodnaFarbaOcaJeCervena) {
                        return otecDouble;
                    }
                    return null;
                }
            }
            if (!bratDoubleVrchola.jeCerveny() && (pSynBrata == null || (pSynBrata != null && !pSynBrata.jeCerveny())) && lSynBrata != null && lSynBrata.jeCerveny()) {//brat a pSYn su cierny, lSyn cerveny
                //System.out.println("Brat a pravy syn brata su cierny, lavy syn brata je cerveny");
                bratDoubleVrchola.setaJeCervenyVrchol(true);
                lSynBrata.setaJeCervenyVrchol(false);
                BVSAlgoritmy.rotujVpravo(lSynBrata);
                //po rotacii sa zmeni brat double vrchola-musime ho zmenit tu lebo dalej riesime pripad 4
                bratDoubleVrchola = Odstranovac.getBratVrchola(paDoubleBlackVrchol);
                pSynBrata = bratDoubleVrchola.getaPravySyn();
                lSynBrata = bratDoubleVrchola.getaLavySyn();

            }
            if (!bratDoubleVrchola.jeCerveny() && pSynBrata != null && pSynBrata.jeCerveny()) {//brat je cierny a pravy syn cerveny
                //System.out.println("Brat je cierny a pravy syn brata je cerveny");
                pSynBrata.setaJeCervenyVrchol(false);
                bratDoubleVrchola.setaJeCervenyVrchol(bratDoubleVrchola.getaOtec().jeCerveny());
                bratDoubleVrchola.getaOtec().setaJeCervenyVrchol(false);
                if (bratDoubleVrchola.getaOtec().getaOtec() == null) {
                    aKoren = bratDoubleVrchola;
                }
                BVSAlgoritmy.rotujVlavo(bratDoubleVrchola);
                return null;
            }

        } else {//mazany prvok je pravym synom svojho oca
            //System.out.println("------ODSTRAN UPRAVA DOUBLE BLACK:Odstraneny bol pravym synom oca-----------------");
            if (!otecDouble.jeCerveny() && bratDoubleVrchola.jeCerveny()) {//otec je cierny a brat je cerveny
                //System.out.println("Otec je cierny a brat je cerveny");
                otecDouble.setaJeCervenyVrchol(true);
                bratDoubleVrchola.setaJeCervenyVrchol(false);
                if (bratDoubleVrchola.getaOtec().getaOtec() == null) {
                    aKoren = bratDoubleVrchola;
                }
                BVSAlgoritmy.rotujVpravo(bratDoubleVrchola);
                return paDoubleBlackVrchol;
            }
            if (!bratDoubleVrchola.jeCerveny()) {//brat aj jeho synovia su cierny
                if ((pSynBrata == null || !pSynBrata.jeCerveny()) && (lSynBrata == null || !lSynBrata.jeCerveny())) {
                    //System.out.println("Brat a jeho synovia su cierny");
                    boolean povodnaFarbaOcaJeCervena = otecDouble.jeCerveny();
                    bratDoubleVrchola.setaJeCervenyVrchol(true);
                    otecDouble.setaJeCervenyVrchol(false);
                    if (!povodnaFarbaOcaJeCervena) {
                        return otecDouble;
                    }
                    return null;
                }
            }
            if (!bratDoubleVrchola.jeCerveny() && (lSynBrata == null || (lSynBrata != null && !lSynBrata.jeCerveny())) && pSynBrata != null && pSynBrata.jeCerveny()) {//brat a lSYn su cierny, pSyn cerveny
                //System.out.println("Brat a lavy syn brata su cierny, pravy syn brata je cerveny");
                bratDoubleVrchola.setaJeCervenyVrchol(true);
                pSynBrata.setaJeCervenyVrchol(false);
                BVSAlgoritmy.rotujVlavo(pSynBrata);
                //po rotacii sa zmeni brat double vrchola-musime ho zmenit tu lebo dalej riesime pripad 4
                bratDoubleVrchola = Odstranovac.getBratVrchola(paDoubleBlackVrchol);
                pSynBrata = bratDoubleVrchola.getaPravySyn();
                lSynBrata = bratDoubleVrchola.getaLavySyn();
            }
            if (!bratDoubleVrchola.jeCerveny() && lSynBrata != null && lSynBrata.jeCerveny()) {//brat je cierny a lavy syn cerveny
                //System.out.println("Brat je cierny a lavy syn brata je cerveny");
                lSynBrata.setaJeCervenyVrchol(false);
                bratDoubleVrchola.setaJeCervenyVrchol(bratDoubleVrchola.getaOtec().jeCerveny());
                bratDoubleVrchola.getaOtec().setaJeCervenyVrchol(false);
                if (bratDoubleVrchola.getaOtec().getaOtec() == null) {
                    aKoren = bratDoubleVrchola;
                }
                BVSAlgoritmy.rotujVpravo(bratDoubleVrchola);
            }

        }
        return null;
    }

    /**
     * 
     * @param aKoren 
     */
    private void setaKoren(Vrchol aKoren) {
        this.aKoren = aKoren;
    }

    public class Vrchol implements Comparable<Vrchol> {

        private K aKluc;
        private V aHodnota;
        private Vrchol aLavySyn;
        private Vrchol aPravySyn;
        private Vrchol aOtec;
        private boolean aJeCervenyVrchol;
        private Boolean aPredOdstranenimBolPravymSynom;

        public Vrchol(K aKluc, V aHodnota, boolean aJeCervenyVrchol) {
            this.aKluc = aKluc;
            this.aHodnota = aHodnota;
            this.aJeCervenyVrchol = aJeCervenyVrchol;
        }

        public Vrchol(Vrchol paVrchol) {
            aKluc = paVrchol.aKluc;
            aHodnota = paVrchol.aHodnota;
            aLavySyn = paVrchol.aLavySyn;
            aPravySyn = paVrchol.aPravySyn;
            aOtec = paVrchol.aOtec;
            aJeCervenyVrchol = paVrchol.jeCerveny();
        }

        public Boolean getaPredOdstranenimBolPravymSynom() {
            return aPredOdstranenimBolPravymSynom;
        }

        public void setaPredOdstranenimBolPravymSynom(Boolean aPredOdstranenimBolPravymSynom) {
            this.aPredOdstranenimBolPravymSynom = aPredOdstranenimBolPravymSynom;
        }

        public Vrchol getaLavySyn() {
            return aLavySyn;
        }

        public void setaLavySyn(Vrchol aLavySyn) {
            this.aLavySyn = aLavySyn;
        }

        public Vrchol getaPravySyn() {
            return aPravySyn;
        }

        public void setaPravySyn(Vrchol aPravySyn) {
            this.aPravySyn = aPravySyn;
        }

        public Vrchol getaOtec() {
            return aOtec;
        }

        public K getaKluc() {
            return aKluc;
        }

        public void setaKluc(K aKluc) {
            this.aKluc = aKluc;
        }

        public void setaOtec(Vrchol aOtec) {
            this.aOtec = aOtec;
        }

        public boolean jeCerveny() {
            return aJeCervenyVrchol;
        }

        public void setaJeCervenyVrchol(boolean aJeCervenyVrchol) {
            this.aJeCervenyVrchol = aJeCervenyVrchol;
        }

        public V getData() {
            return aHodnota;
        }

        public void setData(V aHodnota) {
            this.aHodnota = aHodnota;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Vrchol other = (Vrchol) obj;
            if (!Objects.equals(this.aKluc, other.aKluc)) {
                return false;
            }
            return true;
        }

        

        @Override
        public int hashCode() {
            return Objects.hashCode(aKluc) ^ Objects.hashCode(aHodnota);
        }

        @Override
        public int compareTo(Vrchol o) {
            return aKluc.compareTo(o.aKluc);
        }
    }
    
    
}

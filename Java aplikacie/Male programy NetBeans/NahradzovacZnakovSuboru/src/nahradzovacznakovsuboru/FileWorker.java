/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nahradzovacznakovsuboru;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class FileWorker {

    public static String readDataFromFile(File f, String paOddelovacRiadkov) throws IOException {
        String ret = "";
        Reader r = null;
        try {

            r = new InputStreamReader(new FileInputStream(f), "WINDOWS-1250");
            BufferedReader bFR = new BufferedReader(r);
            String loadedLine = null;
            while ((loadedLine = bFR.readLine()) != null) {
                ret += loadedLine + paOddelovacRiadkov;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw ex;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw ex;
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw ex;
        } finally {
            try {
                if (r != null) {
                    r.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                throw ex;
            }
        }
        return ret;
    }

    public static void writeDataToFile(File f, String paData, boolean paAppend) throws IOException {
        Writer w=null;
        try {
            w = new OutputStreamWriter(new FileOutputStream(f, paAppend), "WINDOWS-1250");
            w.write(paData);
        } finally {
            if (w != null) {
                w.close();
            }
        }
    }
}

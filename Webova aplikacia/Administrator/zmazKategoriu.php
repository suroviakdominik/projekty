<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function zmazKategoriu() {
        $a = htmlspecialchars($_POST["idMazKat"]);

        $sql1 = sprintf("DELETE from zviera WHERE idKategoria=%d", $this->conn->real_escape_string($a));
        if ($this->conn->query($sql1) === TRUE) {
            echo "Zvierata z kategorie boli vymazane.";
            $sql = sprintf("DELETE FROM kategoria WHERE id=%d", $this->conn->real_escape_string($a));

            if ($this->conn->query($sql) === TRUE) {
                echo "Kategoria bola odstranena.";
            } else {
                echo "Chyba pri mazani kategorie: " . $this->conn->error;
            }
        } else {
            echo "Chyba pri mazani zvierat kategorie: " . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

if (isset($_SESSION["login"]) && $_SESSION["login"] == 0) {
    $db = new DBPraca();
    $db->zmazKategoriu();
    $db->ukonciPristup();
} else {
    echo "Kategoriu moze mazat iba administrator po prihlaseni.";
}
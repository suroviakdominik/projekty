/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Dominik
 */
 public class Sused implements Serializable
    {
        private static final long serialVersionUID = 6L;
        private double aVzdialenost;
        private BodNaCeste aSusednyBod;

        public Sused(BodNaCeste aSusednyBod) {
            
            this.aSusednyBod = aSusednyBod;
        }
        
        public void setVzdialenost(double paVzdialenost)
        {
            aVzdialenost=paVzdialenost;
        }

        public double getVzdialenost() {
            return aVzdialenost;
        }

        public BodNaCeste getSusednyBod() {
            return aSusednyBod;
        }

        

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 23 * hash + (int) (Double.doubleToLongBits(this.aVzdialenost) ^ (Double.doubleToLongBits(this.aVzdialenost) >>> 32));
            hash = 23 * hash + Objects.hashCode(this.aSusednyBod);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Sused other = (Sused) obj;
            
            return Objects.equals(this.aSusednyBod, other.aSusednyBod);
        }
        
        

        @Override
        public String toString() {
            return aSusednyBod+", Vzdialenost: "+aVzdialenost+" m";
        }
        
        
    }

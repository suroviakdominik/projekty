/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kamenpapiernoznice;

/**
 *
 * @author Dominik
 */
public enum Stavka {

    KAMEN("Kameň",0),
    PAPIER("Papier",1),
    NOZNICE("Nožnice",2);
    private String aNazov;
    private int aOznacenie;
    private Stavka(String aNazov, int paOznacenie) {
        this.aNazov = aNazov;
        aOznacenie=paOznacenie;
    }

    public String getaNazov() {
        return aNazov;
    }

    public int getaOznacenie() {
        return aOznacenie;
    }

    public static StavHry vyhralPocitac(Stavka paHrac,Stavka paPocitac)
    {
        if(paHrac==paPocitac)
        {
            return StavHry.REMIZA;
        }
        if((paHrac==Stavka.KAMEN&&paPocitac==Stavka.PAPIER)
                ||(paHrac==NOZNICE&&paPocitac==Stavka.KAMEN)
                ||(paHrac==PAPIER&&paPocitac==Stavka.NOZNICE))
        {
         return StavHry.VYHRA;   
        }
        
        return StavHry.PREHRA;
    }
    
    
}

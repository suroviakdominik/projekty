#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
int idSemafora = atoi(argv[1]);
struct sembuf wait[1] = {0,-1,0};//sem_num=0,sem_op=-1,sem_flg=0, pole obsahujuce prvky typu: struktura sembuf. {0,1,0} naplni strukturu nachadzajucu sa na indexe 0,
if(semop(idSemafora,&wait[0],1)<0)
{
printf("Error: Chyba pri wait semafora. Operaciu wait sa nepodarilo vykonat");
return -1;
}
printf("Nova hodnota semafora po vykonani wait:  %d  \n",semctl(idSemafora,0,GETVAL));

}
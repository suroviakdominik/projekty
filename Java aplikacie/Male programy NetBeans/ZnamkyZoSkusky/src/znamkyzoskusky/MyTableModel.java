/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyzoskusky;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import znamkyzoskusky.RiadokTabulky.TypZnamky;

/**
 *
 * @author Dominik
 */
public class MyTableModel extends AbstractTableModel{
    private List<String> aZnamky;
    private List<RiadokTabulky> aRiadky;
    public MyTableModel() {
        File f =new File("./znamky.txt");
        String data=FileWorker.readDataFromFile(f,"\n");
        String[] menoAZnamka=data.split(":|\n");
        aZnamky=new ArrayList<>();
        for(int i=1;i<menoAZnamka.length;i+=2)
        {
            aZnamky.add(menoAZnamka[i]);
        }
        aRiadky=new ArrayList<>();
        for(TypZnamky prech:TypZnamky.values())
        {
            RiadokTabulky r= new RiadokTabulky(prech.getaZnamka(),
                    new ZnamkyOperacie(aZnamky).getPocetZnamokTypu(prech));
            aRiadky.add(r);
        }
    }

    
    
    @Override
    public int getRowCount() {
        return TypZnamky.values().length;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return RiadokTabulky.NazvyStlpcov.values()[columnIndex].getHodnota(aRiadky.get(rowIndex));
    }

    @Override
    public String getColumnName(int column) {
        return RiadokTabulky.NazvyStlpcov.values()[column].getaNazovStlpca();
    }
    
   
}

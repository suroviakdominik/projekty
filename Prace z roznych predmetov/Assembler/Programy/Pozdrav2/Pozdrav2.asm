.Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA
Retaz DB 'No nazdar!',0;deklaracia premennej, ked najde nulu ukonci program
.CODE
Zac: mov ax,@data 
     mov ds,ax;uloz bazovu adresu aby sme vedeli kde je retaz, nemozno ju priradit rovno do ds ak ds,@data
     mov di,0; prvy znak ma index 0
VypisZnak: mov dl,[retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
           cmp dl,0;porovnaj hodnotu v dl s 0
           jz ResetDI;ak bola 0, skok na navestie koniec
           mov ah,2
           int 21h; vypis znak, ktoreho kod je v dl
           inc DL;
           int 21h 
           mov DL,0AH; uloz do registra posun dole
           mov AH,2; do registra AH uloz cislo sluzby
           int 21h
           mov DL,0DH; uloz do registra posun vlavo
           mov AH,2
           int 21h
           inc di;zvys index o 1/prejdi na dalsi znak
           jmp VypisZnak
           
ResetDI:        mov di,0
                mov ax,0
                

PocitadloA: 
            
            
            mov dl,[retaz+di]
            
            cmp dl,0
            jz Koniec
            
            cmp dl,'a';iba porovna ale pomocou jz musime urcit co sa vykona ak plati
            jz Zvys
            

Pokracuj:            inc di;
                     jmp PocitadloA     
            
Zvys:            
            inc ax
            jmp Pokracuj            

Koniec: mov Dl,AL;
        add DL,'0';s DL sme spravisli ascii kod lebo sluzba 2 chce ascii
        mov AH,2
        int 21h
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zac
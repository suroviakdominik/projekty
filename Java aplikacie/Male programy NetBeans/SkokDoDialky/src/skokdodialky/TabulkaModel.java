/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skokdodialky;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Dominik
 */
public class TabulkaModel extends AbstractTableModel{
    private List<RiadokTabulky> aRiadkyTabulky;

    public TabulkaModel() {
        aRiadkyTabulky=new ArrayList<>();
    }

    @Override
    public String getColumnName(int column) {
        return RiadokTabulky.NazvyStlpcov.values()[column].getaStlpecNazov();
    }
    
    
    
    @Override
    public int getRowCount() {
        return aRiadkyTabulky.size();
    }

    @Override
    public int getColumnCount() {
        return RiadokTabulky.NazvyStlpcov.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return RiadokTabulky.NazvyStlpcov.values()[columnIndex].dajHodnotu(aRiadkyTabulky.get(rowIndex));
    }
    
    public void pridajNovyRiadok(RiadokTabulky paRiadok)
    {
        aRiadkyTabulky.add(paRiadok);
        fireTableDataChanged();
    }
    
    public String getPravychTroch()
    {
        String ret="";
        List<RiadokTabulky> paList=new ArrayList<>(aRiadkyTabulky);
        
        Collections.sort(paList, RiadokTabulky.podlaSkoku);
        for(int i=0;i<paList.size()&&i<3;i++)
        {
            ret+=paList.get(i).getaMenoSportovca()+" : "
                    + paList.get(i).getaDlzkaSkoku()+" m\n";
        }
        return ret;
    }
}

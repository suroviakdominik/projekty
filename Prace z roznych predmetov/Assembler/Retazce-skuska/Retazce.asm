;Moje zadanie zo v�era:

;1.) Na��taj re�azec R a ��slo N. Naprogamova� obdobu funkcie ROL - o po�et miest zadan�ch ��slom N zrotova� jednotliv� bity (znaky) re�azca.
;Pr�klad: N=1 R="Ahoj" -> vystup hojA

;2.) V�sledok do s�boru

;3.) ClrScr() a v�sledok na ur�en� polohu na obrazovke 

.MODEL small
.STACK 100h
.DATA
Retaz1 DB 15 dup(?)
Retaz DB 9 DUP(?)
PocetMiestRotacie DB 0 
NacitaneCislo DW 0
DesiatPriNas DW 10
PopisovacSuboru DW (?)
MenoSuboru DB 'TextSub.txt',0 
.CODE

Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 


NacitajRetazec PROC
         push cx
         push ax
         mov Retaz1,13
         mov DX,offset Retaz1;do registra dx ulozime offset Retazca
         mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
         int 21h
           
         mov DL,0AH;uloz do registra posun dole
         mov AH,2;do registra AH uloz cislo sluzby
         int 21h
         
         mov ch,0
         mov cl,Retaz1[1]
         jcxz Na
        CyklusVypVlavo:
            mov DL,0DH;uloz do registra posun vlavo
            mov AH,2
            int 21h
            mov ax,0
         loop CyklusVypVlavo
         
       Na:
         pop ax
         pop cx
         ret
ENDP NacitajRetazec

VypRetazec PROC
push cx
push dx
push ax


mov di,2
mov ch,0
mov cl,Retaz1[1]
jcxz Kon
Vypisuj:
mov dl,Retaz1[di]
mov ah,2
int 21h
inc di
loop Vypisuj
Kon: 
    pop ax
    pop dx
    pop cx
    ret
ENDP VypRetazec



RotujL PROC
   push di
   push ax
   push bx
   push cx
   push dx
   InicRot:
   mov cx,ax
   jcxz Uk
   mov di,3
   
 Rotuj:
 mov bl,Retaz1[2]
   push di
   PosunLav:
      mov al,Retaz1[di]
      sub di,1
      mov Retaz1[di],al
      add di,2
    cmp Retaz1[di],0Dh 
    jnz PosunLav
    sub di,1
    mov Retaz1[di],bl 
    pop di
 loop Rotuj
Uk:
pop dx
pop cx
pop bx
pop ax
pop di
ret
ENDP RotujL

ClrScr PROC
	push ax
	
	mov ah,0
	mov al,3
	int 10h
	
	pop ax
	
	ret
ClrScr ENDP
;------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call NacitajRetazec
          call VypRetazec

          call Read
          mov NacitaneCislo,ax

          call RotujL
          call VypRetazec
          
          ;---------------------------------------------VYTVORI SUBOR PRE ZAPIS A OTVORI HO------------------
          mov dx,offset MenoSuboru
          mov ah,3Ch
          mov cx,0
          int 21h
          mov PopisovacSuboru,ax
          ;---------------------------------------------ZAPISE DO SUBORU POCET BYTOV ZADANYCH V REGISTRIcx-----------------------------
          mov bx,PopisovacSuboru
          mov ch,0
          mov cl, Retaz1[1]
          jcxz Koniec
          lea dx, Retaz1[2]
          mov ah,40h
          int 21h
          ;-------------------------------------ZATVOR SUBOR-------------------------------------------------------
          mov bx,PopisovacSuboru
          mov ah,3Eh
          int 21h

          call ClrScr 
          mov ah,0Fh
          int 10h
           
          mov dh,5
          mov dl,10
          mov ah,02h
          int 10h
          call VypRetazec
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































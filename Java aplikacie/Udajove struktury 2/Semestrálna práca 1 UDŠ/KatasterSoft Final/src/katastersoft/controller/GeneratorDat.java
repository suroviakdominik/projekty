/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.controller;

import java.util.List;
import java.util.Random;
import katastersoft.MyComponents.RandomString;
import katastersoft.data.KatastralneUzemie;
import katastersoft.data.ListVlastnictva;
import katastersoft.data.Nehnutelnost;
import katastersoft.data.Osoba;
import katastersoft.rbstrom.BVSAlgoritmy;
import katastersoft.rbstrom.RBStrom;
import katastersoft.rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public class GeneratorDat {

    private RBStrom<Integer, KatastralneUzemie> aKUPodlaCisla;
    private RBStrom<String, KatastralneUzemie> aKauPodlaNazvu;
    private RBStrom<String, Osoba> aOsoby;
    private Random aGenCisel;
    private List<Vrchol> aOsobyZoznam;

    /**
     * stromy, ktore sa maplnia vygerneovanymi datami
     * @param aKUPodlaCisla
     * @param aKauPodlaNazvu
     * @param aOsoby 
     */
    public GeneratorDat(RBStrom<Integer, KatastralneUzemie> aKUPodlaCisla, RBStrom<String, KatastralneUzemie> aKauPodlaNazvu, RBStrom<String, Osoba> aOsoby) {
        this.aKUPodlaCisla = aKUPodlaCisla;
        this.aKauPodlaNazvu = aKauPodlaNazvu;
        this.aOsoby = aOsoby;
        aGenCisel = new Random();
    }

    /**
     * Vygenreuje data, ktorymi naplni aplikaciu
     * @param paPoceOsob- pocet vyg. osob
     * @param paPocetKU- pocet vyg. KU
     * @param paPriemPocetLVVKU-priem. pocet LV v KU
     * @param preiemPocetnehnLV-priem. pocet nehnutelnosti na LV
     * @param priemPocetMajitelov - priem pocet majitelov na LV
     */
    public void generujData(int paPoceOsob,int paPocetKU,int paPriemPocetLVVKU,int preiemPocetnehnLV,int priemPocetMajitelov){
        generujOsoby(paPoceOsob);
        vygenerujKU(paPocetKU,paPriemPocetLVVKU,preiemPocetnehnLV,priemPocetMajitelov);
        priradOsobamBydliska();
    }
    
    private void vygenerujKU(int paPocetGenerKU, int priemPocetLVKU, int priemPocetNehnutelnostiLV, int priemPocetMajitelov) {
        System.out.println("Generujem KU,LV a nehn:");
        KatastralneUzemie pom;
        RandomString genSt = new RandomString(10);
        int ID = aGenCisel.nextInt(Integer.MAX_VALUE);
        String nazov = genSt.nextString();
        boolean vlozene = false;
        for (int i = 0; i < paPocetGenerKU; i++) {
            System.out.println("genKu: "+i);
            while (!vlozene) {
                if (!aKUPodlaCisla.obsahujeKluc(ID) && !aKauPodlaNazvu.obsahujeKluc(nazov)) {
                    vlozene = true;
                    break;
                }
                nazov = genSt.nextString();
            }
            vlozene = false;
            pom = new KatastralneUzemie(i, nazov);
            if (!aKUPodlaCisla.pridaj(pom.getaCisloKU(), pom)) {
                throw new IllegalStateException("Chyba v generatore. Strom KU uz obsahuje prvok s vkladanym klucom");
            }
            if (!aKauPodlaNazvu.pridaj(pom.getaNazovKU(), pom)) {
                throw new IllegalStateException("Chyba v generatore. Strom KU uz obsahuje prvok s vkladanym klucom");
            }
            generujLV(pom, priemPocetLVKU, priemPocetNehnutelnostiLV, priemPocetMajitelov);
        }
        System.out.println("Pocet vygenerovanych KU: " + aKUPodlaCisla.getaPocetPrvkov() + " " + aKauPodlaNazvu.getaPocetPrvkov());
    }

    private void generujLV(KatastralneUzemie paKU, int priemPocetLV, int priemPocetNehnutelnosti, int priemPoceMajitelov) {
        int poceVygLV = 1 + aGenCisel.nextInt(priemPocetLV);
        for (int i = 0; i < poceVygLV; i++) {
            int cisloPridLV = aGenCisel.nextInt(Integer.MAX_VALUE);
            boolean vlozene = false;
            while (!vlozene) {
                if (!paKU.existujeListVlastnictva(cisloPridLV)) {
                    vlozene = true;
                    break;
                }
                cisloPridLV = aGenCisel.nextInt(Integer.MAX_VALUE);
            }
            ListVlastnictva vkladanyLV = new ListVlastnictva(paKU, cisloPridLV);
            paKU.pridajNovyLV(vkladanyLV);
            priradMajetkovePodielyLV(vkladanyLV, priemPoceMajitelov);
            generujNehnutelnosti(vkladanyLV, priemPocetNehnutelnosti);
        }
    }

    private void priradMajetkovePodielyLV(ListVlastnictva vkladanyLV, int priemPoceMajitelov) {
        int pocetPrid = 1 + aGenCisel.nextInt(priemPoceMajitelov);
        for (int i = 0; i < pocetPrid; i++) {
            Osoba os=(Osoba)aOsobyZoznam.get(aGenCisel.nextInt(aOsobyZoznam.size())).getData();
            vkladanyLV.pridajMajetkovyPodiel(os);
        }
    }

    private void generujNehnutelnosti(ListVlastnictva paLV, int priemPocetNehnutelnosti) {
        int pocetVygNehn = 1 + aGenCisel.nextInt(priemPocetNehnutelnosti);
        RandomString genStr = new RandomString(5);
        for (int i = 0; i < pocetVygNehn; i++) {
            int supCislo = aGenCisel.nextInt(Integer.MAX_VALUE);
            boolean vlozene = false;
            while (!vlozene) {
                if (!paLV.getaKU().existujeNehnutelnost(supCislo)) {
                    vlozene = true;
                    break;
                }
                supCislo = aGenCisel.nextInt(Integer.MAX_VALUE);
            }
            Nehnutelnost vkladanaNehn = new Nehnutelnost(supCislo, genStr.nextString());
            paLV.pridajNehnutelnost(vkladanaNehn);
        }
    }

    private void generujOsoby(int paPocetVygenOsob) {
        System.out.println("Generujem osoby: ");
        Osoba pom;
        RandomString genSt = new RandomString(16);
        Random gen = new Random();
        int ID = gen.nextInt(Integer.MAX_VALUE);
        String rod_cislo = genSt.nextString();
        boolean vlozene = false;
        for (int i = 0; i < paPocetVygenOsob; i++) {
            while (!vlozene) {
                if (!aOsoby.obsahujeKluc(rod_cislo)) {
                    vlozene = true;
                    break;
                }
                rod_cislo = genSt.nextString();
            }
            vlozene = false;
            pom = new Osoba(rod_cislo, genSt.nextString(), genSt.nextString());
            if (!aOsoby.pridaj(pom.getaRodneCislo(), pom)) {
                throw new IllegalStateException("Chyba v generatore. Strom osob uz obsahuje prvok s vkladanym klucom");
            }
        }
        System.out.println("Pocet vygenerovanych ososb: " + aOsoby.getaPocetPrvkov());
        aOsobyZoznam=BVSAlgoritmy.getInorder(aOsoby.getKoren());
    }

    private void priradOsobamBydliska() {
        System.out.println("Generovanie: Priradujem bydliska obyvatelom: ");
        List<Vrchol> lpKU = BVSAlgoritmy.getInorder(aKUPodlaCisla.getKoren());
        List<Vrchol> lpOsoby = BVSAlgoritmy.getInorder(aOsoby.getKoren());
        for (Vrchol prech : lpOsoby) {
            Osoba os = (Osoba) prech.getData();
            int paKUIndex = aGenCisel.nextInt(lpKU.size());
            KatastralneUzemie kU = (KatastralneUzemie) lpKU.get(paKUIndex).getData();
            int paIndexNehnutelnosti = aGenCisel.nextInt(kU.getNehnutelnosti().size());
            Nehnutelnost nehn = (Nehnutelnost) kU.getNehnutelnosti().get(paIndexNehnutelnosti).getData();
            os.setaTrvaleBydlisko(nehn);
        }
        System.out.println("Popriradoval som bydliska");
    }
}

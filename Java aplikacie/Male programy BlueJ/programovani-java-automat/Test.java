import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Trida testuje automat na jizdenky vcetne GUI
 * 
 * @author Jirka Krhanek
 * @version 2006-05-10
 */
public class Test
{

/**
 * Metoda main je vstupnim bodem do programu
 * 
 * @param arg seznam argumentu z prikazove radky
 */
    public static void main(String[] arg)
    {
        // vytvoreni ramecku, ve kterem aplikace pobezi + titulek
        JFrame frame = new JFrame("Automat - GUI");
        // nastaveni velikosti okna aplikace
        frame.setSize(400,220);
        // nastaveni pozice okna
        frame.setLocation(200,200);
        
        // vytvoreni objektu gui
        Gui gui = new Gui();
        // inicializace objektu gui
        gui.init();
        // pridani objektu do stredu ramecku
        frame.getContentPane().add(gui,BorderLayout.CENTER);
        
        // metoda setDefaultCloseOperation definuje chovani okna pri jeho zavreni uzivatelem
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // zobrazeni ramecku
        frame.setVisible(true);
    }
}

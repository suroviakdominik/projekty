/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.HashMap;
import wof00.Hra.Hrac;
import wof00.Osoby.OsobaPredavackavBufete;

/**
 *
 * @author Dominik
 */
public class PredmetKasa implements IPredmet, Serializable {

    private HashMap<IPredmet, Integer> aCeny;
    private HashMap<String, Integer> aZostavajuceMnozstvo;
    private String aNazov = "kasa";

    public PredmetKasa() {
        aCeny = new HashMap<IPredmet, Integer>();
        aZostavajuceMnozstvo = new HashMap<String, Integer>();
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return "kasa, ktoru moze pouzit len predavacka";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        OsobaPredavackavBufete predavacka = (OsobaPredavackavBufete) paHrac.getaRozhovor();
        odpocitajMnozstvo(predavacka.getVybranyTovar());
        paHrac.odpocitajPeniaze(aCeny.get(getPredmet(predavacka.getVybranyTovar())));
        return "Kupili ste si predmet: " + predavacka.getVybranyTovar();
    }

    private void odpocitajMnozstvo(String paNazov) {
        int aktualneMnozstvo = aZostavajuceMnozstvo.get(paNazov);
        aZostavajuceMnozstvo.remove(paNazov);
        aZostavajuceMnozstvo.put(paNazov, --aktualneMnozstvo);
    }

    public int zistiMnozstvo(String paNazov) {
        return aZostavajuceMnozstvo.get(paNazov);
    }

    public int zistiCenu(IPredmet paPredmet) {
        return aCeny.get(paPredmet);
    }

    public void pridajTovar(IPredmet paPredmet, Integer cena, Integer mnozstvo) {
        aZostavajuceMnozstvo.put(paPredmet.getNazov(), mnozstvo);
        aCeny.put(paPredmet, cena);
    }

    public IPredmet getPredmet(String paNazov) {
        for (IPredmet prechadzac : aCeny.keySet()) {
            if (paNazov.equals(prechadzac.getNazov())) {
                return prechadzac;
            }
        }

        return null;
    }

    public void doplnZasobyTovaru(String paNazov, int paMnozstvo) {
        aZostavajuceMnozstvo.remove(paNazov);
        aZostavajuceMnozstvo.put(paNazov, paMnozstvo);
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.hra;

import šach.Ine.Casovac;
import šach.spusteniehry.Menu;
import Grafika.Platno;
import šach.Prikazy.NacitavacPrikazov;

/**
 * Inštancia triedy hra má za úlohu vykonávať striedanie hráčov, spustenie
 * časovača odohranej hry a umožniť im zadávanie príkazov. Okrem toho
 * vyhodnocuje, či sa má hra ukončiť.
 *
 * @author Dominik
 */
public class Hra {

    private Hrac aHracA;
    private Hrac aHracB;
    private NacitavacPrikazov aNacitavac;
    private Menu aMenuHry;
    private Casovac aPocitadloStravenehoCasu;

    /**
     * Konštruktor pri zavolaní priradí do atribútov hráčov, ktorý budú danú hru
     * hrať, vytvorí objekt typu Nacitavac príkazov a Casovac a priradí ich do
     * atribútov.Okrem toho zavolá metódu startCasuj() objektu aCasovac, ktorá
     * začne počítať odohraný čas.
     *
     * @param paHracA - prvý hráč
     * @param paHracB - druhý hráč
     * @param paMenu - menu
     * @param paHodiny - počiatočný stav odohraný hodín
     * @param paMinuty - počiatočný stav odohraný minút
     * @param paSekundy - počiatočný stav odohraný sekúnd
     */
    public Hra(Hrac paHracA, Hrac paHracB, Menu paMenu, int paHodiny, int paMinuty, int paSekundy) {
        aMenuHry = paMenu;
        aHracA = paHracA;
        aHracB = paHracB;
        aNacitavac = new NacitavacPrikazov();
        aPocitadloStravenehoCasu = new Casovac(paHodiny, paMinuty, paSekundy);
    }

    /**
     * spustí hru a postupne bude striedať hráčov. Strieda ich tak, že zavolá
     * metódu nacitajPrikaz() vytvoreného objektu NacitavacPrikazov. Ak bol
     * vykonaný taký príkaz, po ktorom je potrebné vystriedať hráča, hráči sa
     * vystriedajú.
     */
    public void hraj() {
        boolean lpPrikazUkonci;

        aPocitadloStravenehoCasu.startCasuj();
        do {
            if (aHracA.isaJeNaTahu() == true) {
                System.out.println("Na rade je hráč: " + aHracA.getMeno() + " Farba figúrok: " + aHracA.getFarbaFigurok());
                aNacitavac.vlozHracaKtoryZadavaPrikazy(aHracA);
                aHracA.setaJeNaTahu(false);
                aHracB.setaJeNaTahu(true);
            } else {
                System.out.println("Na rade je hráč: " + aHracB.getMeno() + " Farba figúrok: " + aHracB.getFarbaFigurok());
                aNacitavac.vlozHracaKtoryZadavaPrikazy(aHracB);
                aHracB.setaJeNaTahu(false);
                aHracA.setaJeNaTahu(true);
            }
            aNacitavac.nacitajPrikaz();
            lpPrikazUkonci = ukoncitHru();

        } while (!lpPrikazUkonci);
        aMenuHry.spustiHlavneMenu();
        Platno.dajPlatno().getJFrame().removeAll();

    }

    /**
     * zisťuje, či má danú hru ukončiť.
     *
     * @return -true ak ukončiť, false ak neukončiť hru
     */
    private boolean ukoncitHru() {
        if ((aHracA.getaProtiHrac() == null) || (aHracB.getaProtiHrac() == null)) {
            return true;
        } else {
            int pocetKralov = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if ((aHracA.getaSachovnicaNaKtorejSaHra()[i][j] != null) && (aHracA.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals("Kral"))) {
                        pocetKralov += 1;
                    }
                }
            }
            if (pocetKralov == 2) {
                return false;
            } else {
                System.out.println("Bol prevedený šach mat, hra sa ukončí.");
                return true;
            }

        }

    }

}

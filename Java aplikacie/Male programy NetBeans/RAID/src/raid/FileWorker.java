/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raid;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {

    public static void writeDataToFilesRaid3(File f1, File f2, File parFile, String paData) {
        try {
            ByteArrayOutputStream bAOS = new ByteArrayOutputStream();
            bAOS.write(paData.getBytes());

            byte[] bfull;
            if (bAOS.toByteArray().length % 2 == 0) {
                bfull = bAOS.toByteArray();
            } else {
                bfull = Arrays.copyOf(bAOS.toByteArray(), bAOS.toByteArray().length + 1);
                bfull[bfull.length - 1] = 0;
            }
            FileOutputStream fOS = new FileOutputStream(f1);
            FileOutputStream fOS2 = new FileOutputStream(f2);
            FileOutputStream fPF = new FileOutputStream(parFile);
            for (int i = 0; i < bfull.length; i++) {
                if (i % 2 == 0) {
                    fOS.write(bfull[i]);
                } else {
                    fOS2.write(bfull[i]);
                    fPF.write(bfull[i] ^ bfull[i - 1]);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static String readDataFromRaidFiles(File f1, File f2) {
        String ret = "";
        FileInputStream fIS = null;
        FileInputStream fIS2 = null;
        try {
            fIS = new FileInputStream(f1);
            fIS2 = new FileInputStream(f2);
            ArrayList<Integer> bFile1 = new ArrayList<>();
            ArrayList<Integer> bFile2 = new ArrayList<>();
            int readByte = 0;
            while ((readByte = fIS.read()) != -1) {
                bFile1.add(readByte);
            }
            readByte = 0;
            while ((readByte = fIS2.read()) != -1) {
                bFile2.add(readByte);
            }
            char[] vyseledok = new char[bFile1.size() + bFile2.size()];
            for (int i = 0; i < bFile1.size(); i++) {
                vyseledok[i * 2] = (char) bFile1.get(i).byteValue();
            }

            for (int i = 0; i < bFile2.size(); i++) {

                vyseledok[i * 2 + 1] = (char) bFile2.get(i).byteValue();

            }
            StringBuilder builder = new StringBuilder();
            builder.append(vyseledok);
            return builder.toString();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fIS != null) {
                    fIS.close();
                }
                if (fIS2 != null) {
                    fIS2.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static String obnovDataVPripadeStraty(File f, File parSub, boolean paParneBajtyChybaju) {
        String ret = "";
        FileInputStream fIS = null;
        FileInputStream fIS2 = null;
        try {
            fIS = new FileInputStream(f);
            fIS2 = new FileInputStream(parSub);
            ArrayList<Integer> bFile1 = new ArrayList<>();
            ArrayList<Integer> bFile2 = new ArrayList<>();
            int readByte = 0;
            while ((readByte = fIS.read()) != -1) {
                bFile1.add(readByte);
            }
            readByte = 0;
            while ((readByte = fIS2.read()) != -1) {
                bFile2.add(readByte);
            }

            if (bFile1.size() > bFile2.size()) {
                for (int i = bFile2.size(); i < bFile1.size(); i++) {
                    bFile2.add(0);
                }
            } else if (bFile2.size() > bFile1.size()) {
                for (int i = bFile1.size(); i < bFile2.size(); i++) {
                    bFile1.add(0);
                }
            }

            StringBuilder builder = new StringBuilder();
            if (paParneBajtyChybaju) {
                for (int i = 0; i < bFile1.size(); i++) {
                    builder.append((char) bFile1.get(i).byteValue());
                    builder.append((char) (bFile1.get(i).byteValue() ^ bFile2.get(i).byteValue()));
                }
            }

            return builder.toString();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fIS != null) {
                    fIS.close();
                }
                if (fIS2 != null) {
                    fIS2.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}

package katastersoft.MyComponents;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import katastersoft.data.KatastralneUzemie;
import katastersoft.data.ListVlastnictva;
import katastersoft.data.Nehnutelnost;
import katastersoft.data.Osoba;
import katastersoft.rbstrom.BVSAlgoritmy;
import katastersoft.rbstrom.RBStrom;
import katastersoft.rbstrom.RBStrom.Vrchol;
import optimalizaciasieti.FileWorker;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dominik
 */
public class Zalohovac {

    private List<Vrchol> aKatastrePodlaCKU;
    private List<Vrchol> aOsoby;
    private File aPriecinok = new File(System.getProperty("user.home") + "\\Desktop");
    private RBStrom<Integer, KatastralneUzemie> aRBKUPCKU;
    private RBStrom<String, KatastralneUzemie> aRBKatastrePodlaNazvu;
    private RBStrom<String, Osoba> aRBOsoby;

    /**
     * Stromy, ktore treba zalohovat
     * @param paKatastrePodlaCKU
     * @param paKatastrePodlaNazvu
     * @param paOsoby 
     */
    public Zalohovac(RBStrom<Integer, KatastralneUzemie> paKatastrePodlaCKU, RBStrom<String, KatastralneUzemie> paKatastrePodlaNazvu, RBStrom<String, Osoba> paOsoby) {
        aKatastrePodlaCKU = BVSAlgoritmy.getInorder(paKatastrePodlaCKU.getKoren());
        aOsoby = BVSAlgoritmy.getInorder(paOsoby.getKoren());
        aRBKUPCKU = paKatastrePodlaCKU;
        aRBKatastrePodlaNazvu = paKatastrePodlaNazvu;
        aRBOsoby = paOsoby;
    }

    /**
     * vytovri zalohu. Data  budu ulozene v suboroch na ploche
     */
    public void vytvorZalohu() {
        zalohujOsoby();
        zalohaKULVNehn();
    }

    /*private void zalohujOsoby() {
     new File(aPriecinok.getAbsolutePath() + "\\os.txt").delete();
     Osoba spracOsob;
     String zapisRiadok;
     for (Vrchol prechOS : aOsoby) {
     if (prechOS.getData() instanceof Osoba) {
     spracOsob = (Osoba) prechOS.getData();
     zapisRiadok = spracOsob.getaRodneCislo() + "|" + spracOsob.getaMeno() + "|" + spracOsob.getaPriezvisko();
     if (spracOsob.getaTrvaleBydlisko() != null) {
     zapisRiadok += "|||" + spracOsob.getaTrvaleBydlisko().getaZapNaLV().getaKU().getaCisloKU() + "|" + spracOsob.getaTrvaleBydlisko().getaSupisneCislo() + "|||";
     }else{
     zapisRiadok+="|||NON|||";
     }
     for (ListVlastnictva prechLVOS : spracOsob.getLVOsoby()) {
     zapisRiadok += prechLVOS.getaKU().getaCisloKU() + "|" + prechLVOS.getaCisloLV() + "||";
     }
     FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\os.txt"), zapisRiadok, true);
     } else {
     throw new IllegalArgumentException("Predpokladany typ Osoba. Najdeny iny.");
     }
     }
     }*/
    /*private void zalohaKULVNehn() {
     new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt").delete();
     KatastralneUzemie spracKu;
     ListVlastnictva spracLV;
     Nehnutelnost spracNehnuelnost;
     String zapisRiadok;
     for (Vrchol prechKU : aKatastrePodlaCKU) {
     spracKu = (KatastralneUzemie) prechKU.getData();
     zapisRiadok = "" + spracKu.getaCisloKU() + "|" + spracKu.getaNazovKU();
     if (spracKu.getLV().size() == 0) {
     FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt"), zapisRiadok, true);
     }
     for (Vrchol prechLVKU : spracKu.getLV()) {
     spracLV = (ListVlastnictva) prechLVKU.getData();
     zapisRiadok += "|||" + spracLV.getaCisloLV() + "|||";
     for (Nehnutelnost spracNehn : spracLV.getNehnutelnosti()) {
     zapisRiadok += spracNehn.getaSupisneCislo() + "|" + spracNehn.getaAdresa() + "||";
     }
     FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt"), zapisRiadok, true);
     zapisRiadok = "" + spracKu.getaCisloKU() + "|" + spracKu.getaNazovKU();;
     }
     }
     }*/
    private void zalohujOsoby() {
        new File(aPriecinok.getAbsolutePath() + "\\os.txt").delete();
        Osoba spracOsob;
        StringBuilder zapRiadok = new StringBuilder(10000000);
        int pocetPriRiadkov = 0;
        for (Vrchol prechOS : aOsoby) {
            if (prechOS.getData() instanceof Osoba) {
                spracOsob = (Osoba) prechOS.getData();
                zapRiadok.append(spracOsob.getaRodneCislo() + "|" + spracOsob.getaMeno() + "|" + spracOsob.getaPriezvisko());
                if (spracOsob.getaTrvaleBydlisko() != null) {
                    zapRiadok.append("|||" + spracOsob.getaTrvaleBydlisko().getaZapNaLV().getaKU().getaCisloKU() + "|" + spracOsob.getaTrvaleBydlisko().getaSupisneCislo() + "|||");
                } else {
                    zapRiadok.append("|||NON|||");
                }
                for (ListVlastnictva prechLVOS : spracOsob.getLVOsoby()) {
                    zapRiadok.append(prechLVOS.getaKU().getaCisloKU() + "|" + prechLVOS.getaCisloLV() + "||");
                }
                pocetPriRiadkov++;
                zapRiadok.append("\n");
                if (pocetPriRiadkov >= 1000) {
                    FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\os.txt"), zapRiadok.toString(), true);
                    zapRiadok = new StringBuilder(10000000);
                    pocetPriRiadkov = 0;
                }
            } else {
                throw new IllegalArgumentException("Predpokladany typ Osoba. Najdeny iny.");
            }
        }
        FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\os.txt"), zapRiadok.toString(), true);
    }

    private void zalohaKULVNehn() {
        new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt").delete();
        KatastralneUzemie spracKu;
        ListVlastnictva spracLV;
        Nehnutelnost spracNehnuelnost;
        StringBuilder zapisRiadok = new StringBuilder(10000000);
        int pocetPrejdRiadkov = 0;
        int progres = 0;
        for (Vrchol prechKU : aKatastrePodlaCKU) {
            progres++;
            System.out.print("Progres: " + progres + "\n");
            spracKu = (KatastralneUzemie) prechKU.getData();
            zapisRiadok.append(spracKu.getaCisloKU()).append("|").append(spracKu.getaNazovKU());
            if (spracKu.getLV().isEmpty()) {
                zapisRiadok.append("\n");
                pocetPrejdRiadkov++;
            }
            String koniecRiadku = null;
            for (Vrchol prechLVKU : spracKu.getLV()) {
                if (koniecRiadku != null) {
                    zapisRiadok.append(koniecRiadku);
                }
                spracLV = (ListVlastnictva) prechLVKU.getData();
                zapisRiadok.append("|||").append(spracLV.getaCisloLV()).append("|||");
                for (Nehnutelnost spracNehn : spracLV.getNehnutelnosti()) {
                    zapisRiadok.append(spracNehn.getaSupisneCislo()).append("|").append(spracNehn.getaAdresa()).append("||");
                }
                pocetPrejdRiadkov++;

                koniecRiadku = ("\n" + spracKu.getaCisloKU() + "|" + spracKu.getaNazovKU());
            }
            zapisRiadok.append("\n");
            if (pocetPrejdRiadkov >= 1000) {
                FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt"), zapisRiadok.toString(), true);
                pocetPrejdRiadkov = 0;
                zapisRiadok = new StringBuilder(10000000);
            }
        }
        FileWorker.writeDataToFile(new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt"), zapisRiadok.toString(), true);
    }
    
    /**
     * Obnovi zalohu z ecistujucich suborov, vytvorenych pri poslednej zalohe
     */
    public void obnovZalohu() {
        obnovKULVNehn();
        obnovOsoby();
    }

    private void obnovKULVNehn() {
        FileWorker fW = new FileWorker(new File(aPriecinok.getAbsolutePath() + "\\kuLVNehn.txt"));
        try {
            fW.startRead();
            String panacRiadok;
            while ((panacRiadok = fW.readNextLine()) != null) {
                zistiAkeUdajeRiadokObsahuje(panacRiadok);
            }

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (IOException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (RuntimeException ex) {
            fW.stopRead();
            throw ex;
        } finally {
            fW.stopRead();
        }
    }

    private void zistiAkeUdajeRiadokObsahuje(String paRiadok) {
        String s[] = paRiadok.split("\\|\\|\\|");
        switch (s.length) {
            case 1:
                vytvorKU(s[0]);
                break;
            case 2:
                vytvorKU(s[0]);
                vytvorLV(s[0], s[1]);
                break;
            case 3:
                vytvorKU(s[0]);
                vytvorLV(s[0], s[1]);
                vytvorNehnutelnosti(s[0], s[1], s[2]);
                break;
            default:
                throw new IllegalStateException("Riadok v takom formate by nemal existovat");
        }
    }

    private void vytvorKU(String s) {
        String[] paData = s.split("\\|");
        Integer paCKU = Integer.parseInt(paData[0]);
        String nazovKU = paData[1];
        if ((aRBKUPCKU.obsahujeKluc(paCKU) && !aRBKatastrePodlaNazvu.obsahujeKluc(nazovKU))
                || (!aRBKUPCKU.obsahujeKluc(paCKU) && aRBKatastrePodlaNazvu.obsahujeKluc(nazovKU))) {
            throw new IllegalStateException("Stromy KU - podla nazvu a cisla sa nezhoduju");
        }
        if (!aRBKUPCKU.obsahujeKluc(paCKU)) {
            KatastralneUzemie pridKU = new KatastralneUzemie(paCKU, nazovKU);
            aRBKUPCKU.pridaj(pridKU.getaCisloKU(), pridKU);
            aRBKatastrePodlaNazvu.pridaj(pridKU.getaNazovKU(), pridKU);
        }
    }

    private void vytvorLV(String s, String s0) {
        Integer lpCKU = Integer.parseInt(s.split("\\|")[0]);
        Integer lpCLV = Integer.parseInt(s0);
        KatastralneUzemie lpVklDoKU = aRBKUPCKU.dajHodnotu(lpCKU);
        if (!lpVklDoKU.existujeListVlastnictva(lpCLV)) {
            ListVlastnictva lv = new ListVlastnictva(lpVklDoKU, lpCLV);
            lpVklDoKU.pridajNovyLV(lv);
        }
    }

    private void vytvorNehnutelnosti(String s, String s0, String s1) {
        Integer lpCKU = Integer.parseInt(s.split("\\|")[0]);
        Integer lpCLV = Integer.parseInt(s0);
        KatastralneUzemie lpVklDoKU = aRBKUPCKU.dajHodnotu(lpCKU);
        ListVlastnictva lv = lpVklDoKU.getListVlastnictva(lpCLV);
        String[] nehnutelnosti = s1.split("\\|\\|");
        Integer supCisloVklNehm;
        String adresaVklNehn;
        for (int i = 0; i < nehnutelnosti.length; i++) {
            supCisloVklNehm = Integer.parseInt(nehnutelnosti[i].split("\\|")[0]);
            adresaVklNehn = nehnutelnosti[i].split("\\|")[1];
            Nehnutelnost vklN = new Nehnutelnost(supCisloVklNehm, adresaVklNehn);
            if (lpVklDoKU.existujeNehnutelnost(supCisloVklNehm)) {
                throw new IllegalArgumentException("Nehnutelnost uz existuje");
            }
            lv.pridajNehnutelnost(vklN);
        }
    }

    private void obnovOsoby() {
        FileWorker fW = new FileWorker(new File(aPriecinok.getAbsolutePath() + "\\os.txt"));
        try {
            fW.startRead();
            String panacRiadok;
            while ((panacRiadok = fW.readNextLine()) != null) {
                zistiAkeUdajeRiadokOsobaObsahuje(panacRiadok);
            }

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (IOException ex) {
            Logger.getLogger(Zalohovac.class.getName()).log(Level.SEVERE, null, ex);
            fW.stopRead();
        } catch (RuntimeException ex) {
            fW.stopRead();
            throw ex;
        } finally {
            fW.stopRead();
        }
    }

    private void zistiAkeUdajeRiadokOsobaObsahuje(String paNacRiadok) {
        String[] paData = paNacRiadok.split("\\|\\|\\|");
        switch (paData.length) {
            case 1:
                vytvorOsobu(paData[0]);
                break;
            case 2:
                vytvorOsobu(paData[0]);
                nastavOsobeTrvaleBydlisko(paData[0], paData[1]);
                break;
            case 3:
                vytvorOsobu(paData[0]);
                nastavOsobeTrvaleBydlisko(paData[0], paData[1]);
                priradMajetkovePodiely(paData[0], paData[2]);
                break;
            default:
                throw new IllegalStateException("Riadok v takom formate by nemal existovat");
        }
    }

    private void vytvorOsobu(String paDataOsoba) {
        String[] paData = paDataOsoba.split("\\|");
        String rC = paData[0];
        String meno = paData[1];
        String priez = paData[2];
        if (!aRBOsoby.obsahujeKluc(rC)) {
            Osoba pridOs = new Osoba(rC, meno, priez);
            aRBOsoby.pridaj(pridOs.getaRodneCislo(), pridOs);
        }
    }

    private void nastavOsobeTrvaleBydlisko(String paDataOsoba, String paDataBydlisko) {
        if(paDataBydlisko.equals("NON")){
            return;
        }
        String osobaRC=paDataOsoba.split("\\|")[0];
        String[] dataTrvB=paDataBydlisko.split("\\|");
        int lpCisloKU=Integer.parseInt(dataTrvB[0]);
        int lpSupC=Integer.parseInt(dataTrvB[1]);
        Nehnutelnost lpNehn=aRBKUPCKU.dajHodnotu(lpCisloKU).getNehnutelnost(lpSupC);
        aRBOsoby.dajHodnotu(osobaRC).setaTrvaleBydlisko(lpNehn);
    }

    private void priradMajetkovePodiely(String paDataOsoba, String paDataLV) {
        String[] paLVOsoby=paDataLV.split("\\|\\|");
        String osobaRC=paDataOsoba.split("\\|")[0];
        Osoba spracOsoba=aRBOsoby.dajHodnotu(osobaRC);
        Integer ckULV;
        Integer cLV;
        for (int i = 0; i < paLVOsoby.length; i++) {
            String[] majPodData=paLVOsoby[i].split("\\|");
            ckULV=Integer.parseInt(majPodData[0]);
            cLV=Integer.parseInt(majPodData[1]);
            ListVlastnictva lv=aRBKUPCKU.dajHodnotu(ckULV).getListVlastnictva(cLV);
            lv.pridajMajetkovyPodiel(spracOsoba);
        }
    }
}

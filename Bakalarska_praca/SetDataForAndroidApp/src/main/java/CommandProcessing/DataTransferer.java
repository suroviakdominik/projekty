/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommandProcessing;

import setdatatoandroidaplication.*;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Trieda typu SINGLETON.
 * Pouziva sa na prenos dat medzi zalozkami(Tabami) a editormi, ktore sluzia
 * na editaciu dat ´prijanych zaloykami.
 * @author Dominik
 */
public class DataTransferer {

    private HashMap<String, String> aStringData;
    private HashMap<String, Integer> aIntData;
    private HashMap<String, Double> aDoubleData;
    private HashMap<String, BufferedImage> aImageData;
    private static DataTransferer aInstance = null;
    private HashMap<String,Info_About_Item> aInfoAboutItemData;

    /**
     * vytvori Map objekty, kde nozno v tvare
     * KLUC:HODNOTA pridavatudaje
      */
    private DataTransferer() {
        aStringData = new HashMap<>();
        aIntData = new HashMap<>();
        aDoubleData = new HashMap<>();
        aImageData = new HashMap<>();
        aInfoAboutItemData=new HashMap<>();
    }

    /**
     * 
     * @return instancia DataTransfer. Typ SINGLETON
     */
    public static DataTransferer getInstance() {
        if (aInstance == null) {
            aInstance = new DataTransferer();
        }
        return aInstance;
    }

    /**
     * 
     * @param paKey -kluc
     * @return hodnota nachdzajuca sa HasHMape s danym kluco
     */
    public Integer getInt(String paKey) {
        return aIntData.get(paKey);
    }

    /**
     * Prida nove data typu int do DataTransferer objektu pod klucom zadanym ako parameter
     * @param paKey - kluc pridavanej hodnoty
     * @param paValue - hodnota priradena klucu
     */
    public void addInt(String paKey, int paValue) {
        aIntData.put(paKey, paValue);
    }
    
    /**
     * 
     * @return vsetky objekty typu String nachadzajuce sa v DataTransferer
     */
    public List<String> getAllStrings()
    {
        List<String> ret=new ArrayList<>();
        for(String prech:aStringData.keySet())
        {
            ret.add(aStringData.get(prech));
        }
        return ret;
    }
    
    /**
     * 
     * @return vsetky kluce objektov typu String nachadzajuce sa v instancii
     *          DataTransferer
     */
    public List<String> getAllStringsKeys()
    {
        ArrayList<String> ret=new ArrayList<>();
        for(String prech:aStringData.keySet())
        {
            ret.add(prech);
        }
        return ret;
    }
    
     
    /**
     * 
     * @return vsetky objekty typu Info_About_Item nachadzajuce sa v DataTransferer
     */
    public List<Info_About_Item> getAllInfoAboutItem()
    {
       ArrayList<Info_About_Item> ret=new ArrayList<>();
        for(String prech:aInfoAboutItemData.keySet())
        {
            ret.add(aInfoAboutItemData.get(prech));
        }
        return ret;
    }
    
     /**
     * 
     * @return vsetky kluce objektov typu Info_About_Item nachadzajuce sa v instancii
     *          DataTransferer
     */
    public List<String> getAllInfoAboutItemKeys()
    {
        ArrayList<String> ret=new ArrayList<>();
        for(String prech:aInfoAboutItemData.keySet())
        {
            ret.add(prech);
        }
        return ret;
    }
/**
     * 
     * @param paKey -kluc
     * @return hodnota typu String nachdzajuca sa HasHMape s danym klucom
     */
    public String getString(String paKey) {
        return aStringData.get(paKey);
    }

     /**
     * Prida nove data typu String do DataTransferer objektu pod klucom zadanym ako parameter
     * @param paKey - kluc pridavanej hodnoty
     * @param paValue - hodnota priradena klucu
     */
    public void addString(String paKey, String paValue) {
        aStringData.put(paKey, paValue);
    }

    /**
     * 
     * @param paKey -kluc
     * @return hodnota typu Double nachdzajuca sa HasHMape s danym klucom
     */
    public Double getDouble(String paKey) {
        return aDoubleData.get(paKey);
    }

     /**
     * Prida nove data typu double do DataTransferer objektu pod klucom zadanym ako parameter
     * @param paKey - kluc pridavanej hodnoty
     * @param paValue - hodnota priradena klucu
     */
    public void addDouble(String paKey, double paValue) {
        aDoubleData.put(paKey, paValue);
    }

    public BufferedImage getImage(String paKey) {
        return aImageData.get(paKey);
    }

     /**
     * Prida nove data typu BufferedImage do DataTransferer objektu pod klucom zadanym ako parameter
     * @param paKey - kluc pridavanej hodnoty
     * @param paValue - hodnota priradena klucu
     */
    public void addImage(String paKey, BufferedImage paValue) {
        aImageData.put(paKey, paValue);
    }
    
     /**
     * Prida nove data typu Infoˇ_About_Item do DataTransferer objektu pod klucom zadanym ako parameter
     * @param paKey - kluc pridavanej hodnoty
     * @param paValue - hodnota priradena klucu
     */
    public void addItemInfo(String paKey,Info_About_Item paItem)
    {
        aInfoAboutItemData.put(paKey, paItem);
    }
    
    /**
     * 
     * @param paKey -kluc
     * @return hodnota typu Info_About_Item nachdzajuca sa HasHMape s danym klucom
     */
    public Info_About_Item getItemInfo(String paKey)
    {
        return aInfoAboutItemData.get(paKey);
    }
    
    /**
     * Vymaye vsetky data, ktore sa aktualne nachadzaju v tomto objkte 
     */
    public void removeAllData()
    {
        aImageData.clear();
        aStringData.clear();
        aIntData.clear();
        aDoubleData.clear();
        aInfoAboutItemData.clear();
    }

}

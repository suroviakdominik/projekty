set pagesize 123
set linesize 60

TTITLE LEFT datum CENTER 'V�pis tovarov spolu s poetom predan�ch kusov, ktor� sa predali spolu so zadan�m tovarom'
COLUMN datum NEW_VALUE datum NOPRINT

SELECT nazov,cisFaktZadTov, decode(Tovar.NAZOV,'&NazovTovaru',VyfakturovanyTovar.mnozstvo-1,VyfakturovanyTovar.mnozstvo)as mnozstvo
from VYfakturovanyTovar,Tovar, (select vyfakturovanytovar.CISLO_FA as cisFaktZadTov
from tovar,vyfakturovanytovar,faktura
where tovar.KATALOGOVECISLO=vyfakturovanytovar.KATALOGOVECISLO
and  vyfakturovanytovar.CISLO_FA=faktura.CISLO_FA
and tovar.nazov = '&NazovTovaru'
group by(nazov,vyfakturovanytovar.cislo_fa))
where cisFaktZadTov=VYfakturovanyTovar.CISLO_FA
and VYfakturovanyTovar.KATALOGOVECISLO =Tovar.KATALOGOVECISLO
and ((tovar.nazov = '&NazovTovaru' and VyfakturovanyTovar.mnozstvo-1>0)OR (tovar.nazov != '&NazovTovaru'))
group by nazov,VyfakturovanyTovar.mnozstvo,cisFaktZadTov;


/**
 * Write a description of class Tester here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Tester
{
private boolean SpustiT1;
private boolean SpustiT2;
private boolean SpustiT3;

private boolean test1(int cislo)
    {
      return
      (
      (cislo%2 != 0)&&
      (cislo>0)&&
      (cislo <=10) ||
      (cislo==1024));
    }
    
private boolean test2(int cislo)
    {
    return
    (cislo%7==0) ||
    ((cislo*2)%4==0);
    }
    
    
private boolean test3(int cislo)
    {
    return
    (cislo%4==0) &&
    !(
    (cislo%100==0)&&(cislo%400!=0));
    
    }    
    
    public Tester(boolean SpustiTest1,
                  boolean SpustiTest2,
                  boolean SpustiTest3)
                   {
                   this.SpustiT1=SpustiTest1;
                   this.SpustiT2=SpustiTest2;
                   this.SpustiT3=SpustiTest3;
      
                   }

    public boolean Otestuj(int cislo)
    {
      return
      (!(SpustiT1 && test1(cislo))) &&
      (!(SpustiT2 && !test2(cislo))) &&
      (!(SpustiT3 && !test3(cislo)));
    }  
}    









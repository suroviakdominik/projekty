package com.suroviak.dominik.zooAplication.FlowTextAroundPicture;

import android.content.Context;
import android.graphics.Point;
import android.text.SpannableString;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Dominik on 4.3.2015.
 */
public class FlowTextHelper {
    private static boolean mNewClassAvailable;

    /* class initialization fails when this throws an exception */
    static {
        try {
            Class.forName("android.text.style.LeadingMarginSpan$LeadingMarginSpan2");
            mNewClassAvailable = true;
        } catch (Exception ex) {
            mNewClassAvailable = false;
        }
    }

    public static void tryFlowText(Context cc, String text, View thumbnailView, TextView messageView, Display display, int addPadding) {
        // There is nothing I can do for older versions, so just return
        if (text.length() == 0) return;
        if (!mNewClassAvailable) return;
        // Get height and width of the image and height of the text line
        thumbnailView.measure(display.getWidth(), display.getHeight());
        int height = thumbnailView.getMeasuredHeight();
        int width = thumbnailView.getMeasuredWidth() + addPadding;
        messageView.measure(width, height); //to allow getTotalPaddingTop
        int padding = messageView.getTotalPaddingTop();
        float textLineHeight = messageView.getPaint().getTextSize();

        // Set the span according to the number of lines and width of the image
        int lines = (int) Math.round((height - padding) / textLineHeight);
        SpannableString ss = new SpannableString(text);

        //For an html text you can use this line: SpannableStringBuilder ss = (SpannableStringBuilder)Html.fromHtml(text);
        //int pixelsInLine=(int) (messageView.getPaint().getTextSize());

        //Full width and height of screen
        WindowManager wm = (WindowManager) cc.getSystemService(Context.WINDOW_SERVICE);
        Display displayFull = wm.getDefaultDisplay();
        Point size = new Point();
        displayFull.getSize(size);
        int widthFull = size.x;
        int heightFull = size.y;
        int numberOfCharactersToSpan = 0;
        int widthOfOneSpannedLine = widthFull - width;
        int widthInPixelsOfSpannedLines = lines * widthOfOneSpannedLine;
        int sumOfWidthInPixelsCharToSpan = 0;


        ss.setSpan(new MyLeadingMarginSpan2(lines, width), 0, 0, 0);
        messageView.setText(ss);

        // Align the text with the image by removing the rule that the text is to the right of the image
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) messageView.getLayoutParams();
        int[] rules = params.getRules();
        rules[RelativeLayout.RIGHT_OF] = 0;
    }


}

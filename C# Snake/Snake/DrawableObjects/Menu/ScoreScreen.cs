﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Snake.Network;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace Snake.DrawableObjects.Menu
{
    /// <summary>
    /// enum polozky menu v score options
    /// </summary>
    public enum ScoreOptions
    {
        BestScore,
        MyBestScore,
        StartMenu
    }

    /// <summary>
    /// ak pouzivatel zvoli najeku polozku z menu, vyvola sa udalost, ktora dostane ako parameter objekt tejto triedy
    /// </summary>
    public class ScoreMenuOptEventArgs : EventArgs
    {
        private ScoreOptions aSelOption;
        public ScoreMenuOptEventArgs(ScoreOptions paSelectedOption)
        {
            aSelOption = paSelectedOption;
        }

        public ScoreOptions SelOption
        {
            get { return aSelOption; }
        }
    }

  /// <summary>
    /// Trieda, ktora zobrazuje menu ak chce pouzivatel zistit najlepsie dosiahnute skore
  /// </summary>
    class ScoreScreen
    {
        private const int VerticalGap = 10;///vertikalna medzera menu poloziek
        private GraphicsDevice aGraphicsDevice;///obrazovka, ktora sluzi ako platno
        private Texture2D aOptionFrame;///ramik pre polozku menu
        private SpriteFont aFont;///pismo pre polozku menu
        private int aCountOfOptions;///pocet enum moznosti menu
        private List<Vector2> aPositionsOfOptions;///zoznam suradnic, kde vykreslovat polozky menu
        private List<string> aOptionNames;///mena jednotlivych poloziek menu
        public ScoreOptions SelectedOption { get; set; }///aktualne zvolena moznost v start menu - este nepotvrdena
        private float aTimeFromLastUpdate;///cas od posledneho updatu zvolenej polozky menu

        public event EventHandler<ScoreMenuOptEventArgs> OptionSelectedEvent;/// udalost ktora sa vyvola ked pouzivatel vyberie jednu z menu
        private List<string> aDataToDraw;//riadky z databazy na vypis
        private SpriteFont aScoreFont;//pismo pre score
        
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public ScoreScreen(GraphicsDevice graphicsDevice)
        {
            aGraphicsDevice = graphicsDevice;
            aPositionsOfOptions = new List<Vector2>();
            aOptionNames = new List<string>();
            aOptionNames.Add("Best Score");
            aOptionNames.Add("My best score");
            aOptionNames.Add("Start menu");
            SelectedOption = ScoreOptions.BestScore;
            aCountOfOptions = Enum.GetValues(typeof(ScoreOptions)).Length;
            aDataToDraw = new List<string>();
        }

        /// <summary>
        /// sirka ramika menu polozky
        /// </summary>
        public float OptFrameWidth
        {
            get { return aOptionFrame.Width; }
        }

        /// <summary>
        /// vyska ramika menu polozky
        /// </summary>
        public float OptFrameHeight
        {
            get { return aOptionFrame.Height; }
        }

        /// <summary>
        /// nacita polozky, ktore sa budu vykreslovat, prehravat ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aOptionFrame = content.Load<Texture2D>("start");
            aFont = content.Load<SpriteFont>("SMFont");
            aScoreFont = content.Load<SpriteFont>("score");
        }

        /// <summary>
        /// Update vybranej moznosti- ak pouzivatel stlacil sipku nadol alebo nahor
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            Vector2 firstOptPos = new Vector2((float)aGraphicsDevice.Viewport.Width / 2 - OptFrameWidth / 2,
                aGraphicsDevice.Viewport.Height - aCountOfOptions * VerticalGap - aCountOfOptions * OptFrameHeight);
            aPositionsOfOptions.Clear();
            for (int i = 0; i < aCountOfOptions; i++)
            {
                aPositionsOfOptions.Add(new Vector2(firstOptPos.X, firstOptPos.Y + i * VerticalGap + i * OptFrameHeight));
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
            {
                if (SelectedOption == ScoreOptions.BestScore || SelectedOption == ScoreOptions.MyBestScore)
                {
                    if (DatabaseManager.checkForDatabaseConnectivity())
                    {
                        aDataToDraw.Clear();
                        if (SelectedOption == ScoreOptions.MyBestScore)
                        {
                            string name = DataDialog.showDialog("Name: ", "Name for score");
                            if (DataDialog.Result == DialogResult.OK)
                            {
                                int i = 1;
                                foreach (var iter in DatabaseManager.DisplayBestMyBestScore(name))
                                {
                                    aDataToDraw.Add(i + " ." + iter);
                                    i++;
                                }
                            }
                        }
                        else
                        {
                            int i = 1;
                            foreach (var iter in DatabaseManager.DisplayBestFiveSinglePlayerScore())
                            {
                                aDataToDraw.Add(i+" ."+iter);
                                i++;
                            }
                        }
                        Thread.Sleep(Game1.SleepTimeMenuItemChoose);
                    }
                }
                else
                {
                    OnOptionSelectedEvent(new ScoreMenuOptEventArgs(SelectedOption));
                }
            }
            aTimeFromLastUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (aTimeFromLastUpdate > 0.1)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                {
                    if (SelectedOption != 0)
                    {
                        SelectedOption--;
                    }
                    else
                    {
                        SelectedOption = (ScoreOptions)Enum.GetValues(typeof(ScoreOptions)).GetValue(aCountOfOptions - 1);
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Down) ||
                         GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                {
                    if (SelectedOption != (ScoreOptions)Enum.GetValues(typeof(ScoreOptions)).GetValue(aCountOfOptions - 1))
                    {
                        SelectedOption++;
                    }
                    else
                    {
                        SelectedOption = 0;
                    }
                }
                aTimeFromLastUpdate = 0;
            }
        }

        /// <summary>
        /// Vykreslenie poloziek menu
        /// </summary>
        /// <param name="drawer"></param>
        public void Draw(SpriteBatch drawer)
        {
            float centerOfScreenWidth = aGraphicsDevice.Viewport.Width;
            for (int i = 0; i < aDataToDraw.Count; i++)
            {
                Vector2 stringSize = aScoreFont.MeasureString(aDataToDraw[i]);
                drawer.DrawString(aScoreFont, aDataToDraw[i], new Vector2((int)(centerOfScreenWidth / 2 - stringSize.X / 2), i * stringSize.Y + 50), Color.Green, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            }
            for (int i = 0; i < aPositionsOfOptions.Count; i++)
            {
                Color optCol = Color.White;
                if ((ScoreOptions)Enum.GetValues(typeof(ScoreOptions)).GetValue(i) == SelectedOption)
                {
                    optCol = Color.Red;
                }
                Vector2 stringSize = aFont.MeasureString(aOptionNames[i]);
                drawer.Draw(aOptionFrame, aPositionsOfOptions[i], null, optCol, 0f, Vector2.Zero, 1f, SpriteEffects.None,
                    1f);
                Vector2 stringPos = new Vector2(aPositionsOfOptions[i].X + OptFrameWidth / 2 - stringSize.X / 2,
                    aPositionsOfOptions[i].Y + OptFrameHeight / 2 - stringSize.Y / 2);
                drawer.DrawString(aFont, aOptionNames[i], stringPos, Color.Black, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
            }
        }


        protected virtual void OnOptionSelectedEvent(ScoreMenuOptEventArgs e)
        {
            var handler = OptionSelectedEvent;
            if (handler != null) handler(this, e);
        }
    }
}

package databazysemestralka;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Dominik
 */
public class Core {

    private static final String DB_URL = "jdbc:oracle:thin:@asterix.fri.uniza.sk:1521:orcl";
    private static final String USER = "suroviak3";
    private static final String PASS = "suroviak";

    private static Connection conn;

    public static void connect() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (SQLException ex) {
            Logger.getLogger(Core.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static DefaultTableModel vlastnaQuery(String query) {
        query = query.replaceAll("\\s", " ");
        DefaultTableModel ret = null;

        try {
            Statement stmt = null;

            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            ret = buildTableModel(rs);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(Core.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public static void closeConnection() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Core.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /* public static void vykonajProceduru(String nazovProcedury, String... params)
     {
     try {     
     String st = "call " + nazovProcedury+"(";
            
     for(int i = 0; i < params.length; i++){
     st+= params[i] + 
     }
     CallableStatement cs = conn.prepareCall(st);
     cs.execute();
     } catch (SQLException ex) {
     Logger.getLogger(Core.class.getName()).log(Level.SEVERE, null, ex);
     }
     }*/
    public static DefaultTableModel vykonaj(String procedura, String... params) throws SQLException {
        DefaultTableModel ret = null;
            
            String p = "call " + procedura + "(?";
            for (int i = 0; i < params.length; i++) {
                p += ", ?";
            }
            p += ")";
            
            CallableStatement cs = conn.prepareCall(p);
            cs.registerOutParameter(1, OracleTypes.CURSOR);

            for (int i = 0; i < params.length; i++) {
                cs.setString(i + 2, f(params[i]));
            }

            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject(1);

            ret = buildTableModel(rs);
            rs.close();

        

        return ret;
    }

    public static DefaultTableModel vykonajInsert(String procedura, String... params) throws SQLException {
        DefaultTableModel ret = null;

        String p = "call " + procedura + "(";
        for (int i = 0; i < params.length; i++) {
            p += "? ";
            if (i != params.length - 1) {
                p += ",";
            }
        }
        p += ")";

        CallableStatement cs = conn.prepareCall(p);
        for (int i = 0; i < params.length; i++) {
            cs.setString(i + 1, f(params[i]));
        }
        
        cs.execute();
        return ret;
    }
    
    private static String f(String str) { return str.replaceAll("May", "máj").replaceAll("Jun", "jún").replaceAll("Jul", "júl").replaceAll("Oct", "okt"); }
    
    private static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
        return new DefaultTableModel(data, columnNames);
    }
}

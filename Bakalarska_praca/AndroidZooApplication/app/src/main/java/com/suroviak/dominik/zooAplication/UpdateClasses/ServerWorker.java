package com.suroviak.dominik.zooAplication.UpdateClasses;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Dominik on 15.2.2015.
 */
public class ServerWorker {


    public ServerWorker() {

    }


    public boolean isDomainValidDomain(String paDomain) {
        try {
            URL lpURL = new URL(paDomain);

        } catch (MalformedURLException e) {
            return false;
        }

        return true;
    }

    public String downloadDataFromURLToUrlComputeDirectory(String paDomainOfFile, String paPathOfDownloadFileInDevice) throws FileNotFoundException, IOException {
        URL lpURL = new URL(paDomainOfFile);


        int downloadedSize = 0;
        int filesize = 0;
        HttpURLConnection urlConnection = (HttpURLConnection) lpURL.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);

        //connect
        urlConnection.connect();

        //create a new file, to save the downloaded file
        File file = new File(paPathOfDownloadFileInDevice);

        Log.d("MyAppUpdate", file.getAbsolutePath());

        FileOutputStream fileOutput = new FileOutputStream(file);

        //Stream used for reading the data from the internet
        InputStream inputStream = urlConnection.getInputStream();

        //this is the total size of the file which we are downloading

        filesize = urlConnection.getContentLength();
        //create a buffer...
        byte[] buffer = new byte[1024];
        int bufferLength = 0;
        Log.d("MyAppUpdateActivity", "Downloading update ... ");
        while ((bufferLength = inputStream.read(buffer)) > 0) {
            fileOutput.write(buffer, 0, bufferLength);
            downloadedSize += bufferLength;

        }
        //close the output stream when complete //

        fileOutput.close();
        return "";
    }


    public long getContentsLength(String paDomainOfFile) throws IOException {
        URL lpURL = new URL(paDomainOfFile);
        return lpURL.getFile().length();

    }

}

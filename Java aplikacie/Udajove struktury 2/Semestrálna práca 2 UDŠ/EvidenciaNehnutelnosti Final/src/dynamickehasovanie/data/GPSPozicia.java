/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.data;

import static dynamickehasovanie.data.ImplementatorZaznam.aInstance;
import dynamickehasovanie.struktura.TypeInfo;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author Dominik
 */
public class GPSPozicia extends Zaznam {

    private static final int VELKOST_ZEM_SIRKA = TypeInfo.sizeof(Character.class);
    private static final int VELKOST_ZEM_DLZKA = TypeInfo.sizeof(Character.class);
    private static final int VELKOST_ZEM_SIRKA_HODNOTA = TypeInfo.sizeof(Double.class);
    private static final int VELKOST_ZEM_DLZKA_HODNOTA = TypeInfo.sizeof(Double.class);
    private char aZemepisnaSirka;
    private char aZemepisnaDlzka;
    private double aZemSirkaHodnota;
    private double aZemepisnaDlzkaHodnota;

    public GPSPozicia(char aZemepisnaSirka, char aZemepisnaDlzka, double aZemSirkaHodnota, double aZemepisnaDlzkaHodnota) {
        this.aZemepisnaSirka = aZemepisnaSirka;
        this.aZemepisnaDlzka = aZemepisnaDlzka;
        this.aZemSirkaHodnota = aZemSirkaHodnota;
        this.aZemepisnaDlzkaHodnota = aZemepisnaDlzkaHodnota;
    }

    private GPSPozicia() {
        super();
    }

    public void setaZemepisnaSirka(char aZemepisnaSirka) {
        this.aZemepisnaSirka = aZemepisnaSirka;
    }

    public void setaZemepisnaDlzka(char aZemepisnaDlzka) {
        this.aZemepisnaDlzka = aZemepisnaDlzka;
    }

    public void setaZemSirkaHodnota(double aZemSirkaHodnota) {
        this.aZemSirkaHodnota = aZemSirkaHodnota;
    }

    public void setaZemepisnaDlzkaHodnota(double aZemepisnaDlzkaHodnota) {
        this.aZemepisnaDlzkaHodnota = aZemepisnaDlzkaHodnota;
    }

    public char getaZemepisnaSirka() {
        return aZemepisnaSirka;
    }

    public char getaZemepisnaDlzka() {
        return aZemepisnaDlzka;
    }

    public double getaZemSirkaHodnota() {
        return aZemSirkaHodnota;
    }

    public double getaZemepisnaDlzkaHodnota() {
        return aZemepisnaDlzkaHodnota;
    }

    public static ImplementatorZaznam getImplmentatorTohtoZaznamu() {
        return GPSPozicia.ImplementatorGPSPozicia.getInstance();
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GPSPozicia other = (GPSPozicia) obj;
        if (this.aZemepisnaSirka != other.aZemepisnaSirka) {
            return false;
        }
        if (this.aZemepisnaDlzka != other.aZemepisnaDlzka) {
            return false;
        }
        if (Double.doubleToLongBits(this.aZemSirkaHodnota) != Double.doubleToLongBits(other.aZemSirkaHodnota)) {
            return false;
        }
        if (Double.doubleToLongBits(this.aZemepisnaDlzkaHodnota) != Double.doubleToLongBits(other.aZemepisnaDlzkaHodnota)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return super.aJePlatny.getA_Popis()+" "+String.format("%.5f",aZemepisnaDlzkaHodnota) + " " + aZemepisnaDlzka + "; " + String.format("%.5f",aZemSirkaHodnota) + " " + aZemepisnaSirka;
    }

    private static class ImplementatorGPSPozicia extends ImplementatorZaznam {

        private ImplementatorGPSPozicia(Class aImplementatorForClass) {
            super(aImplementatorForClass);
        }

        @Override
        public Zaznam toZaznam(byte[] paBinDataZaznam) {
            ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(paBinDataZaznam);
            DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
            GPSPozicia ret = new GPSPozicia();
            try {
                char platnost = hlpInStream.readChar();
                if (platnost == Zaznam.Platnost.PLATNY.get_Znacka_V_Subore()) {
                    ret.aJePlatny = Zaznam.Platnost.PLATNY;
                } else {
                    ret.aJePlatny = Zaznam.Platnost.NEPLATNY;
                }
                ret.aZemepisnaDlzkaHodnota = hlpInStream.readDouble();
                ret.aZemepisnaDlzka = hlpInStream.readChar();
                ret.aZemSirkaHodnota = hlpInStream.readDouble();
                ret.aZemepisnaSirka = hlpInStream.readChar();

            } catch (IOException ex) {
                ex.printStackTrace();
                throw new IllegalStateException("Error during conversion from byte array.");
                
            }
            return ret;
        }

        @Override
        public byte[] toByteArray(Zaznam paZaznam) {
            ByteArrayOutputStream hlpByteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
            if (paZaznam instanceof GPSPozicia) {
                GPSPozicia os = (GPSPozicia) paZaznam;
                try {
                    hlpOutStream.writeChar(os.aJePlatny.get_Znacka_V_Subore());

                    hlpOutStream.writeDouble(os.aZemepisnaDlzkaHodnota);
                    hlpOutStream.writeChar(os.aZemepisnaDlzka);
                    hlpOutStream.writeDouble(os.aZemSirkaHodnota);
                    hlpOutStream.writeChar(os.aZemepisnaSirka);

                    return hlpByteArrayOutputStream.toByteArray();

                } catch (IOException e) {
                    throw new IllegalStateException("Error during conversion to byte array.");
                }
            }
            throw new IllegalArgumentException("Implementator nie je implementatorom "
                    + "pre genericky typ: " + paZaznam.getClass().getSimpleName()
                    + " zadany pri vytvarani dynamickeho hashu"
                    + "Implementator je implementatorom pre objekty typu: "
                    + super.aImplementatorForClass.getSimpleName());
        }

        public static ImplementatorZaznam getInstance() {
            if (aInstance != null) {
                return aInstance;
            } else {
                return new ImplementatorGPSPozicia(GPSPozicia.class);
            }
        }

        @Override
        public int get_Velkost_Zaznamu_V_Bajtoch() {
            return Zaznam.getVelkostObjektuVBajtoch() + VELKOST_ZEM_DLZKA + VELKOST_ZEM_DLZKA_HODNOTA + VELKOST_ZEM_SIRKA + VELKOST_ZEM_SIRKA_HODNOTA;
        }

        @Override
        public Zaznam vytvorNeplatnyZaznam() {
            GPSPozicia gps = new GPSPozicia();
            gps.setPlatny(Zaznam.Platnost.NEPLATNY);
            return gps;
        }
    }
}

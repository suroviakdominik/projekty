/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Miestnost;

import wof00.Hra.Hrac;
import wof00.Osoby.IOsoba;
import wof00.Predmety.IPredmet;

/**
 *
 * @author suroviak3
 */
public interface IMiestnost {

    public IMiestnost dajVychod(String paSmer);

    public void vlozPredmet(IPredmet paPredmet);

    public void nastavVychody(IMiestnost paMiestnost);

    public String dajPopis();

    public String dajPopisVychodov();

    public String getaNazov();

    public String dajPredmety();

    public void vypisInfoMiestnosti();

    public IPredmet zoberPredmet(String paNazov);

    public boolean maPredmety();

    public IPredmet zistiPredmet(String paNazov);

    public void zmazVychod(String paMiestnost);

    public void vlozOsobu(IOsoba osoba);

    public void vymazOsobu(String paMenoOsoby);

    public String vypisOsoby();

    public int dlzkaZatNajdNajkrCesZoSucVrch();

    public IMiestnost dajPredchodcuVrchola();

    public IOsoba dajOsobu(String paMeno);

    public void setaDlzkaZoSucasnejDoTejtoMiestnosti(int aDlzkaZoSucasnejDoTejtoMiestnosti);

    public void setaPredchodca(IMiestnost aPredchodca);

    public void vymazVychod(IMiestnost paMiestnost);

    public boolean jePristupna(Hrac paHrac);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raid;

import java.io.File;

/**
 *
 * @author Dominik
 */
public class RAID {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       FileWorker.writeDataToFilesRaid3(new File("./data1.txt"), new File("./data2.txt"),new File("./par.txt"),"Dominik Suroviak");
       System.out.println(FileWorker.readDataFromRaidFiles(new File("./data1.txt"), new File("./data2.txt")));
       System.out.println(FileWorker.obnovDataVPripadeStraty(new File("./data1.txt"),new File("./par.txt"),true));
    }
    
}

package strom;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//trieda ma za ulohu viest evidenciu objektov a aktualne informacie o danej evidencii
//posledna zmena pridane do interfacu IObjekt equals metoda

import Objekty.*;
import java.util.*;
import java.awt.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Trieda strom predstavuje ADŠ binárny strom implementovaný pomocou binárneho
 * súboru
 *
 * @author Dominik
 */
public class Strom {

    /**
     * Vnorená trieda serializácia predstavuje pomocnú triedu, ktorá obsahuje
     * základné metódy na prácu s binárnym súborom ako zápis do súbor, načítanie
     * zo súboru...
     */
    private class Serializacia {

        private File aPodadresar;

        /**
         * Konštruktor vytvorí inštanciu triedy serializacia. Aok parameter
         * vstupuje názov podadresára, kde sa nachádzajú binárne súbory s
         * ktorými budeme pracovať.
         *
         * @param paNazovPodadresara
         */
        public Serializacia(String paNazovPodadresara) {
            aPodadresar = new File(paNazovPodadresara);
            if (!aPodadresar.exists()) {
                aPodadresar.mkdir();//vytvori novy priecinok v pricinku programu kde budu ulozene prvky frontu
            }

        }

        /**
         * metóda zapíše do binárneho súboru objekt typu IObjekt
         *
         * @param paVkladanyPrvok - objekt Typu IObjekt, ktorý bude zapísaný do
         * súboru.
         * @param paCisloSuboru - cislo súboru, ktoré predstavuje poradie súboru
         * uložené v poli
         * @return -vráti objekt typu File, ktorý predstavuje odkaz na zapísaný
         * súbor
         * @throws IOException - v prípade,že sa zápis nepodaril, vyhodí výnimku
         */
        private File zapisDoSuboru(IObjekt paVkladanyPrvok, int paCisloSuboru) throws IOException {

            try {

                File subor = new File(aPodadresar, String.format("%04d", paCisloSuboru) + ".txt");
                subor.createNewFile();

                FileOutputStream fileOut = new FileOutputStream(subor);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);

                out.writeObject(paVkladanyPrvok);
                fileOut.close();
                out.close();
                return subor;
            } catch (IOException ex) {
                throw ex;
            }
        }

        /**
         * metóda načita z binárneho súboru objekt typu IObjekt.
         *
         * @param paSubor --súbor z ktorého má byť objekt načitaný
         * @return -- vráti načitaný objekt
         * @throws IOException
         * @throws ClassNotFoundException
         */
        private IObjekt nacitanieZoSuboru(File paSubor) throws IOException, ClassNotFoundException {
            IObjekt lpNavrat = null;
            FileInputStream fileIn = new FileInputStream(paSubor);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            lpNavrat = (IObjekt) in.readObject();
            fileIn.close();
            in.close();

            return lpNavrat;

        }
    }

    private final int MAX_POCET_PRVKOV = 10000;
    private File[] aPrvkyStromu;
    private int aAktualnyPocetPrvkov;
    private Serializacia aSerialDeserial;
    private int aAktualnyOtecIndex = 0;

    /**
     * Konštruktor má za úlohu inicializovať počiatočný stav BinárnehoStromu.
     * Aktuálny počet prvkov nastaví na 0. Vytvorí nové pole, kde budú ukladané
     * objekty typu File predstavujúce prvky Stromu. Takisto vytvorí objekt typu
     * Serializacia.
     */
    public Strom() {

        aAktualnyPocetPrvkov = 0;
        aPrvkyStromu = new File[MAX_POCET_PRVKOV];
        aSerialDeserial = new Serializacia("Binarny strom prvky");

    }

    /**
     * metóda zobrazí aktuálny prvok
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void citajAktualny() throws IOException, ClassNotFoundException {
        aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]).zobraz();
    }

    /**
     * metoda vracia hodnotu ci je strom prazdny
     *
     * @return TRUE-prazdny strom, FALSE-nie je prazdny
     */
    public boolean jePrazdny() {
        return aAktualnyPocetPrvkov == 0;
    }

    /**
     * metóda nastaví aktuálne vybraný prvok v Strome na jeho laveho syna.
     *
     * @return TRUE-ak má ľavého syna, FALSE- ak nemá ľavého syna
     */
    public boolean set_AktualnyOtecNaLavy() {
        if (aPrvkyStromu[aAktualnyOtecIndex * 2] != null) {
            aAktualnyOtecIndex = aAktualnyOtecIndex * 2;
            return true;
        }
        return false;
    }

    /**
     * metóda nastaví aktuálne vybraný prvok v Strome na jeho pravého syna.
     *
     * @return TRUE-ak má pravého syna, FALSE- ak nemá praeho syna
     */
    public boolean set_AktualnyOtecNaPravy() {
        if (aPrvkyStromu[aAktualnyOtecIndex * 2 + 1] != null) {
            aAktualnyOtecIndex = aAktualnyOtecIndex * 2 + 1;
            return true;
        }
        return false;
    }

    /**
     * metóda nastaví aktuálne vybraný prvok v Strome na jeho oca.
     *
     * @return TRUE-ak má oca, FALSE- ak nemá oca
     */
    public boolean set_AktualnyOtecNaPredka() {
        if (aPrvkyStromu[aAktualnyOtecIndex / 2] != null) {
            aAktualnyOtecIndex = aAktualnyOtecIndex / 2;
            return true;
        }
        return false;
    }

    /**
     * metóda zruší aktuálne vytvorený Strom.
     */
    public void zrus() {
        aAktualnyPocetPrvkov = 0;
        aPrvkyStromu = null;
        aPrvkyStromu = new File[MAX_POCET_PRVKOV];
    }

    /**
     * metóda zistí, či je aktuálne vybraný prvok koreňom alebo nie je.
     *
     * @return TRUE-je Koreňom, FALSE- nie je koreňom
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean jeKorenom() throws IOException, ClassNotFoundException {
        return aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[1]).equals(aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]));
    }

    /**
     * metóda zistí, či je aktuálne vybraný prvok listom alebo nie je.
     *
     * @return TRUE-je listom, FALSE- nie je listom
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean jeListom() throws IOException, ClassNotFoundException {

        if (aPrvkyStromu[aAktualnyOtecIndex * 2] == null && aPrvkyStromu[aAktualnyOtecIndex * 2 + 1] == null) {
            return true;
        }

        return false;

    }

    /**
     * metóda hľadá v strome prvok zadaný ako Parameter. Ak ho nájde, tak vráti
     * jeho index v poli PrvkovStromu. Ak ho nenájde, tak vráti -1.
     *
     * @param paObjekt
     * @return -1 ak ho nenájde, index v poli prvkov ak ho nájde.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public int hladaj(IObjekt paObjekt) throws IOException, ClassNotFoundException {
        int r = -1;
        int volnaPozicia = 0;
        int pocetPrvkovFrontu = 0;
        int frontPrvkov[] = new int[aAktualnyPocetPrvkov];
        if (aPrvkyStromu[1] != null) {
            frontPrvkov[volnaPozicia] = 1;
            pocetPrvkovFrontu++;
            volnaPozicia++;
        } else {
            return -1;
        }

        while (pocetPrvkovFrontu > 0) {
            int vybranyPrvokZFrontu = frontPrvkov[0];
            volnaPozicia--;
            pocetPrvkovFrontu--;

            for (int i = 1; i <= pocetPrvkovFrontu; i++) {
                frontPrvkov[i - 1] = frontPrvkov[i];
            }

            if (aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[vybranyPrvokZFrontu]).equals(paObjekt)) {
                return frontPrvkov[0];
            }

            if (aPrvkyStromu[vybranyPrvokZFrontu * 2] != null) {
                frontPrvkov[volnaPozicia] = vybranyPrvokZFrontu * 2;
                volnaPozicia++;
                pocetPrvkovFrontu++;
            }
            if (aPrvkyStromu[vybranyPrvokZFrontu * 2 + 1] != null) {
                frontPrvkov[volnaPozicia] = vybranyPrvokZFrontu * 2 + 1;
                volnaPozicia++;
                pocetPrvkovFrontu++;
            }
        }
        return r;
    }

    /**
     * Prida do stromu novy koren, ak este ziadny koren nie je pridany.
     *
     * @param paPridavanyPrvok-- prvok objekt typu IObjekt, ktorý sa stane
     * koreňom Ak strom ešte nemá koreň.
     * @throws IOException
     */
    public void pridajKoren(IObjekt paPridavanyPrvok) throws IOException {
        if (aPrvkyStromu[1] == null) {
            aPrvkyStromu[1] = aSerialDeserial.zapisDoSuboru(paPridavanyPrvok, 1);
            aAktualnyPocetPrvkov++;
            aAktualnyOtecIndex = 1;
        } else {
            System.out.println("Koren uz existuje. Nemozno ho pridat.");
        }
    }

    /**
     * Pridá do stromu k aktuálne vybranému prvku pravý list, ak aktuálne
     * vybraný prvok zatiaľ nemá pravého syna.
     *
     * @param paPridavanyPrvok --objekt typu IObjekt. Prvok, ktorý bude pridaný.
     * @return --true ak sa prvok podarilo pridať, false ak sa nepodarilo pridať
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean pridajPravyList(IObjekt paPridavanyPrvok) throws IOException, ClassNotFoundException {
        boolean boloZapisane = false;
        int indexOca = hladaj(aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]));
        if (indexOca != -1) {
            if (aPrvkyStromu[2 * aAktualnyOtecIndex + 1] == null) {
                aPrvkyStromu[2 * aAktualnyOtecIndex + 1] = aSerialDeserial.zapisDoSuboru(paPridavanyPrvok, 2 * aAktualnyOtecIndex + 1);
                boloZapisane = true;
            }
        }

        if (!boloZapisane) {
            return false;
        } else {
            aAktualnyPocetPrvkov++;
            return true;
        }
    }

    /**
     * Pridá do stromu k aktuálne vybranému prvku ľavý list, ak aktuálne vybraný
     * prvok zatiaľ nemá ľavého syna.
     *
     * @param paPridavanyPrvok --objekt typu IObjekt. Prvok, ktorý bude pridaný.
     * @return --true ak sa prvok podarilo pridať, false ak sa nepodarilo pridať
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public boolean pridajLavyList(IObjekt paPridavanyPrvok) throws IOException, ClassNotFoundException {
        boolean boloZapisane = false;
        int indexOca = hladaj(aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]));
        if (indexOca != -1) {
            if (aPrvkyStromu[2 * aAktualnyOtecIndex] == null) {
                aPrvkyStromu[2 * aAktualnyOtecIndex] = aSerialDeserial.zapisDoSuboru(paPridavanyPrvok, 2 * aAktualnyOtecIndex);
                boloZapisane = true;
            }
        }

        if (!boloZapisane) {
            return false;
        } else {
            aAktualnyPocetPrvkov++;
            return true;
        }
    }

    /**
     * Odoberie zo stromu pravý list aktuálne vybraného prvku, ak aktuálne
     * vybraný prvok má pravý list.
     *
     * @return --objekt typu IObjekt, ktorý bol odobraný.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public IObjekt odoberPravyList() throws IOException, ClassNotFoundException {
        IObjekt odoberany = null;
        int indexOca = hladaj(aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]));

        if ((aPrvkyStromu[2 * aAktualnyOtecIndex + 1] != null) && (aPrvkyStromu[(2 * aAktualnyOtecIndex + 1) * (2 * aAktualnyOtecIndex + 1)] == null)) {
            odoberany = aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[2 * aAktualnyOtecIndex + 1]);
            aPrvkyStromu[2 * aAktualnyOtecIndex + 1].delete();
            aPrvkyStromu[2 * aAktualnyOtecIndex + 1] = null;
            aAktualnyPocetPrvkov--;
            return odoberany;

        }

        return null;
    }

    /**
     * Odoberie zo stromu ľavý list aktuálne vybraného prvku, ak aktuálne
     * vybraný prvok má ľavý list.
     *
     * @return --objekt typu IObjekt, ktorý bol odobraný.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public IObjekt odoberLavyList() throws IOException, ClassNotFoundException {
        IObjekt odoberany = null;
        int indexOca = hladaj(aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aAktualnyOtecIndex]));

        if ((aPrvkyStromu[2 * aAktualnyOtecIndex] != null) && (aPrvkyStromu[(2 * aAktualnyOtecIndex) * (2 * aAktualnyOtecIndex)] == null)) {
            odoberany = aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[2 * aAktualnyOtecIndex]);
            aPrvkyStromu[2 * aAktualnyOtecIndex].delete();
            aPrvkyStromu[2 * aAktualnyOtecIndex] = null;
            aAktualnyPocetPrvkov--;
            return odoberany;

        }

        return null;
    }

    /**
     * metóda vytvóri objekt zo súboru. Používa sa pri načitaní prvokv binárneho
     * stromu zo súboru.
     *
     * @param PodAdresar--podadresár kde sú uložené prvky súboru
     * @param paNazov--názov súboru, ktorý predstavuje názov uloženého súboru s
     * prvkom.
     * @return --objekt typu IObjekt vytvorený zo ´súboru
     * @throws IOException
     */
    private IObjekt vytvorObjektZoSuboru(File PodAdresar, String paNazov) throws IOException {
        IObjekt navrat = null;

        if (paNazov.charAt(0) == 'A') {

            navrat = new Automobil();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);

        } else if (paNazov.charAt(0) == 'K') {

            navrat = new Kniha();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);

        } else {
            navrat = new Osoba();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);
        }

        return navrat;
    }

    /**
     * metoda vrati true ak ma aktualny prvok pravy list
     */
    public boolean maAktualnyPravyList() {
        if (aPrvkyStromu[aAktualnyOtecIndex * 2 + 1] != null) {
            return true;
        }
        return false;
    }

    /**
     * metoda vrati true ak ma aktualny prvok lavy list
     */
    public boolean maAktualnyLavyList() {
        if (aPrvkyStromu[aAktualnyOtecIndex * 2] != null) {
            return true;
        }
        return false;
    }

    /**
     * metóda nájde v priečinku z uloženým stromom, prvok, ktorý mal v poli
     * zadané poradové čislo.
     *
     * @param paNazvySuborov--názvy vštkých súborov, ktoré predstavujú uložené
     * prvky bin. súboru
     * @param paPoradoveCislo--index v poli kde sa prvok nachádzal pred
     * uložením.
     * @return
     */
    private String najdiSuborSporadovymCislom(String[] paNazvySuborov, int paPoradoveCislo)//lebo nevieme ako su ulozene subory v priecinku usporiadane
    {

        String podRetazecCisloSuboru;
        for (String paNazvySuborov1 : paNazvySuborov) {
            podRetazecCisloSuboru = paNazvySuborov1.substring(1, 5);
            if (Integer.parseInt(podRetazecCisloSuboru) == paPoradoveCislo) {
                return paNazvySuborov1;
            }
        }
        return null;
    }

    /**
     * Metóda načíta prvky, ktoré sme si predtým uložili do súboru z tohoto
     * súboru do zoznamu. Staré prvky sa stratia.
     *
     * @throws FileNotFoundException-chyba ak neboli zatiaľ uložené žiadne prvky
     * alebo ak sa niektorý zo súborov nepodarilo načitať, lebo ho používateľ
     * upravil v textovom editore.
     * @throws IOException-chyba, ktorá nastala v inom prípade
     */
    public void nacitajPrvkyZoSuboru() throws FileNotFoundException, IOException, NullPointerException {
        File podadresarUlozPrvkyStromu = new File("Strom ulozisko");
        if (!podadresarUlozPrvkyStromu.exists() || podadresarUlozPrvkyStromu.list().length == 0) {

            throw new FileNotFoundException("Zatial neboli ulozene ziadne prvky");
        }
        zrus();
        String[] nazvyUlozSuborov = podadresarUlozPrvkyStromu.list();
        if (nazvyUlozSuborov.length == 0) {
            throw new FileNotFoundException();
        }
        int boloNacitanzych = 0;
        int IndexKamZapisat;
        while (boloNacitanzych < nazvyUlozSuborov.length) {
            IndexKamZapisat = Integer.parseInt(nazvyUlozSuborov[boloNacitanzych].substring(1, 5));
            {
                aPrvkyStromu[IndexKamZapisat] = aSerialDeserial.zapisDoSuboru(vytvorObjektZoSuboru(podadresarUlozPrvkyStromu, najdiSuborSporadovymCislom(nazvyUlozSuborov, IndexKamZapisat)), IndexKamZapisat);
                aAktualnyPocetPrvkov++;
                boloNacitanzych++;
            }

        }
        aAktualnyOtecIndex = 1;
    }

    /**
     * Metóda uloží prvky, ktoré sa aktuálne nachádzaju vo fronte do súboru.
     * Prvky, ktoré boli uložené predtým sa stratia(vymažú).
     *
     * @throws FileNotFoundException- chyba, ak sa nepodarilo vytvoriť súbor pre
     * zápis, kvôli tomu, že taký súbor už existuje.
     */
    public void zapisPrvkyDoSuboru() throws FileNotFoundException, IOException, ClassNotFoundException {

        File podadresarUlozPrvkyStromu = new File("Strom ulozisko");
        if (!podadresarUlozPrvkyStromu.exists()) {
            podadresarUlozPrvkyStromu.mkdir();
        } else if (podadresarUlozPrvkyStromu.list().length > 0) {
            File prvky[] = podadresarUlozPrvkyStromu.listFiles();
            for (int i = 0; i < prvky.length; i++) {
                prvky[i].delete();
            }
        }
        int kolkoBoloZapisanych = 0;
        for (int i = 1; kolkoBoloZapisanych < aAktualnyPocetPrvkov; i++) {
            if (aPrvkyStromu[i] != null) {
                kolkoBoloZapisanych++;
                aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[i]).zapisDoSuboru(podadresarUlozPrvkyStromu, i);
            }
        }
    }

    /**
     * metóda zobrazí na plátne binárny strom
     *
     * @param gg
     * @param poziciaX --pozicia x kde sa zacina vykreslovat
     * @param poziciaY --pozicia y kde sa zacina vykreslovat
     * @param rozlisenieOknaVyska--rozlisenie hlavneho okna vyska
     * @param RozliseniieOknaSirka --rozlisenie hlavneho okna sirka
     */
    public void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY, int rozlisenieOknaVyska, int RozliseniieOknaSirka) {

        //gg.drawString( this.toString(), poziciaX, poziciaY );
        //gg.drawString( new Date().toString(), poziciaX + 20, poziciaY + 50 );
        gg.drawRect(RozliseniieOknaSirka / 2 - 50, poziciaY - 5, RozliseniieOknaSirka / 12, 20);
        gg.drawString("Pocet prvkov = " + aAktualnyPocetPrvkov, RozliseniieOknaSirka / 2 - 40, poziciaY + 10);

    }

    public void znazorni(Graphics2D gg, int riadok, int stlpec) //pozicai x je stlpex, pozicia y je riadok vykreslovania,metoda je rekurzivna
    {
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
        zobrazRec(gg, 1, riadok, stlpec, 128);

    }

    //vykresli strom na plochu
    private void zobrazRec(Graphics2D gg, int vrchol, int poziciaR, int poziciaS, int poziciaSZmena) {
        if (aPrvkyStromu[vrchol] != null) {
            try {
                if (vrchol == aAktualnyOtecIndex) {
                    gg.setColor(Color.RED);
                } else {
                    gg.setColor(Color.BLACK);
                }
                String retNaVyp = aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[vrchol]).getPopis();
                gg.drawString("" + retNaVyp, poziciaS - gg.getFont().getSize() * retNaVyp.length() / 4, poziciaR);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo zobrazit. Neda sa nacitat z binarneho suboru.", "ERROR=Metoda zobrazRect Strom", JOptionPane.ERROR_MESSAGE);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Objekt sa nepodarilo zobrazit. Neda sa nacitat z binarneho suboru.", "ERROR=Metoda zobrazRect Strom", JOptionPane.ERROR_MESSAGE);
            }

            gg.setColor(Color.BLUE);

            gg.drawLine(poziciaS + 5, poziciaR + 5, poziciaS - poziciaSZmena + 5, poziciaR + 28);

            zobrazRec(gg, vrchol * 2, poziciaR + 40, poziciaS - poziciaSZmena, poziciaSZmena / 2);
            gg.setColor(Color.red);
            gg.drawLine(poziciaS + 5, poziciaR + 5, poziciaS + poziciaSZmena + 5, poziciaR + 28);
            zobrazRec(gg, vrchol * 2 + 1, poziciaR + 40, poziciaS + poziciaSZmena, poziciaSZmena / 2);

        }
    }

    /**
     * meoda prevedie na danom strome všetky prehliadky(IN,Pre,Post a Level
     * Order) a zobrazi ich na platno.
     *
     * @param gg
     * @param riadok
     * @param sposob
     */
    public void prehliadka(Graphics2D gg, int riadok, int sposob) {
        String ret = "";
        switch (sposob) {
            case 1:
                ret = "PreOrder  :  " + prechodPre(1);
                break;
            case 2:
                ret = "InOrder   :  " + prechodIN(1);
                break;
            case 3:
                ret = "PostOrder :  " + prechodpost(1);
                break;
            case 4:
                ret = "LevelOrder:  " + levelOrder(1);
                break;

        }
        gg.setColor(Color.BLUE);
        gg.drawRect(8, riadok - 20, 600, 25);
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 15));
        gg.setColor(Color.BLACK);
        gg.drawString(ret, 10, riadok);
    }

    /**
     * metóda na pre Order
     *
     * @param paKoren--Koren stromu
     * @return --vracia reťazec typu String, ktorý obsahuje poradie pre order
     */
    private String prechodPre(int aKoren) {
        String r = "";
        if (aPrvkyStromu[aKoren] != null) {
            try {
                r = r + aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[aKoren]).getPopis() + " ";
            } catch (IOException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            }
            r += prechodPre(aKoren * 2);
            r += prechodPre(aKoren * 2 + 1);
        }
        return r;

    }

    /**
     * metóda na IN Order
     *
     * @param paKoren--Koren stromu
     * @return --vracia reťazec typu String, ktorý obsahuje poradie In order
     */
    private String prechodIN(int paKoren) {
        String r = "";
        if (aPrvkyStromu[paKoren] != null) {

            r += prechodIN(paKoren * 2);
            try {
                r = r + aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[paKoren]).getPopis() + " ";
            } catch (IOException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            }
            r += prechodIN(paKoren * 2 + 1);
        }
        return r;
    }

    /**
     * metóda na post order
     *
     * @param paKoren--Koren stromu
     * @return --vracia reťazec typu String, ktorý obsahuje poradie Post order
     */
    private String prechodpost(int paKoren) {
        String r = "";
        if (aPrvkyStromu[paKoren] != null) {
            r += prechodpost(paKoren * 2);
            r += prechodpost(paKoren * 2 + 1);
            try {
                r = r + aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[paKoren]).getPopis() + " ";
            } catch (IOException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return r;
    }

    /**
     * metóda na pre Order
     *
     * @param paKoren--Koren stromu
     * @return --vracia reťazec typu String, ktorý obsahuje poradie level order
     */
    private String levelOrder(int paKoren) {
        String r = "";
        int volnaPozicia = 0;
        int pocetPrvkovFrontu = 0;
        int frontPrvkov[] = new int[aAktualnyPocetPrvkov];
        if (aPrvkyStromu[paKoren] != null) {
            frontPrvkov[volnaPozicia] = paKoren;
            pocetPrvkovFrontu++;
            volnaPozicia++;
        } else {
            return r;
        }

        while (pocetPrvkovFrontu > 0) {
            int vybranyPrvokZFrontu = frontPrvkov[0];
            volnaPozicia--;
            pocetPrvkovFrontu--;

            for (int i = 1; i <= pocetPrvkovFrontu; i++) {
                frontPrvkov[i - 1] = frontPrvkov[i];
            }
            try {
                r += aSerialDeserial.nacitanieZoSuboru(aPrvkyStromu[vybranyPrvokZFrontu]).getPopis() + " ";
            } catch (IOException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Strom.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (aPrvkyStromu[vybranyPrvokZFrontu * 2] != null) {
                frontPrvkov[volnaPozicia] = vybranyPrvokZFrontu * 2;
                volnaPozicia++;
                pocetPrvkovFrontu++;
            }
            if (aPrvkyStromu[vybranyPrvokZFrontu * 2 + 1] != null) {
                frontPrvkov[volnaPozicia] = vybranyPrvokZFrontu * 2 + 1;
                volnaPozicia++;
                pocetPrvkovFrontu++;
            }

        }
        return r;
    }

}

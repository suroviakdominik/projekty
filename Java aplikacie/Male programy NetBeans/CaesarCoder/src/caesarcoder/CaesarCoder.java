/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caesarcoder;

import java.nio.CharBuffer;

/**
 *
 * @author Dominik
 */
public abstract class CaesarCoder {

    public static String encode(String paTextToEcode, int paPosunutie) {
        String ret = "";
        StringBuilder buf = new StringBuilder(paTextToEcode);
        for (int i = 0; i < buf.length(); i++) {
            if (Character.isLetter(buf.charAt(i))) {
                if (Character.isLowerCase(buf.charAt(i))) {
                   buf.setCharAt(i,(char)('a'+(buf.charAt(i) + paPosunutie-'a')%26));
                }
                else
                {
                    buf.setCharAt(i,(char)('A'+ (buf.charAt(i) + paPosunutie-'A')%26));
                }
            }
        }

        return buf.toString();
    }

    public static String decode(String paTextToDEcode, int paPosunutie) {
        String ret = "";
        StringBuilder buf = new StringBuilder(paTextToDEcode);
        for (int i = 0; i < buf.length(); i++) {
            if (Character.isLetter(buf.charAt(i))) {
                if (Character.isLowerCase(buf.charAt(i))) {
                   buf.setCharAt(i,(char)('z'-('z'-buf.charAt(i) + paPosunutie)%26));
                }
                else
                {
                    buf.setCharAt(i,(char)('Z'- ('Z'-buf.charAt(i) + paPosunutie)%26));
                }
            }
        }

        return buf.toString();
    }

    
}

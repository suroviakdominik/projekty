/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.struktura;

import dynamickehasovanie.data.ImplementatorZaznam;
import dynamickehasovanie.data.Zaznam;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class Blok implements Iterable<Zaznam> {

    protected static int VELKOST_HLAVICKY_BLOKU = 2 * TypeInfo.sizeof(Long.class);

    protected long aCisloBloku;
    protected int aMaxPocetZaznamovVBloku;
    protected ImplementatorZaznam aImplementatorZaznamu;
    protected LinkedList<Zaznam> aZoznamZaznamovBloku;
    protected boolean aSomUlozenyVPreplnujucomSubore;
    protected long aCisloZretazenehoBloku;

    public Blok(int aMaxPocetZaznamovVBloku, ImplementatorZaznam aImplementatorZaznamu, boolean paNachazaSaVPreplSubore) {
        this.aMaxPocetZaznamovVBloku = aMaxPocetZaznamovVBloku;
        this.aImplementatorZaznamu = aImplementatorZaznamu;
        this.aZoznamZaznamovBloku = new LinkedList<>();
        this.aCisloBloku = -1;
        this.aSomUlozenyVPreplnujucomSubore = paNachazaSaVPreplSubore;
        this.aCisloZretazenehoBloku = -1;
    }

    public Blok(Blok paBlok) {
        this.aCisloBloku = paBlok.aCisloBloku;
        this.aMaxPocetZaznamovVBloku = paBlok.aMaxPocetZaznamovVBloku;
        this.aImplementatorZaznamu = paBlok.aImplementatorZaznamu;
        this.aZoznamZaznamovBloku = new LinkedList<>(paBlok.aZoznamZaznamovBloku);
        this.aSomUlozenyVPreplnujucomSubore = paBlok.aSomUlozenyVPreplnujucomSubore;
        this.aCisloZretazenehoBloku = paBlok.aCisloZretazenehoBloku;
    }
    
    /**
     * nevvytvori novy blok v pamati ako copy konstruktor ale iba skopiruje atributy
     * @param paBlok 
     */
    public void copyBlok(Blok paBlok){
        this.aCisloBloku = paBlok.aCisloBloku;
        this.aMaxPocetZaznamovVBloku = paBlok.aMaxPocetZaznamovVBloku;
        this.aImplementatorZaznamu = paBlok.aImplementatorZaznamu;
        this.aZoznamZaznamovBloku = new LinkedList<>(paBlok.aZoznamZaznamovBloku);
        this.aSomUlozenyVPreplnujucomSubore = paBlok.aSomUlozenyVPreplnujucomSubore;
        this.aCisloZretazenehoBloku = paBlok.aCisloZretazenehoBloku;
    }

    public long getaCisloBloku() {
        return aCisloBloku;
    }

    public long getaCisloZretazenehoBloku() {
        return aCisloZretazenehoBloku;
    }

    public int getaMaxPocetZaznamovVBloku() {
        return aMaxPocetZaznamovVBloku;
    }

    public int getVelkostBlokuVBajtoch() {
        return VELKOST_HLAVICKY_BLOKU + aMaxPocetZaznamovVBloku * aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch();
    }

    public int getPocetPlatnychZaznamovBloku() {
        int ret = 0;
        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (prech.jePlatny()) {
                ret++;
            }
        }
        return ret;
    }
    
    public Zaznam getZaznam(Zaznam paZaznam){
        int index = aZoznamZaznamovBloku.indexOf(paZaznam);
        if(index>=0){
            return aZoznamZaznamovBloku.get(index);
        }
        return null;
    }

    public Zaznam getPrvyPlatnyZaznam() {
        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (prech.jePlatny()) {
                return prech;
            }
        }
        return null;
    }

    public boolean jeBlokPreplneny() {
        return aZoznamZaznamovBloku.size() > aMaxPocetZaznamovVBloku;
    }

    public boolean jeBlokPlny() {
        if (aZoznamZaznamovBloku.size() != aMaxPocetZaznamovVBloku) {
            throw new IllegalStateException("Pole zoznamov musi mat velkost max poctu zaznamov v bloku");
        }
        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (!prech.jePlatny()) {
                return false;
            }
        }
        return true;
    }

    public boolean jeBlokPrazdny() {
        if (aZoznamZaznamovBloku.size() != aMaxPocetZaznamovVBloku) {
            throw new IllegalStateException("Pole zoznamov musi mat velkost max poctu zaznamov v bloku");
        }
        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (prech.jePlatny()) {
                return false;
            }
        }
        return true;
    }

    public boolean existujeZaznam(Zaznam paZaznam) {
        if (aZoznamZaznamovBloku.size() != aMaxPocetZaznamovVBloku) {
            throw new IllegalStateException("Pole zoznamov musi mat velkost max poctu zaznamov v bloku");
        }

        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (prech.jePlatny() && prech.equals(paZaznam)) {
                return true;
            }
        }
        return false;
    }

    public boolean odstranZaznam(Zaznam paZaznam) {
        for (Zaznam prech : aZoznamZaznamovBloku) {
            if (prech.jePlatny() && prech.equals(paZaznam)) {
                prech.setPlatny(Zaznam.Platnost.NEPLATNY);
                return true;
            }
        }
        return false;
    }

    public void setaCisloBloku(long aCisloBloku) {
        this.aCisloBloku = aCisloBloku;
    }

    public boolean somUlozenyVPreplnujucomSubore() {
        return aSomUlozenyVPreplnujucomSubore;
    }

    public void setaSomUlozenyVPreplnujucomSubore(boolean aSomUlozenyVPreplnujucomSubore) {
        this.aSomUlozenyVPreplnujucomSubore = aSomUlozenyVPreplnujucomSubore;
    }

    public void setCisloZretazenehoBloku(long paCisloZretazenehoBloku) {
        this.aCisloZretazenehoBloku = paCisloZretazenehoBloku;
    }

    public void vlozZaznam(Zaznam paZaznam) {
        for (Iterator<Zaznam> iterator = aZoznamZaznamovBloku.iterator(); iterator.hasNext();) {
            Zaznam next = iterator.next();
            if (!next.jePlatny()) {
                iterator.remove();
                break;
            }
        }
        //aZoznamZaznamovBloku.add(0, paZaznam);
        aZoznamZaznamovBloku.add(paZaznam);
    }

    /**
     *
     * @return
     */
    public Zaznam odstranPoslednePridanyZaznam() {
        Zaznam ret = aZoznamZaznamovBloku.getLast();
        aZoznamZaznamovBloku.removeLast();
        return ret;
    }

    @Override
    public Iterator<Zaznam> iterator() {
        return aZoznamZaznamovBloku.iterator();
    }

    public void doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami() {
        int kolkoDoplnit = aMaxPocetZaznamovVBloku - aZoznamZaznamovBloku.size();
        for (int i = 0; i < kolkoDoplnit; i++) {
            aZoznamZaznamovBloku.add(aImplementatorZaznamu.vytvorNeplatnyZaznam());
        }
    }

    /**
     * nastavi blok na neinicializovany a vymaze vsetky aktualne nacitane
     * zaznamy - !!!LEN V OPERACNEJ PAMATI
     */
    public void reset() {
        aCisloBloku = -1;
        aCisloZretazenehoBloku = -1;
        aSomUlozenyVPreplnujucomSubore = false;
        aZoznamZaznamovBloku.clear();
    }

    public void resetZoznamZaznamov() {
        aZoznamZaznamovBloku.clear();
    }

    public void nacitajZaznamyBlokuZBinarnychDat(byte[] paData) {
        if (paData.length != getVelkostBlokuVBajtoch()) {
            throw new IllegalStateException("Velkost pola paData musi byt rovnaka ako velkost bloku. "
                    + "Inak sa pravdepodobne nepodarilo nacitat cely blok zo suboru alebo niekto zmenil vypocet velkosti bloku"
                    + "co by sa nemalo. Potom je potrebne vytvorit novy subor. Nepouzivat stare");
        }

        //nacitanie hlavicky bloku
        try {
            ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(paData);
            DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
            aCisloBloku = hlpInStream.readLong();
            aCisloZretazenehoBloku = hlpInStream.readLong();
        } catch (IOException ex) {
            Logger.getLogger(Blok.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 0; i < aMaxPocetZaznamovVBloku; i++) {
            int od = VELKOST_HLAVICKY_BLOKU + i * aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch();
            int po = (i + 1) * aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch() + VELKOST_HLAVICKY_BLOKU;
            byte[] zaznam = Arrays.copyOfRange(paData, od, po);
            aZoznamZaznamovBloku.add(aImplementatorZaznamu.toZaznam(zaznam));
        }
    }

    public byte[] transformujBlokDoBinarnejPodoby() {
        byte[] ret = new byte[getVelkostBlokuVBajtoch()];
        try {
            ByteArrayOutputStream hlpByteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
            hlpOutStream.writeLong(aCisloBloku);
            hlpOutStream.writeLong(aCisloZretazenehoBloku);
            byte[] hlavicka = hlpByteArrayOutputStream.toByteArray();
            if (VELKOST_HLAVICKY_BLOKU != hlavicka.length) {
                throw new IllegalStateException("Velkost hlavicky bloku musi byt rovnaka ako velkost pola hlavicka.");
            }
            System.arraycopy(hlavicka, 0, ret, 0, VELKOST_HLAVICKY_BLOKU);
        } catch (IOException ex) {
            Logger.getLogger(Blok.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        if (aZoznamZaznamovBloku.size() != aMaxPocetZaznamovVBloku) {
            throw new IllegalStateException("Zoznam musi obsahovat maximalny mozny pocet zaznamov. Lebo Musi byt blok vzdy rovnako velky.");
        }
        int pomSerializujemZaznamCislo = 0;
        for (Zaznam prech : aZoznamZaznamovBloku) {
            byte[] serZaznam = aImplementatorZaznamu.toByteArray(prech);
            for (int j = 0; j < aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch(); j++) {
                int p = aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch();
                ret[pomSerializujemZaznamCislo * aImplementatorZaznamu.get_Velkost_Zaznamu_V_Bajtoch() + j + VELKOST_HLAVICKY_BLOKU] = serZaznam[j];
            }
            pomSerializujemZaznamCislo++;
        }
        return ret;
    }

    @Override
    public String toString() {
        String ret = "Blok{\n";
        ret += "Cislo bloku: " + aCisloBloku + " Cislo bloku v prepln sub: " + aCisloZretazenehoBloku + "\n";
        int zaznamCislo = 0;
        for (Zaznam prech : aZoznamZaznamovBloku) {
            ret += "Zaznam cislo: "+zaznamCislo+prech.toString() + "\n";
            zaznamCislo ++;
        }
        ret += "}\n\n";
        return ret;
    }
}

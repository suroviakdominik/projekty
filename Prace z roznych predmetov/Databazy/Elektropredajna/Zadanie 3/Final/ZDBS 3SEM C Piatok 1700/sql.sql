/*
Created		3.3.2014
Modified		24.4.2014
Project		
Model		
Company		
Author		
Version		
Database		Oracle 10g 
*/


-- Create Tables section


Create table Zamestnanec (
	osobne_cislo Char (6) NOT NULL ,
	id_predajne Char (6) NOT NULL ,
	id_adresy Char (10) NOT NULL ,
	pracovna_pozicia Varchar2 (15) NOT NULL ,
	meno Varchar2 (20),
	priezvisko Varchar2 (20),
primary key (osobne_cislo) 
) 
/

Create table Adresa (
	id_adresy Char (10) NOT NULL ,
	ulica Varchar2 (30) NOT NULL ,
	cislo Varchar2 (6) NOT NULL ,
	mesto Varchar2 (30) NOT NULL ,
	psc Char (5) NOT NULL ,
primary key (id_adresy) 
) 
/

Create table Predajna (
	id_predajne Char (6) NOT NULL ,
	id_adresy Char (10) NOT NULL ,
	nazov_predajne Varchar2 (30) NOT NULL ,
	ICO Char (8),
	pocetZamestnancov Integer,
	pravnaForma Varchar2 (10),
primary key (id_predajne) 
) 
/

Create table Kategoria (
	id_kategorie Varchar2 (10) NOT NULL ,
	nazov_kat Varchar2 (20) NOT NULL ,
	min_pocet Integer NOT NULL ,
	ocakavany_limit_predaja Integer NOT NULL ,
primary key (id_kategorie) 
) 
/

Create table tovar (
	katalogoveCislo Varchar2 (10) NOT NULL ,
	id_predajne Char (6) NOT NULL ,
	id_kategorie Varchar2 (10) NOT NULL ,
	id_typu Char (3) NOT NULL ,
	nazov Varchar2 (30) NOT NULL ,
	minimum_na_sklade Integer NOT NULL ,
	cena Float,
	mnozstvo Integer,
primary key (katalogoveCislo,id_predajne) 
) 
/

Create table faktura (
	cislo_fa Varchar2 (15) NOT NULL ,
	osobne_cislo Char (6) NOT NULL ,
	id_zakaznika Char (5) NOT NULL ,
	datum_vystavenia Date NOT NULL ,
	datum_splatnosti Date NOT NULL ,
	cena_za_fakturu Float,
	stav_faktury Varchar2 (12)  NOT NULL,
primary key (cislo_fa,osobne_cislo) 
) 
/

Create table VyfakturovanyTovar (
	cislo_fa Varchar2 (15) NOT NULL ,
	katalogoveCislo Varchar2 (10) NOT NULL ,
	id_predajne Char (6) NOT NULL ,
	osobne_cislo Char (6) NOT NULL ,
	mnozstvo Integer NOT NULL ,
primary key (cislo_fa,katalogoveCislo,id_predajne,osobne_cislo) 
) 
/

Create table Zakaznik (
	id_zakaznika Char (5) NOT NULL ,
	id_adresy Char (10) NOT NULL ,
	meno Varchar2 (20),
	priezvisko Varchar2 (20),
primary key (id_zakaznika) 
) 
/

Create table OdporucaneTovary (
	katalogoveCislo Varchar2 (10) NOT NULL ,
	id_predajne Char (6) NOT NULL ,
	katalogoveCisloODPTovaru Varchar2 (10) NOT NULL ,
	id_predajneODPTovaru Char (6) NOT NULL ,
primary key (katalogoveCislo,id_predajne,katalogoveCisloODPTovaru,id_predajneODPTovaru) 
) 
/

Create table Typ (
	id_typu Char (3) NOT NULL ,
	nazov_typu Varchar2 (30),
primary key (id_typu) 
) 
/


-- Create Indexes section


-- Create Foreign keys section

Alter table faktura add  foreign key (osobne_cislo) references Zamestnanec (osobne_cislo) 
/

Alter table Zamestnanec add  foreign key (id_adresy) references Adresa (id_adresy) 
/

Alter table Zakaznik add  foreign key (id_adresy) references Adresa (id_adresy) 
/

Alter table Predajna add  foreign key (id_adresy) references Adresa (id_adresy) 
/

Alter table Zamestnanec add  foreign key (id_predajne) references Predajna (id_predajne) 
/

Alter table tovar add  foreign key (id_predajne) references Predajna (id_predajne) 
/

Alter table tovar add  foreign key (id_kategorie) references Kategoria (id_kategorie) 
/

Alter table VyfakturovanyTovar add  foreign key (katalogoveCislo,id_predajne) references tovar (katalogoveCislo,id_predajne) 
/

Alter table OdporucaneTovary add  foreign key (katalogoveCisloODPTovaru,id_predajneODPTovaru) references tovar (katalogoveCislo,id_predajne) 
/

Alter table OdporucaneTovary add  foreign key (katalogoveCislo,id_predajne) references tovar (katalogoveCislo,id_predajne) 
/

Alter table VyfakturovanyTovar add  foreign key (cislo_fa,osobne_cislo) references faktura (cislo_fa,osobne_cislo) 
/

Alter table faktura add  foreign key (id_zakaznika) references Zakaznik (id_zakaznika) 
/

Alter table tovar add  foreign key (id_typu) references Typ (id_typu) 
/


-- Create Views section


-- Create Sequences section


-- Create Synonyms section


-- Create Table comments section


-- Create Attribute comments section



<?php
session_start();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(isset($_SESSION["login"])&&$_SESSION["login"]==0)
{
$target_dir = "../ObrazkyZvierat/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "Subor je obrazok - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Subor nie je obrazok.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Subor s rovnakym menom je uz na servere nahraty.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Vas subor je prilis velky.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Prepáčte podporované formáty sú: JPG, JPEG, PNG & GIF.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Váš súbor nebol nahraný na server";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "Súbor ". basename( $_FILES["fileToUpload"]["name"]). " bol nahraný na server.";
        header("Location: index.html");
    } else {
        echo "Pri nahrávaní súboru na server sa vyskytla chyba";
    }
}
}
else
{
    echo "Obrazky moze na server uploadovat iba prihlaseny administrator";
}
?> 
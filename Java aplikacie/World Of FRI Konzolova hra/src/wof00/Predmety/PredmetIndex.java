/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.util.ArrayList;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class PredmetIndex implements IPredmet {

    private String aNazov;
    private String aPopis;
    private ArrayList<String> aZnamky;

    public PredmetIndex(String aNazov, String aPopis) {
        this.aNazov = aNazov;
        this.aPopis = aPopis;
        aZnamky = new ArrayList<String>();
    }

    public void zapisZnamky(String paZnamka) {
        aZnamky.add(paZnamka);
    }

    public String vypisZnamky() {
        String hodnotenie = "";
        for (String prechadzac : aZnamky) {
            hodnotenie += prechadzac;
        }
        return hodnotenie;
    }

    public int pocetZnamok() {
        return aZnamky.size();
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return aPopis;
    }

    @Override
    public String pouzi(Hrac paHrac) {
        return "Hodnotenie: " + vypisZnamky();
    }

}

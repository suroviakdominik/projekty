/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeoLocationComponents;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.event.FocusListener;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * JPanel umoznujuci zadat zemepisu sirku aleboo zemepisnu dlzku a 
 * akontrolovat, ci su tieto udaje platne
 * @author Dominik
 */
public class JLatLonPanel extends JPanel {

    private JNumberTextField aDegree;
    private JNumberTextField aMinutes;
    private JNumberTextField aSeconds;
    private double aMin;
    private double aMax;

    public JLatLonPanel(double min, double max) {
        this.setLayout(new GridLayout(1, 3));
        aDegree = new JNumberTextField(-180, 180, "Stupne", true);
        aMinutes = new JNumberTextField(0, 59, "Minúty", false);
        aSeconds = new JNumberTextField(0, 59, "Sekundy", false);
        add(aDegree);
        add(aMinutes);
        add(aSeconds);
        aMin = min;
        aMax = max;
    }

    @Override
    public void addFocusListener(FocusListener paFocus) {
        for (Component prech : getComponents()) {
            if (prech instanceof JNumberTextField) {
                prech.addFocusListener(paFocus);
            }
        }
    }

    @Override
    public void setEnabled(boolean paIsEnable) {
        for (Component prech : getComponents()) {
            prech.setEnabled(paIsEnable);
        }
    }

    @Override
    public void setFont(Font paFont) {
        for (Component prech : getComponents()) {
            prech.setFont(paFont);
        }
    }
    /**
     * 
     * @return true - ak je cast suradnice (Lot alebo Lan) platna
     */
    public boolean areSetValidValues() {
        for (Component prech : getComponents()) {
            if (prech instanceof JNumberTextField) {
                JNumberTextField locattioDataField = (JNumberTextField) prech;
                if (!locattioDataField.isSetValidaValue()) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * 
     * @return true - ak sa suradnice nachdzaju v oblasti zadanyi parameterami aMin, aMax
     */
    public boolean areValuesInAreatOfZOO()
    {
        if(parsujSuradnicu()<aMin||parsujSuradnicu()>aMax)
        {
            return false;
        }
        return true;
    }

    /**
     * 
     * @return dekadika reprezentacia zadanej casti suradnice 
     *          -1 - ak je suradnica naplatna, nenachdza sa vo vymedzeni aMin, aMax
     *              alebo niektory z udajov nie je cislos
     */
    public double getPartOfGeoPoint() {
        if (areSetValidValues()) {
            try {
                return parsujSuradnicu();
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Nepodarilo sa rozparsovat niektoru z casti suradnice", "Class: " + this.getClass().getName(), JOptionPane.ERROR_MESSAGE);
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * 
     * @return - dekadicka forma casti suradnice zadanej v ramci tohto komponentu
     * @throws NumberFormatException 
     */
    private double parsujSuradnicu() throws NumberFormatException {
        double stupne = Double.parseDouble(aDegree.getText());
        double minuty = Double.parseDouble(aMinutes.getText());
        double sekundy = Double.parseDouble(aSeconds.getText());
        double ret = stupne + minuty / 60 + sekundy / 3600;
        return ret;
    }

    /**
     * 
     * @return  textva reprezentacia suradnice v tvare DD:MM:SS
     */
    public String getTextRepresentation() {
        return aDegree.getText() + ":" + aMinutes.getText() + ":" + aSeconds.getText();
    }

    /**
     * 
     * @param paData - nastavi do komponentov cast suradnice v tvare DD MMM SS
     */
    public void setData(String paData) {
        aDegree.setText(paData.split(":")[0]);
        aMinutes.setText(paData.split(":")[1]);
        aSeconds.setText(paData.split(":")[2]);
        aDegree.setIsSetValidValue(true);
        aMinutes.setIsSetValidValue(true);
        aSeconds.setIsSetValidValue(true);
    }

    public void restore() {

        aDegree.setText("Stupne");
        aMinutes.setText("Minúty");
        aSeconds.setText("Sekundy");
        aDegree.setIsSetValidValue(false);
        aMinutes.setIsSetValidValue(false);
        aSeconds.setIsSetValidValue(false);
    }

}

Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Retaz DB 10 DUP(?);deklaracia pola o velkosti 8B
RetazAno DB 'Ano',0;
.CODE;//zacina kodovy segment
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je retaz, nemozno ju priradit rovno do ds ak ds,@data 
          mov di, 2
          mov Retaz,6;do retaze na 1B ulozit max 6 preto lebo ahoj+0Dh+velkost retazca = 6
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          
VypisZnak:
          mov dl,[Retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp dl,0Dh;porovnaj hodnotu v dl s 0Dh
          jz NacitajPismeno;ak bola 0Dh, skok na navestie koniec
          mov ah,2
          int 21h
          
          inc di;zvys index o 1/prejdi na dalsi znak
          jmp VypisZnak

NacitajPismeno:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          
          mov di,2
          mov AH,1;sluzba na nacitanie jedneho znaku s klavesnice
          int 21h;
          mov BH,AL;lebo ak do registra ah ulozime cislo sluzby , pokazi sa aj regiter al
          
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          

ZistiVyskytZnaku:
          mov dl,[Retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp dl,BH;porovnaj hodnotu v dl s BH=s nacitanym znakom
          jz InicializujDi
          jmp Koniec

InicializujDi:
          mov di,0
          
          
VypisAno: mov dl,[RetazAno+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp dl,0;porovnaj hodnotu v dl s 0
          jz Koniec;ak bola 0, skok na navestie koniec
        
          mov ah,2
          int 21h
          
          inc di;zvys index o 1/prejdi na dalsi znak
          jmp VypisAno

Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.Scanner;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.MiestnostLabak;

/**
 *
 * @author suroviak3
 */
public class PredmetObriaMucha implements IPredmet, Serializable {

    @Override
    public String getNazov() {
        return "obriamucha";
    }

    @Override
    public String getPopis() {
        return "mucha, ktora vas prenesie do akejkolvek miestnosti";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        System.out.println("Zadajte miestnost: ");
        Scanner scan = new Scanner(System.in);
        String miestnost = scan.nextLine();
        IMiestnost kamIst = paHrac.getaMapaHry().najdiMiestnost(miestnost);
        if (kamIst != null) {
            if (!(kamIst instanceof MiestnostLabak)) {
                paHrac.setPoziciu(kamIst);
                paHrac.dajPoziciu().vypisInfoMiestnosti();
                return "Presunuli ste sa do misetnosti: " + miestnost;
            } else {
                if (paHrac.isaMamNaVleky()) {
                    paHrac.setPoziciu(kamIst);
                    paHrac.dajPoziciu().vypisInfoMiestnosti();
                    return "Presunuli ste sa do misetnosti: " + miestnost;
                } else {
                    return "Nemate navleky. Do Labaku mozete ist iba v navlekoch.";
                }
            }

        }
        return "Taka miestnost neexistuje.";

    }

}

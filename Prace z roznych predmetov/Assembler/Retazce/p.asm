.MODEL small
.STACK 100h
.DATA
DesiatPriNas DW 10
Retazec DB 'Koleso',0
RetazecSKla DB 10 dup(?)
VypisPocetZadanPrvkovVRetazci DB 'Pocet vyskytov zadaneho znaku v retazci: $'
.CODE
;--------------------------------------------------------KONVERZIA A VYPIS NA TERMINAL BINARNEHO CISLA V REGISTRI AX--------------------------------
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                           
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo

Write ENDP
;--------------------------------------------PROCEDURA NA NAJDENIE ZADANEHO ZNAKU V RETAZCI-------------------------------------------------------
NajdiPrvokVRetazci PROC
;vstupny parameter-do AL vlozit znak ktory v retazci hladame
;a do bx vlozit offset retazca v ktorom znak hladame
;a do di vlozit od ktoreho indexu chceme vyskys zistovat
;vystup - pocet prvkov v registri SI=ak je vacsi ako 0, potom bol prvok najdeny
 push di
 push ax
 push dx
 mov si,0
 Cyklus:
      cmp [bx+di],al
      je Zvys
      inc di
      jmp Kon
 Zvys:
   inc si
   inc di
Kon:
    cmp byte ptr [bx+di],0
    jne Cyklus
    je Navrat
    cmp byte ptr [bx+di],0Dh
    jne Cyklus
Navrat: 
    mov dx,offset VypisPocetZadanPrvkovVRetazci
    mov ah,9
    int 21h
    mov ax,si
    call Write 
    pop dx
    pop ax
    pop di
    ret                   
NajdiPrvokVRetazci ENDP

;---------------------------------------------------HLAVNY PROGRAM----------------------------------------------------------------
Zaciatok:
   mov ax,@data  
   mov ds,ax
   
   mov bx,offset Retazec
   mov di,0
   mov al,'K'
   call NajdiPrvokVRetazci
   
  
Koniec:
       mov ah,4Ch
       int 21h
END Zaciatok






/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyGUIJComponent;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.Border;

/**
 * Trieda, ktorej instancia je panelom, ktory
 * obsahuje komponenty na zadanie otvaracich hodin
 * a metody pomocou ktorych mozno otvaracie hodiny zistit
 * @author Dominik
 */
public class JOpeningHoursPanel extends JPanel {
    private JTextField aOpenTimeData[][];
    public JOpeningHoursPanel(Font paFont) {
        super();
        setLayout(new GridLayout(8, 3, 2, 0));
        inicialuzujKomnponenty();
        for (Component prech : this.getComponents()) {
            prech.setFont(paFont);
        }
    }

    /**
     * inicializuje vizualne komponenty totho panela
     */
    private void inicialuzujKomnponenty() {
        aOpenTimeData= new JTextField[7][2];
        for (int i = 0; i < aOpenTimeData.length; i++) {
            for (int j = 0; j < aOpenTimeData[1].length; j++) {

                aOpenTimeData[i][j] = new JTextField("hh:mm");
            }
        }

        JLabel l = new JLabel();
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        l.setBorder(border);
        add(l);
        JLabel od = new JLabel("  OD");
        od.setBorder(border);
        add(od);
        JLabel po = new JLabel("  DO");
        po.setBorder(border);
        add(po);

        JLabel pondelok = new JLabel("  PONDELOK");
        pondelok.setBorder(border);
        add(pondelok);

        add(aOpenTimeData[0][0]);
        add(aOpenTimeData[0][1]);

        JLabel utorok = new JLabel("  UTOROK");
        utorok.setBorder(border);
        add(utorok);
        add(aOpenTimeData[1][0]);
        add(aOpenTimeData[1][1]);

        JLabel streda = new JLabel("  STREDA");
        streda.setBorder(border);
        add(streda);
        add(aOpenTimeData[2][0]);
        add(aOpenTimeData[2][1]);

        JLabel stvrtok = new JLabel("  ŠTVRTOK");
        stvrtok.setBorder(border);
        add(stvrtok);
        add(aOpenTimeData[3][0]);
        add(aOpenTimeData[3][1]);

        JLabel piatok = new JLabel("  PIATOK");
        piatok.setBorder(border);
        add(piatok);
        add(aOpenTimeData[4][0]);
        add(aOpenTimeData[4][1]);

        JLabel sobota = new JLabel("  SOBOTA");
        sobota.setBorder(border);
        add(sobota);
        add(aOpenTimeData[5][0]);
        add(aOpenTimeData[5][1]);

        JLabel nedela = new JLabel("  NEDEĽA");
        nedela.setBorder(border);
        add(nedela);
        add(aOpenTimeData[6][0]);
        add(aOpenTimeData[6][1]);

    }
    
    /**
     * Vrati data v postupnosti. PONDELOK +"OD"|"DO";;;
     *                          UTOROK +"OD"|"DO";;; a.t.d
     * @return 
     */
    public String getData()
    {
        String ret="";
        for (int i = 0; i < aOpenTimeData.length; i++) {
            for (int j = 0; j < aOpenTimeData[1].length; j++) {

                ret+=aOpenTimeData[i][j].getText()+"|";
            }
            ret+=";;;";
        }
        return ret;
    }
    
    /**
     * - nastavi otvaracie hodiny tyzdna podla dat z pola
     * pole obsahuje v riadkoch dni od pondelka do piatku
     * a v stlpcoch od do casy
     * @param paData otvaracie hodiny tyzdna
     * @return 
     */
    public boolean setData(String paData[][])
    {
        if(paData.length!=7||paData[1].length!=2)
        {
             System.err.println("MyLogFiles: Data v triede JOpeningHoursPanel: setData(...) sa nepodarilo nastaviť, pretože nie sú v požadovanom formáte HH:MM");
            return false;
        }
         for (int i = 0; i < aOpenTimeData.length; i++) {
            for (int j = 0; j < aOpenTimeData[1].length; j++) {

                aOpenTimeData[i][j].setText(paData[i][j]);
            }
        }
         if(checkTimesData())
         {
             System.out.println("MyLogFiles: Data in method: setData of class: "+this.getClass().getName()+" was successfully set.");
             return true;
         }
         else
         {
            for (int i = 0; i < aOpenTimeData.length; i++) {
            for (int j = 0; j < aOpenTimeData[1].length; j++) {

                aOpenTimeData[i][j] = new JTextField("hh:mm");
            }
        }
             System.err.println("MyLogFiles: Data v triede JOpeningHoursPanel: setData(...) sa nepodarilo nastaviť, pretože nie sú v požadovanom formáte HH:MM");
             return false;
         }
    }
    
    /**
     * skontroluje ci maju data v tabulke otvaracich hodin spravny format
     * @return true- ak su udaje v tabulke otvaracich hodin dobre udaje
     */
    public boolean checkTimesData()
    {
         String otvaraciaDoba=null;
         String dobaZatvorenia=null;
        for(int i = 0; i < aOpenTimeData.length; i++) {
            try {
                otvaraciaDoba=aOpenTimeData[i][0].getText();
                dobaZatvorenia=aOpenTimeData[i][1].getText();
                if(!isValidHour(Integer.parseInt(otvaraciaDoba.split(":")[0]))||!isValidMinute(Integer.parseInt(otvaraciaDoba.split(":")[1])))
                {
                    return false;
                }
                if(!isValidHour(Integer.parseInt(dobaZatvorenia.split(":")[0]))||!isValidMinute(Integer.parseInt(dobaZatvorenia.split(":")[1])))
                {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
              
           
        }
        return true;
    }

    private boolean isValidHour(int paHour)
    {
        return paHour>=0&&paHour<=23?true:false;
    }
    
    private boolean isValidMinute(int paMinute)
    {
        return paMinute>=0&&paMinute<=59?true:false;
    }
    
    
}

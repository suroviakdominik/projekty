/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class Coordinate implements Serializable{
    private String aDegree;
    private String aMinutes;
    private String aSeconds;
    private static final long serialVersionUID = 4L;

    public Coordinate(String aDegree, String aMinutes, String aSeconds) {
        this.aDegree = aDegree;
        this.aMinutes = aMinutes;
        this.aSeconds = aSeconds;
    }

    public double getDECRepresentationOfCoordinate()
    {
        double deg=Double.parseDouble(aDegree);
        double min = Double.parseDouble(aMinutes);
        double sec =Double.parseDouble(aSeconds);
        return deg+min/60+sec/3600;
    }

    public String getaDegree() {
        return aDegree;
    }

    public String getaMinutes() {
        return aMinutes;
    }

    public String getaSeconds() {
        return aSeconds;
    }

    
    
    
    @Override
    public String toString() {
        return aDegree+":"+aMinutes+":"+aSeconds;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.aDegree);
        hash = 89 * hash + Objects.hashCode(this.aMinutes);
        hash = 89 * hash + Objects.hashCode(this.aSeconds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate other = (Coordinate) obj;
        if (!Objects.equals(this.aDegree, other.aDegree)) {
            return false;
        }
        if (!Objects.equals(this.aMinutes, other.aMinutes)) {
            return false;
        }
        if (!Objects.equals(this.aSeconds, other.aSeconds)) {
            return false;
        }
        return true;
    }
  
    
    
}

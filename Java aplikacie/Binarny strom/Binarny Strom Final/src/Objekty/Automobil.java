/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objekty;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author Dominik
 */
public class Automobil implements IObjekt, Serializable {

    private static boolean navrat;
    private String aZnacka;
    private String aTyp;
    private double aObjemMotora;
    private String aPalivo;
    private int aRokVyroby;

    /**
     * Metóda nastaví hodnoty atribútov objektu. Hodnoty budú načitané z okna,
     * ktoré pouźívateľ vyplní.
     *
     * @return
     */
    @Override
    public boolean napln() //metoda naplni atributy triedy automobil pomocou modalneho okna kde zadame hodnoty atributov
    {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 250);
        ramik.setResizable(false);
        ramik.setTitle("Automobil  *  naplnenie");

        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(7, 2, 5, 5));

        //	so  vstupnými poľami
        final JTextField txtZnacka = new JTextField();
        final JTextField txtTyp = new JTextField();
        final JTextField txtObjemMotora = new JTextField();
        final JTextField txtPalivo = new JTextField();
        final JTextField txtRok = new JTextField();

        // 	a dvoma tlačidlami
        JButton btUlozit = new JButton("Uložiť");
        btUlozit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    aZnacka = txtZnacka.getText().trim();
                    if (aZnacka.length() < 2) {
                        throw new IllegalArgumentException(
                                "\n     neprípustná dĺžka   / min. 2 znaky /");
                    }

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka znacka", JOptionPane.ERROR_MESSAGE);
                    txtZnacka.requestFocus();
                    return;
                }

                try {
                    aTyp = txtTyp.getText().trim();
                    if (aTyp.length() < 2) {
                        throw new IllegalArgumentException("\n neprípustná dĺžka   / min. 2 znaky /");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka typ", JOptionPane.ERROR_MESSAGE);
                    txtTyp.requestFocus();
                    return;
                }

                try {
                    aObjemMotora = Double.parseDouble(txtObjemMotora.getText().trim());
                    if (aObjemMotora < 0.5 || aObjemMotora > 8) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný rozsah / 0.5-8 /");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null,
                            ex + "\n     údaj musí byť realne číslo",
                            "Chyba-Objem Motora", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka objem motora", JOptionPane.ERROR_MESSAGE);
                    txtTyp.requestFocus();
                    return;
                }

                try {
                    aPalivo = txtPalivo.getText().trim();
                    if (aPalivo.length() < 2) {
                        throw new IllegalArgumentException("\n neprípustná dĺžka   / min. 2 znaky /");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka palivo", JOptionPane.ERROR_MESSAGE);
                    txtTyp.requestFocus();
                    return;
                }

                try {
                    aRokVyroby = Integer.parseInt(txtRok.getText().trim());
                    if (aRokVyroby < 1901 || aRokVyroby > 2099) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný rozsah / 1901-2099 /");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null,
                            ex + "\n     údaj musí byť celočíselný",
                            "Chyba roku vyroby", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba roku vyroby", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                }

                navrat = true;
                ramik.dispose();

            }
        });

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                navrat = false;
                ramik.dispose();

            }
        });

        //	a popisnými poľami,    a všetko poskladáme
        ramik.add(new JLabel("   Znacka      :  ", JLabel.RIGHT));
        ramik.add(txtZnacka);
        ramik.add(new JLabel("   Typ            :  ", JLabel.RIGHT));
        ramik.add(txtTyp);
        ramik.add(new JLabel("   Objem motora   :  ", JLabel.RIGHT));
        ramik.add(txtObjemMotora);
        ramik.add(new JLabel("   Palivo   :  ", JLabel.RIGHT));
        ramik.add(txtPalivo);
        ramik.add(new JLabel("   Rok   :  ", JLabel.RIGHT));
        ramik.add(txtRok);

        ramik.add(btUlozit);
        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda zobrazí okno na načitanie hodnôt atribútorv objektu a na základe
     * zadanýcho hodnôt nastaví atribúty tohto objektu.
     *
     * @return boolean - true ak načítalo, false ak nenačitalo
     */
    @Override
    public boolean zobraz() {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 250);
        ramik.setResizable(false);
        ramik.setTitle("Automobil  *  Udaje");
        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(7, 2, 5, 5));

        ramik.add(new JLabel("   Znacka      :  ", JLabel.RIGHT));
        ramik.add(new JLabel(aZnacka));
        ramik.add(new JLabel("   Typ            :  ", JLabel.RIGHT));
        ramik.add(new JLabel(aTyp));
        ramik.add(new JLabel("   Objem motora   :  ", JLabel.RIGHT));
        ramik.add(new JLabel(aObjemMotora + ""));
        ramik.add(new JLabel("   Palivo   :  ", JLabel.RIGHT));
        ramik.add(new JLabel(aPalivo));
        ramik.add(new JLabel("   Rok vyroby   :  ", JLabel.RIGHT));
        ramik.add(new JLabel(aRokVyroby + ""));

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                navrat = false;
                ramik.dispose();
            }
        });

        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda načita hodnoty atribútov zo súboru, ktorý bol predtým uložený
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param panazov-názov súboru z ktorého sa má načítať
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void nacitajZoSuboru(File paPodadresar, String panazov) throws IOException {
        File file = new File(paPodadresar, panazov);
        Scanner scan = new Scanner(file);

        aZnacka = scan.next();
        aTyp = scan.next();
        aObjemMotora = Double.parseDouble(scan.next());
        aPalivo = scan.next();
        aRokVyroby = scan.nextInt();
        scan.close();

    }

    /**
     * Metóda zapíše hodnoty atribútov objektu do súboru, ktorý budeme neskôr
     * používať pre načitanie objektu zo súboru
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param paCisloSuboru-poradie uloženia súboru
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void zapisDoSuboru(File paPodadresar, int paCisloSuboru) throws IOException {
        File subor = new File(paPodadresar, "A" + String.format("%04d", paCisloSuboru) + ".txt");
        try (PrintWriter pw = new PrintWriter(subor)) {
            pw.print(odstranMedzery(aZnacka) + " "); // napíšeme do súboru "Jano" a čakáme na ďalší zápis konci tohto riadku
            pw.print(odstranMedzery(aTyp) + " "); // napíšeme do súboru "Javák", ideme do nového riadku a čakáme
            pw.print(aObjemMotora + " "); // ideme do nového riadku a čakáme
            pw.print(aPalivo + " "); // ideme do nového riadku a čakáme
            pw.print(aRokVyroby + " "); // ideme do nového riadku a čakáme
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Do suboru sa nepodarilo ulozit objekt. Automobil ukladanie");
        }

    }

    private String odstranMedzery(String paRetazec) {
        String s = "";
        for (int i = 0; i < paRetazec.length(); i++) {
            if (paRetazec.charAt(i) != ' ') {
                s += paRetazec.charAt(i);
            }
        }
        return s;
    }

    /**
     * Metóda vráti vo forme Stringu popis objektu Automobil
     *
     * @return String-automobil popis
     */
    @Override
    public String toString() {
        String s = "";
        s += "Objekt: Auto, " + "Nazov: " + aZnacka + ", Typ: " + aTyp + ", Objem motora: " + aObjemMotora + ", Palivo: " + aPalivo;
        return s;
    }

    @Override
    public String getPopis() {
        return "A" + aZnacka + " " + aTyp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Automobil other = (Automobil) obj;
        if (!Objects.equals(this.aZnacka, other.aZnacka)) {
            return false;
        }
        if (!Objects.equals(this.aTyp, other.aTyp)) {
            return false;
        }
        if (Double.doubleToLongBits(this.aObjemMotora) != Double.doubleToLongBits(other.aObjemMotora)) {
            return false;
        }
        if (!Objects.equals(this.aPalivo, other.aPalivo)) {
            return false;
        }
        if (this.aRokVyroby != other.aRokVyroby) {
            return false;
        }
        return true;
    }

}

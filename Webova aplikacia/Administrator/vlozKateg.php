<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId=0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
                $sql = "SELECT id FROM kategoria";
                $result = mysqli_query($this->conn, $sql);
                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row["id"]>$this->biggestId)
                        {
                            $this->biggestId=$row["id"];
                        }
                    }
                    
                } 
            }

    
    function vlozitKategoriu() {
        
        
        $this->biggestId+=1;
        $nazov = htmlspecialchars($_POST["nazovKat"]);
        $popis = htmlspecialchars($_POST["popisKat"]);
        
        $sql = sprintf("INSERT INTO kategoria(id,meno,popis)
                     VALUES ($this->biggestId,'%s','%s')",
             $this->conn->real_escape_string($nazov),
              $this->conn->real_escape_string($popis));

        if ($this->conn->query($sql) === TRUE) {
            echo "Nova kategoria vytvorena";
           
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}
if(isset($_SESSION["login"])&&$_SESSION["login"]==0)
{
$db = new DBPraca();
$db->findBiggestId();
$db->vlozitKategoriu();
$db->ukonciPristup();
}
else
{
    echo "Vkladat novu kategoiru moze admin po prihlaseni.";
}

?>
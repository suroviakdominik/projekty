/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.hra.InicializaciaHry;

import Grafika.Platno;
import java.util.Scanner;
import šach.hra.Hra;
import šach.hra.Hrac;
import šach.spusteniehry.Menu;
import šach.sachovnica.Sachovnica;
import šach.sachovnica.figurky.Dáma;
import šach.sachovnica.figurky.Figurka;
import šach.sachovnica.figurky.Kon;
import šach.sachovnica.figurky.Kral;
import šach.sachovnica.figurky.Kralovna;
import šach.sachovnica.figurky.Pesiak;
import šach.sachovnica.figurky.Veza;

/**
 * má za úlohu vytvoriť už samotnú hru, vykresliť plátno a jednotlivé figúrky, a
 * poskytnúť používateľovi možnosť ovládania týchto príkazov. Implemetnuje
 * interface IInicializator.
 *
 * @author Dominik
 */
public class InicializatorNovejHry implements IInicializator {

    private Figurka[][] aMapaFigurok;
    private Sachovnica aSachovnica;
    private Scanner aScan;
    private String aFarbaFigurokHracaA;
    private String aKtoryHrac = "A";
    private Menu aMenu;

    /**
     * Konštruktor vytvorí objekty typu Sachovnica, DvojrozmernePole a svojmu
     * atribútu aMenu priradí parametrom dostaný objekt paMenu.
     *
     * @param paMenu - menu, ktoré je používané v tomto programe.
     */
    public InicializatorNovejHry(Menu paMenu) {
        aMapaFigurok = new Figurka[8][8];
        aSachovnica = new Sachovnica();
        aMenu = paMenu;
    }

    /**
     * je jediná metóda, ktorá má verejné rozhranie. Jej úlohou je vytvoriť
     * hráčov a následne spustiť samotnú hru, načo používa ostatné metódy.Okrem
     * toho vytvorí plátno kde budú vykresľované grafické objekty.
     *
     * @return true- ak sa hru podarí inicializovať, false -ak neinicializuje
     */
    @Override
    public boolean inicializuj() {
        Hrac lpHracA = vytvorHraca();
        System.out.println("Hráč " + lpHracA.getMeno() + " má farbu " + lpHracA.getFarbaFigurok());
        Hrac lpHracB = vytvorHraca();
        lpHracA.setaProtiHrac(lpHracB);
        lpHracB.setaProtiHrac(lpHracA);
        System.out.println("Hráč " + lpHracB.getMeno() + " má farbu " + lpHracB.getFarbaFigurok());
        Platno.dajPlatno();
        vytvorFigurky();
        vykresliGrafickeObjekty();
        lpHracA.setaJeNaTahu(true);
        Hra novaHra = new Hra(lpHracA, lpHracB, aMenu, 0, 0, 0);
        novaHra.hraj();
        return true;
    }

    /**
     * vytvorí hráča. Pri vytváraní hráča sa použijú metódy nacitajMenoHraca() a
     * nacitajFarbuFigurokHraca(). Vytvoreného hráča vráti metóde, ktorá ju
     * používa.
     *
     * @return -vráti vytvoreného hráča
     */
    private Hrac vytvorHraca() {
        aScan = new Scanner(System.in);
        Hrac lpHrac = new Hrac(nacitajMenoHraca(), nacitajFarbuFigurokHraca(), aMapaFigurok);
        return lpHrac;
    }

    /**
     * pri vytváraní hráča je potrebné získať jeho meno. Táto metóda zabezpečuje
     * požiadanie používateľa o zadanie jeho mena a následne jeho načítanie.
     *
     * @return -načítané meno vráti vo forme Stringu.
     */
    private String nacitajMenoHraca() {
        String lpMenoHraca = null;
        boolean lpTypovoKomatibilnyVstup;
        System.out.println("Zadajte vaše meno : ");

        do {
            System.out.print("Meno hráča " + aKtoryHrac + ": ");

            lpTypovoKomatibilnyVstup = aScan.hasNextLine();
            if (lpTypovoKomatibilnyVstup) {
                lpMenoHraca = aScan.nextLine();
                aKtoryHrac = "B";
            } else {
                System.out.println("Zadali ste nekompatibilný názov. Zadajte spravny nazov.");
                lpTypovoKomatibilnyVstup = false;
            }
        } while (!lpTypovoKomatibilnyVstup);

        return lpMenoHraca;
    }

    /**
     * – okrem mena je potrebné získať aj farbu figúrok hráča, čo zabezpečuje
     * táto metóda. Farby sú vopred preddefinované a používateľ zadá už iba
     * číslo prislúchajúce určitej farbe.
     *
     * @return -naćítanú farbu vráti vo forme Sringu.
     */
    private String nacitajFarbuFigurokHraca() {
        boolean lpTypovoKomatibilnyVstup;
        String lpFarbaFigurokHraca = null;
        if (aFarbaFigurokHracaA == null) {
            System.out.println("Zadajte číslo prislúchajúce výberu farbe figúrok : ");
            System.out.println("1- červená");
            System.out.println("2- zelená");

            do {
                try {
                    System.out.print("Farba figúrok hráča: ");

                    int lpVyber = aScan.nextInt();
                    if ((lpVyber > 0) && (lpVyber < 3)) {
                        if (lpVyber == 1) {
                            lpFarbaFigurokHraca = "red";
                            aFarbaFigurokHracaA = "red";
                        } else {
                            lpFarbaFigurokHraca = "green";
                            aFarbaFigurokHracaA = "green";
                        }
                        lpTypovoKomatibilnyVstup = true;
                    } else {
                        System.out.println("Taká možnosť neexistuje.");
                        lpTypovoKomatibilnyVstup = false;
                    }
                } catch (Exception ex) {
                    System.out.println("Nekompatibilny vstup. Zadajte cislo prisluchajuce farbe.");
                    lpTypovoKomatibilnyVstup = false;
                    aScan.nextLine();
                }
            } while (!lpTypovoKomatibilnyVstup);
        } else {
            if (aFarbaFigurokHracaA.equals("red")) {

                lpFarbaFigurokHraca = "green";
            } else {

                lpFarbaFigurokHraca = "red";
            }
        }

        return lpFarbaFigurokHraca;
    }

    /**
     * naplní dvojrozmerné pole figúrok novovytvorenými figúrkami. Keďže ide o
     * vytvorenie novej hry uloží ich do základného stavu.
     */
    private void vytvorFigurky() {
        for (int i = 0; i < 8; i++) {
            aMapaFigurok[1][i] = new Pesiak("red");
            aMapaFigurok[6][i] = new Pesiak("green");

            aMapaFigurok[0][0] = new Veza("red");
            aMapaFigurok[0][7] = new Veza("red");
            aMapaFigurok[7][0] = new Veza("green");
            aMapaFigurok[7][7] = new Veza("green");

            aMapaFigurok[0][1] = new Kon("red");
            aMapaFigurok[0][6] = new Kon("red");
            aMapaFigurok[7][1] = new Kon("green");
            aMapaFigurok[7][6] = new Kon("green");

            aMapaFigurok[0][2] = new Dáma("red");
            aMapaFigurok[0][5] = new Dáma("red");
            aMapaFigurok[7][2] = new Dáma("green");
            aMapaFigurok[7][5] = new Dáma("green");

            aMapaFigurok[0][3] = new Kral("red");
            aMapaFigurok[7][3] = new Kral("green");
            aMapaFigurok[0][4] = new Kralovna("red");
            aMapaFigurok[7][4] = new Kralovna("green");

        }
    }

    /**
     * vykreslí grafické objekty na plátno.
     */
    private void vykresliGrafickeObjekty() {
        aSachovnica.vykreslenieSachovnice();
        aSachovnica.vykresliFigurky(aMapaFigurok);
    }

}

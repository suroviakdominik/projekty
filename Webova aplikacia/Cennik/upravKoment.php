<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId = 0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
        $sql = "SELECT idKoment FROM komenty";
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row["idKoment"] > $this->biggestId) {
                    $this->biggestId = $row["idKoment"];
                }
            }
        }
    }

    function upravKomentar() {
        $text=htmlspecialchars($_POST["text"]);
        $id=htmlspecialchars($_POST["komId"]);
        $sql = sprintf("UPDATE komenty SET text='%s' WHERE idKoment=%d",
                $this->conn->real_escape_string($text),
                $this->conn->real_escape_string($id));

        if ($this->conn->query($sql) === TRUE) {
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

$db = new DBPraca();
$db->upravKomentar();
$db->ukonciPristup();

?>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skokdodialkytripokusy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class Sportovci {
    private List<Sportovec> aSportovci;

    public Sportovci() {
        aSportovci=new ArrayList<>();
    }
    
    public void pridajSportovca(Sportovec paSportovec){
        aSportovci.add(paSportovec);
    }
    
    public String dajSportovcovAIchNajlepsieVysledky()
    {
        Collections.sort(aSportovci);
        String ret="";
        for(Iterator<Sportovec> i=aSportovci.iterator();i.hasNext();)
        {
            Sportovec s=null;
            s=i.next();
           ret+=s.getaMeno()+": "+s.getaPrvySkok()+" : "+s.getaDruhySkok()+" : "+s.getaTretiSkok()+"\n";
        }
        return ret;
    }
}

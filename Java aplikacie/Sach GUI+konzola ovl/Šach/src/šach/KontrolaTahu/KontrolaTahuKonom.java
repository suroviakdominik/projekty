/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import šach.hra.Hrac;

/**
 * Kontroluje ťah koňom na základe aktuálnych a nových súradníc.
 *
 * @author Dominik
 */
public class KontrolaTahuKonom implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráčom , ktorý
     * vykonáva ťah.
     *
     * @param paNovaXPozicia
     * @param paNovaYPozicia
     * @param paHrac
     */
    public KontrolaTahuKonom(int paNovaXPozicia, int paNovaYPozicia, Hrac paHrac) {
        this.aNovaXPozicia = paNovaXPozicia;
        this.aNovaYPozicia = paNovaYPozicia;
        this.aHrac = paHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    /**
     * skontroluje ťah koňom.
     *
     * @return true ak je ťah platný, inak false
     */
    @Override
    public boolean skontrolujTah() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;

        boolean lpJeSmerProtiOsiX = (lpAktualnaXPozicia > aNovaXPozicia) ? true : false;
        boolean lpJeSmerProtiOsiY = (lpAktualnaYPozicia > aNovaYPozicia) ? true : false;

        if (((Math.abs(lpAktualnaXPozicia - aNovaXPozicia) == 2) && (Math.abs(lpAktualnaYPozicia - aNovaYPozicia) == 1)) || ((Math.abs(lpAktualnaXPozicia - aNovaXPozicia) == 1) && (Math.abs(lpAktualnaYPozicia - aNovaYPozicia) == 2))) {
            if ((aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][aNovaXPozicia] == null) || (!aHrac.getFarbaFigurok().equals(aHrac.getaSachovnicaNaKtorejSaHra()[aNovaYPozicia][aNovaXPozicia].getFarba()))) {
                return true;
            }

        }
        return false;

    }

}

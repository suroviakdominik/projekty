/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.ScaleImage;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import setdatatoandroidaplication.DataTransferer;

/**
 *  * Editor, ktory edituje AKTUALITY
 * @author Dominik
 */
public class Info_File_NEWS_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTransferer;

     /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce aktuality
     *                      a popis ich dat
     */
    public Info_File_NEWS_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aInfoAboutDirsFile = paFilesInfo;
        aDataTransferer = DataTransferer.getInstance();
    }

    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return
     */
    @Override
    public boolean getDataOfInfoFile() {
        Info_About_Item sel_actuality = null;
        if (aDataTransferer.getItemInfo("GET_TEXT_ACTUALITY") == null && aDataTransferer.getItemInfo("GET_IMAGE_ACTUALITY") == null) {
            aDataTransferer.removeAllData();
            for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
                aDataTransferer.addItemInfo(prech, aAktualneUdajeInfoSuboru.get(prech));
            }
            System.out.println("\nMyLogFiles:All news "
                    + "has been set to data transfer object in editor:"
                    + this.getClass().getSimpleName());
        } else if (aDataTransferer.getItemInfo("GET_TEXT_ACTUALITY") != null) {
            sel_actuality = aDataTransferer.getItemInfo("GET_TEXT_ACTUALITY");
            if (sel_actuality != null) {
                return getDescriptionOfSelectedNews(sel_actuality);
            }

        } else if (aDataTransferer.getItemInfo("GET_IMAGE_ACTUALITY") != null) {
            sel_actuality = aDataTransferer.getItemInfo("GET_IMAGE_ACTUALITY");
            BufferedImage im = getPictureOfSelectedActualityIfExists(sel_actuality);
            if (im != null) {
                aDataTransferer.removeAllData();
                aDataTransferer.addImage("RET_IMAGE_ACTUALITY", im);
                System.out.println("\nMyLogFiles: Picture of selected actuality \n"
                        + "has been set to data transfer object in editor:" + this.getClass().getSimpleName());
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

     /**
     * Vytvorenie novej aktuality. Data dostaneme zo zalozky pomocou objektu DataTransferer
     * @return true - aktualitu sa podarilo vytvorit
     */
    @Override
    public boolean addNewItem() {
        String nazovNovAktuality = aDataTransferer.getString("NEW_ACTUALITY");

        aDataTransferer.removeAllData();
        if (nazovNovAktuality.equals("")) {
            return false;
        }
        for (String prech : aAktualneUdajeInfoSuboru.keySet()) {
            if (aAktualneUdajeInfoSuboru.get(prech).getOriginalName().equals(nazovNovAktuality)) {
                return false;
            }
        }
        String relativePath;
        relativePath = aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice() 
                + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + InfoSuboryENUM.FILE_SEPARATOR
                + (aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                + aInfoAboutDirsFile.getNumberOfNextItem() 
                + InfoSuboryENUM.FILE_SEPARATOR);

        Info_About_Item newInfo = new Info_About_Item(nazovNovAktuality, null, relativePath);
        aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + aInfoAboutDirsFile.getNumberOfNextItem(),
                newInfo);
        DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.NEWS.getaRelativePathOfDirecotryWhereIAM()
                + InfoSuboryENUM.NEWS.getSignOfDir()
                + InfoSuboryENUM.FILE_SEPARATOR 
                + InfoSuboryENUM.NEWS.getSignOfDir() 
                + aInfoAboutDirsFile.getNumberOfNextItem());
        dw.vytvorAdresarAkNeexistuje();
        FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + relativePath 
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        fw.createFileIfNonExists();
        newInfo.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.NEWS.getServerDIRDomainOfThisFile()
                + relativePath
                + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);

        aInfoAboutDirsFile.raiseNumberOfNextNewItem();
        System.out.println("MyLogFiles: New news with name "
                + newInfo.getOriginalName()
                + "\n has been created in editor: "
                + this.getClass().getSimpleName());
        aDataTransferer.addItemInfo("CREATED_ACTUALITY", newInfo);
        return saveInfoFile();
    }

      /**
     * Zmaze aktualitu, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer
     * @return true -aktualitu sa podarilo zmazat
     */
    @Override
    public boolean deleteItem() {
        Info_About_Item choosenActuality = aDataTransferer.getItemInfo("SELECTED_ACTUALITY_TO_DELETE");

        if (aAktualneUdajeInfoSuboru.containsValue(choosenActuality)) {
            File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + choosenActuality.getRelativePathOfFilesOfurrentItemInDevice());
            try {
                delete(f);
            } catch (IOException ex) {
                Logger.getLogger(Info_File_NEWS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                aAktualneUdajeInfoSuboru.remove(new File(choosenActuality.getRelativePathOfFilesOfurrentItemInDevice()).getName());
                return false;
            }
            aAktualneUdajeInfoSuboru.remove(new File(choosenActuality.getRelativePathOfFilesOfurrentItemInDevice()).getName());
            System.out.println("MyLogFiles: Actuality: " + choosenActuality.getOriginalName()
                    + "\n has been deleted in editor" + this.getClass().getSimpleName());
            saveInfoFile();
            return true;
        }

        return false;
    }

    /**
     * Upravi data aktuality, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -aktualitu sa podarilo upravit(zmenit data)
     */
    @Override
    public boolean editItem() {
        Info_About_Item nameOfEdActuality = aDataTransferer.getItemInfo("EDITED_ACTUALITY");
        String path = aDataTransferer.getString("EDIT_PICTURE_PATH");
        String relativePath = nameOfEdActuality.getRelativePathOfFilesOfurrentItemInDevice();
        if (nameOfEdActuality == null) {
            return false;
        }
        if (path != null) {
            try {

                Path source = FileSystems.getDefault().getPath(path);
                deleteExistsPictureThatWillBeReplaced(nameOfEdActuality, InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativePath, getEndOfFile(source));
                Path target = FileSystems.getDefault().getPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                        + relativePath
                        + InfoSuboryENUM.NAME_OF_PICTURE_FILE
                        + "." + getEndOfFile(source));
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                ScaleImage.scale(target.toString(), getEndOfFile(source));

                String domainOfFile = (InfoSuboryENUM.NEWS.getServerDIRDomainOfThisFile()
                        + relativePath
                        + InfoSuboryENUM.NAME_OF_PICTURE_FILE
                        + "." + getEndOfFile(source));
                nameOfEdActuality.getDomainsOfFilesInCurrentItem().add(domainOfFile);
                aDataTransferer.removeAllData();

            } catch (IOException ex) {
                Logger.getLogger(Info_File_NEWS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                aDataTransferer.removeAllData();
                return false;
            }

        } else {
            String text = aDataTransferer.getString("EDITED_TEXT");
            FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                    + nameOfEdActuality.getRelativePathOfFilesOfurrentItemInDevice()
                    + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            try {
                fw.writeDataToFile(text);

            } catch (IOException ex) {
                Logger.getLogger(Info_File_NEWS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        nameOfEdActuality.setDateOfEditOrCreateFile(null);
        saveInfoFile();
        return true;

    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.NEWS.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.NEWS.getaRelativePathOfDirecotryWhereIAM());
        sw.saveObjectToFile();
        System.out.println("MyLogFiles: Info_About_Dirs_File in editor: "
                + this.getClass().getSimpleName()
                + " has been saved.");
        return true;
    }

    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
    @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }

    private BufferedImage getPictureOfSelectedActualityIfExists(Info_About_Item paItemPicture) {
        String relativeTargetPath = paItemPicture.getRelativePathOfFilesOfurrentItemInDevice();
        File f = new File(InfoSuboryENUM.ROOT_DEVICE_FOLDER + relativeTargetPath);
        if (f.listFiles().length == 0) {
            f = null;
        } else {
            for (File prech : f.listFiles()) {
                if (prech.getName().split("\\.").length > 1 && ((prech.getName().split("\\.")[1].toLowerCase()).equals("jpg")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("png")
                        || (prech.getName().split("\\.")[1].toLowerCase()).equals("gif"))) {
                    f = new File(prech.getAbsolutePath());
                    break;
                } else {
                    f = null;
                }
            }
        }
        BufferedImage myPicture = null;
        if (f != null) {

            try {
                myPicture = ImageIO.read(f);
            } catch (IOException ex) {
                Logger.getLogger(Info_File_NEWS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return null;
            }
        }
        return myPicture;
    }

    private boolean getDescriptionOfSelectedNews(Info_About_Item paPolokaPopis) {
        String ret = paPolokaPopis.getRelativePathOfFilesOfurrentItemInDevice();
        aDataTransferer.removeAllData();
        if (ret != null) {
            FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER 
                    + ret 
                    + InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            fw.createFileIfNonExists();
            try {
                String textData = fw.readDataFromFile();
                aDataTransferer.addString("RET_TEXT_ACTUALITY", textData);
                System.out.println("\nMyLogFiles: Description of selected actuality \n"
                        + "has been set to data transfer object in editor:" + this.getClass().getSimpleName());
            } catch (IOException ex) {
                Logger.getLogger(Info_File_NEWS_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private String getEndOfFile(Path paPath) {
        String fileName = paPath.getFileName().toString();
        try {
            return fileName.split("\\.")[1].toLowerCase();
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }

    }

    private void deleteExistsPictureThatWillBeReplaced(Info_About_Item paEditedActuality, String paAdresar, String paEndOfFile) {
        File f = new File(paAdresar);
        if (f.listFiles().length == 0) {
            return;
        }
        for (File prech : f.listFiles()) {
            if (!prech.isDirectory() && prech.getName().split("\\.")[0].equals(InfoSuboryENUM.NAME_OF_PICTURE_FILE)) {
                paEditedActuality.getDomainsOfFilesInCurrentItem().remove(InfoSuboryENUM.NEWS.getServerDIRDomainOfThisFile()
                        + paEditedActuality.getRelativePathOfFilesOfurrentItemInDevice()
                        + InfoSuboryENUM.NAME_OF_PICTURE_FILE 
                        + "." + paEndOfFile);
                prech.delete();
                return;
            }
        }
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import šach.hra.Hrac;

/**
 * Kontroluje ťah kráľovnou na základe aktuálnych a nových súradníc.
 *
 * @author Dominik
 */
public class KontrolaTahuKralovnou implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráčom , ktorý
     * vykonáva ťah.
     *
     * @param aNovaXPozicia
     * @param aNovaYPozicia
     * @param aHrac
     */
    public KontrolaTahuKralovnou(int aNovaXPozicia, int aNovaYPozicia, Hrac aHrac) {
        this.aNovaXPozicia = aNovaXPozicia;
        this.aNovaYPozicia = aNovaYPozicia;
        this.aHrac = aHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    /**
     * skontroluje ťah kráľovnou. Pri kontrole použije ďalšie metódy tejto
     * triedy, ktorých prístupový typ je private.
     *
     * @return Ak je ťah platný vráti true, inak vráti false.
     */
    @Override
    public boolean skontrolujTah() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;

        if (Math.abs(lpAktualnaXPozicia - aNovaXPozicia) == (Math.abs(lpAktualnaYPozicia - aNovaYPozicia))) {
            return moznoVykonatTahPoDiagonale();
        } else if (((lpAktualnaYPozicia == aNovaYPozicia) && (lpAktualnaXPozicia != aNovaXPozicia)) || ((lpAktualnaXPozicia == aNovaXPozicia) && (lpAktualnaYPozicia != aNovaYPozicia))) {
            return moznoVykonatTahVRadeAleboStlpci();
        }
        return false;
    }

    /**
     * pomocná metóda, ktorá skontroluje či sa možno posunúť kráľovnou na novú
     * pozíciu ak sa pohybujeme v riadku alebo stĺpci.
     *
     * @return Ak možno, vráti true, inak false.
     */
    private boolean moznoVykonatTahVRadeAleboStlpci() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        boolean lpJeSmerProtiOsiX = (lpAktualnaXPozicia > aNovaXPozicia) ? true : false;
        boolean lpJeSmerProtiOsiY = (lpAktualnaYPozicia > aNovaYPozicia) ? true : false;

        if ((lpAktualnaYPozicia == aNovaYPozicia) && (lpAktualnaXPozicia != aNovaXPozicia)) {
            return skontrolujTahHorizontalne(lpJeSmerProtiOsiX);
        } else if ((lpAktualnaXPozicia == aNovaXPozicia) && (lpAktualnaYPozicia != aNovaYPozicia)) {
            return skontrolujTahVertikalne(lpJeSmerProtiOsiY);
        }
        return false;
    }

    /**
     * pomocná metóda, ktorá skontroluje či sa možno posunúť kráľovnou na novú
     * pozíciu ak sa pohybujeme po diagonále. Na zistenie použije ďalšie metódy.
     *
     * @return Ak možno, vráti true, inak false.
     */
    private boolean moznoVykonatTahPoDiagonale() {

        boolean lpSmerProtiOsiY = (aSucasnaYPozicia > aNovaYPozicia) ? true : false;
        boolean lpSmerProtiOsiX = (aSucasnaXPozicia > aNovaXPozicia) ? true : false;

        if ((!lpSmerProtiOsiX) && (!lpSmerProtiOsiY)) {
            return moznoSaPosunutSikmoDopravaNadol();
        } else if ((lpSmerProtiOsiX) && (!lpSmerProtiOsiY)) {
            return moznoSaPosunutSikmoDolavaNadol();
        } else if ((!lpSmerProtiOsiX) && (lpSmerProtiOsiY)) {
            return moznoSaPosunutSikmoDopravaNahor();
        } else if ((lpSmerProtiOsiX) && (lpSmerProtiOsiY)) {
            return moznoSaPosunutSikmoDolavaNahor();
        }

        return false;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * doprava nadol.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDopravaNadol() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
            int j = aSucasnaXPozicia + pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * dolava nadol.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDolavaNadol() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
            int j = aSucasnaXPozicia - pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * doprava nahor.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDopravaNahor() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
            int j = aSucasnaXPozicia + pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * doľava nahor.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDolavaNahor() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
            int j = aSucasnaXPozicia - pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * ak sa pohybujeme kráľovnou v stĺpci skontroluje sa ťah touto čiastkovou
     * metódou.
     *
     * @param lpJeSmerProtiOsiY
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean skontrolujTahVertikalne(boolean lpJeSmerProtiOsiY)//vertikalne == stojato(knihy)
    {
        if (!lpJeSmerProtiOsiY) {
            for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia] != null) {
                    if ((i == aNovaYPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } else if (lpJeSmerProtiOsiY) {
            for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia] != null) {
                    if ((i == aNovaYPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * ak sa pohybujeme kráľovnou v stĺpci skontroluje sa ťah touto čiastkovou
     * metódou
     *
     * @param lpJeSmerProtiOsiX
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean skontrolujTahHorizontalne(boolean lpJeSmerProtiOsiX) {
        if (!lpJeSmerProtiOsiX) {
            for (int i = aSucasnaXPozicia + 1; i <= aNovaXPozicia; i++) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i] != null) {
                    if ((i == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } else if (lpJeSmerProtiOsiX) {
            for (int i = aSucasnaXPozicia - 1; i >= aNovaXPozicia; i--) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i] != null) {
                    if ((i == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

}

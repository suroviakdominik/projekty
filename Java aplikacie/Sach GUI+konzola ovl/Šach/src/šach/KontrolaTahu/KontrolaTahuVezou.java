/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import šach.hra.Hrac;

/**
 * Kontroluje ťah vežou na základe aktuálnych a nových súradníc.
 *
 * @author Dominik
 */
public class KontrolaTahuVezou implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráča , ktorý
     * vykonáva ťah.
     *
     * @param paNovaXPozicia
     * @param paNovaYPozicia
     * @param paHrac
     */
    public KontrolaTahuVezou(int paNovaXPozicia, int paNovaYPozicia, Hrac paHrac) {
        this.aNovaXPozicia = paNovaXPozicia;
        this.aNovaYPozicia = paNovaYPozicia;
        this.aHrac = paHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    /**
     * skontroluje ťah vežou. Pri kontrole použije ďalšie metódy tejto triedy,
     * ktorých prístupový typ je private.
     *
     * @return Ak je ťah platný vráti true, inak vráti false.
     */
    @Override
    public boolean skontrolujTah() {

        boolean lpJeSmerProtiOsiX = (aSucasnaXPozicia > aNovaXPozicia) ? true : false;
        boolean lpJeSmerProtiOsiY = (aSucasnaYPozicia > aNovaYPozicia) ? true : false;

        if ((aSucasnaYPozicia == aNovaYPozicia) && (aSucasnaXPozicia != aNovaXPozicia)) {
            return skontrolujTahHorizontalne(lpJeSmerProtiOsiX);
        } else if ((aSucasnaXPozicia == aNovaXPozicia) && (aSucasnaYPozicia != aNovaYPozicia)) {
            return skontrolujTahVertikalne(lpJeSmerProtiOsiY);
        }
        return false;
    }

    /**
     * pomocná metóda, ktorá skontroluje či možno vykonať ťah vežou v stĺpci.
     * Parameter vraví, či ideme v smere osi y alebo proti smeru a použije sa v
     * algoritme na kontrolu ťahu.
     *
     * @param lpJeSmerProtiOsiY - či ideme v smere alebo proti smeru osi y
     * @return Vráti true ak možno ťah v stĺpi vykonať,inak false.
     */
    private boolean skontrolujTahVertikalne(boolean lpJeSmerProtiOsiY)//vertikalne == stojato(knihy)
    {
        if (!lpJeSmerProtiOsiY) {
            for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia] != null) {
                    if ((i == aNovaYPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } else if (lpJeSmerProtiOsiY) {
            for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia] != null) {
                    if ((i == aNovaYPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][aSucasnaXPozicia].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * pomocná metóda, ktorá skontroluje či možno vykonať ťah vežou v riadku.
     * Parameter vraví, či ideme v smere osi x alebo proti smeru a použije sa v
     * algoritme na kontrolu ťahu.
     *
     * @param lpJeSmerProtiOsiX - či ideme v smere alebo proti smeru osi x
     * @return Vráti true ak možno ťah v riadku vykonať, inak false.
     */
    private boolean skontrolujTahHorizontalne(boolean lpJeSmerProtiOsiX) {
        if (!lpJeSmerProtiOsiX) {
            for (int i = aSucasnaXPozicia + 1; i <= aNovaXPozicia; i++) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i] != null) {
                    if ((i == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } else if (lpJeSmerProtiOsiX) {
            for (int i = aSucasnaXPozicia - 1; i >= aNovaXPozicia; i--) {
                if (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i] != null) {
                    if ((i == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia][i].getFarba().equals(aHrac.getFarbaFigurok())))//zisti i mozno vyhadzovat
                    {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

}

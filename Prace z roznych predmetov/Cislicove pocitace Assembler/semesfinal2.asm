;toto su konstanty pre jednotlive znaky na displeje ( segment svieti pri log.0 )
;konstanty su ulozene od pametoveho miesta 0x00
BYTE 0xC0;0
BYTE 0xF9 ;1
BYTE 0xA4;2
BYTE 0xB0;3
BYTE 0x99;4
BYTE 0x92;5
BYTE 0x82;6
BYTE 0xF8;7
BYTE 0x80;8
BYTE 0x90;9
BYTE 0x88;A
BYTE 0x83;B
BYTE 0xC6;C
BYTE 0xA1;D
BYTE 0x86;E
BYTE 0x8E,F
BYTE 0xFF,nic

Reset:

;na nasledujuce 2 bajty pameti pocitaca bude ukladany vysledok
; inicializuje prve pametove miesto v pocitacovej pameti na 0
mvi b,0x01 ;do b nastavi adresu pametoveho miesta (offset je 1)
mvi d,0x00 ; do d ulozime konstantu, ktoru ulozime na pametove miesto
str b,d ; ulozi obsah registra d na pametove meisto ulozene v registri b

mvi b,0x00; do b nastavi adresu pametoveho miesta ( offset je 0)
mvi d,0x00;  do d ulozime konstantu, ktoru ulozime na pametove miesto
str b,d; ulozi obsah registra d na pametove meisto ulozene v registri b


HlavnyProgram:

cal nacitanie
cal porovnanie
cal Posuvanie
vypisss:
cal Vypisnadispleje

mvi b,0x0B ;do becka ulozi adresu 3.ho riadku
out 0x0004,b; aktivuje 3.cip(573 druhy sprava), zodpovedny za aktivovanie riadkov tlacidiel a ten treti cip
                     ;zabezpeci ze treti riadok bude aktivny
inn c,0x0001;z vysokodo-impedancneho stavu nastavi posledny cip (prvy sprava) do aktivneho stavu a prepise
	         ;data aktualne vstupujuce do tohto cipu na dat. zbernicu, kade sa prenesu do registra c.
	         ;Tieto data predstavuju stlpec v tretom riadku, ktory bol stlaceny, ak aspon jedna z hodnot je rovna 0 
cmi c,0x0F; porovna ci bolo nieco stlacene
jnz HlavnyProgram; ak bolo nieco v tretom riadku stlacene skoci na zaciatok hlavneo programu(nacitava odznovu 
                               ;dalsie cislo ktore pricita)

mvi b,0x07
out 0x04,b
inn c,0x01
cmi c,0x0F
jnz Reset



jmp vypisss

;----------------------------------------------------------------  konvertovanie a praca s displejmi	------------------------------
;						                Procedura scitovanie
;Procedura pripocita k vysledku ktory je dvojbajtovy a ulozeny v pameti obsah registra a, ktory je bajtovy
;Vykonava sa to tak, ze do registra c sa najskor ulozi prvych 8bitov pamete, obsah registra c sa scita 
;s obsahom registra a , ak doslo k preteceniu nastavi sa Caryy Flag na 1. Ak nedoslo k preteceniu
;vysledok v registri c zapise spet na prve pametove miesto offset 0. Ak doslo k preteceniu vysledok 
;v registri c zapise spet na prve pametove miesto offset 0 a do registra c sa ulozi obsah druheho pametoveho
;miesta a pripocita k cecku jednotku, ktora pretiekla avysledok zapise spet na druhe pametove miesto
Scitovanie:
;-------------------------------------------------------------------------------------------------------------------------------------------------------
pus b
pus c
pus d

mvi b,0x00; do b nastavi adresu prveho pametoveho miesta
ldr c,b;  do registra c nacita hodnotu z prveho pametoveho miesta
adc c,a; scita obsahy registrov c a a , a vysledok ulozi do regitra c 
jcy Akpretecie ;skoci ak Carry Flag = 1 (doslo k preteceniu)

str b,c ;ak nedoslo k preteceniu, tak vrati obsah cecka na prve pametove meisto, ktoreho adresa je v b
jmp navr



Akpretecie: ; ak doslo k preteceniu
str b,c ; doslo k preteceniu a vrati obsah cecka na prve pametove meisto, ktoreho adresa je v b
mvi b,0x01 ; do b ulozi adresu druheho pametoveho miesta
ldr c,b; nacita do registra c , obsah z druheho pametoveho meista ( ktoreho adresa je ulozne v b)
adi c,1; k cecku pripocita konstantu 1, pretoze doslo k preteceniu
str b,c; ulozi (zapise) obsahregistra c , spet na druhe pametove miesto ( ktoreho adresa je ulozena v b)

navr:

pop d ; obnovi obsahy registrov
pop c
pop b
ret

;-------------------------------------------------------------------------------------------------------------------------------------------------------
					;Posuvanie
;rozdeli binarne cislonachadzajuce sav pameti na prvych dvoch bajtoch(na offsete 0x00 a 0x01 
; na styri stvorice bitov , ktore sa ulozia do pametovych miest 0x02 az 0x05 (00 a01 su obsadene)
;a neskor sa pouziju na vypis na jednotlive displeje, tieto stvorice bitov predstavuju jednu cifru v 16kovej sus.
;princip spociva v tom ze pomocou masky and , vynuluje biti ktore patria inej cifre
;postup - nacita prve cislo z adresy 0x00 do registra a pomocou and-u vynuluje horne 4 biti (and 00001111)
;a vysledok ulozi na pametove miesto s adresou 0x02. Nacita znova prve cislo z adresy 0x00, tentokrat vsak
; ho najskor posunie o 4 biti doprava (shr a,4) tym ziskame horne 4 bity cisla , a vysledok ulozi na pamet. miesto
;a adresou 0x03. To iste urobi pre druhe cislo s adresy 0x01 a vysledky ulozi na pamet. miesta 0x04 a 0x05
;cislo na prvych dvoch pametovych miestach: 10100011(offset 0x00) 00011001 (offset 0x01)
;offset: 0x02 = 0011, offset: 0x03 = 1010, offset: 0x04 = 1001, offset: 0x05 = 0001 
;-------------------------------------------------------------------------------------------------------------------------------------------------------
Posuvanie:

pus a
pus b
pus c
pus d

mvi b,0x00; do b ulozi adresu prveho pametoveho miesta (prveho bajtu)
ldr a,b; do acka ulozi obsah z adresy b ( 1.ho pamet miesta)
mvi c,0x0F; do c ulozi masku (00001111)
and a,c; a vymaskuje pomocou masky ulozenej v c (cize vynuluje 4 horne bity)
mvi b,0x02; nastavi adresu pametoveho miesta , kam ulozi vysledok ( 1. cifru nasho 16koveho cisla) najmenej vyznamny bit
str b,a ; ulozi a-cko na adresu pametoveho miesta ulozenu v b-ecku

mvi b,0x00; do b ulozi adresu prveho pametoveho miesta (prveho bajtu)
ldr a,b; do acka ulozi obsah z adresy b ( 1.ho pamet miesta)
mvi c,0xF0; do c ulozi masku (11110000)
and a,c; a vymaskuje pomocou masky ulozenej v c (cize vynuluje 4 dolne bity)
shr a,4 ; posunieme obsah acka (bitovy posun o 4 vpravo) a tak ziskame 2.cifru nasho16koveho cisla
mvi b,0x03; do becka si ulozime adresu 4-ho miesta
str b,a; ulozi obsah acka na adresu pametoveho miesta v becku

mvi b,0x01; do becka ulozi adresu drueho pametoveho miesta
ldr a,b; do acka ulozi obsah z adresy b ( 2.ho pamet miesta)
mvi c,0x0F; do c ulozi masku (00001111)
and a,c; a vymaskuje pomocou masky ulozenej v c (cize vynuluje 4 horne bity)
mvi b,0x04; nastavi adresu pametoveho miesta , kam ulozi vysledok ( 3. cifru nasho 16koveho cisla) 
str b,a; ulozi a-cko na adresu pametoveho miesta ulozenu v b-ecku

mvi b,0x01; do becka ulozi adresu drueho pametoveho miesta
ldr a,b; do acka ulozi obsah z adresy b ( 2.ho pamet miesta)
mvi c,0xF0; do c ulozi masku (11110000)
and a,c; a vymaskuje pomocou masky ulozenej v c (cize vynuluje 4 dolne bity)
shr a,4 ; posunieme obsah acka (bitovy posun o 4 vpravo) a tak ziskame 2.cifru nasho16koveho cisla
mvi b,0x05; do becka si ulozime adresu 6-ho miesta
str b,a; ulozi obsah acka na adresu pametoveho miesta v becku

pop d
pop c
pop b
pop a

ret
;-------------------------------------------------------------------------------------------------------------------------------------------------------
						;Vypis na displeje

;procedura, vypisuje hodnoty z pametovych miest 0x02 az 0x05 na jednotlive displeje v 16kovej sus.
;Na datovu zbernicu nastavime data, ktore urcuju , ktory segment bude svietit,svieti ten segment , ktory ma 
;nastavene jedno z A1,A2,A3,A4 na log.0, pricom A4 jest uplne vpravo; A1 je uplne vlavo
;Na adresnu zbernicu nastavi adresu 0x02 (riadiaci cip) ,ktora adresuje druhy cip 573 zlava, signal IW
; nastavi do log.0 data (z dat.zbernice) zapise do druheho cipu , ktory si ich bude pametat do nasledujucej zmeny
;postup: do becka ulozi data , ktore urcuju ktory segment bude svietit , instrukciou (out ) aktivuje druhy riadiaci 
;cip a preda mu obsah registra b. Druhy cip si ich bude pametat do nasledujucej zmeny. Do registra b ulozi
;adresu pametoveho miesta, ktore obsahuje cislo , ktore sa vypise na segment , postupne aktivuje vsetky
;segmenty a vypise prisulusne cifry, segment prvy sprava adresa(0x02) , segment druhy sprava(0x03)
;segment treti sprava (0x04), segment stvrty sprava (0x05)
;-------------------------------------------------------------------------------------------------------------------------------------------------------
Vypisnadispleje:
pus a; odlozi obsahy registrov
pus b
pus c
pus d

;vypisovanie na 1.displej sprava
mvi b,0x0E ;do becka ulozi data , ktore urcuju ktory segment bude svietit
out 0x02,b ; na druhy riadiaci cip vysle data ulozene v becku (teda 0x0E co zopoveda 1.displeju sprava)
	       ;tym tento displej aktivuje

mvi b,0x02; do becka ulozi adresu pametoveho miesta , kde sa nachadza prva cifra 16kohe cisla
	      ; ktore vypiseme na 1.displej sprava ( vsetky cipy 573 maju pametove spravanie) pameta si 
                  ;co mu vysles , az kym to nezmenis
ldr a,b       ;do acka nacita obsah pametoveho miesta b (cize 1.cifru)


mmr b,a; do registra b ulozi adresu bajtu definovaneho na zaciatku, ktory obsahuje informacie o tom, co sa vypise na displeje
out 0x01,b; pomocou prveho cipu(573 prvy zlava) vypise obsah registra b na aktivny displej
                 ;na adresnu zbernicu nastavi adresu 0x01, na datovu zbernicu nastavi obsah registra b, 
                 ;signal IW da do log.0, cim sa aktivuje prvy cip (573) a zapisu sa nan data nachadzajuce sa v dat.zbernici
	     ;signal IW znovu padne do log.1 cim urobi prvy neaktivny, ale kedze ma tento cip pametove spravanie
	     ; ak sa data nestratia ale vypisuju sa na aktivny displej

;zhasnutie 1.displeja 
mvi a,0x10 ;do acka ulozi adresu konstanty na mieste 0x10,ktora reprezentuje nic nesvieti
mmr b,a; do registra b ulozi adresu bajtu definovaneho na zaciatku, ktory obsahuje informacie o tom, co sa vypise 
out 0x01,b ;vysvieti nic na 1.displeji


;vypisovanie na 2.displej sprava
mvi b,0x0D ;nastavi adresu druheho displeja sprava
out 0x02,b; aktivuje druhy displej sprava
mvi b,0x03; z tretieho pametoveho miesta ( 2.cifra sprava)
ldr a,b; do acka nacita obsah pametoveho miesta b (cize 2.cifru)

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

;vypisovanie na 3.displej sprava
mvi b,0x0B
out 0x02,b
mvi b,0x04
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

;vypisovanie na 4.displej sprava
mvi b,0x07
out 0x02,b
mvi b,0x05
ldr a,b

mmr b,a
out 0x01,b

;zhasnutie
mvi a,0x10
mmr b,a
out 0x01,b

pop d
pop c
pop b
pop a
 
ret
;-------------------------------------------------------------------------------------------------------------------------------------------------------
							;praca s klavesnicou
nacitanie: 

;nastavime data ktore predstavuju, ktory riadok bude aktivovany, riadok je aktivovany ked nadobuda log.0
;nastavime adresu cipu, ktory chcem aktivovat (out 0x0004,4 cize 3.ti cip(adresovatelny) na doske zlava)
;pomocou instrukcie out vysleme data na adresu cipu, tento cip si pameta posledne ulozene data, az kym 
;nedojde k dalsej zmene, postupne v cykle aktivujeme jednotlive riadky, a ak bolo stlacene tlacidlo 
;v tomto riadku,tak procedura konci a v registri c - bude nacitany stlpec a v registri b bude nacitany riadok
;------------------------------------------------------------------------------------------------------------------------------------------------------
mvi b,0x0E; do b nastavime adresu prveho riadku
out 0x0004,b ; adresuje 3.cip
inn c,0x0001 ; signal IR nastavi na log.0 , tym padom prejde posledny cip (125 uplne vpravo) z 
		;vysoko impedancneho stavu na aktivny stav a prepise data(o stlacenom stlpci) na
		;datovu zbernicu a tieto data ulozi doregistra c(0x0001 nema vplyv, lebo amme len jeden 125cip)
cmi c,0x0F ;porovnanie ci bolo niecostlacene v prvom riadku ( teda je rozne od F)
jnz Navrat ;ak bolo nieco stlacene procedura konci(v registri c-bude nac. stlpec a v registri,b bude nac. riadok)

mvi b,0x0D; nastavime adresu 2. riadku
out 0x0004,b; adresuje 3.cip
inn c,0x0001; signal IR nastavi na log.0 , tym padom prejde posledny cip (125 uplne vpravo) z 
		;vysoko impedancneho stavu na aktivny stav a prepise data(o stlacenom stlpci) na
		;datovu zbernicu a tieto data ulozi doregistra c(0x0001 nema vplyv, lebo amme len jeden 125cip)
		;cip 125 pracuje len s datovou zbernicou(bitmi 0 az 3) a signalom IR (0x0001 je nepodstatna)
cmi c,0x0F; porovna ci bolo nieco stlacene v druhom riadku
jnz Navrat; ak bolo nieco stlacene procedura konci(v registri c-bude nac. stlpec a v registri,b bude nac. riadok)


jmp nacitanie ; cyklus sa opakuje dokym nieje nieco stlacene
Navrat:

ret
;------------------------------------------------------------------------------------------------------------------------------------------------------
							;Procedura Porovnanie
;procedura vyhodnoti aketlacidlo bolo stlacene na zaklade obsahu registra c a b (ktore obsahuju stalcene stlpec
; a riadok), a potom pripocita k vysledku danu kostantu na zaklade vyhodnotenia
;------------------------------------------------------------------------------------------------------------------------------------------------------
porovnanie:

pus a ;odlozime obsahy registrov
pus b
pus c
pus d

cmi b,0x0E; porovna b s konstantou 0x0E, ktora zodpoveda prvemu riadku
jzr riadok1; ak sa rovnaju skoci na navestie riadok1
cmi b,0x0D; porovna b s konstantou 0x0D, ktora zodpoveda druhemu riadku
jzr riadok2; ak sa rovnaju skoci na navestie riadok2

riadok1:
cmi c,0x07 ;porovna c s konstantou 0x07, ktora zodpoveda prvemu stlpcu sprava
jzr pripocitaj1; ak sa rovnaju ( bolo stlacene tlacidlo v prvom riadku v prvom stlcpi sprava) skoci na navestie Pripocitaj1
cmi c,0x0B;porovna c s konstantou 0x0B, ktora zodpoveda prvemu 2.stlpcu sprava
jzr pripocitaj2; ak sa rovnaju ( bolo stlacene tlacidlo v prvom riadku v druhom stlcpi sprava) skoci na navestie Pripocitaj2
cmi c,0x0D;porovna c s konstantou 0x0D, ktora zodpoveda tretiemu stlpcu sprava
jzr pripocitaj4; ak sa rovnaju ( bolo stlacene tlacidlo v prvom riadku v tretom stlcpi sprava) skoci na navestie Pripocitaj4
cmi c,0x0E;porovna c s konstantou 0x0E, ktora zodpoveda stvrtemu stlpcu sprava
jzr pripocitaj8 ; ak sa rovnaju ( bolo stlacene tlacidlo v prvom riadku v stvrtom stlcpi sprava) skoci na navestie Pripocitaj8



riadok2:
cmi c,0x07 ;porovna c s konstantou 0x07, ktora zodpoveda prvemu stlpcu sprava
jzr pripocitaj16; ak sa rovnaju ( bolo stlacene tlacidlo v druhom riadku v prvom stlcpi sprava) skoci na navestie Pripocitaj16
cmi c,0x0B;porovna c s konstantou 0x0B, ktora zodpoveda prvemu 2.stlpcu sprava
jzr pripocitaj32; ak sa rovnaju ( bolo stlacene tlacidlo v druhom riadku v prvom stlcpi sprava) skoci na navestie Pripocitaj32
cmi c,0x0D;porovna c s konstantou 0x0D, ktora zodpoveda tretiemu stlpcu sprava
jzr pripocitaj64; ak sa rovnaju ( bolo stlacene tlacidlo v druhom riadku v prvom stlcpi sprava) skoci na navestie Pripocitaj64
cmi c,0x0E;porovna c s konstantou 0x0E, ktora zodpoveda stvrtemu stlpcu sprava
jzr pripocitaj128; ak sa rovnaju ( bolo stlacene tlacidlo v druhom riadku v prvom stlcpi sprava) skoci na navestie Pripocitaj128

navraat:

pop d; obnovi obsahy registrov
pop c
pop b
pop a

ret

pripocitaj1:
mvi a,1 ;do registra a ulozi konstantu 1
cal Scitovanie; zavola proceduru Scitovanie, ktora pripocita obsah registra a k doterajsiemu vysledku ulozenemu v pameti
jmp navraat ; vrati sa a skonci proceduru

pripocitaj2:
mvi a,2
cal Scitovanie
jmp navraat

pripocitaj4:
mvi a,4
cal Scitovanie
jmp navraat

pripocitaj8:
mvi a,8
cal Scitovanie
jmp navraat

pripocitaj16:
mvi a,16
cal Scitovanie
jmp navraat

pripocitaj32:
mvi a,32
cal Scitovanie
jmp navraat

pripocitaj64:
mvi a,64
cal Scitovanie
jmp navraat

pripocitaj128:
mvi a,128
cal Scitovanie
jmp navraat




/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;
import wof00.Questy.IQuest;

/**
 *
 * @author suroviak3
 */
public class VykonavacUkonci implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        IQuest vyplneny = (IQuest) paHrac.dajPredmet("bcTitul");
        if (vyplneny != null) {
            vyplneny.ukonci(paHrac);
        }
        return false;
    }

}

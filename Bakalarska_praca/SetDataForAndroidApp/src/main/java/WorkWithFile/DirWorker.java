/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WorkWithFile;

import java.io.File;
import java.util.ArrayList;

/**
 * Trieda sluzi na pracu z priecinkami
 * @author Dominik
 */
public class DirWorker {
    private File aDir=null;

    /**
     * Vytvori instanciu, ktora obsahuje uplnu cestu adresara s ktorzym chceme praovat
     * @param paAbsolutePath - cesta priecinka, s ktorym chceme pracovat
     */
    public DirWorker(String paAbsolutePath) {
        aDir=new File(paAbsolutePath);
    
    }

    /**
     * zmeni adresar, s ktorym tento objekt pracuje
     * @param paPath -adresar, s ktorym tento objekt bude nasledovne pracovat
     */
    public void setAbsolutePathOfDir(String paPath) {
        this.aDir = new File(paPath);
    }
    
    
    /**
     * 
     * @return absolutna cesta pricnika z ktorym pracujeme
     */
    public String getAbsolutePath()
    {
        return aDir.getAbsolutePath();
    }
    
    /**
     * 
     * @return true - ak cesta priecinka odkazuje na adresar
     *         false - ak cesta priecinka odkazuje na subor
     */
    public boolean isDirectory()
    {
       return aDir.isDirectory();
    }
    
    /**
     * vytvori adresar ak neexistuje cestou, ktora je nastavena ako atribut
     */
    public void vytvorAdresarAkNeexistuje()
    {
        if(!aDir.exists())
        {
            aDir.mkdirs();
        }
    }
    
    /**
     * 
     * @return true - k priecinok zo zadanou cestou existuje
     */
    public boolean existDir()
    {
        return aDir.exists();
    }
    
    /**
     * 
     * @return zoznam podadresatov aktuaalneho adresara
     */
    public ArrayList<String> getNazvyPodadresarovVAktualnomAdresari()
    {
        ArrayList<String> ret=new ArrayList<>();
        File files[]=aDir.listFiles();
        for(int i=0;i<files.length;i++)
        {
            if(files[i].isDirectory())
            {
                ret.add(files[i].getName());
            }
        }
        
        return ret;
    }
    
    /**
     * 
     * @return zoznam nazvov suborov v aktualnom adresari
     */
     public ArrayList<String> getNazvySuborovVAktualnomAdresari()
    {
        ArrayList<String> ret=new ArrayList<>();
        File files[]=aDir.listFiles();
        for(int i=0;i<files.length;i++)
        {
            if(!files[i].isDirectory())
            {
                ret.add(files[i].getName());
            }
        }
        
        return ret;
    }
    
    
    
}

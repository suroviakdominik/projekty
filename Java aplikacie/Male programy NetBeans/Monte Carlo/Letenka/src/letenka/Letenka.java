/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package letenka;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class Letenka {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //NEVIEM CI JE TENTO PRIKLAD RIESENY DOBRE
        Random genPravdepodobnostiPrichodu = new Random();
        Random genRezervacii = new Random();
        
        int pocetPokusov = 10000;
        double cenaLtenky = 150;
        double cestujucichMax = 100;
        double cenaLetuPrazdnehoLietadla = 8000;
        double nakladyLetuNaCestujuceho = 30;
        double kompenzaciaZaPrevysenieMaxPocLiet = 300;
        double pravdepodobnstDostaveniaNaLet = 0.95;
        double[] priemerneVysledkyPokusov = new double[30];
        double[] priemerneZiskyPokusov = new double[30];
        int[] cestujuci;
        int prislo;
        for (int i = 0; i < 30; i++) {
            cestujuci = new int[100 + i];
            
            for (int k = 0; k < pocetPokusov; k++) {
                
                for (int j = 0; j < 100 + i; j++) {
                    cestujuci[j] = genPravdepodobnostiPrichodu.nextInt(101);
                }
                prislo = 0;
                
                for (int j = 0; j < 100 + i; j++) {
                    if (cestujuci[j] > 5) {
                        prislo++;
                    }
                }
                priemerneVysledkyPokusov[i] += prislo;
                priemerneZiskyPokusov[i]+=prislo>100?((100+i)*cenaLtenky-((prislo-100)*kompenzaciaZaPrevysenieMaxPocLiet+cenaLetuPrazdnehoLietadla+nakladyLetuNaCestujuceho*cestujucichMax)):(100+i)*cenaLtenky-cenaLetuPrazdnehoLietadla-prislo*nakladyLetuNaCestujuceho;
            }
            System.out.println("Prislo priemerne: "+(priemerneVysledkyPokusov[i]/pocetPokusov)+" zo "+(100+i)+" rezervacii.Priemerny zisk: "+(priemerneZiskyPokusov[i])/pocetPokusov );
            
        }
        int maxZiskIndex=0;
            for (int j = 0; j < 30; j++) {
                if((priemerneZiskyPokusov[j]/pocetPokusov)>(priemerneZiskyPokusov[maxZiskIndex]/pocetPokusov))
                {
                    maxZiskIndex=j;
                }
            }
            System.out.println("Maximalny zisk: "+(priemerneZiskyPokusov[maxZiskIndex]/pocetPokusov)+
                    " spolocnost dostane ak rezervuje: "+(100+maxZiskIndex)+" leteniek");
    }
}

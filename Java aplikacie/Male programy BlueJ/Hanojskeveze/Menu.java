import java.util.Scanner;
import java.util.ArrayList;
public class Menu
{
    private Scanner scan;
    private ArrayList<Veze> aArrayListVezi;
    public Menu()
    {
        scan=new Scanner(System.in);
        aArrayListVezi=new ArrayList<Veze>();
        {

            for (int i=0;i<3;i++)
            {
                Veze veza=new Veze(""+(i+1));
                aArrayListVezi.add(veza);
            }
        }
    }

    public void vypisMenu()
    {
        System.out.println("Prava veza=1");
        System.out.println("Stredna veza=2");
        System.out.println("Lava veza=3");
    }

    public void spustiHru()
    {
        vypisMenu();
        System.out.println("Zadajte pocet kotucov, ktore chcete premiestnovat: ");
        int lpPocetKotucov=scan.nextInt();
        Veze strednaVeza=aArrayListVezi.get(1);
        strednaVeza.nacitajArrayList(lpPocetKotucov);
        System.out.println(strednaVeza.vypisPrvkovArrayListu());
        do
        {
            System.out.println("Premiestnit z veze cislo: ");
            int zVeze=scan.nextInt()-1;
            if((zVeze<3)&&((zVeze+1)>0))
            {
                Veze zVybranejVeze=aArrayListVezi.get(zVeze);
                if(zVybranejVeze.zistiPrvokArrayListu()!=0)
                {

                    System.out.println("Premiestnit na vezu cislo: ");
                    int naVezu=scan.nextInt()-1;

                    if(((naVezu)!=(zVeze))&&(naVezu<3)&&((naVezu+1)>0))
                    {
                        Veze naVybranuVezu=aArrayListVezi.get(naVezu);
                        naVybranuVezu.premiestnenieKotucaVezu(zVybranejVeze.zistiPrvokArrayListu());
                        zVybranejVeze.vymazPrvokArrayListu();
                        System.out.println("Veza "+(zVeze+1)+" obsahuje tieto prvky: "+zVybranejVeze.vypisPrvkovArrayListu());
                        System.out.println("Veza "+(naVezu+1)+" obsahuje tieto prvky: "+naVybranuVezu.vypisPrvkovArrayListu());
                    }
                    else
                    {
                        System.out.println("Vybrana veza neexistuje");
                    }
                } 
                else
                {
                    System.out.println("Veza je prazdna");  
                }
            }
            else
            {
                System.out.println("Vybrana veza neexistuje");
            }
        }
        while((aArrayListVezi.get(0).velkostArrayListu()!=lpPocetKotucov)&&(aArrayListVezi.get(2).velkostArrayListu()!=lpPocetKotucov));
        System.out.println("Hlavolam vyrieseny");
    }
    
}

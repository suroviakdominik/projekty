/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import java.util.ArrayList;
import java.util.Scanner;
import šach.hra.Hrac;
import šach.sachovnica.figurky.Dáma;
import šach.sachovnica.figurky.Figurka;
import šach.sachovnica.figurky.Kon;
import šach.sachovnica.figurky.Kralovna;
import šach.sachovnica.figurky.Veza;

/**
 * Kontroluje ťah pešiakom na základe aktuálnych a nových súradníc.
 *
 * @author Dominik
 */
public class KontrolaTahuPesiakom implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráča , ktorý
     * vykonáva ťah.
     *
     * @param paNovaXPozicia
     * @param paNovaYPozicia
     * @param paHrac - hráč, ktorý vykonáva ťah.
     */
    public KontrolaTahuPesiakom(int paNovaXPozicia, int paNovaYPozicia, Hrac paHrac) {
        this.aNovaXPozicia = paNovaXPozicia;
        this.aNovaYPozicia = paNovaYPozicia;
        this.aHrac = paHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    /**
     * skontroluje ťah pešiakom. Pri kontrole použije ďalšie metódy tejto
     * triedy, ktorých prístupový typ je private. Ak je ťah platný vráti true,
     * inak vráti false.
     *
     * @return true ak platný ťah, false inak
     */
    @Override
    public boolean skontrolujTah() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        boolean lpVykonatTah = false;

        if ((Math.abs(lpAktualnaYPozicia - aNovaYPozicia) == 1) && ((lpAktualnaXPozicia == aNovaXPozicia))) {
            lpVykonatTah = moznoVykonatPosunOJednoDopredu();
        } else if ((Math.abs(lpAktualnaYPozicia - aNovaYPozicia) == 2) && (lpAktualnaXPozicia == aNovaXPozicia)) {
            lpVykonatTah = jeMoznyPosunODveDopredu();
        } else if ((Math.abs(lpAktualnaYPozicia - aNovaYPozicia) == 1) && (Math.abs(lpAktualnaXPozicia - aNovaXPozicia) == 1)) {
            lpVykonatTah = moznoVyhadzovat();
        }

        if ((lpVykonatTah) && (aHrac.getaVybranaFigurka().getFarba().equals("red") && lpAktualnaYPozicia == 6) || ((lpVykonatTah) && (aHrac.getaVybranaFigurka().getFarba().equals("green") && lpAktualnaYPozicia == 1))) {
            System.out.println("Zadajte voľbu prislúchajúcu figúrke na ktorú chcete zmeniť pešiaka:");
            System.out.println("0 - zrusitTah");
            System.out.println("1 - kráľovná");
            System.out.println("2 - veža");
            System.out.println("3 - kôň");
            System.out.println("4 - dáma");

            boolean lpVykonavat = true;
            int volba;
            do {
                Scanner scan = new Scanner(System.in);
                System.out.print(">");
                volba = scan.nextInt();

                if (volba != 0) {
                    lpVykonavat = zmenaPesiaka(volba);
                    if (lpVykonavat) {
                        System.out.println("Pesiak sa zmenil.");
                    } else {
                        System.out.println("Pesiak sa nemoze zmenit na zadanu figurku lebo ich je na sachovnici plny pocet.");
                        System.out.println("Zopakujte volbu.");
                    }
                } else {
                    lpVykonatTah = false;
                }

            } while ((!lpVykonavat) && (volba != 0));

        }

        return lpVykonatTah;
    }

    /**
     * kontroluje, či možno zmeniť pešiaka na inú figúrku ak prešiel na druhú
     * stranu šachovnice. Hráč si vyberie figúrku na ktorú chce pešiaka zmeniť.
     * Metóda však skontroluje či by nebol na šachovnici prekročený maximálny
     * počet figúrok daného typu. Ako možno zmeniť, vráti true.
     *
     * @param paNazov -názov novej figurky
     * @param maxPocetFigurokNaSachovnici - aký je maximálny povolený počet
     * figúrok na šachovnici
     * @return
     */
    public boolean moznoZmenitPesiakaNaInuFigurku(String paNazov, int maxPocetFigurokNaSachovnici) {
        int pocetFigurokSPaNazvomNaSachovnici = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok())) && (aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getMeno().equals(paNazov))) {
                    pocetFigurokSPaNazvomNaSachovnici += 1;
                }
            }
        }
        if (pocetFigurokSPaNazvomNaSachovnici < maxPocetFigurokNaSachovnici) {
            return true;
        }
        return false;
    }

    /**
     * vyhodnotí voľbu hráča v ktorej si zvolil novú figúrku, či možno pešiaka
     * na túto figúrku zmeniť.
     *
     * @param paVolba - nová figúrka, ak možno pešiaka meniť
     * @return
     */
    public boolean zmenaPesiaka(int paVolba) {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        switch (paVolba) {
            case 1:
                if (moznoZmenitPesiakaNaInuFigurku("Kralovna", 1)) {
                    Kralovna kralovna = new Kralovna(aHrac.getFarbaFigurok());
                    aHrac.setaVybranaFigurka(kralovna);

                    return true;
                }
                return false;

            case 2:
                if (moznoZmenitPesiakaNaInuFigurku("Veza", 2)) {
                    Veza veza = new Veza(aHrac.getFarbaFigurok());
                    aHrac.setaVybranaFigurka(veza);

                    return true;
                }
                return false;

            case 3:
                if (moznoZmenitPesiakaNaInuFigurku("Kôň", 2)) {
                    Kon kon = new Kon(aHrac.getFarbaFigurok());
                    aHrac.setaVybranaFigurka(kon);

                    return true;
                }
                return false;

            case 4:
                if (moznoZmenitPesiakaNaInuFigurku("Dáma", 2)) {
                    Dáma dama = new Dáma(aHrac.getFarbaFigurok());
                    aHrac.setaVybranaFigurka(dama);

                    return true;
                }
                return false;

            default:
                System.out.println("Takato volba neexistuje. Opakujte volbu:");
                return false;
        }
    }

    /**
     * pomocná metóda, ktorá kontroluje, či je možný posun pešiakom o dve miesta
     * dopredu.
     *
     * @return True, ak je ťah možný, false inak.
     */
    private boolean jeMoznyPosunODveDopredu() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
        int lpAktualnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;

        if (lpAktualnaYPozicia == 1 || lpAktualnaYPozicia == 6) {
            if ((aHrac.getaVybranaFigurka().getFarba().equals("red")) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia + 2][lpAktualnaXPozicia] == null)) {
                return true;
            } else if ((aHrac.getaVybranaFigurka().getFarba().equals("green")) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia - 2][lpAktualnaXPozicia] == null)) {
                return true;
            }
        }
        return false;
    }

    /**
     * pomocná metóda, ktorá kontroluje, či je možný posun pešiakom o jedno
     * miesto dopredu.
     *
     * @return True, ak je ťah možný, false inak.
     */
    private boolean moznoVykonatPosunOJednoDopredu() {

        if ((aHrac.getFarbaFigurok().equals("red")) && (aNovaYPozicia - aSucasnaYPozicia == 1) && (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia + 1][aSucasnaXPozicia] == null)) {
            return true;
        } else if ((aHrac.getFarbaFigurok().equals("green")) && (aSucasnaYPozicia - aNovaYPozicia == 1) && (aHrac.getaSachovnicaNaKtorejSaHra()[aSucasnaYPozicia - 1][aSucasnaXPozicia] == null)) {
            return true;
        }
        return false;
    }

    /**
     * pomocná metóda ktorá kontroluje či možno vyhadzovať/ možno vykonať ťah
     * pešiakom o jedno po diagonále.
     *
     * @return True, ak je ťah možný, false inak.
     */
    private boolean moznoVyhadzovat() {
        int lpAktualnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;

        if ((aHrac.getFarbaFigurok().equals("red")) && (aNovaYPozicia - lpAktualnaYPozicia == 1) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia + 1][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia + 1][aNovaXPozicia].getFarba().equals("green"))) {
            return true;
        } else if ((aHrac.getFarbaFigurok().equals("green")) && (lpAktualnaYPozicia - aNovaYPozicia == 1) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia - 1][aNovaXPozicia] != null) && (aHrac.getaSachovnicaNaKtorejSaHra()[lpAktualnaYPozicia - 1][aNovaXPozicia].getFarba().equals("red"))) {
            return true;
        }

        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pohyblivaradciarka;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class FileWorker {

    public static void writeDataToFile(File f, boolean paAppend, String paData) {
        Writer wr = null;

        try {
            wr = new OutputStreamWriter(new FileOutputStream(f, paAppend), "UTF-8");
            wr.write(paData);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            if (wr != null) {
                try {
                    wr.flush();
                    wr.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
            }
        }
    }

    public static String readDataFromFile(File f, String paOddelovacRiadkov) {
        Reader r = null;
        String ret = "";
        try {
            r = new InputStreamReader(new FileInputStream(f), "UTF-8");
            BufferedReader bufRead = new BufferedReader(r);
            String readLine = null;
            while ((readLine = bufRead.readLine()) != null) {
                ret += readLine + paOddelovacRiadkov;
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                r.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return ret;
    }

    public static ArrayList<String> readAllDoubleAndNonDoubleValues(File f) {
        ArrayList<String> ret = new ArrayList<>();
        Reader r = null;

        try {
            r = new InputStreamReader(new FileInputStream(f), "UTF-8");
            BufferedReader bufRead = new BufferedReader(r);
            String readLine = null;
            while ((readLine = bufRead.readLine()) != null) {
                if (!readLine.matches("^\\s*#\\S*")) {
                    ret.add(readLine);
                }

            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                r.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        return ret;

    }
}

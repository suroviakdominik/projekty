/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Hra.Hrac;

/**
 *
 * @author Dominik
 */
public class PredmetNapoj implements IPredmet, Serializable {

    private String aNazov;
    private int aVelkost;

    public PredmetNapoj(String aNazov, int aVelkost) {
        this.aNazov = aNazov;
        this.aVelkost = aVelkost;
    }

    @Override
    public String getNazov() {
        return aNazov; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPopis() {
        return "napoj zvysujuci zivot o 10";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        paHrac.zvysZivot(aVelkost);
        paHrac.odstranZInventara(aNazov);
        return "Zivot sa vam zvysil o: " + aVelkost;
    }

}

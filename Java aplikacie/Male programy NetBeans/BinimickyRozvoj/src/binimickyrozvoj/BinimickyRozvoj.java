/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binimickyrozvoj;

/**
 *
 * @author Dominik
 */
public class BinimickyRozvoj {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String data=null;
       while(data==null){
        data=ConsoleWorker.nacitajDataZKonzoly("exponenct vyrazu, ktory chcete rozlozit do binomickeho rozvoja","\\d+");
       }
       int n=Integer.parseInt(data);
       String vys="";
        for (int i = 0; i <= n; i++) {
            vys+=vypoceitajNNadK(n, i)+"(a^"+(n-i)+"b^"+(i)+")\n+";
        }
        System.out.println(vys);
        
    }
    
    private static long vypoceitajNNadK(long paN, long paK)
    {
       long mensi=paK<(paN-paK)?paK:(paN-paK);
       long vacsi=paK>(paN-paK)?paK:(paN-paK);
       long mensiFakt=vypocitajNFaktorial(mensi,2);
       long citatel=vypocitajNFaktorial(paN, vacsi+1);
       return citatel/mensiFakt;
    }
    
    private static long vypocitajNFaktorial(long paN,long paZacOd)
    {
        long ret=1;
        for (long i = paZacOd; i <= paN; i++) {
            if(Long.MAX_VALUE/ret<i)
            {
                System.err.println("Pretecenie pri vypocte faktorialu: "+paN+" . Zacali sme od: "+paZacOd);
                System.exit(-1);
            }
            ret*=i;
        }
        return ret;
    }
}

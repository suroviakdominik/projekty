Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Retaz DB 10 DUP(?);deklaracia pola o velkosti 8B
RetazBezZadZnak DB 10 DUP(?)
.CODE;//zacina kodovy segment
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je retaz, nemozno ju priradit rovno do ds ak ds,@data 
          mov di, 2
          mov Retaz,6;do retaze na 1B ulozit max 6 preto lebo ahoj+0Dh+velkost retazca = 6
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          
VypisZnak:
          mov dl,[Retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp dl,0Dh;porovnaj hodnotu v dl s 0Dh
          jz NacitajPismeno
          mov ah,2
          int 21h
          
          inc di;zvys index o 1/prejdi na dalsi znak
          jmp VypisZnak

NacitajPismeno:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          
          mov di,2
          mov si,2;di a si su indexove registre	
          mov AH,1;sluzba na nacitanie jedneho znaku s klavesnice
          int 21h;
          mov BL,AL;lebo ak do registra ah ulozime cislo sluzby , pokazi sa aj regiter al
          
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          

ZisitVyskytZnaku:
          mov dl,[Retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
          
          cmp dl,BL;porovnaj hodnotu v dl s BL=s nacitanym znakom
          jz InkrementujIndexAkSudlbhRovnake
          mov [RetazBezZadZnak+si],dl;nemohli by sme dat mov [RetazBezZadZnak+si],[Retaz+di] lebo nemozme pristupovat v jednej instrukcie 2 krat do pamate
          inc si
          
InkrementujIndexAkSudlbhRovnake:
          cmp dl,0DH
          jz InicializujDI          
          inc di;zvys index o 1/prejdi na dalsi znak
          jmp ZisitVyskytZnaku




InicializujDI:
         mov di,2

VypisSkopirovanyRetazec:
          
          mov dl,[RetazBezZadZnak+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp dl,0DH;porovnaj hodnotu v dl s 0DH
          jz Koniec
          mov ah,2
          int 21h
          
          inc di;zvys index o 1/prejdi na dalsi znak
          jmp VypisSkopirovanyRetazec

Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok
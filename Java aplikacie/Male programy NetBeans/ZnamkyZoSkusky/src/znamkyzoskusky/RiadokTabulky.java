/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyzoskusky;

/**
 *
 * @author Dominik
 */
public class RiadokTabulky {
    private String aZnamkaOznacenie;
    private int aPocetnost;

    public RiadokTabulky(String aZnamkaOznacenie, int aPocetnost) {
        this.aZnamkaOznacenie = aZnamkaOznacenie;
        this.aPocetnost = aPocetnost;
    }
    
    
    
    public enum NazvyStlpcov{
        Znamka("Známka"){
            @Override
            public String getHodnota(RiadokTabulky paRiadok)
            {
                return paRiadok.aZnamkaOznacenie;
            }
        },
        
        Pocetnost("Početnosť"){
        @Override
            public String getHodnota(RiadokTabulky paRiadok)
            {
                return ""+paRiadok.aPocetnost;
            }
        };
        
        private String aNazovStlpca;
        private NazvyStlpcov(String paNazovStlpca)
        {
            aNazovStlpca=paNazovStlpca;
        }

        public String getaNazovStlpca() {
            return aNazovStlpca;
        }
        
        public abstract String getHodnota(RiadokTabulky paRiadok);
        
        
    }
    
    public enum TypZnamky {

        A("A"),
        B("B"),
        C("C"),
        D("D"),
        E("E"),
        FX("FX");
        private String aZnamka;

        private TypZnamky(String paZnamka) {
            aZnamka = paZnamka;
        }

        public String getaZnamka() {
            return aZnamka;
        }

    }
}

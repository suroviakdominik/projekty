/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import java.util.HashMap;
import java.util.Scanner;

import šach.hra.Hrac;

/**
 *
 * @author Dominik
 */
public class NacitavacPrikazov {

    private Hrac aHracKtoryZadavaPrikazy;
    private HashMap<String, IPrikaz> aPrikazy;
    private Scanner aScan;

    /**
     * Vytvorí inštanciu triedy Scanner a priradí ju atribútu aScan. Okrem toho
     * vytvorí kontajner typu HashMap do ktorého uloží všetky možné príkazy,
     * pričom každý príkaz predstavuje objekt ale zároveň majú všetky spoločný
     * interface IPrikaz, ktorý sa použije ako typ objektu v kontajneri.
     */
    public NacitavacPrikazov() {
        aScan = new Scanner(System.in);

        aPrikazy = new HashMap<>();
        pridajPrikaz("chod", new PrikazChod());
        pridajPrikaz("info", new PrikazInfoOFigurke());
        pridajPrikaz("pomoc", new PrikazPomoc());
        pridajPrikaz("ukonci", new PrikazUkonci());
        pridajPrikaz("uloz", new PrikazUloz());
        pridajPrikaz("vyber", new PrikazVyberFigurku());

    }

    /**
     * je typu private a dokáže pridať do kontajneru s príkazmi nový príkaz
     * ktorého triedu máme naprogramovanú a implementuje interface IPrikaz. V
     * kontajneri bude uložený pod kľúčom prijatým ako parameter paNazov.
     *
     * @param paNazov kľúč prvku v kontajneri príkazov
     * @param paPrikaz - objekt implementujúci IPrikaz
     */
    private void pridajPrikaz(String paNazov, IPrikaz paPrikaz) {
        aPrikazy.put(paNazov, paPrikaz);
    }

    /**
     * inicializuje hráča ktorý je aktuálne na ťahu a zadáva príkazy.
     *
     * @param paHrac -hráč zadávajúci príkazy
     */
    public void vlozHracaKtoryZadavaPrikazy(Hrac paHrac) {
        aHracKtoryZadavaPrikazy = paHrac;
    }

    /**
     * požiada používateľa o zadanie príkazu, ktorý následne vyhodnotí. Ak
     * príkaz neexistuje požiada používateľa znova.
     */
    public void nacitajPrikaz() {

        boolean lpUkoncitZadavanieprikazu = false;
        String lpPrikaz = null;
        String lpParameter = null;
        String lpVstupnyRiadok;
        System.out.println("Môžete použiť tieto príkazy: " + dajPrikaz());
        do {
            System.out.println("Zadajte príkaz: ");
            System.out.print(">");
            lpVstupnyRiadok = aScan.nextLine();

            Scanner lpCitacRiadku = new Scanner(lpVstupnyRiadok);
            if (lpCitacRiadku.hasNext()) {
                lpPrikaz = lpCitacRiadku.next();

                if (lpCitacRiadku.hasNext()) {
                    lpParameter = lpCitacRiadku.next();
                }
            }

            if (aPrikazy.containsKey(lpPrikaz)) {

                lpUkoncitZadavanieprikazu = aPrikazy.get(lpPrikaz).vykonaj(aHracKtoryZadavaPrikazy, lpParameter);

            } else {
                System.out.println("Taky prikaz neexistuje. Zadajte platny prikaz.");
            }
        } while (!lpUkoncitZadavanieprikazu);

    }

    /**
     * vráti všetky existujúce príkazy vo forme Stringu.
     *
     * @return reťazec všetkých existujúcih príkazov, ktoré dokáže program
     * spracovať
     */
    private String dajPrikaz() {
        String prikazy = "";
        for (String prechadzac : aPrikazy.keySet()) {
            prikazy += prechadzac + " ";
        }
        return prikazy;
    }

}

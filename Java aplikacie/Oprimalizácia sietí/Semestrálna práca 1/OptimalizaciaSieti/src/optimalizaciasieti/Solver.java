/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti;

import optimalizaciasieti.data.Mesto;
import optimalizaciasieti.data.Vrchol;
import optimalizaciasieti.data.Hrana;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author Dominik
 */
public class Solver {

    public static HashMap<Integer, Vrchol> filtrujObce(File paFileOfAllCities, String paRegionID) {
        HashMap<Integer, Vrchol> ret = new HashMap<>();
        int pocitadlo = 0;
        try {
            Pattern p;
            Matcher m;

            FileWorker fW = new FileWorker(paFileOfAllCities);
            fW.startRead();
            String riadok;
            String[] paDataRiadku = new String[4];
            while ((riadok = fW.readNextLine()) != null) {
                pocitadlo++;
                paDataRiadku = riadok.split("\\t");
                if (paDataRiadku[2].equals(paRegionID)) {
                    ret.put(Integer.parseInt(paDataRiadku[0]), new Vrchol(Integer.parseInt(paDataRiadku[0]),
                            new Mesto(paDataRiadku[0], paDataRiadku[1], Integer.parseInt(paDataRiadku[3]))));
                }
            }
            fW.stopRead();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        System.out.println("Pocet obiec a mist v regione s ID: "+paRegionID+ " je: "+ret.size());
        System.out.println("Pri filtrovani obiec a miest som prehladal: "+pocitadlo+" riadkov\n\n");

        return ret;
    }

    public static HashMap<Integer, Hrana> najdiHrany(File aFileToProcessing, Map<Integer, Vrchol> aNodes) {
        try {
            int pocitadloPrehlRiadkov = 0;
            int pocetZlychRiadkov = 0;
            int pocetNajdenychHran=0;
            HashMap<Integer, Hrana> hranyRet = new HashMap<>();
            FileWorker fW = new FileWorker(aFileToProcessing);
            fW.startRead();
            String loadedLine;
            while ((loadedLine = fW.readNextLine()) != null) {
                pocitadloPrehlRiadkov++;
                if (loadedLine.matches("\\d+\\s+\\d+\\s+\\d+\\s*")) {
                    String[] pom = new String[3];
                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(loadedLine);
                    for (int i = 0; i < 3; i++) {
                        if (m.find()) {
                            pom[i] = m.group();
                        }
                    }
                    if ((aNodes.get(Integer.parseInt(pom[1])) == null) && aNodes.get(Integer.parseInt(pom[2])) != null) {
                        aNodes.put(Integer.parseInt(pom[1]), new Vrchol(Integer.parseInt(pom[1]), null));
                    }
                    if (aNodes.get(Integer.parseInt(pom[2])) == null && (aNodes.get(Integer.parseInt(pom[1])) != null)) {
                        aNodes.put(Integer.parseInt(pom[2]), new Vrchol(Integer.parseInt(pom[2]), null));
                    }
                    if(aNodes.get(Integer.parseInt(pom[2])) == null && (aNodes.get(Integer.parseInt(pom[1])) == null)) {
                        aNodes.put(Integer.parseInt(pom[1]), new Vrchol(Integer.parseInt(pom[1]), null));
                        aNodes.put(Integer.parseInt(pom[2]), new Vrchol(Integer.parseInt(pom[2]), null));
                    }
                    if (aNodes.containsKey(Integer.parseInt(pom[1]))
                            && aNodes.containsKey(Integer.parseInt(pom[2]))) {
                        hranyRet.put(Integer.parseInt(pom[0]), new Hrana(Integer.parseInt(pom[0]),aNodes.get(Integer.parseInt(pom[1])),
                                aNodes.get(Integer.parseInt(pom[2]))));
                        pocetNajdenychHran++;
                    }
                } else {
                    pocetZlychRiadkov++;
                }

            }
            fW.stopRead();
            System.out.println("Hrany: pocet prehladanych riadkov: " + pocitadloPrehlRiadkov);
            System.out.println("Hrany: pocet neplatnych riadkov: " + pocetZlychRiadkov);
            System.out.println("Nasiel som: "+pocetNajdenychHran+" hran obshujucich vrcholy vybraneho regionu\n\n");
            return hranyRet;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void priradHranamVzdialenosti(File aFileToProcessing, HashMap<Integer, Hrana> aHrany) {
        FileWorker fW = new FileWorker(aFileToProcessing);
        try {
            int pocZlychRiadkov=0;
            int pocPrejdenRiadkov=0;
            int pocetZaznamenanychRiadkov=0;
            fW.startRead();
            String loadedLine;
            Integer loadedNumber;
            while ((loadedLine = fW.readNextLine()) != null) {
                pocPrejdenRiadkov++;
                if(loadedLine.matches("\\d+\\s+\\d+(\\.\\d+){0,1}\\s*"))
                {
                    Scanner s=new Scanner(loadedLine);
                    if(s.hasNextInt())
                    {
                        loadedNumber=s.nextInt();
                        if(aHrany.get(loadedNumber)!=null)
                        {
                            if(s.hasNext())
                            {
                                try{
                                    double d=Double.parseDouble(s.next());
                                    aHrany.get(loadedNumber).setaVzdialenost(d);
                                    pocetZaznamenanychRiadkov++;
                                }
                                catch(NumberFormatException ex)
                                {
                                    pocZlychRiadkov++;
                                }
                                
                            }else{
                                pocZlychRiadkov++;
                            }
                        }
                    }else
                    {
                        pocZlychRiadkov++;
                    }
                }else{
                    System.out.println("NESPRAVNY FORMAT= CHYBA PRIDELENIE VZDIALENOSTI HRANE: "+loadedLine);
                }
            }
            System.out.println("Pocet zlych riadkov vzdialenosti: "+pocZlychRiadkov);
            System.out.println("Pocet prejdenych riadkov vzdialenosti: "+pocPrejdenRiadkov);
            System.out.println("Nastavil som vzdialenosti: "+pocetZaznamenanychRiadkov+" hranam z "+aHrany.size()+" hran\n\n");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Solver.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fW.stopRead();
        }
    }

    public static Floyd inicializaciaFloyd(List<Vrchol> paNodes, HashMap<Integer, Hrana> paHrany,int paPodmaticaN) {
        Floyd f=new Floyd(paNodes,paHrany,paPodmaticaN);
        f.inicializuj();
        f.spustiVypocet();
        return f;
    }
}

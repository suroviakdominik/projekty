#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>


//FUNKCIA MAIN
//**************************************************
///Aplikacia, ktora sluzi ako 
///operacia delete nad danym semaforom teda vymaze
///dany semafor zo systemu a nebude ho uz mozne pouzit
///Funkcia prijima pr�ve jeden argument ktor� je povinn�
///a predstavuje identifikator semafora nad ktorym sa
///ma operacia delete vykonat
/// \param argv predstavuje pole argumentov v tomto pripade pozostava iba z identifikatora semafora.
/// \return 0 v pripade uspesneho ukoncenia ,-1 v pripade chyby.
int main(int argc, char *argv[])
{
   int idMazSemafora = atoi(argv[1]);
   //char premenna[2];
   //printf("Skutocne chcete zmazat mnozinu semaforov s ID: %d ? (odpoved: A-ano,ine-nie)\n",idMazSemafora);
   //scanf("%79s" ,premenna);
   //if(premenna[0] == 'A' || premenna[0] == 'a')
   //{
       if(semctl(idMazSemafora,0,IPC_RMID)<0)
	{
          printf("Error. Mnozinu semaforov sa nepodarilo zmazat(pravdepodobne neexistuje). semctl vratilo -1 \n");
	  return -1;
	}
        printf("Mnozina semaforov s ID: %d BOLA  zmazana. \n",idMazSemafora);
   //}
   
   return 0;
}
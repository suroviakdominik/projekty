.MODEL small
.STACK 100h
.DATA
VypisZadSurX DB 'Zadajte suradnicu X bodu v rovine: $'
VypisZadSurY DB 'Zadajte suradnicu Y bodu v rovine: $' 
VypisKvadrant1 DB '1. kvadrant $'
VypisKvadrant2 DB '2. kvadrant $'
VypisKvadrant3 DB '3. kvadrant $'
VypisKvadrant4 DB '4. kvadrant $'
VypisKvadrant0 DB 'Suradnica 0-Neviem rozhodnut $'
VypisAno DB 'Ano$'
VypisNie DB 'Nie$'
VypisNvm DB 'Vysledok nasobenia sa nezmesti do typu byte. Neviem rozhodnut$'
Retaz DB 7 DUP(?);deklaracia pola o velkosti 9B
DesiatPriNas DW 10
RetazPriCitaniZoSuboru DB 10 dup (?)
JednoCisloNaKonv DB 5 dup (?)
SuradnicaX DB ?
SuradnicaY DB ?
Postupnost DW 10 dup (?)
MenoSuboru DB 'Subor.txt',0
PopisovacSuboru DW (-1)
errorOtvSub DB 'Subor sa nepodarilo pre citanie otvorit!$'
.CODE
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,5;do retaze na 1B ulozit max 5 preto lebo cislo+0Dh+velkost retazca = 5
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 

NacitanieSurVRovine PROC
push ax
push dx
SurX:
mov dx,offset VypisZadSurX
mov ah,9
int 21h

call Read
cmp ax,127
jg SurX
cmp ax,-128
jl SurX
mov SuradnicaX,al

SurY:
mov dx,offset VypisZadSurY
mov ah,9
int 21h
call Read
cmp ax,127
jg SurY
cmp ax,-128
jl SurY
mov SuradnicaY,al
pop dx
pop ax
ret
NacitanieSurVRovine ENDP

ZistiKvadrant PROC
push ax
push dx
cmp SuradnicaX,0
jg PrvAlStvrt
jl DruhTret
je Nvm
 PrvAlStvrt:
  cmp SuradnicaY,0
  jg Prv
  jl Stv 
  je Nvm
   Prv:
      mov dx,offset VypisKvadrant1
      mov ah,9
      int 21h
      jmp Navr
   Stv:
      mov dx,offset VypisKvadrant4
      mov ah,9
      int 21h
      jmp Navr
  DruhTret:
      cmp SuradnicaY,0
      jg Druh
      jl Tret 
      je Nvm
     Druh:
      mov dx,offset VypisKvadrant2
      mov ah,9
      int 21h
      jmp Navr
    Tret:
      mov dx,offset VypisKvadrant3
      mov ah,9
      int 21h
      jmp Navr
    Nvm:
      mov dx,offset VypisKvadrant0
      mov ah,9
      int 21h
   Navr:
       mov DL,0AH;uloz do registra posun dole
       mov AH,2;do registra AH uloz cislo sluzby
       int 21h
       pop dx
       pop ax
       ret
ZistiKvadrant ENDP

LeziNaPriamke PROC;lezi na priamke y=2x+3
  push ax
  push dx
  mov al,2
  imul SuradnicaX
  add ax,3
    
  cmp ah,0
  jne PorovFF
  jmp Por 
  PorovFF:
   cmp ah,11111111b
   jne Nev
  Por:
  cmp al,SuradnicaY
  je VypAno
  jmp VypNie
  
  VypAno:
     mov dx, offset VypisAno
     mov ah,9
     int 21h
     jmp Re
  VypNie:
     mov dx, offset VypisNie
     mov ah,9
     int 21h
     jmp Re
  Nev:
   mov dx, offset VypisNvm
   mov ah,9
   int 21h
   Re:
     mov DL,0AH;uloz do registra posun dole
     mov AH,2;do registra AH uloz cislo sluzby
     int 21h
     pop dx
     pop ax
     ret
LeziNaPriamke ENDP

OpenFileForRead PROC 
        push ax
        mov al,0
        mov ah,3Dh
        int 21h
        mov PopisovacSuboru,ax;ak subor s menom este neexistuje v ax bude 2, pri pokuse citania zo suboru sa bude citat z klavesnice
        lahf;skopiruje register priznakov flags do registra ah
        AND ah,00000001b;vsetky ostatne az na carryflag vynuluje, ak cf=1 subor pre citanie sa nepodarilo otvorit
        
        cmp ah,1; ak je cf=1 vypise chybovu hlasku
        je VypisChybHlasku
        
        
        pop ax
        ret
  
 VypisChybHlasku:
        mov dx,offset errorOtvSub
        mov ah, 9
        int 21h 
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h 
        
        pop ax
        ret
        
OpenFileForRead ENDP

KonverziaZDesCislaNaDvojCislo PROC
;vstupnymi parametrami su OFFSET retazec s ascii kodmi ukonceny koncovym znakom 0Dh do registra BX a offset od ktoreho zacinaju cisla v retazci do registra DI
;skonvertovne cislo vracia v registri ax
          push cx
          push dx
          push di
          mov dx,0
          mov ax,0
          cmp byte ptr [bx+di],'-'
          jz CisloJeZaporn
          mov dl,0 
          
KonvertujRetazecNaCisl:
           
          
          mov cl,[bx+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporn
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          push dx
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          pop dx
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCisl
          

CisloJeZaporn:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov dl, 1
          jmp KonvertujRetazecNaCisl

RozhodniCiJeCisloZaporn:
          cmp dl,1
          jz DvojkovyDoplno
          mov di,0
          mov cx,0
NavratSProc:
          pop di
          pop dx 
          pop cx
          ret
DvojkovyDoplno:
         neg ax;dvojkovy doplnok zmeni znamienko cisla
         jmp NavratSProc

KonverziaZDesCislaNaDvojCislo ENDP

Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo
                                                                                                                         
Write ENDP
;-----------------------------------------------------------HLAVNY PROGRAM------------------------------------------------------
Zaciatok:
        mov ax,@DATA
        mov ds,ax
        
        ;call NacitanieSurVRovine
        ;call ZistiKvadrant
        ;call LeziNaPriamke
        
        mov dx, offset MenoSuboru
        call OpenFileForRead
 
        ;--------------------------------CITANIE ZO SUBORU---------------------------------------------------
        ;RetazPriCitaniZoSuboru naplni ascii znakmi zo suboru
        mov bx,PopisovacSuboru;ak je popisovac suboru 2, tak znaky musime zadat z klavesnice lebo subor sa pre citanie nepodarilo otvorit
        mov cx,9;pocet bytov, ktore sa budu kopirovat do retazca(vratane medzier=1B)
        mov dx, offset RetazPriCitaniZoSuboru;offset pola kde sa budu znaky kopirovat
        mov ah,3Fh
        int 21h
        
        push bx 
        mov bx,ax
        mov RetazPriCitaniZoSuboru[bx],0;na koniec nacitaneho retazca dame 0 aby sme vedeli kde retaz  Konci
        pop bx
        mov si,0 ;310
        mov bx,0
        KonvertAsciiToBin:
               cmp RetazPriCitaniZoSuboru[si],0
               je Dalej
               mov cx,0
              
              KoniecCisla:
              cmp RetazPriCitaniZoSuboru[si],' '
              je Konvert
              mov dl,RetazPriCitaniZoSuboru[si]
              push si
              mov si,cx
              mov [JednoCisloNaKonv+si],dl
              pop si
              inc si
              inc cx
              jmp KoniecCisla
              
              Konvert:
                  push si
                  mov si,cx
                  mov JednoCisloNaKonv[si],0Dh
                  pop si
                  push bx
                  mov bx,offset JednoCisloNaKonv
                  mov di,0
                  call KonverziaZDesCislaNaDvojCislo
                  pop bx
                  mov Postupnost[bx],ax
                  mov cx,0
                  add bx,2
                  inc si
                  jmp KonvertAsciiToBin

                  
        Dalej:
           
            mov ax,[Postupnost]
            mov SuradnicaX,al
            mov ax,[Postupnost+2]
            mov SuradnicaY,al
            call ZistiKvadrant
            
            call LeziNaPriamke
         
          ;-------------------------------ZATVORENIE SUBORU PRE CITANIE----------------------------------------
         mov bx,PopisovacSuboru
         mov ah,3Eh
         int 21h   
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok

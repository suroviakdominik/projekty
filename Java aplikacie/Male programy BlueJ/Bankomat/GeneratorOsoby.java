import java.util.Random;
public class GeneratorOsoby
{
  private NameGenerator aGenerujMeno;
  private NameGenerator aGenerujPriezvisko;
  private GeneratorAdresy aGenAdd;
  private Random aGenNumber;
  
  public GeneratorOsoby()
  {
      aGenerujMeno=new NameGenerator(3,10);
      aGenerujPriezvisko= new NameGenerator(5,15);
      aGenAdd=new GeneratorAdresy();
      aGenNumber=new Random();
  }
  
  public Osoba Generuj()
  {
      return new Osoba(aGenerujMeno.generuj(),
                       aGenerujPriezvisko.generuj(),
                       aGenNumber.nextInt(57)+18,
                       aGenAdd.generuj());
  }
  
}
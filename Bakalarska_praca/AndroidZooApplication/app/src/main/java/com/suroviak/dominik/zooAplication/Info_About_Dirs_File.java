package com.suroviak.dominik.zooAplication;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Dominik on 3.3.2015.
 */
public class Info_About_Dirs_File implements Serializable {
    private HashMap<String, Info_About_Item> aItemsInfo = null;
    private String aRelativePathOfThisFileInDevice = null;
    private String aServerDomainOfThisFile = null;
    private int aNumberOfNextItem = 0;
    private String aSignOfDirOfThisInfoFile = null;

    public Info_About_Dirs_File(String paDomainOfThisFile, String paRelativePathOfThisFileInDevice, String paSignOfDirOfThisInfoFile) {
        aItemsInfo = new HashMap<String, Info_About_Item>();
        aServerDomainOfThisFile = paDomainOfThisFile;
        aRelativePathOfThisFileInDevice = paRelativePathOfThisFileInDevice;
        aSignOfDirOfThisInfoFile = paSignOfDirOfThisInfoFile;
    }

    public String getNameOfDirForThisInfoFile() {
        return aSignOfDirOfThisInfoFile;
    }

    public HashMap<String, Info_About_Item> getaItemsInfo() {
        return aItemsInfo;
    }

    public String getaRelativePathOfThisFileInDevice() {
        return aRelativePathOfThisFileInDevice;
    }

    public String getServerDomainOfThisFile() {
        return aServerDomainOfThisFile;
    }

    public int getNumberOfNextItem() {
        return aNumberOfNextItem;
    }

    public void raiseNumberOfNextNewItem() {
        aNumberOfNextItem += 1;
    }

    public void setDomainOfThisFile(String aDomainOfThisFile) {
        this.aServerDomainOfThisFile = aDomainOfThisFile;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.triestrom;

import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class Vrchol {
    protected int aHlbkaVrchola;
    protected Vrchol aOtec;
    protected Vrchol aPravySyn;
    protected Vrchol aLavySyn;

    public Vrchol(int paHlbkaVrchola, Vrchol paOtec, Vrchol paLavySyn, Vrchol paPravySyn) {
        this.aHlbkaVrchola = paHlbkaVrchola;
        this.aOtec = paOtec;
        this.aPravySyn = paPravySyn;
        this.aLavySyn = paLavySyn;
    }

    public Vrchol(int paHlbkaVrchola, Vrchol paOtec) {
        this.aHlbkaVrchola = paHlbkaVrchola;
        this.aOtec = paOtec;
    }
    
    public Vrchol getaOtec() {
        return aOtec;
    }

    public void setaOtec(Vrchol aOtec) {
        this.aOtec = aOtec;
    }

    public Vrchol getaPravySyn() {
        return aPravySyn;
    }

    public void setaPravySyn(Vrchol aPravySyn) {
        this.aPravySyn = aPravySyn;
    }

    public Vrchol getaLavySyn() {
        return aLavySyn;
    }

    public void setaLavySyn(Vrchol aLavySyn) {
        this.aLavySyn = aLavySyn;
    }

    public int getaHlbkaVrchola() {
        return aHlbkaVrchola;
    }

    public void setaHlbkaVrchola(int aHlbkaVrchola) {
        this.aHlbkaVrchola = aHlbkaVrchola;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }

    @Override
    public String toString() {
        return "0";
    }
}

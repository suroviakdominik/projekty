

import java.util.Scanner;
 
public class Palindrom
{
   public static void main(String args[])
   {
      String retazec; 
      String otocenyRetazec="";
      Scanner scan= new Scanner(System.in);
 
      System.out.println("Zadajte retazec znakov,ktorych vlastnost chcete zistit: ");
      retazec = scan.nextLine();
 
      int length = retazec.length();
 
      for ( int i = length - 1 ; i >= 0 ; i-- )
      {
         otocenyRetazec = otocenyRetazec + retazec.charAt(i);
      }
 
      if (retazec.equals(otocenyRetazec))
      {
         System.out.println("Zadany retazec je palindrom, pretoze ked ho citame zlava dostaneme: ");
         System.out.println(otocenyRetazec.toUpperCase());
      }
      else
      {   
          System.out.println("Zadany retazec nie je palindrom.");
      }
 
   }
}	
import java.util.ArrayList;

public class Osoba
{
  private String aMeno;
  private String aPriezvisko;
  private int aVek;
  private Adresa aAdresa;
  private ArrayList<Klient> aKlient;
  
  
  public Osoba(String paMeno,
               String paPriezvisko,
               int paVek,
               Adresa paAdresa)
  {
      aMeno=paMeno;
      aPriezvisko=paPriezvisko;
      aVek=paVek;
      aAdresa=paAdresa;
      aKlient= new ArrayList <Klient>();
  }
  
        public String getMeno()
        {
            return aMeno;
        }
        
        public String getPriezvisko()
        {
            return aPriezvisko;
        }
  
        public int getVek()
        {
            return aVek;
        }
        
        public Adresa getAdresa()
        {
            return aAdresa;
        }
        
        public ArrayList<Klient> dajKlienta()
        {
            return aKlient;
        }
        
        public void pridajKlienta(Klient paKlient)
        {
            aKlient.add(paKlient);
        }   
        
         public String toString()
        {
           return aMeno+" "+ aPriezvisko+ " "+aVek+"\n"+aAdresa;  
        }
}        

        
        
    
       
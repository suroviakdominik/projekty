/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication;


import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.io.Serializable;
import java.util.HashMap;


/**
 * Instanciia tohto suboru predstavuje odkazovy datovy subor popisujuci
 * konkretny druh dat nachadzajucich sa v ramci aplikacie
 * @author Dominik
 */
public class Info_About_Dirs_File implements Serializable{
    private HashMap<String,Info_About_Item> aItemsInfo=null;
    private String aRelativePathOfThisFileInDevice=null;
    private String aServerDomainOfThisFile=null;
    private int aNumberOfNextItem=0;
    private String aSignOfDirOfThisInfoFile=null;
    /**
     * 
     * @param paDomainOfThisFile-domena serializobaneho objektu tejto triedy na servere
     * @param paRelativePathOfThisFileInDevice- relativna cesta v onkretnom zariadeni (PC, MOBIL)
     * @param paSignOfDirOfThisInfoFile - oznacenie priecinka, ktory obsahuje data, ktore popisuje instancia tejto triedy
     */
    public Info_About_Dirs_File(String paDomainOfThisFile,String paRelativePathOfThisFileInDevice,String paSignOfDirOfThisInfoFile) {
        aItemsInfo = new HashMap<String,Info_About_Item>();
        aServerDomainOfThisFile=paDomainOfThisFile;
        aRelativePathOfThisFileInDevice=paRelativePathOfThisFileInDevice;
        aSignOfDirOfThisInfoFile=paSignOfDirOfThisInfoFile;
    }

    /**
     * 
     * @return vrati nazov priecinka, kde su data pre tento informacny subor
     */
    public String getNameOfDirForThisInfoFile() {
        return aSignOfDirOfThisInfoFile;
    }

    /**
     * polozky popisujuce subory, ktore tento objekt popisuje
     * @return Map poloziek typu Info_About_Item obsahujuci informacie o suboroch,
     * ktore tento objekt popisuje
     */
    public HashMap<String, Info_About_Item> getaItemsInfo() {
        return aItemsInfo;
    }

    /**
     * relativna cesta ulozeneho serializovaneho objektu typu tejto triedy 
     * @return  relativna cesta ulozeneho serializovaneho objektu typu tejto triedy 
     */
    public String getaRelativePathOfThisFileInDevice() {
        return aRelativePathOfThisFileInDevice;
    }

    /**
     * 
     * @return domena serializovaneho objektu tejto triedy na servere
     */
    public String getServerDomainOfThisFile() {
        return aServerDomainOfThisFile;
    }

    /**
     * cislo pridavanej polozky (napr. pridavaneho zvierata zvierat, ak tetnto subor popisuje data
     * pre zvierata) urcene pre nazova priecinka s datami danej polozky
     * @return pocet poloziek
     */
    public int getNumberOfNextItem() {
        return aNumberOfNextItem;
    }
    
    /**
     * Po pridani polozky zvys cislo, ktore bude priradene dalsej pridavanej polozke
     */
    public void raiseNumberOfNextNewItem()
    {
        aNumberOfNextItem+=1;
    }

    /**
     * nastav domenu serializovaneho objektu predstavujuci objekt tejto triedy
     * @param aDomainOfThisFile domena na serveri serializovaneho objektu
     */
    public void setDomainOfThisFile(String aDomainOfThisFile) {
        this.aServerDomainOfThisFile = aDomainOfThisFile;
    }
    
    
    
    
    
    
    
    
}

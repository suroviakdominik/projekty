/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication;

import CommandProcessing.LoadAndSaveInfoFiles;
import CommandProcessing.CommandProcessing;
import Server.IndeterminateProgressBar;


import java.awt.BorderLayout;
import java.awt.Dimension;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javafx.scene.control.TabPane;
import javax.swing.JComponent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import setdatatoandroidaplication.Tabs.Tab;
import setdatatoandroidaplication.Tabs.TabNews;
import setdatatoandroidaplication.Tabs.TabTicketPrices;
import setdatatoandroidaplication.Tabs.TabZooInfo;
import setdatatoandroidaplication.Tabs.TabObjectsInZoo;
import setdatatoandroidaplication.Tabs.TabRemover;
import setdatatoandroidaplication.Tabs.TabOfflineMapa;
import setdatatoandroidaplication.Tabs.TabAnimalsInZoo;
import setdatatoandroidaplication.Tabs.TabEvents;

/**
 *  V rámci triedy GUI sa vytvoria jednotlivé záložky,
 * ktoré predstavujú oddelené časti grafického používateľského rozhrania.
 * @author Dominik
 */
public class GUI extends JFrame {

    private final int aPoziciaOknaX;
    private final int aPoziciaOknaY;
    private JTabbedPane tabPane1;

    /**
     * inicializuje komponenty grafického rozhrania
     */
    public GUI() {
        super("Set Data For Zoo Android Application");
        Toolkit t = Toolkit.getDefaultToolkit();

        final int OKNO_CAST = 80;//n percent z vysky a sirky okna
        Dimension d = t.getScreenSize();
        setLocation(d.width * (100 - OKNO_CAST) / 100 / 2, d.height * (100 - OKNO_CAST) / 100 / 2);

        setSize((int) (d.getWidth() * OKNO_CAST / 100), (int) (d.getHeight() * OKNO_CAST / 100));
        setResizable(true);
        setMinimumSize(new Dimension(800, 600));
        aPoziciaOknaX = d.width * (100 - OKNO_CAST) / 100 / 2;
        aPoziciaOknaY = d.height * (100 - OKNO_CAST) / 100 / 2;
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        tabPane1 = new JTabbedPane();

        Klavesnica klav = new Klavesnica();
        addKeyListener(klav);
        tabPane1.addKeyListener(klav);

        this.add(tabPane1, BorderLayout.CENTER);
        //Create and set up the content pane.
       
 
        setResizable(true);
        

        inicializujHorneMenu();
        inicializujTaby(tabPane1, klav);
        setVisible(true);
        toFront();

        //  prihlasenie  WindowEvent                                                                                    
        this.addWindowListener(new WindowAdapter() {//Windows Adapter je anonymna trieda                                 
            @Override
            public void windowClosing(WindowEvent e) {
                action_exit();
            }   //Windows Event je anonymna trieda              
        });
        
    }

    /**
     * Vytvorí záložky typu Tab a vloží ich do tabPane objektu zadaného ako 
     * parameter
     * @param tabPane1- kontajner tabov
     * @param klav- key adapter celá, udalosti v rámci celej aplikácie
     */
    private void inicializujTaby(JTabbedPane tabPane1, KeyAdapter klav) {
        tabPane1.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent ce) {
                Tab selectedTab=(Tab)GUI.this.tabPane1.getSelectedComponent();
                selectedTab.updateAllTabData();
               
            }
        });
        LoadAndSaveInfoFiles infil = new LoadAndSaveInfoFiles();
        CommandProcessing paVykonavac = new CommandProcessing(infil.loadInfoFiles());
        TabAnimalsInZoo tab2 = new TabAnimalsInZoo(tabPane1, klav, "Zvieratá", paVykonavac);
        TabRemover tab3 = new TabRemover(tabPane1, klav, "Odstraňovač", paVykonavac);
        TabZooInfo tab4=new TabZooInfo(tabPane1, klav,"Info o Zoo", paVykonavac);
        TabTicketPrices tab8=new TabTicketPrices(tabPane1, klav, "Cenník", paVykonavac);
        TabEvents tab5=new TabEvents(tabPane1,klav,"Udalosti",paVykonavac);
        TabNews tab6=new TabNews(tabPane1, klav, "Aktuality",paVykonavac);
        TabObjectsInZoo tab7=new TabObjectsInZoo(tabPane1, klav, "Objekty", paVykonavac);
        TabOfflineMapa tab9=new TabOfflineMapa(tabPane1, klav,"Offline mapa", paVykonavac) ;
        repaint();

        //TabPridajUdalost tab3=  new TabPridajUdalost(tabPane1, klav, "Pridaj udalosť");
    }

  
    /**
     * Metóda inicializuje horné menu hlavného okna GUI, ktoré bude obsahovať
     * položky Súbor a Pomoc
     */
    private void inicializujHorneMenu() {
        ///////////////////////////////////////////////////////////////////////HORNE MENU//////////////////////////////////////////////////////////////////////////
        //Ovládanie  pomocou  menu
        //musíme vytvoriť   “neviditeľnú lištu pre menu”   a   priradiť ju pre nášmu formulár
        //  lista pre horne menu
        JMenuBar mb = new JMenuBar();
        this.setJMenuBar(mb);

        //a potom  popridávať jednotlivé  hlavne položky menu
        //JMenu su vodorovne polozky horneho menu
        //JItem su zvisle polozky jednotlivych vodorovnych poloziek horneho menu
        //  polozky menu na hornej liste
        JMenu mnSubor = new JMenu("Súbor");
        mb.add(mnSubor);

        JMenuItem mniSubor_Koniec = new JMenuItem("Koniec programu");      // polozka v menu
        mniSubor_Koniec.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            @Override
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach
                action_exit();

            }     // 
        });
        mnSubor.add(mniSubor_Koniec);

        /*mnSubor.addSeparator();

        JMenuItem mniSubor_citZozSuboru = new JMenuItem("Nacitaj zo suboru");      // polozka v menu
        mniSubor_citZozSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach

            }     // 
        });
        mnSubor.add(mniSubor_citZozSuboru);
        mnSubor.addSeparator();

        JMenuItem mniSubor_zapisDoSuboru = new JMenuItem("Zapis do suboru");      // polozka v menu
        mniSubor_zapisDoSuboru.addActionListener(new ActionListener() {           // anonym. vnut. trieda pre udalost
            public void actionPerformed(ActionEvent e) {// -ak nastane udalost stlacenia na polozku Koniec,program sa ukonci,podobne to je aj pri inych polozkach

            }     // 

        });
        mnSubor.add(mniSubor_zapisDoSuboru);
        mnSubor.addSeparator();*/

        //dalsia polozka menu na hornej liste    
        JMenu mnPomoc = new JMenu("Pomoc");
        mb.add(mnPomoc);

        JMenuItem mniKlavSkratky_Pomoc = new JMenuItem("Klávesové skratky");
        mniKlavSkratky_Pomoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                action_key_abbreviation();
            }
        });
        mnPomoc.add(mniKlavSkratky_Pomoc);

        mnPomoc.addSeparator();							//  separator

        JMenuItem mniPomoc_Info = new JMenuItem("O programe");
        mniPomoc_Info.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                  JOptionPane.showMessageDialog(GUI.this,
                 "Program: Dominik Suroviak\n\n"
                 +"O programe: Editor dát pre Android ZOO aplikáciu", 
                  "Editor Android aplikácie",
                  JOptionPane.INFORMATION_MESSAGE);//nadpis okna akcie info 
                 GUI.this.repaint();
            }
        });
        mnPomoc.add(mniPomoc_Info);

    }

    /**
     * Trida predstavujúca Listener pre udalosti, ktoré môžu nastať v rámci celej aplikácie
     */
    private class Klavesnica extends KeyAdapter // vnorena trieda Klavesnica
    {

        //Nemoze mat ENTER, lebo ho pouzivam pri vytvarani kategorii a zvierat
        @Override
        public void keyPressed(KeyEvent e) // ked stlacim klavesu, tak vznikne nejaka instancia  triedy KeyEvent a vlozi sa do tato instancia bude parametrom tejto funkcie
        {
            //  System.out.println( "Stisol som " + e.getKeyCode() );   // kontrolny vypis o stisku klavesy

            switch (e.getKeyCode()) {                             // vyber cinnosti podla klavesy

                case KeyEvent.VK_ESCAPE:
                    action_exit();
                    break;

                case KeyEvent.VK_F1:
                    action_key_abbreviation();
                    break;

            }
        }  // keyPressed
    }  // Klavesnica

    //AKCIE, KTORÉ PROGRAM VYKONÁVA
    /**
     * metóda na ukončenie aplikácie a zavretie hlavného okna programu.
     */
    private void action_exit() // koniec()
    {
        if (JOptionPane.showConfirmDialog(this,
                "Chceš skutočne ukončit program", "Ukončenie",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            System.exit(0);
        }
        this.repaint();
    }

    /**
     * metóda vypíše pomoc
     */
    private void action_key_abbreviation() // pomoc()
    {
        JOptionPane.showMessageDialog(this,
                "F1    klávesové skratky\n"
                + "Esc   koniec\n"
                + "\n\n\n\n\n",
                "Editor dát Android aplikácie", JOptionPane.INFORMATION_MESSAGE); //Nadpis okna nasho problemu 

        this.repaint();
    }
}

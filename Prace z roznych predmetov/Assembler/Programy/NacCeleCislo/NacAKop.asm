Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
DesiatPriNas DW 10

.CODE;//zacina kodovy segment
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          mov dx,DesiatPriNas
          cmp cl,'-'
          jz CisloJeZaporne
          mov bl,0
KonvertujRetazecNaCislo:
           
          
          mov cl,[Retaz+di];uloz do dl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul dx
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
          

CisloJeZaporne:
          inc di
          mov bl 1;
          jmp KonvertujRetazecNaCislo

RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz UrobZapornym:
          jmp KOniec;

UrobZapornym:
         neg ax;
          
          
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok
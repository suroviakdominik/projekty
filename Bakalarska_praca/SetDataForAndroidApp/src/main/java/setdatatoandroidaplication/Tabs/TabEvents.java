/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import MyGUIJComponent.JTextAreaWithHint;
import EditorsForInfo_Files.InfoSuboryENUM;
import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import static java.awt.image.ImageObserver.WIDTH;

import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerDateModel;
import javax.swing.border.Border;

import setdatatoandroidaplication.DataTransferer;

/**
 *  V rámci tejto triedy je možné vytvárať udalosti, 
 *  ktoré sa vyskytnú v zoologickej záhrade v najbližšej dobe. 
 * @author Dominik
 */
public class TabEvents extends Tab {

   
    private JSpinner aDatumUdalosti;
    private JTextField aNazovUdalosti;
    private JTextAreaWithHint aPopisUdalosti;
    private JComboBox<Info_About_Item> aEventsList;
    private final DataTransferer aDataTransferer;
    private final JPanel aPanelKomponentov;
    private final GridBagConstraints aGridConstrains;
    private final int PREFFERED_HIGHT = 30, PREFFERED_WIDTH = 150, MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 50, MARGIN_RIGHT = 50;
   
    
     /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabEvents(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paCommandProcessingForTab) {
        super(paTabPane, paKeyListener, paNazovTabu,paCommandProcessingForTab);
        aCommandProcessingForTab = paCommandProcessingForTab;
        aPanelKomponentov = getMiddleMainTabPanel();
        aPanelKomponentov.setLayout(new GridBagLayout());
        aGridConstrains = new GridBagConstraints();
        aDataTransferer = DataTransferer.getInstance();
        initializeMainTabPanelComponents();
        aPopisUdalosti.addKeyListener(paKeyListener);
        for (Component prech : aPanelKomponentov.getComponents()) {
            prech.setFont(FONT);
            prech.addKeyListener(paKeyListener);
        }
    }

    /**
     * vytvori vsetky vizualne komponenty tohto tabu
     */
    private void initializeMainTabPanelComponents() {
        aGridConstrains.fill = GridBagConstraints.BOTH;
        aGridConstrains.insets = new Insets(MARGIN_TOP, MARGIN_LEFT, MARGIN_BOTTOM, MARGIN_RIGHT);
        aGridConstrains.weighty = 0.05;

        addComponentForEventsShower();
        aGridConstrains.gridy = 1;
        addComponentsForEventDateSet();
        aGridConstrains.gridy = 2;
        aGridConstrains.weighty = 0.05;
        addComponentsForAddAndShowNameOfEvents();

        aGridConstrains.gridy = 3;
        aGridConstrains.gridwidth = 2;
        aGridConstrains.weightx = 1;
        addComponentsForEventDescription();
    }
    
    /**
     * vytvori komponenty v ramci ktorych budu zobrazene vytvorene komponenty
     */
    private void addComponentForEventsShower() {
        aEventsList = new JComboBox<>();
        aPanelKomponentov.add(aEventsList, aGridConstrains);
        aEventsList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getDescriptionOfSelectedEvent();
            }
        });

        JButton btNew = new JButton("Vlož novú udalosť");
        aPanelKomponentov.add(btNew, aGridConstrains);
        btNew.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                aNazovUdalosti.setText("");
            aPopisUdalosti.showCurrentDefaultText();
            aDatumUdalosti.setValue(new Date());
            aPopisUdalosti.setEditable(true);
            aNazovUdalosti.setEditable(true);
            aDatumUdalosti.setEnabled(true);
            aPopisUdalosti.setJeUznejakyTextnacitany(false);
            }
        });
    }

    /**
     * vytvori vizualne komponenty pre zadanie datumu udalosti
     */
    private void addComponentsForEventDateSet() {
        JLabel l = new JLabel("Zadajte dátum udalosti: ");
        aPanelKomponentov.add(l, aGridConstrains);

        aDatumUdalosti = new JSpinner(new SpinnerDateModel());
        JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(aDatumUdalosti, "dd-MM-yyyy HH:mm:ss");
        aDatumUdalosti.setEditor(timeEditor);
        aDatumUdalosti.setValue(new Date()); // will only show the current time
        //Date a=(Date)aDatumUdalosti.getValue();
        aPanelKomponentov.add(aDatumUdalosti, aGridConstrains);

    }

    /**
     * vytvori vizualne komponenty pre zadanie nazvu vytvaranej udalotsi
     * a zobrazenie nazvu vytvorenej udalosti
     * 
     */
    private void addComponentsForAddAndShowNameOfEvents() {
        JLabel l = new JLabel("Zadajte názov udalosti:");
        aPanelKomponentov.add(l, aGridConstrains);

        aNazovUdalosti = new JTextField();
        aPanelKomponentov.add(aNazovUdalosti, aGridConstrains);
    }

    /**
     * vytvori komponenty pre editaciu a zobrazenie popisu udalosti
     */
    private void addComponentsForEventDescription() {
        aGridConstrains.weighty = 0.8;
        aPopisUdalosti = new JTextAreaWithHint();
        aPopisUdalosti.setDefaultText("Popis udalosti ....");
        aPopisUdalosti.setFont(FONT);
        JScrollPane lpScrollPaneOfTextArea = new JScrollPane(aPopisUdalosti);
        lpScrollPaneOfTextArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        lpScrollPaneOfTextArea.setPreferredSize(new Dimension(PREFFERED_WIDTH, PREFFERED_HIGHT));
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        aPopisUdalosti.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        aPanelKomponentov.add(lpScrollPaneOfTextArea, aGridConstrains);

        aGridConstrains.gridy = 4;
        aGridConstrains.weighty = 0.05;
        JButton btPridaj = new JButton("Pridaj udalosť: ");
        aPanelKomponentov.add(btPridaj, aGridConstrains);
        btPridaj.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionAddNewEvent();
            }
        });
    }

    /**
     * vytvori novu udalost
     */
    private void actionAddNewEvent() {
        if (aNazovUdalosti.getText().equals("") || aPopisUdalosti.getText().equals("") || aPopisUdalosti.getText().equals(aPopisUdalosti.getDefaultText())) {
            JOptionPane.showMessageDialog(this,
                    "Niektorý z povinných údajov( dátum, text) nebol zadaný.",
                    "ERROR: Nezadaný niektorý údaj.",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        Date lpDatEv = (Date) aDatumUdalosti.getValue();

        aDataTransferer.removeAllData();
        aDataTransferer.addString("NEW_EVENT_NAME", aNazovUdalosti.getText());
        aDataTransferer.addString("NEW_EVENT_TIME", lpDatEv.toGMTString());
        aDataTransferer.addString("NEW_EVENT_TEXT", aPopisUdalosti.getText());
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.EVENTS, PrikazyENUM.PRIDAJ)) {

            JOptionPane.showMessageDialog(this, "New event has been created.");
            Info_About_Item selected = aDataTransferer.getItemInfo("CREATED_EVENT");
            updateEvents();
            aEventsList.setSelectedItem(selected);
        } else {
            JOptionPane.showMessageDialog(this,
                    "AppERROR: Event has not been created",
                    "ERROR: Application Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();

    }

    /**
     * naplni komponent zobrazujuci vytvorene udalosti aktualne existujucimi udalostami
     */
    private void updateEvents() {
        aEventsList.removeAllItems();
        aDataTransferer.removeAllData();
        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.EVENTS, PrikazyENUM.DAJDATA);
        for(Info_About_Item prech:aDataTransferer.getAllInfoAboutItem()){
            aEventsList.addItem(prech);
        };
        aDataTransferer.removeAllData();
    }

    /**
     * zobrazi popis vybranej udalosti
     */
    private void getDescriptionOfSelectedEvent() {
        if (aEventsList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("GET_DATA_OF_SELECTED_EVENT", (Info_About_Item) aEventsList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.EVENTS, PrikazyENUM.DAJDATA)) {
                aNazovUdalosti.setText(aDataTransferer.getString("RET_EVENT_NAME"));
                String lpPopis = aDataTransferer.getString("RET_EVENT_TEXT");
                String pole[] = lpPopis.split("\n");
                aDatumUdalosti.setValue(new Date(pole[0]));
                String popis = "";
                for (int i = 1; i < pole.length; i++) {
                    popis += pole[i] + "\n";
                }
                aPopisUdalosti.setText(popis);
                aPopisUdalosti.setEditable(false);
                aNazovUdalosti.setEditable(false);
                aDatumUdalosti.setEnabled(false);
                aPopisUdalosti.setJeUznejakyTextnacitany(true);
            }
        }
        else
        {
             aPopisUdalosti.showCurrentDefaultText();
             aDatumUdalosti.setValue(new Date());
             aPopisUdalosti.setEditable(true);
             aNazovUdalosti.setEditable(true);
             aDatumUdalosti.setEnabled(true);
             aPopisUdalosti.setJeUznejakyTextnacitany(false);
             aNazovUdalosti.setText("");
        }
    }

    /**
     * aktualizuje vsetky data totho tabu
    */
    @Override
    public void updateAllTabData() {
        updateEvents();
    }
}

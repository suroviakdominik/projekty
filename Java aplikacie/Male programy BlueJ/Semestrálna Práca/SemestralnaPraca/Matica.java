
import java.util.Scanner;
import java.util.Random;
/**
 *Trieda načíta alebo vygeneruje maticu a vloźí maticu do objektu typu PoliaMnozinAHodnotRelacie, dokáže ju vypísať.
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class Matica
{
  private PoliaMnozinAHodnotRelacie aPole;   
  private int aPocetRiadkov;
  private int aPocetStlpcov;
  private Scanner scan;
  private Random generuj;
 
  /**
     * Konštruktor vytvóri nové dvojrozmerné pole do ktorého sa matica uloží. Tiež vytvóri Scanner, ktorý nám umožní čítanie s terminálu
     */
  public Matica(int paPocetRiadkov,int paPocetStlpcov)
  {
      aPocetRiadkov=paPocetRiadkov;
      aPocetStlpcov=paPocetStlpcov;
      aPole= new PoliaMnozinAHodnotRelacie(aPocetRiadkov,aPocetStlpcov);
      scan=new Scanner(System.in);
  }
   
      /**
       * metóda načíta vami zadanú maticu s terminálu
       */
      public void nacitajMaticu()
      {
         System.out.println("Zadajte prvky matice:");
         for (int i=0;i<aPocetRiadkov;i++)
         {
           for (int j=0;j<aPocetStlpcov;j++)
           {
              aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,scan.nextInt());
           }
         }
      }
    
       /**
        * metóda vygeneruje maticu v rozmedzí čísel zadaného intervalu
        * @ param paOdKolkoGenerovat spodná hranica intervalu
        * @ param int paPoKolkoGenerovat horná hranica intervalu
        */
      public void generujNahodnuMaticu(int paOdKolkoGenerovat,int paPoKolkoGenerovat)
      {
         generuj=new Random(); 
         for (int i=0;i<aPocetRiadkov;i++)
         {
             for (int j=0;j<aPocetStlpcov;j++)
             {
                aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,generuj.nextInt(paPoKolkoGenerovat-paOdKolkoGenerovat+1)+paOdKolkoGenerovat);
             }
         }
      }
    
      /**
       * metóda pomocou metódy vypisMatice() nachádzajúcej sa v triede PoliaMnozinHodnotARelacie vypíše maticu do terminálu
       */
      public void vypisMaticu()
      {
         aPole.vypisMatice();
      }
    
      /**
       * metóda vráti počet riadkov matice
       */
      public int dajPocetRiadkov()
      {
          return aPocetRiadkov;
      }
    
      /**
       * metóda vráti počet stĺpcov matice
       */
      public int dajPocetStlpcov()
      {
         return aPocetStlpcov;
      } 
    
      /**
       * metóda pomocou správy metóde triedy PoliaMnozinHodnotARelacie zistí hodnotu prvka nachádzajúceho sa na indexe i,j
       * @param i riadok
       * @param j stĺpec
       */
      public int dajPrvokPola(int i,int j)
      {
          return aPole.zistiHodnotuDoNRozmernehoPolaRelacie(i,j);
      }
    
      /**
       * metóda pomocou správy metóde triedy PoliaMnozinHodnotARelacie vloží hodnotu do poľa na  index i,j
       * @param i riadok 
       * @param j stĺpec
       * @param hodnota hodnota ktorá sa vloží
       */
      public void vlozPrvokDoMatice(int i,int j,int hodnota)
      {
          aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,hodnota);
      }
}    

               
     

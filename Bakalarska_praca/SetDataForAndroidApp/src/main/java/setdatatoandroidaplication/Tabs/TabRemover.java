/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import CommandProcessing.PrikazyENUM;
import CommandProcessing.CommandProcessing;
import EditorsForInfo_Files.InfoSuboryENUM;
import com.suroviak.dominik.zooAplication.Info_About_Item;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import setdatatoandroidaplication.DataTransferer;

/**
 *  Trieda.kde sa dajú vymazať existujúce zvieratá, objekty, aktuality a udalosti. 
 * @author Dominik
 */
public class TabRemover extends Tab {

    private JPanel aTabPanel;
    private JComboBox<Info_About_Item> aCategoryAnimalList;
    private JComboBox<Info_About_Item> aAnimalList;
    private DataTransferer aDataTransferer = DataTransferer.getInstance();
    private static final Font FONT = new Font("myfont", WIDTH, 20);
    private JComboBox<Info_About_Item> aEventsList;
    private JComboBox<Info_About_Item> aActualityList;
    private JComboBox<Info_About_Item> aObjectList;
    private JComboBox<Info_About_Item> aCategoryObjectList;

     /**
     * Inicializuje vsetky vizualne komponenty Tabu
     ** @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalny keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public TabRemover(JTabbedPane paTabPane, KeyListener paKeyListener, String paNazovTabu, CommandProcessing paVykonavacPrikazov) {
        super(paTabPane, paKeyListener, paNazovTabu, paVykonavacPrikazov);
        aCommandProcessingForTab = paVykonavacPrikazov;
        aTabPanel = super.getMiddleMainTabPanel();
        aTabPanel.setLayout(new GridLayout(6, 2, 20, 20));
        aCategoryAnimalList = new JComboBox<>();
        aAnimalList = new JComboBox<>();
        aEventsList = new JComboBox<>();
        aActualityList = new JComboBox<Info_About_Item>();
        aCategoryObjectList = new JComboBox<>();
        aObjectList = new JComboBox<>();
        initializeMainTabPanelComponents(paKeyListener);
        for (Component prech : aTabPanel.getComponents()) {
            prech.addKeyListener(paKeyListener);
            prech.setFont(FONT);
        }
    }

    /**
     * inicializuje vsetky potrebne vizualne komponenty
     * @param paKeyListener 
     */
    private void initializeMainTabPanelComponents(KeyListener paKeyListener) {
        addAnimalCategoryRemover();
        addComponentsForRemoveAnimal();
        addComponentsForEventsRemove();
        addComponentsForNewsRemove();
        addComponentsForObjectCategoriesRemove();
        addComponentsForObjectRemove();
    }

    /**
     * prida komponety na odstranenie kategorie zvierat
     */
    private void addAnimalCategoryRemover() {
        aTabPanel.add(aCategoryAnimalList);

        updateAnimalCategories();
        aCategoryAnimalList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                updateAnimals();
            }
        });
        JButton bt = new JButton("Odstráň zvolenú kategóriu zvierat");

        aTabPanel.add(bt);
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedAnimalCategory();
            }
        });

    }

     /**
     * prida komponety na odstranenie zvierat
     */
    private void addComponentsForRemoveAnimal() {
        aTabPanel.add(aAnimalList);

        updateAnimals();
        JButton bt = new JButton("Odstráň zvolené zviera");

        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedAnimal();
            }

        });
        aTabPanel.add(bt);

    }

    /**
     * prida komponenty na odstranenie udalosti
     */
    private void addComponentsForEventsRemove() {
        aTabPanel.add(aEventsList);

        updateEvents();
        JButton bt = new JButton("Odstráň zvolenú udalosť");

        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedEvent();
            }

        });
        aTabPanel.add(bt);
    }

    /**
     * prida komponenty na odstranenie aktualit
     */
    private void addComponentsForNewsRemove() {
        aTabPanel.add(aActualityList);
        updateActualities();
        JButton bt = new JButton("Odstráň zvolenú aktualitu");

        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedActuality();
            }

        });
        aTabPanel.add(bt);
    }

    /**
     * prida komponenty na odstranenie kategorie objektov
     */
    private void addComponentsForObjectCategoriesRemove() {
        aTabPanel.add(aCategoryObjectList);

        updateObjectCategories();
        aCategoryObjectList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                updateObjects();
            }
        });
        JButton bt = new JButton("Odstráň zvolenú kategóriu objektov");

        aTabPanel.add(bt);
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedObjectCategory();
            }
        });

    }


    /**
     * prida komponenty na odstranenie objektov
     */
    private void addComponentsForObjectRemove() {
        aTabPanel.add(aObjectList);
        updateObjects();
        JButton bt = new JButton("Odstráň zvolený objekt");

        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRemoveSelectedObject();
            }

        });
        aTabPanel.add(bt);
    }

    /**
     * zobrazi v urcenom komponente vsetky vytvorene kategorie zvierat
     */
    private void updateAnimalCategories() {
        aCategoryAnimalList.removeAllItems();
        aDataTransferer.removeAllData();
        aDataTransferer.addItemInfo("GET_ANIMAL_CATEGORIES", new Info_About_Item(null, null, null));
        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);
        for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
            aCategoryAnimalList.addItem(prech);

        }
        aDataTransferer.removeAllData();
        //this.getParent().repaint();
    }

     /**
     * zobrazi v urcenom komponente vsetky vytvorene zvierata
     */
    private void updateAnimals() {

        if (aCategoryAnimalList.getSelectedItem() != null && aAnimalList != null) {
            String keyOfSelectedCategory = getKeyOfSelectedAnimalCategory();
            aDataTransferer.addString("GET_ANIMAL_OF_CHOOSEN_CATEGORY", keyOfSelectedCategory);
            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.DAJDATA);

            aAnimalList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aAnimalList.addItem(prech);
            }

        }

        aDataTransferer.removeAllData();

    }

     /**
     * zobrazi v urcenom komponente vsetky vytvorene udalosti
     */
    private void updateEvents() {
        aEventsList.removeAllItems();
        aDataTransferer.removeAllData();
        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.EVENTS, PrikazyENUM.DAJDATA);
        for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
            aEventsList.addItem(prech);

        }
        aDataTransferer.removeAllData();
    }

     /**
     * zobrazi v urcenom komponente vsetky vytvorene aktuality
     */
    private void updateActualities() {
        if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.DAJDATA)) {
            aActualityList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aActualityList.addItem(prech);
            }

            aDataTransferer.removeAllData();
        }
    }

     /**
     * zobrazi v urcenom komponente vsetky vytvorene kategorie objektov
     */
    private void updateObjectCategories() {
        aCategoryObjectList.removeAllItems();
        aDataTransferer.removeAllData();

        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);
        for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
            aCategoryObjectList.addItem(prech);

        }
        aDataTransferer.removeAllData();
        //this.getParent().repaint();
    }

     /**
     * zobrazi v urcenom komponente vsetky vytvorene objekty
     */
    private void updateObjects() {
        if (aCategoryObjectList.getSelectedItem() != null && aObjectList != null) {
            String keyOfSelectedCategory = getKeyOfSelectedObjectCategory();
            DataTransferer.getInstance().addString("GET_OBJECTS_OF_CHOOSEN_CATEGORY", keyOfSelectedCategory);
            aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.DAJDATA);

            aObjectList.removeAllItems();
            for (Info_About_Item prech : aDataTransferer.getAllInfoAboutItem()) {
                aObjectList.addItem(prech);
            }

        }

        DataTransferer.getInstance().removeAllData();
    }

    private String getKeyOfSelectedAnimalCategory() {
        aDataTransferer.addItemInfo("GET_ANIMAL_CATEGORIES", new Info_About_Item(null, null, null));
        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);

        for (String prech : aDataTransferer.getAllInfoAboutItemKeys()) {
            if (aCategoryAnimalList.getSelectedItem().equals(aDataTransferer.getItemInfo(prech))) {
                aDataTransferer.removeAllData();
                return prech;
            }
        }
        aDataTransferer.removeAllData();

        return null;
    }

    private String getKeyOfSelectedObjectCategory() {

        aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.DAJDATA);

        for (String prech : aDataTransferer.getAllInfoAboutItemKeys()) {
            if (aCategoryObjectList.getSelectedItem().equals(aDataTransferer.getItemInfo(prech))) {
                aDataTransferer.removeAllData();
                return prech;
            }
        }

        aDataTransferer.removeAllData();
        return null;
    }

    /**
     * odstrani kategoriu zvierat
     */
    private void actionRemoveSelectedAnimalCategory() {
        if (aCategoryAnimalList.getSelectedItem() != null && aAnimalList.getSelectedItem() == null) {
            aDataTransferer.removeAllData();
            Info_About_Item selectedCategory = (Info_About_Item) aCategoryAnimalList.getSelectedItem();
            aDataTransferer.addItemInfo("SELECTED_CATEGORY_TO_DELETE", (Info_About_Item) aCategoryAnimalList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Category: " + selectedCategory + " was deleted.");
                updateAnimalCategories();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Cattegory can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.CATEGORIES, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete category",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Cattegory can not be deleted, becouse he consists some animal \n or any category is choosen. First remove animals.",
                    "Delete category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * odstrani vybrane zviera
     */
    private void actionRemoveSelectedAnimal() {
        if (aAnimalList.getSelectedItem() != null) {
            Info_About_Item selectedAnimal = (Info_About_Item) aAnimalList.getSelectedItem();
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("SELECTED_ANIMAL_TO_DELETE", (Info_About_Item) selectedAnimal);
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.ANIMALS, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Animal: " + selectedAnimal + " was deleted.");
                updateAnimals();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Animal can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.ANIMALS, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete animal",
                        JOptionPane.ERROR_MESSAGE);

            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Animal can not be deleted, becouse any animal is choosen. First choose some animal.",
                    "Delete animal error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * odstrani zvolenu udalost
     */
    private void actionRemoveSelectedEvent() {
        if (aEventsList != null && aEventsList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("SELECTED_EVENT_TO_DELETE", (Info_About_Item)aEventsList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.EVENTS, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Event has been deleted.");
                updateEvents();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Event can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.EVENTS, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete event",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Event can not be deleted, becouse any event is choosen. First choose some event.",
                    "Delete event error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * odstrani vybranu atualitu
     */
    private void actionRemoveSelectedActuality() {
        if (aActualityList != null && aActualityList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("SELECTED_ACTUALITY_TO_DELETE", (Info_About_Item) aActualityList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.NEWS, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Actuality has been deleted.");
                updateActualities();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Actuality can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.NEWS, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete actuality",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Actuality can not be deleted, becouse any actuality is choosen. First choose some actuality.",
                    "Delete actuality error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * odstani vybranu kategoriu objektov
     */
    private void actionRemoveSelectedObjectCategory() {
        if (aCategoryObjectList.getSelectedItem() != null && aObjectList.getSelectedItem() == null) {
            aDataTransferer.removeAllData();
            Info_About_Item selectedCategory = (Info_About_Item) aCategoryObjectList.getSelectedItem();
            aDataTransferer.addItemInfo("SELECTED_CATEGORY_TO_DELETE", (Info_About_Item) aCategoryObjectList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.CATEGORIES, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Category: " + selectedCategory + " was deleted.");
                updateObjectCategories();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Cattegory can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.CATEGORIES, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete category",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Cattegory can not be deleted, becouse he consists some objects \n or any category is choosen. First remove objects.",
                    "Delete category error",
                    JOptionPane.ERROR_MESSAGE);
        }
        aDataTransferer.removeAllData();
    }

    /**
     * odstrani vybrany objekt
     */
    private void actionRemoveSelectedObject() {
        if (aObjectList != null && aObjectList.getSelectedItem() != null) {
            aDataTransferer.removeAllData();
            aDataTransferer.addItemInfo("SELECTED_OBJECT_TO_DELETE", (Info_About_Item) aObjectList.getSelectedItem());
            if (aCommandProcessingForTab.vykonajPrikaz(InfoSuboryENUM.OBJECTS, PrikazyENUM.ODOBER)) {
                JOptionPane.showMessageDialog(this, "Object has been deleted.");
                updateObjects();
            } else {
                JOptionPane.showMessageDialog(this,
                        "Object can not be deleted. \n"
                        + "aVykonavacPrikazov.vykonajPrikaz(ExistujuceInfoSubory.OBJECTS, PrikazyENUM.ODOBER return false ",
                        "APP_ERROR: Delete actuality",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    "Object can not be deleted, becouse no object is choosen. First choose some object.",
                    "Delete object error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * aktualizuje vsetky data tohto tabu
     */
    @Override
    public void updateAllTabData() {
        updateAnimalCategories();
        updateAnimals();
        updateEvents();
        updateActualities();
        updateObjectCategories();
        updateObjects();
    }

}

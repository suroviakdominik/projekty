/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy;

import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import linearne.štrukurované.datové.typy.Objekty.Automobil;
import linearne.štrukurované.datové.typy.Objekty.IObjekt;
import linearne.štrukurované.datové.typy.Objekty.Kniha;
import linearne.štrukurované.datové.typy.Objekty.Osoba;

/**
 * Trieda ILinearDataTypy je predokm tried Front, Zasobnik,Zoznam
 *
 * @author Dominik
 */
public abstract class ILinearDataTypy {

    public abstract boolean jePrazdny();

    public abstract int mohutnost();

    public abstract IObjekt citajZaciatok() throws FileNotFoundException, IOException, ClassNotFoundException;

    public abstract void nacitajPrvkyZoSuboru() throws FileNotFoundException, IOException;

    public abstract void zapisPrvkyDoSuboru() throws FileNotFoundException;

    public abstract void uplnaPrehliadka(int poziciaX, int poziciaY, int height, int width);

    public abstract void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY);

    public abstract void zrus();

    protected IObjekt vytvorObjektZoSuboru(File PodAdresar, String paNazov) throws IOException {
        IObjekt navrat;

        if (paNazov.charAt(0) == 'A') {

            navrat = new Automobil();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);

        } else if (paNazov.charAt(0) == 'K') {

            navrat = new Kniha();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);

        } else {
            navrat = new Osoba();
            navrat.nacitajZoSuboru(PodAdresar, paNazov);
        }

        return navrat;
    }

    protected String najdiSuborSporadovymCislom(String[] paNazvySuborov, int paPoradoveCislo)//lebo nevieme ako su ulozene subory v priecinku usporiadane
    {

        String podRetazecCisloSuboru;
        for (String paNazvySuborov1 : paNazvySuborov) {
            podRetazecCisloSuboru = paNazvySuborov1.substring(1, 5);
            if (Integer.parseInt(podRetazecCisloSuboru) == paPoradoveCislo) {
                return paNazvySuborov1;
            }
        }
        return null;
    }
}


import java.util.Scanner;
public class PoliaMnozinAHodnotRelacie
{
 private Scanner scan;
 private int [] aPoleMnozinyA;
 private int [] aPoleMnozinyB;
 private int [][] aNRozmernePoleHodnotRelacie;
 private int aPocetRiadkov;
 private int aPocetStlpcov;
  
  public PoliaMnozinAHodnotRelacie(int paPocetRiadkov,
                                   int paPocetStlpcov)
  {
      aPocetRiadkov=paPocetRiadkov;
      aPocetStlpcov=paPocetStlpcov;
      aNRozmernePoleHodnotRelacie=new int[aPocetRiadkov][aPocetStlpcov];
      scan=new Scanner(System.in);
  }

       public void nacitajMnozinuA()
       {
           aPoleMnozinyA=new int[aPocetRiadkov];
           System.out.println("Zadajte vedla seba prvky mnoziny A  a potvrdte ich stlacenim klavesy ENTER: ");
           for (int i=0;i<aPocetRiadkov;i++)
           {
              aPoleMnozinyA[i]=scan.nextInt(); 
           }
       }
    
       public void nacitajMnozinuB()
       {
         aPoleMnozinyB=new int[aPocetStlpcov];
         System.out.println("Zadajte vedla seba prvky mnoziny B  a potvrdte ich stlacenim klavesy ENTER: ");
         for (int i=0;i<aPocetStlpcov;i++)
         {
            aPoleMnozinyB[i]=scan.nextInt(); 
         }
       }
    
       public void vypisPrvkuMnozinyA(int i)
       {
         System.out.print(aPoleMnozinyA[i]+"\t");
       }
    
      public void vypisMnozinyB()
      { 
        for (int i=0;i<aPocetStlpcov;i++)
        {
          System.out.print(aPoleMnozinyB[i]+"\t");
        }
      }
      
      public int dajPrvokPolaA(int i)
      {
         return aPoleMnozinyA[i];
      } 
   
      public int dajPrvokPolaB(int i)
      {
         return aPoleMnozinyB[i];
      } 
    
      public void vypisPoleHodnotRelacie()
      {
          System.out.print("A/B\t");
          vypisMnozinyB();
          System.out.println();
          
          for (int i=0;i<aPocetRiadkov;i++)
          {
              vypisPrvkuMnozinyA(i);
              for (int j=0;j<aPocetStlpcov;j++)
              {
                  System.out.print(aNRozmernePoleHodnotRelacie[i][j]+"\t");
              }
              System.out.println();
          }
      } 
      
      public void vypisMatice()
      {
         for (int i=0;i<aPocetRiadkov;i++)
         {
             for (int j=0;j<aPocetStlpcov;j++)
             {
                 System.out.print(aNRozmernePoleHodnotRelacie[i][j]+"\t");
             }
           System.out.println();  
        }
     }
              
    public int zistiHodnotuDoNRozmernehoPolaRelacie(int i,int j)
    {
        return aNRozmernePoleHodnotRelacie[i][j];
    }        
    
    public void vlozHodnotuDoNRozmernehoPolaRelacie(int i,int j,int hodnota)
    {
        aNRozmernePoleHodnotRelacie[i][j]=hodnota;
    }
    
    public int[][] dajNRozmernePole()
    {
          return aNRozmernePoleHodnotRelacie;
    }    
}
    
  
    


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake.DrawableObjects.Menu
{
    /// <summary>
    /// enum polozky menu v multiplayer menu
    /// </summary>
    public enum MultiplayerOptions
    {
        CreateServer,
        JoinToServer,
        StartMenu
    }

    /// <summary>
    /// ak pouzivatel zvoli najeku polozku z menu, vyvola sa udalost, ktora dostane ako parameter objekt tejto triedy
    /// </summary>
    public class MultMenuOptEventArgs : EventArgs
    {
        private MultiplayerOptions aSelOption;
        private string aVpnIp;
        public MultMenuOptEventArgs(MultiplayerOptions paSelectedOption, string paVpnIp)
        {
            aSelOption = paSelectedOption;
            aVpnIp = paVpnIp;
        }

        public MultiplayerOptions SelOption
        {
            get { return aSelOption; }
        }

        public string VPNIp
        {
            get { return aVpnIp; }
        }
    }

    /// <summary>
    /// Trieda ktora je zodpovedna za vykreslenie multiplayer menu a volbu moznosti na nej sa nachadzajucich 
    /// </summary>
    class MultiplayerMenu
    {
        private const int VerticalGap = 10;///vertikalna medzera menu poloziek
        private GraphicsDevice aGraphicsDevice;///obrazovka, ktora sluzi ako platno
        private Texture2D aOptionFrame;///ramik pre polozku menu
        private SpriteFont aFont;///pismo pre polozku menu
        private int aCountOfOptions;///pocet enum moznosti menu
        private List<Vector2> aPositionsOfOptions;///zoznam suradnic, kde vykreslovat polozky menu
        private List<string> aOptionNames;///mena jednotlivych poloziek menu
        public MultiplayerOptions SelectedOption { get; set; }///aktualne zvolena moznost v mult menu - este nepotvrdena
        private float aTimeFromLastUpdate;///cas od posledneho updatu zvolenej polozky menu

        public event EventHandler<MultMenuOptEventArgs> OptionSelectedEvent;/// udalost ktora sa vyvola ked pouzivatel vyberie jednu z menu
        private const string DefaultVPNIpAdress = "25.102.172.145";///defaultna vpn adresa
        private StringBuilder aIpAdress;///aktualne zadana ip
        private Texture2D aMultiplayerMenuBackg;///pozadie obrazovky mult menu

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public MultiplayerMenu(GraphicsDevice graphicsDevice)
        {
            aGraphicsDevice = graphicsDevice;
            aPositionsOfOptions = new List<Vector2>();
            aOptionNames = new List<string>();
            aOptionNames.Add("Create server");
            aOptionNames.Add("Join to server");
            aOptionNames.Add("Start menu");
            aIpAdress = new StringBuilder(DefaultVPNIpAdress);
            SelectedOption = MultiplayerOptions.CreateServer;
            aCountOfOptions = Enum.GetValues(typeof(MultiplayerOptions)).Length;
        }

        public void resetVPNIPToDefault()
        {
            aIpAdress = new StringBuilder(DefaultVPNIpAdress);
        }

        /// <summary>
        /// sirka ramika menu polozky
        /// </summary>
        public float OptFrameWidth
        {
            get { return aOptionFrame.Width; }
        }

        /// <summary>
        /// vyska ramika menu polozky
        /// </summary>
        public float OptFrameHeight
        {
            get { return aOptionFrame.Height; }
        }

        /// <summary>
        /// nacita polozky, ktore sa budu vykreslovat, prehravat ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aMultiplayerMenuBackg = content.Load<Texture2D>("mult-menu");
            aOptionFrame = content.Load<Texture2D>("start");
            aFont = content.Load<SpriteFont>("SMFont");
            int pom = aCountOfOptions;
            Vector2 firstOptPos = new Vector2((float)aGraphicsDevice.Viewport.Width / 2 - OptFrameWidth / 2,
                                              aGraphicsDevice.Viewport.Height / 2 - pom * VerticalGap - pom * OptFrameHeight);

            for (int i = 0; i < aCountOfOptions; i++)
            {
                aPositionsOfOptions.Add(new Vector2(firstOptPos.X, firstOptPos.Y + i * VerticalGap + i * OptFrameHeight));
            }
        }

        /// <summary>
        /// Update vybranej moznosti- ak pouzivatel stlacil sipku nadol alebo nahor
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
            {
                if (SelectedOption == MultiplayerOptions.CreateServer ||
                    SelectedOption == MultiplayerOptions.JoinToServer)
                {
                    System.Net.IPAddress ipAddress;
                    bool isValidIp = System.Net.IPAddress.TryParse(aIpAdress.ToString(), out ipAddress);
                    if (isValidIp)
                    {
                        OnOptionSelectedEvent(new MultMenuOptEventArgs(SelectedOption, aIpAdress.ToString()));
                    }
                }
                else
                {
                    OnOptionSelectedEvent(new MultMenuOptEventArgs(SelectedOption, aIpAdress.ToString()));
                }
            }
            aTimeFromLastUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (aTimeFromLastUpdate > 0.1)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                {
                    if (SelectedOption != 0)
                    {
                        SelectedOption--;
                    }
                    else
                    {
                        SelectedOption = (MultiplayerOptions)Enum.GetValues(typeof(MultiplayerOptions)).GetValue(aCountOfOptions - 1);
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Down) ||
                         GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                {
                    if (SelectedOption != (MultiplayerOptions)Enum.GetValues(typeof(MultiplayerOptions)).GetValue(aCountOfOptions - 1))
                    {
                        SelectedOption++;
                    }
                    else
                    {
                        SelectedOption = 0;
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Back) && aIpAdress.Length > 0)
                {
                    aIpAdress.Remove(aIpAdress.Length - 1, 1);
                }
                else if (!Keyboard.GetState().IsKeyDown(Keys.Back) && Keyboard.GetState().GetPressedKeys().Length > 0)
                {
                    string input = convert(Keyboard.GetState().GetPressedKeys());
                    aIpAdress.Append(input);
                }
                aTimeFromLastUpdate = 0;
            }
        }

        /// <summary>
        /// Vykreslenie poloziek menu
        /// </summary>
        /// <param name="drawer"></param>
        public void Draw(SpriteBatch drawer)
        {
            drawer.DrawString(aFont, "VPN IP Adress: " + aIpAdress, Vector2.One, Color.Green, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            for (int i = 0; i < aPositionsOfOptions.Count; i++)
            {
                Color optCol = Color.White;
                if ((MultiplayerOptions)Enum.GetValues(typeof(MultiplayerOptions)).GetValue(i) == SelectedOption)
                {
                    optCol = Color.Red;
                }
                Vector2 stringSize = aFont.MeasureString(aOptionNames[i]);
                drawer.Draw(aOptionFrame, aPositionsOfOptions[i], null, optCol, 0f, Vector2.Zero, 1f, SpriteEffects.None,
                    1f);
                Vector2 stringPos = new Vector2(aPositionsOfOptions[i].X + OptFrameWidth / 2 - stringSize.X / 2,
                    aPositionsOfOptions[i].Y + OptFrameHeight / 2 - stringSize.Y / 2);
                drawer.DrawString(aFont, aOptionNames[i], stringPos, Color.Black, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
            }
            Rectangle destRect = new Rectangle(0, 0, aGraphicsDevice.Viewport.Width, aGraphicsDevice.Viewport.Height);
            drawer.Draw(aMultiplayerMenuBackg, destinationRectangle: destRect, color: Color.White, layerDepth: 0f);
        }

        protected virtual void OnOptionSelectedEvent(MultMenuOptEventArgs e)
        {
            var handler = OptionSelectedEvent;
            if (handler != null) handler(this, e);
        }

        public string convert(Keys[] keys)
        {

            string output = "";

            bool usesShift = (keys.Contains(Keys.LeftShift) || keys.Contains(Keys.RightShift));



            foreach (Keys key in keys)
            {
                if (key >= Keys.A && key <= Keys.Z)

                    output += key.ToString();

                else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)

                    output += (key - Keys.NumPad0).ToString();

                else if (key >= Keys.D0 && key <= Keys.D9)
                {
                    string num = (key - Keys.D0).ToString();

                    #region special num chars
                    if (usesShift)
                    {
                        switch (num)
                        {
                            case "1":
                                {
                                    num = "!";
                                }
                                break;

                            case "2":
                                {
                                    num = "@";
                                }
                                break;
                            case "3":
                                {
                                    num = "#";
                                }
                                break;
                            case "4":
                                {
                                    num = "$";
                                }
                                break;
                            case "5":
                                {
                                    num = "%";
                                }
                                break;
                            case "6":
                                {
                                    num = "^";
                                }
                                break;
                            case "7":
                                {
                                    num = "&";
                                }
                                break;
                            case "8":
                                {
                                    num = "*";
                                }
                                break;
                            case "9":
                                {
                                    num = "(";
                                }
                                break;
                            case "0":
                                {
                                    num = ")";
                                }
                                break;
                        }
                    }
                    #endregion
                    output += num;
                }

                else if (key == Keys.OemPeriod)
                    output += ".";

                else if (key == Keys.OemTilde)
                    output += "'";

                else if (key == Keys.Space)
                    output += " ";

                else if (key == Keys.OemMinus)
                    output += "-";

                else if (key == Keys.OemPlus)
                    output += "+";

                else if (key == Keys.OemQuestion && usesShift)
                    output += "?";

                else if (key == Keys.Back) //backspace
                    output += "\b";
                if (!usesShift) //shouldn't need to upper because it's automagically in upper case

                    output = output.ToLower();
            }
            return output;
        }
    }

}

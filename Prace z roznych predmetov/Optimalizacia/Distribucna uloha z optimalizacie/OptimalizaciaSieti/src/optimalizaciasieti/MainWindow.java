/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalizaciasieti;

import optimalizaciasieti.data.Mesto;
import optimalizaciasieti.data.Vrchol;
import optimalizaciasieti.data.Hrana;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;

/**
 *
 * @author Dominik
 */
public class MainWindow extends javax.swing.JFrame {

    private File aFileToProcessing;
    private List<String> aRegionToProcesing;
    private HashMap<Integer, Vrchol> aNodes;
    private HashMap<Integer, Hrana> aHrany;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        aRegionToProcesing = new ArrayList<>();
        aNodes = new HashMap<>();
        aHrany = new HashMap<>();
        jTextField1.setText("707");
        jTextField2.setText("50");
        jButtonNajdiMestaAObceVybrOkresov.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButtonNajdiMestaAObceVybrOkresov = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jButtonPridajOkres = new javax.swing.JButton();
        jButtonOdoberOkres = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jButtonVyberNNajobyvanejsich = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jButtonUrciHrany = new javax.swing.JButton();
        jButtonPriradHranamVzdialenosti = new javax.swing.JButton();
        jButtonFloyd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonNajdiMestaAObceVybrOkresov.setText("Najdi mesta a obce okresov");
        jButtonNajdiMestaAObceVybrOkresov.setEnabled(false);
        jButtonNajdiMestaAObceVybrOkresov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNajdiMestaAObceVybrOkresovActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButton2.setText("Vyber priecinok obsahujuci subory na spracovanie");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        jLabel1.setText("Spracovavany subor:");

        jLabel2.setText("No File");

        jLabel3.setText("Cisla okresov,ktorych mesta vybrat:");

        jScrollPane2.setViewportView(jList1);

        jButtonPridajOkres.setText("Pridaj okres");
        jButtonPridajOkres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPridajOkresActionPerformed(evt);
            }
        });

        jButtonOdoberOkres.setText("Odober okres");
        jButtonOdoberOkres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOdoberOkresActionPerformed(evt);
            }
        });

        jLabel4.setText("Id noveho okresu:");

        jButtonVyberNNajobyvanejsich.setText("Vyber n najobyvanejsich");
        jButtonVyberNNajobyvanejsich.setEnabled(false);
        jButtonVyberNNajobyvanejsich.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVyberNNajobyvanejsichActionPerformed(evt);
            }
        });

        jLabel5.setText("n:");

        jButton6.setText("Uloz vysledok do suboru");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButtonUrciHrany.setText("Urci hrany");
        jButtonUrciHrany.setEnabled(false);
        jButtonUrciHrany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUrciHranyActionPerformed(evt);
            }
        });

        jButtonPriradHranamVzdialenosti.setText("Vypocitaj vzdilenosti hran");
        jButtonPriradHranamVzdialenosti.setEnabled(false);
        jButtonPriradHranamVzdialenosti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPriradHranamVzdialenostiActionPerformed(evt);
            }
        });

        jButtonFloyd.setText("Vypocet Floydovym algoritmom");
        jButtonFloyd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFloydActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jButton2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButtonOdoberOkres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonPridajOkres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonVyberNNajobyvanejsich, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonNajdiMestaAObceVybrOkresov, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonUrciHrany)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonPriradHranamVzdialenosti))
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonFloyd)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonPridajOkres)
                                .addGap(97, 97, 97)
                                .addComponent(jButtonOdoberOkres)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonNajdiMestaAObceVybrOkresov)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonVyberNNajobyvanejsich)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonUrciHrany)
                            .addComponent(jButtonPriradHranamVzdialenosti))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonFloyd)
                        .addGap(1, 1, 1)
                        .addComponent(jButton6)
                        .addGap(0, 47, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        JFileChooser fCh = new JFileChooser(System.getProperty("user.home") + "/Desktop/");
        fCh.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fCh.showOpenDialog(this);
        if (fCh.getSelectedFile() != null) {
            aFileToProcessing = new File(fCh.getSelectedFile().getAbsolutePath()+"\\obce.txt");
            jButtonNajdiMestaAObceVybrOkresov.setEnabled(true);
            jLabel2.setText(aFileToProcessing.getPath());
        } else {
            fCh = null;
            jLabel2.setText("Ziadny subor vybrany");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButtonPridajOkresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPridajOkresActionPerformed
        if (jTextField1.getText().matches("\\d{3}") && !aRegionToProcesing.contains(jTextField1.getText())) {
            aRegionToProcesing.add(jTextField1.getText());
            jList1.setListData(aRegionToProcesing.toArray());
        }
    }//GEN-LAST:event_jButtonPridajOkresActionPerformed

    private void jButtonOdoberOkresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOdoberOkresActionPerformed
        aRegionToProcesing.remove(jList1.getSelectedIndex());
        jList1.setListData(aRegionToProcesing.toArray());
    }//GEN-LAST:event_jButtonOdoberOkresActionPerformed

    private void jButtonNajdiMestaAObceVybrOkresovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNajdiMestaAObceVybrOkresovActionPerformed
        aNodes.clear();
        for (int i = 0; i < aRegionToProcesing.size(); i++) {
            aNodes.putAll(Solver.filtrujObce(aFileToProcessing, aRegionToProcesing.get(i)));
        }
        aFileToProcessing = new File(aFileToProcessing.getParent()+"\\SR_incid.txt");
        jLabel2.setText(aFileToProcessing.getAbsolutePath());
        jTextArea1.setText(citiesToString(getMesta(aNodes)));
        jButtonNajdiMestaAObceVybrOkresov.setEnabled(false);
        jButtonVyberNNajobyvanejsich.setEnabled(true);
        jButtonUrciHrany.setEnabled(true);
        FileWorker.writeDataToFile(new File("./filtr_obce.txt"),jTextArea1.getText(),false);
    }//GEN-LAST:event_jButtonNajdiMestaAObceVybrOkresovActionPerformed

    private void jButtonVyberNNajobyvanejsichActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVyberNNajobyvanejsichActionPerformed
        List<Vrchol> m = new ArrayList<>(aNodes.values());
        Collections.sort(m);
        m = m.subList(0, Integer.parseInt(jTextField2.getText()));
        aNodes.clear();

        for (Iterator<Vrchol> prech = m.iterator(); prech.hasNext();) {
            Vrchol v = prech.next();
            aNodes.put(v.getaID(), v);
        }
        jTextArea1.setText(citiesToString(getMesta(aNodes)));
        FileWorker.writeDataToFile(new File("./filtr_obce.txt"),jTextArea1.getText(),false);
    }//GEN-LAST:event_jButtonVyberNNajobyvanejsichActionPerformed

    private List<Mesto> getMesta(Map<Integer, Vrchol> paVrcholy) {
        ArrayList<Mesto> m = new ArrayList<>();
        for (Vrchol prech : paVrcholy.values()) {
            if (prech.getaMesto() != null) {
                m.add(prech.getaMesto());
            }
        }
        Collections.sort(m);
        return m;
    }

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        JFileChooser fCh = new JFileChooser(System.getProperty("user.home") + "/Desktop/OS semsetrálka");
        fCh.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fCh.setApproveButtonText("Uloz");
        fCh.showOpenDialog(this);
        if (fCh.getSelectedFile() != null) {
            fCh.getSelectedFile();
            FileWorker.writeDataToFile(fCh.getSelectedFile(), jTextArea1.getText(), false);
        }
        jButtonVyberNNajobyvanejsich.setEnabled(false);
        jButtonUrciHrany.setEnabled(false);
        jButtonPriradHranamVzdialenosti.setEnabled(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButtonUrciHranyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUrciHranyActionPerformed
        if (aFileToProcessing != null) {
            aHrany = Solver.najdiHrany(aFileToProcessing, aNodes);
            jTextArea1.setText(aHrany.values().toString());
            aFileToProcessing=new File(aFileToProcessing.getParent()+"\\SR_cesty.ATR");
            jLabel2.setText(aFileToProcessing.getAbsolutePath());
            jButtonVyberNNajobyvanejsich.setEnabled(false);
            jButtonUrciHrany.setEnabled(false);
            jButtonPriradHranamVzdialenosti.setEnabled(true);
        }
    }//GEN-LAST:event_jButtonUrciHranyActionPerformed

    private void jButtonPriradHranamVzdialenostiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPriradHranamVzdialenostiActionPerformed
        if(aFileToProcessing!=null)
        {
            Solver.priradHranamVzdialenosti(aFileToProcessing,aHrany);
            jTextArea1.setText(aHrany.toString());
            jButtonPriradHranamVzdialenosti.setEnabled(false);
        }
    }//GEN-LAST:event_jButtonPriradHranamVzdialenostiActionPerformed

    private void jButtonFloydActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFloydActionPerformed
        Solver.inicializaciaFloyd(new ArrayList<Vrchol>(aNodes.values()),aHrany,Integer.parseInt(jTextField2.getText()));
    }//GEN-LAST:event_jButtonFloydActionPerformed

    private String citiesToString(List<Mesto> paCities) {
        String ret = "";
        String paSubData="";
        String pom;
        for (Iterator<Mesto> iterator = paCities.iterator(); iterator.hasNext();) {
            Mesto spracMesto=iterator.next();
            ret += spracMesto.toString();
            pom=spracMesto.getaNameOfCity();
            pom=pom.replaceAll("\\s","_");
            paSubData+=pom+"\t"; 
       }
        FileWorker.writeDataToFile(new File("./filtr_obce2.txt"),paSubData,false);
        return ret;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButtonFloyd;
    private javax.swing.JButton jButtonNajdiMestaAObceVybrOkresov;
    private javax.swing.JButton jButtonOdoberOkres;
    private javax.swing.JButton jButtonPridajOkres;
    private javax.swing.JButton jButtonPriradHranamVzdialenosti;
    private javax.swing.JButton jButtonUrciHrany;
    private javax.swing.JButton jButtonVyberNNajobyvanejsich;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}

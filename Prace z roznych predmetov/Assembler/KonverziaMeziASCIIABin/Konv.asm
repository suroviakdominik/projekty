
.MODEL small
.STACK 100h
.DATA
Retaz DB 8 DUP(?);deklaracia pola o velkosti 9B
DesiatPriNas DW 10
Retaz1 DB '32768',0Dh
RetazecNaZapis DB 15 dup(?)
.CODE

;------------------------------------------------PROCEDURA KONVERZIA CISLA V ASCII TYPU WORD KODE NA BINARNE CISLO----------------------------------------------
KonverziaZDesCislaNaDvojCislo PROC
;vstupnymi parametrami su OFFSET retazec ukonceny koncovym znakom 0Dh do registra BX a offset od ktoreho zacinaju cisla v retazci do registra DI
;skonvertovne cislo vracia v registri ax
          push cx
          push dx
          push di
          mov dx,0
          mov ax,0
          cmp byte ptr [bx+di],'-'
          jz CisloJeZaporne
          mov dl,0 
          
KonvertujRetazecNaCislo:
           
          
          mov cl,[bx+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          push dx
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          pop dx
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
          

CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov dl, 1
          jmp KonvertujRetazecNaCislo

RozhodniCiJeCisloZaporne:
          cmp dl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          NavratSProc:
          pop di
          pop dx 
          pop cx
          ret
DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla
         jmp NavratSProc

KonverziaZDesCislaNaDvojCislo ENDP

;-----------------------------------------PROCEDURA KONVERZIA CISLA V BINARNOM TVARE NA DESIATKOVE CISLO V ASCII KODE---------------------------------
;zapise binarne cislo do retazca/pola a to tak ze ho prekonvertuje na desiatkove cislo v ascii kode
;vstupny parameter je offset retazca, ktory konvertujeme v registri BX
;a index v poli od ktoreho sa bude zapisovat cislo

KonverziaZBinCislaNaDesiat PROC
         push ax
         push bx
         pop cx
         push dx
         push di
         mov cx,0
         cmp ax,0
         jnge VypisMinus
KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
          
          
         
VypisCislo:
        
        pop ax;vypise skonvertovane do retazca ktory je ako parameter
        add ax,'0'
        mov byte ptr [bx+di],al
        inc di
        loop VypisCislo
        mov byte ptr [bx+di],0Dh
        
        pop di
        pop dx
        pop cx
        pop bx
        pop ax
        ret

VypisMinus:
        mov byte ptr [bx+di],'-'
        inc di
        
        neg ax
        jmp KonvertujNaDekCislo
KonverziaZBinCislaNaDesiat ENDP
;------------------------------------------------HLAVNY PROGRAM-----------------------------------------------------------------------------
Zaciatok:
       mov ax,@DATA
       mov ds,ax
         ;.......................ak nacitavame retazec pomocou sluzby 9........................
         mov di, 2
         mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cisloword=5znakov=5B+0Dh=1B+velkost retazca=1B = 7
          
         mov DX,offset Retaz;do registra dx ulozime offset Retazca
         mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
         int 21h
           
         mov DL,0AH;uloz do registra posun dole
         mov AH,2;do registra AH uloz cislo sluzby
         int 21h
       
       mov bx,offset Retaz
       mov di,2
       call KonverziaZDesCislaNaDvojCislo
        
       
       
       ;...........................ak je retazec inicializovany na zaciatku..............................  
       mov bx, offset Retaz1
       mov di,0
       call KonverziaZDesCislaNaDvojCislo 
       
       mov bx, offset RetazecNaZapis
       mov di,0
       call KonverziaZBinCislaNaDesiat
       ;........................vypise retazec az kym nenarazi na ukoncovaci znak 0Dh...........................
       mov di,0;index od ktoreho sa zacina v danom poli cislo
       Vypis:
         cmp [RetazecNaZapis+di],0Dh
         je Koniec
         mov dl,[RetazecNaZapis+di]
         mov ah,2
         inc di
         int 21h
         jmp Vypis
Koniec:
       mov ah,4Ch
       int 21h
END Zaciatok






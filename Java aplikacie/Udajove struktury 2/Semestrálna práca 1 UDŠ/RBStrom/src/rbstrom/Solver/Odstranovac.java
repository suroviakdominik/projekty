/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom.Solver;

import java.util.List;
import rbstrom.RBStrom.Vrchol;


/**
 *
 * @author Dominik
 */
public class Odstranovac {

    public static  Vrchol getBratVrchola(Vrchol paVrchol) {
        if (paVrchol == null || paVrchol.getaOtec() == null) {
            throw new IllegalArgumentException("Vrchol nesmie byt null a ani jeho otec");
        }
        
        if (BVSAlgoritmy.jeVrcholPravymSynomOca(paVrchol)) {
            return paVrchol.getaOtec().getaLavySyn();
        } else {
            return paVrchol.getaOtec().getaPravySyn();
        }
    }

    /**
     *
     * @param <K>
     * @param <V>
     * @param paNahradnikOriginal- ten, ktory bol najdeny ako nahradnik napr. in
     * orderom
     * @param paNahradnikPoNahradeni - ten, ktory bol najdeny ako nahradnik a uz
     * nahradil odstranovany
     * @param paOdstrVrchol - ten, ktory sme chceli aby sa odstranil
     * @return
     */
    public static Vrchol zistiCiVznikolDoubleBlack(Vrchol paOdstranenyVrchol) {
        //System.out.println("\n----------ZISTI CI VZNIKOL DOUBLE BLACK----------------");
        //String end="\n----------------------END-ZISTI CI VZNIKOL DB------------------------\n";
        if(paOdstranenyVrchol.jeCerveny()){
            //System.out.println("Nevznikol Double Black. Odstranovany vrchol je cerveny"+end);
            return null;
        }
        if(!paOdstranenyVrchol.jeCerveny()&&(paOdstranenyVrchol.getaPravySyn()!=null&&paOdstranenyVrchol.getaPravySyn().jeCerveny())){
            //System.out.println("Nevznikol Double Black. Odstranovany vrchol je cierny ale ma cerveneho praveho syna"+end);
            paOdstranenyVrchol.getaPravySyn().setaJeCervenyVrchol(false);
            return null;
        }
        if(!paOdstranenyVrchol.jeCerveny()&&(paOdstranenyVrchol.getaLavySyn()!=null&&paOdstranenyVrchol.getaLavySyn().jeCerveny())){
            //System.out.println("Nevznikol Double Black. Odstranovany vrchol je cierny ale ma cerveneho laveho syna"+end);
            paOdstranenyVrchol.getaLavySyn().setaJeCervenyVrchol(false);
            return null;
        }
        //System.out.println("VZNIKOL DOUBLE BLACK PROBLEM"+end);
        return paOdstranenyVrchol;
    }

    
}

set pagesize 60
set linesize 123
column zamestnanec.meno heading "Meno" FORMAT A20;
column zamestnanec.priezvisko heading "Priezvisko" FORMAT A20;
column faktura.cislo_fa heading "Cislo Faktury" FORMAT A20;
column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad faktur zakaznikov' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia: " datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
priezvisko,meno,faktura.cislo_fa
FROM ZAKAZNIK JOIN FAKTURA
using(ID_ZAKAZNIKA)
ORDER BY PRIEZVISKO,meno;
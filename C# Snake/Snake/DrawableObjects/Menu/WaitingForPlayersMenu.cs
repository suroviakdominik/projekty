﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake.DrawableObjects
{
    /// <summary>
    /// enum polozky menu vo waiting for other players screen
    /// </summary>
    public enum WaitingForPlayersOptions
    {
        Cancel
    }

    /// <summary>
    /// ak pouzivatel zvoli najeku polozku z menu, vyvola sa udalost, ktora dostane ako parameter objekt tejto triedy
    /// </summary>
    public class WaitMenuOptEventArgs : EventArgs
    {
        private WaitingForPlayersOptions aSelOption;
        public WaitMenuOptEventArgs(WaitingForPlayersOptions paSelectedOption)
        {
            aSelOption = paSelectedOption;
        }

        public WaitingForPlayersOptions SelOption
        {
            get { return aSelOption; }
        }
    }

    /// <summary>
    /// Trieda ktora vykresluje a upravuje obrazovku pri cakani na drueheho hraca v multiplayer hre
    /// </summary>
    class WaitingForPlayersMenu : IDrawableObject
    {
        private const int VerticalGap = 10;///vertikalna medzera menu poloziek
        private const int HorizontalGap = 10;//////horizontalna medzera menu poloziek

        private Texture2D aGameOverBackgText;///obrazok vykreslovany na pozadi game over screen
        private GraphicsDevice aGraphicsDevice;///obrazovka, ktora sluzi ako platno
        private Texture2D aOptionFrame;///ramik pre polozku menu
        private SpriteFont aFont;///pismo pre polozku menu
        private int aCountOfOptions;///pocet enum moznosti menu
        private List<Vector2> aPositionsOfOptions;///zoznam suradnic, kde vykreslovat polozky menu
        private List<string> aOptionNames;///mena jednotlivych poloziek menu
        public WaitingForPlayersOptions SelectedOption { get; set; }///aktualne zvolena moznost v start menu - este nepotvrdena
                                                                    ///
        public event EventHandler<WaitMenuOptEventArgs> OptionSelectedEvent;/// udalost ktora sa vyvola ked pouzivatel vyberie jednu z menu
                                                                            /// 
        private float aTimeFromLastMenuItemsUpdate;///cas od posledneho updatu zvolenej polozky menu
        private int aNumberOfWaitCircleToDraw;/// pocet kruznic, ktore budu vykreslene
        private float aTimeFromLastCirclesUpdate;///cas od posledneho updatu kruznic

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public WaitingForPlayersMenu(GraphicsDevice graphicsDevice)
        {
            aGraphicsDevice = graphicsDevice;
            aPositionsOfOptions = new List<Vector2>();
            aOptionNames = new List<string>();
            aOptionNames.Add("Cancel");
            SelectedOption = WaitingForPlayersOptions.Cancel;
            aCountOfOptions = Enum.GetValues(typeof(WaitingForPlayersOptions)).Length;
            aNumberOfWaitCircleToDraw = 0;
        }

        /// <summary>
        /// sirka ramika menu polozky
        /// </summary>
        public float OptFrameWidth
        {
            get { return aOptionFrame.Width; }
        }

        /// <summary>
        /// vyska ramika menu polozky
        /// </summary>
        public float OptFrameHeight
        {
            get { return aOptionFrame.Height; }
        }

        /// <summary>
        /// nacita polozky, ktore sa budu vykreslovat, prehravat ...
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            aGameOverBackgText = content.Load<Texture2D>("wait-circle");
            aOptionFrame = content.Load<Texture2D>("start");
            aFont = content.Load<SpriteFont>("SMFont");
            int pom = aCountOfOptions;
            Vector2 firstOptPos = new Vector2(aGraphicsDevice.Viewport.Width - OptFrameWidth - HorizontalGap,
                                             aGraphicsDevice.Viewport.Height - pom * VerticalGap - pom * OptFrameHeight);

            for (int i = 0; i < aCountOfOptions; i++)
            {
                aPositionsOfOptions.Add(new Vector2(firstOptPos.X, firstOptPos.Y + i * VerticalGap + i * OptFrameHeight));
            }
        }

        /// <summary>
        /// Update vybranej moznosti- ak pouzivatel stlacil sipku nadol alebo nahor
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
            {
                OnOptionSelectedEvent(new WaitMenuOptEventArgs(SelectedOption));
            }
            aTimeFromLastMenuItemsUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            aTimeFromLastCirclesUpdate += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (aTimeFromLastMenuItemsUpdate > 0.1)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                {
                    if (SelectedOption != 0)
                    {
                        SelectedOption--;
                    }
                    else
                    {
                        SelectedOption = (WaitingForPlayersOptions)Enum.GetValues(typeof(WaitingForPlayersOptions)).GetValue(aCountOfOptions - 1);
                    }
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Down) ||
                         GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                {
                    if (SelectedOption != (WaitingForPlayersOptions)Enum.GetValues(typeof(WaitingForPlayersOptions)).GetValue(aCountOfOptions - 1))
                    {
                        SelectedOption++;
                    }
                    else
                    {
                        SelectedOption = 0;
                    }
                }
                aTimeFromLastMenuItemsUpdate = 0;
            }

            if (aTimeFromLastCirclesUpdate > 0.3)
            {
                aNumberOfWaitCircleToDraw = aNumberOfWaitCircleToDraw < 2 ? aNumberOfWaitCircleToDraw + 1 : 0;
                aTimeFromLastCirclesUpdate = 0;
            }
        }

        /// <summary>
        /// Vykreslenie poloziek menu
        /// </summary>
        /// <param name="drawer"></param>
        public void Draw(SpriteBatch drawer)
        {
            for (int i = 0; i < aPositionsOfOptions.Count; i++)
            {
                Color optCol = Color.White;
                if ((WaitingForPlayersOptions)Enum.GetValues(typeof(WaitingForPlayersOptions)).GetValue(i) == SelectedOption)
                {
                    optCol = Color.Red;
                }
                Vector2 stringSize = aFont.MeasureString(aOptionNames[i]);
                drawer.Draw(aOptionFrame, aPositionsOfOptions[i], null, optCol, 0f, Vector2.Zero, 1f, SpriteEffects.None,
                    1f);
                Vector2 stringPos = new Vector2(aPositionsOfOptions[i].X + OptFrameWidth / 2 - stringSize.X / 2,
                    aPositionsOfOptions[i].Y + OptFrameHeight / 2 - stringSize.Y / 2);
                drawer.DrawString(aFont, aOptionNames[i], stringPos, Color.Black, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
            }
            for (int i = 0; i <= aNumberOfWaitCircleToDraw; i++)
            {
                //Rectangle destRect = new Rectangle(0, 0, aGraphicsDevice.Viewport.Width, aGraphicsDevice.Viewport.Height);
                Vector2 positionToDraw = new Vector2(aGraphicsDevice.Viewport.Width / 2 - aGameOverBackgText.Width - 15 + i * 30,
                                             (float)aGraphicsDevice.Viewport.Height / 2);
                drawer.Draw(texture: aGameOverBackgText, position: positionToDraw, color: Color.White, layerDepth: 0f);
            }
        }

        protected virtual void OnOptionSelectedEvent(WaitMenuOptEventArgs e)
        {
            var handler = OptionSelectedEvent;
            if (handler != null) handler(this, e);
        }
    }
}

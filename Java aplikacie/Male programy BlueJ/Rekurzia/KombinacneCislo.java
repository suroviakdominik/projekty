
/**
 * Write a description of class KombinacneCislo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class KombinacneCislo
{
    public static long kombinacneCislo( int n, int k ) {
        if( k == 0 || k == n ) return 1;
        else return kombinacneCislo( n-1, k-1 ) +
            kombinacneCislo( n-1, k ) ;
    }
}   

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znamkyzoskusky;

import java.util.List;
import znamkyzoskusky.RiadokTabulky.TypZnamky;



/**
 *
 * @author Dominik
 */
public class ZnamkyOperacie {
    private List<String> aZnamky;

    public ZnamkyOperacie(List<String> aZnamky) {
        this.aZnamky = aZnamky;
    }
    
    public int getPocetZnamokTypu(TypZnamky paTypZnamky)
    {
        int ret=0;
        for(String prech:aZnamky)
        {
            if(prech.equals(paTypZnamky.getaZnamka()))
            {
                ret++;
            }
        }
        return ret;
    }
    
   
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.ArrayList;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.MiestnostVytah;

/**
 *
 * @author suroviak3
 */
public class PredmetOvladac implements IPredmet, Serializable {

    private int aPozicia = 1;
    private boolean aSmer = true;
    ArrayList<String> poshodia = new ArrayList<String>();

    @Override
    public String getNazov() {
        return "ovladac";
    }

    @Override
    public String getPopis() {
        return "ovladac vytahu";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        if (paHrac.dajPoziciu() instanceof MiestnostVytah) {
            MiestnostVytah vytah = (MiestnostVytah) paHrac.getaMapaHry().najdiMiestnost("vytah");

            if ((aPozicia == 0) && (aSmer == false)) {
                aSmer = true;

            } else if ((aPozicia == 3) && (aSmer == true)) {
                aSmer = false;
            }

            if (vytah.dajVychod("chodba1") != null) {
                aPozicia = 2;
            } else if (vytah.dajVychod("chodba2") != null) {
                aPozicia = 3;
            } else if (vytah.dajVychod("chodbaS") != null) {
                aPozicia = 0;
            } else if (vytah.dajVychod("chodbaA") != null) {
                aPozicia = 1;
            }

            poshodia.add("chodbaS");
            poshodia.add("chodbaA");
            poshodia.add("chodba1");
            poshodia.add("chodba2");
            IMiestnost aktualna = paHrac.getaMapaHry().najdiMiestnost(poshodia.get(aPozicia));

            vytah.zmazVychod(poshodia.get(aPozicia));

            if (aSmer == true) {

                IMiestnost nova = paHrac.getaMapaHry().najdiMiestnost(poshodia.get(++aPozicia));
                vytah.nastavVychody(nova);
                nova.nastavVychody(vytah);

            } else if (aSmer == false) {
                IMiestnost nova = paHrac.getaMapaHry().najdiMiestnost(poshodia.get(--aPozicia));
                vytah.nastavVychody(nova);
                nova.nastavVychody(vytah);

            }
            return "Ste na poschodi " + aPozicia + "\n" + vytah.dajPopisVychodov();
        } else {
            return "Ovladac mozno pouzit iba vo vytahu.";
        }

    }

}

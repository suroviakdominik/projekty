package com.suroviak.dominik.zooAplication.ActivityClass;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity.AktualityFragment;
import com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity.CategoriesFragment;
import com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity.EventsFragment;
import com.suroviak.dominik.zooAplication.FragmentsMainMenuActivity.InfoZOOFragment;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.BitmapWorker;

import java.util.Locale;


public class MainMenuTabActivity extends ActionBarActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence tabSetBeforeNavigationDrawerOpened;
    private AnimationAnimals aAnimationAnimals;
    private int aCurrentImageInAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu_tab);
        Log.e("DEBUG", "onCreate of MainMenu");


        if ((savedInstanceState != null)

                && (savedInstanceState.getSerializable("starttime") != null)) {

            String startTime = (String) savedInstanceState

                    .getSerializable("starttime");


        }
        if (savedInstanceState != null) {
            aCurrentImageInAnimation = savedInstanceState.getInt("CURRENT_ANIM_FRAME");
        } else {
            aCurrentImageInAnimation = 0;
        }

        //NAVIGATION DRAWER START
        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.navigation_drawer_items, mNavigationDrawerItemTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(tabSetBeforeNavigationDrawerOpened);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                tabSetBeforeNavigationDrawerOpened = getSupportActionBar().getTitle();
                getSupportActionBar().setTitle(getString(R.string.navigationDrawOpen));

            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //NAVIGATION DRAWER END


        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


        //TABS START
        // Create the adapter that will return a fragment for each of the four
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }


        //TABS END
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e("DEBUG", "onStart of MainMenuActivity");
        ImageView info = (ImageView) findViewById(R.id.animImage);
        aAnimationAnimals = new AnimationAnimals(this, info, 15);
        aAnimationAnimals.setOneCycle(false);
        aAnimationAnimals.setIndexOfPictureThatStartAnimate(aCurrentImageInAnimation);

    }

    @Override
    public void onResume() {
        aAnimationAnimals.execute();
        Log.e("DEBUG", "onResume of MainMenuActivity");
        super.onResume();

    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "onPause of MainMenuActivity");
        aAnimationAnimals.cancel(true);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Call the super. You never know :)
        super.onSaveInstanceState(savedInstanceState);

        // debug
        Log.e("DEBUG", "onSaveInstanceState of MainMenuActivity");

        savedInstanceState.putInt("CURRENT_ANIM_FRAME", aAnimationAnimals.getActutalFrameIndex());
    }

    //POLOŽKY Action Bar Start
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar searchsuggestionsitem clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (id == R.id.action_open_map) {
            Intent i = new Intent(this, ZooMapActivity.class);
            startActivity(i);
        }


        return super.onOptionsItemSelected(item);
    }
    //POLOŽKY Action Bar END


    //Navigation Drawer Methods
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
        getSupportActionBar().setTitle(mNavigationDrawerItemTitles[tab.getPosition()]);

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }


    //ADAPTER PRE TABS, ktorý pripája taby k aktivite
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    // Actual Info fragment activity
                    return new InfoZOOFragment();

                case 1:
                    // Animals fragment activity
                    Fragment lf = new CategoriesFragment();

                    return lf;
                case 2:
                    // Events fragment activity
                    return new EventsFragment();
                case 3:
                    // Info About ZOO fragment activity
                    return new AktualityFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section4).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
                case 3:
                    return getString(R.string.title_section1).toUpperCase(l);
            }
            return null;
        }
    }


    //Navigation Drawer Listener
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }

        private void selectItem(int position) {
            mViewPager.setCurrentItem(position);
            mDrawerLayout.closeDrawer(mDrawerList);
            //!!!  Po novom nevolať getActionBar() lebo vracia NULL, treba volať getSupportActionBar()
            //getSupportActionBar().setTitle(mNavigationDrawerItemTitles[position]);
            tabSetBeforeNavigationDrawerOpened = mNavigationDrawerItemTitles[position];

        }

    }

    private class AnimationAnimals extends AsyncTask<Void, Integer, Void> {
        private final int aTimeForFadeOut = 1000;//miliseconds
        private final int aTimeForFadeIn = 2000;//miliseconds
        private int[] aIDOfAnimDrawable;
        private ImageView aAnimationView;
        private int aTimeForShowingOnePicture;
        private int aIndexOfActutalPicture = 0;
        private int aIDShowingPicture;
        private boolean aOneCycle = true;
        private Activity aCallFromActivity;
        private int aWidth;
        private int aHeight;

        //dorobene moc nemenit
        public AnimationAnimals(Activity paCallFromActivity, ImageView paAnimView, int onePictureShoweTimeSecond) {
            aCallFromActivity = paCallFromActivity;
            Resources r = getResources();
            String a[] = r.getStringArray(R.array.backgroundAnimationMainMenu);
            aAnimationView = paAnimView;
            aTimeForShowingOnePicture = onePictureShoweTimeSecond;
            aWidth = aCallFromActivity.getWindow().getWindowManager().getDefaultDisplay().getWidth();
            aHeight = aCallFromActivity.getWindowManager().getDefaultDisplay().getHeight();
            aIDOfAnimDrawable = new int[a.length];
            for (int i = 0; i < a.length; i++) {
                int drawableResourceId = MainMenuTabActivity.this.getResources().getIdentifier(a[i], "drawable", MainMenuTabActivity.this.getPackageName());
                aIDOfAnimDrawable[i] = drawableResourceId;
            }
        }

        public void setOneCycle(boolean paJustOneCycle) {
            aOneCycle = paJustOneCycle;
        }

        public int getActutalFrameIndex() {
            return aIndexOfActutalPicture;
        }

        public boolean setIndexOfPictureThatStartAnimate(int paIndex) {
            if (paIndex < aIDOfAnimDrawable.length) {
                aIndexOfActutalPicture = paIndex;
                return true;
            }
            return false;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = aIndexOfActutalPicture; i < aIDOfAnimDrawable.length; i++) {
                aIDShowingPicture = aIDOfAnimDrawable[i];
                aIndexOfActutalPicture = i;
                changeImage();
                if (!aOneCycle && i == aIDOfAnimDrawable.length - 1) {
                    i = -1;
                }
                try {
                    Thread.sleep(aTimeForShowingOnePicture * 1000);
                    fadeOut();
                    Thread.sleep(aTimeForFadeOut);
                } catch (InterruptedException e) {
                    Log.e("E/ DEBUG", "ANIMATION WAS CANCELED: " + aCallFromActivity.getClass().toString());

                    return null;
                }
            }
            Log.e("E/ DEBUG", "ANIMATION WAS CANCELED: " + aCallFromActivity.getClass().toString());

            return null;
        }

        private void fadeOut() {
            aCallFromActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Animation fadeOut = new AlphaAnimation(1, 0.1f);
                    fadeOut.setInterpolator(new DecelerateInterpolator());
                    fadeOut.setDuration(aTimeForFadeOut);
                    aAnimationView.setAnimation(fadeOut);
                    aAnimationView.startAnimation(fadeOut);
                }
            });

        }

        private void changeImage() {
            aCallFromActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Animation fadeIn = new AlphaAnimation(0f, 1);
                    fadeIn.setInterpolator(new DecelerateInterpolator());
                    fadeIn.setDuration(aTimeForFadeIn);
                    aAnimationView.setAnimation(fadeIn);
                    aAnimationView.setImageBitmap(BitmapWorker.decodeSampledBitmapFromResource(getResources(), aIDShowingPicture, aWidth, aHeight));
                    System.gc();
                }
            });
        }
    }


}

;1) nacitaj retazec S a cisla N a P. Vypisuj retazec a pocnuc indexom P vynechaj z vypisu N znakov. V pripade, ze P je vacsie ako dlzka retazca, vypis cely retazec
;2) zadanie a) ale retzec nacitat zo suboru
;3) zadanie a) plus vypisat retzec na urcenu polohu na obrazovke 


.MODEL small
.STACK 100h
.DATA
Retaz1 DB 15 dup(?)
Riadok DW (0)
Stlpec DW (0)
Retaz DB 9 DUP(?)
PocetMiestRotacie DB 0 
NacitanyIndex DW 0
NacitanyPocetVyn DW 0
DesiatPriNas DW 10
PopisovacSuboru DW (?)
MenoSuboru DB 'Subor.txt',0 
errorOtvSub DB 'Subor sa nepodarilo pre citanie otvorit!$'
VypZadajteIndex DB 'Indexovanie zaciana cislom nula. Zadajte index: $'
VypZadajtePocZnak DB 'Zadajte pocet znakov, ktore chcete od indexu v retazci vynechat: $'
VypZadRiadok DB 'Zadajte riadok: $'
VypZadStlpec DB 'Zadajte stlpec: $'
.CODE
;.....................................................PROCEDURA NACITA CISLO A SKONVERTUJE HO NA BINARNE CISLO A ULOZI DO REGISTRA AX..................
Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 

;..................................................PROCEDURA NACITA RETAZEC S TERMINALU A ULOZI HO DO PREMENNEJ RETAZ1..........................
NacitajRetazec PROC
         push cx
         push ax
         mov Retaz1,13
         mov DX,offset Retaz1;do registra dx ulozime offset Retazca
         mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
         int 21h
           
         mov DL,0AH;uloz do registra posun dole
         mov AH,2;do registra AH uloz cislo sluzby
         int 21h
         
         mov ch,0
         mov cl,Retaz1[1]
         jcxz Na
        CyklusVypVlavo:
            mov DL,0DH;uloz do registra posun vlavo
            mov AH,2
            int 21h
            mov ax,0
         loop CyklusVypVlavo
         
       Na:
         pop ax
         pop cx
         ret
ENDP NacitajRetazec

;..............................................PROCEDURA VYPISE RETAZEC RETAZ1 POCNUC INDEXOM 2.....................................
VypRetazec PROC
push cx
push dx
push ax


mov di,2
mov ch,0
mov cl,Retaz1[1]
jcxz Kon
Vypisuj:
mov dl,Retaz1[di]
mov ah,2
int 21h
inc di
loop Vypisuj
Kon: 
    pop ax
    pop dx
    pop cx
    ret
ENDP VypRetazec


;........................................PROCEDURA ROTUJE DOLAVA RETAZ O POCET ZADANYCH MIEST V REGISTRI AX......................................
RotujL PROC
   push di
   push ax
   push bx
   push cx
   push dx
   InicRot:
   mov cx,ax
   jcxz Uk
   mov di,3
   
 Rotuj:
 mov bl,Retaz1[2]
   push di
   PosunLav:
      mov al,Retaz1[di]
      sub di,1
      mov Retaz1[di],al
      add di,2
    cmp Retaz1[di],0Dh 
    jnz PosunLav
    sub di,1
    mov Retaz1[di],bl 
    pop di
 loop Rotuj
Uk:
pop dx
pop cx
pop bx
pop ax
pop di
ret
ENDP RotujL
;.....................................PROCEDURA ZMAZE PREDCHADZAJUCI OBSAH TERMINALU.......................................................
ClrScr PROC
	push ax
	
	mov ah,0
	mov al,3
	int 10h
	
	pop ax
	
	ret
ClrScr ENDP
;..........................................................PROCEDURA VYPISUJE RETAZEC A POCNUC NACITANYM INDEXOM VYNECHA NACITANY POCET ZNAKOV.......
VypisAPocnucIndexNevypPoZadZnak PROC
push cx
push dx
push ax
push bx

mov bx,NacitanyIndex
cmp bl,Retaz1[1]
jg VypCelyRetazec
mov di,2
mov bx,0
mov ch,0
mov cl,Retaz1[1]
jcxz Koni;ak nebol nacitany ani jeden znak skoc na navestie koniec
;/////////////////////////
VypisujAVyn:
  cmp bx,NacitanyIndex
  je JePocetVynZnakovVacsiAkoNula
  
  mov dl,Retaz1[di]
  mov ah,2
  int 21h

  inc bx
  inc di
loop VypisujAVyn

jmp Koni
;//////////////////////

;//////////////////////
JePocetVynZnakovVacsiAkoNula:
   cmp NacitanyPocetVyn,0
   jg VynechajZnaky
   
   inc bx
   jmp VypisujAVyn
;//////////////////////

;//////////////////////
VynechajZnaky:
  sub NacitanyPocetVyn,1
  cmp NacitanyPocetVyn,0
  inc bx
  inc di
  dec cx
  cmp cx,0
  je Koni
  cmp NacitanyPocetVyn,0
  je VypisujAVyn
  
  jmp VynechajZnaky
;//////////////////////
  
;//////////////////////
 VypCelyRetazec:
       call VypRetazec
;/////////////////////
Koni:
    pop bx 
    pop ax
    pop dx
    pop cx
    ret
ENDP VypisAPocnucIndexNevypPoZadZnak

OpenFileForRead PROC 
        push ax
        mov al,0
        mov ah,3Dh
        int 21h
        mov PopisovacSuboru,ax;ak subor s menom este neexistuje v ax bude 2, pri pokuse citania zo suboru sa bude citat z klavesnice
        lahf;skopiruje register priznakov flags do registra ah
        AND ah,00000001b;vsetky ostatne az na carryflag vynuluje, ak cf=1 subor pre citanie sa nepodarilo otvorit
        
        cmp ah,1; ak je cf=1 vypise chybovu hlasku
        je VypisChybHlasku
        
        
        pop ax
        ret
         VypisChybHlasku:
        mov dx,offset errorOtvSub
        mov ah, 9
        int 21h 
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h 
        
        pop ax
        ret
        
OpenFileForRead ENDP

Zadanie1 PROC
;-----------------------------------------------ZADANIE 1-------------------------------------------------------
          push ax
          push bx
          push cx
          push dx
          push si
          push di
          call NacitajRetazec;NacitaRetazec
          call VypRetazec;pre kontrolu ci retazec spravne nacitalo
          
          ;........................................................
          ZadIndex:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadajteIndex;dostaneme vyzvu na zadanie indexu od ktoreho chceme vynechavat znaky
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jl ZadIndex
          
          mov NacitanyIndex,ax
          
          ;.........................................................

          ;.........................................................
          ZadPocVynZnak:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadajtePocZnak;dostaneme vyzvu na zadanie poctu vynechanych znakov
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jl ZadPocVynZnak
          
          mov NacitanyPocetVyn,ax
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          ;.......................................................
          call VypisAPocnucIndexNevypPoZadZnak
          pop di
          pop si
          pop dx
          pop cx
          pop bx
          pop ax
          ret
ENDP Zadanie1

Zadanie2 PROC
;-----------------------------------------------ZADANIE 2-------------------------------------------------------
          push ax
          push bx 
          push cx
          push dx
          push si
          push di
          mov dx, offset MenoSuboru
          call OpenFileForRead
          
          ;--------------------------------CITANIE ZO SUBORU---------------------------------------------------
          ;Retaz1 naplni ascii znakmi zo suboru od indexu 2
          mov bx,PopisovacSuboru;ak je popisovac suboru 2, tak znaky musime zadat z klavesnice lebo subor sa pre citanie nepodarilo otvorit
          mov cx,12;pocet bytov, ktore sa budu kopirovat do retazca(vratane medzier=1B)
          lea dx, Retaz1[2];offset pola kde sa budu znaky kopirovat 
          mov ah,3Fh
          int 21h 
          mov Retaz1[1],al
          mov Retaz1,13
          mov di,ax
          add di,1
          mov Retaz[di],0Dh

          ;-------------------------------ZATVORENIE SUBORU PRE CITANIE----------------------------------------
           mov bx,PopisovacSuboru
           mov ah,3Eh
           int 21h
 
          call VypRetazec;vypise nacitany retazec zo suboru
          ;........................................................
          ZadIndex1:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadajteIndex;dostaneme vyzvu na zadanie indexu od ktoreho chceme vynechavat znaky
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jl ZadIndex1
          
          mov NacitanyIndex,ax
          
          ;.........................................................

          ;.........................................................
          ZadPocVynZnak1:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadajtePocZnak;dostaneme vyzvu na zadanie poctu vynechanych znakov
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jl ZadPocVynZnak1
          mov NacitanyPocetVyn,ax
          call VypisAPocnucIndexNevypPoZadZnak
          pop di
          pop si
          pop dx
          pop cx
          pop bx
          pop ax
          ret
Zadanie2 ENDP

Zadanie3 PROC
          push ax
          push bx 
          push cx
          push dx
          push si
          push di
;-----------------------------------------------ZADANIE 3-------------------------------------------------------
          VyzvaRiadok:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadRiadok
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jnge VyzvaRiadok
          mov Riadok,ax         
 
          VyzvaStlpec:
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov dx,offset VypZadStlpec
          mov ah,9
          int 21h
          call Read
          cmp ax,0
          jnge VyzvaStlpec
          mov Stlpec,ax

          call CLRSCR
          mov dh,byte ptr Riadok
          mov dl,byte ptr Stlpec
          mov ah,02h
          int 10h
          call VypRetazec
          pop di
          pop si
          pop dx
          pop cx
          pop bx
          pop ax
          ret
Zadanie3 ENDP
;------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call Zadanie1
          call Zadanie2
          call Zadanie3
          
          
          
         
          
          

         
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































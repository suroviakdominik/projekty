/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearne.štrukurované.datové.typy.Objekty;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Scanner;
import javax.swing.*;

/**
 *
 * @author Dominik
 */
public class Osoba implements IObjekt, Serializable {

    private static boolean navrat;//na zaklade stlacenia tlacidla v modifikacnom okne sa nastavi atribut navrat
    private String meno;
    private String priezv;
    private int rok;//rok narodenia

    /**
     * Metóda nastaví hodnoty atribútov objektu. Hodnoty budú načitané z okna,
     * ktoré pouźívateľ vyplní.
     *
     * @return boolean - true ak úspešne naplnilo, false nenaplnilo
     */
    @Override
    public boolean napln() {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 200);
        ramik.setResizable(false);
        ramik.setTitle("Osoba  *  naplnenie");

        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(4, 2, 5, 10));

        //	so  vstupnými poľami
        final JTextField txtPriezv = new JTextField();
        final JTextField txtMeno = new JTextField();
        final JTextField txtRok = new JTextField();

        // 	a dvoma tlačidlami
        JButton btUlozit = new JButton("Uložiť");
        btUlozit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    priezv = txtPriezv.getText().trim();
                    if (priezv.length() < 2) {
                        throw new IllegalArgumentException(
                                "\n     neprípustná dĺžka   / min. 2 znaky /");
                    }

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka priezviska", JOptionPane.ERROR_MESSAGE);
                    txtPriezv.requestFocus();
                    return;
                }

                try {
                    meno = txtMeno.getText().trim();
                    if (meno.length() < 2) {
                        throw new IllegalArgumentException("\n neprípustná dĺžka   / min. 2 znaky /");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba dlzka mena", JOptionPane.ERROR_MESSAGE);
                    txtPriezv.requestFocus();
                    return;
                }

                try {
                    rok = Integer.parseInt(txtRok.getText().trim());
                    if (rok < 1901 || rok > 2099) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný rozsah / 1901-2099 /");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null,
                            ex + "\n     údaj musí byť celočíselný",
                            "Chyba roku narodenia", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(null, ex,
                            "Chyba roku narodenia", JOptionPane.ERROR_MESSAGE);
                    txtRok.requestFocus();
                    return;
                }

                navrat = true;
                ramik.dispose();

            }
        });

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                navrat = false;
                ramik.dispose();

            }
        });

        //	a popisnými poľami,    a všetko poskladáme
        ramik.add(new JLabel("   Priezvisko      :  ", JLabel.RIGHT));
        ramik.add(txtPriezv);
        ramik.add(new JLabel("   Meno            :  ", JLabel.RIGHT));
        ramik.add(txtMeno);
        ramik.add(new JLabel("   Rok narodenia   :  ", JLabel.RIGHT));
        ramik.add(txtRok);

        ramik.add(btUlozit);
        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda zobrazí okno na načitanie hodnôt atribútorv objektu a na základe
     * zadanýcho hodnôt nastaví atribúty tohto objektu.
     *
     * @return boolean - true ak načítalo, false ak nenačitalo
     */
    @Override
    public boolean zobraz() {
        final JDialog ramik = new JDialog();
        ramik.setLocation(600, 100);
        ramik.setSize(500, 200);
        ramik.setResizable(false);
        ramik.setTitle("Osoba  *  Udaje");
        ramik.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        ramik.setLayout(new GridLayout(4, 2, 5, 10));

        ramik.add(new JLabel("   Priezvisko      :  ", JLabel.RIGHT));
        ramik.add(new JLabel(priezv));
        ramik.add(new JLabel("   Meno            :  ", JLabel.RIGHT));
        ramik.add(new JLabel(meno));
        ramik.add(new JLabel("   Rok narodenia   :  ", JLabel.RIGHT));
        ramik.add(new JLabel(rok + ""));

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                navrat = false;
                ramik.dispose();
            }
        });

        ramik.add(btStorno);
        ramik.setModal(true);
        ramik.setVisible(true);
        return navrat;

    }

    /**
     * Metóda načita hodnoty atribúto zo súboru, ktorý bol predtým uložený
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param panazov-názov súboru z ktorého sa má načítať
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void nacitajZoSuboru(File paPodadresar, String panazov) throws FileNotFoundException {
        File file = new File(paPodadresar, panazov);
        Scanner scan = new Scanner(file);

        meno = scan.nextLine();
        priezv = scan.nextLine();
        rok = scan.nextInt();

        scan.close();
    }

    /**
     * Metóda zapíše hodnoty atribútov objektu do súboru, ktorý budeme neskôr
     * používať pre načitanie objektu zo súboru
     *
     * @param paPodadresar-podadresár odkiaľ sa číta
     * @param paCisloSuboru-poradie uloženia súboru
     * @throws IOException-chyba ak sa súbor nenašiel, alebo jeho štruktúra bola
     * zmenená v textovom editore
     */
    @Override
    public void zapisDoSuboru(File paPodadresar, int paCisloSuboru) throws FileNotFoundException {
        File subor = new File(paPodadresar, "O" + (paCisloSuboru + 1000) + ".txt");
        try (PrintWriter pw = new PrintWriter(subor)) {
            pw.println(meno); // napíšeme do súboru "Jano" a čakáme na ďalší zápis konci tohto riadku
            pw.println(priezv); // napíšeme do súboru "Javák", ideme do nového riadku a čakáme
            pw.println(rok); // ideme do nového riadku a čakáme

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Do suboru sa nepodarilo ulozit objekt. Osoba ukladanie");
        }
    }

    /**
     * Metóda vráti vo forme Stringu popis objektu Osoba
     *
     * @return String-osoba popis
     */
    @Override
    public String toString() {
        String s = "";
        s += "Objekt: Osoba, " + "Meno: " + meno + ", Priezvisko: " + priezv + ", Rok narodenia: " + rok;
        return s;
    }

}

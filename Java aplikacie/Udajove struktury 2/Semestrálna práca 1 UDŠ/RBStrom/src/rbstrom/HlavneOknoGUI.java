/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.basic.BasicOptionPaneUI;

/**
 *
 * @author Dominik
 */
public class HlavneOknoGUI extends JFrame {

    private JPanel aMainPanel;
    private StromPlatno aStromPlatno;
    private JScrollPane aScrollStromPlatno;
    private RBStrom<Integer,Integer> aStrom;
    private JPanel aHornyPanelTlacidiel;

    public HlavneOknoGUI() {
        setPreferredSize(new Dimension(800, 800));
        setTitle("RB STROM");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        aMainPanel = new JPanel(new BorderLayout());
        aMainPanel.setBackground(Color.LIGHT_GRAY);
        aStrom = new RBStrom<Integer,Integer>();
        aStromPlatno = new StromPlatno(aStrom);
        aStromPlatno.setBackground(Color.white);
        aHornyPanelTlacidiel=new JPanel(new BasicOptionPaneUI.ButtonAreaLayout(true, 10));
        aHornyPanelTlacidiel.setBackground(Color.DARK_GRAY);
        
        //Integer[] init=new Integer[]{38,80,24,19,51,73,60,39,71,20,92,41,46,53,79,78,62,31,48,81};
        //aStrom.nastavInit(init,1);
        //aStromPlatno.aktualizujData();
        inicializujKomponenty();
        this.add(aMainPanel, BorderLayout.CENTER);
        pack();
    }

    private void inicializujKomponenty() {
        aScrollStromPlatno = new JScrollPane(aStromPlatno, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        aMainPanel.add(aScrollStromPlatno, BorderLayout.CENTER);
        aMainPanel.add(aHornyPanelTlacidiel,BorderLayout.NORTH);
        pridajTlacidloPridajVrchol();
        pridajTlacidloOdoberVrchol();
        pridajTlacidlaZoom();
        pridajTlacidloGeneruj();
    }

    private void pridajTlacidloPridajVrchol() {
        JButton bPridajTlacidlo = new JButton("Pridaj prvok");
        bPridajTlacidlo.setRequestFocusEnabled(true);
        aHornyPanelTlacidiel.add(bPridajTlacidlo);
        bPridajTlacidlo.addActionListener((ActionEvent e) -> {
            Integer lpPridavanyPrvok=dajHodnotuEditovanehoPrvku();
            if(lpPridavanyPrvok!=null)
            {
                aStrom.pridaj(lpPridavanyPrvok,1);
                aStromPlatno.aktualizujData();
            }
        });
    }

    private void pridajTlacidloOdoberVrchol() {
        JButton bPridajTlacidlo = new JButton("Odober prvok");
        bPridajTlacidlo.setRequestFocusEnabled(true);
        aHornyPanelTlacidiel.add(bPridajTlacidlo);
        bPridajTlacidlo.addActionListener((ActionEvent e) -> {
            Integer lpOdoberanehoPrvku=dajHodnotuEditovanehoPrvku();
            if(lpOdoberanehoPrvku!=null)
            {
                aStrom.odstran(lpOdoberanehoPrvku);
                aStromPlatno.aktualizujData();
            }
        });
    }
    
    private Integer dajHodnotuEditovanehoPrvku() {
        PridajVrchol dial = new PridajVrchol(this, true);
        dial.setLocationRelativeTo(this);
        dial.setVisible(true);
        if (dial.getReturnStatus() == 1) {
           return dial.getNovyVrcholData();
        }
        return null;
    }

    private void pridajTlacidlaZoom() {
        JButton bPribliz = new JButton("+");
        JButton bOddial = new JButton("-");
        aMainPanel.add(bPribliz, BorderLayout.EAST);
        aMainPanel.add(bOddial, BorderLayout.WEST);
        bPribliz.addActionListener((ActionEvent e) -> {
            aStromPlatno.setPreferredSize(new Dimension(aStromPlatno.getWidth() + 200, aStromPlatno.getHeight() + 100));
            aStromPlatno.revalidate();
        });
        bOddial.addActionListener((ActionEvent e) -> {
            aStromPlatno.setPreferredSize(new Dimension(aStromPlatno.getWidth() - 200, aStromPlatno.getHeight() - 100));
            aStromPlatno.revalidate();
        });
    }

    private void pridajTlacidloGeneruj() {
        JButton bGen = new JButton("Generuj");
        aMainPanel.add(bGen, BorderLayout.SOUTH);
        bGen.addActionListener((ActionEvent e) -> {
            aStrom.vymazVsetkyPrvkyStromu();
            Random gen = new Random();
            int count = gen.nextInt(100) + 30;
            for (int i = 0; i < count; i++) {
                aStrom.pridaj(gen.nextInt(100),1);
            }
            aStromPlatno.aktualizujData();
            System.out.println("Vygeneroval som: "+count+" prvkov. Strom hovori ze ma: "+aStrom.getaPocetPrvkov()+"\n"
                    + "Ak ma strom menej ako vygenerovanych. Potom sa viac krat vygeneroval rovnaky kluc.");
        });
    }

    
}

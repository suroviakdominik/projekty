/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katastersoft.data;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class ListVlastnictva {

    private KatastralneUzemie aKU;
    private int aCisloLV;
    private List<Nehnutelnost> aNehnutelnostiNaLv;
    private List<MajetkovyPodiel> aMajetkovePodiely;

    private final Logger LoggerLV = Logger.getLogger(this.getClass().getName());

    public ListVlastnictva(KatastralneUzemie aKU, int aCisloLV) {
        this.aKU = aKU;
        this.aCisloLV = aCisloLV;
        aNehnutelnostiNaLv = new LinkedList<>();
        aMajetkovePodiely = new LinkedList<>();
        LoggerLV.setLevel(Level.OFF);
    }

    public KatastralneUzemie getaKU() {
        return aKU;
    }

    public int getaCisloLV() {
        return aCisloLV;
    }

    public void setaCisloLV(int aCisloLV) {
        this.aCisloLV = aCisloLV;
    }

    /**
     * zmeni LV KU v ktorom sa nachadza. Pozor tato metoda ho nepresunie do LV
     * noveho KU
     *
     * @param aKU
     */
    public void setaKU(KatastralneUzemie aKU) {
        this.aKU = aKU;
    }

    /**
     *Prida nehnutelnost na LV
     * @param paNehnutelnost
     * @return false-ak uz exituje nehn. zo zadanym sup. cislom v inom LV KU
     */
    public boolean pridajNehnutelnost(Nehnutelnost paNehnutelnost) {
        if (aKU.existujeNehnutelnost(paNehnutelnost.getaSupisneCislo())) {
            LoggerLV.log(Level.INFO, "Nepridal som nehnutelnost do zoznamu nehnutelnosti\n"
                    + "lebo sa uz nachadza v niektorom z LV KU");
            return false;
        }
        aNehnutelnostiNaLv.add(paNehnutelnost);
        paNehnutelnost.setaZapNaLV(this);
        LoggerLV.log(Level.INFO, "Pridal som nehnutelnost do zoznamu nehnutelnosti LV");
        if (aKU.pridajNovuNehnutelnost(paNehnutelnost)) {
            LoggerLV.log(Level.INFO, "Pridal som nehnutelnost do zoznamu nehnutelnosti KU");
        }
        if (aNehnutelnostiNaLv.contains(paNehnutelnost)) {
            return true;
        }
        throw new IllegalStateException("Nehnutelnost sa nepodarilo pridat na LV aj napriek tomu,"
                + "ze sa este medzi nehnutelnostami na LV nenachadza. CHYBA Zoznam.");
    }

    /**
     * Odstrani nehnutelnost z LV
     * @param supisneCislo
     * @return zmazana nehnutelnost
     *          null - ak nehnutelnost na LV neexistovala
     */
    public Nehnutelnost odstranNehnutelnostZLV(Integer supisneCislo) {
        Nehnutelnost ret = null;
        for (Iterator<Nehnutelnost> iterator = aNehnutelnostiNaLv.iterator(); iterator.hasNext();) {
            Nehnutelnost next = iterator.next();
            if (next.getaSupisneCislo().equals(supisneCislo)) {
                ret = next;
                iterator.remove();
                break;
            }
        }
        if (ret != null) {
            LoggerLV.log(Level.INFO, "Zmazal som nehnutelnost v zozname nehnutelnosti LV");
            if (aKU.odstranExistujucNehnutelnost(supisneCislo) == null) {
                throw new IllegalStateException("Nehnutelnost, sa mi podarilo zmazat z LV ale nie z KU");
            } else {
                LoggerLV.log(Level.INFO, "Zmazal som nehnutelnost v zozname nehnutelnosti KU");
                ret.zrusUbytovanychAkRusisNehnutelnost();
            }
        }
        return ret;
    }

    /**
     * Prida osobe majetkovy podiel na nehnutelnostiach tohto LV
     * @param paOsoba
     * @return 
     */
    public boolean pridajMajetkovyPodiel(Osoba paOsoba) {
        for (MajetkovyPodiel prech : aMajetkovePodiely) {
            if (prech.aOsoba.getaRodneCislo().equals(paOsoba.getaRodneCislo())) {
                LoggerLV.log(Level.INFO, "Osobe som nepridal majetkovy podiel, lebo ho uz ma.");
                return false;
            }
        }
        aMajetkovePodiely.add(new MajetkovyPodiel(paOsoba, 0));
        paOsoba.pridajNovyLV(this);
        LoggerLV.log(Level.INFO, "Osobe som pridal majetkovy podiel");
        prepocitajPodiel();
        return true;
    }

    private void prepocitajPodiel() {
        for (MajetkovyPodiel prech : aMajetkovePodiely) {
            prech.aMajetkovyPoddiel = (float) 1 / aMajetkovePodiely.size();
        }
    }

    /**
     * Odoberie osobe majetkovy podiel na nehnutelnostiach tohto LV.
     * zrusi ho zo zonamu LV osoby a odstrani osobu z majitelov LV
     * @param paRodneCislo
     * @return 
     */
    public boolean odoberMajetkovyPodiel(String paRodneCislo) {
        for (Iterator<MajetkovyPodiel> iterator = aMajetkovePodiely.iterator(); iterator.hasNext();) {
            MajetkovyPodiel next = iterator.next();
            if (next.aOsoba.getaRodneCislo().equals(paRodneCislo)) {
                LoggerLV.log(Level.INFO, "Osobe som odobral majetkovy podiel");
                next.aOsoba.odoberLV(this);
                iterator.remove();
                prepocitajPodiel();
                return true;
            }
        }
        LoggerLV.log(Level.INFO, "Osobe som neodobral majetkovy podiel, lebo ziadny na tomto LV nema.");
        return false;
    }

    /**
     * odoberie majetkovy podiel vsetkym osobam na tomto LV
     */
    public void odoberMajetkovePodielyVsetkymOsobam() {
        for (Iterator<MajetkovyPodiel> iterator = aMajetkovePodiely.iterator(); iterator.hasNext();) {
            MajetkovyPodiel next = iterator.next();
            next.aOsoba.odoberLV(this);
        }
    }

    @Override
    public String toString() {
        return "------------------------------------ListVlastnictva------------------------------------\n"
                + "Katastralne uzemie: " + aKU + "\n"
                + "Cislo LV=" + aCisloLV + "\n"
                + "Nehnutelnosti:\n" + aNehnutelnostiNaLv + "\n"
                + "MajetkovePodiely=" + aMajetkovePodiely + "\n"
                + "---------------------------------------------------------------------------------------\n\n";
    }

    public List<Nehnutelnost> getNehnutelnosti() {
        return aNehnutelnostiNaLv;
    }

    private class MajetkovyPodiel {

        private Osoba aOsoba;
        private float aMajetkovyPoddiel;

        public MajetkovyPodiel(Osoba aOsoba, float aMajetkovyPoddiel) {
            this.aOsoba = aOsoba;
            this.aMajetkovyPoddiel = aMajetkovyPoddiel;
        }

        @Override
        public String toString() {
            return aOsoba.getaMeno() + " " + aOsoba.getaPriezvisko() + " "
                    + " RC: " + aOsoba.getaRodneCislo() + " Podiel: " + aMajetkovyPoddiel + "\n";
        }

    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Questy;

import wof00.Hra.Hrac;
import wof00.Predmety.IPredmet;
import wof00.Predmety.PredmetIndex;

/**
 *
 * @author suroviak3
 */
public class QuestBcTitul implements IQuest, IPredmet {

    private String aNazov;
    private String aPopis;

    public QuestBcTitul() {
        this.aNazov = "bcTitul";
        this.aPopis = "zbieraj kredity a ziskaj bc tiutul";
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return aPopis;
    }

    @Override
    public void prijmi(Hrac paHrac) {
        IPredmet quest = paHrac.dajPoziciu().zoberPredmet(aNazov);
        paHrac.zdvihniPredmet(quest);

        PredmetIndex bcIndex = new PredmetIndex("bakalrskyIndex", "index so znamkami bakalarskeho studia");
        paHrac.zdvihniPredmet(bcIndex);

        System.out.println("Prijal si quest: " + aNazov);
    }

    @Override
    public void ukonci(Hrac paHrac) {
        PredmetIndex vyplneny = (PredmetIndex) paHrac.dajPredmet("bakalrskyIndex");
        if (vyplneny != null) {
            if (vyplneny.pocetZnamok() >= 2) {
                paHrac.setaMeno("Bc. " + paHrac.getMeno());
                paHrac.polozPredmet(vyplneny.getNazov());
                paHrac.polozPredmet(aNazov);
                System.out.println("Quest je ukonceny. Pokracujte v hre " + paHrac.getMeno() + ".");
            } else {
                System.out.println("Nemate dost znamok.");
            }
        } else {
            System.out.println("Nemate index.");
        }

    }

    @Override
    public String pouzi(Hrac paHrac) {
        return "Quest sa neda pouzit";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Instancia tejto triedy je kompletna cesta zlozena z bodov, ktore
 * su typu BodNaCeste
 * @author Dominik
 */
public class Cesta implements Serializable{
    private List<BodNaCeste> aBodyNaCeste;
    private static final long serialVersionUID = 3L;

    /**
     * Vytvori novu cestu a inicuializuje list, kde budu ulozene body cesty
     */
    public Cesta() {
        aBodyNaCeste=new ArrayList<>();
    }
    
    /**
     * prida novy bod na cestu zadany ako parameter
     * @param paNovyBod - pridavany bod
     */
    public void pridajNovyBodNaCestu(BodNaCeste paNovyBod)
    {
        if(!aBodyNaCeste.contains(paNovyBod))
        aBodyNaCeste.add(paNovyBod);
        else
        {
            System.out.println("DEBUG: Nepridal som bod na cestu lebo uz existuje.");
        }
    }
    
    /**
     * odstrani bod z cesty, ktory je parametrom metody
     * @param paBodNaOdstranenie - bod, ktory bude z cesty odstraneny
     */
    public void odstranBodZCesty(BodNaCeste paBodNaOdstranenie)
    {
        aBodyNaCeste.remove(paBodNaOdstranenie);
    }
   
    /**
     * vrati pocet bodov cesty
     * @return pocet bodov cesty
     */
    public int getPocetBodovNaCeste()
    {
        return aBodyNaCeste.size();
    }
    
    /**
     * vrati bod cety nachadzajuci sa v poli na indexe param
     * @param paIndexBodu - index do pola bodov cesty
     * @return 
     */
    public BodNaCeste getBodNaCeste(int paIndexBodu)
    {
        return aBodyNaCeste.get(paIndexBodu);
    }
    
    
    /**
     * vrati bod na ceste zo suradnicami zadanymi ako parameter
     * @param lat - zeeisna sirka
     * @param lon - zemepisna dlzka
     * @return bod zo zadanou zem dlzkou/ sirkou alebo null ak taky nie nie je
     */
    public BodNaCeste getBodNaCeste(Coordinate lat,Coordinate lon)
    {
        for(BodNaCeste prech:aBodyNaCeste)
        {
            if(prech.getaLatitude().equals(lat)&&prech.getaLongitude().equals(lon))
            {
                return prech;
            }
        }
        System.out.println("ERROR: Cesta: getBodNaCeste(Coord,Coord)- vracia null hodnotu");
        return null;
    }
}

import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
public class Bludisko
{
   private Policko[][] aBludisko;
   private int aDlzkaX;
   private int aDlzkaY;
   private Policko aStart;
   private Policko aCiel;
   
   public Bludisko()
   {
       aBludisko=null;
       aStart=null;
       aCiel=null;
       aDlzkaX=0;
       aDlzkaY=0;
   }    
   
   public void nacitaj(String paSubor) throws IOException//vynimka, ktora vyskoci ked subor neexistuje 
   {
      File subor=new File(paSubor);
      Scanner scan=new Scanner(subor);
      aDlzkaX=scan.nextInt();
      aDlzkaY=scan.nextInt();
      scan.nextLine();
      
      aBludisko=new Policko[aDlzkaX][aDlzkaY];
      String riadok;
      for (int y=0;y<aDlzkaY;y++)
      {
           riadok=scan.nextLine();
           char znak;
           for (int x=0;x<aDlzkaX;x++)
           {
               znak=riadok.charAt(x);
               Policko nove=new Policko(Stav.CharToStav(znak),x,y);
               aBludisko[x][y]=nove;
               if(nove.dajStav()==Stav.START)
               {  
                  aStart=nove;
               }
               else
               {
                 if(nove.dajStav()==Stav.CIEL)
               {  
                  aCiel=nove;
               }
            }
                  
           }
      }     
      scan.close();
   }
   
   public void vypisNaTerminal()
   {
       /**for(Policko[]polePolicok:aBludisko)
       {
           for (Policko policko:polePolicok	)
           {
               System.out.print(policko);
               System.out.println();
           }
       }*/
       for (int y=0;y<aDlzkaY;y++)
       {
           for (int x=0;x<aDlzkaX;x++)
           {
               System.out.print(aBludisko[x][y]);
           }
         System.out.println();
       }
   }
   
   private Policko dajPolicko(Policko paZKtoreho,Smer paSmer)
   {
       int X=paZKtoreho.dajX();
       int Y=paZKtoreho.dajY();
       switch(paSmer)
       {
           case SEVER:
              return Y-1>=0?aBludisko[X][Y-1]:null; 
           case JUH:
              return Y+1<aDlzkaY?aBludisko[X][Y+1]:null; 
           case VYCHOD:
              return X+1>aDlzkaX?aBludisko[X+1][Y]:null; 
           case ZAPAD:
              return X-1>=0?aBludisko[X-1][Y]:null; 
           default: return null;
       }
   }    
   
   private void Zapis(PrintWriter paPW)
   {
       for (int y=0;y<aDlzkaY;y++)
       {
           for (int x=0;x<aDlzkaX;x++)
           {
               paPW.print(aBludisko[x][y]);
           }
           paPW.println();
       }
   }
   
   public void Uloz(String paSubor) throws IOException
   {
     PrintWriter zapisovac=new PrintWriter(new File(paSubor));
     Zapis(zapisovac);
     zapisovac.close();
   }
   
   public void Ries()
   {
     ArrayList<Policko> riesenie=new ArrayList<Policko>(); 
     riesenie.add(aStart);
     
     aStart.nastavOdkialPrisiel(Smer.JUH);
     Policko posledne=aStart;
     while(riesenie.size()>0 && posledne!=aCiel)
     {
         Smer kadePojdem=posledne.kamNaposledySiel();

         Policko kamPojdem=dajPolicko(posledne,kadePojdem);
         
         while (kamPojdem==null || !kamPojdem.dajStav().MozemSpustit())
         {
             kadePojdem=kadePojdem.DOLAVA();
             kamPojdem=dajPolicko(posledne,kadePojdem);
         }
         
         posledne.OdisielSmerom(kadePojdem);
         kamPojdem.nastavOdkialPrisiel(kadePojdem.OpacnySmer());
         
         if(posledne.dajStav()==Stav.PREHLADANE)
         {   
             riesenie.remove(riesenie.size()-1);
         }
         else
         {
             riesenie.add(kamPojdem);
         }
         posledne=riesenie.get(riesenie.size()-1);
     }
    
   }
   
   private void ZapisRiesenie()
   {
       
   }
       
}   
               
  
    

   
    


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class PredmetNotebook implements IPredmet, Serializable {

    private String aNazov;
    private String aPopis;
    private Boolean mabaterku;

    public PredmetNotebook(String paNazov, String paPopis, Boolean mabaterku) {
        this.aNazov = paNazov;
        this.aPopis = paPopis;
        this.mabaterku = false;
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return aPopis;
    }

    @Override
    public String pouzi(Hrac paHrac) {
        if (mabaterku) {
            return "Notebook je spusteny a pripraveny";
        }
        return "Nemate baterku";
    }

    public void vlozBaterku() {
        mabaterku = true;
    }

}

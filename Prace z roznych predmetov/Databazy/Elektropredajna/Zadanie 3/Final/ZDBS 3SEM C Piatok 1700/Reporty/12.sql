set pagesize 60
set linesize 123
column Predajna.NAZOV_PREDAJNE HEADING "NAZOV PREDAJNE" FORMAT A10;
column zamestnanec.meno heading "Meno" FORMAT A10;
column zamestnanec.priezvisko heading "Priezvisko" FORMAT A10;
column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad zamestnancov jednotlivych predajni' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia: " datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
PREDAJNA.NAZOV_PREDAJNE,meno,priezvisko FROM ZAMESTNANEC JOIN PREDAJNA --prv� select od konzultanta
USING(id_Predajne)
ORDER BY PREDAJNA.NAZOV_PREDAJNE;
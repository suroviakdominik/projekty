/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacZdvihni implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paNazov) {
        IPredmet predmet = paHrac.dajPoziciu().zoberPredmet(paNazov);
        if (predmet == null) {
            System.out.println("Tento predmet tu nie je");
        } else {
            paHrac.zdvihniPredmet(predmet);
            paHrac.odpocitajZivot();
            System.out.println("Zdvihli ste predmet: " + paNazov + " z miestnosti: " + paHrac.dajPoziciu().getaNazov());

        }

        return false;
    }
}

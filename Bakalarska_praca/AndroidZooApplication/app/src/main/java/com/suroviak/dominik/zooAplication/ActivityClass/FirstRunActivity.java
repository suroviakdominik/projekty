package com.suroviak.dominik.zooAplication.ActivityClass;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.UnZipper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FirstRunActivity extends Activity {

    private String aAppMainDirectory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_run);
        aAppMainDirectory = Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER;
        FileWorker fw = new FileWorker(aAppMainDirectory);
        fw.createDirectory();
        new CopyFromAssetsThread().start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first_run, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar searchsuggestionsitem clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    private void copyFileOrDir(String path, String where) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            Log.i("ZOOApp", "copyFileOrDir() " + path);
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path, where);

            } else {
                String fullPath = where + path;
                Log.i("ZOOApp", "path=" + fullPath);
                File dir = new File(fullPath);
                if (!dir.exists() && !path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                    if (!dir.mkdirs())
                        Log.i("ZOOApp", "could not create dir " + fullPath);
                for (int i = 0; i < assets.length; ++i) {
                    String p;
                    if (path.equals(""))
                        p = "";
                    else
                        p = path + "/";

                    if (!path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                        copyFileOrDir(p + assets[i], where);
                }
            }
        } catch (IOException ex) {
            Log.e("ZOOApp", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename, String where) {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        String newFileName = null;
        try {
            Log.i("ZOOApp", "copyFile() " + filename);
            in = assetManager.open(filename);
            if (filename.endsWith(".jpg") || filename.endsWith(".png") || filename.endsWith(".gif")) // extension was added to avoid compression on APK file
                newFileName = where + filename.substring(0, filename.length() - 4);
            else
                newFileName = where + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[102400];
            long total = 0;
            int read;
            while ((read = in.read(buffer)) != -1) {
                total += read;
                out.write(buffer, 0, read);
                if (total > 1024 * 1024) {
                    total = 0;
                    out.flush();
                }
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("ZOOApp", "Exception in copyFile() of " + newFileName);
            Log.e("ZOOApp", "Exception in copyFile() " + e.toString());
        }

    }

    private class CopyFromAssetsThread extends Thread {
        public void run() {
            super.run();
            //new ProgressBarThread().start();
            Log.e("MyLog:", "Start copy data from assets folder");
            copyFileOrDir("Files.zip", aAppMainDirectory);
            UnZipper unz = new UnZipper(aAppMainDirectory + "Files.zip", aAppMainDirectory);
            unz.unzip();
            FileWorker fw = new FileWorker(aAppMainDirectory + "Files.zip");
            fw.deleteFile();
            Intent i = new Intent(getBaseContext(), ListViewUpdateActivity.class);
            startActivity(i);
            FirstRunActivity.this.finish();
            Log.e("MyLog:", "Assets copy done.");
        }
    }
}

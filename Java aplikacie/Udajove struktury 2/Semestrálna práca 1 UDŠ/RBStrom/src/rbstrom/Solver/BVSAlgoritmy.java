/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rbstrom.Solver;

import java.util.ArrayList;
import java.util.List;
import rbstrom.RBStrom;
import rbstrom.RBStrom.Vrchol;

/**
 *
 * @author Dominik
 */
public class BVSAlgoritmy {

    /**
     * Hlada hladany vrchol v strome. Ak ho nenajde a vratitiNullAkNajdem=true,
     * potom vrati otca kde moze byt umiestneny Ak ho nenajde a
     * vratitiNullAkNajdem=false, potom vrati null Ak ho najde a
     * vratitiNullAkNajdem=true, potom vrati null Ak ho najde a
     * vratitiNullAkNajdem=false, potom vrati prave tento najdeny Vrchol
     *
     * @param paHladanyVrchol--vrchol, ktory chceme najst
     * @param paZacniOd -- zaciatok prehladavanie praveho a laveho podstromu
     * @param vratitiNullAkNajdem-- true - vratim null ak ho najdem, inak oca ku
     * ktoremu moze byt priradeny -- false - vratim najdeny vrchol ak ho
     * najdem,inak null
     * @return
     */
    public static Vrchol najdiVhodneUmiestnenieAlgoritmomBVS(Vrchol paHladanyVrchol, Vrchol paZacniOd, boolean vratitiNullAkNajdem) {
        Vrchol prechadzac = paZacniOd;
        Vrchol ret = null;
        while (prechadzac != null) {
            ret = prechadzac;
            if (paHladanyVrchol.compareTo(prechadzac) < 0) {
                prechadzac = prechadzac.getaLavySyn();
            } else if (paHladanyVrchol.compareTo(prechadzac) > 0) {
                prechadzac = prechadzac.getaPravySyn();
            } else {
                if (vratitiNullAkNajdem) {
                    return null;
                } else {
                    return prechadzac;
                }
            }
        }

        if (vratitiNullAkNajdem) {
            return ret;
        } else {
            return null;
        }
    }

    public static boolean jeVrcholPravymSynomOca(Vrchol paSyn) {

        try {
            if (paSyn != null && paSyn.getaOtec() != null) {
                if (paSyn.getaOtec().getaLavySyn() != null && paSyn.getaOtec().getaLavySyn().equals(paSyn)) {
                    return false;
                } else if (paSyn.getaOtec().getaPravySyn() != null && paSyn.getaOtec().getaPravySyn().equals(paSyn)) {
                    return true;
                } else {
                    throw new IllegalStateException("Zistilo sa ze vrchol nie je ani pravym a ani lavym synom oca");
                }
            }
            if (paSyn == null) {
                throw new IllegalArgumentException("Parameter nesmie byt null");
            } else {
                throw new IllegalArgumentException("Otec zadaneho vrchola nesmie byt null");
            }
        } catch (RuntimeException ex) {
            if (paSyn.getaPredOdstranenimBolPravymSynom() != null) {
                if (paSyn.getaPredOdstranenimBolPravymSynom()) {
                    return true;
                } else {
                    return false;
                }
            }
            throw ex;
        }

    }

    /**
     *
     * @param paVrcholRotujuciOkoloOca
     * @return true- ak sa rotovalo okolo korena false - inak
     */
    public static boolean rotujVpravo(Vrchol paVrcholRotujuciOkoloOca) {
        Vrchol lpOtec = paVrcholRotujuciOkoloOca.getaOtec();
        Vrchol lpDedo = lpOtec.getaOtec();
        boolean ret = true;
        if (lpOtec != null) {
            if (lpDedo != null) {
                if (jeVrcholPravymSynomOca(lpOtec)) {
                    lpDedo.setaPravySyn(paVrcholRotujuciOkoloOca);
                } else {
                    lpDedo.setaLavySyn(paVrcholRotujuciOkoloOca);
                }
                ret = false;
            }

            lpOtec.setaLavySyn(paVrcholRotujuciOkoloOca.getaPravySyn());
            if (lpOtec.getaLavySyn() != null) {
                lpOtec.getaLavySyn().setaOtec(lpOtec);
            }

            paVrcholRotujuciOkoloOca.setaPravySyn(lpOtec);
            paVrcholRotujuciOkoloOca.setaOtec(lpDedo);
            lpOtec.setaOtec(paVrcholRotujuciOkoloOca);
        } else {
            throw new IllegalArgumentException("Vrchol, ktory rotuje, musi mat oca.");
        }
        return ret;
    }

    /**
     *
     * @param paVrcholRotujuciOkoloOca
     * @return true- ak sa rotovalo okolo korena false - inak
     */
    public static boolean rotujVlavo(Vrchol paVrcholRotujuciOkoloOca) {
        Vrchol lpOtec = paVrcholRotujuciOkoloOca.getaOtec();
        Vrchol lpDedo = lpOtec.getaOtec();
        boolean ret = true;
        if (lpOtec != null) {
            if (lpDedo != null) {//ak ma deda, potom nastav dedovy noveho syna
                if (jeVrcholPravymSynomOca(lpOtec)) {
                    lpDedo.setaPravySyn(paVrcholRotujuciOkoloOca);
                } else {
                    lpDedo.setaLavySyn(paVrcholRotujuciOkoloOca);
                }
                ret = false;
            }

            lpOtec.setaPravySyn(paVrcholRotujuciOkoloOca.getaLavySyn());//ocovi rotujuceho nastav praveho syna lavym synom rotujuceho
            if (lpOtec.getaPravySyn() != null) {
                lpOtec.getaPravySyn().setaOtec(lpOtec);//novemu pravemu synovi oca rotujuceho nastav oca ak syn nie je null
            }
            paVrcholRotujuciOkoloOca.setaLavySyn(lpOtec);//rotujuci vrchol bude mat ako laveho syna vrchol, ktory bol doteraz ocom
            paVrcholRotujuciOkoloOca.setaOtec(lpDedo);//rotujuci vrchol bude mat noveho oca a to toho, ktory bol doteraz jeho dedom
            lpOtec.setaOtec(paVrcholRotujuciOkoloOca);//otec rotujuceho bude mat za oca vrchol, ktory rotuje
        } else {
            throw new IllegalArgumentException("Vrchol, ktory rotuje, musi mat oca.");
        }
        return ret;
    }

    /**
     *
     * @param paVrcholNaOdstranenie
     * @return 0- prvok, ktory nahradi mazany pred nahradenim 1- prvok, ktory
     * nahradil mazany po nahradeni 2- prvok, ktory sa vymazal
     */
    public static Vrchol oddstranAkoVBVS(Vrchol paVrcholNaOdstranenie) {
        //System.out.println("\n-----------Mazanie ako v BVS-----------------");
        //String end="\n--------------------END Mazanie ako v bvs----------------------\n";
        if (paVrcholNaOdstranenie.getaLavySyn() == null && paVrcholNaOdstranenie.getaPravySyn() == null) {//mazany vrchol nema ziadneho syna
            //System.out.println("Mazany vrchol nema ziadneho syna");
            if (paVrcholNaOdstranenie.getaOtec() != null && BVSAlgoritmy.jeVrcholPravymSynomOca(paVrcholNaOdstranenie)) {
                paVrcholNaOdstranenie.getaOtec().setaPravySyn(null);
                paVrcholNaOdstranenie.setaPredOdstranenimBolPravymSynom(Boolean.TRUE);
            } else if (paVrcholNaOdstranenie.getaOtec() != null) {
                paVrcholNaOdstranenie.getaOtec().setaLavySyn(null);
                paVrcholNaOdstranenie.setaPredOdstranenimBolPravymSynom(Boolean.FALSE);
            }
            return paVrcholNaOdstranenie;

        } else if (paVrcholNaOdstranenie.getaLavySyn() != null && paVrcholNaOdstranenie.getaPravySyn() != null) {
            return odstranenieAkMaDvochSynov(paVrcholNaOdstranenie);
        } else if (paVrcholNaOdstranenie.getaLavySyn() != null) {//mazany vrchol ma iba laveho syna
            //System.out.println("Mazany vrchol ma len laveho syna"+end);
            Vrchol ret=paVrcholNaOdstranenie.getaLavySyn();
            Comparable klucNahradnika = paVrcholNaOdstranenie.getaLavySyn().getaKluc();
            Object dataNahradnika = paVrcholNaOdstranenie.getaLavySyn().getData();
            paVrcholNaOdstranenie.getaLavySyn().setData(paVrcholNaOdstranenie.getData());
            paVrcholNaOdstranenie.getaLavySyn().setaKluc(paVrcholNaOdstranenie.getaKluc());
            paVrcholNaOdstranenie.setaKluc(klucNahradnika);
            paVrcholNaOdstranenie.setData(dataNahradnika);
            
            paVrcholNaOdstranenie.setaPravySyn(paVrcholNaOdstranenie.getaLavySyn().getaPravySyn());
            paVrcholNaOdstranenie.setaLavySyn(paVrcholNaOdstranenie.getaLavySyn().getaLavySyn());
            if(paVrcholNaOdstranenie.getaPravySyn()!=null)
            paVrcholNaOdstranenie.getaPravySyn().setaOtec(paVrcholNaOdstranenie);
            if(paVrcholNaOdstranenie.getaLavySyn()!=null)
            paVrcholNaOdstranenie.getaLavySyn().setaOtec(paVrcholNaOdstranenie);
            ret.setaPredOdstranenimBolPravymSynom(Boolean.FALSE);
            return ret;
        } else if (paVrcholNaOdstranenie.getaPravySyn() != null) {//mazany vrchol ma iba praveho syna
            //System.out.println("Mazany vrchol ma len praveho syna"+end);
            Vrchol ret=paVrcholNaOdstranenie.getaPravySyn();
            Comparable klucNahradnika = paVrcholNaOdstranenie.getaPravySyn().getaKluc();
            Object dataNahradnika = paVrcholNaOdstranenie.getaPravySyn().getData();
            paVrcholNaOdstranenie.getaPravySyn().setData(paVrcholNaOdstranenie.getData());
            paVrcholNaOdstranenie.getaPravySyn().setaKluc(paVrcholNaOdstranenie.getaKluc());
            paVrcholNaOdstranenie.setaKluc(klucNahradnika);
            paVrcholNaOdstranenie.setData(dataNahradnika);

            paVrcholNaOdstranenie.setaLavySyn(paVrcholNaOdstranenie.getaPravySyn().getaLavySyn());
            paVrcholNaOdstranenie.setaPravySyn(paVrcholNaOdstranenie.getaPravySyn().getaPravySyn());
            if(paVrcholNaOdstranenie.getaPravySyn()!=null)
            paVrcholNaOdstranenie.getaPravySyn().setaOtec(paVrcholNaOdstranenie);
            if(paVrcholNaOdstranenie.getaLavySyn()!=null)
            paVrcholNaOdstranenie.getaLavySyn().setaOtec(paVrcholNaOdstranenie);
            ret.setaPredOdstranenimBolPravymSynom(Boolean.FALSE);
            return ret;
        }
        return null;
    }

    public static List<Vrchol> getInorder(Vrchol zaciatokPrehliadky) {
        Vrchol naPrDolava = zaciatokPrehliadky;
        List<Vrchol> naPrDoprava = new ArrayList<>();
        List<Vrchol> inOrder = new ArrayList<>();
        while (naPrDolava != null || naPrDoprava.size() > 0) {
            if (naPrDolava != null) {
                while (naPrDolava.getaLavySyn() != null) {
                    naPrDoprava.add(naPrDolava);
                    naPrDolava = naPrDolava.getaLavySyn();
                }
                naPrDoprava.add(naPrDolava);
                naPrDolava = null;
            } else {
                inOrder.add(naPrDoprava.get(naPrDoprava.size() - 1));
                if (naPrDoprava.get(naPrDoprava.size() - 1).getaPravySyn() != null) {
                    naPrDolava = naPrDoprava.get(naPrDoprava.size() - 1).getaPravySyn();
                }
                naPrDoprava.remove(naPrDoprava.size() - 1);
            }
        }
        return inOrder;
    }

    private static  Vrchol odstranenieAkMaDvochSynov(Vrchol paVrcholNaOdstranenie) {
        //String end="\n--------------------END Mazanie ako v bvs----------------------\n";
        //System.out.println("--------Odoberam vrchol, ktory ma dvoch synov.");
        List<Vrchol> inOrderPravehoPodstromuMazaneho = getInorder(paVrcholNaOdstranenie.getaPravySyn());
        Vrchol nahradzVrchol = inOrderPravehoPodstromuMazaneho.get(0);
        Comparable klucNahradnika = nahradzVrchol.getaKluc();
        Object dataNahradnika = nahradzVrchol.getData();
        nahradzVrchol.setData(paVrcholNaOdstranenie.getData());
        nahradzVrchol.setaKluc(paVrcholNaOdstranenie.getaKluc());
        paVrcholNaOdstranenie.setaKluc(klucNahradnika);
        paVrcholNaOdstranenie.setData(dataNahradnika);
        if(BVSAlgoritmy.jeVrcholPravymSynomOca(nahradzVrchol)){
            nahradzVrchol.setaPredOdstranenimBolPravymSynom(Boolean.TRUE);
        }else{
            nahradzVrchol.setaPredOdstranenimBolPravymSynom(Boolean.FALSE);
        }
        if (paVrcholNaOdstranenie.getaPravySyn().equals(nahradzVrchol)) {
            //System.out.println("Nastala situacia, ze nahradzovany je potomkom mazaneho"+end);
            paVrcholNaOdstranenie.setaPravySyn(nahradzVrchol.getaPravySyn());
            paVrcholNaOdstranenie.setaLavySyn(paVrcholNaOdstranenie.getaLavySyn());
            if(nahradzVrchol.getaPravySyn()!=null)
            nahradzVrchol.getaPravySyn().setaOtec(paVrcholNaOdstranenie);
            if(nahradzVrchol.getaLavySyn()!=null)
            nahradzVrchol.getaLavySyn().setaOtec(paVrcholNaOdstranenie);
        } else if (nahradzVrchol.getaPravySyn() != null) {
            //System.out.println("Nastala situacia, ze nahradzovany nie je potomkom mazaneho a ma syna"+end);
            nahradzVrchol.getaPravySyn().setaOtec(nahradzVrchol.getaOtec());
            nahradzVrchol.getaOtec().setaLavySyn(nahradzVrchol.getaPravySyn());
        } else {
            //System.out.println("Nastala situacia, ze nahradzovany nie je potomkom mazaneho a nema syna"+end);
            if(BVSAlgoritmy.jeVrcholPravymSynomOca(nahradzVrchol)){
                nahradzVrchol.getaOtec().setaPravySyn(null);
            }else{
                nahradzVrchol.getaOtec().setaLavySyn(null);
            }
        }
        return nahradzVrchol;
    }
}

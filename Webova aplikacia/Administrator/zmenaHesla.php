<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    /* function vypisPouzivatelov() {
      $sql = "SELECT id, email, heslo,pohlavie FROM pouzivatelia";
      $result = mysqli_query($this->conn, $sql);

      if (mysqli_num_rows($result) > 0) {
      // output data of each row
      while ($row = mysqli_fetch_assoc($result)) {
      echo "id: " . $row["id"] . " - E-Mail: " . $row["email"] . " " . $row["heslo"] . "Pohlavie>" . $row["pohlavie"] . "<br>";
      }
      } else {
      echo "0 results";
      }
      } */

    function zmenHeslo() {

        $this->biggestId+=1;

        $mail = htmlspecialchars($_POST["Mail"]);
        $heslo = htmlspecialchars($_POST["noveHeslo"]);

        $hash = password_hash($heslo, PASSWORD_BCRYPT);
        $sql1 = sprintf("SELECT * FROM pouzivatelia WHERE email='%s'",
                $this->conn->real_escape_string($mail));
        $result1 = $this->conn->query($sql1);

        if (@$result1->num_rows >0) {
            $sql = sprintf("UPDATE pouzivatelia SET heslo='%s' WHERE email='%s'",
                    $this->conn->real_escape_string($hash),
                    $this->conn->real_escape_string($mail));

            if ($this->conn->query($sql) === TRUE) {
                echo "Heslo uspesne zmenene.";
            } else {
                echo "Pri zmene hesla doslo k chybe: " . $this->conn->error;
            }
        } else {
            echo "Zle zadany mail";
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}

if(isset($_SESSION["login"])&&$_SESSION["login"]==0)
{
$db = new DBPraca();
$db->zmenHeslo();
$db->ukonciPristup();
}
else
{
    echo "Pravo menit heslo uzivatelov ma iba ADMINISTRATOR po prihlaseni.";
}

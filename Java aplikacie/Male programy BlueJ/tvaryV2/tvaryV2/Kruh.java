import java.awt.*;
import java.awt.geom.*;

/**
 * Kruh, s ktor�m mo�no pohybova� a nakresl� sa na pl�tno.
 * 
 * @author  Michael Kolling and David J. Barnes
 * @version 1.0  (15 July 2000)
 */

public class Kruh
{
    private int aPriemer;
    private int aXLavyHorny;
    private int aYLavyHorny;
    private String aFarba;
    private boolean aJeViditelny;
    
    /**
     * Vytvor nov� kruh preddefinovanej farby na preddefinovanej poz�cii. 
     */
    public Kruh()
    {
        aPriemer = 30;
        aXLavyHorny = 20;
        aYLavyHorny = 60;
        aFarba = "blue";
        aJeViditelny = false;
    }

    /**
     * (Kruh) Zobraz sa.
     */
    public void zobraz()
    {
        aJeViditelny = true;
        nakresli();
    }
    
    /**
     * (Kruh) Skry sa.
     */
    public void skry()
    {
        zmaz();
        aJeViditelny = false;
    }
    
    /**
     * (Kruh) Posu� sa vpravo o pevn� d�ku. 
     */
    public void posunVpravo()
    {
        posunVodorovne(20);
    }

    /**
     * (Kruh) Posu� sa v�avo o pevn� d�ku. 
     */
    public void posunVlavo()
    {
        posunVodorovne(-20);
    }

    /**
     * (Kruh) Posu� sa hore o pevn� d�ku. 
     */
    public void posunHore()
    {
        posunZvisle(-20);
    }

    /**
     * (Kruh) Posu� sa dole o pevn� d�ku. 
     */
    public void posunDole()
    {
        posunZvisle(20);
    }

    /**
     * (Kruh) Posu� sa vodorovne o d�ku dan� parametrom. 
     */
    public void posunVodorovne(int paVzdialenost)
    {
        zmaz();
        aXLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Kruh) Posu� sa zvisle o d�ku dan� parametrom. 
     */
    public void posunZvisle(int paVzdialenost)
    {
        zmaz();
        aYLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Kruh) Posu� sa pomaly vodorovne o d�ku dan� parametrom. 
     */
    public void pomalyPosunVodorovne(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aXLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Kruh) Posu� sa pomaly zvisle o d�ku dan� parametrom. 
     */
    public void pomalyPosunZvisle(int paVzdialenost)
    {
        int delta;

        if(paVzdialenost < 0) 
        {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < paVzdialenost; i++)
        {
            aYLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Kruh) Zme� priemer na hodnotu dan� parametrom.
     * Priemer mus� by� nez�porn� cel� ��slo. 
     */
    public void zmenPriemer(int paPriemer)
    {
        zmaz();
        aPriemer = paPriemer;
        nakresli();
    }

    /**
     * (Kruh) Zme� farbu na hodnotu dan� parametrom.
     * Nazov farby mus� by� po anglicky.
     * Mo�n� farby s� tieto:
     * �erven� - "red"
     * �lt�    - "yellow"
     * modr�   - "blue"
     * zelen�  - "green"
     * fialov� - "magenta"
     * �ierna  - "black"
     * biela - "white"
     * hned� - "brown"
     */
    public void zmenFarbu(String paFarba)
    {
        aFarba = paFarba;
        nakresli();
    }

    /*
     * Draw the circle with current specifications on screen.
     */
    private void nakresli()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.draw(this, aFarba, new Ellipse2D.Double(aXLavyHorny, aYLavyHorny, 
                                                          aPriemer, aPriemer));
            canvas.wait(10);
        }
    }

    /*
     * Erase the circle on screen.
     */
    private void zmaz()
    {
        if(aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

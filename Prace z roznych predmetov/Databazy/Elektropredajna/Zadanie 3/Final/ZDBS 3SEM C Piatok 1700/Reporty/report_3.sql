set pagesize 60;
set linesize 123;

ttitle  left datum center 'Tovar vypisany po kategoriach zo zlavou 5% ' right;
column datum new_value datum noprint

SELECT to_char (sysdate, 'DD.MM.RRRR') datum,nazov_kat Kategoria, nazov, VyfakturovanyTovar.mnozstvo as Predane, cena as AktualnaCena, (cena - (cena*0.01)*5) as NovaCena--3. select zadanie
FROM Kategoria join Tovar on (Kategoria.id_kategorie = Tovar.id_kategorie) join VyfakturovanyTovar on
(Tovar.katalogoveCislo = VyfakturovanyTovar.katalogoveCislo) ORDER BY nazov_kat;

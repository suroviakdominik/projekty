/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class PredmetBaterka implements IPredmet, Serializable {

    private String aNazov;
    private String aPopis;

    public PredmetBaterka(String aNazov, String aPopis) {
        this.aNazov = aNazov;
        this.aPopis = aPopis;
    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return aPopis;
    }

    @Override
    public String pouzi(Hrac paHrac) {
        if (paHrac.maPredmet("notebook")) {
            PredmetNotebook notebook = (PredmetNotebook) paHrac.dajPredmet("notebook");

            notebook.vlozBaterku();
            paHrac.odstranZInventara(aNazov);
            return "Vlozili ste baterku do notebooku.";

        }

        return "Nemate notebook.";
    }

}

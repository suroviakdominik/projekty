
import java.util.Scanner;
import java.util.Random;
public class Matica
{
  private PoliaMnozinAHodnotRelacie aPole;   
  private int aPocetRiadkov;
  private int aPocetStlpcov;
  private Scanner scan;
  private Random generuj;
 
  public Matica(int paPocetRiadkov,int paPocetStlpcov)
   {
       aPocetRiadkov=paPocetRiadkov;
       aPocetStlpcov=paPocetStlpcov;
       aPole= new PoliaMnozinAHodnotRelacie(aPocetRiadkov,aPocetStlpcov);
       scan=new Scanner(System.in);
   }
   
    public void nacitajMaticu()
    {
       System.out.println("Zadajte prvky matice:");
       for (int i=0;i<aPocetRiadkov;i++)
       {
           for (int j=0;j<aPocetStlpcov;j++)
           {
              aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,scan.nextInt());
           }
       }
    }
    
    public void generujNahodnuMaticu(int paOdKolkoGenerovat,int paPoKolkoGenerovat)
    {
       generuj=new Random(); 
       for (int i=0;i<aPocetRiadkov;i++)
       {
           for (int j=0;j<aPocetStlpcov;j++)
           {
              aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,generuj.nextInt(paPoKolkoGenerovat-paOdKolkoGenerovat+1)+paOdKolkoGenerovat);
           }
       }
    }
    
    public void vypisMaticu()
    {
        aPole.vypisMatice();
    }
    
   public int dajPocetRiadkov()
    {
        return aPocetRiadkov;
    }
    
    public int dajPocetStlpcov()
    {
        return aPocetStlpcov;
    } 
    
    public int dajPrvokPola(int i,int j)
    {
        return aPole.zistiHodnotuDoNRozmernehoPolaRelacie(i,j);
    }
    
    public void vlozPrvokDoMatice(int i,int j,int hodnota)
    {
       aPole.vlozHodnotuDoNRozmernehoPolaRelacie(i,j,hodnota);
    }
}    

               
     


import java.util.Scanner;
/**
 * Hlavno úohou tejto triedy je vytvárať objekty, ktoré nazývame polia. 
 * S touto triedou spolupracujú triedy: 
 * relácie a matice, ktoré tieto objekty vytvárajú.
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class PoliaMnozinAHodnotRelacie
{
 private Scanner scan;
 private int [] aPoleMnozinyA;
 private int [] aPoleMnozinyB;
 private int [][] aNRozmernePoleHodnotRelacie;
 private int aPocetRiadkov;
 private int aPocetStlpcov;
  
  
 /**
     * Konštruktor vytvóri dvorozmerné pole o veľkosti zadaných parametrov.
     * @ param paPocetRiadkov počet riadkov dvojrozmerného poľa
     * @ param paPocetStlpcov počet stĺpcov dvojrozmerného poľa
     */
 public PoliaMnozinAHodnotRelacie(int paPocetRiadkov,
                                   int paPocetStlpcov)
  {
      aPocetRiadkov=paPocetRiadkov;
      aPocetStlpcov=paPocetStlpcov;
      aNRozmernePoleHodnotRelacie=new int[aPocetRiadkov][aPocetStlpcov];
      scan=new Scanner(System.in);
  }

       /**
       * Načíta do množiny A vami zadané prvky.
       */ 
    
       public void nacitajMnozinuA()
       {
           aPoleMnozinyA=new int[aPocetRiadkov];
           System.out.println("Zadajte vedla seba prvky mnoziny A  a potvrdte ich stlacenim klavesy ENTER: ");
           for (int i=0;i<aPocetRiadkov;i++)
           {
              aPoleMnozinyA[i]=scan.nextInt(); 
           }
       }
    
       /**
       * Načíta do množiny B vami zadané prvky.
       */ 
       public void nacitajMnozinuB()
       {
         aPoleMnozinyB=new int[aPocetStlpcov];
         System.out.println("Zadajte vedla seba prvky mnoziny B  a potvrdte ich stlacenim klavesy ENTER: ");
         for (int i=0;i<aPocetStlpcov;i++)
         {
            aPoleMnozinyB[i]=scan.nextInt(); 
         }
       }
    
       /**
       * Vypíše prvok množiny A nachádzajúci sa na i-tom mieste
       * @param i index prvku v poli, ktorý chceme nájsť
       */ 
       public void vypisPrvkuMnozinyA(int i)
       {
         System.out.print(aPoleMnozinyA[i]+"\t");
       }
    
       /**
      * vypíše prvky množiny B
      */
       public void vypisMnozinyB()
      { 
        for (int i=0;i<aPocetStlpcov;i++)
        {
          System.out.print(aPoleMnozinyB[i]+"\t");
        }
      }
      
      /**
      * vráti hodnotu prvku poľa nachadzajúceho sa na indexe
      * @param  i   index prvku poľa
      */
      
      public int dajPrvokPolaA(int i)
      {
         return aPoleMnozinyA[i];
      } 
   
      public int dajPrvokPolaB(int i)
      {
         return aPoleMnozinyB[i];
      } 
    
      /**
      * vypíše poľe hodnôt relácie, pričom vypíše aj v prvom riadku a stĺpci prvky množín A a B medzi ktorými bola uskutočnená relácia
      */
      public void vypisPoleHodnotRelacie()
      {
          System.out.print("A/B\t");
          vypisMnozinyB();
          System.out.println();
          
          for (int i=0;i<aPocetRiadkov;i++)
          {
              vypisPrvkuMnozinyA(i);
              for (int j=0;j<aPocetStlpcov;j++)
              {
                  System.out.print(aNRozmernePoleHodnotRelacie[i][j]+"\t");
              }
              System.out.println();
          }
      } 
      
      
      
      /**
      * pri počte s maticami vypíše danú maticu nachádzajúcu sa v dvojrozmernom poli
      */ 
     public void vypisMatice()
      {
         for (int i=0;i<aPocetRiadkov;i++)
         {
             for (int j=0;j<aPocetStlpcov;j++)
             {
                 System.out.print(aNRozmernePoleHodnotRelacie[i][j]+"\t");
             }
           System.out.println();  
        }
      }
              
    /**
     * zistí hodnotu prvku poľa nachádzajúceho sa na indexe i,j
     * 
     * @param  i   riadok 
     * @param  j   stĺpec
     * @return     prvok v i-tom riadku a j-tom stĺpci
     */
      public int zistiHodnotuDoNRozmernehoPolaRelacie(int i,int j)
    {
        return aNRozmernePoleHodnotRelacie[i][j];
    }        
    
    /**
     * vloží zadanú hodnotu do dvojrozmerného poľa na index i,j
     * 
     * @param  i   riadok 
     * @param  j   stĺpec
     * @param paHodnota hodnota ktorá sa vkladá
     */
    public void vlozHodnotuDoNRozmernehoPolaRelacie(int i,int j,int paHodnota)
    {
        aNRozmernePoleHodnotRelacie[i][j]=paHodnota;
    }
    
    /**
     * sprístupní dvojrozmerné pole pre iné triedy
    */
    public int[][] dajNRozmernePole()
    {
          return aNRozmernePoleHodnotRelacie;
    }    
}
    
  
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nahradzovacznakovsuboru;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Dominik
 */
public class NahradzovacZnakovSuboru {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            /*String cestaSubora=ConsoleWorker.nacitajDataZKonzoly("cestu suboru na spracovanie",
             "(^[A-Z]:(/\\w+(\\.\\w{1,3}){0,1})*)|(./\\w+(\\.\\w{1,3}))");
             System.out.println(cestaSubora);*/
            String a = "";
            String cestaSubora = "./s.txt";
            if (cestaSubora != null) {
                a = FileWorker.readDataFromFile(new File(cestaSubora), "\n");
                System.out.println(a);
            }
            String znakyNaNahradenie = ConsoleWorker.nacitajDataZKonzoly("znaky, ktore chcete nahradit", "\\w+");
            String znakyNahradzovacie = ConsoleWorker.nacitajDataZKonzoly("znaky, ktore nahradia predchadzajuce", "\\w+");
            if (znakyNaNahradenie.trim().length() == znakyNahradzovacie.trim().length()) {
                System.out.println("Znaky: " + znakyNaNahradenie + " budu nahradene znakmi: " + znakyNahradzovacie);
                StringBuilder strBuild = new StringBuilder();
                Pattern p = null;
                Matcher m = null;
                String pom = a;
                for (int i = 0; i < znakyNaNahradenie.trim().length(); i++) {
                    p = Pattern.compile("" + znakyNaNahradenie.trim().charAt(i));
                    m = p.matcher(pom);
                    pom = m.replaceAll("" + znakyNahradzovacie.trim().charAt(i));
                    System.out.println("" + znakyNaNahradenie.trim().charAt(i));
                }
                System.out.println(pom);
                FileWorker.writeDataToFile(new File(cestaSubora), pom, false);
            }
            else
            {
                System.out.println("Pocet Nahradzovane znaky != Pocet nahradzovacie znaky");
            }

        } catch (IOException ex) {
            System.out.println("Subor nenacitany.");
        }
    }

}

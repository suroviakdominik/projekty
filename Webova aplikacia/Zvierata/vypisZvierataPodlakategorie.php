<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId=0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
                $sql = "SELECT id FROM kategoria";
                $result = mysqli_query($this->conn, $sql);
                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row["idKoment"]>$this->biggestId)
                        {
                            $this->biggestId=$row["idKoment"];
                        }
                    }
                    
                } 
            }

    
   function vypisZvierata()
   {
                $idKat=htmlspecialchars($_POST["katID"]);
                $sql = sprintf("SELECT id,nazov FROM zviera WHERE idKategoria=%d",
                $this->conn->real_escape_string($idKat));
                $result = mysqli_query($this->conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        $nazZv=$row["nazov"];
                        $idZv=$row["id"];
                        echo "<option value=\"$idZv\" onchange='kategoriaZvolena(this.value)' >"."$nazZv"."</option>";
                        
                       
                    }
                } else {
                    echo "0 results";
                }
         
   
   }

    function ukonciPristup() {
        $this->conn->close();
    }

}
$db = new DBPraca;
$db->vypisZvierata();
$db->ukonciPristup();
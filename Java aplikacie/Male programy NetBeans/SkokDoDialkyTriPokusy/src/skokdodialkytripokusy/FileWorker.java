/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skokdodialkytripokusy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class FileWorker {
    public static void writeDataToFile(File f,String paData,boolean paAppend)
    {
        Writer w=null;
        try {
            w = new OutputStreamWriter(new FileOutputStream(f,paAppend),"Windows-1250");
            BufferedWriter bW= new BufferedWriter(w);
            
            BufferedReader bR=new BufferedReader(new InputStreamReader(new ByteArrayInputStream(paData.getBytes())));
            String lineToWrite=null;
            while((lineToWrite=bR.readLine())!=null)
            {
                bW.write(lineToWrite);
                bW.newLine();
            }
            bW.flush();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

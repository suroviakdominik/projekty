/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.hra;

import šach.KontrolaTahu.KontrolorTahov;
import šach.sachovnica.figurky.Figurka;

/**
 * Trieda Hrac uchováva základné informácie o hráčovi ako jeho meno, farbu
 * figúrok, šachovnica na ktorej hrá... a poskytuje ich iným objektom.Niektoré
 * metódy predstavujú určitú podporu pre hráča.
 *
 * @author Dominik
 */
public class Hrac {

    private String aMeno;
    private String aFarbaMojichFigurok;
    private Hrac aProtiHrac;
    private Figurka[][] aSachovnicaNaKtorejSaHra;
    private Figurka aVybranaFigurka;
    private boolean aJeNaTahu;
    private KontrolorTahov aKontrolorTahu;

    /**
     * Pri vytváraní tohto objektu je potrebné uviesť meno, farbu figúrok a mapu
     * figúrok/šachovnicu na ktorej sa hrá. Tieto parametre sa priradia
     * jednotlivým atribútom a predstavujú určitý stav objektu. Atribút
     * aVybranaFigurka sa nastaví na null, keďže zatiaľ nie je vybraná žiadna
     * figúrka. V priebehu hry sú mu však priradzované rôzne figúrky.
     *
     * @param paMeno - meno hráča
     * @param paFarabMojichFigurok - farba figúrok hráča
     * @param paMapaFigurok - mapa figúrok/šachovnica/umiestnenie rôznych
     * figúrok.
     */
    public Hrac(String paMeno, String paFarabMojichFigurok, Figurka[][] paMapaFigurok) {
        aMeno = paMeno;
        aFarbaMojichFigurok = paFarabMojichFigurok;
        aVybranaFigurka = null;
        aSachovnicaNaKtorejSaHra = paMapaFigurok;
    }

    /**
     * kontroluje či je objekt tejto triedy naťahu.Vracia hodnotu atribútu
     * aJeNaTahu.
     *
     * @return true - ak nie je na ťahu, false - ak nie je na ťahu
     */
    public boolean isaJeNaTahu() {
        return aJeNaTahu;
    }

    /**
     * volaním zmení hodnotu atribútu aJeNaTahu na hodnotu, ktorá je prijatá
     * metódou ako parameter.
     *
     * @param paJeNaTahuHracA - hodnota, ktorá bude priradená atribútu
     */
    public void setaJeNaTahu(boolean paJeNaTahuHracA) {
        this.aJeNaTahu = paJeNaTahuHracA;
    }

    /**
     * vráti figúrku ktorú si vybral hráč. Vráti hodnotu atribútu
     * aVybranaFigurka.
     *
     * @return - vybraná figurka
     */
    public Figurka getaVybranaFigurka() {
        return aVybranaFigurka;
    }

    /**
     * priradí atribútu aVybranaFigurka Figurku vybranú hráčom.
     *
     * @param paVybranaFigurka
     */
    public void setaVybranaFigurka(Figurka paVybranaFigurka) {
        this.aVybranaFigurka = paVybranaFigurka;
    }

    /**
     * vráti hodnotu atribútu aSachovnicaNaKtorejSaHra, kde sú uložené
     * jednotlivé pozície figúrok.
     *
     * @return aSachovnicaNaKtorejSaHra=pozicie jednotlivých figúrok=Figurka[][]
     */
    public Figurka[][] getaSachovnicaNaKtorejSaHra() {
        return aSachovnicaNaKtorejSaHra;
    }

    /**
     * vráti protihráča, ktorý je tiež typu Hráč a je uložený v atribúte
     * aProtiHrac
     *
     * @return protihráč typu Hrac
     */
    public Hrac getaProtiHrac() {
        return aProtiHrac;
    }

    /**
     * priradí atribútu aProtihrac, hodnotu uvedenú ako parameter.
     *
     * @param paProtiHrac - protihráč hráča
     */
    public void setaProtiHrac(Hrac paProtiHrac) {
        this.aProtiHrac = paProtiHrac;
    }

    /**
     * – vráti meno hráča, ktoré je uložené v atribúte aMeno.
     *
     * @return aMeno
     */
    public String getMeno() {
        return aMeno;
    }

    /**
     * vráti farbu figúrok hráča uloženú v atribúte aFarbaFigurok.
     *
     * @return aFarbaFigurok
     */
    public String getFarbaFigurok() {
        return aFarbaMojichFigurok;
    }

    /**
     * vytvorí objekt typu KontrolorTahov s danými parametrami a nasledovne ho
     * pomocou správy požiada o skontrolovanie ťahu.
     *
     * @param paNazovVybranejFigurky -názov figurky, ktorú má hráč vybranú
     * @param paNoveXUMiestnenie- nová x-sová pozícia figúrky
     * @param paNoveYUmiestnenie- nová y-lonová pozícia figúrky.
     * @return -ak je ťah platný vráti true, inak vráti false.
     */
    public boolean skontrolujTah(String paNazovVybranejFigurky, int paNoveXUMiestnenie, int paNoveYUmiestnenie) {
        aKontrolorTahu = new KontrolorTahov(this, paNoveXUMiestnenie, paNoveYUmiestnenie);
        return aKontrolorTahu.skontrolujTah(paNazovVybranejFigurky, paNoveXUMiestnenie, paNoveYUmiestnenie);
    }
}

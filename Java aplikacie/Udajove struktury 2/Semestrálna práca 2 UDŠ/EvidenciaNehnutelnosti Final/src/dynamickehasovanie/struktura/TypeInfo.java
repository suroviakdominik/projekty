/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.struktura;

/**
 *
 * @author Dominik
 */
public class TypeInfo {

    public static int sizeof(Class datovytyp) {
        if (datovytyp == null) {
            throw new NullPointerException();
        }

        if (datovytyp == int.class || datovytyp == Integer.class) {
            return Integer.BYTES;
        }
        if (datovytyp == short.class || datovytyp == Short.class) {
            return Short.BYTES;
        }
        if (datovytyp == byte.class || datovytyp == Byte.class) {
            return Byte.BYTES;
        }
        if (datovytyp == char.class || datovytyp == Character.class) {
            return Character.BYTES;
        }
        if (datovytyp == long.class || datovytyp == Long.class) {
            return Long.BYTES;
        }
        if (datovytyp == float.class || datovytyp == Float.class) {
            return Float.BYTES;
        }
        if (datovytyp == double.class || datovytyp == Double.class) {
            return Double.BYTES;
        }
        throw new IllegalArgumentException("Parametrom mozu byt iba obalovacie triedy primitivnych typov s vynimkou BOOLEAN");
    }
}

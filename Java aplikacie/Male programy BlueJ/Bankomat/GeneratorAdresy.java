
import java.util.Random;
public class GeneratorAdresy
{
  private NameGenerator aGenerujMesto;
  private NameGenerator aGenerujUlicu;
  private Random aGenNumber;
  
  public GeneratorAdresy()
  {
      aGenerujMesto=new NameGenerator(2,12);
      aGenerujUlicu= new NameGenerator(5,30);
      aGenNumber=new Random();
  }
  
  public Adresa generuj()
  {
      return new Adresa(aGenerujMesto.generuj(),
                        aGenerujUlicu.generuj(),
                        aGenNumber.nextInt(2000)+1,
                        aGenNumber.nextInt(99999)+1);
                        

  }
}
                        

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Trida obsluhuje tlacitka ze tridy Gui 
 * a implementuje rozhrani ActionListener
 * 
 * @author Jirka Krhanek
 * @version 2006-05-10
 */
class ObsluhaTlacitka implements ActionListener
{
    // deklarace instancnich promennych
    private Gui gui;
    // retezec
    private String objednavka = "  Objedn�no v K�: ";

    
/**
 * Konstruktor pro ObsluhaTlacitka
 * 
 * @param gui inicializuje instancni promenou gui
 */    
    public ObsluhaTlacitka(Gui gui)
    {
        // prirazeni parametru gui instancni promenne gui teto tridy
        this.gui = gui;       
    }

/**
 * Metoda vraci retezec objednavka
 * 
 * @return vraci objednavku
 */
    public String vratObjednavku()
    {
        return objednavka;
    }

/**
 * Metoda nastavuje objednavku
 * 
 * @param text text pro nastaveni objednavky
 */
    public void nastavObjednavku(String text)
    {
        objednavka = text;
    }

/**
 * Metoda provede akci, kdyz se vyskytne udalost
 * 
 * @param e udalost
 */
    public void actionPerformed(ActionEvent e)
    {
        // ziskame textovy popisek ze stisknuteho tlacitka a priradime do promenne
        String popisekTlacitka = e.getActionCommand();
        
        // pomoci podminek zjistime jake talcitko bylo stisknuto
        // porovname textovy popisek s retezcem "1 koruna"
        if(popisekTlacitka.equals("  1 koruna  "))
        {
            // po splneni podminky se volaji metody objektu gui
            gui.sectiMince(1);
            gui.nastavMinceCelkem();           
        }
        else if(popisekTlacitka.equals("2 koruny"))
        {
            gui.sectiMince(2);
            gui.nastavMinceCelkem();
        }
        else if(popisekTlacitka.equals("5 korun"))
        {
            gui.sectiMince(5);
            gui.nastavMinceCelkem();
        }
        else if(popisekTlacitka.equals("10 korun"))
        {
            gui.sectiMince(10);
            gui.nastavMinceCelkem();
        }
        else if(popisekTlacitka.equals("20 korun"))
        {
            gui.sectiMince(20);
            gui.nastavMinceCelkem();
        }
        
        // pokud je stisknuto Vytisknout
        if(popisekTlacitka.equals("Vytisknout"))
        {
            // jestlize je celkova cena jizdenek mensi nebo rovna hodnote vhozenych minci
            if(gui.vratCelkovouCenuJizdenek() <= gui.vratMince())
            {
                // zavolame statickou metodu ze tridy Automat, kde parametrem je nova mince o hodnote vhozenych vsech minci
                Automat.vhoditMinci(new Mince(gui.vratMince()));
                // metoda stornovat nad objektem gui
                gui.stornovat();
                // nastaveni textu pro statickoku textovou oblast ve tride Gui
                gui.area.setText(Automat.vratInformace());
                // znepristupneni tlacitka Storno
                gui.storno.setEnabled(false);
                // misto tlacitka Objednat bude tlacitko Znovu
                gui.objednat.setLabel("Znovu");
            }
            // jinak
            else 
            {
                // od celkove ceny jizdenek odecteme hodnotu vhozenych minci a priradime do promenne rozdil
                int rozdil = gui.vratCelkovouCenuJizdenek() - gui.vratMince();
                // vypisme rozdil na textove oblasti
                gui.zobrazitVypis("  Mus�te doplatit: " + rozdil + " K�");
            }
            
        }
        
        // pokud je stisknuto Storno nebo Znovu
        if(popisekTlacitka.equals("Storno") || popisekTlacitka.equals("Znovu"))
        {
            // nastavime retezec Objednavka
            nastavObjednavku("  Objedn�no v K�: ");
            // stornujeme, tedy vynulujeme vse a zacneme od zacatku
            gui.stornovat();
        }
        
        // pokud je stisknuto Objednat
        if(popisekTlacitka.equals("Objednat"))
        {
            // jestlize seznam jizdenek je prazdny
            if(Automat.jizdenky.isEmpty())
            {
                // nastavime text objednavky na vychozi
                nastavObjednavku("  Objedn�no v K�: ");
            }
            // priobedname jizdenky pomoci staticke metody ze tridy Automat
            Automat.priobjednatJizdenky(gui.vratCenuJizdenky(),1);
            // nastavime text objednavky
            nastavObjednavku(vratObjednavku() + gui.vratCenuJizdenky() + ",");
            // pricteme cenu k celkove cene
            gui.pricistCenu();
            // zobrazime vypis do textove oblasti
            gui.zobrazitVypis(vratObjednavku() + "\n  Celkem v K�: " + gui.vratCelkovouCenuJizdenek());            
            // ucinime tlacitko Vytisknout mackatelnym
            gui.vytisknout.setEnabled(true);
        }
    }
}

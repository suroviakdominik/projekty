/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Osoby;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Predmety.PredmetIndex;
import wof00.Predmety.PredmetKasa;

/**
 *
 * @author suroviak3
 */
public class OsobaUcitel1 implements IOsoba {

    private String aMeno;
    private ArrayList<String> aZoznamOspovedi;
    private ArrayList<String[]> aSkuskoveOtazky;
    private Random aGeneratorOtazok;

    public OsobaUcitel1(String paMeno) {
        this.aMeno = paMeno;
        aGeneratorOtazok = new Random();
        aZoznamOspovedi = new ArrayList<String>();
        aSkuskoveOtazky = new ArrayList<String[]>();
        aSkuskoveOtazky.add(naplnOtazky("Hlavna otazka", "42"));
        aSkuskoveOtazky.add(naplnOtazky("Programovaci jazyk", "java"));
        aSkuskoveOtazky.add(naplnOtazky("Programovacie prostredie", "netbeans"));

    }

    @Override
    public String getaMeno() {
        return aMeno;
    }

    @Override
    public void oslov() {
        System.out.println("Dobry den, co potrebujete ?");
        System.out.println("A - skuska");
        System.out.println("B - informacie o predmete");
        System.out.println("U - ukoncirozhovor");

        aZoznamOspovedi.add("A");
        aZoznamOspovedi.add("B");

        aZoznamOspovedi.add("U");
    }

    @Override
    public void rozhovor(Hrac paHrac, String paOdpoved) {
        if (paOdpoved.equals("U")) {
            paHrac.setaRozhovor(null);
            System.out.println("Rozhovor ukonceny");
        } else if (paOdpoved.equals("B")) {
            System.out.println("Predmet Informatika");
        } else if (paOdpoved.equals("A")) {
            String[] skusOtazka = aSkuskoveOtazky.get(aGeneratorOtazok.nextInt(3));
            System.out.println(skusOtazka[0]);

            Scanner scan = new Scanner(System.in);
            String odpoved = scan.nextLine();

            if (odpoved.equals(skusOtazka[1])) {
                System.out.println("Skusku ste spravili. Dajte index.");
                PredmetIndex vyplneny = (PredmetIndex) paHrac.dajPredmet("bakalrskyIndex");

                if (vyplneny != null) {
                    vyplneny.zapisZnamky("A");
                    System.out.println("Znamu mate zapisanu.");
                } else {
                    System.out.println("Nemate index.");
                }
            }
        }

    }

    @Override
    public boolean jeOdpoved(String paOdpoved) {
        for (String prechadzac : aZoznamOspovedi) {
            if (prechadzac.equals(paOdpoved)) {
                return true;
            }

        }
        return false;
    }

    private String[] naplnOtazky(String paOtazka, String paOdpoved) {
        String[] otazka = new String[2];
        otazka[0] = paOtazka;
        otazka[1] = paOdpoved;
        return otazka;
    }
}

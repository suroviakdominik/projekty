/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00;

import wof00.VstupVystup.Prikaz;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author suroviak3
 */
public class PrikazTest {

    private Prikaz prikaz;

    @Before
    public void setUp() {
        // prikaz=new Prikaz("chod", "vychod");
    }

    @Test
    public void dajNazov() {
        assertEquals("chod", prikaz.dajNazov());
    }

    @Test
    public void dajParameter() {
        assertEquals("vychod", prikaz.dajParameter());
    }

    @Test
    public void jeNeznamy() {
        assertFalse(prikaz.jeNeznamy());
    }

    @Test
    public void maParameter() {
        assertTrue(prikaz.maParameter());
    }
}

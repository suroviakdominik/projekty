
import java.util.Scanner;
public class Main
{
  public static void main(String [] args)
  {
      
     Scanner scan=new Scanner(System.in);
     int cislo=0;
     
     do
     {
        PrvociselnyRozklad rozklad=new PrvociselnyRozklad();
        System.out.println("Zadajte prirodzene cislo, ktoreho prvociselny rozklad chcete zistit alebo zadajte nulu pre ukoncenie programu: ");
        cislo=scan.nextInt();
        if (cislo!=0)
        {
           System.out.print("Prvociselny rozklad cisla: "+cislo+" je: ");
           rozklad.prvociselnyRozklad(cislo);
           System.out.println();
           System.out.println("Ak chcete program ukoncit, zadajte nulu.");
        }
        
        else
        {    
              System.out.println("Program ukonceny.");
        }
     }  
     while (cislo!=0);
      
 }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nfaktorial;

import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.converter.BigIntegerStringConverter;

/**
 *
 * @author Dominik
 */
public class NFaktorial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            NFaktorialRekurzivne rek = new NFaktorialRekurzivne(20);
            NFaktorialNerekurzivne ner = new NFaktorialNerekurzivne(BigInteger.valueOf(100000));
            rek.start();
            ner.start();
            rek.join();
            ner.join();
            System.out.println("Rekurzivne trvalo: " + (double) rek.getaCas() / 1000000 + " ms");
            System.out.println("Nerekurzivne trvalo: " + (double) ner.getaCas() / 1000000 + " ms");
            System.out.println("Rekurzivne vysledok: " + rek.getaCislo());
            System.out.println("Nerekurzivne vysledok: " + ner.getaCislo());
        } catch (InterruptedException ex) {
            Logger.getLogger(NFaktorial.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private static class NFaktorialRekurzivne extends Thread {

        private long aCislo;
        private long aCas;

        public NFaktorialRekurzivne(int aCislo) {
            this.aCislo = aCislo;
        }

        public void run() {
            aCas = System.nanoTime();
            aCislo = vypocitajNFaktorialRekurzivne(aCislo);
            aCas = System.nanoTime() - aCas;
        }

        private long vypocitajNFaktorialRekurzivne(long paCislo) {
            if (paCislo == 1) {
                return 1;
            }
            long value = paCislo * vypocitajNFaktorialRekurzivne(paCislo - 1);
            if (paCislo!=aCislo&&((double) Long.MAX_VALUE / value)<paCislo+1) {
                System.err.println("Pretecenie rekurzivna metoda: ");
            }
            return value;
        }

        public long getaCas() {
            return aCas;
        }

        public long getaCislo() {
            return aCislo;
        }

    }

    private static class NFaktorialNerekurzivne extends Thread {

        private BigInteger aCislo;
        private long aCas;

        public NFaktorialNerekurzivne(BigInteger aCislo) {
            this.aCislo = aCislo;
        }

        public void run() {
            aCas = System.nanoTime();
            aCislo=vypociajNFaktorialNerekurzivne(aCislo);
            aCas = System.nanoTime() - aCas;
            
        }

        private BigInteger vypociajNFaktorialNerekurzivne(BigInteger paCislo) {
            BigInteger ret = BigInteger.valueOf(1);
            System.out.println(paCislo);
            for (int i = 1; i <= paCislo.intValueExact(); i++) {
                ret=ret.multiply(BigInteger.valueOf(i));
            }
            
            return ret;
        }

        public BigInteger getaCislo() {
           return aCislo;
        }

        public long getaCas() {
            return aCas;
        }

    }

}

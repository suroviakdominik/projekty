/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacOdpoved implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paPrikaz) {
        if ((paPrikaz == null) && (paHrac.getaRozhovor() != null)) {
            System.out.println("Zadajte odpoved.");
            return false;
        } else if (paHrac.getaRozhovor() != null) {
            if (paHrac.getaRozhovor().jeOdpoved(paPrikaz)) {
                paHrac.getaRozhovor().rozhovor(paHrac, paPrikaz);
            } else {
                System.out.println("Zla odpoved.");
            }
            return false;

        }
        System.out.println("Osoba nebola oslovena.");
        return false;
    }

}

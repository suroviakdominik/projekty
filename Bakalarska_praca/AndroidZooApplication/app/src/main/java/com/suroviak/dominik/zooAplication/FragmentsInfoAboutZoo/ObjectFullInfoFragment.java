package com.suroviak.dominik.zooAplication.FragmentsInfoAboutZoo;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.suroviak.dominik.zooAplication.FlowTextAroundPicture.FlowTextHelper;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_Dirs_File_ENUM;
import com.suroviak.dominik.zooAplication.R;
import com.suroviak.dominik.zooAplication.WorkWithFiles.FileWorker;
import com.suroviak.dominik.zooAplication.WorkWithFiles.SerializationWorker;

import java.io.File;
import java.io.IOException;

/**
 * Created by Dominik on 4.4.2015.
 */
public class ObjectFullInfoFragment extends Fragment {
    private Info_About_Dirs_File aInfoFileObjects;
    private Drawable d;

    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_object_full_info, container, false);
        String text = "";


        // Get the icon and its width
        Drawable DICON = getResources().getDrawable(R.drawable.ic_launcher);
        int leftMargin = DICON.getIntrinsicWidth() + 10;

        //Set the icon in R.id.icon
        /*ImageView icon =(ImageView)V.findViewById(R.id.icon) ;
        icon.setBackgroundDrawable (DICON) ;*/
        String keyselAnimal = getActivity().getIntent().getStringExtra("KEY_OF_SELECTED_OBJECT");
        SerializationWorker sw = new SerializationWorker<Info_About_Dirs_File>(Info_Dirs_File_ENUM.ENDOFINFOFILE);
        sw.setPath(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER + Info_Dirs_File_ENUM.OBJECTS.getaRelativePathOfDirecotryWhereIAM());
        sw.setNameOfFile(Info_Dirs_File_ENUM.OBJECTS.getaNameOfInfoFile());
        try {
            aInfoFileObjects = (Info_About_Dirs_File) sw.readObjetctFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        ImageView icon = (ImageView) V.findViewById(R.id.icon);

        String fullNameOfinfoFile = "";
        for (int i = 0; i < aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getDomainsOfFilesInCurrentItem().size(); i++) {
            String fullDomain = aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getDomainsOfFilesInCurrentItem().get(i);
            if (fullDomain.contains("picture")) {
                File f = new File(fullDomain);
                fullNameOfinfoFile = f.getName();
                break;
            }
        }
        File f = new File(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                + aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getRelativePathOfFilesOfurrentItemInDevice()
                + fullNameOfinfoFile);
        if (f.exists()) {
            d = Drawable.createFromPath(f.getAbsolutePath());
            icon.setImageDrawable(d);

        }

        f = new File(Info_Dirs_File_ENUM.ROOT_DEVICE_FOLDER
                + aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getRelativePathOfFilesOfurrentItemInDevice()
                + "popis.txt");
        if (f.exists()) {
            FileWorker fw = new FileWorker(f.getAbsolutePath());
            try {
                text = fw.readDataFromFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ActionBarActivity a = (ActionBarActivity) this.getActivity();
        a.getSupportActionBar().setTitle(aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getOriginalName());
        TextView title = (TextView) V.findViewById(R.id.title);
        title.setText(aInfoFileObjects.getaItemsInfo().get(keyselAnimal).getOriginalName());
        //Expose the indent for the first three rows
        TextView MessageView = (TextView) V.findViewById(R.id.message_view);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        FlowTextHelper.tryFlowText(getActivity(), text, icon, MessageView, display, 10);
        //SS.setSpan(new MyLeadingMarginSpan2(100/pixelsInLine,leftMargin),0,SS.length(),0);

        Log.e("DEBUG", "OnCreateView of ObjectFullInfo");


        //MessageView. setText ( SS ) ;
        // Inflate the layout for this fragment
        return V;
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of ObjectFullInfo");
        d = null;
        System.gc();
        super.onPause();
    }
}

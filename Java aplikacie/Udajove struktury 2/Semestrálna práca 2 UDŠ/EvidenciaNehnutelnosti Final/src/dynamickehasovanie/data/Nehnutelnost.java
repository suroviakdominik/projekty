/*;
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.data;

import dynamickehasovanie.struktura.TypeInfo;
import static dynamickehasovanie.data.ImplementatorZaznam.aInstance;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author Dominik
 */
public class Nehnutelnost extends Zaznam {

    public static final int DLZKA_NAZOV_KU = 20;
    public static final int DLZKA_POPIS = 60;
    public static final int DLZKA_GPS = 90;
    private static final int VELKOST_PRVOK_SUP_CISLO = TypeInfo.sizeof(Integer.class);
    private static final int VELKOST_PRVOK_NAZOV_KU = TypeInfo.sizeof(Character.class);
    private static final int VELKOST_PRVOK_POPIS = TypeInfo.sizeof(Character.class);
    private static final ImplementatorZaznam aImplementatorGPS = GPSPozicia.getImplmentatorTohtoZaznamu();
    private static final int VELKOST_GPS_POZICIA = Nehnutelnost.aImplementatorGPS.get_Velkost_Zaznamu_V_Bajtoch();

    private int aSupisneCislo;
    private char[] aNazovKU = new char[DLZKA_NAZOV_KU];
    private char[] aPopis = new char[DLZKA_POPIS];
    private GPSPozicia[] aGPS = new GPSPozicia[DLZKA_GPS];

    public Nehnutelnost(int aSupisneCislo, char[] paNazvoKU, char[] paPopis, GPSPozicia[] paGPS) {
        this.aSupisneCislo = aSupisneCislo;
        System.arraycopy(paNazvoKU, 0, aNazovKU, 0, paNazvoKU.length);
        System.arraycopy(paPopis, 0, aPopis, 0, paPopis.length);
        System.arraycopy(paGPS, 0, aGPS, 0, paGPS.length);
    }

    public Nehnutelnost(int aSupisneCislo, char[] paNazvoKU) {
        this.aSupisneCislo = aSupisneCislo;
        System.arraycopy(paNazvoKU, 0, aNazovKU, 0, paNazvoKU.length);
    }

    private Nehnutelnost() {//pre vytvaranie neplatneho zaznamu
        super();
        aSupisneCislo = -1;
    }

    public int getaSupisneCislo() {
        return aSupisneCislo;
    }

    public String getaNazovKU() {
        return new String(aNazovKU).trim();
    }

    public String getaPopis() {
        return new String(aPopis).trim();
    }

    public GPSPozicia[] getaGPS() {
        return aGPS;
    }

    public void setaSupisneCislo(int aSupisneCislo) {
        this.aSupisneCislo = aSupisneCislo;
    }

    public void setaPopis(char[] paPopis) {
        System.arraycopy(paPopis, 0, aPopis, 0, DLZKA_POPIS);
    }

    public void setaNazovKU(char[] paNazovKU) {
        System.arraycopy(paNazovKU, 0, aNazovKU, 0, DLZKA_NAZOV_KU);
    }

    public static ImplementatorZaznam getImplmentatorTohtoZaznamu() {
        return ImplementatorNehnutelnost.getInstance();
    }

    @Override
    public int hashCode() {
        int ret = 0;
        for (int i = 0; i < DLZKA_NAZOV_KU; i++) {
            ret += aNazovKU[i];
        }
        int pom = ret & 15;//vyberie z nazvu 4 najmenej vyznamne bity
        pom = pom ^ (240 & aSupisneCislo);//1111 0000 = 240 -vyberie zo supineho cisla iba 4 bity
        pom = pom ^ (aSupisneCislo & 3840);//1111 0000 0000
        pom = pom ^ (61440 & ret);//1111 0000 0000 0000
        pom = pom ^ (16711680 & aSupisneCislo);
        pom = pom ^ (-16777216 & ret);
        return pom;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nehnutelnost other = (Nehnutelnost) obj;
        for (int i = 0; i < aNazovKU.length; i++) {
            if (aNazovKU[i] != other.aNazovKU[i]) {
                return false;
            }
        }
        return aSupisneCislo == other.aSupisneCislo;
    }

    @Override
    public String toString() {
        String ret;
        if (jePlatny()) {
            ret = "\nNEHNUTELNOST{" + "\nSupisne Cislo: " + String.valueOf(aSupisneCislo).trim()
                    + "\nNazov KU: " + String.valueOf(aNazovKU).trim() + "\n\nPopis:\n"
                    + String.valueOf(aPopis).trim() + "\n\nGPS Pozicie: \n";
            for (int i = 0; i < DLZKA_GPS; i++) {
                if (aGPS[i] != null && aGPS[i].jePlatny()) {
                    ret += aGPS[i].toString() + "\n";
                }
            }
            ret += "}";
        } else {
            ret = "\nNEPLATNY ZAZNAM";
        }
        return ret;
    }

    /**
     *
     * @author Dominik
     */
    private static class ImplementatorNehnutelnost extends ImplementatorZaznam {

        private ImplementatorNehnutelnost(Class aImplementatorForClass) {
            super(aImplementatorForClass);
        }

        @Override
        public Zaznam toZaznam(byte[] paBinDataZaznam) {
            ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(paBinDataZaznam);
            DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
            Nehnutelnost ret = new Nehnutelnost();
            try {
                char platnost = hlpInStream.readChar();
                if (platnost == Platnost.PLATNY.get_Znacka_V_Subore()) {
                    ret.aJePlatny = Platnost.PLATNY;
                } else {
                    ret.aJePlatny = Platnost.NEPLATNY;
                }

                ret.aSupisneCislo = hlpInStream.readInt();

                for (int i = 0; i < DLZKA_NAZOV_KU; i++) {
                    ret.aNazovKU[i] = hlpInStream.readChar();
                }

                for (int i = 0; i < DLZKA_POPIS; i++) {
                    ret.aPopis[i] = hlpInStream.readChar();
                }

                byte[] bufGPS = new byte[VELKOST_GPS_POZICIA];
                for (int i = 0; i < DLZKA_GPS; i++) {
                    if (hlpInStream.read(bufGPS) < 0) {
                        throw new IllegalStateException("Takato situacia nemala nikdy nastat");
                    }
                    ret.aGPS[i] = (GPSPozicia) aImplementatorGPS.toZaznam(bufGPS);
                }
            } catch (IOException e) {
                throw new IllegalStateException("Error during conversion from byte array.");
            }
            return ret;
        }

        @Override
        public byte[] toByteArray(Zaznam paZaznam) {
            ByteArrayOutputStream hlpByteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
            if (paZaznam instanceof Nehnutelnost) {
                Nehnutelnost os = (Nehnutelnost) paZaznam;
                try {
                    hlpOutStream.writeChar(os.aJePlatny.get_Znacka_V_Subore());

                    hlpOutStream.writeInt(os.aSupisneCislo);

                    for (int i = 0; i < DLZKA_NAZOV_KU; i++) {
                        hlpOutStream.writeChar(os.aNazovKU[i]);
                    }

                    for (int i = 0; i < DLZKA_POPIS; i++) {
                        hlpOutStream.writeChar(os.aPopis[i]);
                    }

                    for (int i = 0; i < DLZKA_GPS; i++) {
                        if (os.aGPS[i] != null) {
                            hlpOutStream.write(aImplementatorGPS.toByteArray(os.aGPS[i]));
                        } else {
                            hlpOutStream.write(aImplementatorGPS.toByteArray(aImplementatorGPS.vytvorNeplatnyZaznam()));
                        }
                    }

                    return hlpByteArrayOutputStream.toByteArray();

                } catch (IOException e) {
                    throw new IllegalStateException("Error during conversion to byte array.");
                }
            }
            throw new IllegalArgumentException("Implementator nie je implementatorom "
                    + "pre genericky typ: " + paZaznam.getClass().getSimpleName()
                    + " zadany pri vytvarani dynamickeho hashu"
                    + "Implementator je implementatorom pre objekty typu: "
                    + super.aImplementatorForClass.getSimpleName());
        }

        public static ImplementatorZaznam getInstance() {
            if (aInstance != null) {
                return aInstance;
            } else {
                return new ImplementatorNehnutelnost(Nehnutelnost.class);
            }
        }

        @Override
        public int get_Velkost_Zaznamu_V_Bajtoch() {
            return Zaznam.getVelkostObjektuVBajtoch() + VELKOST_PRVOK_SUP_CISLO
                    + DLZKA_NAZOV_KU * VELKOST_PRVOK_NAZOV_KU + DLZKA_POPIS * VELKOST_PRVOK_POPIS + DLZKA_GPS * VELKOST_GPS_POZICIA;
        }

        @Override
        public Zaznam vytvorNeplatnyZaznam() {
            Nehnutelnost os = new Nehnutelnost();
            os.setPlatny(Platnost.NEPLATNY);
            return os;
        }
    }
}

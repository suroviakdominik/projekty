/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Vykonavac;

import wof00.Predmety.IPredmet;
import wof00.Hra.Hrac;

/**
 *
 * @author suroviak3
 */
public class VykonavacInfo implements IVykonavac {

    @Override
    public boolean vykonaj(Hrac paHrac, String paNazov) {
        IPredmet infoOPredmete = paHrac.dajPredmet(paNazov);
        if (infoOPredmete == null) {
            infoOPredmete = paHrac.dajPoziciu().zistiPredmet(paNazov);
            if (infoOPredmete == null) {
                System.out.println("Taky predmet neexistuje.");
                return false;
            }

        }

        System.out.println("Popis predmetu: " + infoOPredmete.getPopis());
        return false;
    }
}

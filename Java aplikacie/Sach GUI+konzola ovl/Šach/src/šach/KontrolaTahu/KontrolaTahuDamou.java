/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.KontrolaTahu;

import šach.hra.Hrac;

/**
 *
 * @author Dominik
 */
public class KontrolaTahuDamou implements IKontrolaTahu {

    private int aSucasnaXPozicia;
    private int aSucasnaYPozicia;
    private int aNovaXPozicia;
    private int aNovaYPozicia;
    private Hrac aHrac;

    /**
     * Naplní atribúty triedy novými a aktuálnymi súradnicami a hráčom , ktorý
     * vykonáva ťah.
     *
     * @param paNovaXPozicia
     * @param paNovaYPozicia
     * @param paHrac
     */
    public KontrolaTahuDamou(int paNovaXPozicia, int paNovaYPozicia, Hrac paHrac) {
        this.aNovaXPozicia = paNovaXPozicia;
        this.aNovaYPozicia = paNovaYPozicia;
        this.aHrac = paHrac;
        aSucasnaXPozicia = (aHrac.getaVybranaFigurka().getX() - 10) / 50;
        aSucasnaYPozicia = (aHrac.getaVybranaFigurka().getY() - 20) / 50;
    }

    @Override
    public boolean skontrolujTah() {

        boolean lpSmerProtiOsiY = (aSucasnaYPozicia > aNovaYPozicia) ? true : false;
        boolean lpSmerProtiOsiX = (aSucasnaXPozicia > aNovaXPozicia) ? true : false;
        if (Math.abs(aSucasnaXPozicia - aNovaXPozicia) == (Math.abs(aSucasnaYPozicia - aNovaYPozicia))) {
            if ((!lpSmerProtiOsiX) && (!lpSmerProtiOsiY)) {
                return moznoSaPosunutSikmoDopravaNadol();
            } else if ((lpSmerProtiOsiX) && (!lpSmerProtiOsiY)) {
                return moznoSaPosunutSikmoDolavaNadol();
            } else if ((!lpSmerProtiOsiX) && (lpSmerProtiOsiY)) {
                return moznoSaPosunutSikmoDopravaNahor();
            } else if ((lpSmerProtiOsiX) && (lpSmerProtiOsiY)) {
                return moznoSaPosunutSikmoDolavaNahor();
            }
        }
        return false;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * doprava nadol.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDopravaNadol() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
            int j = aSucasnaXPozicia + pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else if ((i == aNovaYPozicia) && (j == aNovaXPozicia)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * dolava nadol.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDolavaNadol() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia + 1; i <= aNovaYPozicia; i++) {
            int j = aSucasnaXPozicia - pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else if ((i == aNovaYPozicia) && (j == aNovaXPozicia)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * doprava nahor.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDopravaNahor() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
            int j = aSucasnaXPozicia + pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else if ((i == aNovaYPozicia) && (j == aNovaXPozicia)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * metóda, ktorá sa použije na kontrolu ťahu ak sa pohybujeme po diagonále
     * dolava nahor.
     *
     * @return Ak je ťah platný vráti true, inak false.
     */
    private boolean moznoSaPosunutSikmoDolavaNahor() {
        int pocitadloPosunVSmereOsiX = 1;
        for (int i = aSucasnaYPozicia - 1; i >= aNovaYPozicia; i--) {
            int j = aSucasnaXPozicia - pocitadloPosunVSmereOsiX;
            pocitadloPosunVSmereOsiX++;
            if (aHrac.getaSachovnicaNaKtorejSaHra()[i][j] != null) {
                if ((i == aNovaYPozicia) && (j == aNovaXPozicia) && (!aHrac.getaSachovnicaNaKtorejSaHra()[i][j].getFarba().equals(aHrac.getFarbaFigurok()))) {
                    return true;
                } else if ((i == aNovaYPozicia) && (j == aNovaXPozicia)) {
                    return false;
                }
            }
        }
        return true;
    }

}

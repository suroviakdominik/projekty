/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package šach.Prikazy;

import Grafika.IGrafickeObjekty;

import Grafika.Platno;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import šach.hra.Hrac;
import šach.sachovnica.figurky.Figurka;

/**
 * Pri zadaní príkazu chod NacitavacPrikazov zavolá práve objekt tejto triedy,
 * ktorý hýbe jednotlivými figúrkami a presúva ich na miesto určené súradnicami
 * prijatými v parametri.Trieda implementuje interface IPrikaz.
 *
 * @author Dominik
 */
public class PrikazChod implements IPrikaz {

    /**
     * ak je to možné a je zadaný správny parameter skontroluje a vykoná ťah.
     * Výsledok či bol ťah prevedený ohlási pomocou návratového typu boolean.
     *
     * @param paHracKtoryZadavaPrikazy - hráč, ktorý je na ťahu
     * @param lpParameter -nové súradnice
     * @return true ak sa ťah vykonal, false inak
     */
    @Override
    public boolean vykonaj(Hrac paHracKtoryZadavaPrikazy, String lpParameter) {

        if (paHracKtoryZadavaPrikazy.getaVybranaFigurka() == null) {
            System.out.println("Pred vykonanim tahu si musite vybrat figurku pomocou prikazu vybrat + suradnice.");

        } else if (lpParameter == null) {
            System.out.println("Nezadali ste suradnice kam sa chcete posunut. Zopakujte prikaz a zadajte suradnice.");

        } else {
            String[] lpPoleSuradnic = new String[2];
            try {
                lpPoleSuradnic[0] = Character.toString(lpParameter.charAt(0));
                lpPoleSuradnic[1] = Character.toString(lpParameter.charAt(1));
            } catch (StringIndexOutOfBoundsException ex) {
                System.out.println("Nezadali ste uplnu suradnicu");
                return false;
            }
            Figurka lpVybranaFigurka;
            int lpSucasnaYSuradnicaFigurky = (paHracKtoryZadavaPrikazy.getaVybranaFigurka().getY() - 20) / 50;
            int lpSucasnaXSuradnicaFigurky = (paHracKtoryZadavaPrikazy.getaVybranaFigurka().getX() - 10) / 50;
            int lpNovaYlonovaSuradnica;
            int lpNovaXSovaSuradnica;

            try {
                lpNovaYlonovaSuradnica = Integer.parseInt(lpPoleSuradnic[0]) - 1;
                lpNovaXSovaSuradnica = Integer.parseInt(lpPoleSuradnic[1]) - 1;
            } catch (NumberFormatException ex) {
                System.out.println("Zadali ste nespravne suradnice. Zopakujte volbu.");
                return false;
            }

            if ((lpNovaYlonovaSuradnica < 8) && (lpNovaYlonovaSuradnica >= 0) && (lpNovaXSovaSuradnica < 8) && (lpNovaXSovaSuradnica >= 0) && paHracKtoryZadavaPrikazy.skontrolujTah(paHracKtoryZadavaPrikazy.getaVybranaFigurka().getMeno(), lpNovaXSovaSuradnica, lpNovaYlonovaSuradnica)) {
                if (paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpNovaYlonovaSuradnica][lpNovaXSovaSuradnica] != null)//ak vyhadzujeme vymaze figurku ktoru vyhadzujeme
                {
                    int dlzkaGrafickehoPolaFigurky = paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpNovaYlonovaSuradnica][lpNovaXSovaSuradnica].getGrafickeObjektyFigurky().length;
                    for (int i = 0; i < dlzkaGrafickehoPolaFigurky; i++) {
                        IGrafickeObjekty objekt = paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpNovaYlonovaSuradnica][lpNovaXSovaSuradnica].getGrafickeObjektyFigurky()[i];
                        Platno.dajPlatno().erase(objekt);
                    }
                }

                paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpNovaYlonovaSuradnica][lpNovaXSovaSuradnica] = paHracKtoryZadavaPrikazy.getaVybranaFigurka();

                for (int i = 0; i < paHracKtoryZadavaPrikazy.getaVybranaFigurka().getGrafickeObjektyFigurky().length; i++)//odtialto je prekreslovanie figurky na platne
                {
                    IGrafickeObjekty objekt = paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpSucasnaYSuradnicaFigurky][lpSucasnaXSuradnicaFigurky].getGrafickeObjektyFigurky()[i];
                    Platno.dajPlatno().erase(objekt);
                }
                paHracKtoryZadavaPrikazy.getaSachovnicaNaKtorejSaHra()[lpSucasnaYSuradnicaFigurky][lpSucasnaXSuradnicaFigurky] = null;
                paHracKtoryZadavaPrikazy.getaVybranaFigurka().umiestniFigurku(lpNovaXSovaSuradnica * 50 + 10, lpNovaYlonovaSuradnica * 50 + 20);
                paHracKtoryZadavaPrikazy.getaVybranaFigurka().nakresli();
                paHracKtoryZadavaPrikazy.setaVybranaFigurka(null);
                return true;
            } else {
                System.out.println("Neplatny tah.");

            }
        }
        return false;
    }

}

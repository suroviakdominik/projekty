/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafika;

import java.awt.*;

/**
 * Stvorec, s ktorym mozno pohybovat a nakresli sa na platno.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 1.0 (15 July 2000)
 */
public class Stvorec implements IGrafickeObjekty {

    private int aStrana;
    private int aXLavyHorny;
    private int aYLavyHorny;
    private String aFarba;
    private boolean aJeViditelny;

    /**
     * Vytvor novy stvorec preddefinovanej farby na preddefinovanej pozicii.
     */
    public Stvorec() {
        aStrana = 30;
        aXLavyHorny = 60;
        aYLavyHorny = 50;
        aFarba = "red";
        aJeViditelny = false;
    }

    /**
     * (Stvorec) Zobraz sa.
     */
    @Override
    public void zobraz() {
        aJeViditelny = true;
        nakresli();
    }

    /**
     * (Stvorec) Skry sa.
     */
    @Override
    public void skry() {
        zmaz();
        aJeViditelny = false;
    }

    /**
     * (Stvorec) Posun sa vpravo o pevnu dlzku.
     */
    @Override
    public void posunVpravo() {
        posunVodorovne(20);
    }

    /**
     * (Stvorec) Posun sa vlavo o pevnu dlzku.
     */
    @Override
    public void posunVlavo() {
        posunVodorovne(-20);
    }

    /**
     * (Stvorec) Posun sa hore o pevnu dlzku.
     */
    @Override
    public void posunHore() {
        posunZvisle(-20);
    }

    /**
     * (Stvorec) Posun sa dole o pevnu dlzku.
     */
    @Override
    public void posunDole() {
        posunZvisle(20);
    }

    /**
     * (Stvorec) Posun sa vodorovne o dlzku danu parametrom.
     */
    @Override
    public void posunVodorovne(int paVzdialenost) {
        zmaz();
        aXLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Stvorec) Posun sa zvisle o dlzku danu parametrom.
     */
    @Override
    public void posunZvisle(int paVzdialenost) {
        zmaz();
        aYLavyHorny += paVzdialenost;
        nakresli();
    }

    /**
     * (Stvorec) Posun sa pomaly vodorovne o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunVodorovne(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aXLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Stvorec) Posun sa pomaly vodorovne o dlzku danu parametrom.
     */
    @Override
    public void pomalyPosunZvisle(int paVzdialenost) {
        int delta;

        if (paVzdialenost < 0) {
            delta = -1;
            paVzdialenost = -paVzdialenost;
        } else {
            delta = 1;
        }

        for (int i = 0; i < paVzdialenost; i++) {
            aYLavyHorny += delta;
            nakresli();
        }
    }

    /**
     * (Stvorec) Zmen dlzku strany na hodnotu danu parametrom. Dlzka strany musi
     * byt nezaporne cele cislo.
     */
    public void zmenStranu(int paDlzka) {
        zmaz();
        aStrana = paDlzka;
        nakresli();
    }

    /**
     * (Stvorec) Zmen farbu na hodnotu danu parametrom. Nazov farby musi byt po
     * anglicky. Mozne farby su tieto: cervena - "red" zlta - "yellow" modra -
     * "blue" zelena - "green" fialova - "magenta" cierna - "black" biela -
     * "white" hneda - "brown"
     */
    @Override
    public void zmenFarbu(String paFarba) {
        aFarba = paFarba;
        nakresli();
    }

    /*
     * Draw the square with current specifications on screen.
     */
    private void nakresli() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.draw(this, aFarba,
                    new Rectangle(aXLavyHorny, aYLavyHorny, aStrana, aStrana));

        }
    }

    /*
     * Erase the square on screen.
     */
    private void zmaz() {
        if (aJeViditelny) {
            Platno canvas = Platno.dajPlatno();
            canvas.erase(this);
        }
    }
}

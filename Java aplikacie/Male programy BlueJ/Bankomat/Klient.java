
import java.util.ArrayList;
public class Klient
{
   private Osoba aOsoba;
   private Banka aBanka;
   private ArrayList <Ucet> aUcty;
   
   public Klient(Osoba paOsoba,
                 Banka paBanka)
   {
       aOsoba=paOsoba;
       aBanka=paBanka;
       aUcty=new ArrayList<Ucet>();//kazdy klient moze mat viac uctov, ak zanikne klient,zanikne aj jeho ucet v danej banke(agragacia-klient vlastni ucty-ked zanikne ucet klient moze stale existovat ak ma vytvoreny ucet v inej banke)
       aOsoba.pridajKlienta(this);//kazda osoba moze byt klientom vo viacerych bankach,ked zanikne osoba,zanikne aj klient danej banky (osoba je vo vztahu kompozicie ku klientovi/osoba riadi zivotny cyklus klienta).Naopak to neplati.Preto ArrayList klientov v osobe.
   }
   
     public Osoba dajOsobu()
     {
         return aOsoba;
     }
     
     public Banka dajBanku()
     {
         return aBanka;
     }
     
     public ArrayList<Ucet> dajUcty()
     {
         return aUcty;
     } 
     
     public void pridajUcet(Ucet paUcet)
     {
         aUcty.add(paUcet);
     }   
     
     public String toString()
     {
       String res= aOsoba+"\n"+ "ucty:\n";
       //for (int i=0;i<aUcty.size();i++)
       //{     
       //    res +=" "+aUcty.get(i)+"\n";
       //}
       
       for(Ucet u: aUcty)
       {
          res +=" "+u+"\n";
       }
      return res;
     }
}

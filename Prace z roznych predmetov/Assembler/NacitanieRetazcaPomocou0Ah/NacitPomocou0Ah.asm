;-----------------------------------------NACITANIE RETAZCA VELKOSTI 4 POMOCOU SLUZBY 0AH A NASLEDOVNY VYPIS PO ZNAKOCH------------------------------
.MODEL small
.STACK 100h
.DATA
Retaz DB 7 dup(?)
.CODE
Zaciatok:
        mov ax,@DATA
        mov ds,ax
        
        mov dx,offset Retaz
        mov bx,dx
        mov Retaz,6
        
        mov ah,0Ah
        int 21h
        mov ch,0
        mov cl,byte ptr[bx+1]
        mov di,2
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h
        
        mov dl,[bx+6]
        Cyklus:
             mov dl,[bx+di]
             mov ah,2
             int 21h
             inc di
        loop Cyklus
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok

package com.suroviak.dominik.zooAplication.UpdateClasses;

/**
 * Created by Dominik on 11.3.2015.
 */
public interface IProgressBarUpdate {
    public void setProgressStatus(int paProgressStatus);

    public void setText(String paText);
}

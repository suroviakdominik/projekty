/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.Objects;

/**
 * Trieda, ktorej instancie predstavuje cast GEO polohy.
 * Instancia moze by  zemepisnou sirkou alebo dlzkou
 * @author Dominik
 */
public class Coordinate implements Serializable{
    private String aDegree;
    private String aMinutes;
    private String aSeconds;
    private static final long serialVersionUID = 4L;

    /**
     * Vytvori zemepisnu dlzku alebo sirku
     * @param aDegree- stupne vytvareho koordinatu
     * @param aMinutes- minuty vytvareho koordinatu
     * @param aSeconds - sekundy vytvareho koordinatu
     */
    public Coordinate(String aDegree, String aMinutes, String aSeconds) {
        this.aDegree = aDegree;
        this.aMinutes = aMinutes;
        this.aSeconds = aSeconds;
    }

    /**
     * Vrati dekadicku podobu koordinati
     * @return dekadicka podoba koordinatu
     */
    public double getDECRepresentationOfCoordinate()
    {
        double deg=Double.parseDouble(aDegree);
        double min = Double.parseDouble(aMinutes);
        double sec =Double.parseDouble(aSeconds);
        return deg+min/60+sec/3600;
    }

    /**
     * 
     * @return stupne koordinatu
     */
    public String getaDegree() {
        return aDegree;
    }

    /**
     * 
     * @return minuty koordinatu
     */
    public String getaMinutes() {
        return aMinutes;
    }

    /**
     * 
     * @return sekundy koordinatu
     */
    public String getaSeconds() {
        return aSeconds;
    }

    
    
    
    @Override
    public String toString() {
        return aDegree+":"+aMinutes+":"+aSeconds;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.aDegree);
        hash = 89 * hash + Objects.hashCode(this.aMinutes);
        hash = 89 * hash + Objects.hashCode(this.aSeconds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate other = (Coordinate) obj;
        if (!Objects.equals(this.aDegree, other.aDegree)) {
            return false;
        }
        if (!Objects.equals(this.aMinutes, other.aMinutes)) {
            return false;
        }
        if (!Objects.equals(this.aSeconds, other.aSeconds)) {
            return false;
        }
        return true;
    }
  
    
    
}

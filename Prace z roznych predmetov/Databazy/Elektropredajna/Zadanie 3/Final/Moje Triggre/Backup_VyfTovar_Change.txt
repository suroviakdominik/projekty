create or replace TRIGGER BACKUPVYFAKTUROVANYTOVARCHANGE 
BEFORE DELETE OR INSERT OR UPDATE ON BACKUPDELETEVYFAKTUROVANYTOVAR 
REFERENCING OLD AS STARY NEW AS NOVY 
For each row
when (user not in ('suroviak3','juris2','kruzelak'))
BEGIN
   raise_application_error(-1017,
  'ERROR - Nemate pravo menit obsah tejto tabulky.');
END;
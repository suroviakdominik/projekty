.MODEL small
.STACK 100h
.DATA
Retaz DB 7 dup(?)
MenoSuboru DB 'Subor.txt',0
PopisovacSuboru DW (-1)
errorOtvSub DB 'Subor sa nepodarilo pre citanie otvorit!$'
.CODE
;-----------------------------------PROCEDURA OTVORI SUBOR PRE CITANIE,VSTUPNY PARAMETER-offset MenaSuboru do DX---------------------------------------
OpenFileForRead PROC 
        push ax
        mov al,0
        mov ah,3Dh
        int 21h
        mov PopisovacSuboru,ax;ak subor s menom este neexistuje v ax bude 2, pri pokuse citania zo suboru sa bude citat z klavesnice
        lahf;skopiruje register priznakov flags do registra ah
        AND ah,00000001b;vsetky ostatne az na carryflag vynuluje, ak cf=1 subor pre citanie sa nepodarilo otvorit
        
        cmp ah,1; ak je cf=1 vypise chybovu hlasku
        je VypisChybHlasku
        
        
        pop ax
        ret
  
 VypisChybHlasku:
        mov dx,offset errorOtvSub
        mov ah, 9
        int 21h 
        mov DL,0AH;uloz do registra posun dole
        mov AH,2;do registra AH uloz cislo sluzby
        int 21h 
        
        pop ax
        ret
        
OpenFileForRead ENDP

Zaciatok:
        mov ax,@DATA
        mov ds,ax
       ;---------------------------------OTVORENIE SUBORU PRE CITANIE POMOCOU PROCEDURY----------------------------------------
        mov dx, offset MenoSuboru
        call OpenFileForRead
        
        
        ;--------------------------------CITANIE ZO SUBORU---------------------------------------------------
        
        mov bx,PopisovacSuboru;ak je popisovac suboru 2, tak znaky musime zadat z klavesnice lebo subor sa pre citanie nepodarilo otvorit
        mov cx,4;pocet bytov, ktore sa budu kopirovat do retazca(vratane medzier=1B)
        mov dx, offset Retaz;offset pola kde sa budu znaky kopirovat
        mov ah,3Fh
        int 21h; 
        
        ;-------------------------------ZATVORENIE SUBORU PRE CITANIE----------------------------------------
         mov bx,PopisovacSuboru
         mov ah,3Eh
         int 21h
        
       ;------------------------------CYKLUS NA VYPIS ZNAKOV Z POLA Retaz-----------------------------------
        mov di,0
        Cyklus:
            mov dl,[Retaz+di]
            mov ah,2
            int 21h
            inc di
            loop Cyklus
             
        
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok

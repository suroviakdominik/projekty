<?php
session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "osoba";
    private $conn;
    private $biggestId=0;

     function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
         mysqli_query($this->conn,"SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function findBiggestId() {
                $sql = "SELECT idKoment FROM komenty";
                $result = mysqli_query($this->conn, $sql);
                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        if($row["idKoment"]>$this->biggestId)
                        {
                            $this->biggestId=$row["idKoment"];
                        }
                    }
                    
                } 
            }

    
   function vypisKomentare()
   {
                
                $sql = "SELECT idKoment,idUzivatel,text FROM komenty ORDER BY idKoment";
                $result = mysqli_query($this->conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        $userId=$row["idUzivatel"];
                        $sql2 = "SELECT email FROM pouzivatelia WHERE id=$userId";
                        $result2 = mysqli_query($this->conn, $sql2);
                        $row2 = mysqli_fetch_assoc($result2);
                        echo "<div class=\"komentar\"><span>Email: " . $row2["email"] . "</span><p>" ."Text: ". $row["text"]."</p>";
                        $komentId=$row["idKoment"];
                        if(isset($_SESSION["login"])&&$_SESSION["login"]==$userId ||isset($_SESSION["login"])&&$_SESSION["login"]==0)
                        {
                            echo "</br>";
                            echo "<input type=\"button\" value=\"Uprav\" onclick=\"upravKoment(this,$komentId)\"/>";
                            echo "<input type=\"button\" value=\"Zmaz\" onclick=\"zmazKoment($komentId)\"/>";
                        }
                         echo "</div>";
                    }
                } else 
                    echo "0 results";
                
         
   
   }

    function ukonciPristup() {
        $this->conn->close();
    }

}
$db = new DBPraca;
$db->vypisKomentare();
$db->ukonciPristup();

?>

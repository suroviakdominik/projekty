/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heuristika;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Dominik
 */
public class Heuristika {

    private int[] aNacitaneCenyPredmetov;
    private int[] aNacitaneHmotnostiPredmetov;
    private int[] aPoradoveCisloPredmetuVSubore;
    private Batoh aBatoh;
    private boolean aPrebehlaVymennaHeuristika;

    public Heuristika(Batoh paBatoh) throws FileNotFoundException, NoSuchElementException {
        aPrebehlaVymennaHeuristika = false;
        aBatoh = paBatoh;
        aNacitaneCenyPredmetov = new int[paBatoh.getaZKolkychPredmetovVyberame()];
        aNacitaneHmotnostiPredmetov = new int[paBatoh.getaZKolkychPredmetovVyberame()];
        nacitajUdajeOPredmetochZoSuboru();
    }

    public boolean prebehlaHeuristikaVyhKoef() {
        return !aPrebehlaVymennaHeuristika;
    }

    private void usporiadaniePodlaVyhKoeficientov(int lavy, int pravy)//quick sort
    {
        int pivot = lavy;
        int pov_pravy = pravy;
        int pov_lavy = lavy;
        while (lavy != pravy && lavy < pravy) {

            for (int i = pravy; i > lavy; i--) {
                if (((double) aNacitaneCenyPredmetov[pravy] / (double) aNacitaneHmotnostiPredmetov[pravy])
                        > ((double) aNacitaneCenyPredmetov[pivot] / (double) aNacitaneHmotnostiPredmetov[pivot])) {
                    prehodPrvkyPola(pivot, pravy);
                    pivot = pravy;
                    break;
                }
                pravy--;

            }

            for (int i = lavy; i < pravy; i++) {
                if (((double) aNacitaneCenyPredmetov[lavy] / (double) aNacitaneHmotnostiPredmetov[lavy])
                        < ((double) aNacitaneCenyPredmetov[pivot] / (double) aNacitaneHmotnostiPredmetov[pivot])) {
                    prehodPrvkyPola(pivot, lavy);
                    pivot = lavy;
                    break;
                }
                lavy++;

            }
        }

        System.out.println("\n\n");
        if (pivot - 1 > pov_lavy) {
            usporiadaniePodlaVyhKoeficientov(pov_lavy, pivot - 1);
        }
        if (pov_pravy > pivot + 1) {
            usporiadaniePodlaVyhKoeficientov(pivot + 1, pov_pravy);
        }
        return;
    }

    private void prehodPrvkyPola(int paIndexPrveho, int paIndexDruheho) {
        int lpCenaPrvy = aNacitaneCenyPredmetov[paIndexPrveho];
        aNacitaneCenyPredmetov[paIndexPrveho] = aNacitaneCenyPredmetov[paIndexDruheho];
        aNacitaneCenyPredmetov[paIndexDruheho] = lpCenaPrvy;

        int lpHmostnostPrvy = aNacitaneHmotnostiPredmetov[paIndexPrveho];
        aNacitaneHmotnostiPredmetov[paIndexPrveho] = aNacitaneHmotnostiPredmetov[paIndexDruheho];
        aNacitaneHmotnostiPredmetov[paIndexDruheho] = lpHmostnostPrvy;

        int lpPorCisloPrvy = aPoradoveCisloPredmetuVSubore[paIndexPrveho];
        aPoradoveCisloPredmetuVSubore[paIndexPrveho] = aPoradoveCisloPredmetuVSubore[paIndexDruheho];
        aPoradoveCisloPredmetuVSubore[paIndexDruheho] = lpPorCisloPrvy;
    }

    private boolean kontrolaQuickSort() {
        for (int i = 0; i < aNacitaneCenyPredmetov.length - 1; i++) {
            if ((double) aNacitaneCenyPredmetov[i] / (double) aNacitaneHmotnostiPredmetov[i] < (double) aNacitaneCenyPredmetov[i + 1] / (double) aNacitaneHmotnostiPredmetov[i + 1]) {
                System.out.println("Chyba");
                return false;
            }

        }
        return true;
    }

    /*private void vypis()
     {
     for (int i = 0; i < aNacitaneCenyPredmetov.length; i++) 
     {
     System.out.println(i+": "+"VK: "+(double)aNacitaneCenyPredmetov[i]/aNacitaneHmotnostiPredmetov[i]);   
     }
     }*/
    public void primHeuristikaVyhKoeficeinty() {
        usporiadaniePodlaVyhKoeficientov(0, aNacitaneCenyPredmetov.length - 1);
        if (kontrolaQuickSort()) {
            for (int i = 0; i < aNacitaneCenyPredmetov.length; i++) {
                if (aNacitaneHmotnostiPredmetov[i] <= (aBatoh.getaKapacita() - aBatoh.getaSucasnaVahaBatohu()) && aBatoh.getaPocetVlozenychPredmetov() < aBatoh.getaMaxPocetPredmetovVBatohu()) {
                    aBatoh.vlozPredmet(aPoradoveCisloPredmetuVSubore[i], aNacitaneHmotnostiPredmetov[i], aNacitaneCenyPredmetov[i]);
                }
            }
        }
        aPrebehlaVymennaHeuristika = false;
        aBatoh.setaPrebehlaVymennaHeuristika(false);
    }

    private void nacitajUdajeOPredmetochZoSuboru() throws FileNotFoundException, NoSuchElementException {
        File Ceny = new File("H5_c.txt");
        File Hmotnosti = new File("H5_a.txt");
        try {
            Scanner scanC = new Scanner(Ceny);
            Scanner scanH = new Scanner(Hmotnosti);
            for (int i = 0; i < aBatoh.getaZKolkychPredmetovVyberame(); i++) {
                aNacitaneCenyPredmetov[i] = scanC.nextInt();
                aNacitaneHmotnostiPredmetov[i] = scanH.nextInt();
            }
            scanC.close();
            scanH.close();

        } catch (FileNotFoundException ex) {
            throw ex;
        } catch (NoSuchElementException ex) {
            throw ex;
        }
        aPoradoveCisloPredmetuVSubore = new int[aBatoh.getaZKolkychPredmetovVyberame()];
        for (int i = 0; i < aBatoh.getaZKolkychPredmetovVyberame(); i++) {
            aPoradoveCisloPredmetuVSubore[i] = i;
        }
    }

    public void primHeuristikaVymenna() throws FileNotFoundException {
         //vymHeur();
         //vymHeur();
        nacitajUdajeOPredmetochZoSuboru();
        int[] lpZaradeneObjekty = new int[aBatoh.getaPocetVlozenychPredmetov()];
        int[] lpNezaradeneObjekty = new int[aBatoh.getaZKolkychPredmetovVyberame() - aBatoh.getaPocetVlozenychPredmetov()];
        inicializujZoznamZaradenychANezaradenychObjektov(lpZaradeneObjekty, lpNezaradeneObjekty);
        boolean naslaSaVhodnaVymena = false;
        boolean zoznamDvojicZarANezJePrazdny = false;
        int lpVolnaKapacitaBatohu = aBatoh.getaKapacita() - aBatoh.getaSucasnaVahaBatohu();

        while (naslaSaVhodnaVymena || !zoznamDvojicZarANezJePrazdny) {
            lpVolnaKapacitaBatohu = aBatoh.getaKapacita() - aBatoh.getaSucasnaVahaBatohu();
            zoznamDvojicZarANezJePrazdny = false;
            naslaSaVhodnaVymena = false;
            lpZaradeneObjekty = new int[aBatoh.getaPocetVlozenychPredmetov()];
            lpNezaradeneObjekty = new int[aBatoh.getaZKolkychPredmetovVyberame() - aBatoh.getaPocetVlozenychPredmetov()];
            inicializujZoznamZaradenychANezaradenychObjektov(lpZaradeneObjekty, lpNezaradeneObjekty);
            int t = 0;
            int k = 0;
            while (!naslaSaVhodnaVymena) {

                if ((lpVolnaKapacitaBatohu + aNacitaneHmotnostiPredmetov[lpZaradeneObjekty[t]]) >= (aNacitaneHmotnostiPredmetov[lpNezaradeneObjekty[k]])) {
                    
                        /*System.out.print("VK: " + lpVolnaKapacitaBatohu + " ");
                        System.out.print("M(t)" + aNacitaneHmotnostiPredmetov[lpZaradeneObjekty[t]] + "\t");
                        System.out.print("M(k)" + aNacitaneHmotnostiPredmetov[lpNezaradeneObjekty[k]] + "\t");
                        System.out.print("C(t)" + aNacitaneCenyPredmetov[lpZaradeneObjekty[t]] + "\t");
                        System.out.println("C(k)" + aNacitaneCenyPredmetov[lpNezaradeneObjekty[k]]);
                        System.out.print("VK+M(t=" + t + ") =" + (lpVolnaKapacitaBatohu + aNacitaneHmotnostiPredmetov[lpZaradeneObjekty[t]]) + "\t");
                        System.out.print("M(k=" + k + ")=" + aNacitaneHmotnostiPredmetov[lpNezaradeneObjekty[k]] + "\t");
                        System.out.print((lpVolnaKapacitaBatohu + aNacitaneHmotnostiPredmetov[lpZaradeneObjekty[t]]) >= (aNacitaneHmotnostiPredmetov[lpNezaradeneObjekty[k]]));
                    */
                    naslaSaVhodnaVymena = (aBatoh.getaZisk()) < (aBatoh.getaZisk() - aNacitaneCenyPredmetov[lpZaradeneObjekty[t]]
                            + aNacitaneCenyPredmetov[lpNezaradeneObjekty[k]]);
                }
                if (naslaSaVhodnaVymena) {
                    aBatoh.odoberPredmet(lpZaradeneObjekty[t]);
                    aBatoh.vlozPredmet(lpNezaradeneObjekty[k], aNacitaneHmotnostiPredmetov[lpNezaradeneObjekty[k]], aNacitaneCenyPredmetov[lpNezaradeneObjekty[k]]);
/*
                    System.out.println("\t" + "Bola Vymena");
                    System.out.println();
                    System.out.println();*/

                } else {
                    if (k < lpNezaradeneObjekty.length - 1) {
                        k++;
                    } else if (t < lpZaradeneObjekty.length - 1) {
                        t++;
                        k = 0;
                    }

                    if (k == lpNezaradeneObjekty.length - 1 && t == lpZaradeneObjekty.length - 1) {
                        zoznamDvojicZarANezJePrazdny = true;
                        break;

                    }

                }
            }
        }

        aPrebehlaVymennaHeuristika = true;
        aBatoh.setaPrebehlaVymennaHeuristika(true);
    }

    /*takisto fungujuci algoritmus
    private void vymHeur() throws FileNotFoundException {
        nacitajUdajeOPredmetochZoSuboru();
        int lpVolnaKapacitaBatohu = aBatoh.getaKapacita() - aBatoh.getaSucasnaVahaBatohu();
        int[] zarObj = new int[aBatoh.getaPocetVlozenychPredmetov()];
        int[] NezObjekty = new int[aBatoh.getaZKolkychPredmetovVyberame()- aBatoh.getaPocetVlozenychPredmetov()];
        boolean koniec = false;
        while (!koniec) {
            boolean bolaVymena = false;
            inicializujZoznamZaradenychANezaradenychObjektov(zarObj, NezObjekty);
            int t;
            int k;
            for (t = 0; t < zarObj.length &&!bolaVymena; t++) {
                for (k = 0; k < NezObjekty.length && !bolaVymena; k++) {
                    if (pripustnaVymena(zarObj[t], NezObjekty[k])) 
                    {
                        if (vhodnaVymena(zarObj[t], NezObjekty[k])) {
                            bolaVymena = true;
                        }
                    }
                }

            }
            if(!bolaVymena )
            {
               koniec = true; 
            }
        }
    }

    private boolean pripustnaVymena(int IMt, int IMk) {
        int lpVolnKap = aBatoh.getaKapacita() - aBatoh.getaSucasnaVahaBatohu();
        
        return (lpVolnKap + aNacitaneHmotnostiPredmetov[IMt] >= aNacitaneHmotnostiPredmetov[IMk]);
    }

    private boolean vhodnaVymena(int ICt, int ICk) {
        long ziskTeraz = aBatoh.getaZisk();
        long ziskPoVym = aBatoh.getaZisk() - aNacitaneCenyPredmetov[ICt] + aNacitaneCenyPredmetov[ICk];
        
        if (ziskTeraz < ziskPoVym) {
           
            aBatoh.odoberPredmet(ICt);
            aBatoh.vlozPredmet(ICk, aNacitaneHmotnostiPredmetov[ICk], aNacitaneCenyPredmetov[ICk]);
            return true;
        }
        return false;
    }*/

    private void inicializujZoznamZaradenychANezaradenychObjektov(int[] paPoleZaradeneObjekty, int[] paPoleNezaradeneObjekty) {
        for (int i = 0; i < aBatoh.getaPocetVlozenychPredmetov(); i++) {
            paPoleZaradeneObjekty[i] = aBatoh.getPoradovCisloPredmetu(i);
        }

        int kamVlozitAkNieJeZarad = 0;
        for (int i = 0; i < aBatoh.getaZKolkychPredmetovVyberame(); i++) {
            boolean bolZaradeny = false;
            for (int j = 0; j < aBatoh.getaPocetVlozenychPredmetov(); j++) {
                if (paPoleZaradeneObjekty[j] == i) {
                    bolZaradeny = true;
                }
            }
            if (!bolZaradeny) {
                paPoleNezaradeneObjekty[kamVlozitAkNieJeZarad] = i;
                kamVlozitAkNieJeZarad++;
            }
        }
    }
    
    

    public void zapisVysledokDoSuboru() throws FileNotFoundException, IOException {
        if (aBatoh.getaPocetVlozenychPredmetov() > 0) {
            File subor;

            if (aPrebehlaVymennaHeuristika) {
                subor = new File("VysledokVymHeur.txt");
            } else {
                subor = new File("VysledokVyHKoef.txt");
            }

            PrintWriter pw = new PrintWriter(subor);
            pw.println("Zisk:                   " + aBatoh.getaZisk());
            pw.println("PocetPredmetovVbatohu:  " + aBatoh.getaPocetVlozenychPredmetov());
            pw.println("Vaha Batohu:            " + aBatoh.getaSucasnaVahaBatohu());
            String s = "";
            for (int k = 0; k < aBatoh.getaZKolkychPredmetovVyberame(); k++) {
                boolean bolZaradeny = false;
                for (int j = 0; j < aBatoh.getaPocetVlozenychPredmetov(); j++) {
                    if (aBatoh.getPoradovCisloPredmetu(j) == k) {
                        bolZaradeny = true;
                    }
                }
                if (!bolZaradeny) {
                    s += "0";
                } else {
                    s += "1";
                }

                pw.println(s);
                s = "";
            }
            pw.close();
        } else {
            throw new NullPointerException("Nie je co ulozit. Heuristika zatial neprebehla");
        }
    }

}

<?php
session_start();
if (isset($_SESSION["registracia"])) {
    echo "Uzivatel zo zadanym menom este nebol registrovany. Registrujte sa.";
    unset($_SESSION["registracia"]);
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link  rel="stylesheet"  type="text/css" href="registracia.css"/>
    </head>
    <body>


        <?php

        class DBPraca {

            private $servername = "localhost";
            private $meno = "root";
            private $heslo = "";
            private $databaza = "osoba";
            private $conn;
            private $biggestId = 0;

            function __construct() {
                $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

                // Check connection
                if ($this->conn->connect_error) {
                    die("Connection failed: " . $this->conn->connect_error);
                }
                mysqli_query($this->conn, "SET CHARACTER SET utf8");
                //echo "Connected successfully";
            }

            /* function vypisPouzivatelov() {
              $sql = "SELECT id, email, heslo,pohlavie FROM pouzivatelia";
              $result = mysqli_query($this->conn, $sql);

              if (mysqli_num_rows($result) > 0) {
              // output data of each row
              while ($row = mysqli_fetch_assoc($result)) {
              echo "id: " . $row["id"] . " - E-Mail: " . $row["email"] . " " . $row["heslo"] . "Pohlavie>" . $row["pohlavie"] . "<br>";
              }
              } else {
              echo "0 results";
              }
              } */

            function findBiggestId() {
                $sql = "SELECT id FROM pouzivatelia";
                $result = mysqli_query($this->conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        if ($row["id"] > $this->biggestId) {
                            $this->biggestId = $row["id"];
                        }
                    }
                }
            }

            function registruj() {

                $this->biggestId+=1;

                $mail = htmlspecialchars($_POST["email"]);
                $heslo = htmlspecialchars($_POST["heslo"]);
                $pohlavie = htmlspecialchars($_POST["pohlavie"]) == "muž" ? "M" : "Z";
                $hash = password_hash($heslo, PASSWORD_BCRYPT);
                $sql1 = "SELECT * FROM pouzivatelia WHERE email='$mail'";
                $result1 = $this->conn->query($sql1);
                $existujeMail = false;
                if (@$result1->num_rows == 0) {
                    $sql = sprintf("INSERT INTO pouzivatelia 
                        VALUES ($this->biggestId, '%s', '%s','%s','0000-00-00 00:00:00.000000',false,false)",
                            $this->conn->real_escape_string($mail),
                            $this->conn->real_escape_string($hash),
                            $this->conn->real_escape_string($pohlavie));

                    if ($this->conn->query($sql) === TRUE) {
                        echo "Regsistracia bola uspesna. Mozete sa prihlasit.";
                        header('Location: RegistraciaUspesna.php');
                        $_SESSION["uspReg"] = true;
                    } else {
                        echo "<div>Uz ste sa v minulosti zaregistrovali. Zadajte znova heslo.</div>"
                        . "Ak sa vam nedari prihlasit kontaktujte administratora pre obnovenie hesla.</div>";
                    }
                }
                else {
                        echo "<div>Uz ste sa v minulosti zaregistrovali. Zadajte znova heslo.</div>"
                        . "Ak sa vam nedari prihlasit kontaktujte administratora pre obnovenie hesla.</div>";
                    }
            }

            function ukonciPristup() {
                $this->conn->close();
            }

        }

        $pocetError = 0;
        $heslo = $potvrdzHeslo = $pohlavie = $email = "";
        $hesloErr = $potHesloErr = $pohlavieErr = $emailErr = "";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $pocetError = 0;


            if (empty($_POST["heslo"])) {
                $hesloErr = "Heslo musí byť zadané";
                $pocetError++;
            } else {
                $heslo = test_input($_POST["heslo"]);
                if (!preg_match("/^[a-zA-Z0-9]*$/", $heslo)) {
                    $hesloErr = "Medzery nie su povolene";
                    $pocetError++;
                }
            }


            if (empty($_POST["potvrdHeslo"])) {
                $potHesloErr = "Potvrdzovacie heslo musí byť zadané";
                $pocetError++;
            } else {
                $potvrdzHeslo = test_input($_POST["potvrdHeslo"]);
                if (!preg_match("/^[a-zA-Z0-9]*$/", $potvrdzHeslo)) {
                    $potHesloErr = "Medzery nie su povolene";
                    $pocetError++;
                }
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email musí byť zadaný";
                $pocetError++;
            } else {
                $email = test_input($_POST["email"]);
                if (!preg_match("/^[@a.-zA-Z0-9]*$/", $email)) {
                    $emailErr = "Medzery nie su povolene";
                    $pocetError++;
                }
            }

            if (empty($_POST["pohlavie"])) {
                $pohlavieErr = "Pohlavie musí byť zadané";
                $pocetError++;
            } else {
                $pohlavie = test_input($_POST["pohlavie"]);
            }

            if ($_POST["heslo"] != $_POST["potvrdHeslo"] && $hesloErr == "" && $potHesloErr == "") {
                $hesloErr = "Hesla sa nezhoduju.";
                $potHesloErr = "Hesla sa nezhoduju.";
                $pocetError++;
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        if ($pocetError == 0 && !empty($_POST["email"]) && !empty($_POST["heslo"]) && !empty($_POST["potvrdHeslo"]) && !empty($_POST["pohlavie"])) {
            $DB = new DBPraca();

            $DB->findBiggestId();
            $DB->registruj();
            $DB->ukonciPristup();
        }
        ?>



        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"> 
            <span>E-mail:</span> <input type="text" name="email" value="<?php echo $email; ?>" required>
            <span class="error">* <?php echo $emailErr; ?></span>
            <br><br>
            <span>Heslo:</span> <input type="text" name="heslo" required>
            <span class="error">* <?php echo $hesloErr; ?></span>
            <br><br>
            <span>Potvrdenie hesla:</span> <input type="text" name="potvrdHeslo" required>
            <span class="error">* <?php echo $potHesloErr; ?></span>
            <br><br>
            <span>Pohlavie:</span>
            <input type="radio" name="pohlavie" value="žena" checked required>Žena
            <input type="radio" name="pohlavie" value="muž">Muž
            <span class="error">* <?php echo $pohlavieErr; ?></span>
            <br><br>
            <input type="submit" name="submit" value="Zaregistrovať"> 

        </form>





    </body>
</html>

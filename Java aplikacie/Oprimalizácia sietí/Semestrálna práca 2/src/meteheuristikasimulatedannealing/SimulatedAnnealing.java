/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meteheuristikasimulatedannealing;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Dominik
 */
public class SimulatedAnnealing {

    private DataLoader aData;
    private List<Integer> aBestFindedSolutionTP;
    private List<Integer> aCurrentSolution;//xi
    private List<Integer> aNewSolutionAfterExchange;
    private int aMaxCountOfExploreTransitions;
    private int aCountOfExploreTransitionsToFindThis;
    private int aMaxCountOfTransitionsFromTempCHange;
    private int aCountOfTransitionsFromTempChange;
    private int aActualTemperature;
    private double aTempChangeCoef;//between 0 1
    
    private int aCountOfAllTransitionsBetterSolution;//Okolie som zmenil lebo po prechode mal UF lepsiu hodnotu ako predchadzajuca
    private int aCountOfAllTransitionsRandom;//Okolie som zmenil na zaklade nahodneho pokusu

    SimulatedAnnealing(DataLoader paDataLoader, List<Integer> paStartSolution) {
        aBestFindedSolutionTP = paStartSolution;
        aData = paDataLoader;
        aMaxCountOfExploreTransitions = 10;
        aMaxCountOfTransitionsFromTempCHange = 10;
        aActualTemperature = 100;
        aCurrentSolution = new LinkedList<>(aBestFindedSolutionTP);
        aNewSolutionAfterExchange = new LinkedList<>();
        aTempChangeCoef = 2;
        aCountOfAllTransitionsBetterSolution=0;
        aCountOfAllTransitionsRandom=0;
    }

    public void setTemperature(int paTemperature) {
        aActualTemperature = paTemperature;
    }

    public void setaTempChangeCoef(double aTempChangeCoef) {
        this.aTempChangeCoef = aTempChangeCoef;
    }

    public String start() {
        String ret = "";
        System.out.println("\n\n\n\n\n\n-----------------------------------Simulated annealing start-----------------------------");
        System.out.println("HODNOTA UF NA ZACIATKU: " + getSolutionValue(aBestFindedSolutionTP));
        ret += "Hodnota UF po inicializacii: " + getSolutionValue(aBestFindedSolutionTP) + "\n";
        boolean koniec = false;
        aCountOfExploreTransitionsToFindThis = 0;
        aCountOfTransitionsFromTempChange = 0;
        do {
            //krok 2
            int[] exchage = exploreWay();
            //System.out.println("Budem vymienat: " + aData.getNameOfCity(exchage[0]) + " z " + aData.getNameOfCity(exchage[1]));
            aCountOfExploreTransitionsToFindThis++;
            aCountOfTransitionsFromTempChange++;
            if (aCountOfTransitionsFromTempChange == aMaxCountOfTransitionsFromTempCHange) {//ak sa ma zmenit teplota, tak ju zmen a reset pocitadlo ktore urcuje zmen t
                aActualTemperature = (int) ((double) aActualTemperature / (1 + aActualTemperature * aTempChangeCoef));
                aCountOfTransitionsFromTempChange = 0;
                //System.out.println("Teplota: "+aActualTemperature);
            }
            changeTwoCitiesAndMakeDecisoinToExchange(exchage);
            //krok 5
            if (aCountOfExploreTransitionsToFindThis == aMaxCountOfTransitionsFromTempCHange) {
                koniec = true;
            }
            

        } while (!koniec);
        System.out.println("HODNOTA UF NA KONCI: " + getSolutionValue(aBestFindedSolutionTP));
        ret += "Hodnota UF po vykonani metaheuristiky: " + getSolutionValue(aBestFindedSolutionTP) + "\n";
        System.out.println("Okolie som zmenil pri zlepseni UF: "+aCountOfAllTransitionsBetterSolution+" krat");
        System.out.println("Okolie som zmenil pri nahodnom pokuse: "+aCountOfAllTransitionsRandom+" krat");
        System.out.println("Okolie som zmenil celkovo: "+(aCountOfAllTransitionsRandom+aCountOfAllTransitionsBetterSolution)+" krat");
        return ret;
    }

    private void changeTwoCitiesAndMakeDecisoinToExchange(int[] exchange) {
        //krok 3
        exchangeSolution(exchange[0], exchange[1]);
        double hodnotaUfPozmene = getSolutionValue(aNewSolutionAfterExchange);
        double hodnotaUfPredZmenou = getSolutionValue(aCurrentSolution);
        if (hodnotaUfPozmene <= hodnotaUfPredZmenou) {
            aCurrentSolution = aNewSolutionAfterExchange;
            aCountOfExploreTransitionsToFindThis = 0;
            aCountOfAllTransitionsBetterSolution++;
            if (getSolutionValue(aCurrentSolution) < getSolutionValue(aBestFindedSolutionTP)) {
                Collections.copy(aBestFindedSolutionTP, aCurrentSolution);
            }
        } else {
            //krok 4
            tryToDoWorstTransition();
        }
    }

    private void tryToDoWorstTransition() {
        double exponent = -(getSolutionValue(aNewSolutionAfterExchange) - getSolutionValue(aCurrentSolution)) / aActualTemperature;
        double tranProbability = Math.pow(Math.E, exponent);
        Random r = new Random();
        if (r.nextDouble() <= tranProbability) {
            aCurrentSolution = aNewSolutionAfterExchange;
            aCountOfExploreTransitionsToFindThis = 0;
            aCountOfAllTransitionsRandom++;
        }
    }

    /**
     * vypocitaj hodnotu UF
     * @param paRiesenie
     * @return 
     */
    private double getSolutionValue(List<Integer> paRiesenie) {
        double ret = 0;
        for (int i = 0; i < paRiesenie.size(); i++) {
            if (i != paRiesenie.size() - 1) {
                ret += aData.getDistance(paRiesenie.get(i), paRiesenie.get(i + 1));
            } else {
                ret += aData.getDistance(paRiesenie.get(i), 0);
            }
        }
        return ret;
    }

    private void exchangeSolution(int i, int j) {
        aNewSolutionAfterExchange = new LinkedList<>(aCurrentSolution);
        int pom = aNewSolutionAfterExchange.get(i);
        aNewSolutionAfterExchange.set(i, aNewSolutionAfterExchange.get(j));
        aNewSolutionAfterExchange.set(j, pom);
    }

    //preskumaj okolie
    private int[] exploreWay() {
        Random r = new Random();
        int vymena1;
        int vymena2;
        do {
            vymena1 = r.nextInt(aBestFindedSolutionTP.size());
            vymena2 = r.nextInt(aBestFindedSolutionTP.size());
        } while (vymena1 == vymena2);
        return new int[]{vymena1, vymena2};
    }
}

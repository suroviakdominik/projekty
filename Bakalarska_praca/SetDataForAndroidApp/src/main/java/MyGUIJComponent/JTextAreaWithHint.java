/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyGUIJComponent;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.text.Document;

/**
 * JTextArea, ktorej mozno nastavit HINT (prednastaveny text)
 * @author Dominik
 */
public class JTextAreaWithHint extends JTextArea implements FocusListener {

    private String aDefaultText = "Vložte popis vybraného zvieraťa";
    private boolean aJeUznejakyTextnacitany = false;
    private ZaznamenajZmeny aZaznemenavacZmien = new ZaznamenajZmeny();
    private boolean aBoliZmenyUlozene = true;

    public JTextAreaWithHint() {
        super();
        super.addFocusListener(this);
        super.addKeyListener(aZaznemenavacZmien);
        super.setText(aDefaultText);
    }

    public JTextAreaWithHint(String text) {
        super(text);
        super.addFocusListener(this);
        super.addKeyListener(aZaznemenavacZmien);
        aDefaultText=text;
        super.setText(aDefaultText);
    }

    public JTextAreaWithHint(int rows, int columns) {
        super(rows, columns);
        super.addFocusListener(this);
        super.addKeyListener(aZaznemenavacZmien);
        super.setText(aDefaultText);
    }

    public JTextAreaWithHint(String text, int rows, int columns) {
        super(text, rows, columns);
        super.addFocusListener(this);
        super.addKeyListener(aZaznemenavacZmien);
        super.setText(aDefaultText);
    }

    public JTextAreaWithHint(Document doc) {
        super(doc);
        super.addFocusListener(this);
        super.addKeyListener(aZaznemenavacZmien);
        super.setText(aDefaultText);
    }

    public JTextAreaWithHint(Document doc, String text, int rows, int columns) {
        super(doc, text, rows, columns);
        super.addKeyListener(aZaznemenavacZmien);
        super.addFocusListener(this);
        super.setText(aDefaultText);
    }

    /**
     * 
     * @return  HINT (defaultny text)
     */
    public String getDefaultText() {
        return aDefaultText;
    }

    /**
     * 
     * @return true ak zadany text uz bol ulozeny a mozno ho nahradit Hintorm 
     */
    public boolean boliZmenyUlozene() {
        return aBoliZmenyUlozene;
    }

    /**
     * 
     * @param aBoliZmenyUlozene true -zadanytext uz nie je potrebny, nahrad ho HINTOM
     */
    public void setBoliZmenyUlozene(boolean aBoliZmenyUlozene) {
        this.aBoliZmenyUlozene = aBoliZmenyUlozene;
    }

    /**
     * 
     * @param aDefaultText - nastav HINT(defaultny text)
     */
    public void setDefaultText(String aDefaultText) {
        this.aDefaultText = aDefaultText;
        this.setText(aDefaultText);
    }

    
    
    
    /**
     * v ramci text area zobrazi defaultny text bez ohadu
     * nato, ci bol zadany text ulozeny/spracovany
     */
    public void showCurrentDefaultText()
    {
        setText(aDefaultText);
    }

    /**
     * 
     * @param aJeUznejakyTextnacitany true- ak bol text zadany, aby sa po odideni
     *                                      nenahradil HINTOM
     */
    public void setJeUznejakyTextnacitany(boolean aJeUznejakyTextnacitany) {
        this.aJeUznejakyTextnacitany = aJeUznejakyTextnacitany;
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (!aJeUznejakyTextnacitany) {
            setText("");
        } else {

        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (!aJeUznejakyTextnacitany||getText().equals("")) {
            setText(aDefaultText);
        } 
    }

    private class ZaznamenajZmeny implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(!e.isControlDown() ||(e.isControlDown()&& e.getKeyCode() != KeyEvent.VK_V))
            aBoliZmenyUlozene = false;
            aJeUznejakyTextnacitany=true;
            if(!e.isControlDown() ||(e.isControlDown()&& e.getKeyCode() != KeyEvent.VK_C))
            aBoliZmenyUlozene = true;
            aJeUznejakyTextnacitany=true;
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }

    }
}

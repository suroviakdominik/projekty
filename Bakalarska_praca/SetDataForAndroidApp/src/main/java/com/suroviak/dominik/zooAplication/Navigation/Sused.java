/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.Serializable;
import java.util.Objects;

/**
 * Instancia tejto triedy je obalom bodu typu BodNaCeste, ktory
 * je susedom niektoreho z bodov
 * @author Dominik
 */
public class Sused implements Serializable
    {
        private static final long serialVersionUID = 6L;
        private double aVzdialenost;
        private BodNaCeste aSusednyBod;

        /**
         * vytvor noveho suseda, ktory je reprezentovany bodom zadanym ako param
         * @param aSusednyBod - bod, ktory sused predstavuje
         */
        public Sused(BodNaCeste aSusednyBod) {
            
            this.aSusednyBod = aSusednyBod;
        }
        
        /**
         * nastav vzdialenost suseda od bodu, ktoreho je susedom
         * @param paVzdialenost 
         */
        public void setVzdialenost(double paVzdialenost)
        {
            aVzdialenost=paVzdialenost;
        }

        /**
         * 
         * @return vzdialenost suseda od bodu, ktoreho je susedom
         */
        public double getVzdialenost() {
            return aVzdialenost;
        }

        /**
         * 
         * @return bod, z ktoreho sa sklada sused
         */
        public BodNaCeste getSusednyBod() {
            return aSusednyBod;
        }

        

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 23 * hash + (int) (Double.doubleToLongBits(this.aVzdialenost) ^ (Double.doubleToLongBits(this.aVzdialenost) >>> 32));
            hash = 23 * hash + Objects.hashCode(this.aSusednyBod);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Sused other = (Sused) obj;
            
            return Objects.equals(this.aSusednyBod, other.aSusednyBod);
        }
        
        

        @Override
        public String toString() {
            return aSusednyBod+", Vzdialenost: "+aVzdialenost+" m";
        }
        
        
    }
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heuristika;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;

/**
 *
 * @author Dominik
 */
public class GUI extends JFrame {

    private Batoh aBatoh;
    private Heuristika aHeuristika;
    private int aXUmiestnenieOkna;
    private int aYUmiestnenieOkno;
    private int aVyskaOkna;
    private int aSirkaOkna;

    public GUI() {
        //super("Primárne Heuristiky");
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension d = new Dimension(t.getScreenSize());
        aBatoh = new Batoh();
        
        if(60 * d.width / 100>600&&60 * d.height / 100>500)
        {
           this.setSize(60 * d.width / 100, 60 * d.height / 100);
           Dimension dMin = new Dimension(600, 500);
           this.setMinimumSize(dMin);
        }
        else
        {
            this.setSize(100 * d.width / 100, 100 * d.height / 100);
        }
        this.setLocation(50 * d.width / 100 - 60 * d.width / 200, 50 * d.height / 100 - 60 * d.height / 200);
        
        
        aXUmiestnenieOkna = 50 * d.width / 100 - 60 * d.width / 200;
        aYUmiestnenieOkno = 50 * d.height / 100 - 60 * d.height / 200;
        aSirkaOkna = this.getWidth();
        aVyskaOkna = this.getHeight();
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {//Windows Adapter je anonymna trieda                                 
            @Override
            public void windowClosing(WindowEvent e) {
                akcia_Koniec();
            }   //Windows Event je anonymna trieda              
        });

        this.addComponentListener(new ComponentAdapter() {
            public void componentMoved(ComponentEvent e) {
                nastavUmiestnenieOkna();
                
            }
        });
        
        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                nastavUmiestnenieOkna();
                nastavVelkostOkna();
            }
        });
        this.setLayout(new BorderLayout(2, 2));
        this.add(BorderLayout.CENTER, new Platno(aBatoh));
        //this.setLayout(new FlowLayout(0));
       
        inicializujTlacidla();
        this.setTitle("Primárna heuristika");
        this.setResizable(true);
        this.setVisible(true);
        this.toFront();
    }

    private void nastavUmiestnenieOkna() {
        Point p = new Point();
        p = this.getLocation();
        aXUmiestnenieOkna = p.x;
        aYUmiestnenieOkno = p.y;
        //System.out.println("PozX: "+p.x+" PozY: "+p.y);
    }

    private void nastavVelkostOkna()
    {
        Dimension d = this.getSize();
        aSirkaOkna = d.width;
        aVyskaOkna = d.height;
        //System.out.println("sirka: "+d.width+" vyska: "+d.height);
        
    }
    

    private void inicializujTlacidla() 
    {
        JPanel pnLista = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //  lista bude hore
        pnLista.setBackground(Color.GRAY);
        this.add(BorderLayout.NORTH, pnLista);
        JButton btKoniec = new JButton("Koniec");
        btKoniec.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_Koniec();
            }
        });

        pnLista.add(btKoniec);

        JButton btZadKapacitu = new JButton("Vytvor batoh");
        btZadKapacitu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_vytvorBatoh();
            }
        });

        pnLista.add(btZadKapacitu);
        
        JButton btSpustiHeuristikuZVyhodnymiKoeficientami = new JButton("Primárna heuristika Výh.Koef");
        btSpustiHeuristikuZVyhodnymiKoeficientami.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_primHeurVyhKoef();
            }
        });

        pnLista.add(btSpustiHeuristikuZVyhodnymiKoeficientami);
        
        JButton btVylepsiHeuristikuSVyHKoef = new JButton("Výmenná heuristika(prvý vhodný)");
        btVylepsiHeuristikuSVyHKoef.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_vymHeuristikaPrvVhodny();
            }
        });

        pnLista.add(btVylepsiHeuristikuSVyHKoef);
        
        
          JButton btzaPisVysledokDoSuboru = new JButton("Zápíš výsledok do dúboru");
        btzaPisVysledokDoSuboru.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                akcia_zapisDoSuboru();
            }
        });

        pnLista.add(btzaPisVysledokDoSuboru);
    }

    
    
    private void akcia_Koniec() {
        if (JOptionPane.showConfirmDialog(this,
                "Chcete skutočne ukončit program", "Ukončenie",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION) {
            //  this.dispose(); 
            System.exit(0);
        }
    }
    
    private void akcia_zapisDoSuboru()
    {
        try {
            aHeuristika.zapisVysledokDoSuboru();
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Súbor sa nepodarilo vytvoriť.",
                            "Chyba -Uloženie výsledku", +JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
           JOptionPane.showMessageDialog(null, "Do súboru sa nepodarilo zapísať.",
                            "Chyba -Uloženie výsledku", +JOptionPane.ERROR_MESSAGE);
        }
        catch(NullPointerException ex)
        {
            JOptionPane.showMessageDialog(null, "Nie je co ulozit. Heuristika zatial neprebehla",
                            "Chyba -Uloženie výsledku", +JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void akcia_primHeurVyhKoef()
    {
        if(aBatoh.jeInicializovany())
        {
            try {
                aBatoh.setaMaxPocetPredmetovVBatohu(aBatoh.getaMaxPocetPredmetovVBatohu());
                aHeuristika=new Heuristika(aBatoh);
                aHeuristika.primHeuristikaVyhKoeficeinty();
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Súbor s údajmi predmetov, ktoré možno vložiť"+"\n"
                        +"do batohu, nebol nájdený. Ceny musia byť uložené v súbore H5_c.txt"+"\n"
                        +"a hmotnosti v súbore H5_a.txt. Tieto súbory sa musia nachádzať v zložke programu",
                            "Chyba -Otvorenie súboru", +JOptionPane.ERROR_MESSAGE);
            }
            catch(NoSuchElementException ex)
            {
                JOptionPane.showMessageDialog(null, "Zadaný počet predmetov, z ktorých možno naplniť batoh presahuje rozsah súboru"
                        + "zo vstupnými údajmi týhchto predmetov",
                            "Chyba -Počet údajov v súboroch je príliš malý", +JOptionPane.ERROR_MESSAGE);
            }
            
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Pre spustenim heuristiky je nutne vytvorit batoh",
                            "Chyba -Nevytvorený batoh", +JOptionPane.ERROR_MESSAGE);
        }
        this.repaint();
    }

     private void akcia_vymHeuristikaPrvVhodny()
     {
         if(aHeuristika!=null && aHeuristika.prebehlaHeuristikaVyhKoef())
         {
             try {
                 aHeuristika.primHeuristikaVymenna();
             } catch (FileNotFoundException ex) {
                 Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
         else
         {
             JOptionPane.showMessageDialog(null, "Pre spustenim vymennej heuristiky je nutne spustit heuristiku s vyh. koef."
                     +"\n"+"Jej výsledok poslúži ako vstup pre výpočet výmennej heuristiky.",
                            "Chyba -Vymenna heuristika", +JOptionPane.ERROR_MESSAGE);
         }
         this.repaint();
     }
    
    private void akcia_vytvorBatoh() {
        int lpKapacitaVKg;
        int lpMaxPocetPrvkov;
        
        final JDialog ramik = new JDialog();
        ramik.setLocation(aXUmiestnenieOkna+aSirkaOkna/2-45*aSirkaOkna/200,aYUmiestnenieOkno+aVyskaOkna/2-20*aVyskaOkna/200);
        ramik.setSize(45*aSirkaOkna/100, 20*aVyskaOkna/100);
        ramik.setResizable(false);
        ramik.setTitle("Batoh vytvorenie");

        ramik.addWindowListener(new WindowAdapter() {//Windows Adapter je anonymna trieda                                 
            @Override
            public void windowClosing(WindowEvent e) {
                ramik.dispose();
            }   //Windows Event je anonymna trieda              
        });

        ramik.setLayout(new GridLayout(4, 2, 5, 10));

        //	so  vstupnými poľami
        final JTextField txtMaxPocetPrvkov = new JTextField();
        final JTextField txtKapacitaVKg = new JTextField();
        final JTextField txtZKoklkychVyberame = new JTextField();

        // 	a dvoma tlačidlami
        JButton btUlozit = new JButton("Vytvoriť");
        btUlozit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    aBatoh.setaMaxPocetPredmetovVBatohu(Integer.parseInt(txtMaxPocetPrvkov.getText().trim()));
                    if (aBatoh.getaMaxPocetPredmetovVBatohu() < 2 ||(aBatoh.getaMaxPocetPredmetovVBatohu()>600)) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný údaj / MaxPocPredm musí byť vacsi ako 1/");
                    }
                }
                catch(IllegalArgumentException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(),
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtMaxPocetPrvkov.requestFocus();
                    return;
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Nevyplnené polia alebo zle zadaný údaj v poli"+"\n"+ex,
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtMaxPocetPrvkov.requestFocus();
                    return;
                }
                

                try {
                    aBatoh.setaKapacita(Integer.parseInt(txtKapacitaVKg.getText().trim()));
                    if (aBatoh.getaKapacita() <1) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný údaj / Kapacita V Kg musí byť vacsia ako 0 /");
                    }
                } 
                catch(IllegalArgumentException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(),
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtKapacitaVKg.requestFocus();
                    return;
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Nevyplnené polia alebo zle zadaný údaj v poli"+"\n"+ex,
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtKapacitaVKg.requestFocus();
                    return;
                }
                
                try {
                    aBatoh.setaZKolkychPredmetovVyberame(Integer.parseInt(txtZKoklkychVyberame.getText().trim()));
                    if (aBatoh.getaZKolkychPredmetovVyberame() <aBatoh.getaMaxPocetPredmetovVBatohu()) {
                        throw new IllegalArgumentException(
                                "\n   neprípustný údaj / Pocet predmetov z ktorych vyberame musi byt vacsi ako maxPocetPredmetov v batohu /");
                    }
                } 
                catch(IllegalArgumentException ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage(),
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtZKoklkychVyberame.requestFocus();
                    return;
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Nevyplnené polia alebo zle zadaný údaj v poli"+"\n"+ex,
                            "Chybne zadaný údaj", +JOptionPane.ERROR_MESSAGE);
                    txtZKoklkychVyberame.requestFocus();
                    return;
                }


               

                
                ramik.dispose();

            }
        });

        JButton btStorno = new JButton("Storno");
        btStorno.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                ramik.dispose();

            }
        });

        //	a popisnými poľami,    a všetko poskladáme
        ramik.add(new JLabel("Maximalny Pocet Prvkov v batohu :  ", JLabel.RIGHT));
        ramik.add(txtMaxPocetPrvkov);
        ramik.add(new JLabel("Kapacita batohu v kg            :  ", JLabel.RIGHT));
        ramik.add(txtKapacitaVKg);
        ramik.add(new JLabel("Z kolkych predmetov mozno vybrat :  ", JLabel.RIGHT));
        ramik.add(txtZKoklkychVyberame);
        
        ramik.add(btUlozit);
        ramik.add(btStorno);

        ramik.setModal(true);
        ramik.setVisible(true);
        this.repaint();
    }
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GUI g = new GUI();
    }
}



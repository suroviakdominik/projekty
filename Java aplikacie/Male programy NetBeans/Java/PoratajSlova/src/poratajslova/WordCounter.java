/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poratajslova;



import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Hawk
 */
public class WordCounter {

    ArrayList<Word> words = new ArrayList<Word>();

    /*public void addNew(Word new){
        slova.add(nove);
    }*/

    //pri pridavani si overime, ci sa tam uz take nachadza, ak nie, tak pridame nove, ak ano, incrementujeme counter
    public void count(String word)
    {
       boolean flag = false;

       for(int i = 0;i<words.size();i++) {
           if ((words.get(i)).getText().equals(word)) {
               (words.get(i)).inc();
               flag = true;
           }
       }
       if (!flag)
       {
           Word w = new Word(word);
           words.add(w);
       }
    }

    //trieda Word implementuje comparable
    public void sort() {
        Collections.sort(words);
    }

    public void init() {
        words.removeAll(words);
    }

    public String getText() {
        this.sort();
        String buf = "Unique words together: " + words.size()+"\n";
        
        for(int i=0;i<words.size();i++) {
            buf+=words.get(i).toString();
        }
        
        return buf;
    }
}



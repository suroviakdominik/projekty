

public enum Smer
{
    SEVER,
    JUH,
    VYCHOD,
    ZAPAD;
   
   public Smer OpacnySmer()
   {
       switch(this)
       {
           case SEVER:return JUH;
           case JUH:return SEVER;
           case ZAPAD:return VYCHOD;
           case VYCHOD:return ZAPAD;
           default:return null;
        }  
    }
    
    public Smer DOPRAVA()
    {
        switch(this)
        {
          case SEVER:return ZAPAD;
          case JUH:return VYCHOD;
          case ZAPAD:return JUH;
          case VYCHOD:return SEVER;
          default:return null;
        }
    }
    
    public Smer DOLAVA()
    {
       switch(this)
        {
          case SEVER:return VYCHOD;
          case JUH:return ZAPAD;
          case ZAPAD:return SEVER;
          case VYCHOD:return JUH;
          default:return null;
        }
    }
}   
       
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frekvencnaanalyzapismen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public class FileWorker {
    public static List<Map.Entry<Character,Integer>> readDataFromFile(File f)
    {
        try {
            HashMap<Character,Integer> ret=new HashMap<>();
            Reader r= new InputStreamReader(new FileInputStream(f),"UTF-8");
            int readChar=-1;
            while((readChar=r.read())!=-1)
            {
                Character c=new Character((char)readChar);
                if(ret.get(c)!=null)
                {
                    ret.replace(c, ret.get(c)+1);
                }
                else
                {
                    ret.put(c, 1);
                }
            }
            Set s=ret.entrySet(); 
            List<Map.Entry<Character,Integer>> sortedList=new ArrayList(s);
            Collections.sort(sortedList, new Comparator<Map.Entry<Character, Integer>>(){

                @Override
                public int compare(Map.Entry<Character, Integer> t, Map.Entry<Character, Integer> t1) {
                    return t1.getValue().compareTo(t.getValue());
                }
                
            });
            return sortedList;
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void writeDataToFile(File f,String paData)
    {
        Writer w= null;
        try {
            w = new OutputStreamWriter(new FileOutputStream(f),"Windows-1250");
            ByteArrayInputStream bAIS=new ByteArrayInputStream(paData.getBytes());
            BufferedReader bR=new BufferedReader(new InputStreamReader(bAIS));
            BufferedWriter bW=new BufferedWriter(w);
            String loadedLine=null;
            while((loadedLine=bR.readLine())!=null)
            {
                bW.write(loadedLine);
                bW.newLine();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

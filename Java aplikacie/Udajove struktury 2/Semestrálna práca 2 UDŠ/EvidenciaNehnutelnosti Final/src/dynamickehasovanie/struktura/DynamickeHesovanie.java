/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamickehasovanie.struktura;

import com.sun.corba.se.spi.extension.CopyObjectPolicy;
import dynamickehasovanie.data.ImplementatorZaznam;
import dynamickehasovanie.data.Zaznam;
import dynamickehasovanie.triestrom.ExternyVrchol;
import dynamickehasovanie.triestrom.TrieStrom;
import dynamickehasovanie.triestrom.Vrchol;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Blob;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 *
 * @author Dominik
 * @param <Z>
 */
public class DynamickeHesovanie<Z extends Zaznam> {

    public static enum STAV_VKLADANIA {

        VLOZENY,
        NEVLOZENY,
        MODIFIKOVANY;
    }
    private static final String NAZOV_HLAVNEHO_SUBORU = "/hlavnySubor.bin";
    private static final String NAZOV_PREPL_SUBORU = "/preplnSubor.bin";
    private static final String NAZOV_TRIE_STROM_SUBOR = "/trie.txt";
    private static final String NAZOV_SUB_VOLNE_BLOKY_HL = "/volneBlokyHL.txt";
    private static final String NAZOV_SUB_VOLNE_BLOKY_PREPL = "/volneBlokyPrepl.txt";
    private static final String PRIECINOK_ZO_ZALOHOU = "/Zaloha";
    private static final String NAZOV_SUBORU_OBS_ZAKL_INFO = "/dh_info";
    private final String PRIECINOK_ZO_SUBORMI;
    private final int aMaxPocetZaznamovVbloku;
    private final ImplementatorZaznam aImplementator;
    private TrieStrom aTrieStrom;
    public FileManager aFilemanagerHlavnehoSuboru;
    public FileManager aFileManagerPreplnSuboru;
    private long aAktualnyPocetZaznamovVStrukture;

    public DynamickeHesovanie(int paMaxPocetZaznamovVBloku, ImplementatorZaznam paImplementator, int paMaxHlbka, File paDataDirectory) {
        PRIECINOK_ZO_SUBORMI = paDataDirectory.getAbsolutePath();
        if (!paDataDirectory.isFile() || !paDataDirectory.exists()) {
            paDataDirectory.mkdirs();
            new File(PRIECINOK_ZO_SUBORMI + PRIECINOK_ZO_ZALOHOU).mkdir();
        }
        this.aTrieStrom = new TrieStrom(paMaxHlbka);
        this.aMaxPocetZaznamovVbloku = paMaxPocetZaznamovVBloku;
        this.aImplementator = paImplementator;
        this.aAktualnyPocetZaznamovVStrukture = 0;
        inicializaciaPriVytvaraniNovehoDH();
        ulozDataAktualnyStavStruktury();
    }

    private void inicializaciaPriVytvaraniNovehoDH() {
        File f = new File(PRIECINOK_ZO_SUBORMI + NAZOV_HLAVNEHO_SUBORU);
        File preplF = new File(PRIECINOK_ZO_SUBORMI + NAZOV_PREPL_SUBORU);
        File volneBlHl = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_HL);
        File volneBlPrepl = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_PREPL);
        File trieStrom = new File(PRIECINOK_ZO_SUBORMI + NAZOV_TRIE_STROM_SUBOR);
        File zakl_info = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUBORU_OBS_ZAKL_INFO);
        f.delete();
        preplF.delete();
        volneBlHl.delete();
        volneBlPrepl.delete();
        trieStrom.delete();
        zakl_info.delete();
        this.aFilemanagerHlavnehoSuboru = new FileManager(f);
        this.aFileManagerPreplnSuboru = new FileManager(preplF);
    }

    public DynamickeHesovanie(ImplementatorZaznam paImplementator, File paDataDirectory) throws IOException {
        PRIECINOK_ZO_SUBORMI = paDataDirectory.getAbsolutePath();
        if (!paDataDirectory.isFile() || !paDataDirectory.exists()) {
            paDataDirectory.mkdirs();
            new File(PRIECINOK_ZO_SUBORMI + PRIECINOK_ZO_ZALOHOU).mkdir();
        }
        this.aImplementator = paImplementator;
        aMaxPocetZaznamovVbloku = obnovDataNaposledyUlozeneDH();
    }

    private int obnovDataNaposledyUlozeneDH() throws FileNotFoundException, IOException {
        File f = new File(PRIECINOK_ZO_SUBORMI + NAZOV_HLAVNEHO_SUBORU);
        File preplF = new File(PRIECINOK_ZO_SUBORMI + NAZOV_PREPL_SUBORU);
        File volneBlHl = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_HL);
        File volneBlPrepl = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_PREPL);
        File trieStromF = new File(PRIECINOK_ZO_SUBORMI + NAZOV_TRIE_STROM_SUBOR);
        File zakl_info = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUBORU_OBS_ZAKL_INFO);
        this.aFilemanagerHlavnehoSuboru = new FileManager(f);
        this.aFileManagerPreplnSuboru = new FileManager(preplF);
        this.aFilemanagerHlavnehoSuboru.nacitajVolneBlokyZoSuboru(volneBlHl);
        this.aFileManagerPreplnSuboru.nacitajVolneBlokyZoSuboru(volneBlPrepl);
        this.aTrieStrom = TrieStrom.vytvorTrieStromZoSuboru(trieStromF);
        FileWorker fw = new FileWorker(zakl_info);
        fw.startRead();
        String max_poc_zazn = fw.readNextLine();
        aAktualnyPocetZaznamovVStrukture = Long.parseLong(fw.readNextLine());
        fw.stopRead();
        return Integer.parseInt(max_poc_zazn);
    }

    public Zaznam hladaj(Z paZaznam) {
        Zaznam najdeny;
        if (aTrieStrom.getKoren() == null) {
            return null;
        }
        ExternyVrchol v = aTrieStrom.traverzuj(paZaznam.hashCode());
        if (v.getaCisloBloku() < 0) {
            return null;
        }
        Blok b = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        b.setaCisloBloku(v.getaCisloBloku());
        b.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(b));
        if (b.existujeZaznam(paZaznam)) {
            najdeny = b.getZaznam(paZaznam);
            if (najdeny == null) {
                throw new IllegalStateException("ERROR: Zaznam existuje ale metoda getZaznam vratila null");
            }
            return najdeny;
        }
        long cisloZretBLoku;
        while ((cisloZretBLoku = b.getaCisloZretazenehoBloku()) >= 0) {
            b.reset();
            b.setaCisloBloku(cisloZretBLoku);
            b.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(b));
            if (b.existujeZaznam(paZaznam)) {
                najdeny = b.getZaznam(paZaznam);
                if (najdeny == null) {
                    throw new IllegalStateException("ERROR: Zaznam existuje ale metoda getZaznam vratila null");
                }
                return najdeny;
            }
        }
        return null;
    }

    /**
     *
     * @param paZaznam - vkladany zaznam
     * @param paPrepisatAkSaNajde - ci ho prepisat ak sa najde -- POZOR:
     * prepisovat mozno len neklucove atributy
     *
     * @return true - zaznam sa podarilo vlozit alebo modifikovat, ak je to
     * dovolene false- zaznam sa nepodarilo vlozit, lebo nie je povolene
     * modifikovanie a nasiel sa
     */
    public STAV_VKLADANIA vloz(Z paZaznam, boolean paPrepisatAkSaNajde) {
        //koren neexistuje
        Blok pridavamDoBloku = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        if (aTrieStrom.getKoren() == null) {
            aTrieStrom.setKoren(new ExternyVrchol(0, null,
                    aFilemanagerHlavnehoSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(pridavamDoBloku.getVelkostBlokuVBajtoch()), 1));
            pridavamDoBloku.vlozZaznam(paZaznam);
            pridavamDoBloku.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
            pridavamDoBloku.setaCisloBloku(((ExternyVrchol) (aTrieStrom.getKoren())).getaCisloBloku());
            aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(pridavamDoBloku);
            aAktualnyPocetZaznamovVStrukture++;
            return STAV_VKLADANIA.VLOZENY;
        } else {
            ExternyVrchol externyTraverzovanim = aTrieStrom.traverzuj(paZaznam.hashCode());
            if (externyTraverzovanim.getaCisloBloku() >= 0) {
                pridavamDoBloku.setaCisloBloku(externyTraverzovanim.getaCisloBloku());
                pridavamDoBloku.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(pridavamDoBloku));
            } else {//alokuj externy vrcholo vloz -> koniec
                long cisloNovAlokBloku = aFilemanagerHlavnehoSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(pridavamDoBloku.getVelkostBlokuVBajtoch());
                pridavamDoBloku.setaCisloBloku(cisloNovAlokBloku);
                pridavamDoBloku.vlozZaznam(paZaznam);
                pridavamDoBloku.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
                externyTraverzovanim.setaCisloBloku(cisloNovAlokBloku);
                externyTraverzovanim.zvysAktualnyPocetZaznamov();
                aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(pridavamDoBloku);
                aAktualnyPocetZaznamovVStrukture++;
                return STAV_VKLADANIA.VLOZENY;
            }
            //*******
            if (pridavamDoBloku.existujeZaznam(paZaznam)) {
                if (paPrepisatAkSaNajde) {//ak sa nasiel a je povolena modifikacia -> modifikuj
                    pridavamDoBloku.odstranZaznam(paZaznam);
                    pridavamDoBloku.vlozZaznam(paZaznam);
                    aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(pridavamDoBloku);
                    return STAV_VKLADANIA.MODIFIKOVANY;
                }
                return STAV_VKLADANIA.NEVLOZENY;
            }
            pridavamDoBloku.vlozZaznam(paZaznam);
            if (!pridavamDoBloku.jeBlokPreplneny()) {
                aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(pridavamDoBloku);
                externyTraverzovanim.zvysAktualnyPocetZaznamov();
                aAktualnyPocetZaznamovVStrukture++;
                return STAV_VKLADANIA.VLOZENY;
            } else {
                if (externyTraverzovanim.getaHlbkaVrchola() != aTrieStrom.getMAXIMALNA_HLBKA_TRIE_STROMU()) {//mozeme urobit z externeho interny
                    TrieStrom.ExterneVrcholyVznikTrasfExtNaInt noveExtVrch;
                    do {
                        noveExtVrch = aTrieStrom.vytvor_Z_Ext_Vrchola_Int_a_Pridaj_Mu_Externe(externyTraverzovanim, pridavamDoBloku);
                        if (noveExtVrch.dosloZnovaKPreplneniu()) {
                            externyTraverzovanim = noveExtVrch.getPreplnenyExternyVrchol();
                        };
                    } while (noveExtVrch.dosloZnovaKPreplneniu()
                            && noveExtVrch.getPreplnenyExternyVrchol().getaHlbkaVrchola() < aTrieStrom.getMAXIMALNA_HLBKA_TRIE_STROMU());
                    if (!noveExtVrch.dosloZnovaKPreplneniu()) {//ak po rozdeleni na nove externe nedoslo k preplneniu -> zapis 2 externe Koniec
                        aFilemanagerHlavnehoSuboru.pridajNovyVolnyBlok(externyTraverzovanim.getaCisloBloku());
                        long volnyBlok = aFilemanagerHlavnehoSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(pridavamDoBloku.getVelkostBlokuVBajtoch());
                        noveExtVrch.getaBlokVLavomExtVrchole().setaCisloBloku(volnyBlok);
                        noveExtVrch.getaLavyExternyVrchol().setaCisloBloku(volnyBlok);
                        aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(noveExtVrch.getaBlokVLavomExtVrchole());
                        volnyBlok = aFilemanagerHlavnehoSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(pridavamDoBloku.getVelkostBlokuVBajtoch());
                        noveExtVrch.getaBlokVPracomExternomVrchole().setaCisloBloku(volnyBlok);
                        noveExtVrch.getaPravyExternyVrchol().setaCisloBloku(volnyBlok);
                        aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(noveExtVrch.getaBlokVPracomExternomVrchole());
                        aAktualnyPocetZaznamovVStrukture++;
                        return STAV_VKLADANIA.VLOZENY;
                    } else {
                        STAV_VKLADANIA stavVkl = vkladamDoPreplnovacieho(noveExtVrch.getaPreplnenyBlok(), paPrepisatAkSaNajde);
                        if (stavVkl != STAV_VKLADANIA.VLOZENY) {
                            externyTraverzovanim.znizAktualnyPocetZaznamov();
                        }
                        return stavVkl;
                    }
                } else {
                    STAV_VKLADANIA stavVkl = vkladamDoPreplnovacieho(pridavamDoBloku, paPrepisatAkSaNajde);
                    if (stavVkl == STAV_VKLADANIA.VLOZENY) {
                        externyTraverzovanim.zvysAktualnyPocetZaznamov();
                    }
                    return stavVkl;
                }
            }
        }
    }

    /**
     *
     * @param paPreplnenyBlok
     * @param paPrepisatAkSaNajde
     * @return true - podarilo sa vlozit do preplnovacieho alebo prepisat
     * existujuci zaznam ak je to povolene(hodnota posl. param) false -
     * neporadrilo sa vlozit zaznam do preplnujuceho suboru lebo uz existuje a
     * modifikacia nie je povolena
     */
    private STAV_VKLADANIA vkladamDoPreplnovacieho(Blok paPreplnenyBlok, boolean paPrepisatAkSaNajde) {
        Zaznam zaznamKtoryPreplnujeBlok = paPreplnenyBlok.odstranPoslednePridanyZaznam();
        boolean jeUzPreplBlokAlokovanyVSuborePreplBlokov = true;
        if (paPreplnenyBlok.getaCisloZretazenehoBloku() < 0) {//ak nemal externy este ziadneho v preplnovacom, tak nastav externemu odkaz na noveho nasledovnika v preplnovacom a externy zapis(zmenilo sa cislo zretazeneho bloku)
            long cisloBlokuVPreplSub = aFileManagerPreplnSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(paPreplnenyBlok.getVelkostBlokuVBajtoch());
            paPreplnenyBlok.setCisloZretazenehoBloku(cisloBlokuVPreplSub);
            aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(paPreplnenyBlok);
            jeUzPreplBlokAlokovanyVSuborePreplBlokov = false;
        }

        long cisloBlokuVPreplSubore = paPreplnenyBlok.getaCisloZretazenehoBloku();
        Blok paPrvyBlokVPreplSubKdeMoznoVlozit = new Blok(aMaxPocetZaznamovVbloku, aImplementator, true);//musim prejst vsetky aby som zistil ci neexistuje, ale ak po prejdenu zistion ze ho mozno vlozit, tak sa vlozi do 1. volneho
        do {
            cisloBlokuVPreplSubore = zapisPreplnujuciBlok(cisloBlokuVPreplSubore,
                    zaznamKtoryPreplnujeBlok,
                    jeUzPreplBlokAlokovanyVSuborePreplBlokov,
                    paPrvyBlokVPreplSubKdeMoznoVlozit, paPrepisatAkSaNajde);
        } while (cisloBlokuVPreplSubore >= 0);
        if (cisloBlokuVPreplSubore == -2) {
            return STAV_VKLADANIA.NEVLOZENY;
        } else if (cisloBlokuVPreplSubore == -3) {
            return STAV_VKLADANIA.MODIFIKOVANY;
        }
        return STAV_VKLADANIA.VLOZENY;
    }

    /**
     *
     * @param paCisloBlokuVpreplSubore
     * @param paVkladanyZaznam
     * @return -1 ak sa podarilo vlozit inak vrati cislo dalsieho bloku v
     * preplnovacom subore -2 ak sa nepodarilo vlozit lebo tam uz taky je a
     * prepisanie je zakazane -3 - je ppovolena modifikacia a bol modifikovany
     */
    private long zapisPreplnujuciBlok(long paCisloBlokuVpreplSubore, Zaznam paVkladanyZaznam, boolean jeUzAlokovanyVSubore, Blok paPrvyVolnyBlokVpreplSub, boolean paPrepisatAkSaNajde) {
        Blok preplBlok = new Blok(aMaxPocetZaznamovVbloku, aImplementator, true);
        preplBlok.setaCisloBloku(paCisloBlokuVpreplSubore);
        if (jeUzAlokovanyVSubore) {//ak uz je alokovany v subore prepl
            preplBlok.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(preplBlok));
        } else {
            preplBlok.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
            preplBlok.vlozZaznam(paVkladanyZaznam);
            aFileManagerPreplnSuboru.zapisBlokNaDisk(preplBlok);
            aAktualnyPocetZaznamovVStrukture++;
            return -1;
        }

        if (preplBlok.existujeZaznam(paVkladanyZaznam)) {//ak je povolena modifikacia a nasiel sa -> modifikuj
            if (paPrepisatAkSaNajde) {
                preplBlok.odstranZaznam(paVkladanyZaznam);
                preplBlok.vlozZaznam(paVkladanyZaznam);
                aFileManagerPreplnSuboru.zapisBlokNaDisk(preplBlok);
                return -3;
            }
            return -2;// nepodarilo sa mi ho vlozit lebo uz tam taky je a modifikacia nie je povolena
        }

        if (preplBlok.jeBlokPlny()) {
            if (preplBlok.getaCisloZretazenehoBloku() < 0 && paPrvyVolnyBlokVpreplSub.getaCisloBloku() < 0) {
                long volnyBlokVPreplSubore = aFileManagerPreplnSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(preplBlok.getVelkostBlokuVBajtoch());
                preplBlok.setCisloZretazenehoBloku(volnyBlokVPreplSubore);
                aFileManagerPreplnSuboru.zapisBlokNaDisk(preplBlok);
                preplBlok.reset();
                preplBlok.setaCisloBloku(volnyBlokVPreplSubore);
                preplBlok.vlozZaznam(paVkladanyZaznam);
                preplBlok.doplnBlokDoMaximalnehoPoctuZaznamovNeplatnymiZaznamami();
                aFileManagerPreplnSuboru.zapisBlokNaDisk(preplBlok);
                aAktualnyPocetZaznamovVStrukture++;
                return -1;
            } else if (preplBlok.getaCisloZretazenehoBloku() < 0 && paPrvyVolnyBlokVpreplSub.getaCisloBloku() >= 0) {
                paPrvyVolnyBlokVpreplSub.vlozZaznam(paVkladanyZaznam);
                aFileManagerPreplnSuboru.zapisBlokNaDisk(paPrvyVolnyBlokVpreplSub);
                aAktualnyPocetZaznamovVStrukture++;
                return -1;
            } else {
                return preplBlok.getaCisloZretazenehoBloku();//chod do dalsieho bloku v preplnovacm subore
            }
        } else if (preplBlok.getaCisloZretazenehoBloku() >= 0) {//******
            if (paPrvyVolnyBlokVpreplSub.getaCisloBloku() < 0) {
                paPrvyVolnyBlokVpreplSub.copyBlok(preplBlok);
            }
            return preplBlok.getaCisloZretazenehoBloku();
        }
        if (paPrvyVolnyBlokVpreplSub.getaCisloBloku() < 0) {
            preplBlok.vlozZaznam(paVkladanyZaznam);
            aFileManagerPreplnSuboru.zapisBlokNaDisk(preplBlok);
        } else {
            paPrvyVolnyBlokVpreplSub.vlozZaznam(paVkladanyZaznam);
            aFileManagerPreplnSuboru.zapisBlokNaDisk(paPrvyVolnyBlokVpreplSub);
        }
        aAktualnyPocetZaznamovVStrukture++;
        return -1;
    }

    public Z odstran(Z paOdstrObjekt) {
        if (aTrieStrom.getKoren() == null) {
            return null;
        }
        ExternyVrchol ext = aTrieStrom.traverzuj(paOdstrObjekt.hashCode());
        if (ext.getaCisloBloku() < 0) {
            return null;
        }
        Blok blok = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        Blok predchodca = null;
        blok.setaCisloBloku(ext.getaCisloBloku());
        blok.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(blok));
        do {
            if (blok.odstranZaznam(paOdstrObjekt)) {
                if (blok.somUlozenyVPreplnujucomSubore()) {
                    //System.out.println("Odstranujem s prepl. suboru");
                    odstranujemSPreplnovaciehoSuboru(blok, predchodca, ext);
                } else {
                    //System.out.println("Odstranujem s EXT vrchola");
                    odstranujemZExternehoVrchola(blok, ext);
                }
                aAktualnyPocetZaznamovVStrukture--;
                return paOdstrObjekt;
            } else {
                if (blok.getaCisloZretazenehoBloku() < 0) {
                    return null;//nepodarilo sa odstranit lebo sa nenasiel a nema preplnovacie subory
                }
                if (predchodca == null) {
                    predchodca = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
                } else {
                    predchodca.setaSomUlozenyVPreplnujucomSubore(true);
                }
                predchodca.copyBlok(blok);
                blok.reset();
                blok.setaSomUlozenyVPreplnujucomSubore(true);
                blok.setaCisloBloku(predchodca.getaCisloZretazenehoBloku());
                blok.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(blok));
            }
        } while (true);
    }

    private void odstranujemZExternehoVrchola(Blok blokZKtorehoOdstranujem, ExternyVrchol ext) {
        ext.znizAktualnyPocetZaznamov();
        if (ext.getaOtec() == null && ext.getPocetPlatnychZaznamov() == 0) {
            aTrieStrom.setKoren(null);
            aFilemanagerHlavnehoSuboru.pridajNovyVolnyBlok(blokZKtorehoOdstranujem);
            return;
        }
        Vrchol bratExt = dajBrataVrchola(ext);
        ExternyVrchol brat = null;
        if (bratExt != null && (bratExt instanceof ExternyVrchol)) {
            brat = (ExternyVrchol) bratExt;
        }
        if (brat != null && brat.getPocetPlatnychZaznamov() + ext.getPocetPlatnychZaznamov() <= aMaxPocetZaznamovVbloku) {
            Blok novyBlokPoSpojeni = spajajExterneVrcholyKymSaDa(ext, brat, blokZKtorehoOdstranujem);
            aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(novyBlokPoSpojeni);
        } else {
            if (blokZKtorehoOdstranujem.getaCisloZretazenehoBloku() < 0) {
                if (blokZKtorehoOdstranujem.getPocetPlatnychZaznamovBloku() == 0) {//ak blok z ktoreho odstranujem me iba interneho brata a ja prazdny
                    aFilemanagerHlavnehoSuboru.pridajNovyVolnyBlok(blokZKtorehoOdstranujem);
                    ext.setaCisloBloku(-1);
                    if (ext.getPocetPlatnychZaznamov() > 0) {
                        throw new IllegalMonitorStateException("Pocet zaznamov by mal byt rovny 0 v externom vrchole");
                    }
                    ext.setaPocetPlatnychZaznamov(0);
                } else {
                    aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(blokZKtorehoOdstranujem);
                }
            } else {
                aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(blokZKtorehoOdstranujem);
            }
        }
    }

    private void odstranujemSPreplnovaciehoSuboru(Blok paBlokZKtorehoOdstr, Blok predchodcaBlokuZKtorehoOstr, ExternyVrchol ext) {
        ext.znizAktualnyPocetZaznamov();
        Vrchol bratExt = dajBrataVrchola(ext);
        ExternyVrchol brat = null;
        if (bratExt != null && (bratExt instanceof ExternyVrchol)) {
            brat = (ExternyVrchol) bratExt;
        }
        if (brat != null && brat.getPocetPlatnychZaznamov() + ext.getPocetPlatnychZaznamov() <= aMaxPocetZaznamovVbloku) {
            Blok paNovyBlokPoSpojeni = spajajExterneVrcholyKymSaDa(ext, brat, paBlokZKtorehoOdstr);
            aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(paNovyBlokPoSpojeni);
        } else {
            if (paBlokZKtorehoOdstr.getPocetPlatnychZaznamovBloku() == 0) {
                predchodcaBlokuZKtorehoOstr.setCisloZretazenehoBloku(paBlokZKtorehoOdstr.getaCisloZretazenehoBloku());
                if (predchodcaBlokuZKtorehoOstr.somUlozenyVPreplnujucomSubore()) {
                    aFileManagerPreplnSuboru.zapisBlokNaDisk(predchodcaBlokuZKtorehoOstr);
                } else {
                    aFilemanagerHlavnehoSuboru.zapisBlokNaDisk(predchodcaBlokuZKtorehoOstr);
                }
                aFileManagerPreplnSuboru.pridajNovyVolnyBlok(paBlokZKtorehoOdstr);
                return;
            }
            aFileManagerPreplnSuboru.zapisBlokNaDisk(paBlokZKtorehoOdstr);
        }
    }

    private Blok spajajExterneVrcholyKymSaDa(ExternyVrchol ext, ExternyVrchol brat, Blok paBlokZKtorehoSomOdstranoval) {
        Vrchol bratExt;
        Blok novyBlok = paBlokZKtorehoSomOdstranoval;
        do {
            novyBlok = spojExterneVrcholy(ext, brat, novyBlok);
            bratExt = dajBrataVrchola(ext);//ext je novy externy vrchol, ktory vznkol spojenim, Upravila ho metoda spoExterneVrcholy
            brat = null;
            if (bratExt != null && (bratExt instanceof ExternyVrchol)) {
                brat = (ExternyVrchol) bratExt;
            }
        } while (brat != null && brat.getPocetPlatnychZaznamov() + ext.getPocetPlatnychZaznamov() <= aMaxPocetZaznamovVbloku);
        return novyBlok;
    }

    /**
     * Majme ext a bratExt. Tieto vrcholy chcceme spojit do noveho externeho.
     * Tato metoda prekopiruje prvky v blokoch urcenych ext a bratExt do noveho
     * bloku vratenym navratovou hodnotou. Novym externy vrchol vznikne zmenou
     * atributou vrchola dostaneho v parametri ext - > ext je zaroven novym
     * externym Vrcholom Nastavi ext pocet aktualnych platnych zaznamov v bloku
     * Znizi ext hlbku o -1 a ext sa posunie o ureoven vyssie v Trie strome
     *
     * @param ext
     * @param bratExt
     * @return
     */
    private Blok spojExterneVrcholy(ExternyVrchol ext, ExternyVrchol bratExt, Blok paBlokZKtorehoOdstr) {
        Blok novyBloPoSpojeni = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        //prechadzaj blok v ext a jeho preplnujuce
        Blok b1 = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        b1.setaCisloBloku(ext.getaCisloBloku());
        while (b1.getaCisloBloku() >= 0) {
            if (b1.somUlozenyVPreplnujucomSubore()) {
                if (paBlokZKtorehoOdstr.somUlozenyVPreplnujucomSubore() && paBlokZKtorehoOdstr.getaCisloBloku() == b1.getaCisloBloku()) {
                    b1.copyBlok(paBlokZKtorehoOdstr);
                } else {
                    b1.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(b1));
                }
                aFileManagerPreplnSuboru.pridajNovyVolnyBlok(b1);
            } else {
                if (paBlokZKtorehoOdstr.somUlozenyVPreplnujucomSubore()) {
                    b1.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(b1));
                } else {
                    b1.copyBlok(paBlokZKtorehoOdstr);
                }
                b1.setaSomUlozenyVPreplnujucomSubore(true);
                aFilemanagerHlavnehoSuboru.pridajNovyVolnyBlok(b1);
            }
            for (Zaznam prech : b1) {
                if (prech.jePlatny()) {
                    novyBloPoSpojeni.vlozZaznam(prech);
                }
            }
            b1.resetZoznamZaznamov();
            b1.setaCisloBloku(b1.getaCisloZretazenehoBloku());
        }
        //prechadzaj blok v bratovi a jeho prepl
        b1.setaCisloBloku(bratExt.getaCisloBloku());
        b1.setaSomUlozenyVPreplnujucomSubore(false);
        while (b1.getaCisloBloku() >= 0) {
            if (b1.somUlozenyVPreplnujucomSubore()) {
                b1.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(b1));
                aFileManagerPreplnSuboru.pridajNovyVolnyBlok(b1);
            } else {
                b1.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(b1));
                b1.setaSomUlozenyVPreplnujucomSubore(true);
                aFilemanagerHlavnehoSuboru.pridajNovyVolnyBlok(b1);
            }
            for (Zaznam prech : b1) {
                if (prech.jePlatny()) {
                    novyBloPoSpojeni.vlozZaznam(prech);
                }
            }
            b1.resetZoznamZaznamov();
            b1.setaCisloBloku(b1.getaCisloZretazenehoBloku());
        }
        ext.setaPocetPlatnychZaznamov(ext.getPocetPlatnychZaznamov() + bratExt.getPocetPlatnychZaznamov());
        novyBloPoSpojeni.setaCisloBloku(aFilemanagerHlavnehoSuboru.dajCisloBlokuKdeBudeUkladanyNovyBlok(novyBloPoSpojeni.getVelkostBlokuVBajtoch()));
        ext.setaCisloBloku(novyBloPoSpojeni.getaCisloBloku());
        upravTrieStrom(ext);
        return novyBloPoSpojeni;
    }

    private void upravTrieStrom(ExternyVrchol ext) {
        Vrchol otec = ext.getaOtec();
        Vrchol dedo = otec.getaOtec();
        ext.setaOtec(dedo);
        ext.setaHlbkaVrchola(ext.getaHlbkaVrchola() - 1);
        if (dedo == null) {
            aTrieStrom.setKoren(ext);
        } else {
            if (jeVrcholPravymSynomOca(otec)) {
                dedo.setaPravySyn(ext);
            } else {
                dedo.setaLavySyn(ext);
            }
        }
    }

    /**
     *
     * @param paVrchol
     * @return null - vrchol nema brata lebo je ocom
     */
    public Vrchol dajBrataVrchola(Vrchol paVrchol) {
        Vrchol otec = paVrchol.getaOtec();
        if (otec == null) {
            return null;
        }

        if (otec.getaPravySyn().equals(paVrchol)) {
            return otec.getaLavySyn();
        }
        return otec.getaPravySyn();
    }

    public boolean jeVrcholPravymSynomOca(Vrchol paVrchol) {
        if (paVrchol.getaOtec() == null) {
            throw new IllegalStateException();
        }
        if (paVrchol.getaOtec().getaPravySyn().equals(paVrchol)) {
            return true;
        }
        return false;
    }

    public String sekvencnyVypisSuboru() {
        int pocetPlat = 0;
        StringBuilder ret = new StringBuilder(300000000);
        ret.append("Počet platných záznamov v Štruktúre: " + aAktualnyPocetZaznamovVStrukture + "\n");
        Blok v = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
        long pocetBlokovSuboru = aFilemanagerHlavnehoSuboru.getPocetBlokovVSubore(v.getVelkostBlokuVBajtoch());
        for (long i = 0; i < pocetBlokovSuboru; i++) {
            if (!aFilemanagerHlavnehoSuboru.obsahujeVolnyBlok(i)) {
                ret.append("-------------------------------------------------------BLOK: " + i + "--------------------------------------------------------------------\n");
                v.setaCisloBloku(i);
                v.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(v));
                pocetPlat += v.getPocetPlatnychZaznamovBloku();
                ret.append(v.toString());
                ret.append("********************************PREPLNOVACIE BLOKY: " + i + "*********************************\n");
                boolean koniec = false;
                if (v.getaCisloZretazenehoBloku() != -1) {
                    do {
                        if (!aFileManagerPreplnSuboru.obsahujeVolnyBlok(v.getaCisloZretazenehoBloku())) {
                            v.setaCisloBloku(v.getaCisloZretazenehoBloku());
                            v.resetZoznamZaznamov();
                            v.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(v));
                            pocetPlat += v.getPocetPlatnychZaznamovBloku();
                            ret.append(v.toString());
                            koniec = v.getaCisloZretazenehoBloku() < 0;
                        } else {
                            koniec = true;
                        }
                    } while (!koniec);
                }
                ret.append("********************************KONIEC PREPLNOVACIE BLOKY: " + i + "*********************************\n");
                ret.append("--------------------------------------------------KONIEC BLOK: " + i + "--------------------------------------------------------------\n\n\n\n");
                v.reset();
            }
        }
        ret.append("\nPocet platnych urcenych pri nacitavani: " + pocetPlat);
        return ret.toString();
    }

    public int pocetNachadzajucichSaVBlokkoch() {
        LinkedList<Vrchol> interne = new LinkedList<>();
        LinkedList<ExternyVrchol> externe = new LinkedList<>();
        if (aTrieStrom.getKoren() == null) {
            return 0;
        } else if (aTrieStrom.getKoren() instanceof ExternyVrchol) {
            externe.add((ExternyVrchol) aTrieStrom.getKoren());
        } else {
            interne.add(aTrieStrom.getKoren());
        }
        int spracHlbka = 0;
        while (interne.size() > 0) {
            Vrchol spracVrchol = interne.getFirst();
            if (spracVrchol.getaLavySyn() != null) {
                if (spracVrchol.getaLavySyn() instanceof ExternyVrchol) {
                    ExternyVrchol ext = (ExternyVrchol) spracVrchol.getaLavySyn();
                    if (ext.getaCisloBloku() >= 0) {
                        externe.add(ext);
                    }
                } else {
                    interne.add(spracVrchol.getaLavySyn());
                }
            }
            if (spracVrchol.getaPravySyn() != null) {
                if (spracVrchol.getaPravySyn() instanceof ExternyVrchol) {
                    ExternyVrchol ext = (ExternyVrchol) spracVrchol.getaPravySyn();
                    if (ext.getaCisloBloku() >= 0) {
                        externe.add(ext);
                    }
                } else {
                    interne.add(spracVrchol.getaPravySyn());
                }
            }
            interne.removeFirst();
        }
        int pocVPreplSuboroch = 0;
        int pocetVTrieBlokoch = 0;
        Blok b = new Blok(aMaxPocetZaznamovVbloku, aImplementator, true);
        for (ExternyVrchol prech : externe) {
            b.setaCisloBloku(prech.getaCisloBloku());
            b.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(b));
            pocetVTrieBlokoch += b.getPocetPlatnychZaznamovBloku();
            while (b.getaCisloZretazenehoBloku() != -1) {
                b.setaCisloBloku(b.getaCisloZretazenehoBloku());
                b.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(b));
                pocVPreplSuboroch += b.getPocetPlatnychZaznamovBloku();
            }
        }
        return pocVPreplSuboroch + pocetVTrieBlokoch;
    }

    public void uvolniSubory() {
        aFileManagerPreplnSuboru.zavri();
        aFilemanagerHlavnehoSuboru.zavri();
    }

    public TrieStrom getaTrieStrom() {
        return aTrieStrom;
    }

    public void vytvorZalohu() {
        ulozDataAktualnyStavStruktury();
        File f = new File(PRIECINOK_ZO_SUBORMI);
        File[] files = f.listFiles();
        Path source;
        Path destination;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                try {
                    source = files[i].toPath();
                    destination = new File(PRIECINOK_ZO_SUBORMI + PRIECINOK_ZO_ZALOHOU + System.getProperty("file.separator") + files[i].getName()).toPath();
                    Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    Logger.getLogger(DynamickeHesovanie.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void obnovPredtymVytvorenuZalohu() {
        uvolniSubory();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DynamickeHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        File f = new File(PRIECINOK_ZO_SUBORMI + PRIECINOK_ZO_ZALOHOU);
        File[] files = f.listFiles();
        Path source;
        Path destination;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                try {
                    source = files[i].toPath();
                    destination = new File(PRIECINOK_ZO_SUBORMI + System.getProperty("file.separator") + files[i].getName()).toPath();
                    Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    Logger.getLogger(DynamickeHesovanie.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            obnovDataNaposledyUlozeneDH();
        } catch (IOException ex) {
            Logger.getLogger(DynamickeHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public final void ulozDataAktualnyStavStruktury() {
        this.aFilemanagerHlavnehoSuboru.ulozZoznamVolnychBlokovDoSuboru(new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_HL));
        this.aFileManagerPreplnSuboru.ulozZoznamVolnychBlokovDoSuboru(new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUB_VOLNE_BLOKY_PREPL));
        File trieStromF = new File(PRIECINOK_ZO_SUBORMI + NAZOV_TRIE_STROM_SUBOR);
        aTrieStrom.zapisTrieStromuDoSuboru(new File(PRIECINOK_ZO_SUBORMI + NAZOV_TRIE_STROM_SUBOR));
        File zakl_info = new File(PRIECINOK_ZO_SUBORMI + NAZOV_SUBORU_OBS_ZAKL_INFO);
        FileWorker.writeDataToFile(zakl_info, Integer.toString(aMaxPocetZaznamovVbloku) + "\n" + aAktualnyPocetZaznamovVStrukture, false);
    }

    public static boolean existujeUlozenyStav(File paPriecinok) {
        List<File> suboryZUlozenymStavom = new LinkedList<>();
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_HLAVNEHO_SUBORU));
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_PREPL_SUBORU));
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_SUB_VOLNE_BLOKY_HL));
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_SUB_VOLNE_BLOKY_PREPL));
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_TRIE_STROM_SUBOR));
        suboryZUlozenymStavom.add(new File(paPriecinok.getAbsolutePath() + NAZOV_SUBORU_OBS_ZAKL_INFO));
        for (File prech : suboryZUlozenymStavom) {
            if (!prech.exists()) {
                return false;
            }
        }
        return true;
    }

    public long getaAktualnyPocetPlatnychZaznamovVStrukture() {
        return aAktualnyPocetZaznamovVStrukture;
    }

    public int getVelkostBlokuVBajtoch() {
        return new Blok(aMaxPocetZaznamovVbloku, aImplementator, true).getVelkostBlokuVBajtoch();
    }

    public int getVelkostJednehoZaznamuVBajtoch() {
        return aImplementator.get_Velkost_Zaznamu_V_Bajtoch();
    }

    public int getMaxHlbkaTrieStromu() {
        return aTrieStrom.getMAXIMALNA_HLBKA_TRIE_STROMU();
    }

    public void vypisStrukturyPomocouVlakna(JTextArea paTextArea) {
        Thread t = new Thread(() -> {
            int pocetPlat = 0;
            StringBuilder ret = new StringBuilder(300000000);
            ret.append("Počet platných záznamov v Štruktúre: " + aAktualnyPocetZaznamovVStrukture + "\n");
            Blok v = new Blok(aMaxPocetZaznamovVbloku, aImplementator, false);
            long pocetBlokovSuboru = aFilemanagerHlavnehoSuboru.getPocetBlokovVSubore(v.getVelkostBlokuVBajtoch());
            for (long i = 0; i < pocetBlokovSuboru; i++) {
                if (!aFilemanagerHlavnehoSuboru.obsahujeVolnyBlok(i)) {
                    ret.append("-------------------------------------------------------BLOK: " + i + "--------------------------------------------------------------------\n");
                    v.setaCisloBloku(i);
                    v.nacitajZaznamyBlokuZBinarnychDat(aFilemanagerHlavnehoSuboru.nacitajZaznamyBloku(v));
                    pocetPlat += v.getPocetPlatnychZaznamovBloku();
                    ret.append(v.toString());
                    ret.append("********************************PREPLNOVACIE BLOKY: " + i + "*********************************\n");
                    boolean koniec = false;
                    if (v.getaCisloZretazenehoBloku() != -1) {
                        do {
                            if (!aFileManagerPreplnSuboru.obsahujeVolnyBlok(v.getaCisloZretazenehoBloku())) {
                                v.setaCisloBloku(v.getaCisloZretazenehoBloku());
                                v.resetZoznamZaznamov();
                                v.nacitajZaznamyBlokuZBinarnychDat(aFileManagerPreplnSuboru.nacitajZaznamyBloku(v));
                                pocetPlat += v.getPocetPlatnychZaznamovBloku();
                                ret.append(v.toString());
                                koniec = v.getaCisloZretazenehoBloku() < 0;
                            } else {
                                koniec = true;
                            }
                        } while (!koniec);
                    }
                    ret.append("********************************KONIEC PREPLNOVACIE BLOKY: " + i + "*********************************\n");
                    ret.append("--------------------------------------------------KONIEC BLOK: " + i + "--------------------------------------------------------------\n\n\n\n");
                    v.reset();
                }
                if (i % 1000 == 0 || ((pocetBlokovSuboru-i)<1000)) {
                    SwingUtilities.invokeLater(() -> {
                        paTextArea.append(ret.toString());
                        ret.delete(0, ret.length());
                    });
                }
            }
            ret.append("\nPocet platnych urcenych pri nacitavani: " + pocetPlat);
            ret.append("\n\n\nKONIEC VYPISU");
            System.out.println(ret.toString());
            //System.out.println(ret.toString());
            /*SwingUtilities.invokeLater(() -> {
                System.out.println(ret.toString());
                paTextArea.setText(ret.toString());
                System.out.println(ret.toString());
            });*/
        });
        t.start();
    }
}

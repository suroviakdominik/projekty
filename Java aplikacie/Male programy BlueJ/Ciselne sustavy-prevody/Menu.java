import java.util.Scanner;
/**
 * -metoda vypis, -metoda zisti Volbu :int, -metoda Volba1,-metoda Volba 2, -metodavykonajvolbu,
 * +Sprav Menu, +Menu
 * Vo vypise vypise xxxxx,dalsi riadok 1..Bin To Dec, dalsi riadok Dec To Bin, dalsi riadok 2..Dec To Bin
 * Dalsi riaok hviezdickz
 */
public class Menu
{
 private Scanner scan;
 public Menu()
  {
    scan= new Scanner(System.in); 
  }    

 private void volba1()
   {
     System.out.print("Zadaja cislo v desiatkovej sustave ktore chcete prekonvertovat");
     int p= scan.nextInt();
     int vysledok=Cislo.decToBin(p);
     System.out.println("Cislo " +p + " v desiatkovej sustave je " +vysledok+ " v dvojkovej sustave");
   }
   
  private void volba2()
   {
   System.out.print("Zadaja cislo v dvojkovej sustave ktore chcete prekonvertovat");
     int p= scan.nextInt();
     int vysledok=Cislo.binToDec(p);
     System.out.println("Cislo " +p + " v dvojkovej sustave je " +vysledok+ " v desiatkovej sustave");
   }
  
  
      private int zadajVolbu()
       {
           System.out.print("Zadajte vasu volbu");
           int volba= scan.nextInt();
           return volba;
        }
  
  private void vypis()
  {
    System.out.println("*******");
    System.out.println("Dec To Bin");
    System.out.println("Bin To Dec");
    System.out.println("Spocitaj priemer");
    System.out.println("Koniec");
    System.out.println("*******");
  }              
        
        public boolean vykonajVolbu(int volba)
         {
            switch (volba)
            {
                case 1: volba1(); return true;
                case 2: volba2(); return true;
                case 3: Stats.spocitajPriemer(); return true;
                case 0: System.out.println("Program sa ukoncil"); return true; 
                default: return false;
            }
        } 
  
public void spustiMenu()
   {
       int v;
       do
       {
         vypis();
         boolean temp;
            do
            {
             v= zadajVolbu();
             temp=vykonajVolbu(v);
             if (!temp) System.out.println("Zla podmienka, opravte sa");
             }
         
            while (!temp);
         
        }
        while (v!=0);
    } 
}

  
 

package wof00.Hra;

import wof00.Miestnost.IMiestnost;
import wof00.Miestnost.Miestnost;
import wof00.VstupVystup.Parser;
import wof00.VstupVystup.Prikaz;

/**
 * Trieda Hra je hlavna trieda aplikacie "World of FRI". "World of FRI" je velmi
 * jednoducha textova hra - adventura. Hrac sa moze prechadzat po niektorych
 * priestoroch - miestnostiach fakulty. To je v tejto verzii vsetko. Hru treba
 * skutocne zancne rozsirit, aby bola zaujimava.
 *
 * Ak chcete hrat "World of FRI", vytvorte instanciu triedy Hra (hra) a poslite
 * jej spravu hraj.
 *
 * Hra vytvori a inicializuje vsetky potebne objekty: vytvori vsetky miestnosti,
 * vytvori parser a zacne hru. Hra tiez vyhodnocuje a vykonava prikazy, ktore
 * vrati parser.
 *
 * @author Michael Kolling, David J. Barnes
 * @version 2006.03.30
 * @author lokalizacia: Lubomir Sadlon, Jan Janech
 * @version 2012.02.21
 */
public class Hra {

    private Parser aParser;

    private Hrac aHrac;
    private MapaHry aMapa;

    /**
     * Create the game and initialise its internal map.
     */
    public Hra(Hrac paHrac) {
        aMapa = new MapaHry();
        aParser = new Parser();
        aHrac = paHrac;
    }

    public Hra(String paMenoHraca, int penazenka, int zivot, String pozicia) {
        aMapa = new MapaHry();
        aHrac = new Hrac(paMenoHraca, penazenka, zivot, aMapa);
        aHrac.setPoziciu(aMapa.najdiMiestnost(pozicia));
        aParser = new Parser();

    }

    public Hra(String menoHraca) {
        aMapa = new MapaHry();
        aHrac = new Hrac(menoHraca, aMapa);
        aParser = new Parser();
        aHrac.setPoziciu(aMapa.dajStartovaciuMiestnost());
    }

    /**
     * Hlavna metoda hry. Cyklicky opakuje kroky hry, kym hrac hru neukonci.
     */
    public void hraj() {

        vypisPrivitanie(aHrac);
        // Vstupny bod hlavneho cyklu.
        // Opakovane nacitava prikazy hraca
        // vykonava ich kym hrac nezada prikaz na ukoncenie hry.

        boolean jeKoniec = false;
        while ((!jeKoniec)) {
            Prikaz prikaz = aParser.dajPrikaz(aHrac);
            jeKoniec = prikaz.vykonajPrikaz(aHrac);

        }
        if (!aHrac.masZivot()) {
            System.out.println("Tvoj život je rovný 0. Zomrel si.!");
        }
        System.out.println("Maj sa fajn!");
    }

    /**
     * Vypise privitanie hraca do terminaloveho okna.
     */
    private void vypisPrivitanie(Hrac paHrac) {
        System.out.println();
        System.out.println("Ahoj: " + paHrac.getMeno());
        System.out.println("Vitaj v hre World of FRI!");
        System.out.println("World of FRI je nova, neuveritelne nudna adventura.");
        System.out.println("Zadaj 'pomoc' ak potrebujes pomoc.");
        System.out.println();
        IMiestnost pozicia = aHrac.dajPoziciu();
        pozicia.vypisInfoMiestnosti();

    }

}

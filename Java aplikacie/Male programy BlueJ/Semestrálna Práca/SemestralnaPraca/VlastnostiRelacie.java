
/**
 * Trieda zistí vlastnosti predtým vytvorenej relácie
 * 
 * @author (Dominik Suroviak) 
 * @version (a version number or a date)
 */
public class VlastnostiRelacie
{

    private Relacie aRelacie;

    /**
     * Konštruktor vytvorí inštanciu triedy. Načíta predtým vytvórenú reláciu
     */
    public VlastnostiRelacie(Relacie paRelacie)
    {
        aRelacie=paRelacie;
    }

         /**
         * Metóda overí, či je daná relácia reflexívna
         * @ return vráti pravdivostnú hodnotu
         */
         public boolean reflexivnost()
         {
            for (int i=0;i<aRelacie.dajPocetPrvkovMnozinyA();i++)
            {
              if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,i)==0)
               {
                 return false;
               }
            }
            return true;
         }  

         /**
          * Metóda overí, či je daná relácia symetrická
          * @ return vráti pravdivostnú hodnotu
          */
         public boolean symetrickost()
         {
            for (int i=0;i<aRelacie.dajPocetPrvkovMnozinyA();i++)
            {
              for (int j=0;j<aRelacie.dajPocetPrvkovMnozinyB();j++)
              {
                if ((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i)!=aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)))
                {
                    return false;
                }
              }   
            }
            return true;
         }   

         /**
          * Metóda overí, či je daná relácia antisymetrická
          * @ return vráti pravdivostnú hodnotu
          */
         public boolean antisymetrickost()
         {
             for (int i=0;i<aRelacie.dajPocetPrvkovMnozinyA();i++)
             {
                 for (int j=0;j<aRelacie.dajPocetPrvkovMnozinyB();j++)
                 {
                    if ((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i)) && (aRelacie.dajPrvokPolaA(i)!=aRelacie.dajPrvokPolaB(j)))
                    {
                       return false;
                    }
                 }      
             } 
             return true;
         }   

         /**
          * Metóda overí, či je daná relácia asymetrická
          * @ return vráti pravdivostnú hodnotu
          */
         public boolean asymetrickost()
         {
             for (int i=0;i<aRelacie.dajPocetPrvkovMnozinyA();i++)
             {
                 for (int j=0;j<aRelacie.dajPocetPrvkovMnozinyB();j++)
                 {
                     if (((aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1) && (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(j,i))) && (aRelacie.dajPrvokPolaA(i)==aRelacie.dajPrvokPolaB(j)))
                     {
                         return false;
                     }
                 }   
             } 
             return true;
         }

        /**
          * Metóda overí, či je daná relácia tranzitívna
          * @ return vráti pravdivostnú hodnotu
          */
         public boolean tranzitivnost()
         {
             for (int i=0;i<aRelacie.dajPocetPrvkovMnozinyA();i++)
             {
                 for (int j=0;j<aRelacie.dajPocetPrvkovMnozinyB();j++)
                 {
                    if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,j)==1)
                    {
                       for (int k=i+1;k<aRelacie.dajPocetPrvkovMnozinyA();k++)
                       {
                          if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(k,j)==1)
                          {
                              if (aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(k,j)!=aRelacie.zistiHodnotuDoNRozmernehoPolaRelacie(i,k))
                              {
                                  return false;
                              }
                          }   
                       }
                    }
                 }
             }         
             return true;
         } 
}

     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     

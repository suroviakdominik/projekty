/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EditorsForInfo_Files;

import WorkWithFile.DirWorker;
import WorkWithFile.FileWorker;
import WorkWithFile.SerializationWorker;
import com.suroviak.dominik.zooAplication.Info_About_Dirs_File;
import com.suroviak.dominik.zooAplication.Info_About_Item;
import java.io.IOException;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import setdatatoandroidaplication.DataTransferer;

/**
 *   * Editor, ktory edituje iNFORMACIE O ZOOLOGICKEJ zAHRADE
 * @author Dominik
 */
public class Info_File_INFO_EDITOR implements IOperacieEDITORA {

    private HashMap<String, Info_About_Item> aAktualneUdajeInfoSuboru = null;
    private Info_About_Dirs_File aInfoAboutDirsFile = null;
    private DataTransferer aDataTransferer = DataTransferer.getInstance();

     /**
     * 
     * @param paFilesInfo- subor, kde su ulozene polozky predstavujuce informacie o ZOO
     *                      a popis ich dat
     */
    public Info_File_INFO_EDITOR(Info_About_Dirs_File paFilesInfo) {
        aAktualneUdajeInfoSuboru = paFilesInfo.getaItemsInfo();
        aInfoAboutDirsFile = paFilesInfo;
    }

    /**
     * V zavislosti od udaju v DataTransferer vrati pozadovane data a
     * ulozi ich do data transfer objektu aby ich mohla zalozka 
     * pouzivajuca tento editor zobrazit.
     * @return
     */
    @Override
    public boolean getDataOfInfoFile() {
        if (aDataTransferer.getString("TICKET_PRICES") != null) {
           return setDataForTicketPrices();
        }
        else
        {
            return getBaseInfoDate();
        }
        
    }

    @Override
    public boolean addNewItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     
    @Override
    public boolean deleteItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Upravi data informacie o zoo, ktory daostane zo zalozky, ktora ho ulozila 
     * do objektu DataTransferer spolu s datami, ktore sa maju zmenit
     * @return true -informaci o zoo sa podarilo upravit(zmenit data)
     */
    @Override
    public boolean editItem() {
        if (aDataTransferer.getString("MAIN_INFO") != null && aDataTransferer.getString("OPENING_HOURS") != null) {
            return editBaseInfoData();
        } else if (aDataTransferer.getString("TICKET_PRICES") != null) {
           return editTicketPrices();
        }
       
        return true;
    }

    /**
     * Serializuje subor typu Info_About_Dirs_File prisluchajuci tonuto editrou.
     * Data su ulozene a pri dalsom spusteni aplikacie budu zobrazene
     * @return true - uspesne ulozeny sriaizovany subor
     */
    @Override
    public boolean saveInfoFile() {
        SerializationWorker<Info_About_Dirs_File> sw = new SerializationWorker<>(InfoSuboryENUM.aEndOfInfoFile);
        sw.setNameOfFile(InfoSuboryENUM.INFO.getaNameOfInfoFile());
        sw.setObjectThatIsSerializable(aInfoAboutDirsFile);
        sw.setPath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                + InfoSuboryENUM.INFO.getaRelativePathOfDirecotryWhereIAM());
        sw.saveObjectToFile();
        System.out.println("\nMyLogFiles: Info_About_Dirs_File in editor: "
                        +this.getClass().getSimpleName()
                        +" has been saved.");
        return true;
    }

    /**
     * 
     * @return  subor typu Info_About_DIRS_File, ktory obsahuje polozky editovane
     *          v ramci tohto editora
     */
    @Override
    public Info_About_Dirs_File getInfoFileThatIEdit() {
        return aInfoAboutDirsFile;
    }

    private boolean editBaseInfoData() {
        DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + InfoSuboryENUM.FILE_SEPARATOR  + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 0 + InfoSuboryENUM.FILE_SEPARATOR );
            dw.vytvorAdresarAkNeexistuje();
            dw.setAbsolutePathOfDir(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 1 + InfoSuboryENUM.FILE_SEPARATOR );
            dw.vytvorAdresarAkNeexistuje();

            Info_About_Item baseInfo = new Info_About_Item("Základné informácie", null, aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + 0 + InfoSuboryENUM.FILE_SEPARATOR);
            Info_About_Item openHoursInfo = new Info_About_Item("Otváracie hodiny", null, aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 1 + InfoSuboryENUM.FILE_SEPARATOR );

            FileWorker fw = new FileWorker();
            fw.setaAbsolutePath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + 0 + InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            try {
                fw.writeDataToFile(aDataTransferer.getString("MAIN_INFO"));
            } catch (IOException ex) {

                ex.printStackTrace();
                return false;
            }

            fw.setaAbsolutePath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 1 
                    + InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            try {
                fw.writeDataToFile(aDataTransferer.getString("OPENING_HOURS"));
            } catch (IOException ex) {
                ex.printStackTrace();
                return false;
            }
            
            baseInfo.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.INFO.getServerDIRDomainOfThisFile()
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 0 + InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            openHoursInfo.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.INFO.getServerDIRDomainOfThisFile()
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 1 
                    + InfoSuboryENUM.FILE_SEPARATOR+InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            aAktualneUdajeInfoSuboru.remove("I0");
            aAktualneUdajeInfoSuboru.remove("I1");
            aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + "0", baseInfo);
            aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + "1", openHoursInfo);
            System.out.println("\nMyLogFiles: All base info data has been edited\n"
                    + "in editor: "+this.getClass().getSimpleName());
            saveInfoFile();
            return true;
    }

    private boolean editTicketPrices() {
         DirWorker dw = new DirWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + InfoSuboryENUM.FILE_SEPARATOR  + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 2 + InfoSuboryENUM.FILE_SEPARATOR );
            dw.vytvorAdresarAkNeexistuje();
            Info_About_Item ticket_price_info = new Info_About_Item("Cenník", null, aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + 2 + InfoSuboryENUM.FILE_SEPARATOR );
            FileWorker fw = new FileWorker();
            fw.setaAbsolutePath(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile()
                    + 2 + InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            try {
                fw.writeDataToFile(aDataTransferer.getString("TICKET_PRICES"));
            } catch (IOException ex) {

                ex.printStackTrace();
                return false;
            }
            aAktualneUdajeInfoSuboru.remove("I2");
            ticket_price_info.getDomainsOfFilesInCurrentItem().add(InfoSuboryENUM.INFO.getServerDIRDomainOfThisFile()
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + InfoSuboryENUM.FILE_SEPARATOR 
                    + aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + 2 
                    + InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            aAktualneUdajeInfoSuboru.put(aInfoAboutDirsFile.getNameOfDirForThisInfoFile() + "2", ticket_price_info);
            System.out.println("\nMyLogFiles: All ticket prices data has been edited\n"
                    + "in editor: "+this.getClass().getSimpleName());
            saveInfoFile();
            return true;
    }

    private boolean setDataForTicketPrices() {
        try {
                if (aAktualneUdajeInfoSuboru.size() == 0) {
                    return false;
                }
                FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                        + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                        + InfoSuboryENUM.INFO.getSignOfDir()
                        + InfoSuboryENUM.FILE_SEPARATOR 
                        + InfoSuboryENUM.INFO.getSignOfDir()
                        + 2+InfoSuboryENUM.FILE_SEPARATOR 
                        +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
                String tick_prices = fw.readDataFromFile();
                aDataTransferer.removeAllData();
                aDataTransferer.addString("RET_TICKET_PRICES", tick_prices);
                
            } catch (IOException ex) {
                ex.printStackTrace();
               return false;
            }
            System.out.println("\nMyLogFiles: All ticket prices data has been set\n"
                    + "to data transfer object in editor: "+this.getClass().getSimpleName());
            return true;
    }

    private boolean getBaseInfoDate() {
       try {
            if (aAktualneUdajeInfoSuboru.size() == 0) {
                return false;
            }
            FileWorker fw = new FileWorker(InfoSuboryENUM.ROOT_DEVICE_FOLDER + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + InfoSuboryENUM.INFO.getSignOfDir() 
                    + InfoSuboryENUM.FILE_SEPARATOR 
                    + InfoSuboryENUM.INFO.getSignOfDir()
                    + 0+InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            String baseInfo = fw.readDataFromFile();
            fw.setaAbsolutePath(InfoSuboryENUM.ROOT_DEVICE_FOLDER
                    + aInfoAboutDirsFile.getaRelativePathOfThisFileInDevice()
                    + InfoSuboryENUM.INFO.getSignOfDir()
                    + InfoSuboryENUM.FILE_SEPARATOR 
                    + InfoSuboryENUM.INFO.getSignOfDir()
                    + "1"+InfoSuboryENUM.FILE_SEPARATOR +InfoSuboryENUM.NAME_OF_FILE_WITH_DESCRIPTION);
            String openHours = fw.readDataFromFile();
            aDataTransferer.removeAllData();
            aDataTransferer.addString("MAIN_INFO", baseInfo);
            aDataTransferer.addString("OPENING_HOURS", openHours);
            System.out.println("\nMyLogFiles: All base info data has been set\n"
                    + "to data transfer object in editor: "+this.getClass().getSimpleName());
            return true;
        } catch (IOException ex) {
            Logger.getLogger(Info_File_INFO_EDITOR.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return false;
        }
    }

}

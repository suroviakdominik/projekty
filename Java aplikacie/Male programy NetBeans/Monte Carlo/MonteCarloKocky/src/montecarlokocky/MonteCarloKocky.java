/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montecarlokocky;

import java.util.Random;

/**
 *
 * @author Dominik
 */
public class MonteCarloKocky {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random kocka1 = new Random();
        Random kocka2 = new Random();
        int pocetPokusov=10000;
        int pocetUspesnych=0;
        int hod1;
        int hod2;
        
        for (int i = 0; i < pocetPokusov; i++) {
            hod1 = 1+kocka1.nextInt(6);
            System.out.print(hod1+" + ");
            hod2 = 1+ kocka2.nextInt(6);
            System.out.println(hod2+" = "+(hod1+hod2));
            if(((hod1+hod2)==7)||((hod1+hod2)==11))
            {
                
                pocetUspesnych++;
            }
        }
        System.out.println("Pocet uspesnych pokusov: "+pocetUspesnych);
        System.out.println("Pocet vsetkych: "+pocetPokusov);
        System.out.println("Pravdepodobnost vyhry: "+((double)pocetUspesnych/(double)pocetPokusov));
    }
    
}

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
   int idMazSemafora = atoi(argv[1]);
   char premenna[2];
   printf("Skutocne chcete zmazat mnozinu semaforov s ID: %d ? (odpoved: A-ano,ine-nie)\n",idMazSemafora);
   scanf("%79s" ,premenna);
   if(premenna[0] == 'A' || premenna[0] == 'a')
   {
       if(semctl(idMazSemafora,0,IPC_RMID)<0)
	{
          printf("Error. Mnozinu semaforov sa nepodarilo zmazat(pravdepodobne neexistuje). semctl vratilo -1 \n");
	  return -1;
	}
        printf("Mnozina semaforov s ID: %d BOLA na zaklada vasej volby zmazana. \n",idMazSemafora);
   }
   else
   {
     printf("Mnozina semaforov s ID: %d NEBOLA na zaklada vasej volby zmazana. \n",idMazSemafora);
 
   }
   return 0;
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var predchadzajuciBodX;
var predchadzajuciBodY;
var sucasnaX;
var sucasnaY;
var context;
var canvas;
$(document).ready(function (){
canvas = document.getElementById('myCanvas');
context = canvas.getContext('2d');
context.strokeStyle = '#3BB9FF';
context.lineWidth = 10;
var mouseIsDown = false;


canvas.addEventListener('mousedown', function (evt) {
    mouseIsDown = true;
    pos=getMousePos(canvas,evt);
    predchadzajuciBodX=pos.x;
    predchadzajuciBodY=pos.y;
}, false);

canvas.addEventListener('mouseup', function (evt) {
    mouseIsDown = false;
}, false);

canvas.addEventListener('mousemove', function (evt) {
    if (mouseIsDown == true)
    {
        var mousePos = getMousePos(canvas, evt);
        //DEBUGvar message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
        //DEBUGwriteMessage(canvas, message);
        sucasnaX=mousePos.x;
        sucasnaY=mousePos.y;
        kresli(evt);
    }
}, false);
});



function writeMessage(canvas, message) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, 25);
    context.font = '18pt Calibri';
    context.fillStyle = 'black';
    context.fillText(message, 10, 25);
}
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}


function kresli(evt)
{
    
        context.beginPath();
        context.moveTo(predchadzajuciBodX, predchadzajuciBodY);
        context.lineTo(sucasnaX, sucasnaY);
        if(sucasnaX!=predchadzajuciBodX && sucasnaY!=predchadzajuciBodY)
        {    
        context.lineTo(sucasnaX+(sucasnaX-predchadzajuciBodX), sucasnaY+(sucasnaY-predchadzajuciBodY));
        context.lineTo(sucasnaX-(sucasnaX-predchadzajuciBodX), sucasnaY-(sucasnaY-predchadzajuciBodY));
        }
        else
        {
            context.lineTo(sucasnaX-2, sucasnaY+2);
            context.lineTo(sucasnaX+2, sucasnaY-2);
        }
        
       
        
        context.stroke();
        
        predchadzajuciBodX=sucasnaX;
        predchadzajuciBodY=sucasnaY;
   
    
}

function changeColor(obj)
{
    if(obj.id=="fixka1")
    {
        context.strokeStyle = '#3BB9FF';
    }
    else if(obj.id=="fixka2")
    {
         context.strokeStyle = '#FF0000';
    }
    else if(obj.id=="fixka3")
    {
         context.strokeStyle = '#008000';
    }
    else
    {
        context.strokeStyle = '#FFFF00';
    }
}

function clearScreen()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function zmenHrubku(obj)
{
    if(obj.value=="2")
    {
        context.lineWidth = 2;
    }
    else if(obj.value=="5")
    {
        context.lineWidth = 5;
    }
    else if(obj.value=="10")
    {
        context.lineWidth = 10;
    }
    else
    {
        context.lineWidth = 15;
    }
   document.getElementById("hrubka").selectedIndex=0;
}

//Hviezdickovy rating system
$(document).ready(function () {
    $('.ratings_stars').hover(
            // Handles the mouseover
                    function () {
                        $(this).prevAll().andSelf().addClass('ratings_over');
                        $(this).nextAll().removeClass('ratings_vote');
                    },
                    // Handles the mouseout
                            function () {
                                $(this).prevAll().andSelf().removeClass('ratings_over');
                                set_votes($(this).parent());
                            });
                });

        $(document).ready(function () {
            $('.rate_widget').each(function (i) {
                var widget = this;
                var out_data = {
                    widget_id: $(widget).attr('id'),
                    fetch: 1
                };
                $.post(
                        'rating.php',
                        out_data,
                        function (INFO) {
                            $(widget).data('fsr', INFO);
                            set_votes(widget);
                        },
                        'json'
                        );
            });
        });

        function set_votes(widget) {

            var avg = $(widget).data('fsr').whole_avg;
            var votes = $(widget).data('fsr').number_votes;
            var exact = $(widget).data('fsr').dec_avg;

            $(widget).find('.star_' + avg).prevAll().andSelf().addClass('ratings_vote');
            $(widget).find('.star_' + avg).nextAll().removeClass('ratings_vote');
            $(widget).find('.total_votes').text(votes + ' osôb hlasovalo (Priemer hlasovania: ' + exact + ')');
        }
        
        
        $(document).ready(function () {
            $('.ratings_stars').bind('click', function () {
                var prihlaseny;
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {

                        if (xmlhttp.responseText != "none")
                        {
                            alert("Na to aby ste mohi hlasovat, musite byt prihlaseny.");
                            return;
                        }
                    }

                }
                xmlhttp.open("POST", "../Prihlasenie/checklogin.php", true);
                xmlhttp.send();

                var star = this;
                var widget = $(this).parent();

                var clicked_data = {
                    clicked_on: $(star).attr('class'),
                    widget_id: widget.attr('id')
                };
                $.post(
                        'rating.php',
                        clicked_data,
                        function (INFO) {
                            widget.data('fsr', INFO);
                            set_votes(widget);
                        },
                        'json'
                        );


            });
        });

    
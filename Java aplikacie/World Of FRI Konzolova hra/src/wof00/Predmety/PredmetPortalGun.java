/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wof00.Predmety;

import java.io.Serializable;
import java.util.HashMap;
import wof00.Hra.Hrac;
import wof00.Miestnost.IMiestnost;
import wof00.Osoby.IOsoba;

/**
 *
 * @author Dominik
 */
public class PredmetPortalGun implements IPredmet, IMiestnost, Serializable {

    private String aNazov;
    private static HashMap<String, IMiestnost> aPortalGun = new HashMap<String, IMiestnost>();
    ;
   private static boolean aJeVytvorenyModryPortal = false;
    private static boolean aJeVytvorenyCervenyPortal = false;
    private static boolean aStriedaniePortalGunov = false;
    private static IMiestnost aModryPortalGun;
    private static IMiestnost aCervenyPortalGun;
    private static Hrac aHrac;

    public PredmetPortalGun(String paNazov) {
        aNazov = paNazov;

    }

    @Override
    public String getNazov() {
        return aNazov;
    }

    @Override
    public String getPopis() {
        return "portal, ktory vytvori striedavo modry a cerveny portal v danej miestnosti." + "\n" + " Cez modry sa mozno dostat do miestnosti s cervenym portalom, a cez cerveny do miestnosti s modrym portalom";
    }

    @Override
    public String pouzi(Hrac paHrac) {
        String nazov;
        if ((aPortalGun.isEmpty()) || !(aStriedaniePortalGunov)) {
            if (aJeVytvorenyCervenyPortal) {
                aPortalGun.get(aCervenyPortalGun.getaNazov()).zmazVychod(aCervenyPortalGun.getaNazov());
                aPortalGun.remove(aCervenyPortalGun.getaNazov());

            }
            aCervenyPortalGun = new PredmetPortalGun("cervenyPortalGun");
            aJeVytvorenyCervenyPortal = true;
            aPortalGun.put(aCervenyPortalGun.getaNazov(), paHrac.dajPoziciu());
            paHrac.dajPoziciu().nastavVychody(aCervenyPortalGun);
            aStriedaniePortalGunov = true;
            nazov = "cervenyPortalGun";
        } else {
            if (aJeVytvorenyModryPortal) {
                aPortalGun.get(aModryPortalGun.getaNazov()).zmazVychod(aModryPortalGun.getaNazov());
                aPortalGun.remove(aModryPortalGun.getaNazov());
            }
            aModryPortalGun = new PredmetPortalGun("modryPortalGun");
            aJeVytvorenyModryPortal = true;
            aPortalGun.put(aModryPortalGun.getaNazov(), paHrac.dajPoziciu());
            paHrac.dajPoziciu().nastavVychody(aModryPortalGun);
            aStriedaniePortalGunov = false;
            nazov = "modryPortalGun";
        }
        aHrac = paHrac;

        return "Vytvorili ste " + nazov + ". " + aHrac.dajPoziciu().dajPopisVychodov() + ".";
    }

    @Override
    public IMiestnost dajVychod(String paSmer) {
        if (paSmer.equals("modryPortalGun")) {
            return aPortalGun.get("cervenyPortalGun");
        }
        return aPortalGun.get("modryPortalGun");
    }

    @Override
    public void vlozPredmet(IPredmet paPredmet) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void nastavVychody(IMiestnost paMiestnost) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String dajPopis() {
        return aHrac.dajPoziciu().dajPopis();
    }

    @Override
    public String dajPopisVychodov() {
        return aHrac.dajPoziciu().dajPopisVychodov();
    }

    @Override
    public String getaNazov() {
        return aNazov;
    }

    @Override
    public String dajPredmety() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void vypisInfoMiestnosti() {
        System.out.println("Teraz ste v miestnosti: " + dajPopis());
        dajPopisVychodov();
    }

    @Override
    public IPredmet zoberPredmet(String paNazov) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean maPredmety() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public IPredmet zistiPredmet(String paNazov) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void zmazVychod(String paMiestnost) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void vlozOsobu(IOsoba osoba) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void vymazOsobu(String paMenoOsoby) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String vypisOsoby() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public IOsoba dajOsobu(String paMeno) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int dlzkaZatNajdNajkrCesZoSucVrch() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IMiestnost dajPredchodcuVrchola() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setaDlzkaZoSucasnejDoTejtoMiestnosti(int aDlzkaZoSucasnejDoTejtoMiestnosti) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setaPredchodca(IMiestnost aPredchodca) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void vymazVychod(IMiestnost paMiestnost) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean jePristupna(Hrac paHrac) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarryhoalgoritmus;

/**
 *
 * @author Dominik
 */
public class Hrana 
{
   private boolean leftToRight;
   private boolean rightToLeft;
   private boolean firstComing;
   private int leftPartOfEdge;
   private int rightPartOfEdge;

    public Hrana() {
        this.leftToRight = false;
        this.rightToLeft = false;
        this.firstComing = false;
    }
   

   
   
   
   
   public void setLeftToRight(boolean leftToRight) {
        this.leftToRight = leftToRight;
    }

    public void setRightToLeft(boolean rightToLeft) {
        this.rightToLeft = rightToLeft;
    }

    public void setFirstComing(boolean firstComing) {
        this.firstComing = firstComing;
    }

    public void setLeftPartOfEdge(int leftPartOfEdge) {
        this.leftPartOfEdge = leftPartOfEdge;
    }

    public void setRightPartOfEdge(int rightPartOfEdge) {
        this.rightPartOfEdge = rightPartOfEdge;
    }

    public boolean isLeftToRight() {
        return leftToRight;
    }

    public boolean isRightToLeft() {
        return rightToLeft;
    }

    public boolean isFirstComing() {
        return firstComing;
    }

    public int getLeftPartOfEdge() {
        return leftPartOfEdge;
    }

    public int getRightPartOfEdge() {
        return rightPartOfEdge;
    }
}

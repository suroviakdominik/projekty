;---------------------------------------------------------------ZADANIE-POSTUPNOST-------------------------------------------------------------------
;Zadanie 1. Vypocitajte a vypiste prvky postupnosti an=2^n-1 pre n=2 to 10
;Zadanie 2. Upravte zadanie 1 tak aby ste n zadali z klavesnica, postupnost vypiste
;Zadanie 3 Prvky postupnosti zo zadania 1 zapiste do suboru
.Model small
.Stack 100h;vyhradi 100h=256B pamati pre zasobnik
.DATA;//zacina datovy segment
Postupnost DW 100 dup (?)
Postupnost2 DW 100 dup (?)
Postupnost3 DW 3,4,5,6,7,8,9,10,11
DvojkaPriNasobeni DW 2 
DesiatPriNas DW 10
RetazeNaZnaky DB 200 dup (?)
Retaz DB 9 DUP(?);deklaracia pola o velkosti 9B
Exponent DW ?
PopisovacSuboru DW ?
ZapisanyZnakDoSuboru DB ?
MenoSuboru DB 'TextSub.txt',0 
VypZadN DB 'Zadajte n: $'
CisloPriVypMocniny DW ?
;------------------------------------------------------------------------------------------------------------------------------------------------------
.CODE;//zacina kodovy segment
;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

                                                                                                                         
Write ENDP

WriteFile PROC 
      push dx
      push cx
      push ax
      push bx
      mov cx,0
          
     
    
     
      KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        push cx
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        loop VypisCisl
        
       mov di,0
       pop bx
       pop ax
       pop cx
       pop dx
       ret

     

WriteFile ENDP

Read PROC
          push di
          push dx
          push cx
          push bx  
        
          mov di, 2
          mov Retaz,7;do retaze na 1B ulozit max 7 preto lebo cislo+0Dh+velkost retazca = 7
          
          mov DX,offset Retaz;do registra dx ulozime offset Retazca
          mov AH,0Ah;do registra Ah ulozime cislo sluzby, ktora sa ma nasledne vykonat
          int 21h
           
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          mov DL,0DH;uloz do registra posun vlavo
          mov AH,2
          int 21h
          mov ax,0
          
          cmp [Retaz+2],'-'
          jz CisloJeZaporne
          mov bl,0
      
     
      KonvertujRetazecNaCislo:          
          mov cl,[Retaz+di];uloz do cl znak na adrese ds:[Retaz + di]
          cmp cl,0DH;porovnaj hodnotu v dl s 0DH                                                                    
          jz RozhodniCiJeCisloZaporne
          sub cl,'0';ked odcitame ascii nulu dostaneme nie ascii kod ale hodnotu znaku
          mul DesiatPriNas;konvertuje cislo nacitane s klavesnice na dvojkove cislo
          mov ch,0
          add ax,cx
          inc di
          jmp KonvertujRetazecNaCislo
     
     CisloJeZaporne:
          inc di;ak je zaporne nastavi bl na 1, cim mozeme neskor zistit ci je zaporne
          mov bl, 1
          jmp KonvertujRetazecNaCislo     
 
     DvojkovyDoplnok:
         neg ax;dvojkovy doplnok zmeni znamienko cisla      
         jmp Navrat
     
RozhodniCiJeCisloZaporne:
          cmp bl,1
          jz DvojkovyDoplnok
          mov di,0
          mov cx,0
          
     Navrat:
         pop bx 
         pop cx
         pop dx
         pop di  
         ret
                                                                                                                        
Read ENDP 



Zad1 PROC
 push ax
 push bx 
 push cx
 push dx
 push si
 push di
 mov cx,1
 mov si,0
 mov di,0
 mov bx,cx
 InicAX:
 mov ax,2
 VypocMocninu:
   mul DvojkaPriNasobeni
   loop VypocMocninu
   inc si
 Ukon:
    cmp si,10
    jl Uloz
    jmp Inl
 Uloz: 
   sub ax,1
   mov Postupnost[di],ax
   add di,2
   inc bx
   mov cx,bx
   
   jmp InicAx
 
 Inl:
 mov di,0
 mov cx,9
 Vyp:
 mov ax,Postupnost[di]
 call Write
 mov dl,0Ah
 mov ah,2
 int 21h
 
 mov dl,0Dh
 mov ah,2
 int 21h
 inc di
 inc di
 loop Vyp
 
 

 pop di 
 pop si
 pop dx
 pop cx
 pop bx
 pop ax
 ret
          
Zad1 ENDP




Zad2 PROC
;n musi byt v tejto verzii mensie ako 16, lebo 2 na 16 prekrocuje datovy typ word
push ax
 push bx 
 push cx
 push dx
 push si
 push di
mov dx, offset VypZadN
mov ah,9
int 21h


ZadN:
   call Read
   cmp ax,0
   jl ZadN

mov di,0
mov si,0
mov Exponent,ax
mov cx,Exponent


SetCx:
mov ax,2;paramater proc vypmocninu
push exponent
sub exponent,cx
mov cx,exponent
pop exponent

VypocetClenyPostupnosti:
call VypocitajMocninu 
sub ax,1  
mov Postupnost2[di],ax
inc di
inc di
cmp si,Exponent
je InicialPreVypis 
inc si
mov cx,exponent
sub cx,si
jmp SetCx   

InicialPreVypis:
mov di,0
mov cx,exponent
add cx,1
 
VypisDoTerminalu:
mov ax,Postupnost2[di]
call Write
inc di
inc di
loop VypisDoTerminalu

Re:
 pop di 
 pop si
 pop dx
 pop cx
 pop bx
 pop ax
ret
   
Zad2 ENDP  
;----------------------------------------------------PROCEDURA VYPOCET MOCNINY----------------------------------------------------------
;vstupparam-zaklad typu prirodzene cislo v registri ax,exponent prirodzene cislo v registri cx
;vystup-umocncene cislo v registri dx:ax
VypocitajMocninu PROC
push cx
dec cx
cmp cx,0
je VyskocZproc
cmp cx,-1
je UmocneneCisloJeJedna

mov CisloPriVypMocniny,ax
Umocnuj:
mul CisloPriVypMocniny 
loop Umocnuj
jmp VyskocZproc

UmocneneCisloJeJedna:
mov ax,1

VyskocZproc:
pop cx
ret
VypocitajMocninu ENDP

;-------------------------------------------------------PROCEDURA ZADANIE 3-----------------------------------------------------------------
Zad3 PROC
push ax
push bx 
push cx
push dx
push si
push di


 mov si,0
 mov cx,9
 mov bx,PopisovacSuboru
 Vypisuj:
 mov ax,Postupnost[si]
 
 call WriteFile
 
 push cx
 mov cx,1
 mov ZapisanyZnakDoSuboru,' '
 mov dx,offset ZapisanyZnakDoSuboru        
 mov ah,40h
 int 21h
 pop cx
 inc si
 inc si
 loop Vypisuj
 pop di 
 pop si
 pop dx
 pop cx
 pop bx
 pop ax
 ret 
Zad3 ENDP

;------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call Zad1
          call Zad2
        
        mov cx,0;vytvori subor pre zapis
        mov dx,offset MenoSuboru
        mov ah,3Ch
        int 21h
        mov PopisovacSuboru,AX
        
        call Zad3
        
        mov bx,PopisovacSuboru
        mov ah,3Eh
        int 21h
      
Koniec: 
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































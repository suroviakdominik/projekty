

public class Tester
{
    private Banka aBanka;
    
    public Tester()
    {
        GeneratorAdresy ag= new GeneratorAdresy();   
        aBanka=new Banka(1234,10,ag.generuj());
    }
    
    public void vytvorKlientov(int paPocetKlientov)
    {
        GeneratorOsoby pg= new GeneratorOsoby();
        for (int i=0;i<paPocetKlientov;i++)
        {
            aBanka.zriadUcet(pg.generuj());
        }
    }
    
    public void vypisKlientov()
  {
      for (Klinet k:aBanka.dajKlientov())
      {
          System.out.println(k);
      }    
  }   
}

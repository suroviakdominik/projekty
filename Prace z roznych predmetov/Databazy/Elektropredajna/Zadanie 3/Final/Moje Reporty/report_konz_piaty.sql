set pagesize 60
set linesize 123
column Predajna.nazov_predajne heading "Nazov Predajne" FORMAT A20;
column Kategoria.nazov_kat heading "Kategoria" FORMAT A20;
column Tovar.nazov heading "Nazov Tovaru" FORMAT A20;
column Tovar.CENA heading "Cena" FORMAT 999,999.00;

column datum NEW_VALUE datum NOPRINT

ttitle center 'Vypis tovarov zadanej kateorie od najlacnejsich po najdrahsie' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia: " datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
PREDAJNA.NAZOV_PREDAJNE,nazov_kat,nazov,TOVAR.CENA --piaty select od konzultanta
FROM KATEGORIA JOIN TOVAR using(id_kategorie) JOIN  PREDAJNA using(id_predajne)
WHERE(nazov_kat = '&&Premenna')
ORDER BY CENA,nazov;
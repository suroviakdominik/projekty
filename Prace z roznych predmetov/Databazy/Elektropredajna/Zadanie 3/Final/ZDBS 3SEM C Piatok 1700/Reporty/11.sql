set pagesize 60
set linesize 123
column Tovar.NAZOV HEADING "NAZOV" FORMAT A20;
column Tovar.NAZOV HEADING "NAZOV" FORMAT A20;
column datum NEW_VALUE datum NOPRINT

ttitle center 'Prehlad odporucanych tovarov k tovarom' skip 2 LEFT "Strana" sql.pno skip 1 LEFT "Datum vytvorenia: " datum;

select TO_CHAR(SYSDATE,'DD.MM.RRRR') datum,
B.nazov, A.nazov
FROM
(SELECT katalogoveCisloODPTovaru, nazov FROM Tovar join OdporucaneTovary on (Tovar.katalogoveCislo = OdporucaneTovary.katalogoveCisloODPTovaru)) A
left join
(select A.nazov, C.katalogoveCisloODPTovaru FROM  (SELECT katalogoveCislo, nazov FROM Tovar) A 
left join (SELECT katalogoveCislo, katalogoveCisloODPTovaru FROM OdporucaneTovary ) C
on A.katalogoveCislo  = C.katalogoveCislo)B
on A.katalogoveCisloODPTovaru  = B.katalogoveCisloODPTovaru
GROUP BY B.nazov, A.nazov;
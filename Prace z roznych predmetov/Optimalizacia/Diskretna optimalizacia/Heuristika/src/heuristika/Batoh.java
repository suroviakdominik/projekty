/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heuristika;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Dominik
 */
public class Batoh {

    private class Predmet {

        private int aVaha;
        private int aCisloPredmetu;
        private int aCena;

        public Predmet(int aCisloPredmetu, int aVaha, int paCena) {
            this.aVaha = aVaha;
            this.aCisloPredmetu = aCisloPredmetu;
            this.aCena = paCena;
        }

        public int getaCena() {
            return aCena;
        }

        public void setaCena(int aCena) {
            this.aCena = aCena;
        }

        public void setaVaha(int aVaha) {
            this.aVaha = aVaha;
        }

        public void setaCisloPredmetu(int aCisloPredmetu) {
            this.aCisloPredmetu = aCisloPredmetu;
        }

        public int getaVaha() {
            return aVaha;
        }

        public int getaCisloPredmetu() {
            return aCisloPredmetu;
        }

    }
    private int aZKolkychPredmetovVyberame;
    private int aKapacitaVKg;
    private int aMaxPocetPredmetovVBatohu;
    private int aSucasnaVahaBatohu;
    private int aPocetVlozenychPredmetov;
    private Predmet[] aPredmety;
    private int aVolneMiestoVBatohu;
    private long aZisk;
    private boolean aPrebehlaVymennaHeuristika = false;

    public Batoh() {
        aVolneMiestoVBatohu = 0;
        aPocetVlozenychPredmetov = 0;
        aSucasnaVahaBatohu = 0;
        aZisk = 0;
    }

    public void setaPrebehlaVymennaHeuristika(boolean aPrebehlaVymennaHeuristika) {
        this.aPrebehlaVymennaHeuristika = aPrebehlaVymennaHeuristika;
    }

    public boolean jeInicializovany() {
        return aMaxPocetPredmetovVBatohu > 1 && aKapacitaVKg > 0 && aZKolkychPredmetovVyberame >= aMaxPocetPredmetovVBatohu;
    }

    public void setaKapacita(int aKapacita) {
        this.aKapacitaVKg = aKapacita;
        aPocetVlozenychPredmetov = 0;
        aSucasnaVahaBatohu = 0;
        aVolneMiestoVBatohu = 0;
        aZisk = 0;
    }

    public void setaMaxPocetPredmetovVBatohu(int aMaxPocetPredmetovVBatohu) {
        this.aMaxPocetPredmetovVBatohu = aMaxPocetPredmetovVBatohu;
        aPredmety = new Predmet[aMaxPocetPredmetovVBatohu];
        aPocetVlozenychPredmetov = 0;
        aSucasnaVahaBatohu = 0;
        aVolneMiestoVBatohu = 0;
        aZisk = 0;
    }

    public int getaZKolkychPredmetovVyberame() {
        return aZKolkychPredmetovVyberame;
    }

    public void setaZKolkychPredmetovVyberame(int aZKolkychPredmetovVyberame) {
        this.aZKolkychPredmetovVyberame = aZKolkychPredmetovVyberame;
    }

    public long getaZisk() {
        return aZisk;
    }

    public int getaKapacita() {
        return aKapacitaVKg;
    }

    public int getaMaxPocetPredmetovVBatohu() {
        return aMaxPocetPredmetovVBatohu;
    }

    public int getaSucasnaVahaBatohu() {
        return aSucasnaVahaBatohu;
    }

    public int getaPocetVlozenychPredmetov() {
        return aPocetVlozenychPredmetov;
    }

    private void triedPodlaCislaPredmetuVzostupne() {
        boolean boloprehadzovane = true;
        for (; boloprehadzovane;) {
            boloprehadzovane = false;
            for (int j = 0; j < aPocetVlozenychPredmetov - 1; j++) {
                if (aPredmety[j].getaCisloPredmetu() > aPredmety[j + 1].getaCisloPredmetu()) {
                    Predmet lpPrvy = aPredmety[j];
                    aPredmety[j] = aPredmety[j + 1];
                    aPredmety[j + 1] = lpPrvy;
                    boloprehadzovane = true;
                }
            }
        }

    }

    public boolean vlozPredmet(int paCisloPredmetu, int paVaha, int paCena) {
        if (aSucasnaVahaBatohu < aKapacitaVKg && aPocetVlozenychPredmetov < aMaxPocetPredmetovVBatohu) {
            aPredmety[aVolneMiestoVBatohu] = new Predmet(paCisloPredmetu, paVaha, paCena);
            aSucasnaVahaBatohu += paVaha;
            aPocetVlozenychPredmetov++;
            aVolneMiestoVBatohu++;
            aZisk += paCena;
            triedPodlaCislaPredmetuVzostupne();
            return true;
        } else {
            return false;
        }
    }

    public boolean odoberPredmet(int paCisloPredmetu) {
        for (int i = 0; i < aPocetVlozenychPredmetov; i++) {
            if (aPredmety[i].getaCisloPredmetu() == paCisloPredmetu) {
                aSucasnaVahaBatohu -= aPredmety[i].getaVaha();
                aZisk-=aPredmety[i].getaCena();
                aPredmety[i] = null;
                aPocetVlozenychPredmetov--;
                aVolneMiestoVBatohu--;
                for (int j = i; j < aPocetVlozenychPredmetov; j++) {
                    aPredmety[j] = aPredmety[j + 1];
                }

                return true;
            }
        }
        return false;
    }

    private boolean kontrolaTriedeniaVzostupne() {
        for (int i = 0; i < aPocetVlozenychPredmetov - 1; i++) {
            if (aPredmety[i].getaCisloPredmetu() > aPredmety[i + 1].getaCisloPredmetu()) {
                return false;
            }
        }
        return true;
    }

    public int getPoradovCisloPredmetu(int indexPredmetu) {
        return aPredmety[indexPredmetu].getaCisloPredmetu();
    }
    
    public int getCenaPredmetu(int indexPredmetu)
    {
        return aPredmety[indexPredmetu].getaCena();
    }

    public void zobrazSa(Graphics2D gg, int poziciaX, int poziciaY, int width, int height) {

        gg.drawString(new Date().toString(), poziciaX + 20, poziciaY + height - 60);
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 16));//nastavi typ pisma
        gg.setColor(Color.DARK_GRAY);

        gg.drawLine(poziciaX + 20 + 10 * width / 200 - 5 * width / 200, poziciaY + 5, poziciaX + 20 + 10 * width / 200 + 5 * width / 200 + 30, poziciaY + 5);
        gg.drawLine(poziciaX + 20 + 10 * width / 200 - 5 * width / 200, poziciaY + 5, poziciaX + 20 + 10 * width / 200 - 5 * width / 200, poziciaY + 20);
        gg.drawLine(poziciaX + 20 + 10 * width / 200 + 5 * width / 200 + 30, poziciaY + 5, poziciaX + 20 + 10 * width / 200 + 5 * width / 200 + 30, poziciaY + 20);

        gg.drawRect(poziciaX + 20 + 10, poziciaY + 20, 10 * width / 100 - 20 + 30, 10 * height / 100);
        gg.drawOval(poziciaX + 20 + 15, poziciaY + 20 + 10 * height / 100 - 15, 10, 10);
        gg.drawOval(poziciaX + 20 + 10 * width / 100 + 5, poziciaY + 20 + 10 * height / 100 - 15, 10, 10);

        gg.drawRect(poziciaX + 20, poziciaY + 20, 10 * width / 100 + 30, 20 * height / 100);
        gg.setFont(new Font(Font.MONOSPACED, Font.BOLD, 8 * (10 * width / 100 + 30) / 100 + 5 * (10 * height / 100) / 100));//nastavi typ pisma
        gg.drawString("Max Kg:" + "\n" + aKapacitaVKg, poziciaX + 40, poziciaY + 10 + 10 * height / 200);
        gg.drawString("Max Ks:" + "\n" + aMaxPocetPredmetovVBatohu, poziciaX + 40, poziciaY + 25 + 10 * height / 200);
        gg.drawString("Akt Kg:" + aSucasnaVahaBatohu, poziciaX + 40, poziciaY + 10 + 30 * height / 200);
        gg.drawString("Akt Ks:" + aPocetVlozenychPredmetov, poziciaX + 40, poziciaY + 25 + 30 * height / 200);
        gg.drawString("Zisk  :" + aZisk, poziciaX + 40, poziciaY + 40 + 30 * height / 200);
        gg.drawString("Batoh mozno naplnit z: " + aZKolkychPredmetovVyberame + " predmetov.", poziciaX + 20, poziciaY);

        if (aPocetVlozenychPredmetov > 0) {
            int lpPozY = poziciaY + 35 * height / 100;
            triedPodlaCislaPredmetuVzostupne();
            if (kontrolaTriedeniaVzostupne()) {
                String s = "<";

                if (aPrebehlaVymennaHeuristika) {
                    gg.drawString("Vysledok výmennej heuristiky prvý vhodný v poradí ako sú údaje uložené v súbore: ", 20, lpPozY - 20);
                } else {
                    gg.drawString("Vysledok heuristiky s vyhodnostnymi koeficientami v poradí ako sú údaje uložené v súbore: ", 20, lpPozY - 20);
                }
                for (int k = 0; k < aZKolkychPredmetovVyberame; k++) {
                    boolean bolZaradeny = false;
                    for (int j = 0; j < this.getaPocetVlozenychPredmetov(); j++) {
                        if (aPredmety[j].getaCisloPredmetu() == k) {
                            bolZaradeny = true;
                        }
                    }
                    if (!bolZaradeny) {
                        s += " 0,";
                    } else {
                        s += " 1,";
                    }

                    if (k > 0 && k % 40 == 0) {
                        gg.drawString(s, 20, lpPozY);
                        lpPozY += 15;
                        s = "";
                    }
                }
                s += ">";
                gg.drawString(s, 20, lpPozY);
            }

        }
        gg.drawString("Vysledok heuristiky je v poradí pôvodnej úlohy nie prerovnanej podľa výh. koeficientov t.j. je v takom poradí,", 20, 85 * height / 100);
        gg.drawString(" ako sú data uložené v textových súboroch(prvý údaj vo výsledku =údaj o (ne)vložení prvého predmetu v súbore)", 20, 85 * height / 100 + 15);

    }
}

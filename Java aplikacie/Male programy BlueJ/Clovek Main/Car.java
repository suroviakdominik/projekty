
/**
 * Write a description of class Car here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Car
{
 private String Farba; 
 private float Hmotnost;
 private double Cena;
 private String Typ;
 private short Vykon;
 
 public Car( String Farba,
             float Hmotnost,
             double Cena,
             String Typ,
             short Vykon)
           
 {
     this.Farba = Farba;
     this.Hmotnost =Hmotnost;
     this.Cena = Cena;
     this.Typ = Typ;
     this.Vykon = Vykon;
 
    }
public float getHmotnost()
{
 return Hmotnost;
}
public String getFarba()
{
return Farba;
}
public double getCena()
{
return Cena;
}
public String getTyp()
{
return Typ;
}
public short getVykon()
{
return Vykon;
} 
public void setHmotnost(float hmotnost)
{
Hmotnost = hmotnost;
}
public void setFarba( String farba)
{
Farba = farba;
}
public void setCena( double cena)
{
Cena = cena;
}
public void setTyp( String typ)
{
Typ = typ;
}
public void setVykon(short vykon)
{
Vykon = vykon;
}
public String naString()
{
String vysledok;
vysledok = "Typ: " + Typ;
vysledok += " Farba: " + Farba;
vysledok += " Hmotnost: " + Hmotnost;
vysledok += " Vykon: " + Vykon;
vysledok += " Cena: " + Cena; 
return vysledok;
}
}



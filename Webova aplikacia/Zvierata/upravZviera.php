<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
class DBPraca {

    private $servername = "localhost";
    private $meno = "root";
    private $heslo = "";
    private $databaza = "zvierata";
    private $conn;
    private $biggestId = 0;

    function __construct() {
        $this->conn = new mysqli($this->servername, $this->meno, $this->heslo, $this->databaza);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_query($this->conn, "SET CHARACTER SET utf8");
        //echo "Connected successfully";
    }

    function upravZviera() {
        $nazov = htmlspecialchars($_POST["nazov"]);
        $popis = htmlspecialchars($_POST["popis"]);
        $id = htmlspecialchars($_POST["idUpZv"]);
        $obrN = htmlspecialchars($_POST["obrN"]);
        $sql = sprintf("UPDATE zviera SET nazov='%s',popis='%s',nazovObrazok='%s' WHERE id=%d",
                $this->conn->real_escape_string($nazov), 
                $this->conn->real_escape_string($popis),
                $this->conn->real_escape_string($obrN), 
                $this->conn->real_escape_string($id));

        if ($this->conn->query($sql) === TRUE) {
            header("Location: index.html");
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . $this->conn->error;
        }
    }

    function ukonciPristup() {
        $this->conn->close();
    }

}
if (isset($_SESSION["login"]) && $_SESSION["login"] == 0) {
$db = new DBPraca();
$db->upravZviera();
$db->ukonciPristup();
}

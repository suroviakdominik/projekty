/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package poratajslova;

/**
 *
 * @author Hawk
 */
public class Word implements Comparable{

    private String text;
    private int count;

    public Word(String text){
        this.text = text;
        count = 1;
    }

    public void inc(){
        count ++;
    }

    public int compareTo(Object o) {
        return -1*((Word)o).getText().compareToIgnoreCase(this.getText());
    }

    public String getText() {
        return this.text;
    }

    public int getCount() {
        return this.count;
    }

    @Override
    public String toString() {
        return this.text + " <--> " + this.count + "\n";
    }
}

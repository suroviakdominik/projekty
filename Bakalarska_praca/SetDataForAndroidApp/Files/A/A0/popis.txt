Je najväčšia šelma, ktorá žije na území Národného parku. Samce dosahujú hmotnosť až 300, výnimočne 350 kg. Samice sú menšie a ich hmotnosť sa pohybuje okolo 160 kg. Farba kožušiny je hnedá s premenlivým tónovaním od svetlohnedej až po čiernu. Medveď je ploskochodec, čo znamená, že našľapuje na celú plochu chodidla a nie len na brušká prstov, ako je to u iných šeliem. Jeho chôdza sa zdá byť ťarbavá, pretože pri chôdzi aj kluse pohybuje vždy skoro súčasne oboma nohami na tej istej strane tela. Jeho ťarbavosť je ale len zdanlivá. Medveď je v skutočnosti veľmi rýchle a obratné zviera. Veľmi dobre - hlavne mladé jedince - šplhá na stromy. S obľubou na tie, na ktorých majú hniezda divé včely.

Zo zmyslov má najlepšie vyvinutý čuch a sluch. Zrak je vzhľadom na malé oči slabší - nehybnú ľudskú postavu rozozná zo vzdialenosti 10-30 m. 

Medveď je aktívny po celý deň, ale hlavne v podvečerných a ranných hodinách. Žije samotársky na svojom teritóriu. Len v čase párenia a v letnom období (v čase dozrievania lesných plodov a v čase, keď samica vodí mláďatá) môžeme vidieť pohromade viac jedincov.

Medveď nemá pravý zimný spánok. Brloh, do ktorého začiatkom zimy zaľahne, počas zimy niekoľko krát opustí. Sú známe prípady, kedy sa medveď vôbec neuložil spať. Mláďatá sa rodia v zime, v období medzi koncom decembra a polovicou februára. Sú úplne slepé a bezbranné. Ich hmotnosť sa pohybuje okolo 0,5 kg. Mláďatá zostávajú s matkou až 3 roky. Medveď je všežravec. Živí sa lesnými plodmi, korienkami, ovocím, väčším hmyzom, slimákmi, menšími stavovcami, zdochlinami a obľubuje med. Občas sa stane, že si zo zle stráženého salaša odnesie ovcu.

Predísť stretnutiu s medveďom môžete ak:
- sa pohybujete výhradne po značených turistických chodníkoch, neodbočujete do voľného terénu
- počas prechádzky v lese občas stupíte na suchý konárik, klopkáte palicou po kmeňoch stromov, alebo sa potichu medzi sebou rozprávate, občas zakašlete
- na zber lesných plodov nepôjdete v skorých ranných hodinách
- pri spozorovaní medveďa nečakáte, kým príde bližšie, ale pomaly od neho ustupujete. Toto platí hlavne v prípade spozorovania mláďat. Nesnažte sa ich fotografovať - ich matka je veľmi zúrivá.

Ak už sa stane, že vás napadne, snažte sa ľahnúť si tvárou na zem a rukami si chrániť krk. Robte sa mŕtvi. Nebráňte sa, ani nerobte prudké pohyby. (Takáto reakcia vyvolá útočnú protireakciu medveďa.) Ak sa nebudete hýbať, po chvíli od vás odíde.

Medveď je celoročne chráneným živočíchom a má v prírode Národného parku Nízke Tatry i celého Slovenska významnú úlohu. Zasluhuje si našu pozornosť i ochranu. 

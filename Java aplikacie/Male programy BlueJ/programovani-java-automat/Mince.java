/**
 * Trida slouzi k uchovani hodnoty mince
 * 
 * @author Jirka Krhanek
 * @version 2006-01-02
 */
public class Mince   // trida slouzi k vytvareni objektu tedy minci 
{
  private int hodnota;

/**
 * Konstruktor pro objekty tridy Mince
 * 
 * @param nHodnota hodnota vytvorene mince
 */
  public Mince(int nHodnota) 
  {
    hodnota = nHodnota;   // uchovava pouze hodnotu mince
  }
    
/**
 * Metoda vraci hodnotu mince
 * 
 * @return hodnotu mince
 */
  public int vratHodnotu() 
  {
    return hodnota;   // vraci hodnotu mince
  }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grepbezprepinacov;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Dominik
 */
public class Grep {
    private File aFilesToRead;
    private String aRegularExpression;
    private BufferedReader aReader;
    private Pattern aPattern;

    public Grep(File aFilesToRead, String aRegularExpression) {
        this.aFilesToRead = aFilesToRead;
        this.aRegularExpression = aRegularExpression;
        aReader=FileWorker.getBufferedReader(aFilesToRead);
        aPattern=Pattern.compile(aRegularExpression);
        
    }
    
    public void makeGrep()
    {
        if(aReader!=null)
        {
            String readLine=null;
            Matcher match=null;
            try {
                while((readLine=aReader.readLine())!=null)
                {
                   match=aPattern.matcher(readLine);
                   if(match.find())
                   {
                       System.out.println(readLine);
                   }
                   else
                   {
                       System.err.println(readLine);
                   }
                }
            } catch (IOException ex) {
                Logger.getLogger(Grep.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
        else
        {
            System.err.println("MyLogsERROR: File reader is no initialize");
        }
        
        try {
            if(aReader!=null)
            aReader.close();
        } catch (IOException ex) {
            Logger.getLogger(Grep.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    
    
}

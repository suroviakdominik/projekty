package linearne.štrukurované.datové.typy.GUI;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author suroviak3
 */
import java.awt.*;
import javax.swing.*;
import linearne.štrukurované.datové.typy.Front;
import linearne.štrukurované.datové.typy.ILinearDataTypy;
import linearne.štrukurované.datové.typy.Zasobnik;
import linearne.štrukurované.datové.typy.Zoznam;

/**
 * Trieda Platno predstavuje šablónu, podľa ktorej budú vytvárané objekty, ktoré
 * sa budú používať v GUI ako komponenty na vykreslenie jedntlivých ADŠ
 *
 * @author Dominik
 */
public class Platno extends JComponent {

    private final ILinearDataTypy aTyp;

    /**
     * Konštrukto má za úlohu priradiť atribútu aTyp ADŠ, ktorá bude n atoto
     * plátno vykresľovaná.
     *
     * @param paTyp :ILinearDataTypy- ADŠ používajúci toto plátno
     */
    public Platno(ILinearDataTypy paTyp) {
        this.aTyp = paTyp;
    }

    /**
     * Metóda vykresľuje danú ADŠ na plátno do hlavného okna GUI
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) // MUSI sa volat presne takto
    {
        Graphics2D gg = (Graphics2D) (g);
        aTyp.zobrazSa(gg, getWidth() * 10 / 100, getHeight() * 5 / 100);
        this.repaint();

        gg.setColor(Color.DARK_GRAY);
        gg.drawString("Dominik Suroviak", getWidth() - 120, getHeight() - 60);
        gg.drawString("Patrik Kruželák", getWidth() - 120, getHeight() - 40);
        gg.drawString("Branislav Juriš", getWidth() - 120, getHeight() - 20);

    }
}

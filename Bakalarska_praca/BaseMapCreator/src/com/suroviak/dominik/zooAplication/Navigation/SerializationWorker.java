/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suroviak.dominik.zooAplication.Navigation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Dominik
 */
public class SerializationWorker<T>{
    private T aObjectThatIsSerializable;
    private String aEndOfFiles;
    private String aNameOfFile;
    private String aPath=null;

    public SerializationWorker(String paEndOfFiles) {
        aEndOfFiles=paEndOfFiles;
    }

    public void setNameOfFile(String paNameOfFile) {
        this.aNameOfFile = paNameOfFile;
    }
    
    

    public void setObjectThatIsSerializable(T aObjectThatIsSerializable) {
        this.aObjectThatIsSerializable = aObjectThatIsSerializable;
    }
    
    public void setPath(String paPath)
    {
        aPath=paPath;
    }
    
    
    public boolean saveObjectToFile()
    {
        FileOutputStream stream = null;
        ObjectOutputStream writer=null;
        try {
            File f = new File(aPath+aNameOfFile+"."+aEndOfFiles);
            stream = new FileOutputStream(f);
            writer=new ObjectOutputStream(stream);
            writer.writeObject(aObjectThatIsSerializable);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
                stream.close();
                return true;
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    public T readObjetctFromFile()
    {
        T ret=null;
        
        FileInputStream stream =null;
        ObjectInputStream reader=null;
        try {
            File f = new File(aPath+aNameOfFile+"."+aEndOfFiles);
            stream = new FileInputStream(f);
            reader = new ObjectInputStream(stream);
            ret =(T)reader.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            try
            {
                reader.close();
                stream.close();
            }
            catch(IOException ex){
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ret;
    }
}




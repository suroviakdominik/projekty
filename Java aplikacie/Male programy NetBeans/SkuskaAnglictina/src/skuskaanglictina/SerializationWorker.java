/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skuskaanglictina;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik
 */
public abstract class SerializationWorker {
    public static <T> void saveObjectToFile(File f,T paObject)
    {
        ObjectOutputStream oOS=null;
        try {
            FileOutputStream str=new FileOutputStream(f);
            oOS=new ObjectOutputStream(str);
            oOS.writeObject(paObject);
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oOS.close();
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static <T> T readObjectFromFile(File f)throws IOException, ClassNotFoundException
    {
        FileInputStream fIS=null;
        try {
            fIS = new FileInputStream(f);
            ObjectInputStream oIS=new ObjectInputStream(fIS);
            return (T)oIS.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (IOException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            try {
                fIS.close();
            } catch (IOException ex) {
                Logger.getLogger(SerializationWorker.class.getName()).log(Level.SEVERE, null, ex);
                throw ex;
            }
        }
        
    }
}

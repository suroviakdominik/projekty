;a. Definujte v pamati postupnost celych cisel z intervalu <-32768; 32767>. Vypiste na obrazovku najmensi a najvacsi prvok postupnosti.
;b. Vymente v postupnosti najmensi prvok s najvacsim a znovu ju vypiste.
;c. Vypiste vysledok do suboru

.MODEL small
.STACK 100h
.DATA
Postupnost DW -2,1,-6,-32760,-8,-9,-10,45,9,2,3,5,8,13,21
DlzkaPostupnosti DW 15
NajmensiPrvokPostupnosti DW (?)
NajvacsiPrvokPostupnosti DW (?)
IndexNajmensiehoPrvkuPostupnosti DW (0)
IndexNajvacsiehoPrvkuPostupnosti DW (0)
DesiatPriNas DW 10
ZapisanyZnakDoSuboru DB ?
MenoSuboru DB 'TextSub.txt',0 
PopisovacSuboru DW ?
VypisNajmensPrvok DB 'Najmensi prvok postupnosti: $'
VypisNajvacsPrvok DB 'Najvacsi prvok postupnosti: $'
VypisIndexNajmensPrvok DB 'Index najmensieho prvku postupnosti: $'
VypisIndexNajvacsPrvok DB 'Index najvacsieho prvku postupnosti: $'
.CODE

;----------------------------------------------------------------PROCEDURA KONVERTUJE CISLO V DVOJKOVEJ SUSTAVE NA DESIATKOVE,KONVERTUJE NA ASCII A VYPISE DO TERMINALU----------------------------------
;vstupny parameter je cislo odovzdane v registri ax
Write PROC 
      push di
      push dx
      push cx
      push ax

     mov cx,0
     cmp ax,0
     jl VypisMinus
     
  
     KonvertujNaDekCislo:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCislo
                                                                                                                        ;RIADOK KODU 100        
          
         
     VypisCislo:
         
        pop ax;vypise skonvertovane dekadicke cislo
        add ax,'0'
        mov dl,al
        mov ah,2
        int 21h
        loop VypisCislo
        
        pop ax
        pop cx
        pop dx
        pop di
        ret

     VypisMinus:
        push ax
        mov dl,'-'
        mov ah,2
        int 21h
        pop ax
        neg ax
        jmp KonvertujNaDekCislo
                                                                                                                         
Write ENDP

;-------------------------------------------------------------------------------------PROCEDURA ZAPIS ZNAKU Z REGISTRA AX DO SUBORU FUNGUJUCA---------------------------------------------------------------------------
WriteFile PROC 
      push dx
      push cx
      push ax
      push bx
      push di
      mov cx,0
          
     
    
     cmp ax,0
     jl VypisMinu
     
     
     KonvertujNaDekCisl:
          
          mov dx,0;konvertuje dvojkove cislo na desiatkove
          div DesiatPriNas 
          push dx
          inc cx
          cmp ax,0
          jnz KonvertujNaDekCisl
          
                                                                                                                                   
         
     VypisCisl:
         
        pop ax;vypise skonvertovane dekadicke cislo do suboru
        add ax,'0'
        mov bx,PopisovacSuboru
        push cx
        mov cx,1
        mov ZapisanyZnakDoSuboru,al
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        loop VypisCisl
        
       pop di
       pop bx
       pop ax
       pop cx
       pop dx
       ret

     VypisMinu:
        push ax
        push cx
        mov al,'-'
        mov bx,PopisovacSuboru
        mov cx,1                                                                           
        mov ZapisanyZnakDoSuboru,al                                                                                           
        mov dx,offset ZapisanyZnakDoSuboru        
        mov ah,40h
        int 21h
        pop cx
        pop ax
        neg ax
        jmp KonvertujNaDekCisl

WriteFile ENDP
;-----------------------------------PROCEDURA ZMAZE OBSAH TERMINALU-----------------------------------------------
ClrScr PROC
	push ax
	
	mov ah,0
	mov al,3
	int 10h
	
	pop ax
	
	ret
ClrScr ENDP
;---------------------------------------------PROCEDURA NAJDE NAJMENSI PRVOK POSTUPNOSTI-----------------------------------------------
NajmensiPrvokPostupnost PROC
   push ax
   push bx
   push cx
   push dx
   push di
   mov bx, offset Postupnost
   mov di,0
   mov NajmensiPrvokPostupnosti,32767
   mov cx,DlzkaPostupnosti
   
   HladajNajmensiPrvok:  
    mov ax,NajmensiPrvokPostupnosti
    cmp [bx+di],ax
    jnl Pokrac
    mov ax,[bx+di]
    mov NajmensiPrvokPostupnosti,ax
    mov IndexNajmensiehoPrvkuPostupnosti,di
   Pokrac: 
    inc di
    inc di
    loop HladajNajmensiPrvok
    shr IndexNajmensiehoPrvkuPostupnosti,1
    pop di
    pop dx
    pop cx
    pop bx
    pop ax
    ret
ENDP NajmensiPrvokPostupnost
;---------------------------------------------PROCEDURA NAJDE NAJVACSI PRVOK POSTUPNOSTI-----------------------------------------------
NajvacsiPrvokPostupnost Proc
   push ax
   push bx
   push cx
   push dx
   push di
   mov bx, offset Postupnost
   mov di,0
   mov NajvacsiPrvokPostupnosti,-32768
   mov cx,DlzkaPostupnosti
   
  HladajNajvacsiPrvok:  
    mov ax,NajvacsiPrvokPostupnosti
    cmp [bx+di],ax
    jng Pokracu
    mov ax,[bx+di]
    mov NajvacsiPrvokPostupnosti,ax
    mov IndexNajvacsiehoPrvkuPostupnosti,di
   Pokracu: 
    inc di
    inc di
    loop HladajNajvacsiPrvok 
    shr IndexNajvacsiehoPrvkuPostupnosti,1 
    pop di
    pop dx
    pop cx
    pop bx
    pop ax
    ret
ENDP NajvacsiPrvokPostupnost

VypisPostupnost PROC
          push ax
          push bx
          push cx
          push dx
          push si
          push di
mov di,0
mov cx,DlzkaPostupnosti
Vyp:
 mov ax,Postupnost[di]
 call Write
  mov dl,0Ah
 mov ah,2
 int 21h
 
 mov dl,0Dh
 mov ah,2
 int 21h
 inc di
 inc di
 loop Vyp
          pop di
          pop si
          pop dx
          pop cx
          pop bx
          pop ax
          ret
ENDP VypisPostupnost

Zadanie1 PROC
          push ax
          push bx
          push cx
          push dx
          push si
          push di
;..................................................................ZADANIE 1................................................
          call VypisPostupnost
          call NajmensiPrvokPostupnost
          call NajvacsiPrvokPostupnost
          
          mov dx, offset VypisNajmensPrvok
          mov ah,9
          int 21h
          mov ax,NajmensiPrvokPostupnosti
          call Write
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h        
          
          mov dx, offset VypisNajvacsPrvok
          mov ah,9
          int 21h
          mov ax,NajvacsiPrvokPostupnosti
          call Write
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
         
          mov dx,offset VypisIndexNajmensPrvok
          mov ah,9
          int 21h
          mov ax,IndexNajmensiehoPrvkuPostupnosti
          call Write
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h

          mov dx,offset VypisIndexNajvacsPrvok
          mov ah,9
          int 21h
          mov ax,IndexNajvacsiehoPrvkuPostupnosti
          call Write
          mov DL,0AH;uloz do registra posun dole
          mov AH,2;do registra AH uloz cislo sluzby
          int 21h
          pop di
          pop si
          pop dx
          pop cx
          pop bx
          pop ax
          ret
ENDP Zadanie1

Zadanie2 PROC 
  push ax
  push bx
  push cx
  push dx
  push si
  push di
 ;...................................................................ZADANIE 2................................................
           mov si,IndexNajvacsiehoPrvkuPostupnosti
           mov di,IndexNajmensiehoPrvkuPostupnosti
           shl si,1
           shl di,1
           mov ax,Postupnost[si]
           mov bx,Postupnost[di]
           mov Postupnost[di],ax
           mov Postupnost[si],bx
           call VypisPostupnost
      pop di
      pop si
      pop dx
      pop cx
      pop bx
      pop ax
       ret
ENDP Zadanie2

Zadanie3 PROC
 push ax
 push bx
 push cx
 push dx
 push si
 push di
 ;...................................................................ZADANIE 3................................................
          
           ;---------------------------------------------VYTVORI SUBOR PRE ZAPIS A OTVORI HO------------------
        mov cx,0;vytvori subor pre zapis
        mov dx,offset MenoSuboru
        mov ah,3Ch
        int 21h
        mov PopisovacSuboru,AX
          
          ;---------------------------------------------ZAPISE PRVKY POSTUPNOSTI DO SUBORU------------------------
          mov cx,DlzkaPostupnosti
          mov di,0
         
          VypisDOSuboru:
          
            mov ax,Postupnost[di]
            call WriteFile
            inc di
            inc di
            mov bx, PopisovacSuboru
            push cx
            mov cx,1
            mov ZapisanyZnakDoSuboru,' '
            mov dx,offset ZapisanyZnakDoSuboru        
            mov ah,40h
            int 21h
            pop cx
           
          loop VypisDOSuboru
          
          ;-------------------------------ZATVORENIE SUBORU----------------------------------------
           mov bx,PopisovacSuboru
           mov ah,3Eh
           int 21h
           pop di
           pop si
           pop dx
           pop cx
           pop bx
           pop ax
           ret
ENDP Zadanie3
;-----------------------------------------------------------HLAVNY PROGRAM---------------------------------------------------------------        
Zaciatok: mov ax,@data 
          mov ds,ax;uloz bazovu adresu aby sme vedeli kde je datovy segment
          
          call Zadanie1
          call Zadanie2
          call Zadanie3
          
         
Koniec:    
        mov ax,4C00h;znamena koniec programu navrat do dos
        int 21h
END Zaciatok












































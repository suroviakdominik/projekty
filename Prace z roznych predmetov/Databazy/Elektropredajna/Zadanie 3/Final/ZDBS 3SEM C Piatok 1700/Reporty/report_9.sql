set pagesize 123
set linesize 60

TTITLE LEFT datum CENTER 'V�pis tr�ieb uctovan�ch zamestnancom za zvolen� easov� obdobie pod3a kateg�rie tovarov'
COLUMN datum NEW_VALUE datum NOPRINT

SELECT meno,priezvisko,kategoria.nazov_kat,sum(cena_Za_Fakturu)
from zamestnanec join faktura using(osobne_cislo)
join vyfakturovanyTovar using (cislo_fa)
join tovar using(katalogoveCislo)
join kategoria using (id_kategorie)
where faktura.DATUM_VYSTAVENIA >=TO_DATE('&&Od_datumu','DD.MM.RR')
and   faktura.DATUM_VYSTAVENIA<=TO_DATE('&&Do_datumu','DD.MM.RR')
group by meno,priezvisko,kategoria.nazov_kat
order by kategoria.nazov_kat;

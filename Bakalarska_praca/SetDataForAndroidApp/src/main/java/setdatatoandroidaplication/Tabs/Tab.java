/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setdatatoandroidaplication.Tabs;

import CommandProcessing.CommandProcessing;
import Server.IndeterminateProgressBar;
import Server.UploadDataToServer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import setdatatoandroidaplication.DataTransferer;

/**
 * Trieda Tab predstavuje predka pre všetky ostatné triedy predstavujúce
 * konkrétnu záložku popísanú v rámci tohto balíčka.
 * @author Dominik
 */
public abstract class Tab extends JPanel {

    
    private JPanel aMiddleTabPanel;
    protected CommandProcessing aCommandProcessingForTab;
    protected KeyListener aGlobalKeyListener;
    protected DataTransferer aDataTransferer;
    protected String aLastVisitedDir = "C:/Users/Dominik/Desktop";
    protected static final Font FONT = new Font("myfont", WIDTH, 16);
    
    /**
     * 
     * @param paTabPane- kontajner, fo ktoreho bude tab umiestneny
     * @param paKeyListener - globalzy keylistener aplikacie
     * @param paNameOfNewTab - nazov vytvaraneho tabu
     * @param paCommandProcessingForTab - vykonavac prikazov zadanych v tomto tabe
     */
    public Tab(JTabbedPane paTabPane, KeyListener paKeyListener, String paNameOfNewTab, CommandProcessing paCommandProcessingForTab) {
        super();
        
        aCommandProcessingForTab = paCommandProcessingForTab;
        aGlobalKeyListener=paKeyListener;
        //  lista bude hore
        aDataTransferer=DataTransferer.getInstance();
        this.setLayout(new BorderLayout());
        JPanel middleTabPanel = new JPanel();
        this.add(middleTabPanel);
        aMiddleTabPanel = middleTabPanel;
        aMiddleTabPanel.addKeyListener(paKeyListener);
        this.add(middleTabPanel, BorderLayout.CENTER);
        paTabPane.addTab(paNameOfNewTab, this);
        inicializujTlacidla();
    }

    /**
     * inicializuje tlacidla, ktore su pre kazdy tab rovnake
     * @param paKey - globalzy keylistener aplikacie
     */
    private void inicializujTlacidla() {
        JPanel newContentPane = new IndeterminateProgressBar(aCommandProcessingForTab.getInfoFiles());
        newContentPane.setOpaque(true); //content panes must be opaque
        newContentPane.setBackground(Color.LIGHT_GRAY);
        newContentPane.addKeyListener(aGlobalKeyListener);
        this.add(newContentPane, BorderLayout.NORTH);
     }


    /**
     * je stredny panel zapnuty
     * @param isEnable 
     */
   @Override
   public void setEnabled(boolean isEnable)
   {
       aMiddleTabPanel.setEnabled(isEnable);
   }

    public JPanel getMiddleMainTabPanel() {
        return aMiddleTabPanel;
    }

    public abstract void updateAllTabData();
    

}
